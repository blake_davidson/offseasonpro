	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor:
Leh_func_begin1:
	bx	lr
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r10, r11}
Ltmp2:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC2_0+8))
	mov	r4, r1
	mov	r1, #2
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC2_0+8))
LPC2_0:
	add	r0, pc, r0
	ldrd	r10, r11, [r0, #48]
	mov	r0, r11
	bl	_p_4_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	mov	r1, #0
	mov	r2, r6
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #1
	mov	r2, r4
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	mov	r1, r5
	bl	_p_5_plt_string_Format_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string:
Leh_func_begin3:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
	push	{r10, r11}
Ltmp5:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC3_0+8))
	mov	r6, r1
	mov	r1, #3
	mov	r10, r2
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC3_0+8))
LPC3_0:
	add	r0, pc, r0
	ldr	r11, [r0, #56]
	ldr	r0, [r0, #52]
	bl	_p_4_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	mov	r1, #0
	mov	r2, r4
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #1
	mov	r2, r6
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #2
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r11
	mov	r1, r5
	bl	_p_5_plt_string_Format_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor:
Leh_func_begin4:
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string:
Leh_func_begin5:
	sub	sp, sp, #4
Ltmp6:
	str	r0, [sp]
	str	r1, [r0, #12]
	add	sp, sp, #4
	bx	lr
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string_string_string:
Leh_func_begin6:
	push	{r4, r5, r7, lr}
Ltmp7:
	add	r7, sp, #8
Ltmp8:
Ltmp9:
	sub	sp, sp, #4
	str	r0, [sp]
	mov	r4, r3
	ldr	r5, [r0, #8]
	mov	r0, r1
	mov	r1, r2
	bl	_p_6_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, r4
	mov	r0, r5
	bl	_p_7_plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string_llvm
	sub	sp, r7, #8
	pop	{r4, r5, r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_Load_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_Load_string_string_string:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp10:
	add	r7, sp, #12
Ltmp11:
	push	{r8, r10, r11}
Ltmp12:
	sub	sp, sp, #12
	str	r0, [sp, #8]
	str	r0, [sp, #4]
	mov	r10, r3
	mov	r6, r2
	mov	r5, r1
	ldr	r0, [r0]
	bl	_p_8_plt__rgctx_fetch_0_llvm
	bl	_p_9_plt__jit_icall_mono_object_new_specific_llvm
	mov	r4, r0
	bl	_p_10_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1_TResource__ctor_llvm
	ldr	r0, [sp, #8]
	mov	r1, r5
	mov	r2, r6
	mov	r3, r10
	str	r0, [r4, #12]
	bl	_p_11_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string_llvm
	str	r0, [sp]
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC7_1+8))
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC7_1+8))
LPC7_1:
	add	r11, pc, r11
	ldr	r0, [r11, #60]
	mov	r8, r0
	bl	_p_12_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm
	mov	r6, r0
	cmp	r4, #0
	beq	LBB7_2
	mov	r0, #0
	str	r0, [r4, #8]
	ldr	r0, [r11, #20]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #16]
	ldr	r10, [sp, #8]
	ldr	r0, [r10]
	bl	_p_13_plt__rgctx_fetch_1_llvm
	str	r0, [r5, #20]
	ldr	r0, [r10]
	bl	_p_14_plt__rgctx_fetch_2_llvm
	str	r0, [r5, #28]
	mov	r2, r5
	ldr	r0, [r11, #32]
	str	r0, [r5, #12]
	ldr	r1, [r6]
	ldr	r0, [r11, #64]
	sub	r1, r1, #36
	mov	r8, r0
	mov	r0, r6
	ldr	r3, [r1]
	ldr	r1, [sp]
	blx	r3
	ldr	r0, [r4, #8]
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp13:
LBB7_2:
	ldr	r0, LCPI7_0
LPC7_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_2_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI7_0:
	.long	Ltmp13-(LPC7_0+8)
	.end_data_region
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string:
Leh_func_begin8:
	push	{r4, r5, r6, r7, lr}
Ltmp14:
	add	r7, sp, #12
Ltmp15:
	push	{r10, r11}
Ltmp16:
	sub	sp, sp, #12
	mov	r6, r1
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r1, #0
	mov	r5, r2
	mov	r10, r3
	str	r1, [sp, #8]
	ldr	r4, [r0, #8]
	mov	r0, r6
	mov	r1, r5
	bl	_p_6_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_llvm
	mov	r1, r0
	ldr	r0, [r4]
	add	r2, sp, #8
	mov	r0, r4
	bl	_p_15_plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string__llvm
	tst	r0, #255
	bne	LBB8_2
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC8_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC8_0+8))
LPC8_0:
	add	r1, pc, r1
	ldr	r0, [r1, #52]
	ldr	r11, [r1, #72]
	mov	r1, #3
	bl	_p_4_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r0, [sp, #4]
	mov	r1, #0
	ldr	r2, [r0, #12]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	mov	r1, #1
	mov	r2, r6
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	mov	r1, #2
	mov	r2, r5
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	mov	r0, r11
	mov	r1, r4
	bl	_p_5_plt_string_Format_string_object___llvm
	str	r0, [sp, #8]
LBB8_2:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC8_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC8_1+8))
LPC8_1:
	add	r1, pc, r1
	ldr	r0, [r1, #52]
	ldr	r4, [r1, #68]
	mov	r1, #2
	bl	_p_4_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r2, [sp, #8]
	mov	r1, #0
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #1
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r4
	mov	r1, r5
	bl	_p_5_plt_string_Format_string_object___llvm
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__ctor:
Leh_func_begin9:
	push	{r4, r5, r6, r7, lr}
Ltmp17:
	add	r7, sp, #12
Ltmp18:
Ltmp19:
	sub	sp, sp, #8
	mov	r4, r0
	str	r4, [sp, #4]
	str	r4, [sp]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC9_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC9_0+8))
LPC9_0:
	add	r6, pc, r6
	ldr	r0, [r6, #76]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_16_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm
	str	r5, [r4, #8]
	ldr	r0, [sp, #4]
	ldr	r1, [r6, #80]
	str	r1, [r0, #12]
	bl	_p_17_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor_llvm
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded:
Leh_func_begin10:
	push	{r4, r7, lr}
Ltmp20:
	add	r7, sp, #4
Ltmp21:
	push	{r8}
Ltmp22:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC10_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC10_0+8))
LPC10_0:
	add	r4, pc, r4
	ldr	r0, [r4, #84]
	mov	r8, r0
	bl	_p_18_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm
	ldr	r2, [r0]
	ldr	r1, [r4, #92]
	sub	r2, r2, #40
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__ctor:
Leh_func_begin11:
	bx	lr
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__cctor:
Leh_func_begin12:
	push	{r4, r7, lr}
Ltmp23:
	add	r7, sp, #4
Ltmp24:
Ltmp25:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC12_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC12_0+8))
LPC12_0:
	add	r4, pc, r4
	ldr	r0, [r4, #96]
	bl	_p_3_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #100]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__ctor:
Leh_func_begin13:
	bx	lr
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ctor:
Leh_func_begin14:
	bx	lr
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ResourceExistsb__3_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ResourceExistsb__3_System_IO_Stream:
Leh_func_begin15:
	cmp	r1, #0
	movne	r1, #1
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__ctor:
Leh_func_begin16:
	sub	sp, sp, #4
Ltmp26:
	str	r0, [sp]
	add	sp, sp, #4
	bx	lr
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__Loadb__0_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__Loadb__0_System_IO_Stream:
Leh_func_begin17:
	push	{r4, r7, lr}
Ltmp27:
	add	r7, sp, #4
Ltmp28:
Ltmp29:
	sub	sp, sp, #8
	str	r0, [sp, #4]
	str	r0, [sp]
	cmp	r1, #0
	beq	LBB17_2
	ldr	r4, [sp, #4]
	ldr	r0, [r4, #12]
	ldr	r2, [r0]
	ldr	r2, [r2, #60]
	blx	r2
	str	r0, [r4, #8]
LBB17_2:
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin18:
	push	{r4, r5, r7, lr}
Ltmp30:
	add	r7, sp, #8
Ltmp31:
Ltmp32:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC18_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC18_0+8))
LPC18_0:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB18_2
	bl	_p_20_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB18_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB18_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB18_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB18_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB18_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string:
Leh_func_begin19:
	push	{r4, r5, r6, r7, lr}
Ltmp33:
	add	r7, sp, #12
Ltmp34:
	push	{r10, r11}
Ltmp35:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC19_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got-(LPC19_0+8))
LPC19_0:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB19_2
	bl	_p_20_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB19_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB19_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB19_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB19_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB19_7
LBB19_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB19_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end19:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_type_info_2,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got,272,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_Load_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ResourceExistsb__3_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__Loadb__0_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	20
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	6
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	7
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	8
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	9
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	10
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	11
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	12
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	14
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	15
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	16
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	17
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	18
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	19
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	21
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	22
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	23
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	24
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	37
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	42
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
Lset20 = Leh_func_end19-Leh_func_begin19
	.long	Lset20
Lset21 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset21
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	4

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	4

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,48,208,77,226,13,176,160,225,32,0,139,229,36,16,139,229,0,0,160,227
	.byte 4,0,139,229,0,0,160,227,12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 4
	.byte 0,0,159,231
bl _p_1

	.byte 0,0,139,229,0,16,160,227,8,16,128,229,36,16,155,229,44,16,139,229,0,16,160,225,40,16,139,229,0,0,80,227
	.byte 67,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 8
	.byte 0,0,159,231
bl _p_1

	.byte 0,32,160,225,40,0,155,229,44,16,155,229,16,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 12
	.byte 0,0,159,231,20,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 16
	.byte 0,0,159,231,28,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 20
	.byte 0,0,159,231,12,0,130,229,32,0,155,229,0,48,160,225,0,48,147,229,15,224,160,225,64,240,147,229,0,0,155,229
	.byte 8,0,144,229,8,0,139,229,29,0,0,234,16,0,155,229,16,0,155,229,4,0,139,229,40,0,139,229,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . -12
	.byte 0,0,159,231,1,16,160,227
bl _p_23

	.byte 44,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 40
	.byte 0,0,159,231,1,16,160,227
bl _p_4

	.byte 12,0,139,229,0,48,160,225,36,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 40,0,155,229,44,16,155,229,12,32,155,229
bl _p_22
bl _p_21

	.byte 8,0,155,229,255,255,255,234,48,208,139,226,0,9,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_24

	.byte 6,2,0,2

Lme_3:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,48,208,77,226,13,176,160,225,28,0,139,229,32,16,139,229,0,0,160,227
	.byte 4,0,203,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 24
	.byte 0,0,159,231
bl _p_3

	.byte 0,0,139,229,0,16,160,227,8,16,192,229,0,16,160,225,40,16,139,229,0,0,80,227,47,0,0,11,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 8
	.byte 0,0,159,231
bl _p_1

	.byte 0,32,160,225,40,0,155,229,16,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 28
	.byte 0,0,159,231,20,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 32
	.byte 0,0,159,231,28,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 20
	.byte 0,0,159,231,12,0,130,229,28,0,155,229,32,16,155,229,28,48,155,229,0,48,147,229,15,224,160,225,64,240,147,229
	.byte 0,0,155,229,8,0,208,229,4,0,203,229,9,0,0,234,8,0,155,229,0,0,160,227,4,0,203,229
bl _p_25

	.byte 24,0,139,229,0,0,80,227,1,0,0,10,24,0,155,229
bl _p_21

	.byte 255,255,255,234,4,0,219,229,255,255,255,234,48,208,139,226,0,9,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_24

	.byte 6,2,0,2

Lme_5:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,16,0,139,229,20,16,139,229,0,0,160,227
	.byte 0,0,139,229,20,0,155,229,0,0,80,227,35,0,0,10,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 92
	.byte 0,0,159,231
bl _p_1

	.byte 24,0,139,229,20,16,155,229
bl _p_19

	.byte 24,0,155,229,0,0,139,229,0,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,52,240,145,229,0,16,160,225
	.byte 16,0,155,229,8,16,128,229,0,0,0,235,15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10
	.byte 0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 100
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,32,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_14:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string:

	.byte 128,64,45,233,13,112,160,225,64,9,45,233,20,208,77,226,13,176,160,225,4,0,139,229,8,16,139,229,4,0,155,229
	.byte 0,0,144,229
bl _p_26

	.byte 0,96,160,225,0,0,150,229,0,0,160,227,0,0,139,229,4,0,155,229,4,16,150,229,1,0,128,224,8,16,155,229
	.byte 0,16,128,229,20,208,139,226,64,9,189,232,128,128,189,232

Lme_1d:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string:

	.byte 128,64,45,233,13,112,160,225,16,9,45,233,36,208,77,226,13,176,160,225,4,0,139,229,8,16,139,229,12,32,139,229
	.byte 16,48,139,229,4,0,155,229,0,0,144,229
bl _p_27

	.byte 0,64,160,225,0,0,148,229,0,0,160,227,0,0,139,229,4,0,155,229,4,16,148,229,1,0,128,224,0,0,144,229
	.byte 24,0,139,229,8,0,155,229,12,16,155,229
bl _p_6

	.byte 0,16,160,225,24,48,155,229,3,0,160,225,16,32,155,229,0,224,211,229
bl _p_7

	.byte 36,208,139,226,16,9,189,232,128,128,189,232

Lme_1e:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string:

	.byte 128,64,45,233,13,112,160,225,64,13,45,233,64,208,77,226,13,176,160,225,0,16,139,229,4,0,139,229,8,32,139,229
	.byte 12,48,139,229,88,224,157,229,16,224,139,229,4,0,155,229,0,0,144,229
bl _p_33

	.byte 0,160,160,225,0,0,154,229,7,96,128,226,7,96,198,227,6,208,77,224,0,96,141,226,4,0,155,229,0,0,144,229
bl _p_32
bl _p_9

	.byte 56,0,139,229
bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor

	.byte 56,0,155,229,52,0,139,229,4,16,155,229,4,32,154,229,2,0,128,224,0,16,128,229,4,0,155,229,8,16,155,229
	.byte 12,32,155,229,16,48,155,229
bl _p_30

	.byte 28,0,139,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 48
	.byte 8,128,159,231
bl _p_12

	.byte 0,16,160,225,52,0,155,229,36,16,139,229,0,0,80,227,69,0,0,11,8,16,154,229,48,0,139,229,1,0,128,224
	.byte 12,16,154,229,16,32,154,229,50,255,47,225,48,0,155,229,44,0,139,229,0,0,80,227,55,0,0,11,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 8
	.byte 0,0,159,231
bl _p_1

	.byte 44,16,155,229,24,16,139,229,16,16,128,229,40,0,139,229,4,0,155,229,0,0,144,229
bl _p_29

	.byte 0,16,160,225,40,0,155,229,20,16,128,229,32,0,139,229,4,0,155,229,0,0,144,229
bl _p_28

	.byte 28,16,155,229,32,32,155,229,36,48,155,229,28,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 20
	.byte 0,0,159,231,12,0,130,229,3,0,160,225,0,48,147,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 52
	.byte 8,128,159,231,4,224,143,226,36,240,19,229,0,0,0,0,24,0,155,229,8,16,154,229,1,16,128,224,24,32,154,229
	.byte 6,0,160,225,2,0,128,224,12,32,154,229,20,48,154,229,51,255,47,225,24,16,154,229,6,0,160,225,1,16,128,224
	.byte 0,0,155,229,12,32,154,229,20,48,154,229,51,255,47,225,64,208,139,226,64,13,189,232,128,128,189,232,14,16,160,225
	.byte 0,0,159,229
bl _p_24

	.byte 6,2,0,2,14,16,160,225,0,0,159,229
bl _p_24

	.byte 118,2,0,2

Lme_1f:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string:

	.byte 128,64,45,233,13,112,160,225,112,9,45,233,44,208,77,226,13,176,160,225,12,0,139,229,16,16,139,229,20,32,139,229
	.byte 24,48,139,229,12,0,155,229,0,0,144,229
bl _p_34

	.byte 0,64,160,225,0,0,148,229,0,0,160,227,8,0,139,229,0,0,160,227,0,0,139,229,12,0,155,229,4,16,148,229
	.byte 1,0,128,224,0,0,144,229,32,0,139,229,16,0,155,229,20,16,155,229
bl _p_6

	.byte 0,16,160,225,32,48,155,229,3,0,160,225,11,32,160,225,0,224,211,229
bl _p_15

	.byte 255,0,0,226,0,0,80,227,37,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 60
	.byte 0,0,159,231,32,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 40
	.byte 0,0,159,231,3,16,160,227
bl _p_4

	.byte 0,96,160,225,6,48,160,225,12,0,155,229,8,16,148,229,1,0,128,224,0,32,144,229,3,0,160,225,0,16,160,227
	.byte 0,48,147,229,15,224,160,225,128,240,147,229,6,0,160,225,1,16,160,227,16,32,155,229,0,48,150,229,15,224,160,225
	.byte 128,240,147,229,6,0,160,225,2,16,160,227,20,32,155,229,0,48,150,229,15,224,160,225,128,240,147,229,32,0,155,229
	.byte 6,16,160,225
bl _p_5

	.byte 0,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 56
	.byte 0,0,159,231,32,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 40
	.byte 0,0,159,231,2,16,160,227
bl _p_4

	.byte 0,80,160,225,5,48,160,225,0,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 5,0,160,225,1,16,160,227,24,32,155,229,0,48,149,229,15,224,160,225,128,240,147,229,32,0,155,229,5,16,160,225
bl _p_5

	.byte 4,0,139,229,44,208,139,226,112,9,189,232,128,128,189,232

Lme_21:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,20,208,77,226,13,176,160,225,4,0,139,229,4,0,155,229,0,0,144,229
bl _p_35

	.byte 0,160,160,225,0,0,154,229,0,0,160,227,0,0,139,229,4,0,155,229,8,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 64
	.byte 0,0,159,231
bl _p_1

	.byte 12,0,139,229
bl _p_16

	.byte 8,0,155,229,12,16,155,229,4,32,154,229,2,0,128,224,0,16,128,229,4,0,155,229,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 68
	.byte 1,16,159,231,8,32,154,229,2,0,128,224,0,16,128,229,4,0,155,229
bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor

	.byte 20,208,139,226,0,13,189,232,128,128,189,232

Lme_22:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,16,208,77,226,13,176,160,225,8,0,139,229,8,0,155,229,0,0,144,229
bl _p_36

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,4,0,139,229,16,208,139,226,0,9,189,232,128,128,189,232

Lme_23:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream
_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream:

	.byte 128,64,45,233,13,112,160,225,96,9,45,233,16,208,77,226,13,176,160,225,0,0,139,229,4,16,139,229,0,0,155,229
	.byte 0,0,144,229
bl _p_38

	.byte 0,96,160,225,0,0,150,229,7,80,128,226,7,80,197,227,5,208,77,224,0,80,141,226,4,0,155,229,0,0,80,227
	.byte 23,0,0,10,0,0,155,229,8,0,139,229,0,0,155,229,4,16,150,229,1,0,128,224,0,0,144,229,12,0,139,229
	.byte 0,0,155,229,0,0,144,229
bl _p_37

	.byte 0,48,160,225,12,0,155,229,20,16,150,229,1,16,133,224,4,32,155,229,51,255,47,225,8,0,155,229,8,16,150,229
	.byte 1,0,128,224,20,16,150,229,1,16,133,224,12,32,150,229,16,48,150,229,51,255,47,225,16,208,139,226,96,9,189,232
	.byte 128,128,189,232

Lme_24:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_Load_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ResourceExistsb__3_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__Loadb__0_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_SetRootLocation_string_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_Load_string_string_string
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader__cctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass4__ResourceExistsb__3_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__Loadb__0_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 43,10,5,2
	.short 0, 14, 28, 42, 60
	.byte 0,0,0,1,255,255,255,255,255,9,7,2,4,4,28,2,2,255,255,255,255,224,38,6,4,6,3,5,64,4,2,2
	.byte 2,255,255,255,255,182,0,0,0,76,78,2,255,255,255,255,176,86,6,4,2,2,255,255,255,255,156,0,0,0,103
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,133,30,0,0,0,0,117
	.long 29,0,0,0,0,0,0,0
	.long 220,36,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,181,34,0,0,0,0
	.long 0,0,0,165,33,19,0,0
	.long 0,203,35,0,0,0,0,149
	.long 31,0,237,37,20,278,42,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 17,26,0,27,0,28,0,29
	.long 117,30,133,31,149,32,0,33
	.long 165,34,181,35,203,36,220,37
	.long 237,38,0,39,0,40,0,41
	.long 0,42,278
.section __TEXT, __const
	.align 3
class_name_table:

	.short 19, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 2, 0, 1, 19, 0
	.short 0, 0, 0, 0, 0, 6, 0, 0
	.short 0, 9, 0, 3, 21, 0, 0, 10
	.short 0, 8, 0, 0, 0, 5, 20, 4
	.short 0, 7, 0, 11, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 29,10,3,2
	.short 0, 11, 22
	.byte 129,32,2,1,1,1,3,6,2,2,6,129,59,2,2,3,7,3,19,5,3,3,129,112,4,12,12,12,3,4,5,1
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 43,10,5,2
	.short 0, 16, 32, 48, 68
	.byte 0,0,0,133,156,255,255,255,250,100,133,172,16,3,3,3,133,200,23,23,255,255,255,250,10,134,13,23,23,3,3,3
	.byte 134,71,12,3,10,23,255,255,255,249,137,0,0,0,134,142,134,170,28,255,255,255,249,58,134,226,29,29,30,30,255,255
	.byte 255,248,168,0,0,0,135,91
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,64,68,13,11,23,12,13,0,72,14,8,135
	.byte 2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,134,5,136,4
	.byte 139,3,142,1,68,14,40,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,132,5,136,4,139,3,142,1,68,14
	.byte 56,68,13,11,27,12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.byte 29,12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,72,68,13,11,25,12
	.byte 13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,40,68,13,11,23,12,13,0,72,14,8,135
	.byte 2,68,14,16,136,4,139,3,142,1,68,14,32,68,13,11,27,12,13,0,72,14,8,135,2,68,14,24,133,6,134,5
	.byte 136,4,139,3,142,1,68,14,40,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 11,10,2,2
	.short 0, 11
	.byte 135,94,7,7,5,5,28,23,5,25,23,135,245

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_plt:
_p_1_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 116,426
_p_2_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 120,449
_p_3_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 124,494
_p_4_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 128,520
_p_5_plt_string_Format_string_object___llvm:
	.no_dead_strip plt_string_Format_string_object__
plt_string_Format_string_object__:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 132,546
_p_6_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 136,551
_p_7_plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string
plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 140,553
_p_8_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 144,610
_p_9_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 148,618
_p_10_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1_TResource__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1_TResource__ctor
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1_TResource__ctor:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 152,645
_p_11_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1_GetStreamLocation_string_string_string:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 156,664
_p_12_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 160,666
_p_13_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 164,685
_p_14_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 168,708
_p_15_plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string__llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string_
plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string_:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 172,731
_p_16_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string__ctor
plt_System_Collections_Generic_Dictionary_2_string_string__ctor:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 176,757

.set _p_17_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor_llvm, _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider__ctor
_p_18_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 184,785
_p_19_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamReader__ctor_System_IO_Stream
plt_System_IO_StreamReader__ctor_System_IO_Stream:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 188,797
_p_20_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 192,802
_p_21_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 196,840
_p_22_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 200,868
_p_23_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 204,873
_p_24_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 208,893
_p_25_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 212,928
_p_26_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 216,983
_p_27_plt__rgctx_fetch_4_llvm:
	.no_dead_strip plt__rgctx_fetch_4
plt__rgctx_fetch_4:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 220,1027
_p_28_plt__rgctx_fetch_5_llvm:
	.no_dead_strip plt__rgctx_fetch_5
plt__rgctx_fetch_5:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 224,1071
_p_29_plt__rgctx_fetch_6_llvm:
	.no_dead_strip plt__rgctx_fetch_6
plt__rgctx_fetch_6:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 228,1093
_p_30_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 232,1115

.set _p_31_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor_llvm, _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor
_p_32_plt__rgctx_fetch_7_llvm:
	.no_dead_strip plt__rgctx_fetch_7
plt__rgctx_fetch_7:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 240,1150
_p_33_plt__rgctx_fetch_8_llvm:
	.no_dead_strip plt__rgctx_fetch_8
plt__rgctx_fetch_8:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 244,1158
_p_34_plt__rgctx_fetch_9_llvm:
	.no_dead_strip plt__rgctx_fetch_9
plt__rgctx_fetch_9:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 248,1220
_p_35_plt__rgctx_fetch_10_llvm:
	.no_dead_strip plt__rgctx_fetch_10
plt__rgctx_fetch_10:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 252,1268
_p_36_plt__rgctx_fetch_11_llvm:
	.no_dead_strip plt__rgctx_fetch_11
plt__rgctx_fetch_11:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 256,1317
_p_37_plt__rgctx_fetch_12_llvm:
	.no_dead_strip plt__rgctx_fetch_12
plt__rgctx_fetch_12:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 260,1359
_p_38_plt__rgctx_fetch_13_llvm:
	.no_dead_strip plt__rgctx_fetch_13
plt__rgctx_fetch_13:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got - . + 264,1392
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader"
	.asciz "D6FEFB8F-01C7-4230-ACE4-ABE563457664"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "D6FEFB8F-01C7-4230-ACE4-ABE563457664"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader__Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 29,272,39,43,11,387000831,0,2042
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_ResourceLoader_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_ResourceLoader_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,6,4,5,6,7,8,13,0,5,9,5,10,11,8,0,0,0,2,13,12,0,2,13,14,0,0,0,0,0,0
	.byte 0,4,8,5,15,16,0,4,13,18,13,17,0,2,20,19,1,8,3,23,22,21,1,8,0,1,8,2,25,24,0,0
	.byte 0,2,26,28,0,0,0,0,0,0,0,0,0,0,0,0,0,4,15,5,8,16,0,4,18,13,17,13,0,2,19,20
	.byte 0,0,0,0,0,1,27,0,1,27,5,19,0,0,1,4,1,7,1,7,106,255,253,0,0,0,7,111,0,198,0,0
	.byte 11,1,7,106,0,255,253,0,0,0,7,111,0,198,0,0,12,1,7,106,0,255,253,0,0,0,7,111,0,198,0,0
	.byte 13,1,7,106,0,255,253,0,0,0,7,111,0,198,0,0,15,1,7,106,0,255,253,0,0,0,7,111,0,198,0,0
	.byte 16,1,7,106,0,4,1,11,1,7,106,255,253,0,0,0,7,128,197,0,198,0,0,24,1,7,106,0,255,253,0,0
	.byte 0,7,128,197,0,198,0,0,25,1,7,106,0,255,252,0,0,0,1,1,3,219,0,0,3,4,2,119,1,2,2,130
	.byte 146,1,2,130,146,1,4,2,97,1,3,2,130,146,1,2,130,146,1,7,128,249,255,252,0,0,0,1,1,7,129,6
	.byte 12,0,39,42,47,14,1,9,14,3,219,0,0,3,6,21,50,21,30,3,219,0,0,3,14,1,10,6,23,50,23,17
	.byte 0,51,14,6,1,2,130,123,1,17,0,67,34,255,253,0,0,0,2,62,2,2,198,0,0,244,0,1,2,70,2,6
	.byte 194,0,1,60,17,0,115,17,0,91,14,3,219,0,0,5,17,0,128,131,34,255,254,0,0,0,0,255,43,0,0,2
	.byte 34,255,254,0,0,0,0,255,43,0,0,3,6,255,254,0,0,0,0,255,43,0,0,3,14,1,8,16,1,8,3,14
	.byte 2,128,228,1,33,6,193,0,17,58,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116
	.byte 0,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97
	.byte 98,115,95,116,114,97,109,112,111,108,105,110,101,0,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95
	.byte 112,116,114,102,114,101,101,0,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105
	.byte 99,0,3,193,0,19,128,3,8,3,255,253,0,0,0,3,219,0,0,5,1,198,0,2,243,2,2,130,146,1,2,130
	.byte 146,1,0,5,19,0,1,0,1,7,255,253,0,0,0,1,7,0,198,0,0,13,1,7,130,67,0,4,1,11,1,7
	.byte 130,67,35,130,74,150,4,7,130,91,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99
	.byte 105,102,105,99,0,3,255,253,0,0,0,7,130,91,0,198,0,0,24,1,7,130,67,0,3,15,3,255,253,0,0,0
	.byte 2,62,2,2,198,0,0,244,0,1,2,70,2,35,130,74,140,10,255,253,0,0,0,7,130,91,0,198,0,0,25,1
	.byte 7,130,67,0,35,130,74,140,22,255,253,0,0,0,7,130,91,0,198,0,0,25,1,7,130,67,0,3,255,253,0,0
	.byte 0,3,219,0,0,5,1,198,0,3,12,2,2,130,146,1,2,130,146,1,0,3,255,253,0,0,0,3,219,0,0,5
	.byte 1,198,0,2,244,2,2,130,146,1,2,130,146,1,0,3,10,3,255,254,0,0,0,0,255,43,0,0,2,3,193,0
	.byte 7,97,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101
	.byte 99,107,112,111,105,110,116,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116
	.byte 105,111,110,0,3,194,0,0,83,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,32,109
	.byte 111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,0
	.byte 7,36,109,111,110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108,101,95,101,120,99
	.byte 101,112,116,105,111,110,0,255,253,0,0,0,7,111,0,198,0,0,11,1,7,106,0,35,131,199,192,0,92,40,255,253
	.byte 0,0,0,7,111,0,198,0,0,11,1,7,106,0,1,15,7,111,2,255,253,0,0,0,7,111,0,198,0,0,12,1
	.byte 7,106,0,35,131,243,192,0,92,40,255,253,0,0,0,7,111,0,198,0,0,12,1,7,106,0,1,15,7,111,1,255
	.byte 253,0,0,0,7,111,0,198,0,0,13,1,7,106,0,35,132,31,140,22,255,253,0,0,0,7,128,197,0,198,0,0
	.byte 25,1,7,106,0,35,132,31,140,10,255,253,0,0,0,7,128,197,0,198,0,0,25,1,7,106,0,3,255,253,0,0
	.byte 0,7,111,0,198,0,0,15,1,7,106,0,3,255,253,0,0,0,7,128,197,0,198,0,0,24,1,7,106,0,35,132
	.byte 31,150,4,7,128,197,35,132,31,192,0,92,40,255,253,0,0,0,7,111,0,198,0,0,13,1,7,106,0,6,15,7
	.byte 128,197,7,15,7,128,197,6,14,7,106,23,7,106,22,7,106,21,7,106,255,253,0,0,0,7,111,0,198,0,0,15
	.byte 1,7,106,0,35,132,180,192,0,92,40,255,253,0,0,0,7,111,0,198,0,0,15,1,7,106,0,2,15,7,111,1
	.byte 15,7,111,2,255,253,0,0,0,7,111,0,198,0,0,16,1,7,106,0,35,132,228,192,0,92,40,255,253,0,0,0
	.byte 7,111,0,198,0,0,16,1,7,106,0,2,15,7,111,1,15,7,111,2,255,253,0,0,0,7,128,197,0,198,0,0
	.byte 24,1,7,106,0,35,133,20,192,0,92,40,255,253,0,0,0,7,128,197,0,198,0,0,24,1,7,106,0,0,255,253
	.byte 0,0,0,7,128,197,0,198,0,0,25,1,7,106,0,35,133,62,192,0,90,34,32,1,19,7,106,18,2,128,226,1
	.byte 255,253,0,0,0,7,111,0,198,0,0,14,1,7,106,0,35,133,62,192,0,92,40,255,253,0,0,0,7,128,197,0
	.byte 198,0,0,25,1,7,106,0,5,15,7,128,197,7,15,7,128,197,6,14,7,106,22,7,106,21,7,106,6,0,1,0
	.byte 16,4,2,130,64,1,44,128,232,128,232,0,6,0,1,0,8,4,2,130,64,1,36,128,216,128,216,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,17,0,0,17,255,253,0,0,0,1,7,0,198,0,0,11,1,7,130,67,0,0,0,17
	.byte 0,0,17,255,253,0,0,0,1,7,0,198,0,0,12,1,7,130,67,0,0,0,17,0,0,17,255,253,0,0,0,1
	.byte 7,0,198,0,0,13,1,7,130,67,0,0,0,17,0,0,17,255,253,0,0,0,1,7,0,198,0,0,15,1,7,130
	.byte 67,0,0,0,17,0,0,17,255,253,0,0,0,1,7,0,198,0,0,16,1,7,130,67,0,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,6,24,1,2,0,128,192,88,124,128,128,0,16,0,0,16,0,0,5,19,0,1,0,1,11
	.byte 17,0,0,17,255,253,0,0,0,1,11,0,198,0,0,24,1,7,134,89,0,0,0,17,0,0,17,255,253,0,0,0
	.byte 1,11,0,198,0,0,25,1,7,134,89,0,0,0,3,48,0,1,11,4,16,255,253,0,0,0,7,111,0,198,0,0
	.byte 11,1,7,106,0,1,1,1,0,0,3,74,0,1,11,4,16,255,253,0,0,0,7,111,0,198,0,0,12,1,7,106
	.byte 0,1,1,1,0,0,3,100,0,1,11,4,16,255,253,0,0,0,7,111,0,198,0,0,13,1,7,106,0,1,1,1
	.byte 0,0,3,128,128,0,1,11,12,16,255,253,0,0,0,7,111,0,198,0,0,15,1,7,106,0,1,1,1,0,0,3
	.byte 128,158,0,1,11,4,16,255,253,0,0,0,7,111,0,198,0,0,16,1,7,106,0,1,1,1,0,0,3,128,184,0
	.byte 1,11,8,17,255,253,0,0,0,7,128,197,0,198,0,0,24,1,7,106,0,1,1,1,0,0,3,128,208,0,1,11
	.byte 0,17,255,253,0,0,0,7,128,197,0,198,0,0,25,1,7,106,0,1,1,1,0,0,16,0,0,16,0,0,0,128
	.byte 144,8,0,0,1,0,128,144,8,0,0,1,255,255,255,255,255,255,255,255,255,255,9,128,152,8,0,0,1,193,0,18
	.byte 178,193,0,18,175,193,0,18,174,193,0,18,172,6,4,5,6,0,4,128,144,8,0,0,1,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,255,255,255,255,255,5,128,196,19,8,4,0,1,193,0,18,178,193,0,18,175,193,0
	.byte 18,174,193,0,18,172,17,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128
	.byte 128,9,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,255,255,255,255,255,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader"

LDIFF_SYM7=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_2:

	.byte 5
	.asciz "_<>c__DisplayClass1"

	.byte 12,16
LDIFF_SYM10=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "text"

LDIFF_SYM11=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClass1"

LDIFF_SYM12=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_5:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM15=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM16=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM16
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM17=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM18=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_4:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM19=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM20=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM21=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_6:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM24=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM25=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM26=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_3:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM27=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM28=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM29=LTDIE_3_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM30=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM31=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM32=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM33=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM34=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM35=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM36=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM37=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM38=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM39=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM40=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM41=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceLoader:GetTextResource"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0
	.long Lme_3

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM44=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,123,32,3
	.asciz "resourcePath"

LDIFF_SYM45=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,123,36,11
	.asciz "V_0"

LDIFF_SYM46=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM47=LTDIE_3_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,123,4,11
	.asciz "V_2"

LDIFF_SYM48=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,123,8,11
	.asciz "V_3"

LDIFF_SYM49=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM50=Lfde0_end - Lfde0_start
	.long LDIFF_SYM50
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0

LDIFF_SYM51=Lme_3 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_GetTextResource_string_0
	.long LDIFF_SYM51
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_8:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM52=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM53=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM54=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM55=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM56=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_7:

	.byte 5
	.asciz "_<>c__DisplayClass4"

	.byte 9,16
LDIFF_SYM57=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,0,6
	.asciz "found"

LDIFF_SYM58=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClass4"

LDIFF_SYM59=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM60=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM61=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceLoader:ResourceExists"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0
	.long Lme_5

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM62=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,123,28,3
	.asciz "resourcePath"

LDIFF_SYM63=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM63
	.byte 2,123,32,11
	.asciz "V_0"

LDIFF_SYM64=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM65=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM66=Lfde1_end - Lfde1_start
	.long LDIFF_SYM66
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0

LDIFF_SYM67=Lme_5 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader_ResourceExists_string_0
	.long LDIFF_SYM67
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_15:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM68=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM69=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM70=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM71=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_14:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM72=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM73=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM74=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM75=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_13:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM76=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM77=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM78=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM79=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_17:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM80=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM81=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM82=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM83=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM83
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM84=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM84
LTDIE_16:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM85=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM86=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM87=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM88=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM89=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM90=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_12:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM91=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM92=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM93=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM94=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM95=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM96=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM97=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM98=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM99=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM100=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM100
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM101=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM102=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM103=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM103
LTDIE_11:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM104=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM105=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM106=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM107=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM108=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM109=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_10:

	.byte 5
	.asciz "System_Func`4"

	.byte 52,16
LDIFF_SYM110=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,0,0,7
	.asciz "System_Func`4"

LDIFF_SYM111=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM111
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM112=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM113=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_18:

	.byte 5
	.asciz "System_Action`3"

	.byte 52,16
LDIFF_SYM114=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 2,35,0,0,7
	.asciz "System_Action`3"

LDIFF_SYM115=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM115
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM116=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM117=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_22:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM118=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM119=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM120=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM121=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM122=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_26:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM123=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM124=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM125=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM126=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_25:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM127=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM128=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM128
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM129=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM130=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM131=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM132=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM133=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM134=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_24:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM135=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM136=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM137=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM138=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_23:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM139=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM139
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM140=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM141=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM142=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_21:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM143=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM143
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM144=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM144
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM145=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM146=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_20:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM149=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM150=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_19:

	.byte 5
	.asciz "System_Threading_AutoResetEvent"

	.byte 20,16
LDIFF_SYM153=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,0,0,7
	.asciz "System_Threading_AutoResetEvent"

LDIFF_SYM154=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM155=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM156=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_9:

	.byte 5
	.asciz "System_IO_Stream"

	.byte 20,16
LDIFF_SYM157=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,0,6
	.asciz "async_read"

LDIFF_SYM158=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM158
	.byte 2,35,8,6
	.asciz "async_write"

LDIFF_SYM159=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,12,6
	.asciz "async_event"

LDIFF_SYM160=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,16,0,7
	.asciz "System_IO_Stream"

LDIFF_SYM161=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM162=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM163=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_28:

	.byte 5
	.asciz "System_IO_TextReader"

	.byte 8,16
LDIFF_SYM164=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,0,0,7
	.asciz "System_IO_TextReader"

LDIFF_SYM165=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM165
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM166=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM166
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM167=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_30:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM168=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM169=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM170=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM170
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM171=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM171
LTDIE_31:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM172=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM173=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM174=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM175=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_29:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM176=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM177=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM178=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM179=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM180=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM181=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM182=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM183=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM184=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM185=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM186=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM187=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM188=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM189=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM190=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM190
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM191=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM192=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM192
LTDIE_33:

	.byte 5
	.asciz "System_Text_DecoderFallbackBuffer"

	.byte 8,16
LDIFF_SYM193=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallbackBuffer"

LDIFF_SYM194=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM195=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM196=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM196
LTDIE_32:

	.byte 5
	.asciz "System_Text_Decoder"

	.byte 16,16
LDIFF_SYM197=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,35,0,6
	.asciz "fallback"

LDIFF_SYM198=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM198
	.byte 2,35,8,6
	.asciz "fallback_buffer"

LDIFF_SYM199=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM199
	.byte 2,35,12,0,7
	.asciz "System_Text_Decoder"

LDIFF_SYM200=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM200
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM201=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM201
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM202=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_34:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM203=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM204=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM205=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM206=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM207=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM208=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM209=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM210=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_35:

	.byte 17
	.asciz "System_Threading_Tasks_IDecoupledTask"

	.byte 8,7
	.asciz "System_Threading_Tasks_IDecoupledTask"

LDIFF_SYM211=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM212=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM212
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM213=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_27:

	.byte 5
	.asciz "System_IO_StreamReader"

	.byte 56,16
LDIFF_SYM214=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,0,6
	.asciz "input_buffer"

LDIFF_SYM215=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,35,8,6
	.asciz "decoded_buffer"

LDIFF_SYM216=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,35,12,6
	.asciz "encoding"

LDIFF_SYM217=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM217
	.byte 2,35,16,6
	.asciz "decoder"

LDIFF_SYM218=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM218
	.byte 2,35,20,6
	.asciz "line_builder"

LDIFF_SYM219=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM219
	.byte 2,35,24,6
	.asciz "base_stream"

LDIFF_SYM220=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,28,6
	.asciz "decoded_count"

LDIFF_SYM221=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,36,6
	.asciz "pos"

LDIFF_SYM222=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,40,6
	.asciz "buffer_size"

LDIFF_SYM223=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM223
	.byte 2,35,44,6
	.asciz "do_checks"

LDIFF_SYM224=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM224
	.byte 2,35,48,6
	.asciz "mayBlock"

LDIFF_SYM225=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 2,35,52,6
	.asciz "async_task"

LDIFF_SYM226=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,32,6
	.asciz "leave_open"

LDIFF_SYM227=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,53,6
	.asciz "foundCR"

LDIFF_SYM228=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,54,0,7
	.asciz "System_IO_StreamReader"

LDIFF_SYM229=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM229
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM230=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM230
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM231=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceLoader/<>c__DisplayClass1:<GetTextResource>b__0"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0
	.long Lme_14

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM232=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,123,16,3
	.asciz "stream"

LDIFF_SYM233=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,123,20,11
	.asciz "V_0"

LDIFF_SYM234=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM235=Lfde2_end - Lfde2_start
	.long LDIFF_SYM235
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0

LDIFF_SYM236=Lme_14 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceLoader__c__DisplayClass1__GetTextResourceb__0_System_IO_Stream_0
	.long LDIFF_SYM236
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_37:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider"

	.byte 8,16
LDIFF_SYM237=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider"

LDIFF_SYM238=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM238
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM239=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM240=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_39:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM241=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM242=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM243=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM243
LTDIE_38:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM244=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM245=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM246=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM246
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM247=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM248=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM249=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM250=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM251=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM252=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM253=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM253
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM254=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM254
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM255=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM255
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM256=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM256
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM257=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM257
LTDIE_36:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader`1"

	.byte 16,16
LDIFF_SYM258=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,35,0,6
	.asciz "_rootLocations"

LDIFF_SYM259=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,35,8,6
	.asciz "_generalRootLocation"

LDIFF_SYM260=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM260
	.byte 2,35,12,0,7
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader`1"

LDIFF_SYM261=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM261
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM262=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM263=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM263
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1<!0>:SetRootLocation"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string
	.long Lme_1d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM264=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM264
	.byte 2,123,4,3
	.asciz "location"

LDIFF_SYM265=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM266=Lfde3_end - Lfde3_start
	.long LDIFF_SYM266
Lfde3_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string

LDIFF_SYM267=Lme_1d - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string
	.long LDIFF_SYM267
	.byte 12,13,0,72,14,8,135,2,68,14,20,134,5,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1<!0>:SetRootLocation"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string
	.long Lme_1e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM268=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM268
	.byte 2,123,4,3
	.asciz "namespaceKey"

LDIFF_SYM269=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,123,8,3
	.asciz "typeKey"

LDIFF_SYM270=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,123,12,3
	.asciz "location"

LDIFF_SYM271=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM272=Lfde4_end - Lfde4_start
	.long LDIFF_SYM272
Lfde4_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string

LDIFF_SYM273=Lme_1e - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_SetRootLocation_string_string_string
	.long LDIFF_SYM273
	.byte 12,13,0,72,14,8,135,2,68,14,20,132,5,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_40:

	.byte 17
	.asciz "Cirrious_CrossCore_Platform_IMvxResourceLoader"

	.byte 8,7
	.asciz "Cirrious_CrossCore_Platform_IMvxResourceLoader"

LDIFF_SYM274=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM275=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM276=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM276
LTDIE_41:

	.byte 5
	.asciz "_<>c__DisplayClass1"

	.byte 16,16
LDIFF_SYM277=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,0,6
	.asciz "resource"

LDIFF_SYM278=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,8,6
	.asciz "<>4__this"

LDIFF_SYM279=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM279
	.byte 2,35,12,0,7
	.asciz "_<>c__DisplayClass1"

LDIFF_SYM280=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM281=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM281
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM282=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1<!0>:Load"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string
	.long Lme_1f

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM283=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,123,4,3
	.asciz "namespaceKey"

LDIFF_SYM284=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,123,8,3
	.asciz "typeKey"

LDIFF_SYM285=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,123,12,3
	.asciz "entryKey"

LDIFF_SYM286=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,123,16,11
	.asciz "V_0"

LDIFF_SYM287=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM288=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM288
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM289=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM289
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM290=Lfde5_end - Lfde5_start
	.long LDIFF_SYM290
Lfde5_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string

LDIFF_SYM291=Lme_1f - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_Load_string_string_string
	.long LDIFF_SYM291
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1<!0>:GetStreamLocation"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string
	.long Lme_21

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM292=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,123,12,3
	.asciz "namespaceKey"

LDIFF_SYM293=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,123,16,3
	.asciz "typeKey"

LDIFF_SYM294=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,123,20,3
	.asciz "entryKey"

LDIFF_SYM295=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,123,24,11
	.asciz "V_0"

LDIFF_SYM296=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM297=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,123,4,11
	.asciz "V_2"

LDIFF_SYM298=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM298
	.byte 1,86,11
	.asciz "V_3"

LDIFF_SYM299=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM299
	.byte 1,85,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM300=Lfde6_end - Lfde6_start
	.long LDIFF_SYM300
Lfde6_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string

LDIFF_SYM301=Lme_21 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0_GetStreamLocation_string_string_string
	.long LDIFF_SYM301
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1<!0>:.ctor"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor
	.long Lme_22

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM302=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM303=Lfde7_end - Lfde7_start
	.long LDIFF_SYM303
Lfde7_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor

LDIFF_SYM304=Lme_22 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__0__ctor
	.long LDIFF_SYM304
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1/<>c__DisplayClass1<!0>:.ctor"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor
	.long Lme_23

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM305=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM306=Lfde8_end - Lfde8_start
	.long LDIFF_SYM306
Lfde8_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor

LDIFF_SYM307=Lme_23 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__ctor
	.long LDIFF_SYM307
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,32,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.MvxResourceObjectLoader`1/<>c__DisplayClass1<!0>:<Load>b__0"
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream
	.long Lme_24

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM308=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM308
	.byte 2,123,0,3
	.asciz "stream"

LDIFF_SYM309=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM310=Lfde9_end - Lfde9_start
	.long LDIFF_SYM310
Lfde9_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream

LDIFF_SYM311=Lme_24 - _Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceObjectLoader_1__c__DisplayClass1__0__Loadb__0_System_IO_Stream
	.long LDIFF_SYM311
	.byte 12,13,0,72,14,8,135,2,68,14,24,133,6,134,5,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde9_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
