	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor:
Leh_func_begin1:
	bx	lr
Leh_func_end1:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute_get_IsReference
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute_get_IsReference:
Leh_func_begin2:
	ldrb	r0, [r0, #8]
	bx	lr
Leh_func_end2:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute__ctor
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute__ctor:
Leh_func_begin3:
	mov	r1, #1
	mvn	r2, #0
	strb	r1, [r0, #13]
	str	r2, [r0, #16]
	bx	lr
Leh_func_end3:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_IsRequired
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_IsRequired:
Leh_func_begin4:
	ldrb	r0, [r0, #12]
	bx	lr
Leh_func_end4:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Name
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Name:
Leh_func_begin5:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end5:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Order
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Order:
Leh_func_begin6:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end6:

	.private_extern	_System_Runtime_Serialization__System_Runtime_Serialization_EnumMemberAttribute_get_Value
	.align	2
_System_Runtime_Serialization__System_Runtime_Serialization_EnumMemberAttribute_get_Value:
Leh_func_begin7:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end7:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_int:
Leh_func_begin8:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r10}
Ltmp2:
	movw	r10, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC8_0+8))
	mov	r4, r0
	mov	r5, r1
	movt	r10, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC8_0+8))
LPC8_0:
	add	r10, pc, r10
	ldr	r0, [r10, #16]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_2_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_XmlDictionaryString__ctor_int_llvm
	ldr	r0, [r10, #20]
	str	r6, [r4, #8]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_3_plt_System_Collections_Generic_List_1_System_Xml_XmlDictionaryString__ctor_int_llvm
	str	r6, [r4, #12]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end8:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool:
Leh_func_begin9:
	push	{r4, r5, r7, lr}
Ltmp3:
	add	r7, sp, #8
Ltmp4:
Ltmp5:
	mov	r4, r1
	mov	r1, #1
	mov	r5, r0
	bl	_p_4_plt_System_Xml_XmlDictionary__ctor_int_llvm
	strb	r4, [r5, #16]
	pop	{r4, r5, r7, pc}
Leh_func_end9:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionary__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionary__cctor:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp6:
	add	r7, sp, #8
Ltmp7:
Ltmp8:
	movw	r5, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC10_0+8))
	movt	r5, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC10_0+8))
LPC10_0:
	add	r5, pc, r5
	ldr	r0, [r5, #24]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #1
	mov	r4, r0
	bl	_p_5_plt_System_Xml_XmlDictionary__ctor_bool_llvm
	ldr	r0, [r5, #28]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor:
Leh_func_begin11:
	push	{r7, lr}
Ltmp9:
	mov	r7, sp
Ltmp10:
Ltmp11:
	mov	r1, #1
	bl	_p_4_plt_System_Xml_XmlDictionary__ctor_int_llvm
	pop	{r7, pc}
Leh_func_end11:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__cctor:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp12:
	add	r7, sp, #8
Ltmp13:
Ltmp14:
	movw	r5, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC12_0+8))
	movt	r5, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC12_0+8))
LPC12_0:
	add	r5, pc, r5
	ldr	r0, [r5, #32]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_6_plt_System_Xml_XmlDictionary_EmptyDictionary__ctor_llvm
	ldr	r0, [r5, #36]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end12:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_get_Quotas
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_get_Quotas:
Leh_func_begin13:
	push	{r4, r5, r7, lr}
Ltmp15:
	add	r7, sp, #8
Ltmp16:
Ltmp17:
	mov	r4, r0
	ldr	r0, [r4, #24]
	cmp	r0, #0
	bne	LBB13_2
	bl	_p_7_plt__class_init_System_Xml_XmlDictionaryReaderQuotas_llvm
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC13_0+8))
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r0, [r0, #40]
	bl	_p_8_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r5, r0
	bl	_p_9_plt_System_Xml_XmlDictionaryReaderQuotas__ctor_llvm
	str	r5, [r4, #24]
LBB13_2:
	ldr	r0, [r4, #24]
	pop	{r4, r5, r7, pc}
Leh_func_end13:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_TryGetLocalNameAsDictionaryString_System_Xml_XmlDictionaryString_
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_TryGetLocalNameAsDictionaryString_System_Xml_XmlDictionaryString_:
Leh_func_begin14:
	mov	r0, #0
	str	r0, [r1]
	mov	r0, #0
	bx	lr
Leh_func_end14:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString:
Leh_func_begin15:
	push	{r4, r7, lr}
Ltmp18:
	add	r7, sp, #4
Ltmp19:
Ltmp20:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #248]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	bl	_p_10_plt_System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_11_plt_System_Xml_XmlDictionaryReader_ReadContentAsString_int_llvm
	pop	{r4, r7, pc}
Leh_func_end15:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int:
Leh_func_begin16:
	push	{r7, lr}
Ltmp21:
	mov	r7, sp
Ltmp22:
Ltmp23:
	bl	_p_12_plt_System_Xml_XmlReader_ReadContentAsString_llvm
	pop	{r7, pc}
Leh_func_end16:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadElementContentAsString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadElementContentAsString:
Leh_func_begin17:
	push	{r4, r5, r7, lr}
Ltmp24:
	add	r7, sp, #8
Ltmp25:
Ltmp26:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #212]
	mov	r0, r4
	blx	r1
	ldr	r1, [r4]
	tst	r0, #255
	beq	LBB17_2
	ldr	r1, [r1, #96]
	mov	r0, r4
	blx	r1
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC17_1+8))
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC17_1+8))
LPC17_1:
	add	r0, pc, r0
	ldr	r0, [r0, #44]
	ldr	r5, [r0]
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB17_2:
	ldr	r1, [r1, #76]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	cmp	r0, #15
	bne	LBB17_4
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC17_0+8))
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC17_0+8))
LPC17_0:
	add	r0, pc, r0
	ldr	r0, [r0, #44]
	ldr	r5, [r0]
	b	LBB17_5
LBB17_4:
	ldr	r0, [r4]
	ldr	r1, [r0, #60]
	mov	r0, r4
	blx	r1
	mov	r5, r0
LBB17_5:
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end17:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadStartElement_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadStartElement_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString:
Leh_func_begin18:
	push	{r7, lr}
Ltmp27:
	mov	r7, sp
Ltmp28:
Ltmp29:
	cmp	r1, #0
	beq	LBB18_3
	cmp	r2, #0
	beq	LBB18_4
	ldr	r3, [r1]
	ldr	r1, [r1, #12]
	ldr	r3, [r2]
	ldr	r2, [r2, #12]
	ldr	r3, [r0]
	ldr	r3, [r3, #72]
	blx	r3
	pop	{r7, pc}
LBB18_3:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC18_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC18_0+8))
LPC18_0:
	ldr	r0, [pc, r0]
	b	LBB18_5
LBB18_4:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC18_1+8))
	mov	r1, #21
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC18_1+8))
LPC18_1:
	ldr	r0, [pc, r0]
LBB18_5:
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_14_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_15_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end18:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString:
Leh_func_begin19:
	push	{r4, r7, lr}
Ltmp30:
	add	r7, sp, #4
Ltmp31:
Ltmp32:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #248]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	bl	_p_10_plt_System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_16_plt_System_Xml_XmlDictionaryReader_ReadString_int_llvm
	pop	{r4, r7, pc}
Leh_func_end19:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int:
Leh_func_begin20:
	push	{r7, lr}
Ltmp33:
	mov	r7, sp
Ltmp34:
Ltmp35:
	bl	_p_17_plt_System_Xml_XmlReader_ReadString_llvm
	pop	{r7, pc}
Leh_func_end20:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReader__cctor:
Leh_func_begin21:
	push	{r4, r5, r7, lr}
Ltmp36:
	add	r7, sp, #8
Ltmp37:
Ltmp38:
	movw	r5, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC21_0+8))
	mov	r1, #4
	movt	r5, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC21_0+8))
LPC21_0:
	add	r5, pc, r5
	ldr	r0, [r5, #48]
	bl	_p_18_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r5, #52]
	mov	r4, r0
	mov	r2, #8
	add	r0, r4, #16
	bl	_p_19_plt_string_memcpy_byte__byte__int_llvm
	ldr	r0, [r5, #56]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end21:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__cctor:
Leh_func_begin22:
	push	{r4, r5, r7, lr}
Ltmp39:
	add	r7, sp, #8
Ltmp40:
Ltmp41:
	movw	r5, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC22_0+8))
	movt	r5, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC22_0+8))
LPC22_0:
	add	r5, pc, r5
	ldr	r0, [r5, #40]
	bl	_p_8_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #1
	mov	r4, r0
	bl	_p_20_plt_System_Xml_XmlDictionaryReaderQuotas__ctor_bool_llvm
	ldr	r0, [r5, #60]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end22:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor:
Leh_func_begin23:
	push	{r7, lr}
Ltmp42:
	mov	r7, sp
Ltmp43:
Ltmp44:
	mov	r1, #0
	bl	_p_20_plt_System_Xml_XmlDictionaryReaderQuotas__ctor_bool_llvm
	pop	{r7, pc}
Leh_func_end23:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor_bool:
Leh_func_begin24:
	cmp	r1, #0
	mvn	r3, #-2147483648
	mvn	r12, #-2147483648
	mvn	r2, #-2147483648
	mvn	r9, #-2147483648
	strb	r1, [r0, #8]
	moveq	r3, #16384
	moveq	r12, #4096
	moveq	r2, #32
	cmp	r1, #0
	moveq	r9, #8192
	str	r3, [r0, #12]
	str	r12, [r0, #16]
	str	r2, [r0, #20]
	str	r3, [r0, #24]
	str	r9, [r0, #28]
	bx	lr
Leh_func_end24:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength:
Leh_func_begin25:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end25:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int:
Leh_func_begin26:
	push	{r7, lr}
Ltmp45:
	mov	r7, sp
Ltmp46:
Ltmp47:
	cmp	r1, #0
	beq	LBB26_4
	cmp	r2, #0
	beq	LBB26_5
	cmp	r3, #536870912
	addlo	r9, r0, #8
	stmlo	r9, {r1, r2, r3}
	poplo	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_0+8))
	mov	r1, #81
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_0+8))
LPC26_0:
	ldr	r0, [pc, r0]
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #520
LBB26_3:
	movt	r0, #512
	bl	_p_14_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_15_plt__jit_icall_mono_arch_throw_exception_llvm
LBB26_4:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_1+8))
	mov	r1, #47
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_1+8))
LPC26_1:
	ldr	r0, [pc, r0]
	b	LBB26_6
LBB26_5:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_2+8))
	mov	r1, #69
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC26_2+8))
LPC26_2:
	ldr	r0, [pc, r0]
LBB26_6:
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	b	LBB26_3
Leh_func_end26:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryString_get_Value
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryString_get_Value:
Leh_func_begin27:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end27:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryString_ToString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryString_ToString:
Leh_func_begin28:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end28:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryString__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryString__cctor:
Leh_func_begin29:
	push	{r4, r5, r6, r7, lr}
Ltmp48:
	add	r7, sp, #12
Ltmp49:
	push	{r10}
Ltmp50:
	movw	r4, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC29_0+8))
	movt	r4, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC29_0+8))
LPC29_0:
	add	r4, pc, r4
	ldr	r1, [r4, #36]
	ldr	r2, [r4, #44]
	ldr	r0, [r4, #64]
	ldr	r10, [r2]
	ldr	r6, [r1]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, r10
	mov	r3, #0
	mov	r5, r0
	bl	_p_21_plt_System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int_llvm
	ldr	r0, [r4, #68]
	str	r5, [r0]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end29:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__ctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__ctor:
Leh_func_begin30:
	bx	lr
Leh_func_end30:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_get_Depth
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_get_Depth:
Leh_func_begin31:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end31:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_Depth_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_Depth_int:
Leh_func_begin32:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end32:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_NSIndex_int
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_NSIndex_int:
Leh_func_begin33:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end33:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_CreateDictionaryWriter_System_Xml_XmlWriter
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_CreateDictionaryWriter_System_Xml_XmlWriter:
Leh_func_begin34:
	push	{r4, r7, lr}
Ltmp51:
	add	r7, sp, #4
Ltmp52:
Ltmp53:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC34_0+8))
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC34_0+8))
LPC34_0:
	add	r0, pc, r0
	ldr	r0, [r0, #72]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	str	r4, [r0, #24]
	pop	{r4, r7, pc}
Leh_func_end34:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlDictionaryReader_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlDictionaryReader_bool:
Leh_func_begin35:
	push	{r4, r5, r6, r7, lr}
Ltmp54:
	add	r7, sp, #12
Ltmp55:
	push	{r10, r11}
Ltmp56:
	sub	sp, sp, #12
	mov	r4, r1
	mov	r1, #0
	mov	r10, r2
	mov	r5, r0
	str	r1, [sp, #4]
	str	r1, [sp, #8]
	cmp	r4, #0
	beq	LBB35_21
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	sub	r1, r0, #1
	cmp	r1, #2
	bls	LBB35_3
	mov	r0, r5
	mov	r1, r4
	mov	r2, r10
	bl	_p_22_plt_System_Xml_XmlWriter_WriteNode_System_Xml_XmlReader_bool_llvm
	b	LBB35_20
LBB35_3:
	cmp	r0, #1
	bne	LBB35_7
	ldr	r0, [r4]
	add	r1, sp, #4
	ldr	r2, [r0, #244]
	mov	r0, r4
	blx	r2
	tst	r0, #255
	beq	LBB35_8
	ldr	r0, [r4]
	add	r1, sp, #8
	ldr	r2, [r0, #244]
	mov	r0, r4
	blx	r2
	tst	r0, #255
	beq	LBB35_8
	ldr	r0, [r4]
	ldr	r1, [r0, #176]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldmib	sp, {r2, r3}
	ldr	r0, [r5]
	ldr	r6, [r0, #156]
	mov	r0, r5
	b	LBB35_9
LBB35_7:
	ldr	r0, [r5]
	mov	r1, r4
	mov	r2, r10
	ldr	r3, [r0, #152]
	mov	r0, r5
	blx	r3
	b	LBB35_20
LBB35_8:
	ldr	r0, [r4]
	ldr	r1, [r0, #176]
	mov	r0, r4
	blx	r1
	str	r0, [sp]
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r11, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	mov	r3, r0
	ldr	r0, [r5]
	ldr	r1, [sp]
	mov	r2, r11
	ldr	r6, [r0, #60]
	mov	r0, r5
LBB35_9:
	blx	r6
	ldr	r0, [r4]
	ldr	r1, [r0, #216]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	beq	LBB35_14
	ldr	r0, [r4]
	ldr	r1, [r0, #236]
	mov	r0, r4
	blx	r1
	ldr	r1, [r4]
	cmp	r0, #0
	ble	LBB35_13
	mov	r6, #0
LBB35_12:
	ldr	r2, [r1, #124]
	mov	r0, r4
	mov	r1, r6
	blx	r2
	mov	r0, r5
	mov	r1, r4
	mov	r2, r10
	bl	_p_23_plt_System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #236]
	mov	r0, r4
	blx	r1
	ldr	r1, [r4]
	add	r6, r6, #1
	cmp	r6, r0
	blt	LBB35_12
LBB35_13:
	ldr	r1, [r1, #108]
	mov	r0, r4
	blx	r1
LBB35_14:
	ldr	r0, [r4]
	ldr	r1, [r0, #212]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	beq	LBB35_16
	ldr	r0, [r5]
	ldr	r1, [r0, #100]
	b	LBB35_19
LBB35_16:
	ldr	r0, [r4]
	ldr	r1, [r0, #224]
	mov	r0, r4
	blx	r1
	mov	r6, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	cmp	r0, #15
	beq	LBB35_18
LBB35_17:
	ldr	r0, [r5]
	mov	r1, r4
	mov	r2, r10
	ldr	r3, [r0, #164]
	mov	r0, r5
	blx	r3
	ldr	r0, [r4]
	ldr	r1, [r0, #224]
	mov	r0, r4
	blx	r1
	cmp	r6, r0
	blt	LBB35_17
LBB35_18:
	ldr	r0, [r5]
	ldr	r1, [r0, #92]
LBB35_19:
	mov	r0, r5
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
LBB35_20:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB35_21:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC35_0+8))
	mov	r1, #89
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC35_0+8))
LPC35_0:
	ldr	r0, [pc, r0]
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_14_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_15_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end35:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool:
Leh_func_begin36:
	push	{r4, r5, r6, r7, lr}
Ltmp57:
	add	r7, sp, #12
Ltmp58:
	push	{r10, r11}
Ltmp59:
	sub	sp, sp, #8
	mov	r5, r1
	mov	r1, #0
	mov	r4, r0
	cmp	r2, #0
	str	r1, [sp]
	str	r1, [sp, #4]
	bne	LBB36_2
	ldr	r0, [r5]
	ldr	r1, [r0, #208]
	mov	r0, r5
	blx	r1
	tst	r0, #255
	bne	LBB36_7
LBB36_2:
	ldr	r0, [r5]
	mov	r1, sp
	ldr	r2, [r0, #244]
	mov	r0, r5
	blx	r2
	tst	r0, #255
	beq	LBB36_5
	ldr	r0, [r5]
	add	r1, sp, #4
	ldr	r2, [r0, #244]
	mov	r0, r5
	blx	r2
	tst	r0, #255
	beq	LBB36_5
	ldr	r0, [r5]
	ldr	r1, [r0, #176]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	ldm	sp, {r2, r3}
	ldr	r0, [r4]
	ldr	r6, [r0, #160]
	mov	r0, r4
	b	LBB36_6
LBB36_5:
	ldr	r0, [r5]
	ldr	r1, [r0, #176]
	mov	r0, r5
	blx	r1
	mov	r10, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #196]
	mov	r0, r5
	blx	r1
	mov	r11, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #188]
	mov	r0, r5
	blx	r1
	mov	r3, r0
	ldr	r0, [r4]
	mov	r1, r10
	mov	r2, r11
	ldr	r6, [r0, #72]
	mov	r0, r4
LBB36_6:
	blx	r6
	ldr	r0, [r4]
	mov	r1, r5
	mov	r2, #1
	ldr	r3, [r0, #152]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r1, [r0, #104]
	mov	r0, r4
	blx	r1
LBB36_7:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end36:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlReader_bool:
Leh_func_begin37:
	push	{r7, lr}
Ltmp60:
	mov	r7, sp
Ltmp61:
Ltmp62:
	cmp	r1, #0
	beq	LBB37_6
	mov	r3, r1
	beq	LBB37_3
	movw	r3, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC37_0+8))
	movt	r3, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC37_0+8))
LPC37_0:
	add	r3, pc, r3
	ldr	r9, [r3, #76]
	ldr	r3, [r1]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r9
	mov	r3, r1
	movne	r3, #0
LBB37_3:
	cmp	r3, #0
	beq	LBB37_5
	ldr	r1, [r0]
	ldr	r9, [r1, #164]
	mov	r1, r3
	blx	r9
	pop	{r7, pc}
LBB37_5:
	bl	_p_22_plt_System_Xml_XmlWriter_WriteNode_System_Xml_XmlReader_bool_llvm
	pop	{r7, pc}
LBB37_6:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC37_1+8))
	mov	r1, #89
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC37_1+8))
LPC37_1:
	ldr	r0, [pc, r0]
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_14_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_15_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end37:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartAttribute_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartAttribute_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString:
Leh_func_begin38:
	push	{r7, lr}
Ltmp63:
	mov	r7, sp
Ltmp64:
Ltmp65:
	ldr	r9, [r2]
	ldr	r2, [r2, #12]
	ldr	r9, [r3]
	ldr	r3, [r3, #12]
	ldr	r9, [r0]
	ldr	r9, [r9, #72]
	blx	r9
	pop	{r7, pc}
Leh_func_end38:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartElement_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartElement_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString:
Leh_func_begin39:
	push	{r4, r7, lr}
Ltmp66:
	add	r7, sp, #4
Ltmp67:
Ltmp68:
	cmp	r2, #0
	beq	LBB39_2
	mov	r9, #0
	cmp	r3, #0
	ldr	r4, [r2]
	ldr	r2, [r2, #12]
	ldrne	r4, [r3]
	ldrne	r9, [r3, #12]
	ldr	r3, [r0]
	ldr	r4, [r3, #60]
	mov	r3, r9
	blx	r4
	pop	{r4, r7, pc}
LBB39_2:
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC39_0+8))
	mov	r1, #103
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC39_0+8))
LPC39_0:
	ldr	r0, [pc, r0]
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC39_1+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC39_1+8))
LPC39_1:
	ldr	r0, [pc, r0]
	bl	_p_13_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_24_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_15_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end39:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteTextNode_System_Xml_XmlDictionaryReader_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteTextNode_System_Xml_XmlDictionaryReader_bool:
Leh_func_begin40:
	push	{r4, r5, r6, r7, lr}
Ltmp69:
	add	r7, sp, #12
Ltmp70:
Ltmp71:
	mov	r4, r1
	mov	r6, r0
	mov	r5, r2
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #56]
	mov	r0, r6
	blx	r2
	cmp	r5, #0
	popne	{r4, r5, r6, r7, pc}
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
	pop	{r4, r5, r6, r7, pc}
Leh_func_end40:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__cctor
	.align	2
_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__cctor:
Leh_func_begin41:
	push	{r4, r5, r7, lr}
Ltmp72:
	add	r7, sp, #8
Ltmp73:
Ltmp74:
	movw	r5, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC41_0+8))
	movt	r5, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC41_0+8))
LPC41_0:
	add	r5, pc, r5
	ldr	r0, [r5, #80]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #0
	mov	r4, r0
	bl	_p_25_plt_System_Text_UTF8Encoding__ctor_bool_llvm
	ldr	r0, [r5, #84]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end41:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter__ctor_System_Xml_XmlWriter
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter__ctor_System_Xml_XmlWriter:
Leh_func_begin42:
	str	r1, [r0, #24]
	bx	lr
Leh_func_end42:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_WriteState
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_WriteState:
Leh_func_begin43:
	push	{r7, lr}
Ltmp75:
	mov	r7, sp
Ltmp76:
Ltmp77:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #144]
	blx	r1
	pop	{r7, pc}
Leh_func_end43:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlLang
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlLang:
Leh_func_begin44:
	push	{r7, lr}
Ltmp78:
	mov	r7, sp
Ltmp79:
Ltmp80:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #140]
	blx	r1
	pop	{r7, pc}
Leh_func_end44:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlSpace
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlSpace:
Leh_func_begin45:
	push	{r7, lr}
Ltmp81:
	mov	r7, sp
Ltmp82:
Ltmp83:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #136]
	blx	r1
	pop	{r7, pc}
Leh_func_end45:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Close
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Close:
Leh_func_begin46:
	push	{r7, lr}
Ltmp84:
	mov	r7, sp
Ltmp85:
Ltmp86:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #132]
	blx	r1
	pop	{r7, pc}
Leh_func_end46:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Flush
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Flush:
Leh_func_begin47:
	push	{r7, lr}
Ltmp87:
	mov	r7, sp
Ltmp88:
Ltmp89:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #124]
	blx	r1
	pop	{r7, pc}
Leh_func_end47:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_LookupPrefix_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_LookupPrefix_string:
Leh_func_begin48:
	push	{r7, lr}
Ltmp90:
	mov	r7, sp
Ltmp91:
Ltmp92:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #120]
	blx	r2
	pop	{r7, pc}
Leh_func_end48:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteCData_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteCData_string:
Leh_func_begin49:
	push	{r7, lr}
Ltmp93:
	mov	r7, sp
Ltmp94:
Ltmp95:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #116]
	blx	r2
	pop	{r7, pc}
Leh_func_end49:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteComment_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteComment_string:
Leh_func_begin50:
	push	{r7, lr}
Ltmp96:
	mov	r7, sp
Ltmp97:
Ltmp98:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #112]
	blx	r2
	pop	{r7, pc}
Leh_func_end50:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteDocType_string_string_string_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteDocType_string_string_string_string:
Leh_func_begin51:
	push	{r7, lr}
Ltmp99:
	mov	r7, sp
Ltmp100:
Ltmp101:
	sub	sp, sp, #4
	ldr	r0, [r0, #24]
	ldr	r12, [r7, #8]
	ldr	r9, [r0]
	ldr	r9, [r9, #108]
	str	r12, [sp]
	blx	r9
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end51:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndAttribute
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndAttribute:
Leh_func_begin52:
	push	{r7, lr}
Ltmp102:
	mov	r7, sp
Ltmp103:
Ltmp104:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #104]
	blx	r1
	pop	{r7, pc}
Leh_func_end52:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndElement
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndElement:
Leh_func_begin53:
	push	{r7, lr}
Ltmp105:
	mov	r7, sp
Ltmp106:
Ltmp107:
	ldr	r1, [r0, #16]
	mov	r3, #0
	sub	r2, r1, #1
	strd	r2, r3, [r0, #16]
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #100]
	blx	r1
	pop	{r7, pc}
Leh_func_end53:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEntityRef_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEntityRef_string:
Leh_func_begin54:
	push	{r7, lr}
Ltmp108:
	mov	r7, sp
Ltmp109:
Ltmp110:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #96]
	blx	r2
	pop	{r7, pc}
Leh_func_end54:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteFullEndElement
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteFullEndElement:
Leh_func_begin55:
	push	{r7, lr}
Ltmp111:
	mov	r7, sp
Ltmp112:
Ltmp113:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #92]
	blx	r1
	pop	{r7, pc}
Leh_func_end55:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteName_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteName_string:
Leh_func_begin56:
	push	{r7, lr}
Ltmp114:
	mov	r7, sp
Ltmp115:
Ltmp116:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #88]
	blx	r2
	pop	{r7, pc}
Leh_func_end56:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteNode_System_Xml_XmlReader_bool:
Leh_func_begin57:
	push	{r7, lr}
Ltmp117:
	mov	r7, sp
Ltmp118:
Ltmp119:
	ldr	r0, [r0, #24]
	ldr	r3, [r0]
	ldr	r3, [r3, #84]
	blx	r3
	pop	{r7, pc}
Leh_func_end57:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteProcessingInstruction_string_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteProcessingInstruction_string_string:
Leh_func_begin58:
	push	{r7, lr}
Ltmp120:
	mov	r7, sp
Ltmp121:
Ltmp122:
	ldr	r0, [r0, #24]
	ldr	r3, [r0]
	ldr	r3, [r3, #80]
	blx	r3
	pop	{r7, pc}
Leh_func_end58:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteRaw_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteRaw_string:
Leh_func_begin59:
	push	{r7, lr}
Ltmp123:
	mov	r7, sp
Ltmp124:
Ltmp125:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #76]
	blx	r2
	pop	{r7, pc}
Leh_func_end59:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartAttribute_string_string_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartAttribute_string_string_string:
Leh_func_begin60:
	push	{r7, lr}
Ltmp126:
	mov	r7, sp
Ltmp127:
Ltmp128:
	ldr	r0, [r0, #24]
	ldr	r9, [r0]
	ldr	r9, [r9, #72]
	blx	r9
	pop	{r7, pc}
Leh_func_end60:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument_bool
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument_bool:
Leh_func_begin61:
	push	{r7, lr}
Ltmp129:
	mov	r7, sp
Ltmp130:
Ltmp131:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #64]
	blx	r2
	pop	{r7, pc}
Leh_func_end61:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument:
Leh_func_begin62:
	push	{r7, lr}
Ltmp132:
	mov	r7, sp
Ltmp133:
Ltmp134:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #68]
	blx	r1
	pop	{r7, pc}
Leh_func_end62:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartElement_string_string_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartElement_string_string_string:
Leh_func_begin63:
	push	{r4, r5, r7, lr}
Ltmp135:
	add	r7, sp, #8
Ltmp136:
Ltmp137:
	ldr	r4, [r0, #16]
	mov	r5, #0
	add	r4, r4, #1
	strd	r4, r5, [r0, #16]
	ldr	r0, [r0, #24]
	ldr	r5, [r0]
	ldr	r9, [r5, #60]
	blx	r9
	pop	{r4, r5, r7, pc}
Leh_func_end63:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteString_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteString_string:
Leh_func_begin64:
	push	{r7, lr}
Ltmp138:
	mov	r7, sp
Ltmp139:
Ltmp140:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #56]
	blx	r2
	pop	{r7, pc}
Leh_func_end64:

	.private_extern	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteWhitespace_string
	.align	2
_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteWhitespace_string:
Leh_func_begin65:
	push	{r7, lr}
Ltmp141:
	mov	r7, sp
Ltmp142:
Ltmp143:
	ldr	r0, [r0, #24]
	ldr	r2, [r0]
	ldr	r2, [r2, #52]
	blx	r2
	pop	{r7, pc}
Leh_func_end65:

	.private_extern	_System_Runtime_Serialization__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_XmlDictionaryString_System_Collections_Generic_KeyValuePair_2_string_System_Xml_XmlDictionaryString_invoke_TRet__this___TKey_TValue_string_System_Xml_XmlDictionaryString
	.align	2
_System_Runtime_Serialization__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_XmlDictionaryString_System_Collections_Generic_KeyValuePair_2_string_System_Xml_XmlDictionaryString_invoke_TRet__this___TKey_TValue_string_System_Xml_XmlDictionaryString:
Leh_func_begin66:
	push	{r4, r5, r6, r7, lr}
Ltmp144:
	add	r7, sp, #12
Ltmp145:
	push	{r10, r11}
Ltmp146:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Runtime_Serialization_got-(LPC66_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Runtime_Serialization_got-(LPC66_0+8))
LPC66_0:
	add	r0, pc, r0
	ldr	r0, [r0, #88]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB66_2
	bl	_p_26_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB66_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB66_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB66_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB66_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB66_7
LBB66_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB66_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end66:

.zerofill __DATA,__bss,_mono_aot_System_Runtime_Serialization_got,200,4
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute_get_IsReference
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute__ctor
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_IsRequired
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Name
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Order
	.no_dead_strip	_System_Runtime_Serialization__System_Runtime_Serialization_EnumMemberAttribute_get_Value
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionary__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_get_Quotas
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_TryGetLocalNameAsDictionaryString_System_Xml_XmlDictionaryString_
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadElementContentAsString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadStartElement_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReader__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryString_get_Value
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryString_ToString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryString__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__ctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_get_Depth
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_Depth_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_NSIndex_int
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_CreateDictionaryWriter_System_Xml_XmlWriter
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlDictionaryReader_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartAttribute_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartElement_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteTextNode_System_Xml_XmlDictionaryReader_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__cctor
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter__ctor_System_Xml_XmlWriter
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_WriteState
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlLang
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlSpace
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Close
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Flush
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_LookupPrefix_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteCData_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteComment_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteDocType_string_string_string_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndAttribute
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndElement
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEntityRef_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteFullEndElement
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteName_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteProcessingInstruction_string_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteRaw_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartAttribute_string_string_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument_bool
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartElement_string_string_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteString_string
	.no_dead_strip	_System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteWhitespace_string
	.no_dead_strip	_System_Runtime_Serialization__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_XmlDictionaryString_System_Collections_Generic_KeyValuePair_2_string_System_Xml_XmlDictionaryString_invoke_TRet__this___TKey_TValue_string_System_Xml_XmlDictionaryString
	.no_dead_strip	_mono_aot_System_Runtime_Serialization_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	67
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	21
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	22
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	23
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	24
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	25
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	26
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	27
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	28
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	29
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	30
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	31
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	32
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	33
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	34
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	35
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	36
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	37
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	38
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	39
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	40
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	41
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	42
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	43
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	44
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	45
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	46
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	47
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	48
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	49
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	50
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	51
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	52
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	53
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	54
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	55
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	56
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	57
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	58
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	59
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	60
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	61
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	62
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	63
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	64
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	70
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
Lset67 = Leh_func_end66-Leh_func_begin66
	.long	Lset67
Lset68 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset68
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin42:
	.byte	0

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin45:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin46:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin51:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin58:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin60:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "System.Runtime.Serialization.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute_get_IsReference
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute__ctor
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_IsRequired
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Name
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Order
.no_dead_strip _System_Runtime_Serialization__System_Runtime_Serialization_EnumMemberAttribute_get_Value
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionary__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_get_Quotas
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_TryGetLocalNameAsDictionaryString_System_Xml_XmlDictionaryString_
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadElementContentAsString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadStartElement_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReader__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryString_get_Value
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryString_ToString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryString__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__ctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_get_Depth
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_Depth_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_NSIndex_int
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_CreateDictionaryWriter_System_Xml_XmlWriter
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlDictionaryReader_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartAttribute_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartElement_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteTextNode_System_Xml_XmlDictionaryReader_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__cctor
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter__ctor_System_Xml_XmlWriter
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_WriteState
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlLang
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlSpace
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Close
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Flush
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_LookupPrefix_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteCData_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteComment_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteDocType_string_string_string_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndAttribute
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndElement
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEntityRef_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteFullEndElement
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteName_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteProcessingInstruction_string_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteRaw_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartAttribute_string_string_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument_bool
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartElement_string_string_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteString_string
.no_dead_strip _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteWhitespace_string
.no_dead_strip _System_Runtime_Serialization__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_XmlDictionaryString_System_Collections_Generic_KeyValuePair_2_string_System_Xml_XmlDictionaryString_invoke_TRet__this___TKey_TValue_string_System_Xml_XmlDictionaryString

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute_get_IsReference
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute__ctor
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_IsRequired
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Name
	bl _System_Runtime_Serialization__System_Runtime_Serialization_DataMemberAttribute_get_Order
	bl _System_Runtime_Serialization__System_Runtime_Serialization_EnumMemberAttribute_get_Value
	bl _System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionary__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_get_Quotas
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_TryGetLocalNameAsDictionaryString_System_Xml_XmlDictionaryString_
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadElementContentAsString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadStartElement_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReader__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas__ctor_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryString_get_Value
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryString_ToString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryString__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__ctor
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_get_Depth
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_Depth_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_set_NSIndex_int
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_CreateDictionaryWriter_System_Xml_XmlWriter
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlDictionaryReader_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartAttribute_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteStartElement_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteTextNode_System_Xml_XmlDictionaryReader_bool
	bl _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter__cctor
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter__ctor_System_Xml_XmlWriter
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_WriteState
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlLang
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_get_XmlSpace
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Close
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_Flush
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_LookupPrefix_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteCData_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteComment_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteDocType_string_string_string_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndAttribute
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEndElement
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteEntityRef_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteFullEndElement
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteName_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteNode_System_Xml_XmlReader_bool
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteProcessingInstruction_string_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteRaw_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartAttribute_string_string_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument_bool
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartDocument
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteStartElement_string_string_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteString_string
	bl _System_Runtime_Serialization__System_Xml_XmlSimpleDictionaryWriter_WriteWhitespace_string
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_Runtime_Serialization__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_XmlDictionaryString_System_Collections_Generic_KeyValuePair_2_string_System_Xml_XmlDictionaryString_invoke_TRet__this___TKey_TValue_string_System_Xml_XmlDictionaryString
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 71,10,8,2
	.short 0, 10, 20, 30, 40, 51, 62, 77
	.byte 1,2,2,2,2,2,2,2,5,3,28,3,5,4,3,3,3,5,3,3,63,6,5,3,3,3,3,3,3,7,102,3
	.byte 3,3,4,3,3,4,3,3,128,134,5,2,2,2,2,2,2,2,2,128,157,2,2,2,2,2,2,2,2,2,128,177
	.byte 2,2,2,2,255,255,255,255,71,0,0,0,0,128,187
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,215
	.long 70,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 5,66,0,67,0,68,0,69
	.long 0,70,215
.section __TEXT, __const
	.align 3
class_name_table:

	.short 37, 5, 0, 0, 0, 0, 0, 0
	.short 0, 8, 0, 0, 0, 0, 0, 6
	.short 0, 10, 39, 0, 0, 0, 0, 0
	.short 0, 4, 40, 7, 37, 2, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 3
	.short 38, 0, 0, 0, 0, 13, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 1
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 9, 0, 11, 0, 12
	.short 0, 14, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 23,10,3,2
	.short 0, 11, 22
	.byte 128,225,2,1,1,1,6,6,3,4,3,129,0,3,7,7,7,4,4,3,4,3,129,45,5,4
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 71,10,8,2
	.short 0, 11, 22, 33, 44, 55, 66, 81
	.byte 130,96,3,3,3,3,3,3,3,3,3,130,126,3,3,3,3,3,3,3,3,3,130,156,3,3,3,3,3,3,3,3
	.byte 3,130,186,3,3,3,3,3,3,3,3,3,130,216,3,3,3,3,3,3,3,3,3,130,246,3,3,3,3,3,3,3
	.byte 3,3,131,20,3,3,3,3,255,255,255,252,224,0,0,0,0,131,35
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 14,10,2,2
	.short 0, 12
	.byte 131,38,7,23,23,23,7,24,24,128,141,24,132,99,75,62,23

.text
	.align 4
plt:
_mono_aot_System_Runtime_Serialization_plt:
_p_1_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 92,311
_p_2_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_XmlDictionaryString__ctor_int_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_XmlDictionaryString__ctor_int
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_XmlDictionaryString__ctor_int:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 96,334
_p_3_plt_System_Collections_Generic_List_1_System_Xml_XmlDictionaryString__ctor_int_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_System_Xml_XmlDictionaryString__ctor_int
plt_System_Collections_Generic_List_1_System_Xml_XmlDictionaryString__ctor_int:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 100,345
_p_4_plt_System_Xml_XmlDictionary__ctor_int_llvm:
	.no_dead_strip plt_System_Xml_XmlDictionary__ctor_int
plt_System_Xml_XmlDictionary__ctor_int:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 104,356

.set _p_5_plt_System_Xml_XmlDictionary__ctor_bool_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionary__ctor_bool

.set _p_6_plt_System_Xml_XmlDictionary_EmptyDictionary__ctor_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionary_EmptyDictionary__ctor
_p_7_plt__class_init_System_Xml_XmlDictionaryReaderQuotas_llvm:
	.no_dead_strip plt__class_init_System_Xml_XmlDictionaryReaderQuotas
plt__class_init_System_Xml_XmlDictionaryReaderQuotas:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 116,362
_p_8_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 120,365
_p_9_plt_System_Xml_XmlDictionaryReaderQuotas__ctor_llvm:
	.no_dead_strip plt_System_Xml_XmlDictionaryReaderQuotas__ctor
plt_System_Xml_XmlDictionaryReaderQuotas__ctor:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 124,391
_p_10_plt_System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength_llvm:
	.no_dead_strip plt_System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength
plt_System_Xml_XmlDictionaryReaderQuotas_get_MaxStringContentLength:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 128,393

.set _p_11_plt_System_Xml_XmlDictionaryReader_ReadContentAsString_int_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadContentAsString_int
_p_12_plt_System_Xml_XmlReader_ReadContentAsString_llvm:
	.no_dead_strip plt_System_Xml_XmlReader_ReadContentAsString
plt_System_Xml_XmlReader_ReadContentAsString:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 136,397
_p_13_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 140,402
_p_14_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 144,422
_p_15_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 148,455

.set _p_16_plt_System_Xml_XmlDictionaryReader_ReadString_int_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionaryReader_ReadString_int
_p_17_plt_System_Xml_XmlReader_ReadString_llvm:
	.no_dead_strip plt_System_Xml_XmlReader_ReadString
plt_System_Xml_XmlReader_ReadString:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 156,485
_p_18_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 160,490
_p_19_plt_string_memcpy_byte__byte__int_llvm:
	.no_dead_strip plt_string_memcpy_byte__byte__int
plt_string_memcpy_byte__byte__int:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 164,516
_p_20_plt_System_Xml_XmlDictionaryReaderQuotas__ctor_bool_llvm:
	.no_dead_strip plt_System_Xml_XmlDictionaryReaderQuotas__ctor_bool
plt_System_Xml_XmlDictionaryReaderQuotas__ctor_bool:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 168,521

.set _p_21_plt_System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionaryString__ctor_System_Xml_IXmlDictionary_string_int
_p_22_plt_System_Xml_XmlWriter_WriteNode_System_Xml_XmlReader_bool_llvm:
	.no_dead_strip plt_System_Xml_XmlWriter_WriteNode_System_Xml_XmlReader_bool
plt_System_Xml_XmlWriter_WriteNode_System_Xml_XmlReader_bool:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 176,525

.set _p_23_plt_System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool_llvm, _System_Runtime_Serialization__System_Xml_XmlDictionaryWriter_WriteAttribute_System_Xml_XmlDictionaryReader_bool
_p_24_plt__jit_icall_mono_create_corlib_exception_2_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_2
plt__jit_icall_mono_create_corlib_exception_2:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 184,532
_p_25_plt_System_Text_UTF8Encoding__ctor_bool_llvm:
	.no_dead_strip plt_System_Text_UTF8Encoding__ctor_bool
plt_System_Text_UTF8Encoding__ctor_bool:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 188,565
_p_26_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Runtime_Serialization_got - . + 192,570
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "System.Runtime.Serialization"
	.asciz "5B04B6ED-328E-42F2-9966-E17AC74BE6D6"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System.Xml"
	.asciz "4D3147BC-C2E3-4CDA-88A7-E0386EE9E899"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "5B04B6ED-328E-42F2-9966-E17AC74BE6D6"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "System.Runtime.Serialization"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_System_Runtime_Serialization_got
	.align 2
	.long _System_Runtime_Serialization__System_Runtime_Serialization_DataContractAttribute__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 23,200,27,71,11,387000831,0,1306
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_System_Runtime_Serialization_info
	.align 2
_mono_aot_module_System_Runtime_Serialization_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,6,2,5,4,1,6,0,1,6,2,7,6,1,7,0,1
	.byte 7,2,9,8,1,8,1,10,1,8,0,1,8,0,1,8,0,1,8,2,11,11,1,8,0,1,8,0,1,8,0,1
	.byte 8,3,14,13,12,1,9,2,15,10,1,9,0,1,9,0,1,9,0,1,10,0,1,10,0,1,10,0,1,10,4,17
	.byte 16,11,9,1,11,0,1,11,0,1,11,0,1,11,0,1,11,1,18,1,11,0,1,11,0,1,11,1,19,1,11,0
	.byte 1,11,0,1,11,0,1,11,2,21,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,22,4,2
	.byte 119,1,2,2,130,146,1,1,10,4,2,97,1,3,2,130,146,1,1,10,7,128,190,255,252,0,0,0,1,1,7,128
	.byte 201,12,0,39,42,47,14,3,219,0,0,1,14,3,219,0,0,2,14,1,6,16,1,6,7,14,1,7,16,1,7,11
	.byte 14,1,9,16,2,130,146,1,136,199,14,6,1,2,130,30,1,29,0,196,0,0,29,0,16,1,8,13,16,1,9,14
	.byte 14,1,10,16,1,10,21,14,1,12,11,1,8,14,2,129,215,1,16,1,11,25,33,7,20,109,111,110,111,95,111,98
	.byte 106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,255,254,0,0,0,0,202,0,0,4,3,255,254,0,0,0,0
	.byte 202,0,0,5,3,8,3,9,3,11,15,1,9,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112
	.byte 116,114,102,114,101,101,0,3,23,3,25,3,16,3,194,0,13,26,7,17,109,111,110,111,95,104,101,108,112,101,114,95
	.byte 108,100,115,116,114,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112
	.byte 116,105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105
	.byte 111,110,0,3,20,3,194,0,13,21,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105
	.byte 102,105,99,0,3,193,0,19,181,3,24,3,26,3,194,0,14,169,3,36,7,30,109,111,110,111,95,99,114,101,97,116
	.byte 101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,50,0,3,193,0,12,11,7,35,109,111,110,111
	.byte 95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,0,128,144,8,0,0,1,4,128,128,9,0,0,1,193,0,18,178,193,0,13,174,193,0,18,174
	.byte 193,0,13,181,4,128,160,20,0,0,4,193,0,18,178,193,0,13,174,193,0,18,174,193,0,13,181,4,128,160,12,0
	.byte 0,4,193,0,18,178,193,0,13,174,193,0,18,174,193,0,13,181,0,128,144,8,0,0,1,4,128,236,10,20,4,0
	.byte 4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,228,12,20,4,0,4,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,55,128,228,21,28,4,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18
	.byte 172,194,0,12,255,194,0,13,28,0,15,17,19,194,0,13,20,194,0,13,19,194,0,13,18,194,0,13,17,194,0,13
	.byte 16,0,0,0,0,0,194,0,13,10,0,0,194,0,13,6,0,194,0,13,4,194,0,13,3,0,0,194,0,13,0,194
	.byte 0,12,244,194,0,12,243,0,194,0,12,241,194,0,12,240,0,0,0,0,0,194,0,12,234,0,194,0,12,232,194,0
	.byte 12,231,194,0,12,230,0,194,0,12,228,0,0,194,0,12,225,0,0,18,14,13,4,128,196,22,32,4,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,228,29,20,4,0,4,28,193,0,18,175,193,0,18,174,193
	.byte 0,18,172,34,128,228,41,24,4,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,194,0,14,152,0
	.byte 0,0,0,0,0,0,0,37,194,0,14,167,0,0,0,0,0,0,0,0,0,194,0,14,151,194,0,14,147,194,0,14
	.byte 146,194,0,14,145,0,194,0,14,143,40,39,38,35,34,128,160,28,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,194,0,14,152,65,64,63,61,62,60,59,58,57,56,55,54,53,52,51,50,49,48,47,194,0,14,151
	.byte 46,45,44,43,194,0,14,143,40,39,38,35,4,128,136,8,8,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193
	.byte 0,18,172,4,128,144,16,0,1,1,193,0,21,37,193,0,21,36,193,0,18,174,193,0,21,34,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
