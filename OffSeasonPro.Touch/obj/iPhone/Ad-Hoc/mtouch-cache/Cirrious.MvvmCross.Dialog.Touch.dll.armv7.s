	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool:
Leh_func_begin1:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	bl	_p_1_plt_CrossUI_Touch_Dialog_DialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	pop	{r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr:
Leh_func_begin2:
	push	{r7, lr}
Ltmp3:
	mov	r7, sp
Ltmp4:
Ltmp5:
	bl	_p_2_plt_CrossUI_Touch_Dialog_DialogViewController__ctor_intptr_llvm
	pop	{r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool:
Leh_func_begin3:
	push	{r4, r5, r7, lr}
Ltmp6:
	add	r7, sp, #8
Ltmp7:
	push	{r8}
Ltmp8:
	mov	r4, r1
	mov	r5, r0
	bl	_p_3_plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r5, #128]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC3_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC3_0+8))
LPC3_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	mov	r8, r1
	mov	r1, r5
	bl	_p_4_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool:
Leh_func_begin4:
	push	{r4, r5, r7, lr}
Ltmp9:
	add	r7, sp, #8
Ltmp10:
	push	{r8}
Ltmp11:
	mov	r4, r1
	mov	r5, r0
	bl	_p_5_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r5, #120]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC4_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC4_0+8))
LPC4_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	mov	r8, r1
	mov	r1, r5
	bl	_p_4_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillAppear_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillAppear_bool:
Leh_func_begin5:
	push	{r4, r5, r7, lr}
Ltmp12:
	add	r7, sp, #8
Ltmp13:
	push	{r8}
Ltmp14:
	mov	r4, r1
	mov	r5, r0
	bl	_p_6_plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r5, #116]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC5_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC5_0+8))
LPC5_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	mov	r8, r1
	mov	r1, r5
	bl	_p_4_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidDisappear_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidDisappear_bool:
Leh_func_begin6:
	push	{r4, r5, r7, lr}
Ltmp15:
	add	r7, sp, #8
Ltmp16:
	push	{r8}
Ltmp17:
	mov	r4, r1
	mov	r5, r0
	bl	_p_7_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm
	ldr	r0, [r5, #124]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC6_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC6_0+8))
LPC6_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	mov	r8, r1
	mov	r1, r5
	bl	_p_4_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad:
Leh_func_begin7:
	push	{r4, r7, lr}
Ltmp18:
	add	r7, sp, #4
Ltmp19:
Ltmp20:
	mov	r4, r0
	bl	_p_8_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	ldr	r0, [r4, #112]
	mov	r1, r4
	bl	_p_9_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
	pop	{r4, r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_Dispose_bool:
Leh_func_begin8:
	push	{r4, r5, r7, lr}
Ltmp21:
	add	r7, sp, #8
Ltmp22:
Ltmp23:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB8_2
	ldr	r0, [r5, #132]
	mov	r1, r5
	bl	_p_9_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
LBB8_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_10_plt_MonoTouch_UIKit_UITableViewController_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin9:
	push	{r4, r5, r6, r7, lr}
Ltmp24:
	add	r7, sp, #12
Ltmp25:
	push	{r8, r10, r11}
Ltmp26:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #112]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC9_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC9_2+8))
LPC9_2:
	add	r0, pc, r0
	ldr	r10, [r0, #24]
	beq	LBB9_5
	ldr	r4, [r0, #20]
	b	LBB9_3
LBB9_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_13_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB9_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB9_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB9_8
	b	LBB9_2
LBB9_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB9_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB9_8
Ltmp27:
LBB9_7:
	ldr	r0, LCPI9_1
LPC9_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp28:
LBB9_8:
	ldr	r0, LCPI9_0
LPC9_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI9_0:
	.long	Ltmp28-(LPC9_0+8)
LCPI9_1:
	.long	Ltmp27-(LPC9_1+8)
	.end_data_region
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin10:
	push	{r4, r5, r6, r7, lr}
Ltmp29:
	add	r7, sp, #12
Ltmp30:
	push	{r8, r10, r11}
Ltmp31:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #112]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC10_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC10_2+8))
LPC10_2:
	add	r0, pc, r0
	ldr	r10, [r0, #24]
	beq	LBB10_5
	ldr	r4, [r0, #20]
	b	LBB10_3
LBB10_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_13_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB10_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB10_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB10_8
	b	LBB10_2
LBB10_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB10_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB10_8
Ltmp32:
LBB10_7:
	ldr	r0, LCPI10_1
LPC10_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp33:
LBB10_8:
	ldr	r0, LCPI10_0
LPC10_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI10_0:
	.long	Ltmp33-(LPC10_0+8)
LCPI10_1:
	.long	Ltmp32-(LPC10_1+8)
	.end_data_region
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin11:
	push	{r4, r5, r6, r7, lr}
Ltmp34:
	add	r7, sp, #12
Ltmp35:
	push	{r8, r10, r11}
Ltmp36:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #116]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC11_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC11_2+8))
LPC11_2:
	add	r0, pc, r0
	ldr	r10, [r0, #32]
	beq	LBB11_5
	ldr	r4, [r0, #28]
	b	LBB11_3
LBB11_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB11_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB11_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB11_8
	b	LBB11_2
LBB11_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB11_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB11_8
Ltmp37:
LBB11_7:
	ldr	r0, LCPI11_1
LPC11_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp38:
LBB11_8:
	ldr	r0, LCPI11_0
LPC11_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI11_0:
	.long	Ltmp38-(LPC11_0+8)
LCPI11_1:
	.long	Ltmp37-(LPC11_1+8)
	.end_data_region
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin12:
	push	{r4, r5, r6, r7, lr}
Ltmp39:
	add	r7, sp, #12
Ltmp40:
	push	{r8, r10, r11}
Ltmp41:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #116]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC12_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC12_2+8))
LPC12_2:
	add	r0, pc, r0
	ldr	r10, [r0, #32]
	beq	LBB12_5
	ldr	r4, [r0, #28]
	b	LBB12_3
LBB12_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB12_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB12_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB12_8
	b	LBB12_2
LBB12_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB12_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB12_8
Ltmp42:
LBB12_7:
	ldr	r0, LCPI12_1
LPC12_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp43:
LBB12_8:
	ldr	r0, LCPI12_0
LPC12_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI12_0:
	.long	Ltmp43-(LPC12_0+8)
LCPI12_1:
	.long	Ltmp42-(LPC12_1+8)
	.end_data_region
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp44:
	add	r7, sp, #12
Ltmp45:
	push	{r8, r10, r11}
Ltmp46:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #120]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC13_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC13_2+8))
LPC13_2:
	add	r0, pc, r0
	ldr	r10, [r0, #32]
	beq	LBB13_5
	ldr	r4, [r0, #28]
	b	LBB13_3
LBB13_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB13_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB13_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB13_8
	b	LBB13_2
LBB13_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB13_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB13_8
Ltmp47:
LBB13_7:
	ldr	r0, LCPI13_1
LPC13_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp48:
LBB13_8:
	ldr	r0, LCPI13_0
LPC13_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI13_0:
	.long	Ltmp48-(LPC13_0+8)
LCPI13_1:
	.long	Ltmp47-(LPC13_1+8)
	.end_data_region
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin14:
	push	{r4, r5, r6, r7, lr}
Ltmp49:
	add	r7, sp, #12
Ltmp50:
	push	{r8, r10, r11}
Ltmp51:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #120]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC14_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC14_2+8))
LPC14_2:
	add	r0, pc, r0
	ldr	r11, [r0, #28]
	ldr	r10, [r0, #32]
LBB14_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB14_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB14_6
LBB14_3:
	cmp	r5, #0
	beq	LBB14_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB14_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp52:
LBB14_6:
	ldr	r0, LCPI14_0
LPC14_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp53:
LBB14_7:
	ldr	r0, LCPI14_1
LPC14_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI14_0:
	.long	Ltmp52-(LPC14_0+8)
LCPI14_1:
	.long	Ltmp53-(LPC14_1+8)
	.end_data_region
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin15:
	push	{r4, r5, r6, r7, lr}
Ltmp54:
	add	r7, sp, #12
Ltmp55:
	push	{r8, r10, r11}
Ltmp56:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #124]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC15_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC15_2+8))
LPC15_2:
	add	r0, pc, r0
	ldr	r11, [r0, #28]
	ldr	r10, [r0, #32]
LBB15_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB15_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB15_6
LBB15_3:
	cmp	r5, #0
	beq	LBB15_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB15_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp57:
LBB15_6:
	ldr	r0, LCPI15_0
LPC15_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp58:
LBB15_7:
	ldr	r0, LCPI15_1
LPC15_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI15_0:
	.long	Ltmp57-(LPC15_0+8)
LCPI15_1:
	.long	Ltmp58-(LPC15_1+8)
	.end_data_region
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin16:
	push	{r4, r5, r6, r7, lr}
Ltmp59:
	add	r7, sp, #12
Ltmp60:
	push	{r8, r10, r11}
Ltmp61:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #124]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC16_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC16_2+8))
LPC16_2:
	add	r0, pc, r0
	ldr	r11, [r0, #28]
	ldr	r10, [r0, #32]
LBB16_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB16_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB16_6
LBB16_3:
	cmp	r5, #0
	beq	LBB16_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB16_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp62:
LBB16_6:
	ldr	r0, LCPI16_0
LPC16_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp63:
LBB16_7:
	ldr	r0, LCPI16_1
LPC16_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI16_0:
	.long	Ltmp62-(LPC16_0+8)
LCPI16_1:
	.long	Ltmp63-(LPC16_1+8)
	.end_data_region
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin17:
	push	{r4, r5, r6, r7, lr}
Ltmp64:
	add	r7, sp, #12
Ltmp65:
	push	{r8, r10, r11}
Ltmp66:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #128]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC17_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC17_2+8))
LPC17_2:
	add	r0, pc, r0
	ldr	r11, [r0, #28]
	ldr	r10, [r0, #32]
LBB17_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB17_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB17_6
LBB17_3:
	cmp	r5, #0
	beq	LBB17_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB17_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp67:
LBB17_6:
	ldr	r0, LCPI17_0
LPC17_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp68:
LBB17_7:
	ldr	r0, LCPI17_1
LPC17_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI17_0:
	.long	Ltmp67-(LPC17_0+8)
LCPI17_1:
	.long	Ltmp68-(LPC17_1+8)
	.end_data_region
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp69:
	add	r7, sp, #12
Ltmp70:
	push	{r8, r10, r11}
Ltmp71:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #128]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC18_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC18_2+8))
LPC18_2:
	add	r0, pc, r0
	ldr	r11, [r0, #28]
	ldr	r10, [r0, #32]
LBB18_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB18_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB18_6
LBB18_3:
	cmp	r5, #0
	beq	LBB18_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB18_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp72:
LBB18_6:
	ldr	r0, LCPI18_0
LPC18_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp73:
LBB18_7:
	ldr	r0, LCPI18_1
LPC18_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI18_0:
	.long	Ltmp72-(LPC18_0+8)
LCPI18_1:
	.long	Ltmp73-(LPC18_1+8)
	.end_data_region
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_DisposeCalled_System_EventHandler:
Leh_func_begin19:
	push	{r4, r5, r6, r7, lr}
Ltmp74:
	add	r7, sp, #12
Ltmp75:
	push	{r8, r10, r11}
Ltmp76:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #132]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC19_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC19_2+8))
LPC19_2:
	add	r0, pc, r0
	ldr	r11, [r0, #20]
	ldr	r10, [r0, #24]
LBB19_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB19_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB19_6
LBB19_3:
	cmp	r5, #0
	beq	LBB19_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_13_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB19_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp77:
LBB19_6:
	ldr	r0, LCPI19_0
LPC19_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp78:
LBB19_7:
	ldr	r0, LCPI19_1
LPC19_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI19_0:
	.long	Ltmp77-(LPC19_0+8)
LCPI19_1:
	.long	Ltmp78-(LPC19_1+8)
	.end_data_region
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_DisposeCalled_System_EventHandler:
Leh_func_begin20:
	push	{r4, r5, r6, r7, lr}
Ltmp79:
	add	r7, sp, #12
Ltmp80:
	push	{r8, r10, r11}
Ltmp81:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #132]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC20_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC20_2+8))
LPC20_2:
	add	r0, pc, r0
	ldr	r11, [r0, #20]
	ldr	r10, [r0, #24]
LBB20_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB20_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB20_6
LBB20_3:
	cmp	r5, #0
	beq	LBB20_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_13_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB20_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp82:
LBB20_6:
	ldr	r0, LCPI20_0
LPC20_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp83:
LBB20_7:
	ldr	r0, LCPI20_1
LPC20_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI20_0:
	.long	Ltmp82-(LPC20_0+8)
LCPI20_1:
	.long	Ltmp83-(LPC20_1+8)
	.end_data_region
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool:
Leh_func_begin21:
	push	{r4, r7, lr}
Ltmp84:
	add	r7, sp, #4
Ltmp85:
Ltmp86:
	mov	r4, r0
	bl	_p_16_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	mov	r0, r4
	bl	_p_17_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_intptr:
Leh_func_begin22:
	push	{r4, r7, lr}
Ltmp87:
	add	r7, sp, #4
Ltmp88:
Ltmp89:
	mov	r4, r0
	bl	_p_18_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_17_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext:
Leh_func_begin23:
	push	{r7, lr}
Ltmp90:
	mov	r7, sp
Ltmp91:
	push	{r8}
Ltmp92:
	ldr	r0, [r0, #140]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC23_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC23_0+8))
LPC23_0:
	add	r1, pc, r1
	ldr	r1, [r1, #36]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object:
Leh_func_begin24:
	push	{r7, lr}
Ltmp93:
	mov	r7, sp
Ltmp94:
	push	{r8}
Ltmp95:
	ldr	r0, [r0, #140]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC24_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC24_0+8))
LPC24_0:
	add	r3, pc, r3
	ldr	r3, [r3, #40]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel:
Leh_func_begin25:
	push	{r7, lr}
Ltmp96:
	mov	r7, sp
Ltmp97:
Ltmp98:
	bl	_p_19_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext_llvm
	mov	r9, #0
	cmp	r0, #0
	beq	LBB25_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC25_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC25_0+8))
	ldr	r3, [r0]
LPC25_0:
	add	r2, pc, r2
	ldr	r2, [r2, #44]
	ldrh	r1, [r3, #20]
	cmp	r1, r2
	blo	LBB25_3
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r9, r0
LBB25_3:
	mov	r0, r9
	pop	{r7, pc}
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin26:
	push	{r7, lr}
Ltmp99:
	mov	r7, sp
Ltmp100:
Ltmp101:
	bl	_p_20_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object_llvm
	pop	{r7, pc}
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_Request
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_Request:
Leh_func_begin27:
	ldr	r0, [r0, #136]
	bx	lr
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin28:
	str	r1, [r0, #136]
	bx	lr
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_BindingContext:
Leh_func_begin29:
	ldr	r0, [r0, #140]
	bx	lr
Leh_func_end29:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin30:
	str	r1, [r0, #140]
	bx	lr
Leh_func_end30:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_string
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_string:
Leh_func_begin31:
	push	{r4, r5, r6, r7, lr}
Ltmp102:
	add	r7, sp, #12
Ltmp103:
	push	{r8}
Ltmp104:
	sub	sp, sp, #4
	mov	r5, r0
	mov	r0, r8
	mov	r4, r2
	mov	r6, r1
	str	r8, [sp]
	bl	_p_21_plt__rgctx_fetch_0_llvm
	mov	r8, r0
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	_p_22_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	sub	sp, r7, #16
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end31:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:
Leh_func_begin32:
	push	{r4, r5, r6, r7, lr}
Ltmp105:
	add	r7, sp, #12
Ltmp106:
	push	{r8}
Ltmp107:
	sub	sp, sp, #4
	mov	r5, r0
	mov	r0, r8
	mov	r4, r2
	mov	r6, r1
	str	r8, [sp]
	bl	_p_23_plt__rgctx_fetch_1_llvm
	mov	r8, r0
	mov	r0, r6
	mov	r1, r5
	mov	r2, r4
	bl	_p_24_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm
	sub	sp, r7, #16
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end32:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogViewController__ctor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogViewController__ctor:
Leh_func_begin33:
	push	{r4, r5, r7, lr}
Ltmp108:
	add	r7, sp, #8
Ltmp109:
Ltmp110:
	mov	r1, #1
	mov	r2, #0
	mov	r3, #0
	mov	r4, r0
	bl	_p_25_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC33_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC33_0+8))
LPC33_0:
	add	r0, pc, r0
	ldr	r0, [r0, #48]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_27_plt_Cirrious_MvvmCross_ViewModels_MvxNullViewModel__ctor_llvm
	mov	r0, r4
	mov	r1, r5
	bl	_p_28_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end33:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
Leh_func_begin34:
	push	{r7, lr}
Ltmp111:
	mov	r7, sp
Ltmp112:
Ltmp113:
	bl	_p_29_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm
	pop	{r7, pc}
Leh_func_end34:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin35:
	push	{r7, lr}
Ltmp114:
	mov	r7, sp
Ltmp115:
Ltmp116:
	bl	_p_30_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm
	pop	{r7, pc}
Leh_func_end35:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
Leh_func_begin36:
	push	{r4, r5, r6, r7, lr}
Ltmp117:
	add	r7, sp, #12
Ltmp118:
	push	{r8, r10, r11}
Ltmp119:
	sub	sp, sp, #12
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC36_0+8))
	mov	r10, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC36_0+8))
LPC36_0:
	add	r5, pc, r5
	ldr	r0, [r5, #52]
	ldr	r4, [r5, #60]
	ldr	r11, [r5, #56]
	str	r0, [sp, #8]
	ldr	r0, [r5, #64]
	str	r4, [sp, #4]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	mov	r2, r11
	mov	r3, r4
	mov	r6, r0
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string_llvm
	ldr	r0, [r10]
	ldr	r1, [r5, #68]
	sub	r0, r0, #44
	str	r1, [sp, #8]
	mov	r8, r1
	mov	r11, r1
	mov	r1, r6
	ldr	r2, [r0]
	mov	r0, r10
	blx	r2
	ldr	r0, [r5, #64]
	ldr	r4, [r5, #72]
	ldr	r1, [r5, #76]
	str	r1, [sp]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	ldm	sp, {r2, r3}
	mov	r1, r4
	mov	r6, r0
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string_llvm
	ldr	r0, [r10]
	mov	r8, r11
	mov	r1, r6
	sub	r0, r0, #44
	ldr	r2, [r0]
	mov	r0, r10
	blx	r2
	ldr	r0, [r5, #64]
	ldr	r6, [r5, #80]
	ldr	r4, [r5, #84]
	ldr	r11, [r5, #88]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, r4
	mov	r3, r11
	mov	r5, r0
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string_llvm
	ldr	r0, [r10]
	ldr	r8, [sp, #8]
	mov	r1, r5
	sub	r0, r0, #44
	ldr	r2, [r0]
	mov	r0, r10
	blx	r2
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end36:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin37:
	push	{r7, lr}
Ltmp120:
	mov	r7, sp
Ltmp121:
	push	{r8}
Ltmp122:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC37_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC37_0+8))
	ldr	r3, [r1]
LPC37_0:
	add	r0, pc, r0
	ldr	r2, [r0, #60]
	ldr	r9, [r0, #76]
	ldr	r0, [r0, #92]
	sub	r3, r3, #68
	ldr	r3, [r3]
	mov	r8, r0
	mov	r0, r1
	mov	r1, r9
	blx	r3
	pop	{r8}
	pop	{r7, pc}
Leh_func_end37:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__:
Leh_func_begin38:
	push	{r4, r5, r7, lr}
Ltmp123:
	add	r7, sp, #8
Ltmp124:
	push	{r8}
Ltmp125:
	mov	r5, r1
	mov	r1, #0
	mov	r2, #0
	mov	r4, r0
	bl	_p_32_plt_Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC38_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC38_0+8))
LPC38_0:
	add	r0, pc, r0
	ldr	r0, [r0, #96]
	mov	r8, r0
	mov	r0, r5
	bl	_p_33_plt_System_Linq_Enumerable_ToList_System_Type_System_Collections_Generic_IEnumerable_1_System_Type_llvm
	str	r0, [r4, #28]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end38:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_get_ValueConverterHolders
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_get_ValueConverterHolders:
Leh_func_begin39:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end39:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_CreateApp
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_CreateApp:
Leh_func_begin40:
	push	{r7, lr}
Ltmp126:
	mov	r7, sp
Ltmp127:
Ltmp128:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC40_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC40_0+8))
LPC40_0:
	add	r0, pc, r0
	ldr	r0, [r0, #100]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	pop	{r7, pc}
Leh_func_end40:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_InitializeViewLookup
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_InitializeViewLookup:
Leh_func_begin41:
	bx	lr
Leh_func_end41:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_Initialize_System_Type__
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_Initialize_System_Type__:
Leh_func_begin42:
	push	{r4, r5, r7, lr}
Ltmp129:
	add	r7, sp, #8
Ltmp130:
Ltmp131:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC42_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC42_0+8))
LPC42_0:
	add	r0, pc, r0
	ldr	r0, [r0, #104]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_34_plt_Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type___llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #188]
	mov	r0, r5
	blx	r1
	pop	{r4, r5, r7, pc}
Leh_func_end42:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxEmptyApp__ctor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxEmptyApp__ctor:
Leh_func_begin43:
	bx	lr
Leh_func_end43:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin44:
	push	{r4, r5, r6, r7, lr}
Ltmp132:
	add	r7, sp, #12
Ltmp133:
Ltmp134:
	mov	r4, r0
	bl	_p_35_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB44_3
	cmp	r4, #0
	beq	LBB44_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC44_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC44_1+8))
LPC44_1:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #112]
	str	r0, [r1, #20]
	ldr	r0, [r6, #116]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_37_plt_CrossUI_Touch_Dialog_Elements_EntryElement_add_Changed_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB44_3:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC44_2+8))
	mov	r1, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC44_2+8))
LPC44_2:
	add	r0, pc, r0
	ldrd	r4, r5, [r0, #124]
	mov	r0, r5
	bl	_p_38_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_39_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp135:
LBB44_4:
	ldr	r0, LCPI44_0
LPC44_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI44_0:
	.long	Ltmp135-(LPC44_0+8)
	.end_data_region
Leh_func_end44:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_get_DefaultMode:
Leh_func_begin45:
	mov	r0, #1
	bx	lr
Leh_func_end45:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_EntryElementOnChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_EntryElementOnChanged_object_System_EventArgs:
Leh_func_begin46:
	push	{r4, r7, lr}
Ltmp136:
	add	r7, sp, #4
Ltmp137:
Ltmp138:
	mov	r4, r0
	bl	_p_36_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View_llvm
	ldr	r1, [r0]
	ldr	r1, [r0, #44]
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end46:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_Dispose_bool:
Leh_func_begin47:
	push	{r4, r5, r6, r7, lr}
Ltmp139:
	add	r7, sp, #12
Ltmp140:
Ltmp141:
	mov	r5, r1
	mov	r4, r0
	bl	_p_40_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	beq	LBB47_3
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	cmp	r4, #0
	beq	LBB47_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC47_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC47_1+8))
LPC47_1:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #112]
	str	r0, [r1, #20]
	ldr	r0, [r6, #116]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_41_plt_CrossUI_Touch_Dialog_Elements_EntryElement_remove_Changed_System_EventHandler_llvm
LBB47_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp142:
LBB47_4:
	ldr	r0, LCPI47_0
LPC47_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI47_0:
	.long	Ltmp142-(LPC47_0+8)
	.end_data_region
Leh_func_end47:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin48:
	push	{r4, r5, r6, r7, lr}
Ltmp143:
	add	r7, sp, #12
Ltmp144:
Ltmp145:
	mov	r4, r0
	bl	_p_42_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_43_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB48_3
	cmp	r4, #0
	beq	LBB48_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC48_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC48_1+8))
LPC48_1:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #132]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #136]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_44_plt_CrossUI_Touch_Dialog_Elements_RootElement_add_RadioSelectedChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB48_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC48_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC48_2+8))
LPC48_2:
	add	r1, pc, r1
	ldr	r0, [r1, #128]
	ldr	r4, [r1, #140]
	mov	r1, #0
	bl	_p_38_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_39_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp146:
LBB48_4:
	ldr	r0, LCPI48_0
LPC48_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI48_0:
	.long	Ltmp146-(LPC48_0+8)
	.end_data_region
Leh_func_end48:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_get_DefaultMode:
Leh_func_begin49:
	mov	r0, #1
	bx	lr
Leh_func_end49:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_RootElementOnRadioSelectedChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_RootElementOnRadioSelectedChanged_object_System_EventArgs:
Leh_func_begin50:
	push	{r4, r5, r7, lr}
Ltmp147:
	add	r7, sp, #8
Ltmp148:
Ltmp149:
	mov	r4, r0
	bl	_p_43_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View_llvm
	ldr	r1, [r0]
	bl	_p_45_plt_CrossUI_Touch_Dialog_Elements_RootElement_get_RadioSelected_llvm
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC50_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC50_0+8))
LPC50_0:
	add	r0, pc, r0
	ldr	r0, [r0, #144]
	bl	_p_46_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	str	r5, [r1, #8]
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end50:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_Dispose_bool:
Leh_func_begin51:
	push	{r4, r5, r6, r7, lr}
Ltmp150:
	add	r7, sp, #12
Ltmp151:
Ltmp152:
	mov	r5, r1
	mov	r4, r0
	bl	_p_40_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	beq	LBB51_3
	mov	r0, r4
	bl	_p_43_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	cmp	r4, #0
	beq	LBB51_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC51_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC51_1+8))
LPC51_1:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #132]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #136]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_47_plt_CrossUI_Touch_Dialog_Elements_RootElement_remove_RadioSelectedChanged_System_EventHandler_llvm
LBB51_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp153:
LBB51_4:
	ldr	r0, LCPI51_0
LPC51_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI51_0:
	.long	Ltmp153-(LPC51_0+8)
	.end_data_region
Leh_func_end51:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin52:
	push	{r4, r5, r6, r7, lr}
Ltmp154:
	add	r7, sp, #12
Ltmp155:
Ltmp156:
	mov	r4, r0
	bl	_p_48_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB52_3
	cmp	r4, #0
	beq	LBB52_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC52_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC52_1+8))
LPC52_1:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #148]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #152]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_50_plt_CrossUI_Touch_Dialog_Elements_ValueElement_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB52_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC52_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC52_2+8))
LPC52_2:
	add	r1, pc, r1
	ldr	r0, [r1, #128]
	ldr	r4, [r1, #156]
	mov	r1, #0
	bl	_p_38_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_39_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp157:
LBB52_4:
	ldr	r0, LCPI52_0
LPC52_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI52_0:
	.long	Ltmp157-(LPC52_0+8)
	.end_data_region
Leh_func_end52:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_ValueElementOnValueChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_ValueElementOnValueChanged_object_System_EventArgs:
Leh_func_begin53:
	push	{r4, r7, lr}
Ltmp158:
	add	r7, sp, #4
Ltmp159:
Ltmp160:
	mov	r4, r0
	bl	_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #96]
	blx	r1
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end53:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_get_DefaultMode:
Leh_func_begin54:
	mov	r0, #1
	bx	lr
Leh_func_end54:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_Dispose_bool:
Leh_func_begin55:
	push	{r4, r5, r6, r7, lr}
Ltmp161:
	add	r7, sp, #12
Ltmp162:
	push	{r10}
Ltmp163:
	mov	r10, r1
	mov	r5, r0
	cmp	r10, #0
	beq	LBB55_4
	mov	r0, r5
	bl	_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View_llvm
	mov	r6, r0
	cmp	r6, #0
	beq	LBB55_4
	cmp	r5, #0
	beq	LBB55_5
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC55_1+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC55_1+8))
LPC55_1:
	add	r4, pc, r4
	ldr	r0, [r4, #108]
	bl	_p_26_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r4, #148]
	str	r5, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r4, #152]
	str	r0, [r1, #28]
	ldr	r0, [r4, #120]
	str	r0, [r1, #12]
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_51_plt_CrossUI_Touch_Dialog_Elements_ValueElement_remove_ValueChanged_System_EventHandler_llvm
LBB55_4:
	mov	r0, r5
	mov	r1, r10
	bl	_p_40_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Ltmp164:
LBB55_5:
	ldr	r0, LCPI55_0
LPC55_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI55_0:
	.long	Ltmp164-(LPC55_0+8)
	.end_data_region
Leh_func_end55:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_Entries
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_Entries:
Leh_func_begin56:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end56:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_Entries_System_Collections_IList
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_Entries_System_Collections_IList:
Leh_func_begin57:
	str	r1, [r0, #52]
	bx	lr
Leh_func_end57:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_DisplayValueConverter
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_DisplayValueConverter:
Leh_func_begin58:
	ldr	r0, [r0, #56]
	bx	lr
Leh_func_end58:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_DisplayValueConverter_Cirrious_CrossCore_Converters_IMvxValueConverter
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_DisplayValueConverter_Cirrious_CrossCore_Converters_IMvxValueConverter:
Leh_func_begin59:
	str	r1, [r0, #56]
	bx	lr
Leh_func_end59:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_BackgroundColor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_BackgroundColor:
Leh_func_begin60:
	ldr	r0, [r0, #60]
	bx	lr
Leh_func_end60:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_BackgroundColor_MonoTouch_UIKit_UIColor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_BackgroundColor_MonoTouch_UIKit_UIColor:
Leh_func_begin61:
	str	r1, [r0, #60]
	bx	lr
Leh_func_end61:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__ctor_string_object_Cirrious_CrossCore_Converters_IMvxValueConverter
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__ctor_string_object_Cirrious_CrossCore_Converters_IMvxValueConverter:
Leh_func_begin62:
	push	{r4, r5, r7, lr}
Ltmp165:
	add	r7, sp, #8
Ltmp166:
Ltmp167:
	mov	r5, r3
	mov	r4, r0
	bl	_p_52_plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object__ctor_string_object_llvm
	cmp	r5, #0
	bne	LBB62_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC62_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC62_0+8))
LPC62_0:
	add	r0, pc, r0
	ldr	r0, [r0, #160]
	bl	_p_57_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r5, r0
LBB62_2:
	str	r5, [r4, #56]
	bl	_p_53_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm
	ldr	r1, [r0]
	mov	r2, #0
	mov	r1, #7
	bl	_p_54_plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int_llvm
	tst	r0, #255
	beq	LBB62_4
	bl	_p_55_plt_MonoTouch_UIKit_UIColor_get_White_llvm
	str	r0, [r4, #60]
	pop	{r4, r5, r7, pc}
LBB62_4:
	bl	_p_56_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	str	r0, [r4, #60]
	pop	{r4, r5, r7, pc}
Leh_func_end62:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetCellImpl_MonoTouch_UIKit_UITableView:
Leh_func_begin63:
	push	{r4, r5, r6, r7, lr}
Ltmp168:
	add	r7, sp, #12
Ltmp169:
	push	{r10}
Ltmp170:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC63_0+8))
	mov	r4, r0
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC63_0+8))
LPC63_0:
	add	r10, pc, r10
	ldr	r6, [r10, #164]
	ldr	r2, [r6]
	ldr	r0, [r1]
	mov	r0, r1
	mov	r1, r2
	bl	_p_58_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm
	mov	r5, r0
	cmp	r5, #0
	bne	LBB63_2
	ldr	r0, [r10, #168]
	ldr	r6, [r6]
	bl	_p_59_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, #1
	mov	r2, r6
	mov	r5, r0
	bl	_p_60_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_llvm
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r2, [r0, #272]
	mov	r0, r5
	blx	r2
LBB63_2:
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #88]
	mov	r0, r4
	blx	r2
	mov	r0, r5
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end63:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Dispose_bool:
Leh_func_begin64:
	push	{r4, r5, r7, lr}
Ltmp171:
	add	r7, sp, #8
Ltmp172:
Ltmp173:
	mov	r4, r0
	cmp	r1, #0
	ldrne	r0, [r4, #48]
	cmpne	r0, #0
	beq	LBB64_2
	ldr	r0, [r4, #48]
	ldr	r1, [r0]
	ldr	r0, [r0, #48]
	ldr	r1, [r0]
	bl	_p_61_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	ldr	r0, [r4, #48]
	mov	r5, #0
	ldr	r1, [r0]
	mov	r1, #0
	bl	_p_62_plt_MonoTouch_UIKit_UIPickerView_set_Model_MonoTouch_UIKit_UIPickerViewModel_llvm
	ldr	r0, [r4, #48]
	ldr	r1, [r0]
	bl	_p_61_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	str	r5, [r4, #48]
LBB64_2:
	pop	{r4, r5, r7, pc}
Leh_func_end64:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF
	.align	3
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF:
Leh_func_begin65:
	push	{r4, r7, lr}
Ltmp174:
	add	r7, sp, #4
Ltmp175:
Ltmp176:
	sub	sp, sp, #68
	bic	sp, sp, #7
	mov	r4, r0
	mov	r0, #0
	str	r1, [sp, #56]
	str	r2, [sp, #60]
	str	r0, [sp, #20]
	str	r0, [sp, #16]
	str	r0, [sp, #12]
	str	r0, [sp, #8]
	bl	_p_63_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #80]
	add	r1, sp, #8
	blx	r2
	bl	_p_64_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #92]
	blx	r1
	sub	r1, r0, #1
	cmp	r1, #4
	blo	LBB65_2
	vldr	d0, LCPI65_0
	vmov.f64	d1, d0
	b	LBB65_6
LBB65_2:
	sub	r0, r0, #3
	cmp	r0, #2
	bhs	LBB65_4
	vldr	s0, [sp, #20]
	vldr	s4, [sp, #56]
	vldr	s6, [sp, #16]
	vldr	s8, [sp, #60]
	vcvt.f64.f32	d1, s0
	vcvt.f64.f32	d5, s4
	vcvt.f64.f32	d2, s6
	vcvt.f64.f32	d3, s8
	vmov.f64	d0, #5.000000e-01
	vsub.f64	d4, d1, d5
	vmov.f64	d1, #-1.700000e+01
	b	LBB65_5
LBB65_4:
	vldr	s0, [sp, #16]
	vldr	s4, [sp, #56]
	vldr	s6, [sp, #20]
	vldr	s8, [sp, #60]
	vcvt.f64.f32	d1, s0
	vcvt.f64.f32	d5, s4
	vcvt.f64.f32	d2, s6
	vcvt.f64.f32	d3, s8
	vmov.f64	d0, #5.000000e-01
	vsub.f64	d4, d1, d5
	vmov.f64	d1, #-2.500000e+01
LBB65_5:
	vsub.f64	d2, d2, d3
	vmul.f64	d3, d4, d0
	vmla.f64	d1, d2, d0
	vcvt.f32.f64	s0, d3
	vcvt.f32.f64	s2, d1
	vcvt.f64.f32	d0, s0
	vcvt.f64.f32	d1, s2
LBB65_6:
	vldr	s4, [sp, #56]
	vldr	s6, [sp, #60]
	mov	r0, #0
	vcvt.f32.f64	s0, d0
	vcvt.f32.f64	s2, d1
	str	r0, [sp, #36]
	str	r0, [sp, #32]
	str	r0, [sp, #28]
	str	r0, [sp, #24]
	vmov	r1, s0
	vmov	r2, s2
	add	r0, sp, #24
	vmov	r3, s4
	vstr	s6, [sp]
	bl	_p_65_plt_System_Drawing_RectangleF__ctor_single_single_single_single_llvm
	add	r3, sp, #24
	add	r9, sp, #40
	ldm	r3, {r0, r1, r2, r3}
	stm	r9, {r0, r1, r2, r3}
	ldr	r0, [sp, #40]
	str	r0, [r4]
	ldr	r0, [sp, #44]
	str	r0, [r4, #4]
	ldr	r0, [sp, #48]
	str	r0, [r4, #8]
	ldr	r0, [sp, #52]
	str	r0, [r4, #12]
	sub	sp, r7, #4
	pop	{r4, r7, pc}
	.align	3
	.data_region
LCPI65_0:
	.long	0
	.long	0
	.end_data_region
Leh_func_end65:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int:
Leh_func_begin66:
	push	{r4, r5, r6, r7, lr}
Ltmp177:
	add	r7, sp, #12
Ltmp178:
	push	{r8}
Ltmp179:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #52]
	cmp	r0, #0
	beq	LBB66_3
	ldr	r0, [r4, #52]
	ldr	r2, [r0]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC66_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC66_1+8))
LPC66_1:
	add	r6, pc, r6
	ldr	r1, [r6, #172]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	cmp	r0, r5
	ble	LBB66_4
	ldr	r0, [r4, #52]
	ldr	r2, [r0]
	ldr	r1, [r6, #176]
	sub	r2, r2, #12
	mov	r8, r1
	mov	r1, r5
	ldr	r2, [r2]
	blx	r2
	mov	r1, r0
	mov	r0, r4
	bl	_p_66_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB66_3:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC66_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC66_0+8))
LPC66_0:
	add	r0, pc, r0
	ldr	r0, [r0, #180]
	b	LBB66_5
LBB66_4:
	ldr	r0, [r6, #180]
LBB66_5:
	ldr	r0, [r0]
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end66:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object:
Leh_func_begin67:
	push	{r4, r5, r6, r7, lr}
Ltmp180:
	add	r7, sp, #12
Ltmp181:
	push	{r8, r10}
Ltmp182:
	sub	sp, sp, #4
	ldr	r5, [r0, #56]
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC67_0+8))
	mov	r10, r1
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC67_0+8))
LPC67_0:
	add	r4, pc, r4
	ldr	r6, [r4, #184]
	bl	_p_67_plt_System_Globalization_CultureInfo_get_CurrentUICulture_llvm
	ldr	r2, [r5]
	ldr	r1, [r4, #188]
	mov	r3, #0
	sub	r2, r2, #28
	mov	r8, r1
	mov	r1, r10
	ldr	r4, [r2]
	str	r0, [sp]
	mov	r0, r5
	mov	r2, r6
	blx	r4
	ldr	r1, [r0]
	ldr	r1, [r1, #32]
	blx	r1
	sub	sp, r7, #20
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end67:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_UpdateDetailDisplay_MonoTouch_UIKit_UITableViewCell
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_UpdateDetailDisplay_MonoTouch_UIKit_UITableViewCell:
Leh_func_begin68:
	push	{r4, r5, r7, lr}
Ltmp183:
	add	r7, sp, #8
Ltmp184:
Ltmp185:
	mov	r5, r1
	mov	r4, r0
	cmp	r5, #0
	beq	LBB68_2
	ldr	r0, [r5]
	ldr	r1, [r0, #292]
	mov	r0, r5
	blx	r1
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r5]
	ldr	r1, [r0, #292]
	mov	r0, r5
	blx	r1
	ldr	r1, [r4, #44]
	mov	r5, r0
	mov	r0, r4
	bl	_p_66_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #308]
	mov	r0, r5
	blx	r2
LBB68_2:
	pop	{r4, r5, r7, pc}
Leh_func_end68:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__cctor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__cctor:
Leh_func_begin69:
	push	{r4, r5, r7, lr}
Ltmp186:
	add	r7, sp, #8
Ltmp187:
	push	{r10, r11}
Ltmp188:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC69_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC69_0+8))
LPC69_0:
	add	r5, pc, r5
	ldrd	r10, r11, [r5, #192]
	mov	r0, r11
	bl	_p_59_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r10
	mov	r4, r0
	bl	_p_68_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	ldr	r0, [r5, #164]
	str	r4, [r0]
	pop	{r10, r11}
	pop	{r4, r5, r7, pc}
Leh_func_end69:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo:
Leh_func_begin70:
	push	{r7, lr}
Ltmp189:
	mov	r7, sp
Ltmp190:
Ltmp191:
	cmp	r1, #0
	beq	LBB70_2
	ldr	r0, [r1]
	ldr	r2, [r0, #32]
	mov	r0, r1
	blx	r2
	pop	{r7, pc}
LBB70_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC70_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC70_0+8))
LPC70_0:
	add	r0, pc, r0
	ldr	r0, [r0, #180]
	ldr	r0, [r0]
	pop	{r7, pc}
Leh_func_end70:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter__ctor
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter__ctor:
Leh_func_begin71:
	bx	lr
Leh_func_end71:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement:
Leh_func_begin72:
	push	{r4, r5, r7, lr}
Ltmp192:
	add	r7, sp, #8
Ltmp193:
Ltmp194:
	mov	r4, r1
	mov	r5, r0
	bl	_p_69_plt_MonoTouch_UIKit_UIPickerViewModel__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end72:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView:
Leh_func_begin73:
	mov	r0, #1
	bx	lr
Leh_func_end73:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int:
Leh_func_begin74:
	push	{r7, lr}
Ltmp195:
	mov	r7, sp
Ltmp196:
	push	{r8}
Ltmp197:
	ldr	r1, [r0, #20]
	ldr	r2, [r1]
	ldr	r2, [r1, #52]
	mov	r1, #0
	cmp	r2, #0
	beq	LBB74_2
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC74_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC74_0+8))
LPC74_0:
	add	r1, pc, r1
	ldr	r1, [r1, #172]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r1, r0
LBB74_2:
	mov	r0, r1
	pop	{r8}
	pop	{r7, pc}
Leh_func_end74:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int:
Leh_func_begin75:
	push	{r7, lr}
Ltmp198:
	mov	r7, sp
Ltmp199:
Ltmp200:
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	mov	r1, r2
	bl	_p_70_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int_llvm
	cmp	r0, #0
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC75_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC75_0+8))
LPC75_0:
	add	r0, pc, r0
	ldr	r0, [r0, #180]
	ldr	r0, [r0]
	pop	{r7, pc}
Leh_func_end75:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentWidth_MonoTouch_UIKit_UIPickerView_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentWidth_MonoTouch_UIKit_UIPickerView_int:
Leh_func_begin76:
	movw	r0, #0
	movt	r0, #17302
	bx	lr
Leh_func_end76:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowHeight_MonoTouch_UIKit_UIPickerView_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowHeight_MonoTouch_UIKit_UIPickerView_int:
Leh_func_begin77:
	movw	r0, #0
	movt	r0, #16928
	bx	lr
Leh_func_end77:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int:
Leh_func_begin78:
	push	{r4, r7, lr}
Ltmp201:
	add	r7, sp, #4
Ltmp202:
	push	{r8}
Ltmp203:
	ldr	r4, [r0, #20]
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	ldr	r0, [r0, #52]
	ldr	r3, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC78_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC78_0+8))
LPC78_0:
	add	r1, pc, r1
	ldr	r1, [r1, #176]
	sub	r3, r3, #12
	ldr	r3, [r3]
	mov	r8, r1
	mov	r1, r2
	blx	r3
	mov	r1, r0
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_71_plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object_OnUserValueChanged_object_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end78:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement:
Leh_func_begin79:
	push	{r4, r5, r7, lr}
Ltmp204:
	add	r7, sp, #8
Ltmp205:
Ltmp206:
	mov	r4, r1
	mov	r5, r0
	bl	_p_72_plt_MonoTouch_UIKit_UIViewController__ctor_llvm
	str	r4, [r5, #48]
	pop	{r4, r5, r7, pc}
Leh_func_end79:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_get_Autorotate
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_get_Autorotate:
Leh_func_begin80:
	ldrb	r0, [r0, #52]
	bx	lr
Leh_func_end80:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_set_Autorotate_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_set_Autorotate_bool:
Leh_func_begin81:
	strb	r1, [r0, #52]
	bx	lr
Leh_func_end81:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_ShouldAutorotateToInterfaceOrientation_MonoTouch_UIKit_UIInterfaceOrientation
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_ShouldAutorotateToInterfaceOrientation_MonoTouch_UIKit_UIInterfaceOrientation:
Leh_func_begin82:
	ldrb	r0, [r0, #52]
	bx	lr
Leh_func_end82:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin83:
	push	{r4, r5, r6, r7, lr}
Ltmp207:
	add	r7, sp, #12
Ltmp208:
Ltmp209:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC83_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC83_0+8))
LPC83_0:
	add	r0, pc, r0
	ldr	r0, [r0, #200]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB83_2
	bl	_p_73_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB83_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB83_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB83_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB83_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB83_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end83:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool:
Leh_func_begin84:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end84:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value:
Leh_func_begin85:
	ldrb	r0, [r0, #8]
	bx	lr
Leh_func_end85:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool:
Leh_func_begin86:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end86:

	.private_extern	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
	.align	2
_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool:
Leh_func_begin87:
	push	{r4, r5, r6, r7, lr}
Ltmp210:
	add	r7, sp, #12
Ltmp211:
Ltmp212:
	mov	r5, r0
	mov	r6, r2
	mov	r4, r1
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC87_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got-(LPC87_0+8))
LPC87_0:
	add	r0, pc, r0
	ldr	r0, [r0, #204]
	bl	_p_57_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r2, r0
	mov	r0, r5
	mov	r1, r4
	strb	r6, [r2, #8]
	ldr	r3, [r5, #12]
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end87:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got,584,4
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillAppear_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidDisappear_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_Request
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_string
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogViewController__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_get_ValueConverterHolders
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_CreateApp
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_InitializeViewLookup
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_Initialize_System_Type__
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxEmptyApp__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_EntryElementOnChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_RootElementOnRadioSelectedChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_ValueElementOnValueChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_Entries
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_Entries_System_Collections_IList
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_DisplayValueConverter
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_DisplayValueConverter_Cirrious_CrossCore_Converters_IMvxValueConverter
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_BackgroundColor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_BackgroundColor_MonoTouch_UIKit_UIColor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__ctor_string_object_Cirrious_CrossCore_Converters_IMvxValueConverter
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_UpdateDetailDisplay_MonoTouch_UIKit_UITableViewCell
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentWidth_MonoTouch_UIKit_UIPickerView_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowHeight_MonoTouch_UIKit_UIPickerView_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_get_Autorotate
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_set_Autorotate_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_ShouldAutorotateToInterfaceOrientation_MonoTouch_UIKit_UIInterfaceOrientation
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	.no_dead_strip	_Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Dialog_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	88
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	21
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	22
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	23
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	24
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	25
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	26
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	27
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	28
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	29
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	30
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	31
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	32
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	33
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	34
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	35
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	36
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	37
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	38
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	39
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	40
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	41
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	42
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	43
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	44
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	45
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	46
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	47
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	48
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	49
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	50
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	51
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	52
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	53
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	54
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	55
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	56
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	57
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	58
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	59
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	60
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	61
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	62
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	63
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	65
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	66
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	67
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	69
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	70
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	71
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	72
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	73
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	74
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	75
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	76
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	77
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	78
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	79
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	80
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	82
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	83
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	84
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	88
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	90
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	91
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	92
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	93
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
Lset88 = Leh_func_end87-Leh_func_begin87
	.long	Lset88
Lset89 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset89
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0

Lmono_eh_func_begin29:
	.byte	0

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin32:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin33:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin34:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin38:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin39:
	.byte	0

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin43:
	.byte	0

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin45:
	.byte	0

Lmono_eh_func_begin46:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin49:
	.byte	0

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin51:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin56:
	.byte	0

Lmono_eh_func_begin57:
	.byte	0

Lmono_eh_func_begin58:
	.byte	0

Lmono_eh_func_begin59:
	.byte	0

Lmono_eh_func_begin60:
	.byte	0

Lmono_eh_func_begin61:
	.byte	0

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin67:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin68:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	5
	.byte	138
	.byte	6

Lmono_eh_func_begin70:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin71:
	.byte	0

Lmono_eh_func_begin72:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin73:
	.byte	0

Lmono_eh_func_begin74:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin75:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin76:
	.byte	0

Lmono_eh_func_begin77:
	.byte	0

Lmono_eh_func_begin78:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin80:
	.byte	0

Lmono_eh_func_begin81:
	.byte	0

Lmono_eh_func_begin82:
	.byte	0

Lmono_eh_func_begin83:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin84:
	.byte	0

Lmono_eh_func_begin85:
	.byte	0

Lmono_eh_func_begin86:
	.byte	0

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Dialog.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker
_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,56,208,77,226,13,176,160,225,24,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 204
	.byte 0,0,159,231,0,16,144,229,8,16,139,229,4,16,144,229,12,16,139,229,8,16,144,229,16,16,139,229,12,0,144,229
	.byte 20,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 200
	.byte 0,0,159,231
bl _p_59

	.byte 48,0,139,229,8,16,155,229,12,32,155,229,16,48,155,229,20,192,155,229,0,192,141,229
bl _p_75

	.byte 48,0,155,229,44,0,139,229,0,32,160,225,2,16,160,227,0,32,146,229,15,224,160,225,196,240,146,229,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 196
	.byte 0,0,159,231
bl _p_59

	.byte 40,0,139,229,24,16,155,229
bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement

	.byte 40,16,155,229,44,32,155,229,2,0,160,225,0,224,210,229,36,32,139,229
bl _p_62

	.byte 36,32,155,229,2,0,160,225,1,16,160,227,32,32,139,229,0,32,146,229,15,224,160,225,16,241,146,229,32,0,155,229
	.byte 56,208,139,226,0,9,189,232,128,128,189,232

Lme_40:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,72,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,44,32,139,229
	.byte 48,48,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 216
	.byte 0,0,159,231
bl _p_59

	.byte 64,0,139,229,6,16,160,225
bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement

	.byte 64,0,155,229,0,32,160,225,0,224,218,229,109,16,218,229,0,224,210,229,52,16,192,229,0,64,160,225,6,0,160,225
	.byte 0,16,150,229,15,224,160,225,100,240,145,229,48,0,134,229,60,0,139,229,48,192,150,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 212
	.byte 0,0,159,231,0,16,144,229,12,16,139,229,4,0,144,229,16,0,139,229,20,16,139,226,12,0,160,225,12,32,155,229
	.byte 16,48,155,229,0,192,156,229,15,224,160,225,140,240,156,229,28,0,139,226,20,16,155,229,24,32,155,229
bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF

	.byte 60,192,155,229,12,0,160,225,56,0,139,229,28,16,155,229,32,32,155,229,36,48,155,229,40,0,155,229,0,0,141,229
	.byte 56,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,52,0,150,229,0,0,80,227,0,0,160,19,1,0,160,3
	.byte 8,0,203,229,0,0,80,227,25,0,0,26,52,32,150,229,44,16,150,229,2,0,160,225,0,32,146,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 208
	.byte 8,128,159,231,4,224,143,226,32,240,18,229,0,0,0,0,0,80,160,225,0,0,80,227,0,0,160,227,1,0,160,179
	.byte 8,0,203,229,0,0,80,227,7,0,0,26,48,192,150,229,12,0,160,225,5,16,160,225,0,32,160,227,1,48,160,227
	.byte 0,192,156,229,15,224,160,225,8,241,156,229,4,0,160,225,0,16,148,229,15,224,160,225,180,240,145,229,0,32,160,225
	.byte 60,16,150,229,2,0,160,225,0,32,146,229,15,224,160,225,0,241,146,229,4,0,160,225,0,16,148,229,15,224,160,225
	.byte 180,240,145,229,0,32,160,225,48,16,150,229,2,0,160,225,0,32,146,229,15,224,160,225,148,240,146,229,10,0,160,225
	.byte 4,16,160,225,0,32,154,229,15,224,160,225,248,240,146,229,72,208,139,226,112,13,189,232,128,128,189,232

Lme_44:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation
_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation:

	.byte 128,64,45,233,13,112,160,225,64,9,45,233,60,208,77,226,13,176,160,225,0,96,160,225,40,16,139,229,6,0,160,225
	.byte 40,16,155,229
bl _p_78

	.byte 48,0,150,229,48,0,144,229,52,0,139,229,48,0,150,229,48,192,144,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 212
	.byte 0,0,159,231,0,16,144,229,8,16,139,229,4,0,144,229,12,0,139,229,16,16,139,226,12,0,160,225,8,32,155,229
	.byte 12,48,155,229,0,192,156,229,15,224,160,225,140,240,156,229,24,0,139,226,16,16,155,229,20,32,155,229
bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF

	.byte 52,192,155,229,12,0,160,225,48,0,139,229,24,16,155,229,28,32,155,229,32,48,155,229,36,0,155,229,0,0,141,229
	.byte 48,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,60,208,139,226,64,9,189,232,128,128,189,232

Lme_51:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string
_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string:

	.byte 128,64,45,233,13,112,160,225,16,9,45,233,36,208,77,226,13,176,160,225,4,128,139,229,0,16,139,229,12,0,139,229
	.byte 16,32,139,229,20,48,139,229,4,0,155,229
bl _p_81

	.byte 0,64,160,225,0,0,148,229,7,0,128,226,7,0,192,227,0,208,77,224,0,0,141,226,8,0,139,229,4,0,155,229
bl _p_80

	.byte 24,0,139,229,4,0,155,229
bl _p_79

	.byte 0,192,160,225,24,0,155,229,0,128,160,225,8,16,155,229,12,32,148,229,8,0,155,229,2,0,128,224,16,16,155,229
	.byte 12,32,155,229,20,48,155,229,60,255,47,225,8,0,155,229,12,16,148,229,8,0,155,229,1,16,128,224,0,0,155,229
	.byte 4,32,148,229,8,48,148,229,51,255,47,225,36,208,139,226,16,9,189,232,128,128,189,232

Lme_56:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:

	.byte 128,64,45,233,13,112,160,225,16,9,45,233,36,208,77,226,13,176,160,225,4,128,139,229,0,16,139,229,12,0,139,229
	.byte 16,32,139,229,20,48,139,229,4,0,155,229
bl _p_84

	.byte 0,64,160,225,0,0,148,229,7,0,128,226,7,0,192,227,0,208,77,224,0,0,141,226,8,0,139,229,4,0,155,229
bl _p_83

	.byte 24,0,139,229,4,0,155,229
bl _p_82

	.byte 0,192,160,225,24,0,155,229,0,128,160,225,8,16,155,229,12,32,148,229,8,0,155,229,2,0,128,224,16,16,155,229
	.byte 12,32,155,229,20,48,155,229,60,255,47,225,8,0,155,229,12,16,148,229,8,0,155,229,1,16,128,224,0,0,155,229
	.byte 4,32,148,229,8,48,148,229,51,255,47,225,36,208,139,226,16,9,189,232,128,128,189,232

Lme_57:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_87

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_85

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_86

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_85
bl _p_59

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_59:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillAppear_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidDisappear_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_Request
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_string
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogViewController__ctor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_get_ValueConverterHolders
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_CreateApp
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_InitializeViewLookup
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_Initialize_System_Type__
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxEmptyApp__ctor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_EntryElementOnChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_RootElementOnRadioSelectedChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_ValueElementOnValueChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_Entries
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_Entries_System_Collections_IList
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_DisplayValueConverter
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_DisplayValueConverter_Cirrious_CrossCore_Converters_IMvxValueConverter
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_BackgroundColor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_BackgroundColor_MonoTouch_UIKit_UIColor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__ctor_string_object_Cirrious_CrossCore_Converters_IMvxValueConverter
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetCellImpl_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_UpdateDetailDisplay_MonoTouch_UIKit_UITableViewCell
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__cctor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter__ctor
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentWidth_MonoTouch_UIKit_UIPickerView_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowHeight_MonoTouch_UIKit_UIPickerView_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_get_Autorotate
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_set_Autorotate_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_ShouldAutorotateToInterfaceOrientation_MonoTouch_UIKit_UIInterfaceOrientation
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
.no_dead_strip _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillAppear_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidDisappear_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_Dispose_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_add_DisposeCalled_System_EventHandler
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_remove_DisposeCalled_System_EventHandler
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_Request
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_BindingContext
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_string
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind_T_T_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogViewController__ctor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_get_ValueConverterHolders
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_CreateApp
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_InitializeViewLookup
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup_Initialize_System_Type__
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Simple_MvxEmptyApp__ctor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_EntryElementOnChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxEntryElementValueBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_RootElementOnRadioSelectedChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxRadioRootElementBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_ValueElementOnValueChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Target_MvxValueElementValueBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_Entries
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_Entries_System_Collections_IList
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_DisplayValueConverter
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_DisplayValueConverter_Cirrious_CrossCore_Converters_IMvxValueConverter
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_get_BackgroundColor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_set_BackgroundColor_MonoTouch_UIKit_UIColor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__ctor_string_object_Cirrious_CrossCore_Converters_IMvxValueConverter
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetCellImpl_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Dispose_bool
	bl _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object
	bl _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_UpdateDetailDisplay_MonoTouch_UIKit_UITableViewCell
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement__cctor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ToStringDisplayValueConverter__ctor
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetComponentWidth_MonoTouch_UIKit_UIPickerView_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_GetRowHeight_MonoTouch_UIKit_UIPickerView_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
	bl _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_get_Autorotate
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_set_Autorotate_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_ShouldAutorotateToInterfaceOrientation_MonoTouch_UIKit_UIInterfaceOrientation
	bl method_addresses
	bl _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string
	bl _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	bl _Cirrious_MvvmCross_Dialog_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	bl _Cirrious_MvvmCross_Dialog_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 94,10,10,2
	.short 0, 10, 20, 30, 40, 51, 62, 73
	.short 84, 100
	.byte 1,2,2,3,3,3,3,2,2,4,29,4,4,4,4,4,4,4,4,4,69,2,2,3,3,4,2,2,2,2,93,2
	.byte 2,3,2,2,17,5,3,2,128,134,2,3,2,8,2,2,6,8,2,128,172,6,8,2,2,6,3,3,3,3,128,211
	.byte 3,4,6,3,6,3,7,5,6,129,1,6,3,2,2,2,3,3,2,2,129,29,2,3,2,2,255,255,255,254,218,129
	.byte 40,2,2,3,129,49,2,2,2
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,0,0,0,353,88,0,0
	.long 0,0,0,0,0,378,89,19
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,444
	.long 92,0,336,87,20,0,0,0
	.long 0,0,0,0,0,0,319,86
	.long 0,0,0,0,0,0,0,0
	.long 0,0,406,90,0,425,91,21
	.long 463,93,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 8,86,319,87,336,88,353,89
	.long 378,90,406,91,425,92,444,93
	.long 463
.section __TEXT, __const
	.align 3
class_name_table:

	.short 37, 10, 0, 0, 0, 0, 0, 8
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 4, 0, 0
	.short 0, 3, 37, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 13, 0, 0
	.short 0, 7, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 11, 0, 0, 0, 0
	.short 0, 12, 0, 6, 0, 5, 0, 1
	.short 0, 9, 0, 0, 0, 0, 0, 2
	.short 0, 0, 0, 14, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 58,10,6,2
	.short 0, 11, 22, 33, 44, 55
	.byte 129,218,2,1,1,1,12,12,5,12,6,130,19,5,4,4,7,7,3,5,5,7,130,73,7,7,3,5,12,3,3,5
	.byte 2,130,122,5,3,7,2,2,4,5,2,2,130,158,3,4,5,5,5,7,7,5,4,130,207,1,4,3,5,6,5,6
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 94,10,10,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 104
	.byte 135,11,3,3,3,3,3,3,3,3,3,135,41,3,3,3,3,3,3,3,3,3,135,71,3,3,3,3,3,3,3,3
	.byte 3,135,101,23,23,3,3,3,3,3,3,3,135,171,3,3,3,3,3,3,3,3,3,135,201,3,3,3,3,3,3,3
	.byte 3,3,135,231,3,3,3,3,3,3,3,3,3,136,5,3,3,3,3,3,3,3,3,3,136,35,3,3,3,3,255,255
	.byte 255,247,209,136,50,29,29,3,136,142,3,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,72,68,13,11,31,12,13,0,72,14,8,135
	.byte 2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,104,68,13,11,25,12,13,0,72,14,8,135
	.byte 2,68,14,20,134,5,136,4,139,3,142,1,68,14,80,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,132,5
	.byte 136,4,139,3,142,1,68,14,56,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14
	.byte 48,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 14,10,2,2
	.short 0, 16
	.byte 136,154,7,129,10,129,18,129,18,128,238,128,232,47,82,82,142,202,5,33,61

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Dialog_Touch_plt:
_p_1_plt_CrossUI_Touch_Dialog_DialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_DialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
plt_CrossUI_Touch_Dialog_DialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 232,752
_p_2_plt_CrossUI_Touch_Dialog_DialogViewController__ctor_intptr_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_DialogViewController__ctor_intptr
plt_CrossUI_Touch_Dialog_DialogViewController__ctor_intptr:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 236,757
_p_3_plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillDisappear_bool_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillDisappear_bool
plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillDisappear_bool:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 240,762
_p_4_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 244,767
_p_5_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool
plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 248,779
_p_6_plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillAppear_bool_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillAppear_bool
plt_CrossUI_Touch_Dialog_DialogViewController_ViewWillAppear_bool:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 252,784
_p_7_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool
plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 256,789
_p_8_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidLoad
plt_MonoTouch_UIKit_UIViewController_ViewDidLoad:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 260,794
_p_9_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object
plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 264,799
_p_10_plt_MonoTouch_UIKit_UITableViewController_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewController_Dispose_bool
plt_MonoTouch_UIKit_UITableViewController_Dispose_bool:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 268,804
_p_11_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 272,809
_p_12_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 276,814
_p_13_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 280,859
_p_14_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Remove_System_Delegate_System_Delegate
plt_System_Delegate_Remove_System_Delegate_System_Delegate:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 284,871
_p_15_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 288,876

.set _p_16_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
_p_17_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 296,890

.set _p_18_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_intptr
_p_19_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext
plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_DataContext:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 304,897
_p_20_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object
plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_DataContext_object:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 308,899
_p_21_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 312,928
_p_22_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 316,951
_p_23_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 320,997
_p_24_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_T_T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 324,1020

.set _p_25_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
_p_26_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 332,1041
_p_27_plt_Cirrious_MvvmCross_ViewModels_MvxNullViewModel__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNullViewModel__ctor
plt_Cirrious_MvvmCross_ViewModels_MvxNullViewModel__ctor:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 336,1064

.set _p_28_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
_p_29_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 344,1071
_p_30_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 348,1076
_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxSimplePropertyInfoTargetBindingFactory__ctor_System_Type_System_Type_string:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 352,1081

.set _p_32_plt_Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_MvxTouchDialogSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
_p_33_plt_System_Linq_Enumerable_ToList_System_Type_System_Collections_Generic_IEnumerable_1_System_Type_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_ToList_System_Type_System_Collections_Generic_IEnumerable_1_System_Type
plt_System_Linq_Enumerable_ToList_System_Type_System_Collections_Generic_IEnumerable_1_System_Type:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 360,1088
_p_34_plt_Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type___llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__
plt_Cirrious_MvvmCross_Dialog_Touch_Simple_MvxSimpleTouchDialogSetup__ctor_System_Type__:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 364,1100
_p_35_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_object_System_Reflection_PropertyInfo:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 368,1102
_p_36_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_EntryElement_get_View:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 372,1113
_p_37_plt_CrossUI_Touch_Dialog_Elements_EntryElement_add_Changed_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement_add_Changed_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_EntryElement_add_Changed_System_EventHandler:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 376,1124
_p_38_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 380,1129
_p_39_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__
plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 384,1155
_p_40_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 388,1160
_p_41_plt_CrossUI_Touch_Dialog_Elements_EntryElement_remove_Changed_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement_remove_Changed_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_EntryElement_remove_Changed_System_EventHandler:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 392,1165
_p_42_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement__ctor_object_System_Reflection_PropertyInfo:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 396,1170
_p_43_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_RootElement_get_View:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 400,1181
_p_44_plt_CrossUI_Touch_Dialog_Elements_RootElement_add_RadioSelectedChanged_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RootElement_add_RadioSelectedChanged_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_RootElement_add_RadioSelectedChanged_System_EventHandler:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 404,1192
_p_45_plt_CrossUI_Touch_Dialog_Elements_RootElement_get_RadioSelected_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RootElement_get_RadioSelected
plt_CrossUI_Touch_Dialog_Elements_RootElement_get_RadioSelected:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 408,1197
_p_46_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 412,1202
_p_47_plt_CrossUI_Touch_Dialog_Elements_RootElement_remove_RadioSelectedChanged_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RootElement_remove_RadioSelectedChanged_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_RootElement_remove_RadioSelectedChanged_System_EventHandler:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 416,1232
_p_48_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement__ctor_object_System_Reflection_PropertyInfo:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 420,1237
_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_CrossUI_Touch_Dialog_Elements_ValueElement_get_View:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 424,1248
_p_50_plt_CrossUI_Touch_Dialog_Elements_ValueElement_add_ValueChanged_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_ValueElement_add_ValueChanged_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_ValueElement_add_ValueChanged_System_EventHandler:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 428,1259
_p_51_plt_CrossUI_Touch_Dialog_Elements_ValueElement_remove_ValueChanged_System_EventHandler_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_ValueElement_remove_ValueChanged_System_EventHandler
plt_CrossUI_Touch_Dialog_Elements_ValueElement_remove_ValueChanged_System_EventHandler:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 432,1264
_p_52_plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object__ctor_string_object_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object__ctor_string_object
plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object__ctor_string_object:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 436,1269
_p_53_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice
plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 440,1280
_p_54_plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int
plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 444,1285
_p_55_plt_MonoTouch_UIKit_UIColor_get_White_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_White
plt_MonoTouch_UIKit_UIColor_get_White:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 448,1290
_p_56_plt_MonoTouch_UIKit_UIColor_get_Black_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_Black
plt_MonoTouch_UIKit_UIColor_get_Black:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 452,1295
_p_57_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 456,1300
_p_58_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 460,1326
_p_59_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 464,1331
_p_60_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 468,1358
_p_61_plt_MonoTouch_Foundation_NSObject_Dispose_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSObject_Dispose
plt_MonoTouch_Foundation_NSObject_Dispose:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 472,1363
_p_62_plt_MonoTouch_UIKit_UIPickerView_set_Model_MonoTouch_UIKit_UIPickerViewModel_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIPickerView_set_Model_MonoTouch_UIKit_UIPickerViewModel
plt_MonoTouch_UIKit_UIPickerView_set_Model_MonoTouch_UIKit_UIPickerViewModel:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 476,1368
_p_63_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIScreen_get_MainScreen
plt_MonoTouch_UIKit_UIScreen_get_MainScreen:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 480,1373
_p_64_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplication_get_SharedApplication
plt_MonoTouch_UIKit_UIApplication_get_SharedApplication:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 484,1378
_p_65_plt_System_Drawing_RectangleF__ctor_single_single_single_single_llvm:
	.no_dead_strip plt_System_Drawing_RectangleF__ctor_single_single_single_single
plt_System_Drawing_RectangleF__ctor_single_single_single_single:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 488,1383
_p_66_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object
plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_ConvertToString_object:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 492,1388
_p_67_plt_System_Globalization_CultureInfo_get_CurrentUICulture_llvm:
	.no_dead_strip plt_System_Globalization_CultureInfo_get_CurrentUICulture
plt_System_Globalization_CultureInfo_get_CurrentUICulture:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 496,1390
_p_68_plt_MonoTouch_Foundation_NSString__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSString__ctor_string
plt_MonoTouch_Foundation_NSString__ctor_string:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 500,1395
_p_69_plt_MonoTouch_UIKit_UIPickerViewModel__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIPickerViewModel__ctor
plt_MonoTouch_UIKit_UIPickerViewModel__ctor:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 504,1400
_p_70_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int
plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_GetString_int:
_p_70:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 508,1405
_p_71_plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object_OnUserValueChanged_object_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object_OnUserValueChanged_object
plt_CrossUI_Touch_Dialog_Elements_ValueElement_1_object_OnUserValueChanged_object:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 512,1407
_p_72_plt_MonoTouch_UIKit_UIViewController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController__ctor
plt_MonoTouch_UIKit_UIViewController__ctor:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 516,1418
_p_73_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 520,1423

.set _p_74_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewModel__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
_p_75_plt_MonoTouch_UIKit_UIPickerView__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIPickerView__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIPickerView__ctor_System_Drawing_RectangleF:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 528,1463

.set _p_76_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_PickerFrameWithSize_System_Drawing_SizeF

.set _p_77_plt_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_llvm, _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController__ctor_Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement
_p_78_plt_MonoTouch_UIKit_UIViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation
plt_MonoTouch_UIKit_UIViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 540,1472
_p_79_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 544,1494
_p_80_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_80:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 548,1535
_p_81_plt__rgctx_fetch_4_llvm:
	.no_dead_strip plt__rgctx_fetch_4
plt__rgctx_fetch_4:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 552,1558
_p_82_plt__rgctx_fetch_5_llvm:
	.no_dead_strip plt__rgctx_fetch_5
plt__rgctx_fetch_5:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 556,1612
_p_83_plt__rgctx_fetch_6_llvm:
	.no_dead_strip plt__rgctx_fetch_6
plt__rgctx_fetch_6:
_p_83:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 560,1661
_p_84_plt__rgctx_fetch_7_llvm:
	.no_dead_strip plt__rgctx_fetch_7
plt__rgctx_fetch_7:
_p_84:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 564,1684
_p_85_plt__rgctx_fetch_8_llvm:
	.no_dead_strip plt__rgctx_fetch_8
plt__rgctx_fetch_8:
_p_85:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 568,1749
_p_86_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 572,1757
_p_87_plt__rgctx_fetch_9_llvm:
	.no_dead_strip plt__rgctx_fetch_9
plt__rgctx_fetch_9:
_p_87:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got - . + 576,1776
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 9
	.asciz "Cirrious.MvvmCross.Dialog.Touch"
	.asciz "BFC6638D-69D4-485A-A95A-5591B6B6D2B5"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross"
	.asciz "066A9949-60EF-4499-87FB-90DB7F8EAEA9"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Binding"
	.asciz "0CA1A6F2-0F7E-44D7-B6B8-AE8E0BEDDDE8"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "CrossUI.Touch"
	.asciz "327FA55C-F955-41C6-AADD-FF30B004E740"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.MvvmCross.Touch"
	.asciz "1E871AF4-634E-4A45-AB5B-DCAE1A4CA1E3"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "BFC6638D-69D4-485A-A95A-5591B6B6D2B5"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Dialog.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Dialog_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch__Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 58,584,88,94,11,387000831,0,4042
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Dialog_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Dialog_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,1,4,0,1,4,0,1,4,0,1,4,0,0,0,0,0,2,6,5,0,2,6,5,0,2,8
	.byte 7,0,2,8,7,0,2,8,7,0,2,8,7,0,2,8,7,0,2,8,7,0,2,8,7,0,2,8,7,0,2,6
	.byte 5,0,2,6,5,0,0,0,0,0,1,9,0,1,10,0,2,11,11,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 0,0,1,12,0,0,0,0,0,15,13,16,22,21,20,17,16,15,19,18,17,16,15,14,17,0,3,23,15,19,0,1
	.byte 24,0,0,0,1,25,0,0,0,1,26,0,0,0,6,32,31,30,29,28,27,0,0,0,0,0,4,30,29,28,27,0
	.byte 6,32,35,30,34,33,27,0,0,0,1,36,0,4,30,34,33,27,0,6,32,39,30,38,37,27,0,0,0,0,0,4
	.byte 30,38,37,27,1,11,0,1,11,0,1,11,0,1,11,0,1,11,0,1,11,0,1,11,1,40,1,11,3,42,41,41
	.byte 1,11,0,1,11,3,54,53,52,1,11,0,1,11,4,45,45,44,43,1,11,2,47,46,1,11,3,57,56,55,1,11
	.byte 0,1,11,3,41,49,48,0,1,45,0,0,0,0,0,0,0,1,43,0,1,45,0,0,0,0,0,1,44,0,0,0
	.byte 1,56,0,0,0,0,0,0,0,0,0,0,0,1,50,0,0,0,0,0,0,0,0,0,1,51,5,30,0,0,1,255
	.byte 253,0,0,0,1,3,0,198,0,0,31,0,1,7,129,58,255,253,0,0,0,1,3,0,198,0,0,32,0,1,7,129
	.byte 58,255,252,0,0,0,1,1,3,219,0,0,5,5,30,0,1,255,255,255,255,255,193,0,13,43,255,253,0,0,0,2
	.byte 130,10,1,1,198,0,13,43,0,1,7,129,109,4,2,29,2,1,2,130,25,1,255,253,0,0,0,7,129,141,2,198
	.byte 0,0,73,1,2,130,25,1,0,255,253,0,0,0,7,129,141,2,198,0,0,74,1,2,130,25,1,0,255,253,0,0
	.byte 0,7,129,141,2,198,0,0,75,1,2,130,25,1,0,255,254,0,0,0,0,255,43,0,0,1,12,0,39,42,47,34
	.byte 255,254,0,0,0,0,255,43,0,0,1,34,255,254,0,0,0,0,255,43,0,0,2,11,2,130,63,1,34,255,254,0
	.byte 0,0,0,255,43,0,0,3,11,3,219,0,0,5,6,194,0,1,138,6,194,0,1,139,23,2,93,3,14,2,108,3
	.byte 19,0,194,0,0,8,0,19,0,193,0,0,12,0,17,0,1,14,2,128,160,4,6,196,0,3,170,19,0,194,0,0
	.byte 10,0,19,0,193,0,0,14,0,19,0,194,0,0,9,0,19,0,193,0,0,13,0,17,0,13,6,196,0,1,170,34
	.byte 255,254,0,0,0,0,255,43,0,0,6,14,1,7,14,1,6,14,2,130,63,1,6,46,50,46,30,2,130,63,1,17
	.byte 0,41,14,6,1,2,130,123,1,6,50,50,50,17,0,128,161,14,2,130,90,1,6,53,50,53,17,0,129,21,14,1
	.byte 12,16,1,11,10,14,2,128,210,6,6,193,0,4,243,6,193,0,5,15,16,2,130,146,1,136,199,19,0,193,0,0
	.byte 83,0,6,194,0,0,8,17,0,129,141,14,2,29,6,33,14,7,129,141,14,1,13,14,2,128,200,6,16,2,102,6
	.byte 129,53,6,193,0,5,20,16,2,103,6,129,56,14,1,14,3,199,0,0,63,3,199,0,0,59,3,199,0,0,58,3
	.byte 255,254,0,0,0,0,255,43,0,0,1,3,198,0,6,30,3,199,0,0,49,3,198,0,6,32,3,198,0,6,28,3
	.byte 194,0,0,4,3,198,0,7,195,3,193,0,16,63,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105
	.byte 98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,3,255,254,0,0
	.byte 0,0,255,43,0,0,2,3,193,0,16,65,3,255,254,0,0,0,0,255,43,0,0,3,3,1,3,200,0,0,26,3
	.byte 2,3,23,3,24,5,30,0,1,255,255,255,255,255,31,255,253,0,0,0,1,3,0,198,0,0,31,0,1,7,131,133
	.byte 35,131,143,140,17,255,253,0,0,0,2,76,4,4,198,0,1,204,0,1,7,131,133,3,255,253,0,0,0,2,76,4
	.byte 4,198,0,1,204,0,1,7,131,133,5,30,0,1,255,255,255,255,255,32,255,253,0,0,0,1,3,0,198,0,0,32
	.byte 0,1,7,131,202,35,131,212,140,17,255,253,0,0,0,2,76,4,4,198,0,1,205,0,1,7,131,202,3,255,253,0
	.byte 0,0,2,76,4,4,198,0,1,205,0,1,7,131,202,3,21,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110
	.byte 101,119,95,102,97,115,116,0,3,195,0,1,168,3,26,3,200,0,0,126,3,200,0,0,125,3,196,0,3,166,3,34
	.byte 3,255,254,0,0,0,0,255,43,0,0,6,3,38,3,255,254,0,0,0,0,202,0,0,36,3,255,254,0,0,0,0
	.byte 202,0,0,37,3,199,0,1,9,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102
	.byte 105,99,0,3,196,0,3,249,3,196,0,2,202,3,199,0,1,10,3,255,254,0,0,0,0,202,0,0,45,3,255,254
	.byte 0,0,0,0,202,0,0,46,3,199,0,1,228,3,199,0,1,226,7,27,109,111,110,111,95,111,98,106,101,99,116,95
	.byte 110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,199,0,1,229,3,255,254,0,0,0,0,202,0,0,50
	.byte 3,255,254,0,0,0,0,202,0,0,51,3,199,0,0,151,3,199,0,0,152,3,255,254,0,0,0,0,202,0,0,55
	.byte 3,198,0,4,148,3,198,0,4,151,3,198,0,4,105,3,198,0,4,102,7,23,109,111,110,111,95,111,98,106,101,99
	.byte 116,95,110,101,119,95,112,116,114,102,114,101,101,0,3,198,0,5,96,7,24,109,111,110,111,95,111,98,106,101,99,116
	.byte 95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,198,0,5,108,3,198,0,2,9,3,198,0,4,247,3,198,0
	.byte 5,35,3,198,0,4,14,3,198,0,2,170,3,68,3,193,0,5,217,3,198,0,1,12,3,198,0,5,9,3,67,3
	.byte 255,254,0,0,0,0,202,0,0,103,3,198,0,6,11,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116
	.byte 101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,74,3,198,0,4,250,3,66,3,81
	.byte 3,198,0,6,35,255,253,0,0,0,1,3,0,198,0,0,31,0,1,7,129,58,35,133,197,192,0,90,33,16,1,3
	.byte 30,7,129,58,30,7,129,58,18,2,120,4,14,255,253,0,0,0,2,76,4,4,198,0,1,204,0,1,7,129,58,35
	.byte 133,197,140,17,255,253,0,0,0,2,76,4,4,198,0,1,204,0,1,7,129,58,35,133,197,192,0,92,41,255,253,0
	.byte 0,0,1,3,0,198,0,0,31,0,1,7,129,58,3,14,7,129,58,22,7,129,58,21,7,129,58,255,253,0,0,0
	.byte 1,3,0,198,0,0,32,0,1,7,129,58,35,134,59,192,0,90,33,16,1,3,30,7,129,58,30,7,129,58,18,2
	.byte 120,4,21,2,111,1,1,2,128,173,4,255,253,0,0,0,2,76,4,4,198,0,1,205,0,1,7,129,58,35,134,59
	.byte 140,17,255,253,0,0,0,2,76,4,4,198,0,1,205,0,1,7,129,58,35,134,59,192,0,92,41,255,253,0,0,0
	.byte 1,3,0,198,0,0,32,0,1,7,129,58,3,14,7,129,58,22,7,129,58,21,7,129,58,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,43,0,1,7,129,109,4,2,130,11,1,1,7,129,109,35,134,185,150,5,7,134,204,3,255,253
	.byte 0,0,0,7,134,204,1,198,0,13,121,1,7,129,109,0,35,134,185,192,0,92,41,255,253,0,0,0,2,130,10,1
	.byte 1,198,0,13,43,0,1,7,129,109,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,17,0,0,17,255,253,0,0,0,1,3,0,198,0,0,31,0,1,7,131,133,0,0,17,0,0,17
	.byte 255,253,0,0,0,1,3,0,198,0,0,32,0,1,7,131,202,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,0,0,16,0,0,16,0,0,16,0,0,2
	.byte 24,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,2,56,0,16,0,0,16,0,0,16,0,0,3,82,0,1,11,4,17,255,253,0,0,0,1,3
	.byte 0,198,0,0,31,0,1,7,129,58,1,0,1,1,0,3,82,0,1,11,4,17,255,253,0,0,0,1,3,0,198,0
	.byte 0,32,0,1,7,129,58,1,0,1,1,0,16,0,0,3,108,0,1,11,4,19,255,253,0,0,0,2,130,10,1,1
	.byte 198,0,13,43,0,1,7,129,109,1,0,1,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0
	.byte 1,77,128,162,198,0,2,8,128,136,0,0,4,198,0,2,18,193,0,18,175,198,0,2,8,193,0,18,172,198,0,2
	.byte 4,198,0,2,9,8,198,0,2,13,198,0,2,12,198,0,2,7,198,0,7,190,198,0,5,28,198,0,5,27,198,0
	.byte 5,26,198,0,6,26,198,0,6,40,198,0,6,39,198,0,6,38,198,0,6,37,198,0,6,36,199,0,0,29,199,0
	.byte 0,28,198,0,6,33,6,3,4,5,7,199,0,0,46,198,0,6,23,198,0,6,22,198,0,6,21,198,0,6,20,198
	.byte 0,6,19,198,0,6,18,198,0,6,17,198,0,6,16,198,0,6,15,198,0,7,194,199,0,0,55,199,0,0,54,199
	.byte 0,0,53,199,0,0,52,199,0,0,51,199,0,0,50,199,0,0,48,199,0,0,47,199,0,0,45,199,0,0,44,199
	.byte 0,0,43,199,0,0,42,199,0,0,41,199,0,0,40,199,0,0,39,199,0,0,38,199,0,0,37,199,0,0,36,199
	.byte 0,0,34,199,0,0,31,199,0,0,24,199,0,0,23,199,0,0,18,199,0,0,17,199,0,0,14,199,0,0,13,9
	.byte 10,11,12,13,14,15,16,17,18,19,20,85,128,162,198,0,2,8,128,144,0,0,4,198,0,2,18,193,0,18,175,198
	.byte 0,2,8,193,0,18,172,198,0,2,4,198,0,2,9,8,198,0,2,13,198,0,2,12,198,0,2,7,198,0,7,190
	.byte 198,0,5,28,198,0,5,27,198,0,5,26,198,0,6,26,198,0,6,40,198,0,6,39,198,0,6,38,198,0,6,37
	.byte 198,0,6,36,199,0,0,29,199,0,0,28,198,0,6,33,6,3,4,5,7,199,0,0,46,198,0,6,23,198,0,6
	.byte 22,198,0,6,21,198,0,6,20,198,0,6,19,198,0,6,18,198,0,6,17,198,0,6,16,198,0,6,15,198,0,7
	.byte 194,199,0,0,55,199,0,0,54,199,0,0,53,199,0,0,52,199,0,0,51,199,0,0,50,199,0,0,48,199,0,0
	.byte 47,199,0,0,45,199,0,0,44,199,0,0,43,199,0,0,42,199,0,0,41,199,0,0,40,199,0,0,39,199,0,0
	.byte 38,199,0,0,37,199,0,0,36,199,0,0,34,199,0,0,31,199,0,0,24,199,0,0,23,199,0,0,18,199,0,0
	.byte 17,199,0,0,14,199,0,0,13,9,10,11,12,13,14,15,16,17,18,19,20,27,28,25,26,23,24,29,30,85,128,162
	.byte 198,0,2,8,128,144,0,0,4,198,0,2,18,193,0,18,175,198,0,2,8,193,0,18,172,198,0,2,4,198,0,2
	.byte 9,8,198,0,2,13,198,0,2,12,198,0,2,7,198,0,7,190,198,0,5,28,198,0,5,27,198,0,5,26,198,0
	.byte 6,26,198,0,6,40,198,0,6,39,198,0,6,38,198,0,6,37,198,0,6,36,199,0,0,29,199,0,0,28,198,0
	.byte 6,33,6,3,4,5,7,199,0,0,46,198,0,6,23,198,0,6,22,198,0,6,21,198,0,6,20,198,0,6,19,198
	.byte 0,6,18,198,0,6,17,198,0,6,16,198,0,6,15,198,0,7,194,199,0,0,55,199,0,0,54,199,0,0,53,199
	.byte 0,0,52,199,0,0,51,199,0,0,50,199,0,0,48,199,0,0,47,199,0,0,45,199,0,0,44,199,0,0,43,199
	.byte 0,0,42,199,0,0,41,199,0,0,40,199,0,0,39,199,0,0,38,199,0,0,37,199,0,0,36,199,0,0,34,199
	.byte 0,0,31,199,0,0,24,199,0,0,23,199,0,0,18,199,0,0,17,199,0,0,14,199,0,0,13,9,10,11,12,13
	.byte 14,15,16,17,18,19,20,27,28,25,26,23,24,29,30,60,128,160,28,0,0,4,193,0,18,178,193,0,18,175,193,0
	.byte 18,174,193,0,18,172,195,0,1,129,200,0,0,143,195,0,1,120,195,0,1,119,200,0,0,152,195,0,1,117,195,0
	.byte 1,116,195,0,1,115,195,0,1,114,195,0,1,113,195,0,1,112,195,0,1,111,195,0,1,110,195,0,1,109,200,0
	.byte 0,130,195,0,1,107,195,0,1,106,195,0,1,105,200,0,0,135,195,0,1,103,195,0,1,102,195,0,1,101,195,0
	.byte 1,100,195,0,1,99,195,0,1,98,195,0,1,97,195,0,1,96,195,0,1,95,195,0,1,94,195,0,1,93,195,0
	.byte 1,92,195,0,1,91,195,0,1,90,195,0,1,89,195,0,1,88,195,0,1,87,200,0,0,134,200,0,0,132,0,200
	.byte 0,0,129,36,200,0,0,150,200,0,0,149,200,0,0,148,37,200,0,0,146,200,0,0,145,200,0,0,144,200,0,0
	.byte 142,200,0,0,141,200,0,0,139,200,0,0,138,200,0,0,137,200,0,0,136,200,0,0,133,200,0,0,131,60,128,160
	.byte 32,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,195,0,1,129,200,0,0,143,41,195,0,1
	.byte 119,200,0,0,152,195,0,1,117,195,0,1,116,195,0,1,115,195,0,1,114,195,0,1,113,195,0,1,112,195,0,1
	.byte 111,195,0,1,110,195,0,1,109,200,0,0,130,195,0,1,107,195,0,1,106,195,0,1,105,200,0,0,135,195,0,1
	.byte 103,195,0,1,102,195,0,1,101,195,0,1,100,195,0,1,99,195,0,1,98,195,0,1,97,195,0,1,96,195,0,1
	.byte 95,195,0,1,94,195,0,1,93,195,0,1,92,195,0,1,91,195,0,1,90,195,0,1,89,195,0,1,88,195,0,1
	.byte 87,200,0,0,134,200,0,0,132,40,200,0,0,129,36,200,0,0,150,39,200,0,0,148,37,200,0,0,146,200,0,0
	.byte 145,200,0,0,144,200,0,0,142,200,0,0,141,200,0,0,139,200,0,0,138,200,0,0,137,200,0,0,136,200,0,0
	.byte 133,200,0,0,131,10,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,195,0,0,185
	.byte 195,0,0,186,195,0,0,187,195,0,0,186,195,0,0,185,195,0,0,184,20,128,162,196,0,0,158,28,0,0,4,193
	.byte 0,18,178,193,0,18,175,196,0,0,158,193,0,18,172,196,0,0,159,47,196,0,2,203,45,196,0,0,204,196,0,0
	.byte 198,196,0,0,199,196,0,0,194,45,196,0,0,204,196,0,2,203,196,0,0,207,196,0,0,194,196,0,0,206,196,0
	.byte 0,205,196,0,2,205,20,128,162,196,0,0,158,28,0,0,4,193,0,18,178,193,0,18,175,196,0,0,158,193,0,18
	.byte 172,196,0,0,159,51,196,0,2,203,49,196,0,0,204,196,0,0,198,196,0,0,199,196,0,0,194,49,196,0,0,204
	.byte 196,0,2,203,196,0,0,207,196,0,0,194,196,0,0,206,196,0,0,205,196,0,2,205,20,128,162,196,0,0,158,28
	.byte 0,0,4,193,0,18,178,193,0,18,175,196,0,0,158,193,0,18,172,196,0,0,159,55,196,0,2,203,54,196,0,0
	.byte 204,196,0,0,198,196,0,0,199,196,0,0,194,54,196,0,0,204,196,0,2,203,196,0,0,207,196,0,0,194,196,0
	.byte 0,206,196,0,0,205,196,0,2,205,255,255,255,255,255,8,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0
	.byte 18,174,193,0,18,172,72,194,0,0,11,194,0,0,11,72,17,128,162,198,0,2,8,24,0,0,4,198,0,2,18,193
	.byte 0,18,175,198,0,2,8,193,0,18,172,198,0,2,4,198,0,2,9,198,0,2,20,198,0,2,13,198,0,2,12,198
	.byte 0,2,7,198,0,2,6,77,80,78,79,76,75,38,128,162,198,0,2,8,56,0,0,4,198,0,2,18,193,0,18,175
	.byte 198,0,2,8,193,0,18,172,198,0,2,4,198,0,2,9,198,0,6,41,198,0,2,13,198,0,2,12,198,0,2,7
	.byte 198,0,6,10,198,0,5,28,198,0,5,27,198,0,5,26,198,0,6,26,198,0,6,40,198,0,6,39,198,0,6,38
	.byte 198,0,6,37,198,0,6,36,82,85,198,0,6,33,198,0,6,32,198,0,6,31,198,0,6,30,198,0,6,29,198,0
	.byte 6,28,198,0,6,27,198,0,6,23,198,0,6,22,198,0,6,21,198,0,6,20,198,0,6,19,198,0,6,18,198,0
	.byte 6,17,198,0,6,16,198,0,6,15,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_4:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_6:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_5:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM10=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM12=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_11:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM15=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM16=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM17=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM18=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM19=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_10:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM20=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM21=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM22=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM22
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM23=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM23
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM24=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM25=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM26=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM27=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM28=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_9:

	.byte 5
	.asciz "MonoTouch_UIKit_UIResponder"

	.byte 24,16
LDIFF_SYM29=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,0,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM30=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,20,0,7
	.asciz "MonoTouch_UIKit_UIResponder"

LDIFF_SYM31=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM32=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM32
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM33=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM33
LTDIE_8:

	.byte 5
	.asciz "MonoTouch_UIKit_UIView"

	.byte 48,16
LDIFF_SYM34=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,0,6
	.asciz "__mt_BackgroundColor_var"

LDIFF_SYM35=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,24,6
	.asciz "__mt_Layer_var"

LDIFF_SYM36=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,28,6
	.asciz "__mt_Superview_var"

LDIFF_SYM37=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,32,6
	.asciz "__mt_Subviews_var"

LDIFF_SYM38=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,36,6
	.asciz "__mt_GestureRecognizers_var"

LDIFF_SYM39=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,40,6
	.asciz "__mt_TintColor_var"

LDIFF_SYM40=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIView"

LDIFF_SYM41=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_7:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewCell"

	.byte 72,16
LDIFF_SYM44=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,0,6
	.asciz "__mt_ImageView_var"

LDIFF_SYM45=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,48,6
	.asciz "__mt_TextLabel_var"

LDIFF_SYM46=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,52,6
	.asciz "__mt_DetailTextLabel_var"

LDIFF_SYM47=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,56,6
	.asciz "__mt_ContentView_var"

LDIFF_SYM48=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM49=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,64,6
	.asciz "__mt_AccessoryView_var"

LDIFF_SYM50=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,35,68,0,7
	.asciz "MonoTouch_UIKit_UITableViewCell"

LDIFF_SYM51=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM52=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM53=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_17:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM54=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM55=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM56=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM57=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_16:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM58=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM59=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM60=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM61=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_15:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM62=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM63=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM64=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM65=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_19:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM66=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM67=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM68=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM69=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM70=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_18:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM71=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM72=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM73=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM74=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM75=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM76=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_14:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM77=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM78=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM79=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM80=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM81=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM82=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM83=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM84=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM85=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM86=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM87=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM88=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM89=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_13:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM90=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM91=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM92=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM93=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM94=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM95=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_12:

	.byte 5
	.asciz "MonoTouch_Foundation_NSAction"

	.byte 52,16
LDIFF_SYM96=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSAction"

LDIFF_SYM97=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM98=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM99=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_20:

	.byte 17
	.asciz "System_Windows_Input_ICommand"

	.byte 8,7
	.asciz "System_Windows_Input_ICommand"

LDIFF_SYM100=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_3:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_Element"

	.byte 36,16
LDIFF_SYM103=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,0,6
	.asciz "_elementID"

LDIFF_SYM104=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,28,6
	.asciz "_lastAttachedCell"

LDIFF_SYM105=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,8,6
	.asciz "Tapped"

LDIFF_SYM106=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,12,6
	.asciz "_caption"

LDIFF_SYM107=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 2,35,16,6
	.asciz "_visible"

LDIFF_SYM108=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2,35,32,6
	.asciz "_selectedCommand"

LDIFF_SYM109=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 2,35,20,6
	.asciz "<ShouldDeselectAfterTouch>k__BackingField"

LDIFF_SYM110=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,33,6
	.asciz "<Parent>k__BackingField"

LDIFF_SYM111=LTDIE_3_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,24,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_Element"

LDIFF_SYM112=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM113=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM114=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_21:

	.byte 8
	.asciz "MonoTouch_UIKit_UITextAlignment"

	.byte 4
LDIFF_SYM115=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 9
	.asciz "Left"

	.byte 0,9
	.asciz "Center"

	.byte 1,9
	.asciz "Right"

	.byte 2,9
	.asciz "Justified"

	.byte 3,9
	.asciz "Natural"

	.byte 4,0,7
	.asciz "MonoTouch_UIKit_UITextAlignment"

LDIFF_SYM116=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM117=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM118=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_22:

	.byte 5
	.asciz "System_EventHandler"

	.byte 52,16
LDIFF_SYM119=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,0,0,7
	.asciz "System_EventHandler"

LDIFF_SYM120=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM121=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM122=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_2:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement"

	.byte 44,16
LDIFF_SYM123=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,0,6
	.asciz "_alignment"

LDIFF_SYM124=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,40,6
	.asciz "ValueChanged"

LDIFF_SYM125=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,35,36,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement"

LDIFF_SYM126=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM127=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM128=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_1:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement`1"

	.byte 48,16
LDIFF_SYM129=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,0,6
	.asciz "_value"

LDIFF_SYM130=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,44,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement`1"

LDIFF_SYM131=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM131
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM132=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM133=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_24:

	.byte 5
	.asciz "MonoTouch_UIKit_UIPickerViewModel"

	.byte 20,16
LDIFF_SYM134=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIPickerViewModel"

LDIFF_SYM135=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM136=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM137=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_23:

	.byte 5
	.asciz "MonoTouch_UIKit_UIPickerView"

	.byte 52,16
LDIFF_SYM138=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,35,0,6
	.asciz "model"

LDIFF_SYM139=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM139
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIPickerView"

LDIFF_SYM140=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM141=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM142=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_25:

	.byte 17
	.asciz "System_Collections_IList"

	.byte 8,7
	.asciz "System_Collections_IList"

LDIFF_SYM143=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM144=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM145=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_26:

	.byte 17
	.asciz "Cirrious_CrossCore_Converters_IMvxValueConverter"

	.byte 8,7
	.asciz "Cirrious_CrossCore_Converters_IMvxValueConverter"

LDIFF_SYM146=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_27:

	.byte 5
	.asciz "MonoTouch_UIKit_UIColor"

	.byte 20,16
LDIFF_SYM149=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIColor"

LDIFF_SYM150=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement"

	.byte 64,16
LDIFF_SYM153=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,0,6
	.asciz "_picker"

LDIFF_SYM154=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM154
	.byte 2,35,48,6
	.asciz "<Entries>k__BackingField"

LDIFF_SYM155=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,52,6
	.asciz "<DisplayValueConverter>k__BackingField"

LDIFF_SYM156=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 2,35,56,6
	.asciz "<BackgroundColor>k__BackingField"

LDIFF_SYM157=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,60,0,7
	.asciz "Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement"

LDIFF_SYM158=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM159=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM160=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2
	.asciz "Cirrious.MvvmCross.Dialog.Touch.Elements.SimplePickerElement:CreatePicker"
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker
	.long Lme_40

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM161=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM161
	.byte 2,123,24,11
	.asciz "V_0"

LDIFF_SYM162=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM163=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM164=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM165=Lfde0_end - Lfde0_start
	.long LDIFF_SYM165
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker

LDIFF_SYM166=Lme_40 - _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_CreatePicker
	.long LDIFF_SYM166
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_30:

	.byte 5
	.asciz "MonoTouch_UIKit_UIViewController"

	.byte 48,16
LDIFF_SYM167=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 2,35,0,6
	.asciz "__mt_View_var"

LDIFF_SYM168=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,24,6
	.asciz "__mt_ParentViewController_var"

LDIFF_SYM169=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,28,6
	.asciz "__mt_NavigationItem_var"

LDIFF_SYM170=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,32,6
	.asciz "__mt_NavigationController_var"

LDIFF_SYM171=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,36,6
	.asciz "__mt_ToolbarItems_var"

LDIFF_SYM172=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,40,6
	.asciz "__mt_ChildViewControllers_var"

LDIFF_SYM173=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIViewController"

LDIFF_SYM174=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM175=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM176=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM176
LTDIE_29:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewController"

	.byte 52,16
LDIFF_SYM177=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,0,6
	.asciz "__mt_TableView_var"

LDIFF_SYM178=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UITableViewController"

LDIFF_SYM179=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM179
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM180=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM180
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM181=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM181
LTDIE_31:

	.byte 8
	.asciz "MonoTouch_UIKit_UITableViewStyle"

	.byte 4
LDIFF_SYM182=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 9
	.asciz "Plain"

	.byte 0,9
	.asciz "Grouped"

	.byte 1,0,7
	.asciz "MonoTouch_UIKit_UITableViewStyle"

LDIFF_SYM183=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM184=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM184
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM185=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM185
LTDIE_32:

	.byte 5
	.asciz "System_Action`1"

	.byte 52,16
LDIFF_SYM186=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,0,0,7
	.asciz "System_Action`1"

LDIFF_SYM187=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM187
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM188=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM188
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM189=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM189
LTDIE_33:

	.byte 5
	.asciz "MonoTouch_UIKit_UISearchBar"

	.byte 56,16
LDIFF_SYM190=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM191=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM191
	.byte 2,35,48,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM192=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,52,0,7
	.asciz "MonoTouch_UIKit_UISearchBar"

LDIFF_SYM193=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM194=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM195=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_35:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollView"

	.byte 52,16
LDIFF_SYM196=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM197=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIScrollView"

LDIFF_SYM198=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM199=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM200=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM200
LTDIE_34:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableView"

	.byte 68,16
LDIFF_SYM201=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,35,0,6
	.asciz "__mt_WeakDataSource_var"

LDIFF_SYM202=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 2,35,52,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM203=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,56,6
	.asciz "__mt_TableHeaderView_var"

LDIFF_SYM204=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM205=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UITableView"

LDIFF_SYM206=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM206
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM207=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM207
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM208=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_37:

	.byte 5
	.asciz "MonoTouch_UIKit_UIActivityIndicatorView"

	.byte 48,16
LDIFF_SYM209=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIActivityIndicatorView"

LDIFF_SYM210=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM211=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM212=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM212
LTDIE_38:

	.byte 5
	.asciz "MonoTouch_UIKit_UILabel"

	.byte 60,16
LDIFF_SYM213=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,35,0,6
	.asciz "__mt_Font_var"

LDIFF_SYM214=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,48,6
	.asciz "__mt_TextColor_var"

LDIFF_SYM215=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,35,52,6
	.asciz "__mt_ShadowColor_var"

LDIFF_SYM216=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,35,56,0,7
	.asciz "MonoTouch_UIKit_UILabel"

LDIFF_SYM217=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM218=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM218
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM219=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_39:

	.byte 5
	.asciz "MonoTouch_UIKit_UIImageView"

	.byte 52,16
LDIFF_SYM220=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,0,6
	.asciz "__mt_Image_var"

LDIFF_SYM221=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIImageView"

LDIFF_SYM222=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM222
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM223=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM224=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_40:

	.byte 8
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshViewStatus"

	.byte 4
LDIFF_SYM225=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 9
	.asciz "ReleaseToReload"

	.byte 0,9
	.asciz "PullToReload"

	.byte 1,9
	.asciz "Loading"

	.byte 2,0,7
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshViewStatus"

LDIFF_SYM226=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM226
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM227=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM227
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM228=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM228
LTDIE_36:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshTableHeaderView"

	.byte 80,16
LDIFF_SYM229=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,0,6
	.asciz "Activity"

LDIFF_SYM230=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,48,6
	.asciz "LastUpdateLabel"

LDIFF_SYM231=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,52,6
	.asciz "StatusLabel"

LDIFF_SYM232=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,56,6
	.asciz "ArrowView"

LDIFF_SYM233=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,60,6
	.asciz "status"

LDIFF_SYM234=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,64,6
	.asciz "IsFlipped"

LDIFF_SYM235=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,68,6
	.asciz "lastUpdateTime"

LDIFF_SYM236=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,72,0,7
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshTableHeaderView"

LDIFF_SYM237=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM237
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM238=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM238
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM239=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_42:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM240=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM240
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM241=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM242=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM243=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM243
LTDIE_43:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_Group"

	.byte 12,16
LDIFF_SYM244=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,0,6
	.asciz "<Key>k__BackingField"

LDIFF_SYM245=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,8,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_Group"

LDIFF_SYM246=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM247=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM247
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM248=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM248
LTDIE_44:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM249=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM250=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM251=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM252=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM253=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM253
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM254=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM255=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM255
LTDIE_41:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_RootElement"

	.byte 68,16
LDIFF_SYM256=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,0,6
	.asciz "_summarySection"

LDIFF_SYM257=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,56,6
	.asciz "_summaryElement"

LDIFF_SYM258=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,35,60,6
	.asciz "CreateOnSelected"

LDIFF_SYM259=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,35,36,6
	.asciz "TableView"

LDIFF_SYM260=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM260
	.byte 2,35,40,6
	.asciz "RadioSelectedChanged"

LDIFF_SYM261=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM261
	.byte 2,35,44,6
	.asciz "<Group>k__BackingField"

LDIFF_SYM262=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM262
	.byte 2,35,48,6
	.asciz "<UnevenRows>k__BackingField"

LDIFF_SYM263=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM263
	.byte 2,35,64,6
	.asciz "<NeedColorUpdate>k__BackingField"

LDIFF_SYM264=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM264
	.byte 2,35,65,6
	.asciz "<Sections>k__BackingField"

LDIFF_SYM265=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,52,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_RootElement"

LDIFF_SYM266=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM266
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM267=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM267
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM268=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_45:

	.byte 5
	.asciz "_SearchTextEventHandler"

	.byte 52,16
LDIFF_SYM269=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,0,0,7
	.asciz "_SearchTextEventHandler"

LDIFF_SYM270=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM270
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM271=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM271
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM272=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM272
LTDIE_48:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

	.byte 20,16
LDIFF_SYM273=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

LDIFF_SYM274=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM275=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM276=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM276
LTDIE_47:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewSource"

	.byte 20,16
LDIFF_SYM277=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UITableViewSource"

LDIFF_SYM278=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM278
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM279=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM279
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM280=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_46:

	.byte 5
	.asciz "_Source"

	.byte 32,16
LDIFF_SYM281=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,35,0,6
	.asciz "Container"

LDIFF_SYM282=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,35,20,6
	.asciz "Root"

LDIFF_SYM283=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,35,24,6
	.asciz "checkForRefresh"

LDIFF_SYM284=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,35,28,0,7
	.asciz "_Source"

LDIFF_SYM285=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM285
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM286=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM286
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM287=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM287
LTDIE_28:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_DialogViewController"

	.byte 112,16
LDIFF_SYM288=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM288
	.byte 2,35,0,6
	.asciz "Style"

LDIFF_SYM289=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM289
	.byte 2,35,100,6
	.asciz "OnSelection"

LDIFF_SYM290=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,52,6
	.asciz "searchBar"

LDIFF_SYM291=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM291
	.byte 2,35,56,6
	.asciz "tableView"

LDIFF_SYM292=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,35,60,6
	.asciz "refreshView"

LDIFF_SYM293=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,35,64,6
	.asciz "root"

LDIFF_SYM294=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,68,6
	.asciz "pushing"

LDIFF_SYM295=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,104,6
	.asciz "dirty"

LDIFF_SYM296=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,105,6
	.asciz "reloading"

LDIFF_SYM297=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,35,106,6
	.asciz "refreshRequested"

LDIFF_SYM298=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM298
	.byte 2,35,72,6
	.asciz "enableSearch"

LDIFF_SYM299=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM299
	.byte 2,35,107,6
	.asciz "originalSections"

LDIFF_SYM300=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,76,6
	.asciz "originalElements"

LDIFF_SYM301=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,80,6
	.asciz "SearchTextChanged"

LDIFF_SYM302=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,84,6
	.asciz "TableSource"

LDIFF_SYM303=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM303
	.byte 2,35,88,6
	.asciz "ViewDissapearing"

LDIFF_SYM304=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2,35,92,6
	.asciz "<AutoHideSearch>k__BackingField"

LDIFF_SYM305=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,35,108,6
	.asciz "<SearchPlaceholder>k__BackingField"

LDIFF_SYM306=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 2,35,96,6
	.asciz "<Autorotate>k__BackingField"

LDIFF_SYM307=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 2,35,109,0,7
	.asciz "CrossUI_Touch_Dialog_DialogViewController"

LDIFF_SYM308=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM308
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM309=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM309
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM310=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM310
LTDIE_49:

	.byte 5
	.asciz "MonoTouch_Foundation_NSIndexPath"

	.byte 20,16
LDIFF_SYM311=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSIndexPath"

LDIFF_SYM312=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM312
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM313=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM313
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM314=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM314
LTDIE_50:

	.byte 5
	.asciz "_SimplePickerViewController"

	.byte 56,16
LDIFF_SYM315=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM315
	.byte 2,35,0,6
	.asciz "_container"

LDIFF_SYM316=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,35,48,6
	.asciz "<Autorotate>k__BackingField"

LDIFF_SYM317=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM317
	.byte 2,35,52,0,7
	.asciz "_SimplePickerViewController"

LDIFF_SYM318=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM318
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM319=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM319
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM320=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM320
	.byte 2
	.asciz "Cirrious.MvvmCross.Dialog.Touch.Elements.SimplePickerElement:Selected"
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.long Lme_44

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM321=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 1,86,3
	.asciz "dvc"

LDIFF_SYM322=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 1,90,3
	.asciz "tableView"

LDIFF_SYM323=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 0,3
	.asciz "path"

LDIFF_SYM324=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 0,11
	.asciz "V_0"

LDIFF_SYM325=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 1,84,11
	.asciz "V_1"

LDIFF_SYM326=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM326
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM327=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM327
	.byte 0,11
	.asciz "V_3"

LDIFF_SYM328=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM329=Lfde1_end - Lfde1_start
	.long LDIFF_SYM329
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath

LDIFF_SYM330=Lme_44 - _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_Selected_CrossUI_Touch_Dialog_DialogViewController_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.long LDIFF_SYM330
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,104,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_51:

	.byte 8
	.asciz "MonoTouch_UIKit_UIInterfaceOrientation"

	.byte 4
LDIFF_SYM331=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 9
	.asciz "Portrait"

	.byte 1,9
	.asciz "PortraitUpsideDown"

	.byte 2,9
	.asciz "LandscapeLeft"

	.byte 4,9
	.asciz "LandscapeRight"

	.byte 3,0,7
	.asciz "MonoTouch_UIKit_UIInterfaceOrientation"

LDIFF_SYM332=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM332
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM333=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM333
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM334=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM334
	.byte 2
	.asciz "Cirrious.MvvmCross.Dialog.Touch.Elements.SimplePickerElement/SimplePickerViewController:DidRotate"
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation
	.long Lme_51

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM335=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM335
	.byte 1,86,3
	.asciz "fromInterfaceOrientation"

LDIFF_SYM336=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM336
	.byte 2,123,40,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM337=Lfde2_end - Lfde2_start
	.long LDIFF_SYM337
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation

LDIFF_SYM338=Lme_51 - _Cirrious_MvvmCross_Dialog_Touch_Elements_SimplePickerElement_SimplePickerViewController_DidRotate_MonoTouch_UIKit_UIInterfaceOrientation
	.long LDIFF_SYM338
	.byte 12,13,0,72,14,8,135,2,68,14,20,134,5,136,4,139,3,142,1,68,14,80,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_54:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM339=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM339
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM340=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM340
LTDIE_54_POINTER:

	.byte 13
LDIFF_SYM341=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM341
LTDIE_54_REFERENCE:

	.byte 14
LDIFF_SYM342=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM342
LTDIE_53:

	.byte 5
	.asciz "Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController"

	.byte 136,1,16
LDIFF_SYM343=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM343
	.byte 2,35,0,6
	.asciz "ViewDidLoadCalled"

LDIFF_SYM344=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM344
	.byte 2,35,112,6
	.asciz "ViewWillAppearCalled"

LDIFF_SYM345=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM345
	.byte 2,35,116,6
	.asciz "ViewDidAppearCalled"

LDIFF_SYM346=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM346
	.byte 2,35,120,6
	.asciz "ViewDidDisappearCalled"

LDIFF_SYM347=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM347
	.byte 2,35,124,6
	.asciz "ViewWillDisappearCalled"

LDIFF_SYM348=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM348
	.byte 3,35,128,1,6
	.asciz "DisposeCalled"

LDIFF_SYM349=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM349
	.byte 3,35,132,1,0,7
	.asciz "Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController"

LDIFF_SYM350=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM350
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM351=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM351
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM352=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM352
LTDIE_56:

	.byte 17
	.asciz "System_Collections_Generic_IDictionary`2"

	.byte 8,7
	.asciz "System_Collections_Generic_IDictionary`2"

LDIFF_SYM353=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM353
LTDIE_56_POINTER:

	.byte 13
LDIFF_SYM354=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM354
LTDIE_56_REFERENCE:

	.byte 14
LDIFF_SYM355=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM355
LTDIE_58:

	.byte 8
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

	.byte 4
LDIFF_SYM356=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM356
	.byte 9
	.asciz "Unknown"

	.byte 0,9
	.asciz "UserAction"

	.byte 1,9
	.asciz "Bookmark"

	.byte 2,9
	.asciz "AutomatedService"

	.byte 3,9
	.asciz "Other"

	.byte 4,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

LDIFF_SYM357=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM357
LTDIE_58_POINTER:

	.byte 13
LDIFF_SYM358=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM358
LTDIE_58_REFERENCE:

	.byte 14
LDIFF_SYM359=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM359
LTDIE_57:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

	.byte 16,16
LDIFF_SYM360=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM360
	.byte 2,35,0,6
	.asciz "<Type>k__BackingField"

LDIFF_SYM361=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM361
	.byte 2,35,12,6
	.asciz "<AdditionalInfo>k__BackingField"

LDIFF_SYM362=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM362
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

LDIFF_SYM363=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM363
LTDIE_57_POINTER:

	.byte 13
LDIFF_SYM364=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM364
LTDIE_57_REFERENCE:

	.byte 14
LDIFF_SYM365=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM365
LTDIE_55:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

	.byte 24,16
LDIFF_SYM366=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,35,0,6
	.asciz "<ViewModelType>k__BackingField"

LDIFF_SYM367=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM367
	.byte 2,35,8,6
	.asciz "<ParameterValues>k__BackingField"

LDIFF_SYM368=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM368
	.byte 2,35,12,6
	.asciz "<PresentationValues>k__BackingField"

LDIFF_SYM369=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM369
	.byte 2,35,16,6
	.asciz "<RequestedBy>k__BackingField"

LDIFF_SYM370=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM370
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

LDIFF_SYM371=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM371
LTDIE_55_POINTER:

	.byte 13
LDIFF_SYM372=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM372
LTDIE_55_REFERENCE:

	.byte 14
LDIFF_SYM373=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM373
LTDIE_59:

	.byte 17
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

LDIFF_SYM374=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM374
LTDIE_59_POINTER:

	.byte 13
LDIFF_SYM375=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM375
LTDIE_59_REFERENCE:

	.byte 14
LDIFF_SYM376=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM376
LTDIE_52:

	.byte 5
	.asciz "Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController"

	.byte 144,1,16
LDIFF_SYM377=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM377
	.byte 2,35,0,6
	.asciz "<Request>k__BackingField"

LDIFF_SYM378=LTDIE_55_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM378
	.byte 3,35,136,1,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM379=LTDIE_59_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM379
	.byte 3,35,140,1,0,7
	.asciz "Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController"

LDIFF_SYM380=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM380
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM381=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM381
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM382=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM382
	.byte 2
	.asciz "Cirrious.MvvmCross.Dialog.Touch.MvxDialogViewController:Bind<!!0>"
	.long _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string
	.long Lme_56

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM383=LTDIE_52_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM383
	.byte 2,123,12,3
	.asciz "element"

LDIFF_SYM384=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM384
	.byte 1,80,3
	.asciz "bindingDescription"

LDIFF_SYM385=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM385
	.byte 2,123,20,11
	.asciz "V_0"

LDIFF_SYM386=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM386
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM387=Lfde3_end - Lfde3_start
	.long LDIFF_SYM387
Lfde3_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string

LDIFF_SYM388=Lme_56 - _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_string
	.long LDIFF_SYM388
	.byte 12,13,0,72,14,8,135,2,68,14,20,132,5,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_60:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM389=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM389
LTDIE_60_POINTER:

	.byte 13
LDIFF_SYM390=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM390
LTDIE_60_REFERENCE:

	.byte 14
LDIFF_SYM391=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM391
	.byte 2
	.asciz "Cirrious.MvvmCross.Dialog.Touch.MvxDialogViewController:Bind<!!0>"
	.long _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.long Lme_57

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM392=LTDIE_52_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM392
	.byte 2,123,12,3
	.asciz "element"

LDIFF_SYM393=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM393
	.byte 1,80,3
	.asciz "bindingDescription"

LDIFF_SYM394=LTDIE_60_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM394
	.byte 2,123,20,11
	.asciz "V_0"

LDIFF_SYM395=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM395
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM396=Lfde4_end - Lfde4_start
	.long LDIFF_SYM396
Lfde4_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription

LDIFF_SYM397=Lme_57 - _Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_Bind___0___0_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.long LDIFF_SYM397
	.byte 12,13,0,72,14,8,135,2,68,14,20,132,5,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_61:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM398=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM398
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM399=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM399
LTDIE_61_POINTER:

	.byte 13
LDIFF_SYM400=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM400
LTDIE_61_REFERENCE:

	.byte 14
LDIFF_SYM401=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM401
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_59

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM402=LTDIE_61_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM402
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM403=Lfde5_end - Lfde5_start
	.long LDIFF_SYM403
Lfde5_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM404=Lme_59 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM404
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Array.cs"

	.byte 1,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,2,1,3,207,0,2,32,1,2,252,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
