	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor
	.align	2
_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor:
Leh_func_begin1:
	bx	lr
Leh_func_end1:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded
	.align	2
_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded:
Leh_func_begin2:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
	push	{r8}
Ltmp2:
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC2_0+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC2_0+8))
LPC2_0:
	add	r4, pc, r4
	ldr	r0, [r4, #16]
	mov	r8, r0
	bl	_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm
	ldr	r1, [r4, #24]
	ldr	r2, [r0]
	sub	r2, r2, #40
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end2:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__cctor
	.align	2
_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__cctor:
Leh_func_begin3:
	push	{r4, r7, lr}
Ltmp3:
	add	r7, sp, #4
Ltmp4:
Ltmp5:
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC3_0+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC3_0+8))
LPC3_0:
	add	r4, pc, r4
	ldr	r0, [r4, #28]
	bl	_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #32]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end3:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.align	2
_OffSeasonPro_Plugin_TextToSpeech__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string:
Leh_func_begin4:
	push	{r4, r5, r6, r7, lr}
Ltmp6:
	add	r7, sp, #12
Ltmp7:
	push	{r10, r11}
Ltmp8:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC4_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got-(LPC4_0+8))
LPC4_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB4_2
	bl	_p_3_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB4_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB4_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB4_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB4_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB4_7
LBB4_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB4_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end4:

.zerofill __DATA,__bss,_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got,56,4
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__cctor
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.no_dead_strip	_mono_aot_OffSeasonPro_Plugin_TextToSpeech_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	5
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	4
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	5
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	6
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	12
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
Lset5 = Leh_func_end4-Leh_func_begin4
	.long	Lset5
Lset6 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset6
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "OffSeasonPro.Plugin.TextToSpeech.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__cctor
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor
	bl _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded
	bl _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__cctor
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Plugin_TextToSpeech__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 13,10,2,2
	.short 0, 14
	.byte 0,0,0,0,1,3,6,255,255,255,255,246,0,0,0,0,15
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,46,12,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 5,8,0,9,0,10,0,11
	.long 0,12,46
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 3
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 2, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 10,10,1,2
	.short 0
	.byte 55,2,1,1,1,12,12,12,3,4
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 13,10,2,2
	.short 0, 15
	.byte 0,0,0,0,128,180,3,3,255,255,255,255,70,0,0,0,0,128,189
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 3,10,1,2
	.short 0
	.byte 128,192,7,7

.text
	.align 4
plt:
_mono_aot_OffSeasonPro_Plugin_TextToSpeech_plt:
_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_got - . + 40,104
_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_got - . + 44,116
_p_3_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_got - . + 48,142
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "OffSeasonPro.Plugin.TextToSpeech"
	.asciz "97E1D0E1-9BEF-4538-8930-56B79454B281"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "97E1D0E1-9BEF-4538-8930-56B79454B281"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "OffSeasonPro.Plugin.TextToSpeech"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_got
	.align 2
	.long _OffSeasonPro_Plugin_TextToSpeech__OffSeasonPro_Plugin_TextToSpeech_PluginLoader__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 10,56,4,13,11,387000831,0,231
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_OffSeasonPro_Plugin_TextToSpeech_info
	.align 2
_mono_aot_module_OffSeasonPro_Plugin_TextToSpeech_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,1,3,0,1,3,3,6,5,4,1,3,2,8,7,0,1,9,4,2,119,1,2,2,130,146,1,2,130,146,1,4
	.byte 2,97,1,3,2,130,146,1,2,130,146,1,7,18,255,252,0,0,0,1,1,7,31,12,0,39,42,47,34,255,254,0
	.byte 0,0,0,255,43,0,0,1,34,255,254,0,0,0,0,255,43,0,0,2,6,255,254,0,0,0,0,255,43,0,0,2
	.byte 14,1,3,16,1,3,1,33,3,255,254,0,0,0,0,255,43,0,0,1,7,23,109,111,110,111,95,111,98,106,101,99
	.byte 116,95,110,101,119,95,112,116,114,102,114,101,101,0,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101
	.byte 114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 0,128,144,8,0,0,1,0,128,144,8,0,0,1,5,128,196,7,8,4,0,1,193,0,18,178,193,0,18,175,193,0
	.byte 18,174,193,0,18,172,6,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
