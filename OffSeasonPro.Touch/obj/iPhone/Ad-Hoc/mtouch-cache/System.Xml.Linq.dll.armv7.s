	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute:
Leh_func_begin1:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	cmp	r1, #0
	ldrne	r2, [r1, #32]
	strne	r2, [r0, #32]
	ldrne	r1, [r1, #36]
	strne	r1, [r0, #36]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC1_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC1_0+8))
LPC1_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end1:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
Ltmp5:
	mov	r6, r1
	mov	r5, r0
	mov	r1, #0
	mov	r4, r2
	mov	r0, r6
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	bne	LBB2_2
	mov	r0, r5
	mov	r1, r4
	str	r6, [r5, #32]
	bl	_p_5_plt_System_Xml_Linq_XAttribute_SetValue_object_llvm
	pop	{r4, r5, r6, r7, pc}
LBB2_2:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC2_0+8))
	mov	r1, #13
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC2_0+8))
LPC2_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end2:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration:
Leh_func_begin3:
	push	{r4, r5, r7, lr}
Ltmp6:
	add	r7, sp, #8
Ltmp7:
Ltmp8:
	mov	r4, r0
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	ldr	r5, [r0, #12]
	bl	_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	mov	r1, r0
	mov	r0, #1
	tst	r1, #255
	bne	LBB3_2
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC3_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC3_0+8))
LPC3_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	ldr	r4, [r0, #12]
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	uxtb	r0, r0
LBB3_2:
	pop	{r4, r5, r7, pc}
Leh_func_end3:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Name
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Name:
Leh_func_begin4:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end4:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NextAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NextAttribute:
Leh_func_begin5:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end5:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_NextAttribute_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_set_NextAttribute_System_Xml_Linq_XAttribute:
Leh_func_begin6:
	str	r1, [r0, #40]
	bx	lr
Leh_func_end6:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NodeType:
Leh_func_begin7:
	mov	r0, #2
	bx	lr
Leh_func_end7:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_PreviousAttribute_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_set_PreviousAttribute_System_Xml_Linq_XAttribute:
Leh_func_begin8:
	str	r1, [r0, #44]
	bx	lr
Leh_func_end8:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value:
Leh_func_begin9:
	push	{r7, lr}
Ltmp9:
	mov	r7, sp
Ltmp10:
Ltmp11:
	ldr	r0, [r0, #36]
	bl	_p_10_plt_System_Xml_Linq_XUtil_ToString_object_llvm
	pop	{r7, pc}
Leh_func_end9:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string:
Leh_func_begin10:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
Ltmp14:
	bl	_p_5_plt_System_Xml_Linq_XAttribute_SetValue_object_llvm
	pop	{r7, pc}
Leh_func_end10:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_Remove
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_Remove:
Leh_func_begin11:
	push	{r4, r5, r6, r7, lr}
Ltmp15:
	add	r7, sp, #12
Ltmp16:
Ltmp17:
	mov	r4, r0
	ldr	r0, [r4, #8]
	cmp	r0, #0
	beq	LBB11_22
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC11_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC11_0+8))
	ldr	r0, [r0]
LPC11_0:
	add	r1, pc, r1
	ldr	r6, [r1, #20]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #16]
	cmp	r0, r6
	bne	LBB11_22
	ldr	r5, [r4, #8]
	mov	r1, r4
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_11_plt_System_Xml_Linq_XObject_OnRemovingObject_object_llvm
	ldr	r0, [r4, #40]
	cmp	r0, #0
	beq	LBB11_4
	mov	r0, r4
	ldr	r1, [r4, #40]
	ldr	r2, [r0, #44]!
	str	r2, [r1, #44]
	b	LBB11_5
LBB11_4:
	add	r0, r4, #44
LBB11_5:
	ldr	r1, [r0]
	cmp	r1, #0
	ldrne	r1, [r4, #44]
	ldrne	r2, [r4, #40]
	strne	r2, [r1, #40]
	ldr	r1, [r4, #8]
	cmp	r1, #0
	beq	LBB11_7
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r3, [r2, #16]
	mov	r2, #0
	cmp	r3, r6
	moveq	r2, r1
	b	LBB11_8
LBB11_7:
	mov	r2, r1
LBB11_8:
	ldr	r1, [r2]
	ldr	r1, [r2, #52]
	cmp	r1, r4
	bne	LBB11_13
	ldr	r1, [r4, #8]
	cmp	r1, #0
	beq	LBB11_11
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r3, [r2, #16]
	mov	r2, #0
	cmp	r3, r6
	moveq	r2, r1
	b	LBB11_12
LBB11_11:
	mov	r2, r1
LBB11_12:
	ldr	r1, [r4, #40]
	ldr	r3, [r2]
	str	r1, [r2, #52]
LBB11_13:
	ldr	r1, [r4, #8]
	cmp	r1, #0
	beq	LBB11_15
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r3, [r2, #16]
	mov	r2, #0
	cmp	r3, r6
	moveq	r2, r1
	b	LBB11_16
LBB11_15:
	mov	r2, r1
LBB11_16:
	ldr	r1, [r2]
	ldr	r1, [r2, #56]
	cmp	r1, r4
	bne	LBB11_21
	ldr	r1, [r4, #8]
	cmp	r1, #0
	beq	LBB11_19
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r3, [r2, #16]
	mov	r2, #0
	cmp	r3, r6
	moveq	r2, r1
	b	LBB11_20
LBB11_19:
	mov	r2, r1
LBB11_20:
	ldr	r0, [r0]
	ldr	r1, [r2]
	str	r0, [r2, #56]
LBB11_21:
	mov	r0, #0
	mov	r1, r4
	str	r0, [r4, #8]
	mov	r0, r5
	bl	_p_12_plt_System_Xml_Linq_XObject_OnRemovedObject_object_llvm
LBB11_22:
	mov	r0, #0
	str	r0, [r4, #40]
	str	r0, [r4, #44]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end11:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp18:
	add	r7, sp, #8
Ltmp19:
Ltmp20:
	mov	r5, r1
	mov	r4, r0
	cmp	r5, #0
	beq	LBB12_2
	mov	r0, r4
	mov	r1, r4
	bl	_p_13_plt_System_Xml_Linq_XObject_OnValueChanging_object_llvm
	mov	r0, r5
	bl	_p_10_plt_System_Xml_Linq_XUtil_ToString_object_llvm
	str	r0, [r4, #36]
	mov	r0, r4
	mov	r1, r4
	bl	_p_14_plt_System_Xml_Linq_XObject_OnValueChanged_object_llvm
	pop	{r4, r5, r7, pc}
LBB12_2:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC12_0+8))
	mov	r1, #35
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC12_0+8))
LPC12_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end12:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute_ToString
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute_ToString:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
	push	{r10, r11}
Ltmp23:
	sub	sp, sp, #28
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC13_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC13_1+8))
LPC13_1:
	add	r6, pc, r6
	ldr	r0, [r6, #24]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_16_plt_System_Text_StringBuilder__ctor_llvm
	ldr	r0, [r5, #32]
	ldr	r1, [r0]
	ldr	r1, [r1, #32]
	blx	r1
	mov	r1, r0
	mov	r0, r4
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
	ldr	r1, [r6, #28]
	mov	r0, r4
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
	ldr	r0, [r5, #36]
	ldr	r1, [r6, #32]
	mov	r11, #0
	ldr	r1, [r1]
	ldr	r2, [r0]
	mov	r2, #0
	bl	_p_18_plt_string_IndexOfAny_char___int_llvm
	mov	r6, r0
	cmp	r6, #0
	blt	LBB13_19
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC13_2+8))
	mov	r11, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC13_2+8))
LPC13_2:
	add	r0, pc, r0
	ldr	r3, [r0, #36]
	ldr	r2, [r0, #40]
	ldr	r1, [r0, #32]
	ldr	r10, [r0, #60]
	str	r3, [sp, #20]
	str	r2, [sp, #16]
	ldr	r3, [r0, #44]
	ldr	r2, [r0, #48]
	str	r1, [sp, #24]
	str	r3, [sp, #12]
	str	r2, [sp, #8]
	ldr	r3, [r0, #52]
	ldr	r2, [r0, #56]
	str	r3, [sp, #4]
	str	r2, [sp]
LBB13_2:
	ldr	r1, [r5, #36]
	sub	r3, r6, r11
	mov	r0, r4
	mov	r2, r11
	bl	_p_19_plt_System_Text_StringBuilder_Append_string_int_int_llvm
	ldr	r0, [r5, #36]
	ldr	r1, [r0, #8]
	cmp	r1, r6
	bls	LBB13_23
	add	r0, r0, r6, lsl #1
	ldrh	r0, [r0, #12]
	sub	r1, r0, #9
	cmp	r1, #5
	bhs	LBB13_10
	cmp	r0, #9
	beq	LBB13_7
	cmp	r0, #10
	bne	LBB13_8
	ldr	r1, [sp]
	mov	r0, r4
	b	LBB13_17
LBB13_7:
	ldr	r1, [sp, #4]
	mov	r0, r4
	b	LBB13_17
LBB13_8:
	cmp	r0, #13
	bne	LBB13_10
	mov	r0, r4
	mov	r1, r10
	b	LBB13_17
LBB13_10:
	sub	r1, r0, #60
	cmp	r1, #2
	bhi	LBB13_12
	cmp	r0, #61
	bne	LBB13_16
LBB13_12:
	cmp	r0, #34
	bne	LBB13_14
	ldr	r1, [sp, #20]
	mov	r0, r4
	b	LBB13_17
LBB13_14:
	cmp	r0, #38
	bne	LBB13_18
	ldr	r1, [sp, #16]
	mov	r0, r4
	b	LBB13_17
LBB13_16:
	cmp	r0, #62
	mov	r0, r4
	ldreq	r1, [sp, #8]
	ldrne	r1, [sp, #12]
LBB13_17:
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
LBB13_18:
	ldr	r0, [r5, #36]
	ldr	r1, [sp, #24]
	add	r11, r6, #1
	ldr	r1, [r1]
	ldr	r2, [r0]
	mov	r2, r11
	bl	_p_18_plt_string_IndexOfAny_char___int_llvm
	mov	r6, r0
	cmp	r6, #0
	bge	LBB13_2
LBB13_19:
	ldr	r1, [r5, #36]
	cmp	r11, #1
	blt	LBB13_21
	ldr	r0, [r5, #36]
	mov	r2, r11
	ldr	r0, [r0, #8]
	sub	r3, r0, r11
	mov	r0, r4
	bl	_p_19_plt_System_Text_StringBuilder_Append_string_int_int_llvm
	b	LBB13_22
LBB13_21:
	mov	r0, r4
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
LBB13_22:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC13_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC13_3+8))
LPC13_3:
	add	r0, pc, r0
	ldr	r1, [r0, #64]
	mov	r0, r4
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #32]
	mov	r0, r4
	blx	r1
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp24:
LBB13_23:
	ldr	r0, LCPI13_0
LPC13_0:
	add	r1, pc, r0
	movw	r0, #600
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI13_0:
	.long	Ltmp24-(LPC13_0+8)
	.end_data_region
Leh_func_end13:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XAttribute__cctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XAttribute__cctor:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp25:
	add	r7, sp, #8
Ltmp26:
Ltmp27:
	movw	r5, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC14_0+8))
	mov	r1, #0
	movt	r5, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC14_0+8))
LPC14_0:
	add	r5, pc, r5
	ldr	r0, [r5, #68]
	bl	_p_21_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r5, #72]
	str	r0, [r1]
	ldr	r0, [r5, #76]
	mov	r1, #7
	bl	_p_21_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r5, #80]
	mov	r4, r0
	mov	r2, #14
	add	r0, r4, #16
	bl	_p_22_plt_string_memcpy_byte__byte__int_llvm
	ldr	r0, [r5, #32]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XCData__ctor_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XCData__ctor_string:
Leh_func_begin15:
	str	r1, [r0, #40]
	bx	lr
Leh_func_end15:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XCData__ctor_System_Xml_Linq_XCData
	.align	2
_System_Xml_Linq__System_Xml_Linq_XCData__ctor_System_Xml_Linq_XCData:
Leh_func_begin16:
	ldr	r1, [r1, #40]
	str	r1, [r0, #40]
	bx	lr
Leh_func_end16:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XCData_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XCData_get_NodeType:
Leh_func_begin17:
	mov	r0, #4
	bx	lr
Leh_func_end17:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XCData_WriteTo_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XCData_WriteTo_System_Xml_XmlWriter:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp28:
	add	r7, sp, #12
Ltmp29:
	push	{r10, r11}
Ltmp30:
	sub	sp, sp, #12
	mov	r5, r0
	str	r1, [sp]
	mov	r4, #0
	mov	r6, #0
	ldr	r0, [r5, #40]
	ldr	r0, [r0, #8]
	sub	r0, r0, #2
	cmp	r0, #1
	blt	LBB18_14
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC18_2+8))
	mov	r4, #0
	mov	r6, #0
	mov	r10, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC18_2+8))
LPC18_2:
	add	r0, pc, r0
	ldr	r1, [r0, #24]
	ldr	r0, [r0, #84]
	str	r1, [sp, #8]
	str	r0, [sp, #4]
LBB18_2:
	ldr	r0, [r5, #40]
	add	r0, r0, r10, lsl #1
	ldrh	r0, [r0, #12]
	cmp	r0, #93
	bne	LBB18_10
	ldr	r0, [r5, #40]
	add	r11, r10, #1
	ldr	r1, [r0, #8]
	cmp	r1, r11
	bls	LBB18_21
	add	r0, r0, r11, lsl #1
	ldrh	r0, [r0, #12]
	cmp	r0, #93
	bne	LBB18_11
	ldr	r1, [r5, #40]
	add	r0, r10, #2
	ldr	r2, [r1, #8]
	cmp	r2, r0
	bls	LBB18_22
	add	r0, r1, r0, lsl #1
	ldrh	r0, [r0, #12]
	cmp	r0, #62
	bne	LBB18_12
	cmp	r6, #0
	bne	LBB18_9
	ldr	r0, [sp, #8]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	bl	_p_16_plt_System_Text_StringBuilder__ctor_llvm
LBB18_9:
	ldr	r1, [r5, #40]
	ldr	r0, [r6]
	sub	r3, r10, r4
	mov	r2, r4
	mov	r0, r6
	bl	_p_19_plt_System_Text_StringBuilder_Append_string_int_int_llvm
	ldr	r1, [sp, #4]
	mov	r0, r6
	bl	_p_17_plt_System_Text_StringBuilder_Append_string_llvm
	add	r4, r10, #3
	mov	r10, r11
	b	LBB18_13
LBB18_10:
	add	r10, r10, #1
	b	LBB18_13
LBB18_11:
	mov	r10, r11
	b	LBB18_13
LBB18_12:
	mov	r10, r11
LBB18_13:
	ldr	r0, [r5, #40]
	ldr	r0, [r0, #8]
	sub	r0, r0, #2
	cmp	r10, r0
	blt	LBB18_2
LBB18_14:
	cmp	r4, #0
	beq	LBB18_17
	ldr	r0, [r5, #40]
	ldr	r0, [r0, #8]
	cmp	r4, r0
	beq	LBB18_17
	ldr	r1, [r5, #40]
	ldr	r0, [r5, #40]
	ldr	r0, [r0, #8]
	ldr	r2, [r6]
	mov	r2, r4
	sub	r3, r0, r4
	mov	r0, r6
	bl	_p_19_plt_System_Text_StringBuilder_Append_string_int_int_llvm
LBB18_17:
	cmp	r6, #0
	beq	LBB18_19
	ldr	r0, [r6]
	ldr	r1, [r0, #32]
	mov	r0, r6
	blx	r1
	mov	r1, r0
	b	LBB18_20
LBB18_19:
	ldr	r1, [r5, #40]
LBB18_20:
	ldr	r0, [sp]
	ldr	r2, [r0]
	ldr	r2, [r2, #116]
	blx	r2
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp31:
LBB18_21:
	ldr	r0, LCPI18_0
LPC18_0:
	add	r1, pc, r0
	b	LBB18_23
Ltmp32:
LBB18_22:
	ldr	r0, LCPI18_1
LPC18_1:
	add	r1, pc, r0
LBB18_23:
	movw	r0, #600
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI18_0:
	.long	Ltmp31-(LPC18_0+8)
LCPI18_1:
	.long	Ltmp32-(LPC18_1+8)
	.end_data_region
Leh_func_end18:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XComment__ctor_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XComment__ctor_string:
Leh_func_begin19:
	str	r1, [r0, #40]
	bx	lr
Leh_func_end19:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XComment__ctor_System_Xml_Linq_XComment
	.align	2
_System_Xml_Linq__System_Xml_Linq_XComment__ctor_System_Xml_Linq_XComment:
Leh_func_begin20:
	ldr	r1, [r1, #40]
	str	r1, [r0, #40]
	bx	lr
Leh_func_end20:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XComment_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XComment_get_NodeType:
Leh_func_begin21:
	mov	r0, #8
	bx	lr
Leh_func_end21:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XComment_get_Value
	.align	2
_System_Xml_Linq__System_Xml_Linq_XComment_get_Value:
Leh_func_begin22:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end22:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XComment_WriteTo_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XComment_WriteTo_System_Xml_XmlWriter:
Leh_func_begin23:
	push	{r7, lr}
Ltmp33:
	mov	r7, sp
Ltmp34:
Ltmp35:
	ldr	r2, [r0, #40]
	ldr	r0, [r1]
	ldr	r3, [r0, #112]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	pop	{r7, pc}
Leh_func_end23:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__ctor:
Leh_func_begin24:
	bx	lr
Leh_func_end24:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer_get_FirstNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer_get_FirstNode:
Leh_func_begin25:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end25:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode:
Leh_func_begin26:
	push	{r4, r5, r7, lr}
Ltmp36:
	add	r7, sp, #8
Ltmp37:
Ltmp38:
	mov	r2, #0
	mov	r5, r1
	mov	r4, r0
	bl	_p_24_plt_System_Xml_Linq_XContainer_CheckChildType_object_bool_llvm
	mov	r0, r5
	bl	_p_30_plt_System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject_llvm
	cmp	r0, #0
	beq	LBB26_2
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC26_1+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC26_1+8))
	ldr	r2, [r0]
LPC26_1:
	add	r1, pc, r1
	ldr	r1, [r1, #104]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r1
	bne	LBB26_3
LBB26_2:
	ldr	r1, [r0]
	str	r4, [r0, #8]
	ldr	r1, [r4, #40]
	cmp	r1, #0
	streq	r0, [r4, #40]
	streq	r0, [r4, #44]
	popeq	{r4, r5, r7, pc}
	ldr	r1, [r4, #44]
	ldr	r2, [r1]
	str	r0, [r1, #36]
	ldr	r1, [r4, #44]
	str	r1, [r0, #32]
	str	r0, [r4, #44]
	pop	{r4, r5, r7, pc}
Ltmp39:
LBB26_3:
	ldr	r0, LCPI26_0
LPC26_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI26_0:
	.long	Ltmp39-(LPC26_0+8)
	.end_data_region
Leh_func_end26:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool:
Leh_func_begin27:
	mov	r0, #0
	bx	lr
Leh_func_end27:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer_Nodes
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer_Nodes:
Leh_func_begin28:
	push	{r4, r7, lr}
Ltmp40:
	add	r7, sp, #4
Ltmp41:
Ltmp42:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC28_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC28_0+8))
LPC28_0:
	add	r0, pc, r0
	ldr	r0, [r0, #120]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mvn	r1, #1
	str	r4, [r0, #16]
	str	r1, [r0, #28]
	pop	{r4, r7, pc}
Leh_func_end28:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin29:
	push	{r4, r5, r6, r7, lr}
Ltmp43:
	add	r7, sp, #12
Ltmp44:
Ltmp45:
	mov	r4, r2
	mov	r5, r1
	mov	r6, r0
	b	LBB29_2
LBB29_1:
	ldr	r0, [r5]
	ldr	r1, [r0, #180]
	mov	r0, r5
	blx	r1
	cmp	r0, #15
	popeq	{r4, r5, r6, r7, pc}
	mov	r0, r5
	mov	r1, r4
	bl	_p_31_plt_System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	mov	r1, r0
	mov	r0, r6
	bl	_p_32_plt_System_Xml_Linq_XContainer_Add_object_llvm
LBB29_2:
	ldr	r0, [r5]
	ldr	r1, [r0, #220]
	mov	r0, r5
	blx	r1
	tst	r0, #255
	beq	LBB29_1
	pop	{r4, r5, r6, r7, pc}
Leh_func_end29:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XNode_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XNode_get_Current:
Leh_func_begin30:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end30:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerator_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerator_get_Current:
Leh_func_begin31:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end31:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0__ctor:
Leh_func_begin32:
	bx	lr
Leh_func_end32:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_MoveNext
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_MoveNext:
Leh_func_begin33:
	ldr	r2, [r0, #28]
	mvn	r1, #0
	str	r1, [r0, #28]
	mov	r1, #0
	cmp	r2, #1
	bhi	LBB33_6
	bne	LBB33_3
	ldr	r2, [r0, #12]
	b	LBB33_4
LBB33_3:
	ldr	r2, [r0, #16]
	ldr	r2, [r2, #40]
LBB33_4:
	str	r2, [r0, #8]
	ldr	r2, [r0, #8]
	cmp	r2, #0
	beq	LBB33_7
	ldr	r1, [r0, #8]
	ldr	r2, [r1]
	ldr	r1, [r1, #36]
	str	r1, [r0, #12]
	ldr	r1, [r0, #8]
	str	r1, [r0, #20]
	mov	r1, #1
	ldrb	r2, [r0, #24]
	cmp	r2, #0
	beq	LBB33_8
LBB33_6:
	mov	r0, r1
	bx	lr
LBB33_7:
	mvn	r2, #0
	str	r2, [r0, #28]
	mov	r0, r1
	bx	lr
LBB33_8:
	mov	r1, #1
	str	r1, [r0, #28]
	mov	r0, r1
	bx	lr
Leh_func_end33:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Dispose
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Dispose:
Leh_func_begin34:
	mov	r1, #1
	mvn	r2, #0
	strb	r1, [r0, #24]
	str	r2, [r0, #28]
	bx	lr
Leh_func_end34:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Reset
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Reset:
Leh_func_begin35:
	push	{r7, lr}
Ltmp46:
	mov	r7, sp
Ltmp47:
Ltmp48:
	movw	r0, #629
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end35:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerable_GetEnumerator:
Leh_func_begin36:
	push	{r7, lr}
Ltmp49:
	mov	r7, sp
Ltmp50:
Ltmp51:
	bl	_p_34_plt_System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator_llvm
	pop	{r7, pc}
Leh_func_end36:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator:
Leh_func_begin37:
	push	{r4, r7, lr}
Ltmp52:
	add	r7, sp, #4
Ltmp53:
Ltmp54:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB37_2
	add	r0, r4, #28
	mov	r1, #0
	mvn	r2, #1
	bl	_p_35_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm
	cmn	r0, #2
	moveq	r0, r4
	popeq	{r4, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC37_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC37_1+8))
LPC37_1:
	add	r0, pc, r0
	ldr	r0, [r0, #120]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #16]
	str	r1, [r0, #16]
	pop	{r4, r7, pc}
Ltmp55:
LBB37_2:
	ldr	r0, LCPI37_0
LPC37_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI37_0:
	.long	Ltmp55-(LPC37_0+8)
	.end_data_region
Leh_func_end37:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string:
Leh_func_begin38:
	str	r2, [r0, #8]
	str	r3, [r0, #12]
	str	r1, [r0, #16]
	bx	lr
Leh_func_end38:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration:
Leh_func_begin39:
	push	{r7, lr}
Ltmp56:
	mov	r7, sp
Ltmp57:
Ltmp58:
	cmp	r1, #0
	ldrne	r2, [r1, #16]
	strne	r2, [r0, #16]
	ldrne	r2, [r1, #8]
	strne	r2, [r0, #8]
	ldrne	r1, [r1, #12]
	strne	r1, [r0, #12]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC39_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC39_0+8))
LPC39_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end39:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Encoding
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Encoding:
Leh_func_begin40:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end40:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Standalone
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Standalone:
Leh_func_begin41:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end41:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Version
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Version:
Leh_func_begin42:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end42:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDeclaration_ToString
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDeclaration_ToString:
Leh_func_begin43:
	push	{r4, r5, r6, r7, lr}
Ltmp59:
	add	r7, sp, #12
Ltmp60:
	push	{r10}
Ltmp61:
	movw	r10, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC43_0+8))
	mov	r5, r0
	mov	r1, #11
	movt	r10, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC43_0+8))
LPC43_0:
	add	r10, pc, r10
	ldr	r0, [r10, #124]
	bl	_p_21_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r2, [r10, #128]
	mov	r1, #0
	mov	r6, #0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #16]
	ldr	r2, [r10, #144]
	mov	r1, #1
	cmp	r0, #0
	moveq	r2, r0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #16]
	mov	r2, #0
	mov	r1, #2
	cmp	r0, #0
	ldrne	r2, [r5, #16]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #16]
	ldr	r2, [r10, #64]
	mov	r1, #3
	cmp	r0, #0
	moveq	r2, r0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r2, [r5, #8]
	mov	r1, #4
	cmp	r2, #0
	ldrne	r2, [r10, #140]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #8]
	mov	r1, #5
	cmp	r0, #0
	ldrne	r6, [r5, #8]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	mov	r2, r6
	blx	r3
	ldr	r0, [r5, #8]
	ldr	r2, [r10, #64]
	mov	r1, #6
	cmp	r0, #0
	moveq	r2, r0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #12]
	ldr	r2, [r10, #136]
	mov	r1, #7
	cmp	r0, #0
	moveq	r2, r0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #12]
	mov	r2, #0
	mov	r1, #8
	cmp	r0, #0
	ldrne	r2, [r5, #12]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #12]
	ldr	r2, [r10, #64]
	mov	r1, #9
	cmp	r0, #0
	moveq	r2, r0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r2, [r10, #132]
	mov	r1, #10
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	mov	r0, r4
	bl	_p_36_plt_string_Concat_string___llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end43:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument__ctor:
Leh_func_begin44:
	bx	lr
Leh_func_end44:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_get_Declaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_get_Declaration:
Leh_func_begin45:
	ldr	r0, [r0, #48]
	bx	lr
Leh_func_end45:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_set_Declaration_System_Xml_Linq_XDeclaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_set_Declaration_System_Xml_Linq_XDeclaration:
Leh_func_begin46:
	str	r1, [r0, #48]
	bx	lr
Leh_func_end46:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_get_NodeType:
Leh_func_begin47:
	mov	r0, #9
	bx	lr
Leh_func_end47:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_Load_System_IO_Stream
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_Load_System_IO_Stream:
Leh_func_begin48:
	push	{r4, r5, r7, lr}
Ltmp62:
	add	r7, sp, #8
Ltmp63:
Ltmp64:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC48_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC48_0+8))
LPC48_0:
	add	r0, pc, r0
	ldr	r0, [r0, #160]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_38_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm
	mov	r0, r5
	mov	r1, #0
	bl	_p_39_plt_System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end48:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin49:
	push	{r4, r5, r6, r7, lr}
Ltmp65:
	add	r7, sp, #12
Ltmp66:
Ltmp67:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC49_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC49_0+8))
LPC49_0:
	add	r0, pc, r0
	ldr	r0, [r0, #168]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r4
	mov	r6, r0
	bl	_p_43_plt_System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end49:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin50:
	push	{r4, r5, r6, r7, lr}
Ltmp68:
	add	r7, sp, #12
Ltmp69:
	push	{r10, r11}
Ltmp70:
	sub	sp, sp, #8
	mov	r6, r1
	mov	r4, r0
	mov	r5, r2
	ldr	r0, [r6]
	ldr	r1, [r0, #172]
	mov	r0, r6
	blx	r1
	cmp	r0, #0
	bne	LBB50_2
	ldr	r0, [r6]
	ldr	r1, [r0, #96]
	mov	r0, r6
	blx	r1
LBB50_2:
	mov	r0, r4
	mov	r1, r6
	mov	r2, r5
	str	r5, [sp, #4]
	bl	_p_44_plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	ldr	r0, [r6]
	ldr	r1, [r0, #180]
	mov	r0, r6
	blx	r1
	cmp	r0, #17
	bne	LBB50_4
	mov	r11, r4
	movw	r4, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC50_0+8))
	movt	r4, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC50_0+8))
	ldr	r0, [r6]
LPC50_0:
	add	r4, pc, r4
	ldr	r1, [r4, #172]
	ldr	r2, [r0, #144]
	mov	r0, r6
	blx	r2
	str	r0, [sp]
	ldr	r1, [r4, #176]
	ldr	r0, [r6]
	ldr	r2, [r0, #144]
	mov	r0, r6
	blx	r2
	mov	r5, r0
	ldr	r0, [r6]
	ldr	r1, [r4, #180]
	ldr	r2, [r0, #144]
	mov	r0, r6
	blx	r2
	mov	r10, r0
	ldr	r0, [r4, #184]
	mov	r4, r11
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp]
	mov	r2, r5
	mov	r3, r10
	mov	r11, r0
	bl	_p_47_plt_System_Xml_Linq_XDeclaration__ctor_string_string_string_llvm
	str	r11, [r4, #48]
	ldr	r0, [r6]
	ldr	r1, [r0, #96]
	mov	r0, r6
	blx	r1
LBB50_4:
	ldr	r2, [sp, #4]
	mov	r0, r4
	mov	r1, r6
	bl	_p_45_plt_System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	mov	r0, r4
	bl	_p_46_plt_System_Xml_Linq_XDocument_get_Root_llvm
	cmp	r0, #0
	subne	sp, r7, #20
	popne	{r10, r11}
	popne	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC50_1+8))
	movw	r1, #343
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC50_1+8))
LPC50_1:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end50:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string:
Leh_func_begin51:
	push	{r7, lr}
Ltmp71:
	mov	r7, sp
Ltmp72:
Ltmp73:
	ldr	r9, [r0, #8]
	cmp	r9, #1
	blt	LBB51_7
	mov	r2, #0
	mov	lr, r0
	b	LBB51_3
LBB51_2:
	add	r2, r2, #1
	add	lr, lr, #2
	cmp	r2, r9
	popge	{r7, pc}
LBB51_3:
	ldr	r1, [r0, #8]
	cmp	r1, r2
	bls	LBB51_8
	ldrh	r12, [lr, #12]
	sub	r1, r12, #9
	sub	r3, r12, #11
	cmp	r1, #5
	mov	r1, #0
	movlo	r1, #1
	cmp	r3, #1
	mov	r3, #0
	movhi	r3, #1
	tst	r3, r1
	bne	LBB51_2
	cmp	r12, #32
	beq	LBB51_2
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC51_1+8))
	movw	r1, #409
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC51_1+8))
LPC51_1:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
LBB51_7:
	pop	{r7, pc}
Ltmp74:
LBB51_8:
	ldr	r0, LCPI51_0
LPC51_0:
	add	r1, pc, r0
	movw	r0, #600
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI51_0:
	.long	Ltmp74-(LPC51_0+8)
	.end_data_region
Leh_func_end51:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool:
Leh_func_begin52:
	push	{r7, lr}
Ltmp75:
	mov	r7, sp
Ltmp76:
Ltmp77:
	ldr	r2, [r7, #8]
	bl	_p_48_plt_System_Xml_Linq_XDocument_VerifyAddedNode_object_bool_llvm
	mov	r0, #0
	pop	{r7, pc}
Leh_func_end52:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocument_VerifyAddedNode_object_bool
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocument_VerifyAddedNode_object_bool:
Leh_func_begin53:
	push	{r4, r5, r6, r7, lr}
Ltmp78:
	add	r7, sp, #12
Ltmp79:
Ltmp80:
	mov	r6, r1
	mov	r4, r2
	mov	r5, r0
	cmp	r6, #0
	beq	LBB53_22
	beq	LBB53_5
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_2+8))
	ldr	r1, [r6]
LPC53_2:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB53_6
	cmp	r6, #0
	beq	LBB53_5
	ldr	r1, [r6]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB53_23
LBB53_5:
	mov	r0, r6
	bl	_p_50_plt_System_Xml_Linq_XDocument_ValidateWhitespace_string_llvm
LBB53_6:
	cmp	r6, #0
	beq	LBB53_21
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_3+8))
	ldr	r2, [r6]
LPC53_3:
	add	r0, pc, r0
	ldr	r1, [r0, #192]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	beq	LBB53_19
	cmp	r6, #0
	beq	LBB53_16
	ldr	r2, [r6]
	ldr	r1, [r0, #156]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	beq	LBB53_16
	cmp	r6, #0
	beq	LBB53_12
	ldr	r1, [r6]
	ldr	r0, [r0, #20]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB53_18
LBB53_12:
	mov	r0, r5
	bl	_p_46_plt_System_Xml_Linq_XDocument_get_Root_llvm
	cmp	r0, #0
	bne	LBB53_24
	mov	r0, r5
	bl	_p_49_plt_System_Xml_Linq_XDocument_get_DocumentType_llvm
	cmp	r0, #0
	cmpne	r4, #0
	beq	LBB53_18
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_6+8))
	movw	r1, #885
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_6+8))
LPC53_6:
	ldr	r0, [pc, r0]
LBB53_15:
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
LBB53_16:
	mov	r0, r5
	bl	_p_49_plt_System_Xml_Linq_XDocument_get_DocumentType_llvm
	cmp	r0, #0
	bne	LBB53_25
	mov	r0, r5
	bl	_p_46_plt_System_Xml_Linq_XDocument_get_Root_llvm
	cmp	r0, #0
	bne	LBB53_26
LBB53_18:
	pop	{r4, r5, r6, r7, pc}
LBB53_19:
	cmp	r6, #0
	beq	LBB53_21
	ldr	r1, [r6]
	ldr	r0, [r0, #192]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB53_27
LBB53_21:
	ldr	r0, [r6]
	ldr	r0, [r6, #40]
	bl	_p_50_plt_System_Xml_Linq_XDocument_ValidateWhitespace_string_llvm
	pop	{r4, r5, r6, r7, pc}
LBB53_22:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_4+8))
	movw	r1, #525
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_4+8))
LPC53_4:
	ldr	r0, [pc, r0]
	b	LBB53_15
Ltmp81:
LBB53_23:
	ldr	r0, LCPI53_0
LPC53_0:
	add	r1, pc, r0
	b	LBB53_28
LBB53_24:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_5+8))
	movw	r1, #801
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_5+8))
LPC53_5:
	ldr	r0, [pc, r0]
	b	LBB53_15
LBB53_25:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_8+8))
	movw	r1, #581
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_8+8))
LPC53_8:
	ldr	r0, [pc, r0]
	b	LBB53_15
LBB53_26:
	cmp	r4, #0
	popne	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC53_7+8))
	movw	r1, #683
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC53_7+8))
LPC53_7:
	ldr	r0, [pc, r0]
	b	LBB53_15
Ltmp82:
LBB53_27:
	ldr	r0, LCPI53_1
LPC53_1:
	add	r1, pc, r0
LBB53_28:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI53_0:
	.long	Ltmp81-(LPC53_0+8)
LCPI53_1:
	.long	Ltmp82-(LPC53_1+8)
	.end_data_region
Leh_func_end53:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string:
Leh_func_begin54:
	add	r9, r0, #40
	stm	r9, {r1, r2, r3}
	ldr	r2, [sp]
	str	r2, [r0, #52]
	bx	lr
Leh_func_end54:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType:
Leh_func_begin55:
	push	{r7, lr}
Ltmp83:
	mov	r7, sp
Ltmp84:
Ltmp85:
	cmp	r1, #0
	ldrne	r2, [r1, #40]
	strne	r2, [r0, #40]
	ldrne	r2, [r1, #44]
	strne	r2, [r0, #44]
	ldrne	r2, [r1, #48]
	strne	r2, [r0, #48]
	ldrne	r1, [r1, #52]
	strne	r1, [r0, #52]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC55_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC55_0+8))
LPC55_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end55:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_Name
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_Name:
Leh_func_begin56:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end56:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_PublicId
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_PublicId:
Leh_func_begin57:
	ldr	r0, [r0, #44]
	bx	lr
Leh_func_end57:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_SystemId
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_SystemId:
Leh_func_begin58:
	ldr	r0, [r0, #48]
	bx	lr
Leh_func_end58:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_InternalSubset
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_InternalSubset:
Leh_func_begin59:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end59:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_NodeType:
Leh_func_begin60:
	mov	r0, #10
	bx	lr
Leh_func_end60:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XDocumentType_WriteTo_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XDocumentType_WriteTo_System_Xml_XmlWriter:
Leh_func_begin61:
	push	{r4, r5, r7, lr}
Ltmp86:
	add	r7, sp, #8
Ltmp87:
Ltmp88:
	sub	sp, sp, #4
	mov	r4, r1
	mov	r5, r0
	bl	_p_51_plt_System_Xml_Linq_XObject_get_Document_llvm
	ldr	r1, [r0]
	bl	_p_46_plt_System_Xml_Linq_XDocument_get_Root_llvm
	cmp	r0, #0
	beq	LBB61_2
	ldr	r1, [r0]
	ldr	r0, [r0, #48]
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	ldr	r2, [r5, #44]
	ldr	r3, [r5, #48]
	ldr	r0, [r5, #52]
	ldr	r5, [r4]
	ldr	r5, [r5, #108]
	str	r0, [sp]
	mov	r0, r4
	blx	r5
LBB61_2:
	sub	sp, r7, #8
	pop	{r4, r5, r7, pc}
Leh_func_end61:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement:
Leh_func_begin62:
	push	{r4, r5, r7, lr}
Ltmp89:
	add	r7, sp, #8
Ltmp90:
Ltmp91:
	mov	r5, r1
	mov	r4, r0
	mov	r0, #1
	strb	r0, [r4, #60]
	cmp	r5, #0
	beq	LBB62_2
	ldr	r0, [r5, #48]
	str	r0, [r4, #48]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_52_plt_System_Xml_Linq_XElement_Attributes_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_32_plt_System_Xml_Linq_XContainer_Add_object_llvm
	mov	r0, r5
	bl	_p_37_plt_System_Xml_Linq_XContainer_Nodes_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_32_plt_System_Xml_Linq_XContainer_Add_object_llvm
	pop	{r4, r5, r7, pc}
LBB62_2:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC62_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC62_0+8))
LPC62_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end62:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName:
Leh_func_begin63:
	push	{r4, r5, r7, lr}
Ltmp92:
	add	r7, sp, #8
Ltmp93:
Ltmp94:
	mov	r4, r1
	mov	r5, r0
	mov	r0, #1
	mov	r1, #0
	strb	r0, [r5, #60]
	mov	r0, r4
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	streq	r4, [r5, #48]
	popeq	{r4, r5, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC63_0+8))
	mov	r1, #13
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC63_0+8))
LPC63_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end63:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_FirstAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_FirstAttribute:
Leh_func_begin64:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end64:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_set_FirstAttribute_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_set_FirstAttribute_System_Xml_Linq_XAttribute:
Leh_func_begin65:
	str	r1, [r0, #52]
	bx	lr
Leh_func_end65:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_LastAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_LastAttribute:
Leh_func_begin66:
	ldr	r0, [r0, #56]
	bx	lr
Leh_func_end66:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_set_LastAttribute_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_set_LastAttribute_System_Xml_Linq_XAttribute:
Leh_func_begin67:
	str	r1, [r0, #56]
	bx	lr
Leh_func_end67:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_HasAttributes
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_HasAttributes:
Leh_func_begin68:
	ldr	r0, [r0, #52]
	cmp	r0, #0
	movne	r0, #1
	bx	lr
Leh_func_end68:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_IsEmpty
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_IsEmpty:
Leh_func_begin69:
	push	{r4, r5, r7, lr}
Ltmp95:
	add	r7, sp, #8
Ltmp96:
	push	{r8}
Ltmp97:
	mov	r4, r0
	bl	_p_37_plt_System_Xml_Linq_XContainer_Nodes_llvm
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC69_0+8))
	movt	r5, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC69_0+8))
LPC69_0:
	add	r5, pc, r5
	ldr	r1, [r5, #148]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	ldr	r1, [r0]
	ldr	r2, [r5, #92]
	sub	r1, r1, #60
	mov	r8, r2
	ldr	r1, [r1]
	blx	r1
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	ldrbeq	r0, [r4, #60]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end69:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_Name
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_Name:
Leh_func_begin70:
	ldr	r0, [r0, #48]
	bx	lr
Leh_func_end70:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_get_NodeType:
Leh_func_begin71:
	mov	r0, #1
	bx	lr
Leh_func_end71:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName:
Leh_func_begin72:
	push	{r4, r5, r6, r7, lr}
Ltmp98:
	add	r7, sp, #12
Ltmp99:
Ltmp100:
	ldr	r6, [r0, #52]
	mov	r5, r1
	mov	r4, #0
	b	LBB72_2
LBB72_1:
	ldr	r6, [r6, #40]
LBB72_2:
	cmp	r6, #0
	beq	LBB72_5
	ldr	r0, [r6]
	mov	r1, r5
	ldr	r0, [r6, #32]
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	beq	LBB72_1
	mov	r4, r6
LBB72_5:
	mov	r0, r4
	pop	{r4, r5, r6, r7, pc}
Leh_func_end72:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_Attributes
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_Attributes:
Leh_func_begin73:
	push	{r4, r7, lr}
Ltmp101:
	add	r7, sp, #4
Ltmp102:
Ltmp103:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC73_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC73_0+8))
LPC73_0:
	add	r0, pc, r0
	ldr	r0, [r0, #196]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mvn	r1, #1
	str	r4, [r0, #16]
	str	r1, [r0, #28]
	pop	{r4, r7, pc}
Leh_func_end73:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin74:
	push	{r4, r5, r6, r7, lr}
Ltmp104:
	add	r7, sp, #12
Ltmp105:
	push	{r10, r11}
Ltmp106:
	mov	r4, r0
	mov	r10, r1
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	cmp	r0, #1
	bne	LBB74_14
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r5
	bl	_p_53_plt_System_Xml_Linq_XName_Get_string_string_llvm
	movw	r11, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC74_0+8))
	mov	r5, r0
	movt	r11, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC74_0+8))
LPC74_0:
	add	r11, pc, r11
	ldr	r0, [r11, #200]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_54_plt_System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName_llvm
	mov	r0, r6
	mov	r1, r4
	mov	r2, r10
	bl	_p_44_plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #104]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	beq	LBB74_10
	ldr	r11, [r11, #16]
LBB74_3:
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r1, r11
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB74_5
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	bl	_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	bne	LBB74_6
LBB74_5:
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r5
	bl	_p_53_plt_System_Xml_Linq_XName_Get_string_string_llvm
	b	LBB74_7
LBB74_6:
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	ldr	r1, [r0]
	mov	r1, r11
	bl	_p_57_plt_System_Xml_Linq_XNamespace_GetName_string_llvm
LBB74_7:
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r2, r0
	mov	r0, r6
	mov	r1, r5
	bl	_p_55_plt_System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object_llvm
	ldr	r0, [r6, #56]
	mov	r2, r10
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_44_plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #100]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	bne	LBB74_3
	ldr	r0, [r4]
	ldr	r1, [r0, #108]
	mov	r0, r4
	blx	r1
LBB74_10:
	ldr	r0, [r4]
	ldr	r1, [r0, #212]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	beq	LBB74_12
	mov	r0, #1
	strb	r0, [r6, #60]
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
	b	LBB74_13
LBB74_12:
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
	mov	r0, r6
	mov	r1, r4
	mov	r2, r10
	bl	_p_45_plt_System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	mov	r0, #0
	strb	r0, [r6, #60]
LBB74_13:
	mov	r0, r6
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB74_14:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC74_1+8))
	movw	r1, #1013
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC74_1+8))
LPC74_1:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end74:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter:
Leh_func_begin75:
	push	{r7, lr}
Ltmp107:
	mov	r7, sp
Ltmp108:
Ltmp109:
	ldr	r2, [r0]
	ldr	r2, [r2, #64]
	blx	r2
	pop	{r7, pc}
Leh_func_end75:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object:
Leh_func_begin76:
	push	{r4, r5, r6, r7, lr}
Ltmp110:
	add	r7, sp, #12
Ltmp111:
	push	{r10}
Ltmp112:
	mov	r4, r2
	mov	r6, r1
	mov	r10, r0
	bl	_p_58_plt_System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName_llvm
	mov	r5, r0
	cmp	r4, #0
	beq	LBB76_3
	cmp	r5, #0
	beq	LBB76_5
	mov	r0, r4
	bl	_p_10_plt_System_Xml_Linq_XUtil_ToString_object_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_59_plt_System_Xml_Linq_XAttribute_set_Value_string_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB76_3:
	cmp	r5, #0
	beq	LBB76_6
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_62_plt_System_Xml_Linq_XAttribute_Remove_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB76_5:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC76_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC76_0+8))
LPC76_0:
	add	r0, pc, r0
	ldr	r0, [r0, #204]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, r4
	mov	r5, r0
	bl	_p_60_plt_System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object_llvm
	mov	r0, r10
	mov	r1, r5
	bl	_p_61_plt_System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute_llvm
LBB76_6:
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end76:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute:
Leh_func_begin77:
	push	{r4, r5, r7, lr}
Ltmp113:
	add	r7, sp, #8
Ltmp114:
Ltmp115:
	mov	r5, r1
	mov	r4, r0
	bl	_p_27_plt_System_Xml_Linq_XObject_OnAddingObject_object_llvm
	mov	r0, r5
	bl	_p_30_plt_System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB77_2
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC77_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC77_1+8))
	ldr	r2, [r1]
LPC77_1:
	add	r0, pc, r0
	ldr	r0, [r0, #208]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	bne	LBB77_6
LBB77_2:
	ldr	r0, [r1]
	str	r4, [r1, #8]
	ldr	r0, [r4, #52]
	cmp	r0, #0
	beq	LBB77_4
	ldr	r0, [r4, #56]
	ldr	r2, [r0]
	str	r1, [r0, #40]
	ldr	r0, [r4, #56]
	str	r0, [r1, #44]
	b	LBB77_5
LBB77_4:
	str	r1, [r4, #52]
LBB77_5:
	mov	r0, r4
	str	r1, [r4, #56]
	bl	_p_29_plt_System_Xml_Linq_XObject_OnAddedObject_object_llvm
	pop	{r4, r5, r7, pc}
Ltmp116:
LBB77_6:
	ldr	r0, LCPI77_0
LPC77_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI77_0:
	.long	Ltmp116-(LPC77_0+8)
	.end_data_region
Leh_func_end77:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool:
Leh_func_begin78:
	push	{r4, r5, r6, r7, lr}
Ltmp117:
	add	r7, sp, #12
Ltmp118:
	push	{r8, r10, r11}
Ltmp119:
	sub	sp, sp, #24
	movw	r11, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC78_1+8))
	mov	r5, r0
	mov	r6, r2
	mov	r10, r1
	movt	r11, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC78_1+8))
LPC78_1:
	add	r11, pc, r11
	ldr	r0, [r11, #224]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	cmp	r6, #0
	bne	LBB78_5
	ldr	r0, [r11, #256]
	ldr	r1, [r0]
	cmp	r1, #0
	bne	LBB78_3
	ldr	r0, [r11, #236]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r11, #260]
	str	r1, [r0, #20]
	ldr	r1, [r11, #264]
	str	r1, [r0, #28]
	ldr	r1, [r11, #248]
	str	r1, [r0, #12]
	ldr	r1, [r11, #256]
	str	r0, [r1]
	ldr	r0, [r11, #256]
	ldr	r1, [r0]
LBB78_3:
	ldr	r0, [r11, #252]
	mov	r8, r0
	mov	r0, r10
	bl	_p_68_plt_System_Linq_Enumerable_All_System_Xml_Linq_XAttribute_System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_System_Func_2_System_Xml_Linq_XAttribute_bool_llvm
	tst	r0, #255
	beq	LBB78_5
	ldr	r0, [r11, #212]
	ldr	r0, [r0]
	b	LBB78_9
LBB78_5:
	mov	r0, #0
	cmp	r4, #0
	str	r0, [r4, #8]
	ldrd	r0, r1, [r11, #228]
	str	r0, [sp, #20]
	beq	LBB78_10
	ldr	r3, [r11, #240]
	ldr	r2, [r11, #244]
	ldr	r0, [r11, #236]
	mov	r6, r10
	mov	r10, r1
	str	r3, [sp, #12]
	str	r2, [sp, #8]
	ldr	r3, [r11, #248]
	ldr	r2, [r11, #252]
	str	r0, [sp, #16]
	str	r3, [sp, #4]
	str	r2, [sp]
LBB78_7:
	ldr	r0, [r5]
	add	r11, r0, #1
	mov	r0, r10
	str	r11, [r5]
	bl	_p_67_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	ldr	r0, [sp, #20]
	str	r11, [r1, #8]
	bl	_p_23_plt_string_Concat_object_object_llvm
	str	r0, [r4, #8]
	ldr	r0, [sp, #16]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [sp, #8]
	ldr	r8, [sp]
	str	r0, [r1, #28]
	ldr	r0, [sp, #12]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [sp, #4]
	str	r0, [r1, #12]
	mov	r0, r6
	bl	_p_68_plt_System_Linq_Enumerable_All_System_Xml_Linq_XAttribute_System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_System_Func_2_System_Xml_Linq_XAttribute_bool_llvm
	tst	r0, #255
	beq	LBB78_7
	ldr	r0, [r4, #8]
LBB78_9:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB78_10:
	ldr	r0, [r5]
	add	r6, r0, #1
	mov	r0, r1
	str	r6, [r5]
	bl	_p_67_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	ldr	r0, [sp, #20]
	str	r6, [r1, #8]
	bl	_p_23_plt_string_Concat_object_object_llvm
	str	r0, [r4, #8]
Ltmp120:
	ldr	r0, LCPI78_0
LPC78_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI78_0:
	.long	Ltmp120-(LPC78_0+8)
	.end_data_region
Leh_func_end78:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace:
Leh_func_begin79:
	push	{r4, r5, r7, lr}
Ltmp121:
	add	r7, sp, #8
Ltmp122:
Ltmp123:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC79_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC79_0+8))
LPC79_0:
	add	r0, pc, r0
	ldr	r0, [r0, #276]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mvn	r1, #1
	str	r1, [r0, #36]
	strd	r4, r5, [r0, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end79:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter:
Leh_func_begin80:
	push	{r7, lr}
Ltmp124:
	mov	r7, sp
Ltmp125:
Ltmp126:
	bl	_p_80_plt_System_Xml_Linq_XElement_Save_System_Xml_XmlWriter_llvm
	pop	{r7, pc}
Leh_func_end80:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader:
Leh_func_begin81:
	push	{r7, lr}
Ltmp127:
	mov	r7, sp
Ltmp128:
Ltmp129:
	mov	r2, #0
	bl	_p_45_plt_System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	pop	{r7, pc}
Leh_func_end81:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_GetSchema
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_GetSchema:
Leh_func_begin82:
	mov	r0, #0
	bx	lr
Leh_func_end82:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__cctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__cctor:
Leh_func_begin83:
	push	{r4, r7, lr}
Ltmp130:
	add	r7, sp, #4
Ltmp131:
Ltmp132:
	movw	r4, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC83_0+8))
	movt	r4, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC83_0+8))
LPC83_0:
	add	r4, pc, r4
	ldr	r0, [r4, #288]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #292]
	ldr	r1, [r1]
	str	r1, [r0, #8]
	ldr	r1, [r4, #296]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end83:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacem__0_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacem__0_System_Xml_Linq_XAttribute:
Leh_func_begin84:
	push	{r4, r7, lr}
Ltmp133:
	add	r7, sp, #4
Ltmp134:
Ltmp135:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC84_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC84_0+8))
LPC84_0:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	bl	_p_81_plt_string_op_Inequality_string_string_llvm
	mov	r1, r0
	mov	r0, #1
	tst	r1, #255
	popne	{r4, r7, pc}
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	bl	_p_69_plt_System_Xml_Linq_XName_get_NamespaceName_llvm
	mov	r4, r0
	bl	_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r0, r0
	pop	{r4, r7, pc}
Leh_func_end84:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XAttribute_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XAttribute_get_Current:
Leh_func_begin85:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end85:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerator_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerator_get_Current:
Leh_func_begin86:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end86:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0__ctor:
Leh_func_begin87:
	bx	lr
Leh_func_end87:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_MoveNext
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_MoveNext:
Leh_func_begin88:
	ldr	r2, [r0, #28]
	mvn	r1, #0
	str	r1, [r0, #28]
	mov	r1, #0
	cmp	r2, #1
	bhi	LBB88_6
	bne	LBB88_3
	ldr	r2, [r0, #12]
	b	LBB88_4
LBB88_3:
	ldr	r2, [r0, #16]
	ldr	r2, [r2, #52]
LBB88_4:
	str	r2, [r0, #8]
	ldr	r2, [r0, #8]
	cmp	r2, #0
	beq	LBB88_7
	ldr	r1, [r0, #8]
	ldr	r2, [r1]
	ldr	r1, [r1, #40]
	str	r1, [r0, #12]
	ldr	r1, [r0, #8]
	str	r1, [r0, #20]
	mov	r1, #1
	ldrb	r2, [r0, #24]
	cmp	r2, #0
	beq	LBB88_8
LBB88_6:
	mov	r0, r1
	bx	lr
LBB88_7:
	mvn	r2, #0
	str	r2, [r0, #28]
	mov	r0, r1
	bx	lr
LBB88_8:
	mov	r1, #1
	str	r1, [r0, #28]
	mov	r0, r1
	bx	lr
Leh_func_end88:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Dispose
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Dispose:
Leh_func_begin89:
	mov	r1, #1
	mvn	r2, #0
	strb	r1, [r0, #24]
	str	r2, [r0, #28]
	bx	lr
Leh_func_end89:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Reset
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Reset:
Leh_func_begin90:
	push	{r7, lr}
Ltmp136:
	mov	r7, sp
Ltmp137:
Ltmp138:
	movw	r0, #629
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end90:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerable_GetEnumerator:
Leh_func_begin91:
	push	{r7, lr}
Ltmp139:
	mov	r7, sp
Ltmp140:
Ltmp141:
	bl	_p_82_plt_System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator_llvm
	pop	{r7, pc}
Leh_func_end91:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator:
Leh_func_begin92:
	push	{r4, r7, lr}
Ltmp142:
	add	r7, sp, #4
Ltmp143:
Ltmp144:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB92_2
	add	r0, r4, #28
	mov	r1, #0
	mvn	r2, #1
	bl	_p_35_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm
	cmn	r0, #2
	moveq	r0, r4
	popeq	{r4, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC92_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC92_1+8))
LPC92_1:
	add	r0, pc, r0
	ldr	r0, [r0, #196]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #16]
	str	r1, [r0, #16]
	pop	{r4, r7, pc}
Ltmp145:
LBB92_2:
	ldr	r0, LCPI92_0
LPC92_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI92_0:
	.long	Ltmp145-(LPC92_0+8)
	.end_data_region
Leh_func_end92:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__ctor:
Leh_func_begin93:
	bx	lr
Leh_func_end93:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__m__0_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__m__0_System_Xml_Linq_XAttribute:
Leh_func_begin94:
	push	{r4, r7, lr}
Ltmp146:
	add	r7, sp, #4
Ltmp147:
Ltmp148:
	mov	r4, r1
	ldr	r1, [r4]
	ldr	r1, [r4, #32]
	ldr	r2, [r1]
	ldr	r2, [r1, #8]
	ldr	r1, [r0, #8]
	mov	r0, r2
	bl	_p_81_plt_string_op_Inequality_string_string_llvm
	mov	r1, r0
	mov	r0, #1
	tst	r1, #255
	popne	{r4, r7, pc}
	ldr	r0, [r4, #32]
	ldr	r1, [r0]
	bl	_p_69_plt_System_Xml_Linq_XName_get_NamespaceName_llvm
	mov	r4, r0
	bl	_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r0, r0
	pop	{r4, r7, pc}
Leh_func_end94:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerator_string_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerator_string_get_Current:
Leh_func_begin95:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end95:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerator_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerator_get_Current:
Leh_func_begin96:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end96:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3__ctor:
Leh_func_begin97:
	bx	lr
Leh_func_end97:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Reset
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Reset:
Leh_func_begin98:
	push	{r7, lr}
Ltmp149:
	mov	r7, sp
Ltmp150:
Ltmp151:
	movw	r0, #629
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end98:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerable_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerable_GetEnumerator:
Leh_func_begin99:
	push	{r7, lr}
Ltmp152:
	mov	r7, sp
Ltmp153:
Ltmp154:
	bl	_p_83_plt_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator_llvm
	pop	{r7, pc}
Leh_func_end99:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator:
Leh_func_begin100:
	push	{r4, r7, lr}
Ltmp155:
	add	r7, sp, #4
Ltmp156:
Ltmp157:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB100_2
	add	r0, r4, #36
	mov	r1, #0
	mvn	r2, #1
	bl	_p_35_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm
	cmn	r0, #2
	moveq	r0, r4
	popeq	{r4, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC100_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC100_1+8))
LPC100_1:
	add	r0, pc, r0
	ldr	r0, [r0, #276]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #24]
	str	r1, [r0, #24]
	ldr	r1, [r4, #20]
	str	r1, [r0, #20]
	pop	{r4, r7, pc}
Ltmp158:
LBB100_2:
	ldr	r0, LCPI100_0
LPC100_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI100_0:
	.long	Ltmp158-(LPC100_0+8)
	.end_data_region
Leh_func_end100:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace:
Leh_func_begin101:
	push	{r4, r5, r7, lr}
Ltmp159:
	add	r7, sp, #8
Ltmp160:
Ltmp161:
	mov	r4, r0
	mov	r0, r1
	mov	r5, r2
	bl	_p_84_plt_System_Xml_XmlConvert_VerifyNCName_string_llvm
	str	r0, [r4, #8]
	str	r5, [r4, #12]
	pop	{r4, r5, r7, pc}
Leh_func_end101:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_get_LocalName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_get_LocalName:
Leh_func_begin102:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end102:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_get_Namespace
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_get_Namespace:
Leh_func_begin103:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end103:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName:
Leh_func_begin104:
	push	{r7, lr}
Ltmp162:
	mov	r7, sp
Ltmp163:
Ltmp164:
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	pop	{r7, pc}
Leh_func_end104:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_Equals_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_Equals_object:
Leh_func_begin105:
	push	{r4, r5, r6, r7, lr}
Ltmp165:
	add	r7, sp, #12
Ltmp166:
Ltmp167:
	mov	r4, r0
	cmp	r1, #0
	beq	LBB105_2
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC105_0+8))
	mov	r5, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC105_0+8))
	ldr	r2, [r1]
LPC105_0:
	add	r0, pc, r0
	ldr	r0, [r0, #300]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r0
	moveq	r5, r1
	b	LBB105_3
LBB105_2:
	mov	r5, r1
LBB105_3:
	mov	r0, r5
	mov	r1, #0
	mov	r6, #0
	bl	_p_85_plt_System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	beq	LBB105_5
	mov	r0, r4
	mov	r1, r5
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	uxtb	r6, r0
LBB105_5:
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end105:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_System_IEquatable_System_Xml_Linq_XName_Equals_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_System_IEquatable_System_Xml_Linq_XName_Equals_System_Xml_Linq_XName:
Leh_func_begin106:
	push	{r7, lr}
Ltmp168:
	mov	r7, sp
Ltmp169:
Ltmp170:
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	pop	{r7, pc}
Leh_func_end106:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_Get_string_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_Get_string_string:
Leh_func_begin107:
	push	{r4, r7, lr}
Ltmp171:
	add	r7, sp, #4
Ltmp172:
Ltmp173:
	mov	r4, r0
	mov	r0, r1
	bl	_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_57_plt_System_Xml_Linq_XNamespace_GetName_string_llvm
	pop	{r4, r7, pc}
Leh_func_end107:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_GetHashCode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_GetHashCode:
Leh_func_begin108:
	push	{r4, r5, r7, lr}
Ltmp174:
	add	r7, sp, #8
Ltmp175:
Ltmp176:
	mov	r4, r0
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	mov	r5, r0
	ldr	r0, [r4, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	eor	r0, r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end108:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName:
Leh_func_begin109:
	push	{r4, r5, r7, lr}
Ltmp177:
	add	r7, sp, #8
Ltmp178:
Ltmp179:
	mov	r5, r0
	mov	r4, r1
	mov	r0, #0
	cmp	r5, #0
	beq	LBB109_3
	cmp	r4, #0
	movne	r0, #1
	cmpne	r5, r4
	beq	LBB109_4
	ldr	r0, [r5, #8]
	ldr	r1, [r4, #8]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	mov	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r5, #12]
	ldr	r1, [r4, #12]
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	uxtb	r0, r0
	pop	{r4, r5, r7, pc}
LBB109_3:
	cmp	r4, #0
	moveq	r0, #1
LBB109_4:
	pop	{r4, r5, r7, pc}
Leh_func_end109:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName:
Leh_func_begin110:
	push	{r7, lr}
Ltmp180:
	mov	r7, sp
Ltmp181:
Ltmp182:
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	moveq	r0, #1
	pop	{r7, pc}
Leh_func_end110:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XName_ToString
	.align	2
_System_Xml_Linq__System_Xml_Linq_XName_ToString:
Leh_func_begin111:
	push	{r4, r5, r6, r7, lr}
Ltmp183:
	add	r7, sp, #12
Ltmp184:
Ltmp185:
	mov	r4, r0
	ldr	r5, [r4, #12]
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	cmp	r5, r0
	ldreq	r0, [r4, #8]
	popeq	{r4, r5, r6, r7, pc}
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC111_0+8))
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC111_0+8))
LPC111_0:
	add	r6, pc, r6
	ldr	r5, [r6, #304]
	ldr	r0, [r4, #12]
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	ldr	r2, [r6, #308]
	ldr	r3, [r4, #8]
	mov	r1, r0
	mov	r0, r5
	bl	_p_86_plt_string_Concat_string_string_string_string_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end111:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace__cctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace__cctor:
Leh_func_begin112:
	push	{r4, r5, r7, lr}
Ltmp186:
	add	r7, sp, #8
Ltmp187:
Ltmp188:
	movw	r5, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC112_0+8))
	movt	r5, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC112_0+8))
LPC112_0:
	add	r5, pc, r5
	ldr	r0, [r5, #312]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_87_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace__ctor_llvm
	ldr	r0, [r5, #316]
	str	r4, [r0]
	ldr	r0, [r5, #212]
	ldr	r0, [r0]
	bl	_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm
	ldr	r1, [r5, #320]
	str	r0, [r1]
	ldr	r0, [r5, #324]
	bl	_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm
	ldr	r1, [r5, #328]
	str	r0, [r1]
	ldr	r0, [r5, #332]
	bl	_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm
	ldr	r1, [r5, #336]
	str	r0, [r1]
	pop	{r4, r5, r7, pc}
Leh_func_end112:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_None
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_get_None:
Leh_func_begin113:
	push	{r7, lr}
Ltmp189:
	mov	r7, sp
Ltmp190:
Ltmp191:
	bl	_p_88_plt__class_init_System_Xml_Linq_XNamespace_llvm
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC113_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC113_0+8))
LPC113_0:
	add	r0, pc, r0
	ldr	r0, [r0, #320]
	ldr	r0, [r0]
	pop	{r7, pc}
Leh_func_end113:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_Xmlns
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_get_Xmlns:
Leh_func_begin114:
	push	{r7, lr}
Ltmp192:
	mov	r7, sp
Ltmp193:
Ltmp194:
	bl	_p_88_plt__class_init_System_Xml_Linq_XNamespace_llvm
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC114_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC114_0+8))
LPC114_0:
	add	r0, pc, r0
	ldr	r0, [r0, #336]
	ldr	r0, [r0]
	pop	{r7, pc}
Leh_func_end114:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace__ctor_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace__ctor_string:
Leh_func_begin115:
	push	{r7, lr}
Ltmp195:
	mov	r7, sp
Ltmp196:
Ltmp197:
	cmp	r1, #0
	strne	r1, [r0, #8]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC115_0+8))
	movw	r1, #1399
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC115_0+8))
LPC115_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end115:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_NamespaceName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_get_NamespaceName:
Leh_func_begin116:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end116:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_Equals_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_Equals_object:
Leh_func_begin117:
	push	{r4, r5, r6, r7, lr}
Ltmp198:
	add	r7, sp, #12
Ltmp199:
Ltmp200:
	mov	r4, r0
	mov	r6, #1
	cmp	r4, r1
	beq	LBB117_6
	cmp	r1, #0
	beq	LBB117_3
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC117_0+8))
	mov	r5, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC117_0+8))
	ldr	r2, [r1]
LPC117_0:
	add	r0, pc, r0
	ldr	r0, [r0, #352]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r0
	moveq	r5, r1
	b	LBB117_4
LBB117_3:
	mov	r5, r1
LBB117_4:
	mov	r0, r5
	mov	r1, #0
	mov	r6, #0
	bl	_p_97_plt_System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	tst	r0, #255
	beq	LBB117_6
	ldr	r0, [r4, #8]
	ldr	r1, [r5, #8]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r6, r0
LBB117_6:
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end117:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace:
Leh_func_begin118:
	push	{r7, lr}
Ltmp201:
	mov	r7, sp
Ltmp202:
Ltmp203:
	cmp	r0, #0
	beq	LBB118_2
	ldr	r2, [r0]
	ldr	r2, [r2, #44]
	blx	r2
	uxtb	r0, r0
	pop	{r7, pc}
LBB118_2:
	mov	r0, #0
	cmp	r1, #0
	moveq	r0, #1
	pop	{r7, pc}
Leh_func_end118:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace:
Leh_func_begin119:
	push	{r7, lr}
Ltmp204:
	mov	r7, sp
Ltmp205:
Ltmp206:
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	moveq	r0, #1
	pop	{r7, pc}
Leh_func_end119:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Implicit_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Implicit_string:
Leh_func_begin120:
	push	{r7, lr}
Ltmp207:
	mov	r7, sp
Ltmp208:
Ltmp209:
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	popeq	{r7, pc}
	mov	r0, r1
	bl	_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm
	pop	{r7, pc}
Leh_func_end120:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_GetHashCode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_GetHashCode:
Leh_func_begin121:
	push	{r7, lr}
Ltmp210:
	mov	r7, sp
Ltmp211:
Ltmp212:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	pop	{r7, pc}
Leh_func_end121:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNamespace_ToString
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNamespace_ToString:
Leh_func_begin122:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end122:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode__ctor:
Leh_func_begin123:
	bx	lr
Leh_func_end123:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_set_PreviousNode_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_set_PreviousNode_System_Xml_Linq_XNode:
Leh_func_begin124:
	str	r1, [r0, #32]
	bx	lr
Leh_func_end124:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_get_NextNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_get_NextNode:
Leh_func_begin125:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end125:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_set_NextNode_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_set_NextNode_System_Xml_Linq_XNode:
Leh_func_begin126:
	str	r1, [r0, #36]
	bx	lr
Leh_func_end126:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions:
Leh_func_begin127:
	push	{r4, r5, r6, r7, lr}
Ltmp213:
	add	r7, sp, #12
Ltmp214:
	push	{r10}
Ltmp215:
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC127_0+8))
	mov	r5, r0
	mov	r10, r1
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC127_0+8))
LPC127_0:
	add	r6, pc, r6
	ldr	r0, [r6, #356]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_98_plt_System_IO_StringWriter__ctor_llvm
	ldr	r0, [r6, #360]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	bl	_p_99_plt_System_Xml_XmlWriterSettings__ctor_llvm
	mov	r0, #0
	cmp	r10, #1
	mov	r1, r6
	str	r0, [r6, #24]
	movne	r0, #1
	strb	r0, [r6, #28]
	mov	r0, #1
	strb	r0, [r6, #36]
	mov	r0, r4
	bl	_p_100_plt_System_Xml_XmlWriter_Create_System_IO_TextWriter_System_Xml_XmlWriterSettings_llvm
	mov	r6, r0
	ldr	r0, [r5]
	mov	r1, r6
	ldr	r2, [r0, #64]
	mov	r0, r5
	blx	r2
	ldr	r0, [r6]
	ldr	r1, [r0, #132]
	mov	r0, r6
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #32]
	mov	r0, r4
	blx	r1
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end127:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin128:
	push	{r4, r5, r6, r7, lr}
Ltmp216:
	add	r7, sp, #12
Ltmp217:
	push	{r10, r11}
Ltmp218:
	sub	sp, sp, #12
	mov	r4, r0
	mov	r11, r1
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	sub	r0, r0, #1
	cmp	r0, #13
	bhi	LBB128_13
	bhi	LBB128_13
	lsl	r0, r0, #2
	adr	r1, LJTI128_0_0
	ldr	r0, [r0, r1]
	add	pc, r0, r1
LJTI128_0_0:
	.data_region jt32
	.long	LBB128_4-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_3-LJTI128_0_0
	.long	LBB128_5-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_6-LJTI128_0_0
	.long	LBB128_7-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_12-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_13-LJTI128_0_0
	.long	LBB128_3-LJTI128_0_0
	.long	LBB128_3-LJTI128_0_0
	.end_data_region
LBB128_3:
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_2+8))
LPC128_2:
	add	r0, pc, r0
	ldr	r0, [r0, #368]
	b	LBB128_8
LBB128_4:
	mov	r0, r4
	mov	r1, r11
	bl	_p_101_plt_System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	mov	r6, r0
	b	LBB128_11
LBB128_5:
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_3+8))
LPC128_3:
	add	r0, pc, r0
	ldr	r0, [r0, #372]
	b	LBB128_8
LBB128_6:
	ldr	r0, [r4]
	ldr	r1, [r0, #192]
	mov	r0, r4
	blx	r1
	mov	r10, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_4+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_4+8))
LPC128_4:
	add	r0, pc, r0
	ldr	r0, [r0, #376]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r2, r5
	mov	r6, r0
	bl	_p_102_plt_System_Xml_Linq_XProcessingInstruction__ctor_string_string_llvm
	b	LBB128_9
LBB128_7:
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_5+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_5+8))
LPC128_5:
	add	r0, pc, r0
	ldr	r0, [r0, #380]
LBB128_8:
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	str	r5, [r6, #40]
LBB128_9:
	mov	r0, r6
	mov	r1, r4
	mov	r2, r11
LBB128_10:
	bl	_p_44_plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #96]
	mov	r0, r4
	blx	r1
LBB128_11:
	mov	r0, r6
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB128_12:
	ldr	r0, [r4]
	ldr	r1, [r0, #192]
	mov	r0, r4
	blx	r1
	str	r0, [sp, #8]
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_6+8))
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_6+8))
LPC128_6:
	add	r6, pc, r6
	ldr	r1, [r6, #384]
	ldr	r0, [r4]
	ldr	r2, [r0, #144]
	mov	r0, r4
	blx	r2
	str	r0, [sp, #4]
	ldr	r0, [r4]
	ldr	r1, [r6, #388]
	ldr	r2, [r0, #144]
	mov	r0, r4
	blx	r2
	mov	r10, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	mov	r5, r11
	mov	r11, r0
	ldr	r0, [r6, #392]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	ldr	r2, [sp, #4]
	mov	r3, r10
	str	r11, [sp]
	mov	r6, r0
	bl	_p_103_plt_System_Xml_Linq_XDocumentType__ctor_string_string_string_string_llvm
	mov	r0, r6
	mov	r1, r4
	mov	r2, r5
	b	LBB128_10
LBB128_13:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_0+8))
	movw	r1, #1455
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_0+8))
LPC128_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC128_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC128_1+8))
LPC128_1:
	add	r0, pc, r0
	ldr	r0, [r0, #364]
	bl	_p_67_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	mov	r0, r5
	str	r4, [r1, #8]
	bl	_p_77_plt_string_Format_string_object_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end128:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_ToString
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_ToString:
Leh_func_begin129:
	push	{r7, lr}
Ltmp219:
	mov	r7, sp
Ltmp220:
Ltmp221:
	mov	r1, #0
	bl	_p_104_plt_System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions_llvm
	pop	{r7, pc}
Leh_func_end129:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode_CreateReader
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode_CreateReader:
Leh_func_begin130:
	push	{r4, r5, r7, lr}
Ltmp222:
	add	r7, sp, #8
Ltmp223:
Ltmp224:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC130_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC130_0+8))
LPC130_0:
	add	r0, pc, r0
	ldr	r0, [r0, #396]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_105_plt_System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end130:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNode__cctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNode__cctor:
Leh_func_begin131:
	push	{r4, r7, lr}
Ltmp225:
	add	r7, sp, #4
Ltmp226:
Ltmp227:
	movw	r4, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC131_0+8))
	movt	r4, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC131_0+8))
LPC131_0:
	add	r4, pc, r4
	ldr	r0, [r4, #400]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #404]
	str	r0, [r1]
	ldr	r0, [r4, #408]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #412]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end131:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer__ctor:
Leh_func_begin132:
	bx	lr
Leh_func_end132:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode:
Leh_func_begin133:
	push	{r4, r7, lr}
Ltmp228:
	add	r7, sp, #4
Ltmp229:
Ltmp230:
	sub	sp, sp, #12
	bic	sp, sp, #7
	mov	r3, #0
	str	r3, [sp, #4]
	str	r3, [sp]
	bl	_p_107_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	mov	r1, r0
	mvn	r0, #0
	cmp	r1, #6
	bhi	LBB133_6
	cmp	r1, #5
	bhi	LBB133_4
	lsl	r2, r1, #2
	adr	r1, LJTI133_0_0
	ldr	r2, [r2, r1]
	add	pc, r2, r1
LJTI133_0_0:
	.data_region jt32
	.long	LBB133_3-LJTI133_0_0
	.long	LBB133_5-LJTI133_0_0
	.long	LBB133_4-LJTI133_0_0
	.long	LBB133_6-LJTI133_0_0
	.long	LBB133_4-LJTI133_0_0
	.long	LBB133_6-LJTI133_0_0
	.end_data_region
LBB133_3:
	mov	r0, #0
	sub	sp, r7, #4
	pop	{r4, r7, pc}
LBB133_4:
	mov	r0, #1
	sub	sp, r7, #4
	pop	{r4, r7, pc}
LBB133_5:
	mov	r4, sp
	mov	r0, r4
	bl	_p_108_plt_System_DateTime_get_Now_llvm
	mov	r0, r4
	bl	_p_109_plt_System_DateTime_get_Ticks_llvm
	mov	r2, #2
	mov	r3, #0
	bl	_p_110_plt__jit_icall___emul_lrem_llvm
	eor	r0, r0, #1
	orrs	r0, r0, r1
	mvn	r0, #0
	moveq	r0, #1
LBB133_6:
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end133:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode:
Leh_func_begin134:
	push	{r4, r5, r6, r7, lr}
Ltmp231:
	add	r7, sp, #12
Ltmp232:
	push	{r10, r11}
Ltmp233:
	sub	sp, sp, #4
	mov	r4, r2
	mov	r10, r1
	mov	r3, r0
	mov	r11, #0
	cmp	r10, r4
	beq	LBB134_28
	mov	r5, r10
	ldr	r0, [r10]
	mov	r6, r4
	ldr	r1, [r5, #8]!
	ldr	r0, [r4]
	ldr	r0, [r6, #8]!
	cmp	r1, #0
	beq	LBB134_7
	cmp	r0, #0
	bne	LBB134_4
	mov	r0, r3
	mov	r1, r4
	mov	r2, r10
	mov	r11, r3
	bl	_p_107_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	mov	r3, r11
	mov	r11, r0
	cmp	r11, #7
	bls	LBB134_12
LBB134_4:
	str	r3, [sp]
	ldr	r1, [r5]
	ldr	r2, [r6]
	mov	r0, r3
	bl	_p_107_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	mov	r11, r0
	cmp	r11, #5
	bhi	LBB134_28
	sub	r0, r11, #1
	cmp	r0, #4
	bls	LBB134_10
	ldr	r0, [sp]
	mov	r1, r10
	mov	r2, r4
	mov	r3, #0
	b	LBB134_27
LBB134_7:
	mov	r11, #1
	cmp	r0, #0
	beq	LBB134_28
	ldr	r2, [r6]
	mov	r0, r3
	mov	r1, r10
	bl	_p_107_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	cmp	r0, #5
	bls	LBB134_15
	mov	r11, r0
	b	LBB134_28
LBB134_10:
	lsl	r1, r0, #2
	adr	r2, LJTI134_1_1
	ldr	r0, [sp]
	mov	r11, #1
	ldr	r1, [r1, r2]
	add	pc, r1, r2
LJTI134_1_1:
	.data_region jt32
	.long	LBB134_28-LJTI134_1_1
	.long	LBB134_11-LJTI134_1_1
	.long	LBB134_18-LJTI134_1_1
	.long	LBB134_19-LJTI134_1_1
	.long	LBB134_23-LJTI134_1_1
	.end_data_region
LBB134_11:
	ldr	r1, [r5]
	mov	r2, r4
	mov	r3, #2
	b	LBB134_27
LBB134_12:
	sub	r0, r11, #2
	cmp	r0, #5
	bhi	LBB134_28
	lsl	r0, r0, #2
	adr	r1, LJTI134_0_0
	ldr	r0, [r0, r1]
	add	pc, r0, r1
LJTI134_0_0:
	.data_region jt32
	.long	LBB134_14-LJTI134_0_0
	.long	LBB134_30-LJTI134_0_0
	.long	LBB134_31-LJTI134_0_0
	.long	LBB134_32-LJTI134_0_0
	.long	LBB134_33-LJTI134_0_0
	.long	LBB134_34-LJTI134_0_0
	.end_data_region
LBB134_14:
	mov	r11, #3
	b	LBB134_28
LBB134_15:
	cmp	r0, #5
	bhi	LBB134_35
	lsl	r1, r0, #2
	adr	r0, LJTI134_2_2
	ldr	r1, [r1, r0]
	add	pc, r1, r0
LJTI134_2_2:
	.data_region jt32
	.long	LBB134_29-LJTI134_2_2
	.long	LBB134_28-LJTI134_2_2
	.long	LBB134_35-LJTI134_2_2
	.long	LBB134_17-LJTI134_2_2
	.long	LBB134_35-LJTI134_2_2
	.long	LBB134_17-LJTI134_2_2
	.end_data_region
LBB134_17:
	mov	r11, #5
	b	LBB134_28
LBB134_18:
	ldr	r2, [r6]
	mov	r1, r10
	mov	r3, #3
	b	LBB134_27
LBB134_19:
	ldr	r1, [r10]
	ldr	r2, [r10, #8]
	b	LBB134_21
LBB134_20:
	ldr	r10, [r5]
	mov	r5, r10
	ldr	r1, [r10]
	ldr	r2, [r5, #8]!
LBB134_21:
	ldr	r1, [r6]
	cmp	r2, r1
	bne	LBB134_20
	mov	r1, r10
	mov	r2, r4
	mov	r3, #4
	b	LBB134_27
LBB134_23:
	ldr	r1, [r4]
	ldr	r2, [r4, #8]
	b	LBB134_25
LBB134_24:
	ldr	r4, [r6]
	mov	r6, r4
	ldr	r1, [r4]
	ldr	r2, [r6, #8]!
LBB134_25:
	ldr	r1, [r5]
	cmp	r2, r1
	bne	LBB134_24
	mov	r1, r10
	mov	r2, r4
	mov	r3, #5
LBB134_27:
	bl	_p_111_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult_llvm
	mov	r11, r0
LBB134_28:
	mov	r0, r11
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB134_29:
	mov	r11, #3
	b	LBB134_28
LBB134_30:
	mov	r11, #2
	b	LBB134_28
LBB134_31:
	mov	r11, #5
	b	LBB134_28
LBB134_32:
	mov	r11, #4
	b	LBB134_28
LBB134_33:
	mov	r11, #7
	b	LBB134_28
LBB134_34:
	mov	r11, #6
	b	LBB134_28
LBB134_35:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC134_0+8))
	movw	r1, #1517
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC134_0+8))
LPC134_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #576
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end134:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult:
Leh_func_begin135:
	cmp	r1, r2
	beq	LBB135_6
	ldr	r0, [r1]
	ldr	r0, [r1, #36]
	b	LBB135_3
LBB135_2:
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
LBB135_3:
	cmp	r0, #0
	beq	LBB135_5
	mov	r3, #7
	cmp	r0, r2
	beq	LBB135_6
	b	LBB135_2
LBB135_5:
	mov	r3, #6
LBB135_6:
	mov	r0, r3
	bx	lr
Leh_func_end135:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_object_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_object_object:
Leh_func_begin136:
	push	{r7, lr}
Ltmp234:
	mov	r7, sp
Ltmp235:
Ltmp236:
	cmp	r1, #0
	beq	LBB136_2
	movw	r3, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC136_3+8))
	movt	r3, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC136_3+8))
LPC136_3:
	add	r3, pc, r3
	ldr	r9, [r3, #104]
	ldr	r3, [r1]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r9
	bne	LBB136_5
LBB136_2:
	cmp	r2, #0
	beq	LBB136_4
	movw	r3, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC136_2+8))
	movt	r3, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC136_2+8))
LPC136_2:
	add	r3, pc, r3
	ldr	r9, [r3, #104]
	ldr	r3, [r2]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r9
	bne	LBB136_6
LBB136_4:
	bl	_p_112_plt_System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	pop	{r7, pc}
Ltmp237:
LBB136_5:
	ldr	r0, LCPI136_0
LPC136_0:
	add	r1, pc, r0
	b	LBB136_7
Ltmp238:
LBB136_6:
	ldr	r0, LCPI136_1
LPC136_1:
	add	r1, pc, r0
LBB136_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI136_0:
	.long	Ltmp237-(LPC136_0+8)
LCPI136_1:
	.long	Ltmp238-(LPC136_1+8)
	.end_data_region
Leh_func_end136:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer__ctor:
Leh_func_begin137:
	bx	lr
Leh_func_end137:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute:
Leh_func_begin138:
	push	{r4, r5, r6, r7, lr}
Ltmp239:
	add	r7, sp, #12
Ltmp240:
Ltmp241:
	mov	r6, r1
	mov	r4, r2
	mov	r5, #0
	cmp	r6, #0
	beq	LBB138_5
	cmp	r4, #0
	beq	LBB138_4
	ldr	r0, [r6]
	ldr	r0, [r6, #32]
	ldr	r1, [r4]
	ldr	r1, [r4, #32]
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	beq	LBB138_4
	mov	r0, r6
	bl	_p_64_plt_System_Xml_Linq_XAttribute_get_Value_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_64_plt_System_Xml_Linq_XAttribute_get_Value_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r5, r0
LBB138_4:
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
LBB138_5:
	cmp	r4, #0
	moveq	r5, #1
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Leh_func_end138:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration:
Leh_func_begin139:
	push	{r4, r5, r6, r7, lr}
Ltmp242:
	add	r7, sp, #12
Ltmp243:
Ltmp244:
	mov	r6, r1
	mov	r5, r2
	mov	r4, #0
	cmp	r6, #0
	beq	LBB139_5
	cmp	r5, #0
	beq	LBB139_6
	ldr	r0, [r6]
	ldr	r0, [r6, #16]
	ldr	r1, [r5]
	ldr	r1, [r5, #16]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB139_6
	ldr	r0, [r6, #8]
	ldr	r1, [r5, #8]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB139_6
	ldr	r0, [r6, #12]
	ldr	r1, [r5, #12]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r4, r0
	mov	r0, r4
	pop	{r4, r5, r6, r7, pc}
LBB139_5:
	cmp	r5, #0
	moveq	r4, #1
LBB139_6:
	mov	r0, r4
	pop	{r4, r5, r6, r7, pc}
Leh_func_end139:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_object_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_object_object:
Leh_func_begin140:
	push	{r7, lr}
Ltmp245:
	mov	r7, sp
Ltmp246:
Ltmp247:
	cmp	r1, #0
	beq	LBB140_2
	movw	r3, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC140_3+8))
	movt	r3, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC140_3+8))
LPC140_3:
	add	r3, pc, r3
	ldr	r9, [r3, #104]
	ldr	r3, [r1]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r9
	bne	LBB140_5
LBB140_2:
	cmp	r2, #0
	beq	LBB140_4
	movw	r3, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC140_2+8))
	movt	r3, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC140_2+8))
LPC140_2:
	add	r3, pc, r3
	ldr	r9, [r3, #104]
	ldr	r3, [r2]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r9
	bne	LBB140_6
LBB140_4:
	bl	_p_113_plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm
	pop	{r7, pc}
Ltmp248:
LBB140_5:
	ldr	r0, LCPI140_0
LPC140_0:
	add	r1, pc, r0
	b	LBB140_7
Ltmp249:
LBB140_6:
	ldr	r0, LCPI140_1
LPC140_1:
	add	r1, pc, r0
LBB140_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI140_0:
	.long	Ltmp248-(LPC140_0+8)
LCPI140_1:
	.long	Ltmp249-(LPC140_1+8)
	.end_data_region
Leh_func_end140:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration:
Leh_func_begin141:
	push	{r4, r5, r7, lr}
Ltmp250:
	add	r7, sp, #8
Ltmp251:
Ltmp252:
	mov	r4, r1
	mov	r0, #0
	cmp	r4, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r4]
	ldr	r0, [r4, #16]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	mov	r5, r0
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	lsl	r0, r0, #6
	eor	r5, r0, r5, lsl #7
	ldr	r0, [r4, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	eor	r0, r5, r0
	pop	{r4, r5, r7, pc}
Leh_func_end141:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_object:
Leh_func_begin142:
	push	{r7, lr}
Ltmp253:
	mov	r7, sp
Ltmp254:
Ltmp255:
	cmp	r1, #0
	beq	LBB142_2
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC142_1+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC142_1+8))
	ldr	r3, [r1]
LPC142_1:
	add	r2, pc, r2
	ldr	r2, [r2, #104]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r2
	bne	LBB142_3
LBB142_2:
	bl	_p_117_plt_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_llvm
	pop	{r7, pc}
Ltmp256:
LBB142_3:
	ldr	r0, LCPI142_0
LPC142_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI142_0:
	.long	Ltmp256-(LPC142_0+8)
	.end_data_region
Leh_func_end142:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LineNumber
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LineNumber:
Leh_func_begin143:
	push	{r4, r7, lr}
Ltmp257:
	add	r7, sp, #4
Ltmp258:
Ltmp259:
	mov	r4, r0
	bl	_p_118_plt_System_Xml_Linq_XNodeReader_GetCurrentAttribute_llvm
	cmp	r0, #0
	mov	r1, #0
	ldreq	r0, [r4, #24]
	cmp	r0, #0
	ldrne	r1, [r0]
	ldrne	r1, [r0, #24]
	mov	r0, r1
	pop	{r4, r7, pc}
Leh_func_end143:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LinePosition
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LinePosition:
Leh_func_begin144:
	push	{r4, r7, lr}
Ltmp260:
	add	r7, sp, #4
Ltmp261:
Ltmp262:
	mov	r4, r0
	bl	_p_118_plt_System_Xml_Linq_XNodeReader_GetCurrentAttribute_llvm
	cmp	r0, #0
	mov	r1, #0
	ldreq	r0, [r4, #24]
	cmp	r0, #0
	ldrne	r1, [r0]
	ldrne	r1, [r0, #28]
	mov	r0, r1
	pop	{r4, r7, pc}
Leh_func_end144:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode:
Leh_func_begin145:
	push	{r4, r5, r6, r7, lr}
Ltmp263:
	add	r7, sp, #12
Ltmp264:
Ltmp265:
	mov	r4, r0
	mvn	r0, #0
	mov	r5, r1
	str	r0, [r4, #40]
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC145_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC145_0+8))
LPC145_0:
	add	r0, pc, r0
	ldr	r0, [r0, #424]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	bl	_p_119_plt_System_Xml_NameTable__ctor_llvm
	str	r5, [r4, #24]
	str	r5, [r4, #28]
	str	r6, [r4, #32]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end145:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_AttributeCount
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_AttributeCount:
Leh_func_begin146:
	push	{r4, r5, r7, lr}
Ltmp266:
	add	r7, sp, #8
Ltmp267:
Ltmp268:
	mov	r5, r0
	mov	r4, #0
	ldr	r0, [r5, #36]
	cmp	r0, #1
	ldrbeq	r0, [r5, #45]
	cmpeq	r0, #0
	bne	LBB146_16
	ldr	r0, [r5, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #60]
	blx	r1
	cmp	r0, #1
	beq	LBB146_6
	cmp	r0, #10
	bne	LBB146_11
	ldr	r0, [r5, #24]
	cmp	r0, #0
	beq	LBB146_5
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC146_4+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC146_4+8))
	ldr	r2, [r0]
LPC146_4:
	add	r1, pc, r1
	ldr	r1, [r1, #156]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB146_17
LBB146_5:
	ldr	r1, [r0]
	ldr	r4, [r0, #44]
	ldr	r1, [r0, #48]
	ldr	r0, [r0, #52]
	cmp	r4, #0
	movne	r4, #1
	cmp	r1, #0
	addne	r4, r4, #1
	b	LBB146_15
LBB146_6:
	ldr	r0, [r5, #24]
	cmp	r0, #0
	beq	LBB146_8
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC146_5+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC146_5+8))
	ldr	r2, [r0]
LPC146_5:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB146_18
LBB146_8:
	ldr	r1, [r0]
	ldr	r0, [r0, #52]
	mov	r4, #0
	b	LBB146_10
LBB146_9:
	ldr	r1, [r0]
	ldr	r0, [r0, #40]
	add	r4, r4, #1
LBB146_10:
	cmp	r0, #0
	bne	LBB146_9
	b	LBB146_16
LBB146_11:
	cmp	r0, #9
	bne	LBB146_16
	ldr	r0, [r5, #24]
	cmp	r0, #0
	beq	LBB146_14
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC146_3+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC146_3+8))
	ldr	r2, [r0]
LPC146_3:
	add	r1, pc, r1
	ldr	r1, [r1, #284]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB146_19
LBB146_14:
	ldr	r1, [r0]
	ldr	r0, [r0, #48]
	ldr	r1, [r0]
	ldr	r4, [r0, #16]
	ldr	r1, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r4, #0
	movne	r4, #1
	cmp	r1, #0
	addne	r4, r4, #1
LBB146_15:
	cmp	r0, #0
	addne	r4, r4, #1
	mov	r0, r4
	pop	{r4, r5, r7, pc}
LBB146_16:
	mov	r0, r4
	pop	{r4, r5, r7, pc}
Ltmp269:
LBB146_17:
	ldr	r0, LCPI146_1
LPC146_1:
	add	r1, pc, r0
	b	LBB146_20
Ltmp270:
LBB146_18:
	ldr	r0, LCPI146_0
LPC146_0:
	add	r1, pc, r0
	b	LBB146_20
Ltmp271:
LBB146_19:
	ldr	r0, LCPI146_2
LPC146_2:
	add	r1, pc, r0
LBB146_20:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI146_0:
	.long	Ltmp270-(LPC146_0+8)
LCPI146_1:
	.long	Ltmp269-(LPC146_1+8)
LCPI146_2:
	.long	Ltmp271-(LPC146_2+8)
	.end_data_region
Leh_func_end146:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_BaseURI
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_BaseURI:
Leh_func_begin147:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	cmp	r0, #0
	bxne	lr
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC147_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC147_0+8))
LPC147_0:
	add	r0, pc, r0
	ldr	r0, [r0, #212]
	ldr	r0, [r0]
	bx	lr
Leh_func_end147:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Depth
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Depth:
Leh_func_begin148:
	push	{r4, r5, r7, lr}
Ltmp272:
	add	r7, sp, #8
Ltmp273:
Ltmp274:
	mov	r5, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #220]
	mov	r0, r5
	blx	r1
	tst	r0, #255
	movne	r0, #0
	popne	{r4, r5, r7, pc}
	ldr	r0, [r5, #24]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	cmp	r0, #0
	beq	LBB148_2
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC148_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC148_0+8))
LPC148_0:
	add	r1, pc, r1
	ldr	r2, [r1, #20]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #16]
	mov	r1, #0
	cmp	r3, r2
	moveq	r1, r0
	b	LBB148_3
LBB148_2:
	mov	r1, r0
LBB148_3:
	mov	r0, #0
	cmp	r1, #0
	beq	LBB148_9
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC148_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC148_1+8))
LPC148_1:
	add	r0, pc, r0
	ldr	r2, [r0, #20]
	mov	r0, #0
LBB148_5:
	ldr	r3, [r1]
	add	r0, r0, #1
	ldr	r3, [r1, #8]
	cmp	r3, #0
	bne	LBB148_7
	mov	r1, r3
	b	LBB148_8
LBB148_7:
	ldr	r1, [r3]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r4, [r1, #16]
	mov	r1, #0
	cmp	r4, r2
	moveq	r1, r3
LBB148_8:
	cmp	r1, #0
	bne	LBB148_5
LBB148_9:
	ldr	r1, [r5, #40]
	mov	r2, #1
	eor	r1, r2, r1, lsr #31
	add	r0, r1, r0
	ldrb	r1, [r5, #44]
	cmp	r1, #0
	addne	r0, r0, #1
	pop	{r4, r5, r7, pc}
Leh_func_end148:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_EOF
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_EOF:
Leh_func_begin149:
	mov	r1, r0
	mov	r0, #1
	ldr	r2, [r1, #36]
	cmp	r2, #3
	bxeq	lr
	ldr	r1, [r1, #36]
	mov	r0, #0
	cmp	r1, #2
	moveq	r0, #1
	bx	lr
Leh_func_end149:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_HasAttributes
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_HasAttributes:
Leh_func_begin150:
	push	{r4, r7, lr}
Ltmp275:
	add	r7, sp, #4
Ltmp276:
Ltmp277:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #220]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	bne	LBB150_8
	ldrb	r1, [r4, #45]
	cmp	r1, #0
	bne	LBB150_8
	ldr	r1, [r4, #24]
	cmp	r1, #0
	popeq	{r4, r7, pc}
	ldr	r1, [r4, #24]
	cmp	r1, #0
	beq	LBB150_4
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC150_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC150_1+8))
	ldr	r1, [r1]
LPC150_1:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	beq	LBB150_5
LBB150_4:
	ldr	r0, [r4]
	ldr	r1, [r0, #236]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	movgt	r0, #1
	pop	{r4, r7, pc}
LBB150_5:
	ldr	r1, [r4, #24]
	cmp	r1, #0
	beq	LBB150_7
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	bne	LBB150_9
LBB150_7:
	ldr	r0, [r1]
	ldr	r0, [r1, #52]
	cmp	r0, #0
	movne	r0, #1
LBB150_8:
	pop	{r4, r7, pc}
Ltmp278:
LBB150_9:
	ldr	r0, LCPI150_0
LPC150_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI150_0:
	.long	Ltmp278-(LPC150_0+8)
	.end_data_region
Leh_func_end150:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_IsEmptyElement
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_IsEmptyElement:
Leh_func_begin151:
	push	{r4, r7, lr}
Ltmp279:
	add	r7, sp, #4
Ltmp280:
Ltmp281:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #220]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	bne	LBB151_6
	ldr	r1, [r4, #40]
	cmp	r1, #0
	bge	LBB151_6
	ldr	r2, [r4, #24]
	cmp	r2, #0
	beq	LBB151_6
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC151_1+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC151_1+8))
	ldr	r2, [r2]
LPC151_1:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	popne	{r4, r7, pc}
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB151_5
	ldr	r2, [r0]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB151_7
LBB151_5:
	ldr	r1, [r0]
	bl	_p_120_plt_System_Xml_Linq_XElement_get_IsEmpty_llvm
	uxtb	r0, r0
LBB151_6:
	pop	{r4, r7, pc}
Ltmp282:
LBB151_7:
	ldr	r0, LCPI151_0
LPC151_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI151_0:
	.long	Ltmp282-(LPC151_0+8)
	.end_data_region
Leh_func_end151:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_LocalName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_LocalName:
Leh_func_begin152:
	push	{r7, lr}
Ltmp283:
	mov	r7, sp
Ltmp284:
Ltmp285:
	bl	_p_121_plt_System_Xml_Linq_XNodeReader_GetCurrentName_llvm
	cmp	r0, #0
	beq	LBB152_6
	mov	r1, #0
	cmp	r0, #0
	beq	LBB152_9
	movw	r9, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC152_2+8))
	movt	r9, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC152_2+8))
	ldr	r3, [r0]
LPC152_2:
	add	r9, pc, r9
	ldr	r2, [r9, #108]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #4]
	cmp	r3, r2
	beq	LBB152_7
	cmp	r0, #0
	beq	LBB152_5
	ldr	r2, [r0]
	ldr	r1, [r9, #300]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	bne	LBB152_10
LBB152_5:
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	mov	r0, r1
	pop	{r7, pc}
LBB152_6:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC152_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC152_3+8))
LPC152_3:
	add	r0, pc, r0
	ldr	r0, [r0, #212]
	ldr	r1, [r0]
	b	LBB152_9
LBB152_7:
	cmp	r0, #0
	beq	LBB152_9
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r2
	mov	r1, r0
	bne	LBB152_11
LBB152_9:
	mov	r0, r1
	pop	{r7, pc}
Ltmp286:
LBB152_10:
	ldr	r0, LCPI152_0
LPC152_0:
	add	r1, pc, r0
	b	LBB152_12
Ltmp287:
LBB152_11:
	ldr	r0, LCPI152_1
LPC152_1:
	add	r1, pc, r0
LBB152_12:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI152_0:
	.long	Ltmp286-(LPC152_0+8)
LCPI152_1:
	.long	Ltmp287-(LPC152_1+8)
	.end_data_region
Leh_func_end152:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NamespaceURI
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NamespaceURI:
Leh_func_begin153:
	push	{r4, r5, r7, lr}
Ltmp288:
	add	r7, sp, #8
Ltmp289:
Ltmp290:
	bl	_p_121_plt_System_Xml_Linq_XNodeReader_GetCurrentName_llvm
	cmp	r0, #0
	beq	LBB153_2
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC153_0+8))
	mov	r4, #0
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC153_0+8))
	ldr	r2, [r0]
LPC153_0:
	add	r1, pc, r1
	ldr	r1, [r1, #300]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	moveq	r4, r0
	b	LBB153_3
LBB153_2:
	mov	r4, r0
LBB153_3:
	mov	r0, r4
	mov	r1, #0
	bl	_p_85_plt_System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	beq	LBB153_8
	ldr	r0, [r4]
	ldr	r0, [r4, #8]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC153_2+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC153_2+8))
LPC153_2:
	add	r1, pc, r1
	ldr	r1, [r1, #16]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB153_7
	ldr	r5, [r4, #12]
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	tst	r0, #255
	beq	LBB153_7
	bl	_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm
	ldr	r1, [r0]
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	pop	{r4, r5, r7, pc}
LBB153_7:
	mov	r0, r4
	bl	_p_69_plt_System_Xml_Linq_XName_get_NamespaceName_llvm
	pop	{r4, r5, r7, pc}
LBB153_8:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC153_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC153_1+8))
LPC153_1:
	add	r0, pc, r0
	ldr	r0, [r0, #212]
	ldr	r0, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end153:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NameTable
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NameTable:
Leh_func_begin154:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end154:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NodeType:
Leh_func_begin155:
	push	{r4, r7, lr}
Ltmp291:
	add	r7, sp, #4
Ltmp292:
Ltmp293:
	mov	r4, r0
	mov	r0, #0
	ldr	r1, [r4, #36]
	cmp	r1, #1
	bne	LBB155_5
	ldrb	r1, [r4, #45]
	mov	r0, #15
	cmp	r1, #0
	bne	LBB155_5
	ldrb	r1, [r4, #44]
	mov	r0, #3
	cmp	r1, #0
	bne	LBB155_5
	ldr	r1, [r4, #40]
	mov	r0, #2
	cmp	r1, #0
	bge	LBB155_5
	ldr	r0, [r4, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #60]
	blx	r1
	mov	r1, r0
	mov	r0, #17
	cmp	r1, #9
	popeq	{r4, r7, pc}
	ldr	r0, [r4, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #60]
	blx	r1
LBB155_5:
	pop	{r4, r7, pc}
Leh_func_end155:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Prefix
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Prefix:
Leh_func_begin156:
	push	{r4, r5, r6, r7, lr}
Ltmp294:
	add	r7, sp, #12
Ltmp295:
Ltmp296:
	mov	r5, r0
	bl	_p_121_plt_System_Xml_Linq_XNodeReader_GetCurrentName_llvm
	cmp	r0, #0
	beq	LBB156_2
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC156_0+8))
	mov	r4, #0
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC156_0+8))
	ldr	r2, [r0]
LPC156_0:
	add	r1, pc, r1
	ldr	r1, [r1, #300]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	moveq	r4, r0
	b	LBB156_3
LBB156_2:
	mov	r4, r0
LBB156_3:
	mov	r0, r4
	mov	r1, #0
	bl	_p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm
	tst	r0, #255
	bne	LBB156_13
	ldr	r0, [r4]
	ldr	r6, [r4, #12]
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	mov	r1, r0
	mov	r0, r6
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	tst	r0, #255
	bne	LBB156_13
	ldr	r1, [r5, #24]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB156_7
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC156_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC156_2+8))
	ldr	r2, [r1]
LPC156_2:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB156_7:
	cmp	r0, #0
	bne	LBB156_11
	ldr	r0, [r5, #24]
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	cmp	r1, #0
	beq	LBB156_10
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC156_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC156_3+8))
LPC156_3:
	add	r0, pc, r0
	ldr	r2, [r0, #20]
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r2
	moveq	r0, r1
	b	LBB156_11
LBB156_10:
	mov	r0, r1
LBB156_11:
	cmp	r0, #0
	beq	LBB156_13
	ldr	r1, [r4, #12]
	ldr	r2, [r0]
	bl	_p_66_plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_llvm
	cmp	r0, #0
	popne	{r4, r5, r6, r7, pc}
LBB156_13:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC156_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC156_1+8))
LPC156_1:
	add	r0, pc, r0
	ldr	r0, [r0, #212]
	ldr	r0, [r0]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end156:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_ReadState
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_ReadState:
Leh_func_begin157:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end157:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Value
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Value:
Leh_func_begin158:
	push	{r4, r5, r6, r7, lr}
Ltmp297:
	add	r7, sp, #12
Ltmp298:
Ltmp299:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #172]
	mov	r0, r4
	blx	r1
	cmp	r0, #1
	bne	LBB158_9
	mov	r0, r4
	bl	_p_118_plt_System_Xml_Linq_XNodeReader_GetCurrentAttribute_llvm
	cmp	r0, #0
	beq	LBB158_3
	ldr	r1, [r0]
	bl	_p_64_plt_System_Xml_Linq_XAttribute_get_Value_llvm
	pop	{r4, r5, r6, r7, pc}
LBB158_3:
	ldr	r0, [r4, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #60]
	blx	r1
	sub	r1, r0, #3
	cmp	r1, #7
	bhi	LBB158_9
	sub	r0, r0, #5
	cmp	r0, #5
	bls	LBB158_8
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB158_7
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_6+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_6+8))
	ldr	r2, [r0]
LPC158_6:
	add	r1, pc, r1
	ldr	r1, [r1, #192]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB158_28
LBB158_7:
	ldr	r1, [r0]
	ldr	r0, [r0, #40]
	pop	{r4, r5, r6, r7, pc}
LBB158_8:
	lsl	r0, r0, #2
	adr	r1, LJTI158_0_0
	ldr	r0, [r0, r1]
	add	pc, r0, r1
LJTI158_0_0:
	.data_region jt32
	.long	LBB158_9-LJTI158_0_0
	.long	LBB158_9-LJTI158_0_0
	.long	LBB158_10-LJTI158_0_0
	.long	LBB158_13-LJTI158_0_0
	.long	LBB158_16-LJTI158_0_0
	.long	LBB158_22-LJTI158_0_0
	.end_data_region
LBB158_9:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_5+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_5+8))
LPC158_5:
	add	r0, pc, r0
	ldr	r0, [r0, #212]
	ldr	r0, [r0]
	pop	{r4, r5, r6, r7, pc}
LBB158_10:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB158_12
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_7+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_7+8))
	ldr	r2, [r0]
LPC158_7:
	add	r1, pc, r1
	ldr	r1, [r1, #416]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB158_29
LBB158_12:
	ldr	r1, [r0]
	ldr	r0, [r0, #44]
	pop	{r4, r5, r6, r7, pc}
LBB158_13:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB158_7
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_8+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_8+8))
	ldr	r2, [r0]
LPC158_8:
	add	r1, pc, r1
	ldr	r1, [r1, #420]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	beq	LBB158_7
Ltmp300:
	ldr	r0, LCPI158_0
LPC158_0:
	add	r1, pc, r0
	b	LBB158_32
LBB158_16:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB158_18
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_10+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_10+8))
	ldr	r2, [r0]
LPC158_10:
	add	r1, pc, r1
	ldr	r1, [r1, #284]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB158_30
LBB158_18:
	ldr	r1, [r0]
	ldr	r5, [r0, #48]
	ldr	r1, [r4, #40]
	cmp	r1, #0
	blt	LBB158_27
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r4, r0
	cmp	r4, #0
	beq	LBB158_21
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_9+8))
	mov	r0, r4
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_9+8))
LPC158_9:
	add	r6, pc, r6
	ldr	r1, [r6, #172]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	ldrne	r0, [r5]
	ldrne	r0, [r5, #16]
	popne	{r4, r5, r6, r7, pc}
	ldr	r1, [r6, #176]
	mov	r0, r4
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	ldrne	r0, [r5]
	ldrne	r0, [r5, #8]
	popne	{r4, r5, r6, r7, pc}
LBB158_21:
	ldr	r0, [r5]
	ldr	r0, [r5, #12]
	pop	{r4, r5, r6, r7, pc}
LBB158_22:
	ldr	r5, [r4, #24]
	cmp	r5, #0
	beq	LBB158_24
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_12+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_12+8))
	ldr	r1, [r5]
LPC158_12:
	add	r0, pc, r0
	ldr	r0, [r0, #156]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB158_31
LBB158_24:
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r4, r0
	cmp	r4, #0
	beq	LBB158_26
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC158_11+8))
	mov	r0, r4
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC158_11+8))
LPC158_11:
	add	r6, pc, r6
	ldr	r1, [r6, #384]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	ldrne	r0, [r5]
	ldrne	r0, [r5, #44]
	popne	{r4, r5, r6, r7, pc}
	ldr	r1, [r6, #388]
	mov	r0, r4
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	ldrne	r0, [r5]
	ldrne	r0, [r5, #48]
	popne	{r4, r5, r6, r7, pc}
LBB158_26:
	ldr	r0, [r5]
	ldr	r0, [r5, #52]
	pop	{r4, r5, r6, r7, pc}
LBB158_27:
	ldr	r0, [r5]
	ldr	r1, [r0, #32]
	mov	r0, r5
	blx	r1
	ldr	r1, [r0, #8]
	ldr	r2, [r0]
	sub	r2, r1, #8
	mov	r1, #6
	bl	_p_122_plt_string_Substring_int_int_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp301:
LBB158_28:
	ldr	r0, LCPI158_4
LPC158_4:
	add	r1, pc, r0
	b	LBB158_32
Ltmp302:
LBB158_29:
	ldr	r0, LCPI158_1
LPC158_1:
	add	r1, pc, r0
	b	LBB158_32
Ltmp303:
LBB158_30:
	ldr	r0, LCPI158_2
LPC158_2:
	add	r1, pc, r0
	b	LBB158_32
Ltmp304:
LBB158_31:
	ldr	r0, LCPI158_3
LPC158_3:
	add	r1, pc, r0
LBB158_32:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI158_0:
	.long	Ltmp300-(LPC158_0+8)
LCPI158_1:
	.long	Ltmp302-(LPC158_1+8)
LCPI158_2:
	.long	Ltmp303-(LPC158_2+8)
LCPI158_3:
	.long	Ltmp304-(LPC158_3+8)
LCPI158_4:
	.long	Ltmp301-(LPC158_4+8)
	.end_data_region
Leh_func_end158:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_HasLineInfo
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_HasLineInfo:
Leh_func_begin159:
	push	{r4, r7, lr}
Ltmp305:
	add	r7, sp, #4
Ltmp306:
	push	{r8}
Ltmp307:
	mov	r4, r0
	bl	_p_118_plt_System_Xml_Linq_XNodeReader_GetCurrentAttribute_llvm
	cmp	r0, #0
	mov	r1, #0
	ldreq	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB159_2
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC159_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC159_0+8))
LPC159_0:
	add	r1, pc, r1
	ldr	r1, [r1, #428]
	sub	r2, r2, #64
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	uxtb	r1, r0
LBB159_2:
	mov	r0, r1
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end159:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute:
Leh_func_begin160:
	push	{r7, lr}
Ltmp308:
	mov	r7, sp
Ltmp309:
Ltmp310:
	ldr	r1, [r0, #40]
	bl	_p_123_plt_System_Xml_Linq_XNodeReader_GetXAttribute_int_llvm
	pop	{r7, pc}
Leh_func_end160:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName:
Leh_func_begin161:
	push	{r4, r7, lr}
Ltmp311:
	add	r7, sp, #4
Ltmp312:
Ltmp313:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #220]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	bne	LBB161_2
	ldrb	r1, [r4, #44]
	cmp	r1, #0
	popne	{r4, r7, pc}
	ldr	r1, [r4, #40]
	mov	r0, r4
	bl	_p_124_plt_System_Xml_Linq_XNodeReader_GetName_int_llvm
LBB161_2:
	pop	{r4, r7, pc}
Leh_func_end161:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetName_int
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetName_int:
Leh_func_begin162:
	push	{r4, r5, r7, lr}
Ltmp314:
	add	r7, sp, #8
Ltmp315:
Ltmp316:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #60]
	blx	r1
	mov	r1, r0
	cmp	r5, #0
	blt	LBB162_7
	cmp	r1, #1
	beq	LBB162_12
	cmp	r1, #9
	bne	LBB162_13
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB162_5
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_15+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_15+8))
	ldr	r2, [r0]
LPC162_15:
	add	r1, pc, r1
	ldr	r1, [r1, #284]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB162_33
LBB162_5:
	ldr	r1, [r0]
	ldr	r0, [r0, #48]
	cmp	r5, #0
	bne	LBB162_19
	ldr	r1, [r0]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_13+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_13+8))
	ldr	r2, [r0, #16]
LPC162_13:
	add	r1, pc, r1
	cmp	r2, #0
	addne	r0, r1, #172
	ldrne	r0, [r0]
	popne	{r4, r5, r7, pc}
	b	LBB162_21
LBB162_7:
	sub	r0, r1, #7
	cmp	r0, #3
	bls	LBB162_17
	mov	r0, #0
	cmp	r1, #1
	bne	LBB162_16
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB162_11
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_5+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_5+8))
	ldr	r2, [r0]
LPC162_5:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	bne	LBB162_34
LBB162_11:
	ldr	r1, [r0]
	ldr	r0, [r0, #48]
	pop	{r4, r5, r7, pc}
LBB162_12:
	mov	r0, r4
	mov	r1, r5
	bl	_p_123_plt_System_Xml_Linq_XNodeReader_GetXAttribute_int_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #32]
	pop	{r4, r5, r7, pc}
LBB162_13:
	mov	r0, #0
	cmp	r1, #10
	bne	LBB162_16
	cmp	r5, #0
	beq	LBB162_30
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_9+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_9+8))
LPC162_9:
	add	r0, pc, r0
	ldr	r0, [r0, #388]
	pop	{r4, r5, r7, pc}
LBB162_16:
	pop	{r4, r5, r7, pc}
LBB162_17:
	mov	r0, #0
	cmp	r1, #8
	popeq	{r4, r5, r7, pc}
	cmp	r1, #9
	bne	LBB162_22
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_6+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_6+8))
LPC162_6:
	add	r0, pc, r0
	ldr	r0, [r0, #432]
	pop	{r4, r5, r7, pc}
LBB162_19:
	cmp	r5, #1
	bne	LBB162_26
	ldr	r1, [r0]
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_12+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_12+8))
	ldr	r2, [r0, #16]
LPC162_12:
	add	r1, pc, r1
	cmp	r2, #0
	addeq	r0, r1, #180
	ldreq	r0, [r0]
	popeq	{r4, r5, r7, pc}
LBB162_21:
	ldr	r2, [r0, #8]
	add	r0, r1, #176
	cmp	r2, #0
	addeq	r0, r1, #180
	ldr	r0, [r0]
	pop	{r4, r5, r7, pc}
LBB162_22:
	cmp	r1, #10
	bne	LBB162_27
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB162_29
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_8+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_8+8))
	ldr	r2, [r0]
LPC162_8:
	add	r1, pc, r1
	ldr	r1, [r1, #156]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	beq	LBB162_29
Ltmp317:
	ldr	r0, LCPI162_2
LPC162_2:
	add	r1, pc, r0
	b	LBB162_37
LBB162_26:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_14+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_14+8))
LPC162_14:
	add	r0, pc, r0
	ldr	r0, [r0, #180]
	pop	{r4, r5, r7, pc}
LBB162_27:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB162_29
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_7+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_7+8))
	ldr	r2, [r0]
LPC162_7:
	add	r1, pc, r1
	ldr	r1, [r1, #416]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB162_35
LBB162_29:
	ldr	r1, [r0]
	ldr	r0, [r0, #40]
	pop	{r4, r5, r7, pc}
LBB162_30:
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB162_32
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_11+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_11+8))
	ldr	r2, [r0]
LPC162_11:
	add	r1, pc, r1
	ldr	r1, [r1, #156]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB162_36
LBB162_32:
	ldr	r1, [r0]
	ldr	r2, [r0, #44]
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC162_10+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC162_10+8))
LPC162_10:
	add	r0, pc, r0
	add	r1, r0, #384
	cmp	r2, #0
	addeq	r1, r0, #388
	ldr	r0, [r1]
	pop	{r4, r5, r7, pc}
Ltmp318:
LBB162_33:
	ldr	r0, LCPI162_0
LPC162_0:
	add	r1, pc, r0
	b	LBB162_37
Ltmp319:
LBB162_34:
	ldr	r0, LCPI162_4
LPC162_4:
	add	r1, pc, r0
	b	LBB162_37
Ltmp320:
LBB162_35:
	ldr	r0, LCPI162_3
LPC162_3:
	add	r1, pc, r0
	b	LBB162_37
Ltmp321:
LBB162_36:
	ldr	r0, LCPI162_1
LPC162_1:
	add	r1, pc, r0
LBB162_37:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI162_0:
	.long	Ltmp318-(LPC162_0+8)
LCPI162_1:
	.long	Ltmp321-(LPC162_1+8)
LCPI162_2:
	.long	Ltmp317-(LPC162_2+8)
LCPI162_3:
	.long	Ltmp320-(LPC162_3+8)
LCPI162_4:
	.long	Ltmp319-(LPC162_4+8)
	.end_data_region
Leh_func_end162:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_Close
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_Close:
Leh_func_begin163:
	mov	r1, #4
	str	r1, [r0, #36]
	bx	lr
Leh_func_end163:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_LookupNamespace_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_LookupNamespace_string:
Leh_func_begin164:
	push	{r4, r5, r6, r7, lr}
Ltmp322:
	add	r7, sp, #12
Ltmp323:
Ltmp324:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5]
	ldr	r1, [r0, #220]
	mov	r0, r5
	blx	r1
	mov	r6, #0
	tst	r0, #255
	bne	LBB164_10
	ldr	r1, [r5, #24]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB164_3
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC164_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC164_0+8))
	ldr	r2, [r1]
LPC164_0:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB164_3:
	cmp	r0, #0
	bne	LBB164_7
	ldr	r0, [r5, #24]
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	cmp	r1, #0
	beq	LBB164_6
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC164_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC164_1+8))
LPC164_1:
	add	r0, pc, r0
	ldr	r2, [r0, #20]
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r2
	moveq	r0, r1
	b	LBB164_7
LBB164_6:
	mov	r0, r1
LBB164_7:
	mov	r6, #0
	cmp	r0, #0
	beq	LBB164_10
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_76_plt_System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_llvm
	mov	r4, r0
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_97_plt_System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	tst	r0, #255
	beq	LBB164_10
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm
	mov	r6, r0
LBB164_10:
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end164:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToElement
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToElement:
Leh_func_begin165:
	ldr	r2, [r0, #40]
	mov	r1, #0
	cmp	r2, #0
	movge	r1, #0
	strbge	r1, [r0, #44]
	mvnge	r1, #0
	strge	r1, [r0, #40]
	movge	r1, #1
	mov	r0, r1
	bx	lr
Leh_func_end165:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToFirstAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToFirstAttribute:
Leh_func_begin166:
	push	{r4, r7, lr}
Ltmp325:
	add	r7, sp, #4
Ltmp326:
Ltmp327:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #236]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #1
	movge	r0, #0
	strge	r0, [r4, #40]
	strbge	r0, [r4, #44]
	movge	r0, #1
	pop	{r4, r7, pc}
Leh_func_end166:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToNextAttribute
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToNextAttribute:
Leh_func_begin167:
	push	{r4, r7, lr}
Ltmp328:
	add	r7, sp, #4
Ltmp329:
Ltmp330:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #236]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldr	r0, [r4, #40]
	add	r2, r0, #1
	mov	r0, #0
	cmp	r2, r1
	popge	{r4, r7, pc}
	ldr	r0, [r4, #40]
	add	r0, r0, #1
	str	r0, [r4, #40]
	mov	r0, #0
	strb	r0, [r4, #44]
	mov	r0, #1
	pop	{r4, r7, pc}
Leh_func_end167:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string:
Leh_func_begin168:
	push	{r4, r5, r6, r7, lr}
Ltmp331:
	add	r7, sp, #12
Ltmp332:
	push	{r10, r11}
Ltmp333:
	sub	sp, sp, #12
	mov	r5, r1
	mov	r11, r0
	cmp	r5, #0
	beq	LBB168_17
	ldr	r0, [r11]
	ldr	r1, [r0, #236]
	mov	r0, r11
	blx	r1
	mov	r1, r0
	mov	r0, #0
	str	r1, [sp, #4]
	cmp	r1, #1
	blt	LBB168_16
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC168_2+8))
	mov	r6, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC168_2+8))
LPC168_2:
	add	r0, pc, r0
	ldr	r1, [r0, #108]
	ldr	r0, [r0, #300]
	str	r1, [sp]
	str	r0, [sp, #8]
LBB168_3:
	mov	r0, r11
	mov	r1, r6
	bl	_p_124_plt_System_Xml_Linq_XNodeReader_GetName_int_llvm
	mov	r4, r0
	cmp	r4, #0
	beq	LBB168_14
	cmp	r4, #0
	bne	LBB168_6
	mov	r0, r4
	b	LBB168_7
LBB168_6:
	ldr	r0, [r4]
	ldr	r2, [sp]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0, #4]
	mov	r0, #0
	cmp	r1, r2
	moveq	r0, r4
LBB168_7:
	mov	r1, r5
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	uxtb	r10, r0
	cmp	r10, #0
	movne	r10, #1
	cmp	r4, #0
	beq	LBB168_9
	ldr	r0, [r4]
	ldr	r1, [sp, #8]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #4]
	cmp	r0, r1
	bne	LBB168_18
LBB168_9:
	ldr	r0, [r4]
	ldr	r1, [r4, #8]
	ldr	r0, [r5]
	mov	r2, #4
	mov	r0, r5
	bl	_p_125_plt_string_EndsWith_string_System_StringComparison_llvm
	tst	r0, #255
	beq	LBB168_13
	cmp	r4, #0
	beq	LBB168_12
	ldr	r0, [r4]
	ldr	r1, [sp, #8]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #4]
	cmp	r0, r1
	bne	LBB168_19
LBB168_12:
	mov	r0, r11
	mov	r1, r4
	bl	_p_126_plt_System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	movne	r10, #1
LBB168_13:
	cmp	r10, #0
	bne	LBB168_15
LBB168_14:
	ldr	r1, [sp, #4]
	add	r6, r6, #1
	mov	r0, #0
	cmp	r6, r1
	blt	LBB168_3
	b	LBB168_16
LBB168_15:
	mov	r0, #0
	str	r6, [r11, #40]
	strb	r0, [r11, #44]
	mov	r0, #1
LBB168_16:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB168_17:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC168_3+8))
	mov	r1, #13
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC168_3+8))
LPC168_3:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp334:
LBB168_18:
	ldr	r0, LCPI168_0
LPC168_0:
	add	r1, pc, r0
	b	LBB168_20
Ltmp335:
LBB168_19:
	ldr	r0, LCPI168_1
LPC168_1:
	add	r1, pc, r0
LBB168_20:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI168_0:
	.long	Ltmp334-(LPC168_0+8)
LCPI168_1:
	.long	Ltmp335-(LPC168_1+8)
	.end_data_region
Leh_func_end168:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName:
Leh_func_begin169:
	push	{r4, r5, r6, r7, lr}
Ltmp336:
	add	r7, sp, #12
Ltmp337:
Ltmp338:
	mov	r4, r1
	ldr	r1, [r0, #24]
	mov	r5, #0
	cmp	r1, #0
	beq	LBB169_2
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC169_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC169_0+8))
	ldr	r3, [r1]
LPC169_0:
	add	r2, pc, r2
	ldr	r2, [r2, #20]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #16]
	cmp	r3, r2
	movne	r1, #0
	mov	r5, r1
LBB169_2:
	cmp	r5, #0
	bne	LBB169_6
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	cmp	r0, #0
	beq	LBB169_5
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC169_2+8))
	mov	r5, #0
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC169_2+8))
	ldr	r2, [r0]
LPC169_2:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	moveq	r5, r0
	b	LBB169_6
LBB169_5:
	mov	r5, r0
LBB169_6:
	cmp	r5, #0
	beq	LBB169_9
	ldr	r0, [r4]
	ldr	r6, [r4, #12]
	bl	_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm
	mov	r1, r0
	mov	r0, r6
	bl	_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm
	tst	r0, #255
	bne	LBB169_9
	ldr	r1, [r4, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_66_plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_llvm
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC169_1+8))
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC169_1+8))
LPC169_1:
	add	r6, pc, r6
	ldr	r1, [r6, #212]
	ldr	r1, [r1]
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB169_10
LBB169_9:
	ldr	r0, [r4]
	ldr	r0, [r4, #8]
	pop	{r4, r5, r6, r7, pc}
LBB169_10:
	ldr	r1, [r4, #12]
	mov	r0, r5
	bl	_p_66_plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_llvm
	ldr	r1, [r6, #436]
	ldr	r2, [r4, #8]
	bl	_p_127_plt_string_Concat_string_string_string_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end169:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string_string:
Leh_func_begin170:
	push	{r4, r5, r6, r7, lr}
Ltmp339:
	add	r7, sp, #12
Ltmp340:
	push	{r10, r11}
Ltmp341:
	sub	sp, sp, #16
	mov	r6, r1
	mov	r5, r2
	cmp	r6, #0
	beq	LBB170_18
	cmp	r5, #0
	beq	LBB170_19
	str	r0, [sp, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #236]
	blx	r1
	mov	r1, r0
	mov	r0, #0
	str	r1, [sp, #8]
	cmp	r1, #1
	blt	LBB170_17
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC170_1+8))
	mov	r4, #0
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC170_1+8))
LPC170_1:
	add	r0, pc, r0
	ldr	r1, [r0, #108]
	ldr	r0, [r0, #300]
	str	r1, [sp, #4]
	str	r0, [sp]
LBB170_4:
	ldr	r0, [sp, #12]
	mov	r1, r4
	bl	_p_124_plt_System_Xml_Linq_XNodeReader_GetName_int_llvm
	mov	r10, r0
	cmp	r10, #0
	beq	LBB170_15
	cmp	r10, #0
	bne	LBB170_7
	mov	r0, r10
	b	LBB170_8
LBB170_7:
	ldr	r0, [r10]
	ldr	r2, [sp, #4]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0, #4]
	mov	r0, #0
	cmp	r1, r2
	moveq	r0, r10
LBB170_8:
	mov	r1, r6
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	mov	r11, #0
	tst	r0, #255
	beq	LBB170_10
	ldr	r0, [r5, #8]
	mov	r11, #0
	cmp	r0, #0
	moveq	r11, #1
LBB170_10:
	cmp	r10, #0
	beq	LBB170_12
	ldr	r0, [r10]
	ldr	r1, [sp]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #4]
	cmp	r0, r1
	bne	LBB170_21
LBB170_12:
	ldr	r0, [r10]
	ldr	r1, [r10, #8]
	mov	r0, r6
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB170_14
	mov	r0, r10
	bl	_p_69_plt_System_Xml_Linq_XName_get_NamespaceName_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_8_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	movne	r11, #1
LBB170_14:
	cmp	r11, #0
	bne	LBB170_16
LBB170_15:
	ldr	r1, [sp, #8]
	add	r4, r4, #1
	mov	r0, #0
	cmp	r4, r1
	blt	LBB170_4
	b	LBB170_17
LBB170_16:
	ldr	r1, [sp, #12]
	mov	r0, #0
	str	r4, [r1, #40]
	strb	r0, [r1, #44]
	mov	r0, #1
LBB170_17:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB170_18:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC170_2+8))
	movw	r1, #1597
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC170_2+8))
LPC170_2:
	ldr	r0, [pc, r0]
	b	LBB170_20
LBB170_19:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC170_3+8))
	movw	r1, #1609
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC170_3+8))
LPC170_3:
	ldr	r0, [pc, r0]
LBB170_20:
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp342:
LBB170_21:
	ldr	r0, LCPI170_0
LPC170_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI170_0:
	.long	Ltmp342-(LPC170_0+8)
	.end_data_region
Leh_func_end170:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_Read
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_Read:
Leh_func_begin171:
	push	{r4, r5, r7, lr}
Ltmp343:
	add	r7, sp, #8
Ltmp344:
Ltmp345:
	mov	r4, r0
	mvn	r0, #0
	str	r0, [r4, #40]
	mov	r0, #0
	strb	r0, [r4, #44]
	ldr	r1, [r4, #36]
	cmp	r1, #0
	bne	LBB171_5
	mov	r0, #1
	mov	r5, r4
	mov	r2, #0
	str	r0, [r4, #36]
	ldr	r1, [r5, #24]!
	cmp	r1, #0
	beq	LBB171_3
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC171_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC171_0+8))
	ldr	r3, [r1]
LPC171_0:
	add	r2, pc, r2
	ldr	r2, [r2, #284]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #16]
	cmp	r3, r2
	movne	r1, #0
	mov	r2, r1
LBB171_3:
	cmp	r2, #0
	beq	LBB171_26
	ldr	r1, [r2]
	ldr	r1, [r2, #48]
	cmp	r1, #0
	popne	{r4, r5, r7, pc}
	b	LBB171_7
LBB171_5:
	cmp	r1, #1
	bne	LBB171_26
	add	r5, r4, #24
LBB171_7:
	ldr	r1, [r5]
	mov	r0, #0
	cmp	r1, #0
	moveq	r0, #1
	beq	LBB171_9
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC171_1+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC171_1+8))
	ldr	r1, [r1]
LPC171_1:
	add	r2, pc, r2
	ldr	r3, [r2, #284]
	mov	r2, #1
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r3
	moveq	r2, r0
	b	LBB171_10
LBB171_9:
	mov	r2, r0
LBB171_10:
	ldr	r1, [r5]
	cmp	r2, #1
	bne	LBB171_27
	mov	r0, #0
	cmp	r1, #0
	beq	LBB171_13
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC171_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC171_2+8))
	ldr	r2, [r1]
LPC171_2:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB171_13:
	cmp	r0, #0
	beq	LBB171_17
	ldrb	r1, [r4, #45]
	cmp	r1, #0
	bne	LBB171_17
	ldr	r1, [r0]
	ldr	r1, [r0, #40]
	cmp	r1, #0
	bne	LBB171_31
	bl	_p_120_plt_System_Xml_Linq_XElement_get_IsEmpty_llvm
	tst	r0, #255
	beq	LBB171_25
LBB171_17:
	mov	r0, #0
	strb	r0, [r4, #45]
	ldr	r0, [r4, #24]
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	cmp	r0, #0
	beq	LBB171_19
	ldr	r0, [r4, #24]
	ldr	r1, [r4, #28]
	cmp	r0, r1
	bne	LBB171_33
LBB171_19:
	ldr	r0, [r5]
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	cmp	r1, #0
	beq	LBB171_32
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC171_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC171_3+8))
	ldr	r1, [r1]
LPC171_3:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB171_32
	ldr	r1, [r4, #24]
	ldr	r2, [r4, #28]
	cmp	r1, r2
	beq	LBB171_32
	ldr	r1, [r5]
	ldr	r2, [r1]
	ldr	r1, [r1, #8]
	mov	r2, #0
	cmp	r1, #0
	beq	LBB171_24
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	movne	r1, #0
	mov	r2, r1
LBB171_24:
	str	r2, [r4, #24]
LBB171_25:
	mov	r0, #1
	strb	r0, [r4, #45]
LBB171_26:
	pop	{r4, r5, r7, pc}
LBB171_27:
	cmp	r1, #0
	beq	LBB171_29
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC171_4+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC171_4+8))
LPC171_4:
	add	r0, pc, r0
	ldr	r2, [r0, #284]
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r2
	moveq	r0, r1
	b	LBB171_30
LBB171_29:
	mov	r0, r1
LBB171_30:
	ldr	r1, [r0]
	ldr	r1, [r0, #40]
	str	r1, [r5]
	ldr	r1, [r5]
	cmp	r1, #0
	beq	LBB171_32
LBB171_31:
	ldr	r0, [r0, #40]
	b	LBB171_34
LBB171_32:
	mov	r0, #3
	str	r0, [r4, #36]
	mov	r0, #0
	pop	{r4, r5, r7, pc}
LBB171_33:
	ldr	r0, [r5]
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
LBB171_34:
	str	r0, [r5]
	mov	r0, #1
	pop	{r4, r5, r7, pc}
Leh_func_end171:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_ReadAttributeValue
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_ReadAttributeValue:
Leh_func_begin172:
	mov	r1, r0
	mov	r0, #0
	ldr	r2, [r1, #40]
	cmp	r2, #0
	bxlt	lr
	ldrb	r2, [r1, #44]
	cmp	r2, #0
	moveq	r0, #1
	strbeq	r0, [r1, #44]
	bx	lr
Leh_func_end172:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XNodeReader_ResolveEntity
	.align	2
_System_Xml_Linq__System_Xml_Linq_XNodeReader_ResolveEntity:
Leh_func_begin173:
	push	{r7, lr}
Ltmp346:
	mov	r7, sp
Ltmp347:
Ltmp348:
	movw	r0, #629
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end173:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LineNumber
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LineNumber:
Leh_func_begin174:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end174:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LinePosition
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LinePosition:
Leh_func_begin175:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end175:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject__ctor:
Leh_func_begin176:
	bx	lr
Leh_func_end176:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_BaseUri
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_BaseUri:
Leh_func_begin177:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end177:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_set_BaseUri_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_set_BaseUri_string:
Leh_func_begin178:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end178:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_Document
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_Document:
Leh_func_begin179:
	push	{r7, lr}
Ltmp349:
	mov	r7, sp
Ltmp350:
Ltmp351:
	mov	r1, r0
	cmp	r1, #0
	beq	LBB179_4
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC179_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC179_3+8))
	ldr	r2, [r1]
LPC179_3:
	add	r0, pc, r0
	ldr	r0, [r0, #284]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	bne	LBB179_3
	cmp	r1, #0
	bne	LBB179_5
LBB179_3:
	mov	r0, r1
	b	LBB179_8
LBB179_4:
	mov	r0, #0
	beq	LBB179_8
LBB179_5:
	mov	r0, #0
	cmp	r1, #0
	beq	LBB179_15
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC179_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC179_2+8))
	ldr	r2, [r1]
LPC179_2:
	add	r0, pc, r0
	ldr	r0, [r0, #284]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r0
	mov	r0, r1
	beq	LBB179_15
Ltmp352:
	ldr	r0, LCPI179_0
LPC179_0:
	add	r1, pc, r0
	b	LBB179_18
LBB179_8:
	ldr	r1, [r0, #8]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB179_15
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC179_4+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC179_4+8))
LPC179_4:
	add	r0, pc, r0
	ldr	r9, [r0, #284]
LBB179_10:
	mov	r0, #0
	mov	r3, #1
	cmp	r1, #0
	beq	LBB179_13
	ldr	r2, [r1]
	mov	r3, #0
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r9
	beq	LBB179_14
	ldr	r1, [r1, #8]
	mov	r0, #0
	cmp	r1, #0
	bne	LBB179_10
	b	LBB179_15
LBB179_13:
	mov	r1, #0
LBB179_14:
	cmp	r3, #0
	beq	LBB179_16
LBB179_15:
	pop	{r7, pc}
LBB179_16:
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #16]
	cmp	r0, r9
	mov	r0, r1
	popeq	{r7, pc}
Ltmp353:
LBB179_17:
	ldr	r0, LCPI179_1
LPC179_1:
	add	r1, pc, r0
LBB179_18:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI179_0:
	.long	Ltmp352-(LPC179_0+8)
LCPI179_1:
	.long	Ltmp353-(LPC179_1+8)
	.end_data_region
Leh_func_end179:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_Parent
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_Parent:
Leh_func_begin180:
	ldr	r1, [r0, #8]
	cmp	r1, #0
	beq	LBB180_2
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC180_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC180_0+8))
LPC180_0:
	add	r0, pc, r0
	ldr	r2, [r0, #20]
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r2
	bxne	lr
LBB180_2:
	mov	r0, r1
	bx	lr
Leh_func_end180:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_Owner
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_Owner:
Leh_func_begin181:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end181:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_LineNumber
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_LineNumber:
Leh_func_begin182:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end182:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_set_LineNumber_int
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_set_LineNumber_int:
Leh_func_begin183:
	str	r1, [r0, #24]
	bx	lr
Leh_func_end183:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_get_LinePosition
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_get_LinePosition:
Leh_func_begin184:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end184:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_set_LinePosition_int
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_set_LinePosition_int:
Leh_func_begin185:
	str	r1, [r0, #28]
	bx	lr
Leh_func_end185:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_SetOwner_System_Xml_Linq_XContainer
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_SetOwner_System_Xml_Linq_XContainer:
Leh_func_begin186:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end186:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_HasLineInfo
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_HasLineInfo:
Leh_func_begin187:
	ldr	r1, [r0, #24]
	mov	r0, #0
	cmp	r1, #0
	movgt	r0, #1
	bx	lr
Leh_func_end187:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
Leh_func_begin188:
	push	{r4, r5, r6, r7, lr}
Ltmp354:
	add	r7, sp, #12
Ltmp355:
	push	{r8, r10, r11}
Ltmp356:
	mov	r10, r2
	mov	r5, r1
	mov	r11, r0
	tst	r10, #4
	beq	LBB188_7
	mov	r6, #0
	cmp	r5, #0
	beq	LBB188_4
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC188_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC188_0+8))
	ldr	r1, [r5]
LPC188_0:
	add	r0, pc, r0
	ldr	r0, [r0, #448]
	ldrh	r2, [r1, #20]
	cmp	r2, r0
	blo	LBB188_4
	ldr	r1, [r1, #16]
	mov	r2, #1
	ldrb	r1, [r1, r0, asr #3]
	and	r0, r0, #7
	and	r6, r1, r2, lsl r0
	cmp	r6, #0
	movne	r6, r5
LBB188_4:
	cmp	r6, #0
	beq	LBB188_7
	ldr	r1, [r6]
	movw	r4, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC188_1+8))
	movt	r4, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC188_1+8))
LPC188_1:
	add	r4, pc, r4
	ldr	r0, [r4, #428]
	sub	r1, r1, #64
	ldr	r1, [r1]
	mov	r8, r0
	mov	r0, r6
	blx	r1
	tst	r0, #255
	beq	LBB188_7
	ldr	r1, [r6]
	ldr	r0, [r4, #440]
	sub	r1, r1, #36
	mov	r8, r0
	mov	r0, r6
	ldr	r1, [r1]
	blx	r1
	str	r0, [r11, #24]
	ldr	r0, [r4, #444]
	ldr	r1, [r6]
	mov	r8, r0
	mov	r0, r6
	sub	r1, r1, #24
	ldr	r1, [r1]
	blx	r1
	str	r0, [r11, #28]
LBB188_7:
	tst	r10, #2
	beq	LBB188_9
	ldr	r0, [r5]
	ldr	r1, [r0, #232]
	mov	r0, r5
	blx	r1
	str	r0, [r11, #12]
LBB188_9:
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end188:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnAddingObject_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnAddingObject_object:
Leh_func_begin189:
	push	{r7, lr}
Ltmp357:
	mov	r7, sp
Ltmp358:
Ltmp359:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC189_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC189_0+8))
LPC189_0:
	add	r2, pc, r2
	ldr	r2, [r2, #452]
	ldr	r2, [r2]
	bl	_p_128_plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end189:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnAddedObject_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnAddedObject_object:
Leh_func_begin190:
	push	{r7, lr}
Ltmp360:
	mov	r7, sp
Ltmp361:
Ltmp362:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC190_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC190_0+8))
LPC190_0:
	add	r2, pc, r2
	ldr	r2, [r2, #452]
	ldr	r2, [r2]
	bl	_p_129_plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end190:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovingObject_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovingObject_object:
Leh_func_begin191:
	push	{r7, lr}
Ltmp363:
	mov	r7, sp
Ltmp364:
Ltmp365:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC191_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC191_0+8))
LPC191_0:
	add	r2, pc, r2
	ldr	r2, [r2, #456]
	ldr	r2, [r2]
	bl	_p_128_plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end191:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovedObject_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovedObject_object:
Leh_func_begin192:
	push	{r7, lr}
Ltmp366:
	mov	r7, sp
Ltmp367:
Ltmp368:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC192_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC192_0+8))
LPC192_0:
	add	r2, pc, r2
	ldr	r2, [r2, #456]
	ldr	r2, [r2]
	bl	_p_129_plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end192:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanging_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanging_object:
Leh_func_begin193:
	push	{r7, lr}
Ltmp369:
	mov	r7, sp
Ltmp370:
Ltmp371:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC193_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC193_0+8))
LPC193_0:
	add	r2, pc, r2
	ldr	r2, [r2, #460]
	ldr	r2, [r2]
	bl	_p_128_plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end193:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanged_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanged_object:
Leh_func_begin194:
	push	{r7, lr}
Ltmp372:
	mov	r7, sp
Ltmp373:
Ltmp374:
	movw	r2, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC194_0+8))
	movt	r2, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC194_0+8))
LPC194_0:
	add	r2, pc, r2
	ldr	r2, [r2, #460]
	ldr	r2, [r2]
	bl	_p_129_plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
	pop	{r7, pc}
Leh_func_end194:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs:
Leh_func_begin195:
	push	{r4, r5, r6, r7, lr}
Ltmp375:
	add	r7, sp, #12
Ltmp376:
Ltmp377:
	mov	r6, r0
	mov	r4, r2
	mov	r5, r1
	ldr	r0, [r6, #16]
	cmp	r0, #0
	beq	LBB195_2
	ldr	r0, [r6, #16]
	mov	r1, r5
	mov	r2, r4
	ldr	r3, [r0, #12]
	blx	r3
LBB195_2:
	ldr	r0, [r6, #8]
	cmp	r0, #0
	beq	LBB195_7
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC195_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC195_0+8))
	ldr	r0, [r0]
LPC195_0:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #16]
	cmp	r0, r1
	popne	{r4, r5, r6, r7, pc}
	ldr	r2, [r6, #8]
	cmp	r2, #0
	beq	LBB195_5
	ldr	r0, [r2]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r1
	moveq	r0, r2
	b	LBB195_6
LBB195_5:
	mov	r0, r2
LBB195_6:
	ldr	r1, [r0]
	mov	r2, r4
	mov	r1, r5
	bl	_p_128_plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
LBB195_7:
	pop	{r4, r5, r6, r7, pc}
Leh_func_end195:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs:
Leh_func_begin196:
	push	{r4, r5, r6, r7, lr}
Ltmp378:
	add	r7, sp, #12
Ltmp379:
Ltmp380:
	mov	r6, r0
	mov	r4, r2
	mov	r5, r1
	ldr	r0, [r6, #20]
	cmp	r0, #0
	beq	LBB196_2
	ldr	r0, [r6, #20]
	mov	r1, r5
	mov	r2, r4
	ldr	r3, [r0, #12]
	blx	r3
LBB196_2:
	ldr	r0, [r6, #8]
	cmp	r0, #0
	beq	LBB196_7
	movw	r1, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC196_0+8))
	movt	r1, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC196_0+8))
	ldr	r0, [r0]
LPC196_0:
	add	r1, pc, r1
	ldr	r1, [r1, #20]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #16]
	cmp	r0, r1
	popne	{r4, r5, r6, r7, pc}
	ldr	r2, [r6, #8]
	cmp	r2, #0
	beq	LBB196_5
	ldr	r0, [r2]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #16]
	mov	r0, #0
	cmp	r3, r1
	moveq	r0, r2
	b	LBB196_6
LBB196_5:
	mov	r0, r2
LBB196_6:
	ldr	r1, [r0]
	mov	r2, r4
	mov	r1, r5
	bl	_p_129_plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs_llvm
LBB196_7:
	pop	{r4, r5, r6, r7, pc}
Leh_func_end196:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__ctor_System_Xml_Linq_XObjectChange
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__ctor_System_Xml_Linq_XObjectChange:
Leh_func_begin197:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end197:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__cctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__cctor:
Leh_func_begin198:
	push	{r4, r7, lr}
Ltmp381:
	add	r7, sp, #4
Ltmp382:
Ltmp383:
	movw	r4, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC198_0+8))
	movt	r4, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC198_0+8))
LPC198_0:
	add	r4, pc, r4
	ldr	r0, [r4, #464]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #0
	str	r1, [r0, #8]
	ldr	r1, [r4, #452]
	str	r0, [r1]
	ldr	r0, [r4, #464]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #2
	str	r1, [r0, #8]
	ldr	r1, [r4, #468]
	str	r0, [r1]
	ldr	r0, [r4, #464]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #1
	str	r1, [r0, #8]
	ldr	r1, [r4, #456]
	str	r0, [r1]
	ldr	r0, [r4, #464]
	bl	_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r2, [r4, #460]
	mov	r1, #3
	str	r1, [r0, #8]
	str	r0, [r2]
	pop	{r4, r7, pc}
Leh_func_end198:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string:
Leh_func_begin199:
	push	{r7, lr}
Ltmp384:
	mov	r7, sp
Ltmp385:
Ltmp386:
	mov	r3, r2
	mov	r2, r1
	cmp	r2, #0
	beq	LBB199_3
	cmp	r3, #0
	strdne	r2, r3, [r0, #40]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC199_1+8))
	movw	r1, #1629
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC199_1+8))
LPC199_1:
	ldr	r0, [pc, r0]
LBB199_2:
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
LBB199_3:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC199_0+8))
	movw	r1, #1615
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC199_0+8))
LPC199_0:
	ldr	r0, [pc, r0]
	b	LBB199_2
Leh_func_end199:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction:
Leh_func_begin200:
	push	{r7, lr}
Ltmp387:
	mov	r7, sp
Ltmp388:
Ltmp389:
	cmp	r1, #0
	ldrne	r2, [r1, #40]
	strne	r2, [r0, #40]
	ldrne	r1, [r1, #44]
	strne	r1, [r0, #44]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC200_0+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC200_0+8))
LPC200_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end200:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Data
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Data:
Leh_func_begin201:
	ldr	r0, [r0, #44]
	bx	lr
Leh_func_end201:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_NodeType:
Leh_func_begin202:
	mov	r0, #7
	bx	lr
Leh_func_end202:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Target
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Target:
Leh_func_begin203:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end203:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_WriteTo_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_WriteTo_System_Xml_XmlWriter:
Leh_func_begin204:
	push	{r7, lr}
Ltmp390:
	mov	r7, sp
Ltmp391:
Ltmp392:
	ldr	r9, [r0, #40]
	ldr	r2, [r0, #44]
	ldr	r0, [r1]
	ldr	r3, [r0, #80]
	mov	r0, r1
	mov	r1, r9
	blx	r3
	pop	{r7, pc}
Leh_func_end204:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText__ctor_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText__ctor_string:
Leh_func_begin205:
	str	r1, [r0, #40]
	bx	lr
Leh_func_end205:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText__ctor_System_Xml_Linq_XText
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText__ctor_System_Xml_Linq_XText:
Leh_func_begin206:
	ldr	r1, [r1, #40]
	str	r1, [r0, #40]
	bx	lr
Leh_func_end206:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText_get_NodeType
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText_get_NodeType:
Leh_func_begin207:
	mov	r0, #3
	bx	lr
Leh_func_end207:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText_get_Value
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText_get_Value:
Leh_func_begin208:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end208:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText_set_Value_string
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText_set_Value_string:
Leh_func_begin209:
	push	{r7, lr}
Ltmp393:
	mov	r7, sp
Ltmp394:
Ltmp395:
	cmp	r1, #0
	strne	r1, [r0, #40]
	popne	{r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC209_0+8))
	mov	r1, #35
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC209_0+8))
LPC209_0:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end209:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText_WriteTo_System_Xml_XmlWriter
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText_WriteTo_System_Xml_XmlWriter:
Leh_func_begin210:
	push	{r4, r5, r6, r7, lr}
Ltmp396:
	add	r7, sp, #12
Ltmp397:
	push	{r8, r10}
Ltmp398:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5, #40]
	ldr	r0, [r0, #8]
	cmp	r0, #0
	ble	LBB210_5
	ldr	r10, [r5, #40]
	movw	r6, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC210_0+8))
	movt	r6, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC210_0+8))
LPC210_0:
	add	r6, pc, r6
	ldr	r0, [r6, #472]
	ldr	r1, [r0]
	cmp	r1, #0
	bne	LBB210_3
	ldr	r0, [r6, #480]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #484]
	str	r1, [r0, #20]
	ldr	r1, [r6, #488]
	str	r1, [r0, #28]
	ldr	r1, [r6, #492]
	str	r1, [r0, #12]
	ldr	r1, [r6, #472]
	str	r0, [r1]
	ldr	r0, [r6, #472]
	ldr	r1, [r0]
LBB210_3:
	ldr	r0, [r6, #476]
	mov	r8, r0
	mov	r0, r10
	bl	_p_130_plt_System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_llvm
	tst	r0, #255
	beq	LBB210_5
	ldr	r1, [r5, #40]
	ldr	r0, [r4]
	ldr	r2, [r0, #52]
	b	LBB210_6
LBB210_5:
	ldr	r1, [r5, #40]
	ldr	r0, [r4]
	ldr	r2, [r0, #56]
LBB210_6:
	mov	r0, r4
	blx	r2
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end210:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XText__WriteTom__0_char
	.align	2
_System_Xml_Linq__System_Xml_Linq_XText__WriteTom__0_char:
Leh_func_begin211:
	mov	r1, r0
	sub	r2, r1, #9
	cmp	r2, #23
	bhi	LBB211_2
	movw	r3, #17
	mov	r9, #1
	mov	r0, #1
	movt	r3, #128
	tst	r3, r9, lsl r2
	bxne	lr
LBB211_2:
	mov	r0, #0
	cmp	r1, #10
	moveq	r0, #1
	bx	lr
Leh_func_end211:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil_ExpandArray_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil_ExpandArray_object:
Leh_func_begin212:
	push	{r4, r7, lr}
Ltmp399:
	add	r7, sp, #4
Ltmp400:
Ltmp401:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC212_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC212_0+8))
LPC212_0:
	add	r0, pc, r0
	ldr	r0, [r0, #496]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mvn	r1, #1
	str	r4, [r0, #8]
	str	r1, [r0, #44]
	pop	{r4, r7, pc}
Leh_func_end212:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil_ToNode_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil_ToNode_object:
Leh_func_begin213:
	push	{r4, r7, lr}
Ltmp402:
	add	r7, sp, #4
Ltmp403:
Ltmp404:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB213_3
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_2+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_2+8))
	ldr	r1, [r4]
LPC213_2:
	add	r0, pc, r0
	ldr	r0, [r0, #208]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #8]
	cmp	r1, r0
	bne	LBB213_4
	cmp	r4, #0
	bne	LBB213_15
	b	LBB213_4
LBB213_3:
	bne	LBB213_15
LBB213_4:
	mov	r0, r4
	cmp	r4, #0
	beq	LBB213_6
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_3+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_3+8))
	ldr	r1, [r4]
LPC213_3:
	add	r0, pc, r0
	ldr	r0, [r0, #104]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #8]
	cmp	r1, r0
	mov	r0, r4
	movne	r0, #0
LBB213_6:
	cmp	r0, #0
	popne	{r4, r7, pc}
	cmp	r4, #0
	beq	LBB213_9
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_5+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_5+8))
	ldr	r1, [r4]
LPC213_5:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB213_13
	cmp	r4, #0
	bne	LBB213_10
	b	LBB213_13
LBB213_9:
	beq	LBB213_13
LBB213_10:
	cmp	r4, #0
	beq	LBB213_14
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_4+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_4+8))
	ldr	r1, [r4]
LPC213_4:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	beq	LBB213_14
Ltmp405:
	ldr	r0, LCPI213_0
LPC213_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB213_13:
	mov	r0, r4
	bl	_p_10_plt_System_Xml_Linq_XUtil_ToString_object_llvm
	mov	r4, r0
LBB213_14:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_6+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_6+8))
LPC213_6:
	add	r0, pc, r0
	ldr	r0, [r0, #368]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	str	r4, [r0, #40]
	pop	{r4, r7, pc}
LBB213_15:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC213_1+8))
	movw	r1, #1707
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC213_1+8))
LPC213_1:
	ldr	r0, [pc, r0]
	bl	_p_1_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
	.align	2
	.data_region
LCPI213_0:
	.long	Ltmp405-(LPC213_0+8)
	.end_data_region
Leh_func_end213:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject:
Leh_func_begin214:
	push	{r7, lr}
Ltmp406:
	mov	r7, sp
Ltmp407:
Ltmp408:
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	cmp	r1, #0
	popeq	{r7, pc}
	bl	_p_131_plt_System_Xml_Linq_XUtil_Clone_object_llvm
	pop	{r7, pc}
Leh_func_end214:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil_Clone_object
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil_Clone_object:
Leh_func_begin215:
	push	{r4, r5, r7, lr}
Ltmp409:
	add	r7, sp, #8
Ltmp410:
Ltmp411:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB215_3
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_9+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_9+8))
	ldr	r1, [r4]
LPC215_9:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB215_7
	cmp	r4, #0
	bne	LBB215_4
	b	LBB215_7
LBB215_3:
	beq	LBB215_7
LBB215_4:
	mov	r5, #0
	cmp	r4, #0
	beq	LBB215_6
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_34+8))
	mov	r5, r4
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_34+8))
	ldr	r1, [r4]
LPC215_34:
	add	r0, pc, r0
	ldr	r0, [r0, #108]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB215_64
LBB215_6:
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_7:
	cmp	r4, #0
	beq	LBB215_10
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_11+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_11+8))
	ldr	r1, [r4]
LPC215_11:
	add	r0, pc, r0
	ldr	r0, [r0, #208]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #8]
	cmp	r1, r0
	bne	LBB215_14
	cmp	r4, #0
	bne	LBB215_11
	b	LBB215_14
LBB215_10:
	beq	LBB215_14
LBB215_11:
	cmp	r4, #0
	beq	LBB215_13
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_33+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_33+8))
	ldr	r1, [r4]
LPC215_33:
	add	r0, pc, r0
	ldr	r0, [r0, #208]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #8]
	cmp	r1, r0
	bne	LBB215_65
LBB215_13:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_10+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_10+8))
LPC215_10:
	add	r0, pc, r0
	ldr	r0, [r0, #204]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_136_plt_System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_14:
	cmp	r4, #0
	beq	LBB215_17
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_13+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_13+8))
	ldr	r1, [r4]
LPC215_13:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB215_21
	cmp	r4, #0
	bne	LBB215_18
	b	LBB215_21
LBB215_17:
	beq	LBB215_21
LBB215_18:
	cmp	r4, #0
	beq	LBB215_20
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_32+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_32+8))
	ldr	r1, [r4]
LPC215_32:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB215_66
LBB215_20:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_12+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_12+8))
LPC215_12:
	add	r0, pc, r0
	ldr	r0, [r0, #200]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_135_plt_System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_21:
	cmp	r4, #0
	beq	LBB215_24
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_15+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_15+8))
	ldr	r1, [r4]
LPC215_15:
	add	r0, pc, r0
	ldr	r0, [r0, #500]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB215_28
	cmp	r4, #0
	bne	LBB215_25
	b	LBB215_28
LBB215_24:
	beq	LBB215_28
LBB215_25:
	cmp	r4, #0
	beq	LBB215_27
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_31+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_31+8))
	ldr	r1, [r4]
LPC215_31:
	add	r0, pc, r0
	ldr	r0, [r0, #500]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	bne	LBB215_67
LBB215_27:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_14+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_14+8))
LPC215_14:
	add	r0, pc, r0
	ldr	r0, [r0, #372]
	b	LBB215_35
LBB215_28:
	cmp	r4, #0
	beq	LBB215_31
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_17+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_17+8))
	ldr	r1, [r4]
LPC215_17:
	add	r0, pc, r0
	ldr	r0, [r0, #420]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_36
	cmp	r4, #0
	bne	LBB215_32
	b	LBB215_36
LBB215_31:
	beq	LBB215_36
LBB215_32:
	cmp	r4, #0
	beq	LBB215_34
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_30+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_30+8))
	ldr	r1, [r4]
LPC215_30:
	add	r0, pc, r0
	ldr	r0, [r0, #420]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_68
LBB215_34:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_16+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_16+8))
LPC215_16:
	add	r0, pc, r0
	ldr	r0, [r0, #380]
LBB215_35:
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r4, #40]
	str	r0, [r5, #40]
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_36:
	cmp	r4, #0
	beq	LBB215_39
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_19+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_19+8))
	ldr	r1, [r4]
LPC215_19:
	add	r0, pc, r0
	ldr	r0, [r0, #416]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_43
	cmp	r4, #0
	bne	LBB215_40
	b	LBB215_43
LBB215_39:
	beq	LBB215_43
LBB215_40:
	cmp	r4, #0
	beq	LBB215_42
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_29+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_29+8))
	ldr	r1, [r4]
LPC215_29:
	add	r0, pc, r0
	ldr	r0, [r0, #416]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_69
LBB215_42:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_18+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_18+8))
LPC215_18:
	add	r0, pc, r0
	ldr	r0, [r0, #376]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_134_plt_System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_43:
	cmp	r4, #0
	beq	LBB215_46
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_21+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_21+8))
	ldr	r1, [r4]
LPC215_21:
	add	r0, pc, r0
	ldr	r0, [r0, #280]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB215_50
	cmp	r4, #0
	bne	LBB215_47
	b	LBB215_50
LBB215_46:
	beq	LBB215_50
LBB215_47:
	cmp	r4, #0
	beq	LBB215_49
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_28+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_28+8))
	ldr	r1, [r4]
LPC215_28:
	add	r0, pc, r0
	ldr	r0, [r0, #280]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	bne	LBB215_70
LBB215_49:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_20+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_20+8))
LPC215_20:
	add	r0, pc, r0
	ldr	r0, [r0, #184]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_133_plt_System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_50:
	cmp	r4, #0
	beq	LBB215_53
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_23+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_23+8))
	ldr	r1, [r4]
LPC215_23:
	add	r0, pc, r0
	ldr	r0, [r0, #156]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_57
	cmp	r4, #0
	bne	LBB215_54
	b	LBB215_57
LBB215_53:
	beq	LBB215_57
LBB215_54:
	cmp	r4, #0
	beq	LBB215_56
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_27+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_27+8))
	ldr	r1, [r4]
LPC215_27:
	add	r0, pc, r0
	ldr	r0, [r0, #156]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_71
LBB215_56:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_22+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_22+8))
LPC215_22:
	add	r0, pc, r0
	ldr	r0, [r0, #392]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_132_plt_System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
LBB215_57:
	cmp	r4, #0
	beq	LBB215_60
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_24+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_24+8))
	ldr	r1, [r4]
LPC215_24:
	add	r0, pc, r0
	ldr	r0, [r0, #192]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_72
	cmp	r4, #0
	beq	LBB215_72
	b	LBB215_61
LBB215_60:
	beq	LBB215_72
LBB215_61:
	cmp	r4, #0
	beq	LBB215_63
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_26+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_26+8))
	ldr	r1, [r4]
LPC215_26:
	add	r0, pc, r0
	ldr	r0, [r0, #192]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	bne	LBB215_73
LBB215_63:
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC215_25+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC215_25+8))
LPC215_25:
	add	r0, pc, r0
	ldr	r0, [r0, #368]
	b	LBB215_35
Ltmp412:
LBB215_64:
	ldr	r0, LCPI215_0
LPC215_0:
	add	r1, pc, r0
	b	LBB215_74
Ltmp413:
LBB215_65:
	ldr	r0, LCPI215_1
LPC215_1:
	add	r1, pc, r0
	b	LBB215_74
Ltmp414:
LBB215_66:
	ldr	r0, LCPI215_2
LPC215_2:
	add	r1, pc, r0
	b	LBB215_74
Ltmp415:
LBB215_67:
	ldr	r0, LCPI215_3
LPC215_3:
	add	r1, pc, r0
	b	LBB215_74
Ltmp416:
LBB215_68:
	ldr	r0, LCPI215_4
LPC215_4:
	add	r1, pc, r0
	b	LBB215_74
Ltmp417:
LBB215_69:
	ldr	r0, LCPI215_5
LPC215_5:
	add	r1, pc, r0
	b	LBB215_74
Ltmp418:
LBB215_70:
	ldr	r0, LCPI215_6
LPC215_6:
	add	r1, pc, r0
	b	LBB215_74
Ltmp419:
LBB215_71:
	ldr	r0, LCPI215_7
LPC215_7:
	add	r1, pc, r0
	b	LBB215_74
LBB215_72:
	movw	r0, #518
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp420:
LBB215_73:
	ldr	r0, LCPI215_8
LPC215_8:
	add	r1, pc, r0
LBB215_74:
	movw	r0, #605
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI215_0:
	.long	Ltmp412-(LPC215_0+8)
LCPI215_1:
	.long	Ltmp413-(LPC215_1+8)
LCPI215_2:
	.long	Ltmp414-(LPC215_2+8)
LCPI215_3:
	.long	Ltmp415-(LPC215_3+8)
LCPI215_4:
	.long	Ltmp416-(LPC215_4+8)
LCPI215_5:
	.long	Ltmp417-(LPC215_5+8)
LCPI215_6:
	.long	Ltmp418-(LPC215_6+8)
LCPI215_7:
	.long	Ltmp419-(LPC215_7+8)
LCPI215_8:
	.long	Ltmp420-(LPC215_8+8)
	.end_data_region
Leh_func_end215:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerator_object_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerator_object_get_Current:
Leh_func_begin216:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end216:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerator_get_Current
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerator_get_Current:
Leh_func_begin217:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end217:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0__ctor
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0__ctor:
Leh_func_begin218:
	bx	lr
Leh_func_end218:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Reset
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Reset:
Leh_func_begin219:
	push	{r7, lr}
Ltmp421:
	mov	r7, sp
Ltmp422:
Ltmp423:
	movw	r0, #629
	movt	r0, #512
	bl	_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end219:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerable_GetEnumerator:
Leh_func_begin220:
	push	{r7, lr}
Ltmp424:
	mov	r7, sp
Ltmp425:
Ltmp426:
	bl	_p_137_plt_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator_llvm
	pop	{r7, pc}
Leh_func_end220:

	.private_extern	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator
	.align	2
_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator:
Leh_func_begin221:
	push	{r4, r7, lr}
Ltmp427:
	add	r7, sp, #4
Ltmp428:
Ltmp429:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB221_2
	add	r0, r4, #44
	mov	r1, #0
	mvn	r2, #1
	bl	_p_35_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm
	cmn	r0, #2
	moveq	r0, r4
	popeq	{r4, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC221_1+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC221_1+8))
LPC221_1:
	add	r0, pc, r0
	ldr	r0, [r0, #496]
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #8]
	str	r1, [r0, #8]
	pop	{r4, r7, pc}
Ltmp430:
LBB221_2:
	ldr	r0, LCPI221_0
LPC221_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI221_0:
	.long	Ltmp430-(LPC221_0+8)
	.end_data_region
Leh_func_end221:

	.private_extern	_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_System_Xml_Linq_XAttribute_bool_invoke_TResult__this___T_System_Xml_Linq_XAttribute
	.align	2
_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_System_Xml_Linq_XAttribute_bool_invoke_TResult__this___T_System_Xml_Linq_XAttribute:
Leh_func_begin222:
	push	{r4, r5, r7, lr}
Ltmp431:
	add	r7, sp, #8
Ltmp432:
Ltmp433:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC222_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC222_0+8))
LPC222_0:
	add	r0, pc, r0
	ldr	r0, [r0, #504]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB222_2
	bl	_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB222_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB222_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB222_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB222_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB222_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end222:

	.private_extern	_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XNamespace_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XNamespace_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XNamespace
	.align	2
_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XNamespace_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XNamespace_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XNamespace:
Leh_func_begin223:
	push	{r4, r5, r6, r7, lr}
Ltmp434:
	add	r7, sp, #12
Ltmp435:
	push	{r10, r11}
Ltmp436:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC223_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC223_0+8))
LPC223_0:
	add	r0, pc, r0
	ldr	r0, [r0, #504]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB223_2
	bl	_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB223_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB223_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB223_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB223_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB223_7
LBB223_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB223_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end223:

	.private_extern	_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XName_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XName_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XName
	.align	2
_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XName_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XName_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XName:
Leh_func_begin224:
	push	{r4, r5, r6, r7, lr}
Ltmp437:
	add	r7, sp, #12
Ltmp438:
	push	{r10, r11}
Ltmp439:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC224_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC224_0+8))
LPC224_0:
	add	r0, pc, r0
	ldr	r0, [r0, #504]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB224_2
	bl	_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB224_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB224_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB224_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB224_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB224_7
LBB224_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB224_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end224:

	.private_extern	_System_Xml_Linq__wrapper_delegate_invoke_System_EventHandler_1_System_Xml_Linq_XObjectChangeEventArgs_invoke_void__this___object_TEventArgs_object_System_Xml_Linq_XObjectChangeEventArgs
	.align	2
_System_Xml_Linq__wrapper_delegate_invoke_System_EventHandler_1_System_Xml_Linq_XObjectChangeEventArgs_invoke_void__this___object_TEventArgs_object_System_Xml_Linq_XObjectChangeEventArgs:
Leh_func_begin225:
	push	{r4, r5, r6, r7, lr}
Ltmp440:
	add	r7, sp, #12
Ltmp441:
Ltmp442:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC225_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC225_0+8))
LPC225_0:
	add	r0, pc, r0
	ldr	r0, [r0, #504]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB225_2
	bl	_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB225_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB225_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB225_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB225_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB225_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end225:

	.private_extern	_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_char_bool_invoke_TResult__this___T_char
	.align	2
_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_char_bool_invoke_TResult__this___T_char:
Leh_func_begin226:
	push	{r4, r5, r7, lr}
Ltmp443:
	add	r7, sp, #8
Ltmp444:
Ltmp445:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC226_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC226_0+8))
LPC226_0:
	add	r0, pc, r0
	ldr	r0, [r0, #504]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB226_2
	bl	_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB226_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB226_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB226_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB226_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB226_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end226:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current:
Leh_func_begin227:
	push	{r4, r5, r7, lr}
Ltmp446:
	add	r7, sp, #8
Ltmp447:
	push	{r8}
Ltmp448:
	movw	r5, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC227_0+8))
	movt	r5, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC227_0+8))
LPC227_0:
	add	r5, pc, r5
	ldr	r1, [r5, #516]
	mov	r8, r1
	bl	_p_140_plt_System_Array_InternalEnumerator_1_char_get_Current_llvm
	mov	r4, r0
	ldr	r0, [r5, #520]
	bl	_p_67_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	strh	r4, [r0, #8]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end227:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array:
Leh_func_begin228:
	mov	r2, r1
	mvn	r3, #1
	strd	r2, r3, [r0]
	bx	lr
Leh_func_end228:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current:
Leh_func_begin229:
	push	{r7, lr}
Ltmp449:
	mov	r7, sp
Ltmp450:
	push	{r8}
Ltmp451:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	beq	LBB229_3
	ldr	r1, [r0, #4]
	cmn	r1, #1
	beq	LBB229_4
	ldr	r2, [r0]
	ldr	r1, [r0]
	ldr	r1, [r1, #12]
	ldr	r9, [r0, #4]
	movw	r3, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC229_0+8))
	movt	r3, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC229_0+8))
	ldr	r0, [r2]
LPC229_0:
	add	r3, pc, r3
	mov	r0, r2
	ldr	r3, [r3, #524]
	sub	r1, r1, #1
	sub	r1, r1, r9
	mov	r8, r3
	bl	_p_141_plt_System_Array_InternalArray__get_Item_char_int_llvm
	pop	{r8}
	pop	{r7, pc}
LBB229_3:
	movw	r0, #47632
	b	LBB229_5
LBB229_4:
	movw	r0, #47718
LBB229_5:
	bl	_p_142_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end229:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose:
Leh_func_begin230:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end230:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext:
Leh_func_begin231:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	ldreq	r1, [r0]
	ldreq	r1, [r1, #12]
	streq	r1, [r0, #4]
	mov	r1, #0
	ldr	r2, [r0, #4]
	cmn	r2, #1
	beq	LBB231_2
	ldr	r1, [r0, #4]
	cmp	r1, #0
	sub	r2, r1, #1
	movne	r1, #1
	str	r2, [r0, #4]
LBB231_2:
	mov	r0, r1
	bx	lr
Leh_func_end231:

	.private_extern	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset
	.align	2
_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset:
Leh_func_begin232:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end232:

	.private_extern	_System_Xml_Linq__System_Array_InternalArray__IEnumerable_GetEnumerator_char
	.align	2
_System_Xml_Linq__System_Array_InternalArray__IEnumerable_GetEnumerator_char:
Leh_func_begin233:
	push	{r4, r7, lr}
Ltmp452:
	add	r7, sp, #4
Ltmp453:
	push	{r8}
Ltmp454:
	sub	sp, sp, #16
	bic	sp, sp, #7
	mov	r1, r0
	movw	r0, :lower16:(_mono_aot_System_Xml_Linq_got-(LPC233_0+8))
	movt	r0, :upper16:(_mono_aot_System_Xml_Linq_got-(LPC233_0+8))
LPC233_0:
	add	r0, pc, r0
	ldr	r4, [r0, #516]
	mov	r0, #0
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
	mov	r8, r4
	bl	_p_143_plt_System_Array_InternalEnumerator_1_char__ctor_System_Array_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	_p_15_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end233:

	.private_extern	_System_Xml_Linq__System_Array_InternalArray__get_Item_char_int
	.align	2
_System_Xml_Linq__System_Array_InternalArray__get_Item_char_int:
Leh_func_begin234:
	push	{r7, lr}
Ltmp455:
	mov	r7, sp
Ltmp456:
Ltmp457:
	ldr	r2, [r0, #12]
	cmp	r2, r1
	addhi	r0, r0, r1, lsl #1
	ldrhhi	r0, [r0, #16]
	pophi	{r7, pc}
	movw	r0, #11703
	bl	_p_142_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_3_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end234:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_type_info_2,4,2
.zerofill __DATA,__bss,_type_info_3,4,2
.zerofill __DATA,__bss,_type_info_4,4,2
.zerofill __DATA,__bss,_type_info_5,4,2
.zerofill __DATA,__bss,_type_info_6,4,2
.zerofill __DATA,__bss,_type_info_7,4,2
.zerofill __DATA,__bss,_type_info_8,4,2
.zerofill __DATA,__bss,_type_info_9,4,2
.zerofill __DATA,__bss,_type_info_10,4,2
.zerofill __DATA,__bss,_type_info_11,4,2
.zerofill __DATA,__bss,_type_info_12,4,2
.zerofill __DATA,__bss,_type_info_13,4,2
.zerofill __DATA,__bss,_type_info_14,4,2
.zerofill __DATA,__bss,_type_info_15,4,2
.zerofill __DATA,__bss,_type_info_16,4,2
.zerofill __DATA,__bss,_type_info_17,4,2
.zerofill __DATA,__bss,_type_info_18,4,2
.zerofill __DATA,__bss,_type_info_19,4,2
.zerofill __DATA,__bss,_mono_aot_System_Xml_Linq_got,1252,4
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Name
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NextAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_NextAttribute_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_PreviousAttribute_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_Remove
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute_ToString
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XAttribute__cctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XCData__ctor_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XCData__ctor_System_Xml_Linq_XCData
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XCData_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XCData_WriteTo_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XComment__ctor_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XComment__ctor_System_Xml_Linq_XComment
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XComment_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XComment_get_Value
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XComment_WriteTo_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer_get_FirstNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer_Nodes
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XNode_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_MoveNext
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Dispose
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Reset
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Encoding
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Standalone
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Version
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDeclaration_ToString
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_get_Declaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_set_Declaration_System_Xml_Linq_XDeclaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_Load_System_IO_Stream
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocument_VerifyAddedNode_object_bool
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_Name
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_PublicId
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_SystemId
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_InternalSubset
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XDocumentType_WriteTo_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_FirstAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_set_FirstAttribute_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_LastAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_set_LastAttribute_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_HasAttributes
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_IsEmpty
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_Name
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_Attributes
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_GetSchema
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__cctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacem__0_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XAttribute_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_MoveNext
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Dispose
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Reset
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__m__0_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerator_string_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Reset
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerable_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_get_LocalName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_get_Namespace
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_Equals_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_System_IEquatable_System_Xml_Linq_XName_Equals_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_Get_string_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_GetHashCode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XName_ToString
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace__cctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_None
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_Xmlns
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace__ctor_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_get_NamespaceName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_Equals_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_op_Implicit_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_GetHashCode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNamespace_ToString
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_set_PreviousNode_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_get_NextNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_set_NextNode_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_ToString
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode_CreateReader
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNode__cctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_object_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_object_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LineNumber
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LinePosition
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_AttributeCount
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_BaseURI
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Depth
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_EOF
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_HasAttributes
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_IsEmptyElement
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_LocalName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NamespaceURI
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NameTable
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Prefix
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_ReadState
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Value
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_HasLineInfo
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetName_int
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_Close
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_LookupNamespace_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToElement
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToFirstAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToNextAttribute
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_Read
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_ReadAttributeValue
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XNodeReader_ResolveEntity
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LineNumber
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LinePosition
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_BaseUri
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_set_BaseUri_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_Document
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_Parent
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_Owner
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_LineNumber
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_set_LineNumber_int
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_get_LinePosition
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_set_LinePosition_int
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_SetOwner_System_Xml_Linq_XContainer
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_HasLineInfo
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnAddingObject_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnAddedObject_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovingObject_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnRemovedObject_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanging_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanged_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__ctor_System_Xml_Linq_XObjectChange
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__cctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Data
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Target
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_WriteTo_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText__ctor_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText__ctor_System_Xml_Linq_XText
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText_get_NodeType
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText_get_Value
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText_set_Value_string
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText_WriteTo_System_Xml_XmlWriter
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XText__WriteTom__0_char
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil_ExpandArray_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil_ToNode_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil_Clone_object
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerator_object_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0__ctor
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Reset
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator
	.no_dead_strip	_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_System_Xml_Linq_XAttribute_bool_invoke_TResult__this___T_System_Xml_Linq_XAttribute
	.no_dead_strip	_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XNamespace_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XNamespace_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XNamespace
	.no_dead_strip	_System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XName_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XName_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XName
	.no_dead_strip	_System_Xml_Linq__wrapper_delegate_invoke_System_EventHandler_1_System_Xml_Linq_XObjectChangeEventArgs_invoke_void__this___object_TEventArgs_object_System_Xml_Linq_XObjectChangeEventArgs
	.no_dead_strip	_System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_char_bool_invoke_TResult__this___T_char
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalArray__IEnumerable_GetEnumerator_char
	.no_dead_strip	_System_Xml_Linq__System_Array_InternalArray__get_Item_char_int
	.no_dead_strip	_mono_aot_System_Xml_Linq_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	235
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	21
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	22
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	23
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	24
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	27
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	28
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	29
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	30
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	31
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	32
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	33
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	34
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	35
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	36
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	37
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	38
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	39
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	40
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	41
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	42
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	43
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	44
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	45
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	46
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	47
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	49
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	51
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	53
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	54
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	55
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	57
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	58
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	59
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	60
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	61
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	62
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	63
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	64
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	65
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	66
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	67
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	68
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	69
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	70
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	71
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	72
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	73
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	74
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	75
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	76
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	77
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	78
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	79
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	80
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	81
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	82
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	84
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	88
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	90
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	91
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	92
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	93
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	94
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	95
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	96
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	97
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	98
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	99
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	100
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	101
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	102
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	103
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	104
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	105
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	106
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	107
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	110
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	111
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	112
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	113
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	114
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	115
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	116
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	117
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	118
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	119
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	120
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	121
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	122
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	123
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	124
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	125
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	126
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	127
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	128
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	131
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	132
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	133
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	134
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	135
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	136
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	137
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	138
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	139
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	140
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	141
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	142
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	143
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	145
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	146
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	147
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	148
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	149
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	150
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	151
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
	.long	152
Lset137 = Lmono_eh_func_begin137-mono_eh_frame
	.long	Lset137
	.long	154
Lset138 = Lmono_eh_func_begin138-mono_eh_frame
	.long	Lset138
	.long	155
Lset139 = Lmono_eh_func_begin139-mono_eh_frame
	.long	Lset139
	.long	156
Lset140 = Lmono_eh_func_begin140-mono_eh_frame
	.long	Lset140
	.long	157
Lset141 = Lmono_eh_func_begin141-mono_eh_frame
	.long	Lset141
	.long	159
Lset142 = Lmono_eh_func_begin142-mono_eh_frame
	.long	Lset142
	.long	160
Lset143 = Lmono_eh_func_begin143-mono_eh_frame
	.long	Lset143
	.long	161
Lset144 = Lmono_eh_func_begin144-mono_eh_frame
	.long	Lset144
	.long	162
Lset145 = Lmono_eh_func_begin145-mono_eh_frame
	.long	Lset145
	.long	163
Lset146 = Lmono_eh_func_begin146-mono_eh_frame
	.long	Lset146
	.long	164
Lset147 = Lmono_eh_func_begin147-mono_eh_frame
	.long	Lset147
	.long	165
Lset148 = Lmono_eh_func_begin148-mono_eh_frame
	.long	Lset148
	.long	166
Lset149 = Lmono_eh_func_begin149-mono_eh_frame
	.long	Lset149
	.long	167
Lset150 = Lmono_eh_func_begin150-mono_eh_frame
	.long	Lset150
	.long	168
Lset151 = Lmono_eh_func_begin151-mono_eh_frame
	.long	Lset151
	.long	169
Lset152 = Lmono_eh_func_begin152-mono_eh_frame
	.long	Lset152
	.long	170
Lset153 = Lmono_eh_func_begin153-mono_eh_frame
	.long	Lset153
	.long	171
Lset154 = Lmono_eh_func_begin154-mono_eh_frame
	.long	Lset154
	.long	172
Lset155 = Lmono_eh_func_begin155-mono_eh_frame
	.long	Lset155
	.long	173
Lset156 = Lmono_eh_func_begin156-mono_eh_frame
	.long	Lset156
	.long	174
Lset157 = Lmono_eh_func_begin157-mono_eh_frame
	.long	Lset157
	.long	175
Lset158 = Lmono_eh_func_begin158-mono_eh_frame
	.long	Lset158
	.long	176
Lset159 = Lmono_eh_func_begin159-mono_eh_frame
	.long	Lset159
	.long	177
Lset160 = Lmono_eh_func_begin160-mono_eh_frame
	.long	Lset160
	.long	179
Lset161 = Lmono_eh_func_begin161-mono_eh_frame
	.long	Lset161
	.long	180
Lset162 = Lmono_eh_func_begin162-mono_eh_frame
	.long	Lset162
	.long	181
Lset163 = Lmono_eh_func_begin163-mono_eh_frame
	.long	Lset163
	.long	182
Lset164 = Lmono_eh_func_begin164-mono_eh_frame
	.long	Lset164
	.long	183
Lset165 = Lmono_eh_func_begin165-mono_eh_frame
	.long	Lset165
	.long	184
Lset166 = Lmono_eh_func_begin166-mono_eh_frame
	.long	Lset166
	.long	185
Lset167 = Lmono_eh_func_begin167-mono_eh_frame
	.long	Lset167
	.long	186
Lset168 = Lmono_eh_func_begin168-mono_eh_frame
	.long	Lset168
	.long	187
Lset169 = Lmono_eh_func_begin169-mono_eh_frame
	.long	Lset169
	.long	188
Lset170 = Lmono_eh_func_begin170-mono_eh_frame
	.long	Lset170
	.long	191
Lset171 = Lmono_eh_func_begin171-mono_eh_frame
	.long	Lset171
	.long	192
Lset172 = Lmono_eh_func_begin172-mono_eh_frame
	.long	Lset172
	.long	193
Lset173 = Lmono_eh_func_begin173-mono_eh_frame
	.long	Lset173
	.long	194
Lset174 = Lmono_eh_func_begin174-mono_eh_frame
	.long	Lset174
	.long	195
Lset175 = Lmono_eh_func_begin175-mono_eh_frame
	.long	Lset175
	.long	196
Lset176 = Lmono_eh_func_begin176-mono_eh_frame
	.long	Lset176
	.long	197
Lset177 = Lmono_eh_func_begin177-mono_eh_frame
	.long	Lset177
	.long	198
Lset178 = Lmono_eh_func_begin178-mono_eh_frame
	.long	Lset178
	.long	199
Lset179 = Lmono_eh_func_begin179-mono_eh_frame
	.long	Lset179
	.long	201
Lset180 = Lmono_eh_func_begin180-mono_eh_frame
	.long	Lset180
	.long	202
Lset181 = Lmono_eh_func_begin181-mono_eh_frame
	.long	Lset181
	.long	203
Lset182 = Lmono_eh_func_begin182-mono_eh_frame
	.long	Lset182
	.long	204
Lset183 = Lmono_eh_func_begin183-mono_eh_frame
	.long	Lset183
	.long	205
Lset184 = Lmono_eh_func_begin184-mono_eh_frame
	.long	Lset184
	.long	206
Lset185 = Lmono_eh_func_begin185-mono_eh_frame
	.long	Lset185
	.long	207
Lset186 = Lmono_eh_func_begin186-mono_eh_frame
	.long	Lset186
	.long	208
Lset187 = Lmono_eh_func_begin187-mono_eh_frame
	.long	Lset187
	.long	209
Lset188 = Lmono_eh_func_begin188-mono_eh_frame
	.long	Lset188
	.long	210
Lset189 = Lmono_eh_func_begin189-mono_eh_frame
	.long	Lset189
	.long	211
Lset190 = Lmono_eh_func_begin190-mono_eh_frame
	.long	Lset190
	.long	212
Lset191 = Lmono_eh_func_begin191-mono_eh_frame
	.long	Lset191
	.long	213
Lset192 = Lmono_eh_func_begin192-mono_eh_frame
	.long	Lset192
	.long	214
Lset193 = Lmono_eh_func_begin193-mono_eh_frame
	.long	Lset193
	.long	215
Lset194 = Lmono_eh_func_begin194-mono_eh_frame
	.long	Lset194
	.long	216
Lset195 = Lmono_eh_func_begin195-mono_eh_frame
	.long	Lset195
	.long	217
Lset196 = Lmono_eh_func_begin196-mono_eh_frame
	.long	Lset196
	.long	218
Lset197 = Lmono_eh_func_begin197-mono_eh_frame
	.long	Lset197
	.long	219
Lset198 = Lmono_eh_func_begin198-mono_eh_frame
	.long	Lset198
	.long	220
Lset199 = Lmono_eh_func_begin199-mono_eh_frame
	.long	Lset199
	.long	221
Lset200 = Lmono_eh_func_begin200-mono_eh_frame
	.long	Lset200
	.long	222
Lset201 = Lmono_eh_func_begin201-mono_eh_frame
	.long	Lset201
	.long	223
Lset202 = Lmono_eh_func_begin202-mono_eh_frame
	.long	Lset202
	.long	224
Lset203 = Lmono_eh_func_begin203-mono_eh_frame
	.long	Lset203
	.long	225
Lset204 = Lmono_eh_func_begin204-mono_eh_frame
	.long	Lset204
	.long	226
Lset205 = Lmono_eh_func_begin205-mono_eh_frame
	.long	Lset205
	.long	227
Lset206 = Lmono_eh_func_begin206-mono_eh_frame
	.long	Lset206
	.long	228
Lset207 = Lmono_eh_func_begin207-mono_eh_frame
	.long	Lset207
	.long	229
Lset208 = Lmono_eh_func_begin208-mono_eh_frame
	.long	Lset208
	.long	230
Lset209 = Lmono_eh_func_begin209-mono_eh_frame
	.long	Lset209
	.long	231
Lset210 = Lmono_eh_func_begin210-mono_eh_frame
	.long	Lset210
	.long	232
Lset211 = Lmono_eh_func_begin211-mono_eh_frame
	.long	Lset211
	.long	234
Lset212 = Lmono_eh_func_begin212-mono_eh_frame
	.long	Lset212
	.long	235
Lset213 = Lmono_eh_func_begin213-mono_eh_frame
	.long	Lset213
	.long	236
Lset214 = Lmono_eh_func_begin214-mono_eh_frame
	.long	Lset214
	.long	237
Lset215 = Lmono_eh_func_begin215-mono_eh_frame
	.long	Lset215
	.long	238
Lset216 = Lmono_eh_func_begin216-mono_eh_frame
	.long	Lset216
	.long	239
Lset217 = Lmono_eh_func_begin217-mono_eh_frame
	.long	Lset217
	.long	240
Lset218 = Lmono_eh_func_begin218-mono_eh_frame
	.long	Lset218
	.long	243
Lset219 = Lmono_eh_func_begin219-mono_eh_frame
	.long	Lset219
	.long	244
Lset220 = Lmono_eh_func_begin220-mono_eh_frame
	.long	Lset220
	.long	245
Lset221 = Lmono_eh_func_begin221-mono_eh_frame
	.long	Lset221
	.long	252
Lset222 = Lmono_eh_func_begin222-mono_eh_frame
	.long	Lset222
	.long	257
Lset223 = Lmono_eh_func_begin223-mono_eh_frame
	.long	Lset223
	.long	262
Lset224 = Lmono_eh_func_begin224-mono_eh_frame
	.long	Lset224
	.long	263
Lset225 = Lmono_eh_func_begin225-mono_eh_frame
	.long	Lset225
	.long	268
Lset226 = Lmono_eh_func_begin226-mono_eh_frame
	.long	Lset226
	.long	271
Lset227 = Lmono_eh_func_begin227-mono_eh_frame
	.long	Lset227
	.long	272
Lset228 = Lmono_eh_func_begin228-mono_eh_frame
	.long	Lset228
	.long	273
Lset229 = Lmono_eh_func_begin229-mono_eh_frame
	.long	Lset229
	.long	274
Lset230 = Lmono_eh_func_begin230-mono_eh_frame
	.long	Lset230
	.long	275
Lset231 = Lmono_eh_func_begin231-mono_eh_frame
	.long	Lset231
	.long	276
Lset232 = Lmono_eh_func_begin232-mono_eh_frame
	.long	Lset232
	.long	277
Lset233 = Lmono_eh_func_begin233-mono_eh_frame
	.long	Lset233
	.long	279
Lset234 = Lmono_eh_func_begin234-mono_eh_frame
	.long	Lset234
Lset235 = Leh_func_end234-Leh_func_begin234
	.long	Lset235
Lset236 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset236
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin40:
	.byte	0

Lmono_eh_func_begin41:
	.byte	0

Lmono_eh_func_begin42:
	.byte	0

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin44:
	.byte	0

Lmono_eh_func_begin45:
	.byte	0

Lmono_eh_func_begin46:
	.byte	0

Lmono_eh_func_begin47:
	.byte	0

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin51:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin56:
	.byte	0

Lmono_eh_func_begin57:
	.byte	0

Lmono_eh_func_begin58:
	.byte	0

Lmono_eh_func_begin59:
	.byte	0

Lmono_eh_func_begin60:
	.byte	0

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin64:
	.byte	0

Lmono_eh_func_begin65:
	.byte	0

Lmono_eh_func_begin66:
	.byte	0

Lmono_eh_func_begin67:
	.byte	0

Lmono_eh_func_begin68:
	.byte	0

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin70:
	.byte	0

Lmono_eh_func_begin71:
	.byte	0

Lmono_eh_func_begin72:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin73:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin74:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin75:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin76:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin77:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin78:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin80:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin81:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin82:
	.byte	0

Lmono_eh_func_begin83:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin84:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin85:
	.byte	0

Lmono_eh_func_begin86:
	.byte	0

Lmono_eh_func_begin87:
	.byte	0

Lmono_eh_func_begin88:
	.byte	0

Lmono_eh_func_begin89:
	.byte	0

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin92:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin93:
	.byte	0

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin95:
	.byte	0

Lmono_eh_func_begin96:
	.byte	0

Lmono_eh_func_begin97:
	.byte	0

Lmono_eh_func_begin98:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin101:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin102:
	.byte	0

Lmono_eh_func_begin103:
	.byte	0

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin105:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin106:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin107:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin108:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin109:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin110:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin113:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin114:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin115:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin116:
	.byte	0

Lmono_eh_func_begin117:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin118:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin119:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin120:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin121:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin122:
	.byte	0

Lmono_eh_func_begin123:
	.byte	0

Lmono_eh_func_begin124:
	.byte	0

Lmono_eh_func_begin125:
	.byte	0

Lmono_eh_func_begin126:
	.byte	0

Lmono_eh_func_begin127:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin128:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin129:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin131:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin132:
	.byte	0

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin135:
	.byte	0

Lmono_eh_func_begin136:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin137:
	.byte	0

Lmono_eh_func_begin138:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin139:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin140:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin141:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin142:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin143:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin144:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin145:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin146:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin147:
	.byte	0

Lmono_eh_func_begin148:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin149:
	.byte	0

Lmono_eh_func_begin150:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin151:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin152:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin153:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin154:
	.byte	0

Lmono_eh_func_begin155:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin156:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin157:
	.byte	0

Lmono_eh_func_begin158:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin159:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin160:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin161:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin162:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin163:
	.byte	0

Lmono_eh_func_begin164:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin165:
	.byte	0

Lmono_eh_func_begin166:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin167:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin168:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin169:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin170:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin171:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin172:
	.byte	0

Lmono_eh_func_begin173:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin174:
	.byte	0

Lmono_eh_func_begin175:
	.byte	0

Lmono_eh_func_begin176:
	.byte	0

Lmono_eh_func_begin177:
	.byte	0

Lmono_eh_func_begin178:
	.byte	0

Lmono_eh_func_begin179:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin180:
	.byte	0

Lmono_eh_func_begin181:
	.byte	0

Lmono_eh_func_begin182:
	.byte	0

Lmono_eh_func_begin183:
	.byte	0

Lmono_eh_func_begin184:
	.byte	0

Lmono_eh_func_begin185:
	.byte	0

Lmono_eh_func_begin186:
	.byte	0

Lmono_eh_func_begin187:
	.byte	0

Lmono_eh_func_begin188:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin189:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin190:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin191:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin192:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin193:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin194:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin195:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin196:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin197:
	.byte	0

Lmono_eh_func_begin198:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin199:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin200:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin201:
	.byte	0

Lmono_eh_func_begin202:
	.byte	0

Lmono_eh_func_begin203:
	.byte	0

Lmono_eh_func_begin204:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin205:
	.byte	0

Lmono_eh_func_begin206:
	.byte	0

Lmono_eh_func_begin207:
	.byte	0

Lmono_eh_func_begin208:
	.byte	0

Lmono_eh_func_begin209:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin210:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin211:
	.byte	0

Lmono_eh_func_begin212:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin213:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin214:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin215:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin216:
	.byte	0

Lmono_eh_func_begin217:
	.byte	0

Lmono_eh_func_begin218:
	.byte	0

Lmono_eh_func_begin219:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin220:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin221:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin222:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin223:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin224:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin225:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin226:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin227:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin228:
	.byte	0

Lmono_eh_func_begin229:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin230:
	.byte	0

Lmono_eh_func_begin231:
	.byte	0

Lmono_eh_func_begin232:
	.byte	0

Lmono_eh_func_begin233:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin234:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "System.Xml.Linq.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XContainer_CheckChildType_object_bool_0
_System_Xml_Linq_XContainer_CheckChildType_object_bool_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,80,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,76,32,203,229
	.byte 0,0,160,227,0,0,139,229,0,0,160,227,4,0,139,229,0,0,90,227,228,0,0,10,10,64,160,225,44,160,139,229
	.byte 0,0,90,227,11,0,0,10,0,0,148,229,0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 96
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,44,0,139,229,44,0,155,229,0,0,80,227,209,0,0,26
	.byte 10,64,160,225,48,160,139,229,0,0,90,227,11,0,0,10,0,0,148,229,0,0,144,229,8,0,144,229,8,0,144,229
	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 92
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,48,0,139,229,48,0,155,229,0,0,80,227,190,0,0,26
	.byte 10,64,160,225,52,160,139,229,0,0,90,227,22,0,0,10,52,0,155,229,0,80,144,229,180,1,213,225,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,1,0,80,225,13,0,0,58,16,0,149,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,56,0,139,229,1,0,0,234,0,0,160,227,56,0,139,229,56,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,8,0,139,229,0,0,0,234,8,64,139,229,8,0,155,229,0,0,80,227
	.byte 134,0,0,10,12,160,139,229,10,0,160,225,60,0,139,229,12,0,155,229,0,0,80,227,22,0,0,10,60,0,155,229
	.byte 0,0,144,229,180,17,208,225,0,32,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 2,32,159,231,2,0,81,225,136,0,0,59,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,122,0,0,11,12,0,155,229,0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 76
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,0,0,139,229,14,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 84
	.byte 8,128,159,231,4,224,143,226,16,240,17,229,0,0,0,0,0,80,160,225,6,0,160,225,5,16,160,225,76,32,219,229
bl _p_24

	.byte 0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,227,255,255,26,0,0,0,235
	.byte 77,0,0,234,40,224,139,229,0,0,155,229,20,0,139,229,20,0,155,229,64,0,139,229,0,0,80,227,24,0,0,10
	.byte 64,0,155,229,0,0,144,229,68,0,139,229,180,1,208,225,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,1,0,80,225,14,0,0,58,68,0,155,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,72,0,139,229,1,0,0,234,0,0,160,227,72,0,139,229,72,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,24,0,139,229,1,0,0,234,20,0,155,229,24,0,139,229,24,0,155,229
	.byte 16,0,139,229,28,0,139,229,0,16,160,225,4,16,139,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,40,192,155,229,12,240,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . -12
	.byte 0,0,159,231,153,16,160,227
bl _p_1

	.byte 0,16,154,229,12,16,145,229
bl _p_23

	.byte 0,16,160,225,6,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_3

	.byte 80,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_19:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XContainer_Add_object_0
_System_Xml_Linq_XContainer_Add_object_0:

	.byte 128,64,45,233,13,112,160,225,112,9,45,233,36,208,77,226,13,176,160,225,0,96,160,225,24,16,139,229,0,0,160,227
	.byte 8,0,139,229,24,0,155,229,0,0,80,227,80,0,0,10,24,0,155,229
bl _p_25

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 100
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,8,0,139,229,34,0,0,234,8,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 104
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,44,48,150,229,6,0,160,225,5,16,160,225
	.byte 0,32,160,227,0,192,160,227,0,192,141,229,0,192,150,229,15,224,160,225,68,240,156,229,255,0,0,226,0,0,80,227
	.byte 11,0,0,26,5,0,160,225
bl _p_26

	.byte 0,64,160,225,6,0,160,225,4,16,160,225
bl _p_27

	.byte 6,0,160,225,4,16,160,225
bl _p_28

	.byte 6,0,160,225,4,16,160,225
bl _p_29

	.byte 8,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,207,255,255,26,0,0,0,235
	.byte 17,0,0,234,8,208,77,226,20,224,139,229,8,0,155,229,0,0,80,227,9,0,0,10,8,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,8,208,141,226,20,192,155,229,12,240,160,225,36,208,139,226
	.byte 112,9,189,232,128,128,189,232

Lme_1a:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XDocument_get_DocumentType_0
_System_Xml_Linq_XDocument_get_DocumentType_0:

	.byte 128,64,45,233,13,112,160,225,80,13,45,233,28,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,139,229
	.byte 10,0,160,225
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,46,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,96,160,225,6,160,160,225,10,64,160,225,0,0,90,227
	.byte 11,0,0,10,0,0,154,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,64,160,227,255,255,255,234,0,0,84,227,17,0,0,10,20,96,139,229
	.byte 0,0,86,227,10,0,0,10,20,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,40,0,0,27,20,0,155,229,4,0,139,229,15,0,0,235,32,0,0,234,0,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,195,255,255,26,0,0,0,235
	.byte 15,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,0,0,160,227,0,0,0,234
	.byte 4,0,155,229,28,208,139,226,80,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_30:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XDocument_get_Root_0
_System_Xml_Linq_XDocument_get_Root_0:

	.byte 128,64,45,233,13,112,160,225,80,13,45,233,28,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,139,229
	.byte 10,0,160,225
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,46,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,96,160,225,6,160,160,225,10,64,160,225,0,0,90,227
	.byte 11,0,0,10,0,0,154,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,64,160,227,255,255,255,234,0,0,84,227,17,0,0,10,20,96,139,229
	.byte 0,0,86,227,10,0,0,10,20,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,40,0,0,27,20,0,155,229,4,0,139,229,15,0,0,235,32,0,0,234,0,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,195,255,255,26,0,0,0,235
	.byte 15,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,0,0,160,227,0,0,0,234
	.byte 4,0,155,229,28,208,139,226,80,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_32:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0
_System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,20,0,139,229,24,16,139,229,0,0,160,227
	.byte 0,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 152
	.byte 0,0,159,231
bl _p_15

	.byte 32,0,139,229
bl _p_40

	.byte 32,0,155,229,0,16,160,225,1,32,160,225,0,224,210,229,0,32,160,227,44,32,193,229,24,32,155,229,1,32,2,226
	.byte 0,0,82,227,0,32,160,19,1,32,160,3,0,224,209,229,34,32,192,229,20,0,155,229
bl _p_41

	.byte 0,0,139,229,0,0,155,229,24,16,155,229
bl _p_42

	.byte 4,0,139,229,0,0,0,235,15,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,4,0,155,229,40,208,139,226
	.byte 0,9,189,232,128,128,189,232

Lme_34:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0
_System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0:

	.byte 128,64,45,233,13,112,160,225,96,13,45,233,20,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,48,0,150,229,0,0,80,227,20,0,0,10,48,0,150,229,0,16,160,225,0,224,209,229,12,0,144,229
	.byte 0,0,80,227,14,0,0,10,48,0,150,229,0,16,160,225,0,224,209,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 176
	.byte 1,16,159,231
bl _p_8

	.byte 255,16,0,226,10,0,160,225,0,32,154,229,15,224,160,225,64,240,146,229,3,0,0,234,10,0,160,225,0,16,154,229
	.byte 15,224,160,225,68,240,145,229,6,0,160,225
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,16,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,5,32,160,225,2,0,160,225,10,16,160,225
	.byte 0,32,146,229,15,224,160,225,64,240,146,229,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,225,255,255,26,0,0,0,235
	.byte 15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,20,208,139,226,96,13,189,232
	.byte 128,128,189,232

Lme_38:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0
_System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0:

	.byte 128,64,45,233,13,112,160,225,112,9,45,233,44,208,77,226,13,176,160,225,16,0,139,229,20,16,139,229,24,32,139,229
	.byte 0,0,160,227,0,0,139,229,20,0,155,229,8,0,144,229,0,0,80,227,15,0,0,218,20,0,155,229
bl _p_65

	.byte 0,16,160,225,16,0,155,229
bl _p_66

	.byte 0,80,160,225,0,0,80,227,6,0,0,26,24,0,155,229,20,16,155,229,24,32,155,229,0,32,146,229,15,224,160,225
	.byte 120,240,146,229,0,80,160,225,4,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 200
	.byte 0,0,159,231,0,80,144,229,5,64,160,225,16,0,155,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,44,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,96,160,225,6,16,160,225,1,0,160,225,0,224,209,229
bl _p_63

	.byte 255,0,0,226,0,0,80,227,26,0,0,10,6,0,160,225,0,224,214,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value

	.byte 20,16,155,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,18,0,0,10,0,224,214,229,32,0,150,229,0,16,160,225,0,224,209,229,12,0,144,229
	.byte 32,0,139,229
bl _p_6

	.byte 0,16,160,225,32,0,155,229
bl _p_7

	.byte 255,0,0,226,0,0,80,227,4,0,0,10,0,224,214,229,32,0,150,229,0,16,160,225,0,224,209,229,8,64,144,229
	.byte 12,0,0,234,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,197,255,255,26,0,0,0,235
	.byte 15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,4,0,160,225,44,208,139,226
	.byte 112,9,189,232,128,128,189,232

Lme_53:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0
_System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,64,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 12,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227,24,0,139,229,48,16,150,229,1,0,160,225,0,224,209,229
bl _System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName

	.byte 0,16,160,225,6,0,160,225,10,32,160,225
bl _p_70

	.byte 8,0,139,229,0,0,160,227,12,0,139,229,8,0,155,229,0,0,80,227,8,0,0,26,12,0,139,226,48,0,139,229
	.byte 6,0,160,225
bl _p_52

	.byte 0,16,160,225,48,0,155,229,0,32,160,227
bl _p_72

	.byte 8,0,139,229,48,0,150,229,0,16,160,225,0,224,209,229,8,0,144,229,48,0,139,229,48,0,150,229,0,16,160,225
	.byte 0,224,209,229,12,16,144,229,1,0,160,225,0,224,209,229
bl _p_56

	.byte 0,48,160,225,48,32,155,229,10,0,160,225,8,16,155,229,0,192,154,229,15,224,160,225,60,240,156,229,6,0,160,225
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,16,0,139,229,119,0,0,234,16,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,5,16,160,225,1,0,160,225,0,224,209,229
bl _p_63

	.byte 255,0,0,226,0,0,80,227,54,0,0,10,0,224,213,229,32,0,149,229,0,16,160,225,0,224,209,229,12,0,144,229
	.byte 48,0,139,229
bl _p_6

	.byte 0,16,160,225,48,0,155,229
bl _p_7

	.byte 255,0,0,226,0,0,80,227,27,0,0,10,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 4
	.byte 0,0,159,231,48,0,139,229,0,224,213,229,32,0,149,229,0,16,160,225,0,224,209,229,8,0,144,229,52,0,139,229
bl _p_6

	.byte 0,16,160,225,0,224,209,229
bl _p_56

	.byte 56,0,139,229,5,0,160,225,0,224,213,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value

	.byte 0,192,160,225,48,16,155,229,52,32,155,229,56,48,155,229,10,0,160,225,0,192,141,229,0,224,218,229
bl _p_71

	.byte 12,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 4
	.byte 0,0,159,231,48,0,139,229,5,0,160,225,0,224,213,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value

	.byte 0,32,160,225,48,16,155,229,10,0,160,225,0,224,218,229
bl _p_73

	.byte 46,0,0,234,0,224,213,229,32,16,149,229,1,0,160,225,0,224,209,229
bl _System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName

	.byte 0,16,160,225,6,0,160,225,10,32,160,225
bl _p_70

	.byte 0,64,160,225,0,0,80,227,8,0,0,26,12,0,139,226,48,0,139,229,6,0,160,225
bl _p_52

	.byte 0,16,160,225,48,0,155,229,1,32,160,227
bl _p_72

	.byte 0,64,160,225,0,224,213,229,32,0,149,229,0,16,160,225,0,224,209,229,8,0,144,229,48,0,139,229,0,224,213,229
	.byte 32,0,149,229,0,16,160,225,0,224,209,229,12,16,144,229,1,0,160,225,0,224,209,229
bl _p_56

	.byte 52,0,139,229,5,0,160,225,0,224,213,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value

	.byte 0,192,160,225,48,32,155,229,52,48,155,229,10,0,160,225,4,16,160,225,0,192,141,229,0,224,218,229
bl _p_71

	.byte 16,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,122,255,255,26,0,0,0,235
	.byte 17,0,0,234,8,208,77,226,36,224,139,229,16,0,155,229,0,0,80,227,9,0,0,10,16,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,8,208,141,226,36,192,155,229,12,240,160,225,6,0,160,225
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,24,0,139,229,15,0,0,234,24,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,20,0,139,229,0,32,160,225,10,16,160,225,0,32,146,229
	.byte 15,224,160,225,64,240,146,229,24,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,226,255,255,26,0,0,0,235
	.byte 17,0,0,234,8,208,77,226,44,224,139,229,24,0,155,229,0,0,80,227,9,0,0,10,24,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,8,208,141,226,44,192,155,229,12,240,160,225,60,0,214,229
	.byte 0,0,80,227,4,0,0,10,10,0,160,225,0,16,154,229,15,224,160,225,100,240,145,229,3,0,0,234,10,0,160,225
	.byte 0,16,154,229,15,224,160,225,92,240,145,229,64,208,139,226,112,13,189,232,128,128,189,232

Lme_55:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0
_System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,16,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,112,0,0,234,6,0,160,225,0,224,214,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,50,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,5,16,160,225,1,0,160,225,0,224,209,229
bl _p_63

	.byte 255,0,0,226,0,0,80,227,32,0,0,10,8,0,154,229,0,0,80,227,12,0,0,26,0,224,213,229,32,0,149,229
	.byte 0,16,160,225,0,224,209,229,8,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 4
	.byte 1,16,159,231
bl _p_8

	.byte 255,0,0,226,0,0,80,227,9,0,0,26,0,224,213,229,32,0,149,229,0,16,160,225,0,224,209,229,8,0,144,229
	.byte 10,16,160,225
bl _p_8

	.byte 255,0,0,226,0,0,80,227,6,0,0,10,5,0,160,225,0,224,213,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
bl _p_74

	.byte 0,64,160,225,15,0,0,235,51,0,0,234,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,191,255,255,26,0,0,0,235
	.byte 15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,0,224,214,229,8,80,150,229
	.byte 5,64,160,225,0,0,85,227,10,0,0,10,0,0,149,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,4,96,160,225,0,0,86,227,140,255,255,26
bl _p_9

	.byte 0,0,0,234,4,0,160,225,16,208,139,226,112,13,189,232,128,128,189,232

Lme_56:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0
_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0:

	.byte 128,64,45,233,13,112,160,225,96,13,45,233,28,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,6,0,160,225,10,16,160,225
bl _p_75

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 256
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,21,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 260
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,6,0,160,225,5,16,160,225
bl _p_76

	.byte 10,16,160,225
bl _p_7

	.byte 255,0,0,226,0,0,80,227,2,0,0,10,4,80,139,229,15,0,0,235,32,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,220,255,255,26,0,0,0,235
	.byte 15,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,0,0,160,227,0,0,0,234
	.byte 4,0,155,229,28,208,139,226,96,13,189,232,128,128,189,232

Lme_57:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0
_System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0:

	.byte 128,64,45,233,13,112,160,225,80,13,45,233,108,208,77,226,13,176,160,225,84,0,139,229,1,96,160,225,88,32,203,229
	.byte 92,48,139,229,136,224,157,229,96,224,139,229,0,0,160,227,0,0,139,229,16,96,139,229,20,96,139,229,0,0,86,227
	.byte 12,0,0,10,16,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 272
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,20,0,139,229,20,0,155,229,0,0,80,227,2,1,0,26
	.byte 24,96,139,229,28,96,139,229,0,0,86,227,12,0,0,10,24,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229
	.byte 12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,28,0,139,229,28,0,155,229,0,0,80,227,238,0,0,26
	.byte 32,96,139,229,36,96,139,229,0,0,86,227,12,0,0,10,32,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229
	.byte 4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 268
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,36,0,139,229,36,0,155,229,0,0,80,227,218,0,0,26
	.byte 88,0,219,229,0,0,80,227,16,0,0,10,6,160,160,225,6,64,160,225,0,0,86,227,10,0,0,10,0,0,154,229
	.byte 0,0,144,229,8,0,144,229,8,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 196
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,0,0,84,227,198,0,0,26,40,96,139,229,44,96,139,229
	.byte 0,0,86,227,12,0,0,10,40,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,8,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 196
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,44,0,139,229,44,64,155,229,4,0,160,225,0,0,80,227
	.byte 81,0,0,10,84,0,155,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,32,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,160,160,225,0,224,212,229,32,0,148,229,0,224,218,229
	.byte 32,16,154,229
bl _System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName

	.byte 255,0,0,226,0,0,80,227,13,0,0,10,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . -12
	.byte 0,0,159,231,183,20,0,227
bl _p_1

	.byte 0,224,212,229,32,16,148,229
bl _p_77

	.byte 0,16,160,225,94,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_3

	.byte 0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,209,255,255,26,0,0,0,235
	.byte 15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,84,0,155,229,4,16,160,225
bl _p_61

	.byte 1,0,160,227,92,0,0,234,48,96,139,229,52,96,139,229,0,0,86,227,12,0,0,10,48,0,155,229,0,0,144,229
	.byte 0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 96
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,52,0,139,229,52,0,155,229,0,0,80,227,71,0,0,10
	.byte 92,0,155,229,56,0,139,229,92,0,155,229,60,0,139,229,92,0,155,229,0,0,80,227,12,0,0,10,56,0,155,229
	.byte 0,0,144,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 180
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,60,0,139,229,60,0,155,229,0,0,80,227,48,0,0,10
	.byte 92,0,155,229,64,0,139,229,92,0,155,229,0,0,80,227,10,0,0,10,64,0,155,229,0,0,144,229,0,0,144,229
	.byte 8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 180
	.byte 1,16,159,231,1,0,80,225,50,0,0,27,64,0,155,229,80,0,139,229,64,0,155,229,0,224,208,229,40,0,144,229
	.byte 76,0,139,229,68,96,139,229,72,96,139,229,0,0,86,227,12,0,0,10,68,0,155,229,0,0,144,229,0,0,144,229
	.byte 8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 96
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,72,0,139,229,76,0,155,229,72,16,155,229
bl _p_78

	.byte 0,16,160,225,80,0,155,229,0,32,160,225,0,224,210,229
bl _System_Xml_Linq__System_Xml_Linq_XText_set_Value_string

	.byte 1,0,160,227,0,0,0,234,0,0,160,227,108,208,139,226,80,13,189,232,128,128,189,232,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . -12
	.byte 0,0,159,231,87,20,0,227
bl _p_1

	.byte 0,16,150,229,12,16,145,229
bl _p_77

	.byte 0,16,160,225,6,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_3

	.byte 14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_59:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0
_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,32,208,77,226,13,176,160,225,16,0,139,229,0,0,160,227,0,0,203,229
	.byte 16,0,155,229,36,160,144,229,16,0,155,229,0,16,224,227,36,16,128,229,0,0,160,227,0,0,203,229,10,96,160,225
	.byte 2,0,90,227,198,0,0,42,6,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 532
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,16,0,155,229,0,16,160,225,24,16,145,229,8,16,128,229
	.byte 178,0,0,234,16,0,155,229,24,0,139,229,16,0,155,229,8,16,144,229,1,0,160,225,0,224,209,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,16,160,225,24,0,155,229,12,16,128,229,2,160,224,227
	.byte 1,160,74,226,1,0,90,227,7,0,0,42,10,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 528
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,86,0,0,234,16,0,155,229,24,0,139,229,16,0,155,229
	.byte 12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,16,160,225,24,0,155,229,16,16,128,229,16,0,155,229
	.byte 16,16,144,229,1,0,160,225,0,224,209,229
bl _p_63

	.byte 255,0,0,226,0,0,80,227,62,0,0,10,16,0,155,229,16,16,144,229,1,0,160,225,0,224,209,229
bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value

	.byte 24,0,139,229,16,0,155,229,20,16,144,229,1,0,160,225,0,224,209,229
bl _p_56

	.byte 0,16,160,225,24,0,155,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,45,0,0,10,16,0,155,229,24,0,139,229,16,0,155,229,16,0,144,229,0,16,160,225
	.byte 0,224,209,229,32,0,144,229,0,16,160,225,0,224,209,229,12,0,144,229,28,0,139,229
bl _p_9

	.byte 0,16,160,225,28,0,155,229
bl _p_7

	.byte 24,16,155,229,255,0,0,226,1,96,160,225,0,0,80,227,5,0,0,10,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 200
	.byte 0,0,159,231,0,80,144,229,7,0,0,234,16,0,155,229,16,0,144,229,0,16,160,225,0,224,209,229,32,0,144,229
	.byte 0,16,160,225,0,224,209,229,8,80,144,229,28,80,134,229,16,0,155,229,32,0,208,229,0,0,80,227,2,0,0,26
	.byte 16,0,155,229,1,16,160,227,36,16,128,229,1,0,160,227,0,0,203,229,16,0,0,235,68,0,0,234,16,0,155,229
	.byte 12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,154,255,255,26,0,0,0,235
	.byte 22,0,0,234,12,224,139,229,0,0,219,229,0,0,80,227,1,0,0,10,12,192,155,229,12,240,160,225,16,0,155,229
	.byte 12,0,144,229,0,0,80,227,10,0,0,10,16,0,155,229,12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,16,96,155,229,6,0,160,225
	.byte 8,0,144,229,0,16,160,225,0,224,209,229,8,80,144,229,5,64,160,225,0,0,85,227,10,0,0,10,0,0,149,229
	.byte 0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,8,64,134,229,16,0,155,229,8,0,144,229,0,0,80,227
	.byte 72,255,255,26,16,0,155,229,0,16,224,227,36,16,128,229,0,0,160,227,0,0,0,234,1,0,160,227,32,208,139,226
	.byte 112,13,189,232,128,128,189,232

Lme_6c:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0
_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,16,0,139,229,16,0,155,229,36,0,144,229
	.byte 16,16,155,229,1,32,160,227,32,32,193,229,16,16,155,229,0,32,224,227,36,32,129,229,12,0,139,229,2,0,80,227
	.byte 28,0,0,42,12,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 536
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,0,0,0,235,17,0,0,234,8,224,139,229,16,0,155,229
	.byte 12,0,144,229,0,0,80,227,10,0,0,10,16,0,155,229,12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,8,192,155,229,12,240,160,225,24,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_6d:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNamespace_Get_string_0
_System_Xml_Linq_XNamespace_Get_string_0:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,44,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,139,229
	.byte 0,0,160,227,4,0,203,229,0,0,160,227,8,0,139,229
bl _p_88

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 304
	.byte 0,0,159,231,0,0,144,229,0,0,139,229,0,0,160,227,4,0,203,229,0,0,155,229,4,16,139,226
bl _p_89
bl _p_88

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 304
	.byte 0,0,159,231,0,48,144,229,8,32,139,226,3,0,160,225,10,16,160,225,0,224,211,229
bl _p_90

	.byte 255,0,0,226,0,0,80,227,20,0,0,26
bl _p_88

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 328
	.byte 0,0,159,231
bl _p_15

	.byte 32,0,139,229,10,16,160,225
bl _p_91

	.byte 32,0,155,229,8,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 304
	.byte 0,0,159,231,0,48,144,229,8,32,155,229,3,0,160,225,10,16,160,225,0,224,211,229
bl _p_92

	.byte 8,0,155,229,12,0,139,229,0,0,0,235,7,0,0,234,24,224,139,229,4,0,219,229,0,0,80,227,1,0,0,10
	.byte 0,0,155,229
bl _p_145

	.byte 24,192,155,229,12,240,160,225,12,0,155,229,44,208,139,226,0,13,189,232,128,128,189,232

Lme_81:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNamespace_GetName_string_0
_System_Xml_Linq_XNamespace_GetName_string_0:

	.byte 128,64,45,233,13,112,160,225,64,13,45,233,40,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,4,0,203,229,0,0,160,227,8,0,139,229,12,0,150,229,0,0,80,227,8,0,0,26
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 336
	.byte 0,0,159,231
bl _p_15

	.byte 32,0,139,229
bl _p_96

	.byte 32,0,155,229,12,0,134,229,12,0,150,229,0,0,139,229,0,0,160,227,4,0,203,229,0,0,155,229,4,16,139,226
bl _p_89

	.byte 12,48,150,229,8,32,139,226,3,0,160,225,10,16,160,225,0,224,211,229
bl _p_93

	.byte 255,0,0,226,0,0,80,227,16,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 332
	.byte 0,0,159,231
bl _p_15

	.byte 32,0,139,229,10,16,160,225,6,32,160,225
bl _System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace

	.byte 32,0,155,229,8,0,139,229,12,48,150,229,8,32,155,229,3,0,160,225,10,16,160,225,0,224,211,229
bl _p_95

	.byte 8,0,155,229,12,0,139,229,0,0,0,235,7,0,0,234,24,224,139,229,4,0,219,229,0,0,80,227,1,0,0,10
	.byte 0,0,155,229
bl _p_145

	.byte 24,192,155,229,12,240,160,225,12,0,155,229,40,208,139,226,64,13,189,232,128,128,189,232

Lme_82:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0
_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,144,208,77,226,13,176,160,225,120,0,139,229,124,16,139,229,128,32,139,229
	.byte 0,0,160,227,16,0,139,229,0,0,160,227,36,0,139,229,0,0,160,227,44,0,139,229,124,0,155,229,0,0,80,227
	.byte 4,0,0,26,128,0,155,229,0,0,80,227,0,0,160,19,1,0,160,3,206,2,0,234,128,0,155,229,0,0,80,227
	.byte 1,0,0,26,0,0,160,227,201,2,0,234,124,0,155,229,0,16,160,225,0,16,145,229,15,224,160,225,60,240,145,229
	.byte 136,0,139,229,128,0,155,229,0,16,160,225,0,16,145,229,15,224,160,225,60,240,145,229,0,16,160,225,136,0,155,229
	.byte 1,0,80,225,1,0,0,10,0,0,160,227,184,2,0,234,124,0,155,229,0,16,160,225,0,16,145,229,15,224,160,225
	.byte 60,240,145,229,0,0,139,229,7,0,64,226,100,0,139,229,4,0,80,227,8,0,0,42,100,0,155,229,0,17,160,225
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 544
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,0,0,155,229,1,0,64,226,104,0,139,229,3,0,80,227
	.byte 148,2,0,42,104,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 540
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,124,96,155,229,6,0,160,225,0,0,80,227,9,0,0,10
	.byte 0,0,150,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 272
	.byte 1,16,159,231,1,0,80,225,140,2,0,27,4,96,139,229,128,80,155,229,5,0,160,225,0,0,80,227,9,0,0,10
	.byte 0,0,149,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 272
	.byte 1,16,159,231,1,0,80,225,125,2,0,27,8,80,139,229,4,0,155,229,0,224,208,229,48,16,144,229,0,224,213,229
	.byte 48,32,149,229,120,0,155,229
bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration

	.byte 255,0,0,226,0,0,80,227,1,0,0,26,0,0,160,227,109,2,0,234,8,0,155,229,0,16,160,225,0,224,209,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,160,160,225,4,0,155,229,0,16,160,225,0,224,209,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,16,0,139,229,46,0,0,234,16,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,12,0,139,229,10,0,160,225,0,16,154,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227
	.byte 20,0,203,229,35,0,0,235,52,2,0,234,10,0,160,225,0,16,154,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,32,160,225,120,0,155,229,12,16,155,229
bl _p_113

	.byte 255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227,20,0,203,229,15,0,0,235,32,2,0,234,16,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,195,255,255,26,0,0,0,235
	.byte 15,0,0,234,80,224,139,229,16,0,155,229,0,0,80,227,9,0,0,10,16,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,80,192,155,229,12,240,160,225,10,0,160,225,0,16,154,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,0,0,160,19,1,0,160,3
	.byte 244,1,0,234,124,0,155,229,108,0,139,229,124,0,155,229,0,0,80,227,10,0,0,10,108,0,155,229,0,0,144,229
	.byte 0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,231,1,0,27,108,0,155,229,24,0,139,229,128,0,155,229,112,0,139,229,128,0,155,229
	.byte 0,0,80,227,10,0,0,10,112,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,213,1,0,27,112,0,155,229,28,0,139,229,24,0,155,229,0,224,208,229,48,0,144,229
	.byte 112,16,155,229,0,224,209,229,48,16,145,229
bl _System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName

	.byte 255,0,0,226,0,0,80,227,1,0,0,10,0,0,160,227,196,1,0,234,28,0,155,229,0,16,160,225,0,224,209,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,96,160,225,24,0,155,229,0,16,160,225,0,224,209,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,36,0,139,229,46,0,0,234,36,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,32,0,139,229,6,0,160,225,0,16,150,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227
	.byte 20,0,203,229,35,0,0,235,139,1,0,234,6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,32,160,225,120,0,155,229,32,16,155,229
bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute

	.byte 255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227,20,0,203,229,15,0,0,235,119,1,0,234,36,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,195,255,255,26,0,0,0,235
	.byte 15,0,0,234,88,224,139,229,36,0,155,229,0,0,80,227,9,0,0,10,36,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,88,192,155,229,12,240,160,225,6,0,160,225,0,16,150,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,1,0,0,10,0,0,160,227
	.byte 75,1,0,234,28,0,155,229,0,16,160,225,0,224,209,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,80,160,225,24,0,155,229,0,16,160,225,0,224,209,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,44,0,139,229,46,0,0,234,44,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,40,0,139,229,5,0,160,225,0,16,149,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227
	.byte 20,0,203,229,35,0,0,235,18,1,0,234,5,0,160,225,0,16,149,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,32,160,225,120,0,155,229,40,16,155,229
bl _p_113

	.byte 255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227,20,0,203,229,15,0,0,235,254,0,0,234,44,16,155,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,195,255,255,26,0,0,0,235
	.byte 15,0,0,234,96,224,139,229,44,0,155,229,0,0,80,227,9,0,0,10,44,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,96,192,155,229,12,240,160,225,5,0,160,225,0,16,149,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,0,0,160,19,1,0,160,3
	.byte 210,0,0,234,124,96,155,229,6,0,160,225,0,0,80,227,9,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229
	.byte 12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 408
	.byte 1,16,159,231,1,0,80,225,199,0,0,27,48,96,139,229,128,80,155,229,5,0,160,225,0,0,80,227,9,0,0,10
	.byte 0,0,149,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 408
	.byte 1,16,159,231,1,0,80,225,184,0,0,27,52,80,139,229,48,0,155,229,0,224,208,229,40,0,144,229,0,224,213,229
	.byte 40,16,149,229
bl _p_8

	.byte 255,0,0,226,172,0,0,234,124,96,155,229,6,0,160,225,0,0,80,227,9,0,0,10,0,0,150,229,0,0,144,229
	.byte 8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 404
	.byte 1,16,159,231,1,0,80,225,161,0,0,27,56,96,139,229,128,80,155,229,5,0,160,225,0,0,80,227,9,0,0,10
	.byte 0,0,149,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 404
	.byte 1,16,159,231,1,0,80,225,146,0,0,27,60,80,139,229,56,0,155,229,0,224,208,229,40,0,144,229,0,224,213,229
	.byte 40,16,149,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,9,0,0,10,56,0,155,229,0,224,208,229,44,0,144,229,60,16,155,229,0,224,209,229
	.byte 44,16,145,229
bl _p_8

	.byte 255,0,0,226,68,0,139,229,1,0,0,234,0,0,160,227,68,0,139,229,68,0,155,229,119,0,0,234,124,96,155,229
	.byte 6,0,160,225,0,0,80,227,9,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,108,0,0,27,6,64,160,225,128,80,155,229,5,0,160,225,0,0,80,227,9,0,0,10
	.byte 0,0,149,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,93,0,0,27,64,80,139,229,0,224,212,229,40,0,148,229,0,224,213,229,40,16,149,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,26,0,0,10,0,224,212,229,44,0,148,229,64,16,155,229,0,224,209,229,44,16,145,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,17,0,0,10,0,224,212,229,48,0,148,229,64,16,155,229,0,224,209,229,48,16,145,229
bl _p_8

	.byte 255,0,0,226,0,0,80,227,8,0,0,10,0,224,212,229,52,0,148,229,64,16,155,229,0,224,209,229,52,16,145,229
bl _p_8

	.byte 255,0,0,226,68,0,139,229,1,0,0,234,0,0,160,227,68,0,139,229,68,0,155,229,50,0,0,234,124,96,155,229
	.byte 6,0,160,225,0,0,80,227,9,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 180
	.byte 1,16,159,231,1,0,80,225,39,0,0,27,0,224,214,229,40,80,150,229,128,0,155,229,116,0,139,229,128,0,155,229
	.byte 0,0,80,227,10,0,0,10,116,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 180
	.byte 1,16,159,231,1,0,80,225,21,0,0,27,116,0,155,229,0,224,208,229,40,16,144,229,5,0,160,225
bl _p_8

	.byte 255,0,0,226,11,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . -12
	.byte 0,0,159,231,237,21,0,227
bl _p_1

	.byte 0,16,160,225,64,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_3

	.byte 20,0,219,229,144,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_99:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0
_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,96,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 8,0,139,229,0,0,160,227,20,0,139,229,0,0,160,227,28,0,139,229,0,0,90,227,1,0,0,26,0,0,160,227
	.byte 140,1,0,234,10,0,160,225,0,16,154,229,15,224,160,225,60,240,145,229,0,83,160,225,10,0,160,225,0,16,154,229
	.byte 15,224,160,225,60,240,145,229,0,64,160,225,7,0,64,226,68,0,139,229,4,0,80,227,8,0,0,42,68,0,155,229
	.byte 0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 552
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,1,64,68,226,3,0,84,227,113,1,0,42,4,17,160,225
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 548
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,10,64,160,225,0,0,90,227,9,0,0,10,0,0,148,229
	.byte 0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 272
	.byte 1,16,159,231,1,0,80,225,96,1,0,27,0,64,139,229,0,224,212,229,48,16,148,229,6,0,160,225
bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration

	.byte 0,80,37,224,4,0,160,225,0,224,212,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,8,0,139,229,16,0,0,234,8,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,4,0,139,229,0,16,160,225,0,16,145,229,15,224,160,225
	.byte 36,240,145,229,128,2,160,225,0,80,37,224,8,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,225,255,255,26,0,0,0,235
	.byte 40,1,0,234,48,224,139,229,8,0,155,229,0,0,80,227,9,0,0,10,8,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,48,192,155,229,12,240,160,225,10,64,160,225,0,0,90,227
	.byte 9,0,0,10,0,0,148,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,15,1,0,27,12,64,139,229,0,224,212,229,48,16,148,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,36,240,145,229,128,1,160,225,0,80,37,224,4,0,160,225,0,224,212,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,20,0,139,229,16,0,0,234,20,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,16,0,139,229,0,16,160,225,0,16,145,229,15,224,160,225
	.byte 36,240,145,229,128,3,160,225,0,80,37,224,20,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,225,255,255,26,0,0,0,235
	.byte 15,0,0,234,56,224,139,229,20,0,155,229,0,0,80,227,9,0,0,10,20,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,56,192,155,229,12,240,160,225,12,0,155,229,0,16,160,225
	.byte 0,224,209,229
bl _p_37

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 136
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,28,0,139,229,16,0,0,234,28,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 140
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,24,0,139,229,0,16,160,225,0,16,145,229,15,224,160,225
	.byte 36,240,145,229,0,3,160,225,0,80,37,224,28,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,225,255,255,26,0,0,0,235
	.byte 149,0,0,234,64,224,139,229,28,0,155,229,0,0,80,227,9,0,0,10,28,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,64,192,155,229,12,240,160,225,5,64,160,225,72,160,139,229
	.byte 0,0,90,227,10,0,0,10,72,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 408
	.byte 1,16,159,231,1,0,80,225,122,0,0,27,72,0,155,229,0,224,208,229,40,16,144,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,36,240,145,229,0,80,36,224,109,0,0,234,10,64,160,225,0,0,90,227,9,0,0,10,0,0,148,229
	.byte 0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 404
	.byte 1,16,159,231,1,0,80,225,100,0,0,27,32,64,139,229,0,224,212,229,40,16,148,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,36,240,145,229,0,3,160,225,80,0,139,229,0,224,212,229,44,16,148,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,36,240,145,229,0,16,160,225,80,0,155,229,1,0,128,224,0,80,37,224,76,0,0,234,10,96,160,225
	.byte 0,0,90,227,9,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 144
	.byte 1,16,159,231,1,0,80,225,67,0,0,27,36,96,139,229,0,224,214,229,40,16,150,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,36,240,145,229,128,3,160,225,0,0,37,224,88,0,139,229,0,224,214,229,44,16,150,229,1,0,160,225
	.byte 0,16,145,229,15,224,160,225,36,240,145,229,0,16,160,225,88,0,155,229,1,19,160,225,1,0,32,224,84,0,139,229
	.byte 0,224,214,229,48,16,150,229,1,0,160,225,0,16,145,229,15,224,160,225,36,240,145,229,0,16,160,225,84,0,155,229
	.byte 129,18,160,225,1,0,32,224,80,0,139,229,0,224,214,229,52,16,150,229,1,0,160,225,0,16,145,229,15,224,160,225
	.byte 36,240,145,229,0,16,160,225,80,0,155,229,1,18,160,225,1,80,32,224,20,0,0,234,5,64,160,225,76,160,139,229
	.byte 0,0,90,227,10,0,0,10,76,0,155,229,0,0,144,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 180
	.byte 1,16,159,231,1,0,80,225,9,0,0,27,76,0,155,229,0,16,160,225,0,16,145,229,15,224,160,225,36,240,145,229
	.byte 0,80,36,224,5,0,160,225,96,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_9e:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNodeReader_GetXAttribute_int_0
_System_Xml_Linq_XNodeReader_GetXAttribute_int_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,32,208,77,226,13,176,160,225,0,96,160,225,28,16,139,229,0,0,160,227
	.byte 0,0,139,229,6,0,160,225,0,16,150,229,15,224,160,225,220,240,145,229,255,0,0,226,0,0,80,227,1,0,0,10
	.byte 0,0,160,227,91,0,0,234,24,0,150,229,20,0,139,229,24,0,139,229,0,0,80,227,12,0,0,10,20,0,155,229
	.byte 0,0,144,229,0,0,144,229,8,0,144,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 8
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,24,0,139,229,24,80,155,229,5,0,160,225,0,0,80,227
	.byte 1,0,0,26,0,0,160,227,67,0,0,234,0,64,160,227,5,0,160,225,0,224,213,229
bl _p_52

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 204
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,0,139,229,18,0,0,234,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 208
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,160,160,225,4,0,160,225,1,64,132,226,28,16,155,229
	.byte 1,0,80,225,2,0,0,26,4,160,139,229,15,0,0,235,32,0,0,234,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,223,255,255,26,0,0,0,235
	.byte 15,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,0,0,160,227,0,0,0,234
	.byte 4,0,155,229,32,208,139,226,112,13,189,232,128,128,189,232

Lme_b2:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNodeReader_GetAttribute_string_0
_System_Xml_Linq_XNodeReader_GetAttribute_string_0:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,36,208,77,226,13,176,160,225,24,0,139,229,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,4,0,203,229,24,0,155,229,40,0,144,229,0,0,139,229,24,0,155,229,44,0,208,229
	.byte 4,0,203,229,24,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,108,240,145,229,24,32,155,229,2,0,160,225
	.byte 10,16,160,225,0,32,146,229,15,224,160,225,120,240,146,229,255,0,0,226,0,0,80,227,6,0,0,10,24,16,155,229
	.byte 1,0,160,225,0,16,145,229,15,224,160,225,160,240,145,229,0,160,160,225,0,0,0,234,0,160,160,227,8,160,139,229
	.byte 0,0,0,235,8,0,0,234,20,224,139,229,24,0,155,229,0,16,155,229,40,16,128,229,24,0,155,229,4,16,219,229
	.byte 44,16,192,229,20,192,155,229,12,240,160,225,8,0,155,229,36,208,139,226,0,13,189,232,128,128,189,232

Lme_bd:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0
_System_Xml_Linq_XNodeReader_GetAttribute_string_string_0:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,36,208,77,226,13,176,160,225,24,0,139,229,28,16,139,229,2,160,160,225
	.byte 0,0,160,227,0,0,139,229,0,0,160,227,4,0,203,229,24,0,155,229,40,0,144,229,0,0,139,229,24,0,155,229
	.byte 44,0,208,229,4,0,203,229,24,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,108,240,145,229,24,48,155,229
	.byte 3,0,160,225,28,16,155,229,10,32,160,225,0,48,147,229,15,224,160,225,116,240,147,229,255,0,0,226,0,0,80,227
	.byte 6,0,0,10,24,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,160,240,145,229,0,160,160,225,0,0,0,234
	.byte 0,160,160,227,8,160,139,229,0,0,0,235,8,0,0,234,20,224,139,229,24,0,155,229,0,16,155,229,40,16,128,229
	.byte 24,0,155,229,4,16,219,229,44,16,192,229,20,192,155,229,12,240,160,225,8,0,155,229,36,208,139,226,0,13,189,232
	.byte 128,128,189,232

Lme_be:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XUtil_ToString_object
_System_Xml_Linq_XUtil_ToString_object:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,64,208,77,226,0,160,160,225,0,0,160,227,0,0,141,229,0,0,160,227
	.byte 4,0,141,229,0,0,160,227,8,0,141,229,0,0,160,227,12,0,141,229,0,11,159,237,1,0,0,234,0,0,0,0
	.byte 0,0,0,0,4,11,141,237,0,11,159,237,1,0,0,234,0,0,0,0,0,0,0,0,192,235,183,238,6,234,141,237
	.byte 0,0,90,227,234,0,0,10,0,0,154,229,12,0,144,229
bl _p_153

	.byte 0,96,160,225,13,80,64,226,6,0,85,227,7,0,0,42,5,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 588
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,3,0,86,227,120,0,0,10,127,0,0,234,10,96,160,225
	.byte 0,0,90,227,9,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 96
	.byte 1,16,159,231,1,0,80,225,214,0,0,27,6,0,160,225,198,0,0,234,0,0,154,229,22,16,208,229,0,0,81,227
	.byte 208,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 584
	.byte 1,16,159,231,1,0,80,225,200,0,0,27,8,0,138,226,0,16,144,229,32,16,141,229,4,0,144,229,36,0,141,229
	.byte 32,0,157,229,36,16,157,229,3,32,160,227
bl _p_152

	.byte 176,0,0,234,0,0,154,229,22,16,208,229,0,0,81,227,186,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 580
	.byte 1,16,159,231,1,0,80,225,178,0,0,27,8,0,138,226,0,16,144,229,0,16,141,229,4,16,144,229,4,16,141,229
	.byte 8,16,144,229,8,16,141,229,12,0,144,229,12,0,141,229,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 564
	.byte 0,0,159,231,0,16,144,229,13,0,160,225
bl _p_151

	.byte 147,0,0,234,0,0,154,229,22,16,208,229,0,0,81,227,157,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 576
	.byte 1,16,159,231,1,0,80,225,149,0,0,27,2,11,154,237,4,11,141,237,16,0,141,226,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 568
	.byte 1,16,159,231,0,32,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 564
	.byte 2,32,159,231,0,32,146,229
bl _p_150

	.byte 121,0,0,234,0,0,154,229,22,16,208,229,0,0,81,227,131,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 572
	.byte 1,16,159,231,1,0,80,225,123,0,0,27,2,234,154,237,206,10,183,238,192,235,183,238,6,234,141,237,24,0,141,226
	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 568
	.byte 1,16,159,231,0,32,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 564
	.byte 2,32,159,231,0,32,146,229
bl _p_149

	.byte 93,0,0,234,10,0,160,225,0,16,154,229,15,224,160,225,32,240,145,229,0,16,160,225,0,224,209,229
bl _p_148

	.byte 85,0,0,234,10,64,160,225,10,176,160,225,0,0,90,227,10,0,0,10,0,0,148,229,0,0,144,229,8,0,144,229
	.byte 8,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 560
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,176,160,227,0,0,91,227,20,0,0,10,0,0,154,229,22,16,208,229
	.byte 0,0,81,227,78,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 560
	.byte 1,16,159,231,1,0,80,225,70,0,0,27,8,0,138,226,0,16,144,229,40,16,141,229,4,0,144,229,44,0,141,229
	.byte 40,0,157,229,44,16,157,229
bl _p_147

	.byte 47,0,0,234,10,176,160,225,10,64,160,225,0,0,90,227,10,0,0,10,0,0,155,229,0,0,144,229,8,0,144,229
	.byte 8,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 556
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,0,0,84,227,26,0,0,10,0,0,154,229,22,16,208,229
	.byte 0,0,81,227,40,0,0,27,0,0,144,229,0,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 556
	.byte 1,16,159,231,1,0,80,225,32,0,0,27,8,0,138,226,0,16,144,229,48,16,141,229,4,16,144,229,52,16,141,229
	.byte 8,16,144,229,56,16,141,229,12,0,144,229,60,0,141,229,48,0,157,229,52,16,157,229,56,32,157,229,60,48,157,229
bl _p_146

	.byte 3,0,0,234,10,0,160,225,0,16,154,229,15,224,160,225,32,240,145,229,64,208,141,226,112,13,189,232,128,128,189,232
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . -12
	.byte 0,0,159,231,103,22,0,227
bl _p_1

	.byte 0,16,160,225,94,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_3

	.byte 14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_e9:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext
_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,96,208,77,226,13,176,160,225,84,0,139,229,0,0,160,227,0,0,203,229
	.byte 0,0,160,227,4,0,139,229,84,0,155,229,44,160,144,229,84,0,155,229,0,16,224,227,44,16,128,229,0,0,160,227
	.byte 0,0,203,229,10,96,160,225,5,0,90,227,140,1,0,42,6,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 600
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,84,96,155,229,6,0,160,225,8,80,144,229,5,64,160,225
	.byte 0,0,85,227,10,0,0,10,0,0,149,229,0,0,144,229,8,0,144,229,8,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 92
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,12,64,134,229,84,0,155,229,12,0,144,229,0,0,80,227
	.byte 11,0,0,10,84,0,155,229,0,16,160,225,12,16,145,229,36,16,128,229,84,0,155,229,40,0,208,229,0,0,80,227
	.byte 104,1,0,26,84,0,155,229,1,16,160,227,44,16,128,229,100,1,0,234,84,0,155,229,8,96,144,229,6,64,160,225
	.byte 0,0,86,227,10,0,0,10,0,0,150,229,0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 96
	.byte 1,16,159,231,1,0,80,225,0,0,0,10,0,64,160,227,0,0,84,227,11,0,0,10,84,0,155,229,0,16,160,225
	.byte 8,16,145,229,36,16,128,229,84,0,155,229,40,0,208,229,0,0,80,227,74,1,0,26,84,0,155,229,2,16,160,227
	.byte 44,16,128,229,70,1,0,234,84,0,155,229,8,0,144,229,8,0,139,229,0,96,160,225,0,0,80,227,21,0,0,10
	.byte 0,96,150,229,180,1,214,225,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,1,0,80,225,13,0,0,58,16,0,150,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,1,0,0,26,1,80,160,227,0,0,0,234,0,80,160,227,0,0,85,227,2,0,0,10,0,0,160,227
	.byte 12,0,139,229,1,0,0,234,8,0,155,229,12,0,139,229,12,0,155,229,0,0,80,227,12,1,0,10,84,0,155,229
	.byte 56,0,139,229,84,0,155,229,8,64,144,229,52,64,139,229,0,0,84,227,22,0,0,10,52,0,155,229,0,0,144,229
	.byte 180,17,208,225,0,32,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 2,32,159,231,2,0,81,225,17,1,0,59,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 88
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,3,1,0,11,4,0,160,225,0,16,148,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 76
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,0,16,160,225,56,0,155,229,16,16,128,229,2,160,224,227
	.byte 3,0,74,226,60,0,139,229,1,0,80,227,8,0,0,42,60,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 596
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,118,0,0,234,84,0,155,229,92,0,139,229,84,0,155,229
	.byte 16,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 84
	.byte 8,128,159,231,4,224,143,226,16,240,17,229,0,0,0,0,0,16,160,225,92,0,155,229,20,16,128,229,84,0,155,229
	.byte 88,0,139,229,84,0,155,229,20,0,144,229
bl _p_25

	.byte 0,16,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 100
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,16,160,225,88,0,155,229,28,16,128,229,2,160,224,227
	.byte 3,0,74,226,64,0,139,229,1,0,80,227,8,0,0,42,64,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 592
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,31,0,0,234,84,0,155,229,88,0,139,229,84,0,155,229
	.byte 28,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 104
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,16,160,225,88,0,155,229,32,16,128,229,84,0,155,229
	.byte 0,16,160,225,32,16,145,229,36,16,128,229,84,0,155,229,40,0,208,229,0,0,80,227,2,0,0,26,84,0,155,229
	.byte 3,16,160,227,44,16,128,229,1,0,160,227,0,0,203,229,17,0,0,235,55,0,0,235,148,0,0,234,84,0,155,229
	.byte 28,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,209,255,255,26,0,0,0,235
	.byte 22,0,0,234,44,224,139,229,0,0,219,229,0,0,80,227,1,0,0,10,44,192,155,229,12,240,160,225,84,0,155,229
	.byte 28,0,144,229,0,0,80,227,10,0,0,10,84,0,155,229,28,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,44,192,155,229,12,240,160,225,84,0,155,229,16,16,144,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,122,255,255,26,0,0,0,235
	.byte 88,0,0,234,48,224,139,229,0,0,219,229,0,0,80,227,1,0,0,10,48,192,155,229,12,240,160,225,84,0,155,229
	.byte 80,0,139,229,84,0,155,229,16,0,144,229,20,0,139,229,20,0,155,229,68,0,139,229,0,0,80,227,24,0,0,10
	.byte 68,0,155,229,0,0,144,229,72,0,139,229,180,1,208,225,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,1,0,80,225,14,0,0,58,72,0,155,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,76,0,139,229,1,0,0,234,0,0,160,227,76,0,139,229,76,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,24,0,139,229,1,0,0,234,20,0,155,229,24,0,139,229,24,0,155,229
	.byte 16,0,139,229,28,0,139,229,0,16,160,225,28,0,155,229,4,0,139,229,80,0,155,229,24,16,128,229,4,0,155,229
	.byte 0,0,80,227,10,0,0,10,84,0,155,229,24,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,48,192,155,229,12,240,160,225,84,0,155,229,0,16,160,225
	.byte 8,16,145,229,36,16,128,229,84,0,155,229,40,0,208,229,0,0,80,227,8,0,0,26,84,0,155,229,4,16,160,227
	.byte 44,16,128,229,4,0,0,234,84,0,155,229,0,16,224,227,44,16,128,229,0,0,160,227,0,0,0,234,1,0,160,227
	.byte 96,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_144

	.byte 93,2,0,2

Lme_f1:
.text
	.align 2
	.no_dead_strip _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose
_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,64,208,77,226,13,176,160,225,60,0,139,229,0,0,160,227,0,0,139,229
	.byte 60,0,155,229,44,0,144,229,60,16,155,229,1,32,160,227,40,32,193,229,60,16,155,229,0,32,224,227,44,32,129,229
	.byte 40,0,139,229,5,0,80,227,102,0,0,42,40,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 604
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,0,0,0,235,17,0,0,234,28,224,139,229,60,0,155,229
	.byte 28,0,144,229,0,0,80,227,10,0,0,10,60,0,155,229,28,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,28,192,155,229,12,240,160,225,0,0,0,235,71,0,0,234
	.byte 36,224,139,229,60,0,155,229,56,0,139,229,60,0,155,229,16,0,144,229,8,0,139,229,8,0,155,229,44,0,139,229
	.byte 0,0,80,227,24,0,0,10,44,0,155,229,0,0,144,229,48,0,139,229,180,1,208,225,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,1,0,80,225,14,0,0,58,48,0,155,229,16,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 524
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,52,0,139,229,1,0,0,234,0,0,160,227,52,0,139,229,52,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,12,0,139,229,1,0,0,234,8,0,155,229,12,0,139,229,12,0,155,229
	.byte 4,0,139,229,16,0,139,229,0,16,160,225,16,0,155,229,0,0,139,229,56,0,155,229,24,16,128,229,0,0,155,229
	.byte 0,0,80,227,10,0,0,10,60,0,155,229,24,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,36,192,155,229,12,240,160,225,64,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_f2:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_157

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_155

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_156

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_155
bl _p_154

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_f7:
.text
	.align 2
	.no_dead_strip _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0
_System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0:

	.byte 128,64,45,233,13,112,160,225,64,13,45,233,24,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 4,0,139,229,6,0,160,225,10,16,160,225
bl _p_139

	.byte 6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 496
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,4,0,139,229,22,0,0,234,4,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 500
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,24,160,225,33,24,160,225,176,0,203,225,10,0,160,225
	.byte 15,224,160,225,12,240,154,229,255,0,0,226,0,0,80,227,3,0,0,26,0,0,160,227,8,0,203,229,15,0,0,235
	.byte 32,0,0,234,4,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 80
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,219,255,255,26,0,0,0,235
	.byte 15,0,0,234,20,224,139,229,4,0,155,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_Xml_Linq_got - . + 520
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,20,192,155,229,12,240,160,225,1,0,160,227,0,0,0,234
	.byte 8,0,219,229,24,208,139,226,64,13,189,232,128,128,189,232

Lme_10d:
.text
ut_271:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current

.text
ut_272:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array

.text
ut_273:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current

.text
ut_274:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose

.text
ut_275:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext

.text
ut_276:

	.byte 8,0,128,226
	b _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset

.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Name
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_get_NextAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_set_NextAttribute_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_set_PreviousAttribute_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_Remove
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute_ToString
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XAttribute__cctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XCData__ctor_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XCData__ctor_System_Xml_Linq_XCData
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XCData_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XCData_WriteTo_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XComment__ctor_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XComment__ctor_System_Xml_Linq_XComment
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XComment_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XComment_get_Value
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XComment_WriteTo_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer_get_FirstNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer_Nodes
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XNode_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerator_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_MoveNext
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Dispose
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Reset
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Encoding
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Standalone
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Version
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDeclaration_ToString
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_get_Declaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_set_Declaration_System_Xml_Linq_XDeclaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_Load_System_IO_Stream
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocument_VerifyAddedNode_object_bool
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_Name
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_PublicId
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_SystemId
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_InternalSubset
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XDocumentType_WriteTo_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_FirstAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_set_FirstAttribute_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_LastAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_set_LastAttribute_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_HasAttributes
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_IsEmpty
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_Name
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_Attributes
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_GetSchema
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__cctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacem__0_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XAttribute_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerator_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_MoveNext
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Dispose
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Reset
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__m__0_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerator_string_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerator_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Reset
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerable_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_get_LocalName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_get_Namespace
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_Equals_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_System_IEquatable_System_Xml_Linq_XName_Equals_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_Get_string_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_GetHashCode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XName_ToString
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace__cctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_get_None
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_get_Xmlns
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace__ctor_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_get_NamespaceName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_Equals_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Implicit_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_GetHashCode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNamespace_ToString
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_set_PreviousNode_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_get_NextNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_set_NextNode_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_ToString
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode_CreateReader
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNode__cctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_object_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_object_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LineNumber
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LinePosition
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_AttributeCount
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_BaseURI
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Depth
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_EOF
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_HasAttributes
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_IsEmptyElement
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_LocalName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NamespaceURI
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NameTable
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Prefix
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_ReadState
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Value
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_HasLineInfo
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetName_int
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_Close
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_LookupNamespace_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToElement
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToFirstAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToNextAttribute
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_Read
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_ReadAttributeValue
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XNodeReader_ResolveEntity
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LineNumber
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LinePosition
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_BaseUri
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_set_BaseUri_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_Document
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_Parent
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_Owner
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_LineNumber
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_set_LineNumber_int
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_get_LinePosition
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_set_LinePosition_int
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_SetOwner_System_Xml_Linq_XContainer
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_HasLineInfo
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnAddingObject_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnAddedObject_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnRemovingObject_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnRemovedObject_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanging_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanged_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__ctor_System_Xml_Linq_XObjectChange
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__cctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Data
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Target
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_WriteTo_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText__ctor_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText__ctor_System_Xml_Linq_XText
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText_get_NodeType
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText_get_Value
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText_set_Value_string
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText_WriteTo_System_Xml_XmlWriter
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XText__WriteTom__0_char
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil_ExpandArray_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil_ToNode_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil_Clone_object
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerator_object_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerator_get_Current
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0__ctor
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Reset
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerable_GetEnumerator
.no_dead_strip _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator
.no_dead_strip _System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_System_Xml_Linq_XAttribute_bool_invoke_TResult__this___T_System_Xml_Linq_XAttribute
.no_dead_strip _System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XNamespace_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XNamespace_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XNamespace
.no_dead_strip _System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XName_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XName_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XName
.no_dead_strip _System_Xml_Linq__wrapper_delegate_invoke_System_EventHandler_1_System_Xml_Linq_XObjectChangeEventArgs_invoke_void__this___object_TEventArgs_object_System_Xml_Linq_XObjectChangeEventArgs
.no_dead_strip _System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_char_bool_invoke_TResult__this___T_char
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext
.no_dead_strip _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset
.no_dead_strip _System_Xml_Linq__System_Array_InternalArray__IEnumerable_GetEnumerator_char
.no_dead_strip _System_Xml_Linq__System_Array_InternalArray__get_Item_char_int

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Name
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_NextAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_set_NextAttribute_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_set_PreviousAttribute_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_Remove
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute_ToString
	bl _System_Xml_Linq__System_Xml_Linq_XAttribute__cctor
	bl _System_Xml_Linq__System_Xml_Linq_XCData__ctor_string
	bl _System_Xml_Linq__System_Xml_Linq_XCData__ctor_System_Xml_Linq_XCData
	bl _System_Xml_Linq__System_Xml_Linq_XCData_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XCData_WriteTo_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XComment__ctor_string
	bl _System_Xml_Linq__System_Xml_Linq_XComment__ctor_System_Xml_Linq_XComment
	bl _System_Xml_Linq__System_Xml_Linq_XComment_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XComment_get_Value
	bl _System_Xml_Linq__System_Xml_Linq_XComment_WriteTo_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XContainer_get_FirstNode
	bl _System_Xml_Linq_XContainer_CheckChildType_object_bool_0
	bl _System_Xml_Linq_XContainer_Add_object_0
	bl _System_Xml_Linq__System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XContainer_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	bl _System_Xml_Linq__System_Xml_Linq_XContainer_Nodes
	bl _System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XNode_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerator_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_MoveNext
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Dispose
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_Reset
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Encoding
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Standalone
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration_get_Version
	bl _System_Xml_Linq__System_Xml_Linq_XDeclaration_ToString
	bl _System_Xml_Linq__System_Xml_Linq_XDocument__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_get_Declaration
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_set_Declaration_System_Xml_Linq_XDeclaration
	bl _System_Xml_Linq_XDocument_get_DocumentType_0
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_get_NodeType
	bl _System_Xml_Linq_XDocument_get_Root_0
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_Load_System_IO_Stream
	bl _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string
	bl _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool
	bl _System_Xml_Linq__System_Xml_Linq_XDocument_VerifyAddedNode_object_bool
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_Name
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_PublicId
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_SystemId
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_InternalSubset
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XDocumentType_WriteTo_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement
	bl _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_FirstAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement_set_FirstAttribute_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_LastAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement_set_LastAttribute_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_HasAttributes
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_IsEmpty
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_Name
	bl _System_Xml_Linq__System_Xml_Linq_XElement_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XElement_Attributes
	bl _System_Xml_Linq__System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object
	bl _System_Xml_Linq__System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0
	bl _System_Xml_Linq__System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool
	bl _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0
	bl _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0
	bl _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0
	bl _System_Xml_Linq__System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace
	bl _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0
	bl _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	bl _System_Xml_Linq__System_Xml_Linq_XElement_System_Xml_Serialization_IXmlSerializable_GetSchema
	bl _System_Xml_Linq__System_Xml_Linq_XElement__cctor
	bl _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacem__0_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerator_System_Xml_Linq_XAttribute_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerator_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_MoveNext
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Dispose
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_Reset
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XElement__CreateDummyNamespacec__AnonStorey4__m__0_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerator_string_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerator_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3__ctor
	bl _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0
	bl _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Reset
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_IEnumerable_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace
	bl _System_Xml_Linq__System_Xml_Linq_XName_get_LocalName
	bl _System_Xml_Linq__System_Xml_Linq_XName_get_Namespace
	bl _System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName
	bl _System_Xml_Linq__System_Xml_Linq_XName_Equals_object
	bl _System_Xml_Linq__System_Xml_Linq_XName_System_IEquatable_System_Xml_Linq_XName_Equals_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XName_Get_string_string
	bl _System_Xml_Linq__System_Xml_Linq_XName_GetHashCode
	bl _System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XName_ToString
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace__cctor
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_get_None
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_get_Xmlns
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace__ctor_string
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_get_NamespaceName
	bl _System_Xml_Linq_XNamespace_Get_string_0
	bl _System_Xml_Linq_XNamespace_GetName_string_0
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_Equals_object
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_op_Implicit_string
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_GetHashCode
	bl _System_Xml_Linq__System_Xml_Linq_XNamespace_ToString
	bl _System_Xml_Linq__System_Xml_Linq_XNode__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XNode_set_PreviousNode_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XNode_get_NextNode
	bl _System_Xml_Linq__System_Xml_Linq_XNode_set_NextNode_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions
	bl _System_Xml_Linq__System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XNode_ToString
	bl method_addresses
	bl _System_Xml_Linq__System_Xml_Linq_XNode_CreateReader
	bl _System_Xml_Linq__System_Xml_Linq_XNode__cctor
	bl _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult
	bl _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_System_Collections_IComparer_Compare_object_object
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer__ctor
	bl _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_Equals_object_object
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration
	bl _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0
	bl _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_System_Collections_IEqualityComparer_GetHashCode_object
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LineNumber
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_get_LinePosition
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_AttributeCount
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_BaseURI
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Depth
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_EOF
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_HasAttributes
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_IsEmptyElement
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_LocalName
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NamespaceURI
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NameTable
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Prefix
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_ReadState
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_get_Value
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_System_Xml_IXmlLineInfo_HasLineInfo
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute
	bl _System_Xml_Linq_XNodeReader_GetXAttribute_int_0
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetName_int
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_Close
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_LookupNamespace_string
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToElement
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToFirstAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToNextAttribute
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_MoveToAttribute_string_string
	bl _System_Xml_Linq_XNodeReader_GetAttribute_string_0
	bl _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_Read
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_ReadAttributeValue
	bl _System_Xml_Linq__System_Xml_Linq_XNodeReader_ResolveEntity
	bl _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LineNumber
	bl _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_get_LinePosition
	bl _System_Xml_Linq__System_Xml_Linq_XObject__ctor
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_BaseUri
	bl _System_Xml_Linq__System_Xml_Linq_XObject_set_BaseUri_string
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_Document
	bl method_addresses
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_Parent
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_Owner
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_LineNumber
	bl _System_Xml_Linq__System_Xml_Linq_XObject_set_LineNumber_int
	bl _System_Xml_Linq__System_Xml_Linq_XObject_get_LinePosition
	bl _System_Xml_Linq__System_Xml_Linq_XObject_set_LinePosition_int
	bl _System_Xml_Linq__System_Xml_Linq_XObject_SetOwner_System_Xml_Linq_XContainer
	bl _System_Xml_Linq__System_Xml_Linq_XObject_System_Xml_IXmlLineInfo_HasLineInfo
	bl _System_Xml_Linq__System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnAddingObject_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnAddedObject_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnRemovingObject_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnRemovedObject_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanging_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnValueChanged_object
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs
	bl _System_Xml_Linq__System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs
	bl _System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__ctor_System_Xml_Linq_XObjectChange
	bl _System_Xml_Linq__System_Xml_Linq_XObjectChangeEventArgs__cctor
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Data
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_get_Target
	bl _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction_WriteTo_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XText__ctor_string
	bl _System_Xml_Linq__System_Xml_Linq_XText__ctor_System_Xml_Linq_XText
	bl _System_Xml_Linq__System_Xml_Linq_XText_get_NodeType
	bl _System_Xml_Linq__System_Xml_Linq_XText_get_Value
	bl _System_Xml_Linq__System_Xml_Linq_XText_set_Value_string
	bl _System_Xml_Linq__System_Xml_Linq_XText_WriteTo_System_Xml_XmlWriter
	bl _System_Xml_Linq__System_Xml_Linq_XText__WriteTom__0_char
	bl _System_Xml_Linq_XUtil_ToString_object
	bl _System_Xml_Linq__System_Xml_Linq_XUtil_ExpandArray_object
	bl _System_Xml_Linq__System_Xml_Linq_XUtil_ToNode_object
	bl _System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject
	bl _System_Xml_Linq__System_Xml_Linq_XUtil_Clone_object
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerator_object_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerator_get_Current
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0__ctor
	bl _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext
	bl _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Reset
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_IEnumerable_GetEnumerator
	bl _System_Xml_Linq__System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator
	bl method_addresses
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_System_Xml_Linq_XAttribute_bool_invoke_TResult__this___T_System_Xml_Linq_XAttribute
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XNamespace_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XNamespace_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XNamespace
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_Xml_Linq__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Xml_Linq_XName_System_Collections_Generic_KeyValuePair_2_string_System_Xml_Linq_XName_invoke_TRet__this___TKey_TValue_string_System_Xml_Linq_XName
	bl _System_Xml_Linq__wrapper_delegate_invoke_System_EventHandler_1_System_Xml_Linq_XObjectChangeEventArgs_invoke_void__this___object_TEventArgs_object_System_Xml_Linq_XObjectChangeEventArgs
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_Xml_Linq__wrapper_delegate_invoke_System_Func_2_char_bool_invoke_TResult__this___T_char
	bl _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0
	bl method_addresses
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_get_Current
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char__ctor_System_Array
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char_get_Current
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char_Dispose
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char_MoveNext
	bl _System_Xml_Linq__System_Array_InternalEnumerator_1_char_System_Collections_IEnumerator_Reset
	bl _System_Xml_Linq__System_Array_InternalArray__IEnumerable_GetEnumerator_char
	bl method_addresses
	bl _System_Xml_Linq__System_Array_InternalArray__get_Item_char_int
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:

	.long 271

	bl ut_271

	.long 272

	bl ut_272

	.long 273

	bl ut_273

	.long 274

	bl ut_274

	.long 275

	bl ut_275

	.long 276

	bl ut_276
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 280,10,28,2
	.short 0, 10, 20, 30, 40, 51, 62, 73
	.short 84, 95, 106, 117, 128, 139, 150, 166
	.short 177, 188, 199, 210, 221, 232, 243, 254
	.short 265, 285, 305, 321
	.byte 1,3,3,4,3,3,3,3,3,3,32,8,3,14,8,2,2,2,4,2,79,2,2,2,2,2,17,7,3,2,121,2
	.byte 2,2,2,2,2,2,2,3,128,142,2,2,2,2,11,2,2,2,9,128,178,9,3,5,3,6,2,8,2,8,128,226
	.byte 2,2,2,2,2,2,2,3,3,128,249,3,3,3,3,5,3,3,3,4,129,29,3,4,4,9,20,15,10,8,4,129
	.byte 123,3,3,3,6,4,2,2,2,2,129,152,2,2,3,2,2,2,2,2,13,129,188,2,2,3,2,2,2,2,3,2
	.byte 129,210,2,2,2,4,11,4,4,3,3,129,252,5,4,3,3,3,3,3,3,3,130,29,3,5,11,255,255,255,253,208
	.byte 130,51,4,7,2,2,130,68,2,4,2,45,2,2,4,2,27,130,161,2,2,3,5,3,4,2,4,4,130,196,5,2
	.byte 2,8,2,13,3,2,8,130,243,18,2,4,2,2,2,5,6,4,131,34,2,8,2,2,2,2,2,2,2,0,131,64
	.byte 3,2,2,2,2,2,2,2,131,88,3,3,3,3,3,3,4,4,3,131,128,2,2,2,2,2,2,2,2,2,131,148
	.byte 2,10,2,31,3,8,2,28,2,131,238,2,28,12,2,2,255,255,255,251,228,132,31,255,255,255,251,225,0,0,0,132
	.byte 33,255,255,255,251,223,0,0,0,132,36,255,255,255,251,220,0,0,0,132,39,3,255,255,255,251,214,0,0,0,132,45
	.byte 3,0,132,56,6,2,4,2,2,2,255,255,255,251,182,132,80
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 37,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,1234,268,0,1121,247,39,1177
	.long 257,37,1324,274,0,1140,252,0
	.long 0,0,0,1343,275,0,1305,273
	.long 0,0,0,0,1381,277,0,0
	.long 0,0,1286,272,0,0,0,0
	.long 1212,262,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,1222,263,38,1362
	.long 276,0,0,0,0,0,0,0
	.long 1246,269,0,1267,271,0,1401,279
	.long 0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 33,247,1121,248,0,249,0,250
	.long 0,251,0,252,1140,253,0,254
	.long 0,255,0,256,0,257,1177,258
	.long 0,259,0,260,0,261,0,262
	.long 1212,263,1222,264,0,265,0,266
	.long 0,267,0,268,1234,269,1246,270
	.long 0,271,1267,272,1286,273,1305,274
	.long 1324,275,1343,276,1362,277,1381,278
	.long 0,279,1401
.section __TEXT, __const
	.align 3
class_name_table:

	.short 73, 0, 0, 0, 0, 0, 0, 30
	.short 0, 0, 0, 21, 0, 0, 0, 1
	.short 0, 0, 0, 0, 0, 0, 0, 25
	.short 76, 16, 0, 0, 0, 0, 0, 0
	.short 0, 7, 73, 10, 0, 0, 0, 13
	.short 74, 27, 0, 0, 0, 8, 0, 0
	.short 0, 11, 0, 0, 0, 28, 0, 0
	.short 0, 14, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 15, 0, 9
	.short 0, 0, 0, 5, 0, 0, 0, 0
	.short 0, 0, 0, 24, 0, 0, 0, 29
	.short 0, 0, 0, 0, 0, 20, 0, 18
	.short 0, 0, 0, 17, 0, 2, 0, 0
	.short 0, 6, 75, 3, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 12, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 4, 0, 0, 0, 0
	.short 0, 0, 0, 31, 0, 0, 0, 0
	.short 0, 0, 0, 19, 0, 22, 0, 23
	.short 0, 26, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 155,10,16,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 165
	.byte 133,141,2,1,1,1,3,3,5,3,4,133,167,3,3,3,3,3,3,3,5,4,133,204,7,4,5,5,5,5,3,5
	.byte 11,134,9,3,7,4,4,4,4,4,11,11,134,64,5,5,3,4,4,4,3,4,3,134,102,3,3,3,7,11,11,3
	.byte 4,5,134,158,2,2,6,12,4,2,2,11,11,134,213,3,3,14,6,4,3,4,4,6,135,8,4,4,4,4,4,3
	.byte 3,6,3,135,48,5,5,3,3,3,3,4,4,3,135,84,3,4,3,4,3,3,5,5,4,135,122,5,5,5,4,4
	.byte 4,3,4,4,135,172,6,3,3,6,3,3,10,29,20,136,3,5,21,21,5,5,4,5,5,8,136,92,8,10,5,5
	.byte 7,4,5,5,5,136,151,14,4,4,11
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 280,10,28,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 170
	.short 181, 192, 203, 214, 225, 236, 247, 258
	.short 269, 289, 309, 325
	.byte 140,229,3,3,3,3,3,3,3,3,3,141,3,3,3,3,3,3,3,3,3,3,141,33,3,3,3,3,3,14,13,3
	.byte 3,141,84,3,3,3,3,3,3,3,3,3,141,114,3,3,3,3,3,3,3,3,18,141,159,18,3,14,3,3,3,14
	.byte 3,3,141,226,3,3,3,3,3,3,3,3,3,142,0,3,3,3,3,3,3,3,3,3,142,30,3,3,3,15,3,25
	.byte 19,19,3,142,138,3,3,3,3,3,3,3,3,3,142,168,3,3,3,3,3,3,3,3,20,142,224,3,3,3,3,3
	.byte 3,3,3,3,142,254,3,3,3,3,3,3,3,3,3,143,39,14,3,3,3,3,3,3,3,3,143,80,3,3,3,255
	.byte 255,255,240,167,143,92,3,3,3,3,143,107,3,3,3,60,3,3,3,3,35,143,226,3,3,3,3,3,3,3,3,3
	.byte 144,0,3,3,3,3,3,3,3,3,20,144,47,3,3,3,3,3,3,3,3,3,144,88,14,3,3,3,3,3,3,3
	.byte 3,0,144,129,3,3,3,3,3,3,3,3,144,156,3,3,3,3,3,3,3,3,3,144,186,3,3,3,3,3,3,3
	.byte 3,3,144,216,3,3,3,4,3,3,3,3,3,144,247,3,34,21,3,3,255,255,255,238,201,145,58,255,255,255,238,198
	.byte 0,0,0,145,90,255,255,255,238,166,0,0,0,145,93,255,255,255,238,163,0,0,0,145,96,3,255,255,255,238,157,0
	.byte 0,0,145,102,3,0,145,124,3,3,3,3,3,3,255,255,255,238,114,145,145
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,112,68,13,11
	.byte 29,12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,64,68,13,11,29,12
	.byte 13,0,72,14,8,135,2,68,14,28,132,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11,23,12,13,0
	.byte 72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11,29,12,13,0,72,14,8,135,2,68,14,28
	.byte 133,7,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11,29,12,13,0,72,14,8,135,2,68,14,28,132,7
	.byte 133,6,134,5,136,4,139,3,142,1,68,14,72,68,13,11,31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7
	.byte 134,6,136,5,138,4,139,3,142,1,68,14,96,68,13,11,31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7
	.byte 134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11,29,12,13,0,72,14,8,135,2,68,14,28,133,7,134,6
	.byte 136,5,138,4,139,3,142,1,68,14,56,68,13,11,30,12,13,0,72,14,8,135,2,68,14,28,132,7,134,6,136,5
	.byte 138,4,139,3,142,1,68,14,136,1,68,13,11,31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136
	.byte 5,138,4,139,3,142,1,68,14,64,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68
	.byte 14,40,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,64,68,13,11,27
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,64,68,13,11,32,12,13,0,72
	.byte 14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,176,1,68,13,11,32,12,13,0
	.byte 72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13,11,25,12,13
	.byte 0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,56,68,13,11,28,12,13,0,72,14,8,135,2
	.byte 68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,96,23,12,13,0,72,14,8,135,2,68,14,16
	.byte 136,4,139,3,142,1,68,14,80,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14
	.byte 48,68,13,11,27,12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 31,10,4,2
	.short 0, 11, 22, 34
	.byte 145,148,7,99,99,28,29,29,30,30,20,147,37,29,34,30,23,30,15,18,31,27,148,117,31,128,171,30,99,25,31,31
	.byte 23,37,150,106

.text
	.align 4
plt:
_mono_aot_System_Xml_Linq_plt:
_p_1_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 620,2243
_p_2_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 624,2263
_p_3_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 628,2296

.set _p_4_plt_System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm, _System_Xml_Linq__System_Xml_Linq_XName_op_Equality_System_Xml_Linq_XName_System_Xml_Linq_XName

.set _p_5_plt_System_Xml_Linq_XAttribute_SetValue_object_llvm, _System_Xml_Linq__System_Xml_Linq_XAttribute_SetValue_object
_p_6_plt_System_Xml_Linq_XNamespace_get_Xmlns_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_get_Xmlns
plt_System_Xml_Linq_XNamespace_get_Xmlns:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 640,2328
_p_7_plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
plt_System_Xml_Linq_XNamespace_op_Equality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 644,2330
_p_8_plt_string_op_Equality_string_string_llvm:
	.no_dead_strip plt_string_op_Equality_string_string
plt_string_op_Equality_string_string:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 648,2333
_p_9_plt_System_Xml_Linq_XNamespace_get_None_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_get_None
plt_System_Xml_Linq_XNamespace_get_None:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 652,2338
_p_10_plt_System_Xml_Linq_XUtil_ToString_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XUtil_ToString_object
plt_System_Xml_Linq_XUtil_ToString_object:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 656,2340
_p_11_plt_System_Xml_Linq_XObject_OnRemovingObject_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnRemovingObject_object
plt_System_Xml_Linq_XObject_OnRemovingObject_object:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 660,2343
_p_12_plt_System_Xml_Linq_XObject_OnRemovedObject_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnRemovedObject_object
plt_System_Xml_Linq_XObject_OnRemovedObject_object:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 664,2346
_p_13_plt_System_Xml_Linq_XObject_OnValueChanging_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnValueChanging_object
plt_System_Xml_Linq_XObject_OnValueChanging_object:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 668,2349
_p_14_plt_System_Xml_Linq_XObject_OnValueChanged_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnValueChanged_object
plt_System_Xml_Linq_XObject_OnValueChanged_object:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 672,2352
_p_15_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 676,2355
_p_16_plt_System_Text_StringBuilder__ctor_llvm:
	.no_dead_strip plt_System_Text_StringBuilder__ctor
plt_System_Text_StringBuilder__ctor:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 680,2378
_p_17_plt_System_Text_StringBuilder_Append_string_llvm:
	.no_dead_strip plt_System_Text_StringBuilder_Append_string
plt_System_Text_StringBuilder_Append_string:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 684,2383
_p_18_plt_string_IndexOfAny_char___int_llvm:
	.no_dead_strip plt_string_IndexOfAny_char___int
plt_string_IndexOfAny_char___int:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 688,2388
_p_19_plt_System_Text_StringBuilder_Append_string_int_int_llvm:
	.no_dead_strip plt_System_Text_StringBuilder_Append_string_int_int
plt_System_Text_StringBuilder_Append_string_int_int:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 692,2393
_p_20_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 696,2398
_p_21_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 700,2443
_p_22_plt_string_memcpy_byte__byte__int_llvm:
	.no_dead_strip plt_string_memcpy_byte__byte__int
plt_string_memcpy_byte__byte__int:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 704,2469
_p_23_plt_string_Concat_object_object_llvm:
	.no_dead_strip plt_string_Concat_object_object
plt_string_Concat_object_object:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 708,2474
_p_24_plt_System_Xml_Linq_XContainer_CheckChildType_object_bool_llvm:
	.no_dead_strip plt_System_Xml_Linq_XContainer_CheckChildType_object_bool
plt_System_Xml_Linq_XContainer_CheckChildType_object_bool:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 712,2479
_p_25_plt_System_Xml_Linq_XUtil_ExpandArray_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XUtil_ExpandArray_object
plt_System_Xml_Linq_XUtil_ExpandArray_object:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 716,2481
_p_26_plt_System_Xml_Linq_XUtil_ToNode_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XUtil_ToNode_object
plt_System_Xml_Linq_XUtil_ToNode_object:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 720,2484
_p_27_plt_System_Xml_Linq_XObject_OnAddingObject_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnAddingObject_object
plt_System_Xml_Linq_XObject_OnAddingObject_object:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 724,2487
_p_28_plt_System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode_llvm:
	.no_dead_strip plt_System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode
plt_System_Xml_Linq_XContainer_AddNode_System_Xml_Linq_XNode:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 728,2490
_p_29_plt_System_Xml_Linq_XObject_OnAddedObject_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnAddedObject_object
plt_System_Xml_Linq_XObject_OnAddedObject_object:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 732,2492

.set _p_30_plt_System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject_llvm, _System_Xml_Linq__System_Xml_Linq_XUtil_GetDetachedObject_System_Xml_Linq_XObject
_p_31_plt_System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XNode_ReadFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 740,2498
_p_32_plt_System_Xml_Linq_XContainer_Add_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XContainer_Add_object
plt_System_Xml_Linq_XContainer_Add_object:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 744,2501
_p_33_plt__jit_icall_mono_create_corlib_exception_0_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_0
plt__jit_icall_mono_create_corlib_exception_0:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 748,2503
_p_34_plt_System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator_llvm:
	.no_dead_strip plt_System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator
plt_System_Xml_Linq_XContainer__Nodesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XNode_GetEnumerator:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 752,2536
_p_35_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_int__int_int
plt_System_Threading_Interlocked_CompareExchange_int__int_int:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 756,2538
_p_36_plt_string_Concat_string___llvm:
	.no_dead_strip plt_string_Concat_string__
plt_string_Concat_string__:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 760,2543
_p_37_plt_System_Xml_Linq_XContainer_Nodes_llvm:
	.no_dead_strip plt_System_Xml_Linq_XContainer_Nodes
plt_System_Xml_Linq_XContainer_Nodes:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 764,2548
_p_38_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamReader__ctor_System_IO_Stream
plt_System_IO_StreamReader__ctor_System_IO_Stream:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 768,2550
_p_39_plt_System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 772,2555
_p_40_plt_System_Xml_XmlReaderSettings__ctor_llvm:
	.no_dead_strip plt_System_Xml_XmlReaderSettings__ctor
plt_System_Xml_XmlReaderSettings__ctor:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 776,2557
_p_41_plt_System_Xml_XmlReader_Create_System_IO_TextReader_System_Xml_XmlReaderSettings_llvm:
	.no_dead_strip plt_System_Xml_XmlReader_Create_System_IO_TextReader_System_Xml_XmlReaderSettings
plt_System_Xml_XmlReader_Create_System_IO_TextReader_System_Xml_XmlReaderSettings:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 780,2562
_p_42_plt_System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XDocument_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 784,2567
_p_43_plt_System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XDocument_ReadContent_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 788,2569
_p_44_plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XObject_FillLineInfoAndBaseUri_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 792,2571

.set _p_45_plt_System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm, _System_Xml_Linq__System_Xml_Linq_XContainer_ReadContentFrom_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
_p_46_plt_System_Xml_Linq_XDocument_get_Root_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_get_Root
plt_System_Xml_Linq_XDocument_get_Root:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 800,2576

.set _p_47_plt_System_Xml_Linq_XDeclaration__ctor_string_string_string_llvm, _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_string_string_string
_p_48_plt_System_Xml_Linq_XDocument_VerifyAddedNode_object_bool_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_VerifyAddedNode_object_bool
plt_System_Xml_Linq_XDocument_VerifyAddedNode_object_bool:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 808,2580
_p_49_plt_System_Xml_Linq_XDocument_get_DocumentType_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_get_DocumentType
plt_System_Xml_Linq_XDocument_get_DocumentType:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 812,2582

.set _p_50_plt_System_Xml_Linq_XDocument_ValidateWhitespace_string_llvm, _System_Xml_Linq__System_Xml_Linq_XDocument_ValidateWhitespace_string
_p_51_plt_System_Xml_Linq_XObject_get_Document_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_get_Document
plt_System_Xml_Linq_XObject_get_Document:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 820,2586
_p_52_plt_System_Xml_Linq_XElement_Attributes_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_Attributes
plt_System_Xml_Linq_XElement_Attributes:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 824,2589

.set _p_53_plt_System_Xml_Linq_XName_Get_string_string_llvm, _System_Xml_Linq__System_Xml_Linq_XName_Get_string_string

.set _p_54_plt_System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName_llvm, _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XName
_p_55_plt_System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object
plt_System_Xml_Linq_XElement_SetAttributeValue_System_Xml_Linq_XName_object:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 836,2595
_p_56_plt_System_Xml_Linq_XNamespace_get_NamespaceName_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_get_NamespaceName
plt_System_Xml_Linq_XNamespace_get_NamespaceName:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 840,2597
_p_57_plt_System_Xml_Linq_XNamespace_GetName_string_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_GetName_string
plt_System_Xml_Linq_XNamespace_GetName_string:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 844,2600

.set _p_58_plt_System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName_llvm, _System_Xml_Linq__System_Xml_Linq_XElement_Attribute_System_Xml_Linq_XName

.set _p_59_plt_System_Xml_Linq_XAttribute_set_Value_string_llvm, _System_Xml_Linq__System_Xml_Linq_XAttribute_set_Value_string

.set _p_60_plt_System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object_llvm, _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XName_object
_p_61_plt_System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute
plt_System_Xml_Linq_XElement_SetAttributeObject_System_Xml_Linq_XAttribute:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 860,2609
_p_62_plt_System_Xml_Linq_XAttribute_Remove_llvm:
	.no_dead_strip plt_System_Xml_Linq_XAttribute_Remove
plt_System_Xml_Linq_XAttribute_Remove:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 864,2611
_p_63_plt_System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration_llvm:
	.no_dead_strip plt_System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration
plt_System_Xml_Linq_XAttribute_get_IsNamespaceDeclaration:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 868,2613

.set _p_64_plt_System_Xml_Linq_XAttribute_get_Value_llvm, _System_Xml_Linq__System_Xml_Linq_XAttribute_get_Value
_p_65_plt_System_Xml_Linq_XNamespace_op_Implicit_string_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_op_Implicit_string
plt_System_Xml_Linq_XNamespace_op_Implicit_string:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 876,2617
_p_66_plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace
plt_System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 880,2620
_p_67_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 884,2622
_p_68_plt_System_Linq_Enumerable_All_System_Xml_Linq_XAttribute_System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_System_Func_2_System_Xml_Linq_XAttribute_bool_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_All_System_Xml_Linq_XAttribute_System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_System_Func_2_System_Xml_Linq_XAttribute_bool
plt_System_Linq_Enumerable_All_System_Xml_Linq_XAttribute_System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_System_Func_2_System_Xml_Linq_XAttribute_bool:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 888,2652

.set _p_69_plt_System_Xml_Linq_XName_get_NamespaceName_llvm, _System_Xml_Linq__System_Xml_Linq_XName_get_NamespaceName
_p_70_plt_System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter
plt_System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter:
_p_70:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 896,2666
_p_71_plt_System_Xml_XmlWriter_WriteAttributeString_string_string_string_string_llvm:
	.no_dead_strip plt_System_Xml_XmlWriter_WriteAttributeString_string_string_string_string
plt_System_Xml_XmlWriter_WriteAttributeString_string_string_string_string:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 900,2668
_p_72_plt_System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool
plt_System_Xml_Linq_XElement_CreateDummyNamespace_int__System_Collections_Generic_IEnumerable_1_System_Xml_Linq_XAttribute_bool:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 904,2673
_p_73_plt_System_Xml_XmlWriter_WriteAttributeString_string_string_llvm:
	.no_dead_strip plt_System_Xml_XmlWriter_WriteAttributeString_string_string
plt_System_Xml_XmlWriter_WriteAttributeString_string_string:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 908,2675
_p_74_plt_System_Xml_Linq_XNamespace_Get_string_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_Get_string
plt_System_Xml_Linq_XNamespace_Get_string:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 912,2680
_p_75_plt_System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace
plt_System_Xml_Linq_XElement_GetPrefixOfNamespaceCore_System_Xml_Linq_XNamespace:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 916,2683
_p_76_plt_System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_GetNamespaceOfPrefix_string
plt_System_Xml_Linq_XElement_GetNamespaceOfPrefix_string:
_p_76:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 920,2685
_p_77_plt_string_Format_string_object_llvm:
	.no_dead_strip plt_string_Format_string_object
plt_string_Format_string_object:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 924,2687
_p_78_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 928,2692

.set _p_79_plt_System_Xml_Linq_XText_set_Value_string_llvm, _System_Xml_Linq__System_Xml_Linq_XText_set_Value_string

.set _p_80_plt_System_Xml_Linq_XElement_Save_System_Xml_XmlWriter_llvm, _System_Xml_Linq__System_Xml_Linq_XElement_Save_System_Xml_XmlWriter
_p_81_plt_string_op_Inequality_string_string_llvm:
	.no_dead_strip plt_string_op_Inequality_string_string
plt_string_op_Inequality_string_string:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 940,2702
_p_82_plt_System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator
plt_System_Xml_Linq_XElement__Attributesc__Iterator0_System_Collections_Generic_IEnumerable_System_Xml_Linq_XAttribute_GetEnumerator:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 944,2707
_p_83_plt_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator
plt_System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_System_Collections_Generic_IEnumerable_string_GetEnumerator:
_p_83:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 948,2709
_p_84_plt_System_Xml_XmlConvert_VerifyNCName_string_llvm:
	.no_dead_strip plt_System_Xml_XmlConvert_VerifyNCName_string
plt_System_Xml_XmlConvert_VerifyNCName_string:
_p_84:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 952,2711

.set _p_85_plt_System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName_llvm, _System_Xml_Linq__System_Xml_Linq_XName_op_Inequality_System_Xml_Linq_XName_System_Xml_Linq_XName
_p_86_plt_string_Concat_string_string_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string_string_string
plt_string_Concat_string_string_string_string:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 960,2718
_p_87_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace__ctor
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace__ctor:
_p_87:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 964,2723
_p_88_plt__class_init_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt__class_init_System_Xml_Linq_XNamespace
plt__class_init_System_Xml_Linq_XNamespace:
_p_88:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 968,2734
_p_89_plt_System_Threading_Monitor_Enter_object_bool__llvm:
	.no_dead_strip plt_System_Threading_Monitor_Enter_object_bool_
plt_System_Threading_Monitor_Enter_object_bool_:
_p_89:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 972,2737
_p_90_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_TryGetValue_string_System_Xml_Linq_XNamespace__llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_TryGetValue_string_System_Xml_Linq_XNamespace_
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_TryGetValue_string_System_Xml_Linq_XNamespace_:
_p_90:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 976,2742
_p_91_plt_System_Xml_Linq_XNamespace__ctor_string_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace__ctor_string
plt_System_Xml_Linq_XNamespace__ctor_string:
_p_91:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 980,2753
_p_92_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_set_Item_string_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_set_Item_string_System_Xml_Linq_XNamespace
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XNamespace_set_Item_string_System_Xml_Linq_XNamespace:
_p_92:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 984,2756
_p_93_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_TryGetValue_string_System_Xml_Linq_XName__llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_TryGetValue_string_System_Xml_Linq_XName_
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_TryGetValue_string_System_Xml_Linq_XName_:
_p_93:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 988,2767

.set _p_94_plt_System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace_llvm, _System_Xml_Linq__System_Xml_Linq_XName__ctor_string_System_Xml_Linq_XNamespace
_p_95_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_set_Item_string_System_Xml_Linq_XName_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_set_Item_string_System_Xml_Linq_XName
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName_set_Item_string_System_Xml_Linq_XName:
_p_95:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 996,2780
_p_96_plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName__ctor
plt_System_Collections_Generic_Dictionary_2_string_System_Xml_Linq_XName__ctor:
_p_96:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1000,2791
_p_97_plt_System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace
plt_System_Xml_Linq_XNamespace_op_Inequality_System_Xml_Linq_XNamespace_System_Xml_Linq_XNamespace:
_p_97:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1004,2802
_p_98_plt_System_IO_StringWriter__ctor_llvm:
	.no_dead_strip plt_System_IO_StringWriter__ctor
plt_System_IO_StringWriter__ctor:
_p_98:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1008,2805
_p_99_plt_System_Xml_XmlWriterSettings__ctor_llvm:
	.no_dead_strip plt_System_Xml_XmlWriterSettings__ctor
plt_System_Xml_XmlWriterSettings__ctor:
_p_99:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1012,2810
_p_100_plt_System_Xml_XmlWriter_Create_System_IO_TextWriter_System_Xml_XmlWriterSettings_llvm:
	.no_dead_strip plt_System_Xml_XmlWriter_Create_System_IO_TextWriter_System_Xml_XmlWriterSettings
plt_System_Xml_XmlWriter_Create_System_IO_TextWriter_System_Xml_XmlWriterSettings:
_p_100:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1016,2815
_p_101_plt_System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions
plt_System_Xml_Linq_XElement_LoadCore_System_Xml_XmlReader_System_Xml_Linq_LoadOptions:
_p_101:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1020,2820

.set _p_102_plt_System_Xml_Linq_XProcessingInstruction__ctor_string_string_llvm, _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_string_string

.set _p_103_plt_System_Xml_Linq_XDocumentType__ctor_string_string_string_string_llvm, _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_string_string_string_string
_p_104_plt_System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions
plt_System_Xml_Linq_XNode_ToString_System_Xml_Linq_SaveOptions:
_p_104:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1032,2827
_p_105_plt_System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode
plt_System_Xml_Linq_XNodeReader__ctor_System_Xml_Linq_XNode:
_p_105:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1036,2830
_p_106_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_106:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1040,2833

.set _p_107_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareCore_System_Xml_Linq_XNode_System_Xml_Linq_XNode
_p_108_plt_System_DateTime_get_Now_llvm:
	.no_dead_strip plt_System_DateTime_get_Now
plt_System_DateTime_get_Now:
_p_108:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1048,2862
_p_109_plt_System_DateTime_get_Ticks_llvm:
	.no_dead_strip plt_System_DateTime_get_Ticks
plt_System_DateTime_get_Ticks:
_p_109:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1052,2867
_p_110_plt__jit_icall___emul_lrem_llvm:
	.no_dead_strip plt__jit_icall___emul_lrem
plt__jit_icall___emul_lrem:
_p_110:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1056,2872

.set _p_111_plt_System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_CompareSibling_System_Xml_Linq_XNode_System_Xml_Linq_XNode_System_Xml_Linq_XNodeDocumentOrderComparer_CompareResult

.set _p_112_plt_System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeDocumentOrderComparer_Compare_System_Xml_Linq_XNode_System_Xml_Linq_XNode
_p_113_plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode
plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode:
_p_113:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1068,2892

.set _p_114_plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XAttribute_System_Xml_Linq_XAttribute

.set _p_115_plt_System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XDeclaration_System_Xml_Linq_XDeclaration

.set _p_116_plt_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XDeclaration
_p_117_plt_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode
plt_System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode:
_p_117:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1084,2904

.set _p_118_plt_System_Xml_Linq_XNodeReader_GetCurrentAttribute_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentAttribute
_p_119_plt_System_Xml_NameTable__ctor_llvm:
	.no_dead_strip plt_System_Xml_NameTable__ctor
plt_System_Xml_NameTable__ctor:
_p_119:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1092,2910
_p_120_plt_System_Xml_Linq_XElement_get_IsEmpty_llvm:
	.no_dead_strip plt_System_Xml_Linq_XElement_get_IsEmpty
plt_System_Xml_Linq_XElement_get_IsEmpty:
_p_120:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1096,2915

.set _p_121_plt_System_Xml_Linq_XNodeReader_GetCurrentName_llvm, _System_Xml_Linq__System_Xml_Linq_XNodeReader_GetCurrentName
_p_122_plt_string_Substring_int_int_llvm:
	.no_dead_strip plt_string_Substring_int_int
plt_string_Substring_int_int:
_p_122:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1104,2920
_p_123_plt_System_Xml_Linq_XNodeReader_GetXAttribute_int_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeReader_GetXAttribute_int
plt_System_Xml_Linq_XNodeReader_GetXAttribute_int:
_p_123:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1108,2925
_p_124_plt_System_Xml_Linq_XNodeReader_GetName_int_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeReader_GetName_int
plt_System_Xml_Linq_XNodeReader_GetName_int:
_p_124:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1112,2928
_p_125_plt_string_EndsWith_string_System_StringComparison_llvm:
	.no_dead_strip plt_string_EndsWith_string_System_StringComparison
plt_string_EndsWith_string_System_StringComparison:
_p_125:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1116,2931
_p_126_plt_System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName
plt_System_Xml_Linq_XNodeReader_GetPrefixedName_System_Xml_Linq_XName:
_p_126:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1120,2936
_p_127_plt_string_Concat_string_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string_string
plt_string_Concat_string_string_string:
_p_127:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1124,2939
_p_128_plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs
plt_System_Xml_Linq_XObject_OnChanging_object_System_Xml_Linq_XObjectChangeEventArgs:
_p_128:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1128,2944
_p_129_plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs_llvm:
	.no_dead_strip plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs
plt_System_Xml_Linq_XObject_OnChanged_object_System_Xml_Linq_XObjectChangeEventArgs:
_p_129:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1132,2947
_p_130_plt_System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool
plt_System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool:
_p_130:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1136,2950
_p_131_plt_System_Xml_Linq_XUtil_Clone_object_llvm:
	.no_dead_strip plt_System_Xml_Linq_XUtil_Clone_object
plt_System_Xml_Linq_XUtil_Clone_object:
_p_131:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1140,2962

.set _p_132_plt_System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType_llvm, _System_Xml_Linq__System_Xml_Linq_XDocumentType__ctor_System_Xml_Linq_XDocumentType

.set _p_133_plt_System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration_llvm, _System_Xml_Linq__System_Xml_Linq_XDeclaration__ctor_System_Xml_Linq_XDeclaration

.set _p_134_plt_System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction_llvm, _System_Xml_Linq__System_Xml_Linq_XProcessingInstruction__ctor_System_Xml_Linq_XProcessingInstruction

.set _p_135_plt_System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement_llvm, _System_Xml_Linq__System_Xml_Linq_XElement__ctor_System_Xml_Linq_XElement

.set _p_136_plt_System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute_llvm, _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
_p_137_plt_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator_llvm:
	.no_dead_strip plt_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator
plt_System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_System_Collections_Generic_IEnumerable_object_GetEnumerator:
_p_137:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1164,2976
_p_138_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_138:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1168,2979
_p_139_plt_System_Linq_Check_SourceAndPredicate_object_object_llvm:
	.no_dead_strip plt_System_Linq_Check_SourceAndPredicate_object_object
plt_System_Linq_Check_SourceAndPredicate_object_object:
_p_139:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1172,3017
_p_140_plt_System_Array_InternalEnumerator_1_char_get_Current_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_char_get_Current
plt_System_Array_InternalEnumerator_1_char_get_Current:
_p_140:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1176,3022
_p_141_plt_System_Array_InternalArray__get_Item_char_int_llvm:
	.no_dead_strip plt_System_Array_InternalArray__get_Item_char_int
plt_System_Array_InternalArray__get_Item_char_int:
_p_141:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1180,3042
_p_142_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_142:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1184,3063
_p_143_plt_System_Array_InternalEnumerator_1_char__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_char__ctor_System_Array
plt_System_Array_InternalEnumerator_1_char__ctor_System_Array:
_p_143:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1188,3092
_p_144_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_144:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1192,3112
_p_145_plt_System_Threading_Monitor_Exit_object_llvm:
	.no_dead_strip plt_System_Threading_Monitor_Exit_object
plt_System_Threading_Monitor_Exit_object:
_p_145:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1196,3147
_p_146_plt_System_Xml_XmlConvert_ToString_System_DateTimeOffset_llvm:
	.no_dead_strip plt_System_Xml_XmlConvert_ToString_System_DateTimeOffset
plt_System_Xml_XmlConvert_ToString_System_DateTimeOffset:
_p_146:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1200,3152
_p_147_plt_System_Xml_XmlConvert_ToString_System_TimeSpan_llvm:
	.no_dead_strip plt_System_Xml_XmlConvert_ToString_System_TimeSpan
plt_System_Xml_XmlConvert_ToString_System_TimeSpan:
_p_147:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1204,3157
_p_148_plt_string_ToLower_llvm:
	.no_dead_strip plt_string_ToLower
plt_string_ToLower:
_p_148:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1208,3162
_p_149_plt_single_ToString_string_System_IFormatProvider_llvm:
	.no_dead_strip plt_single_ToString_string_System_IFormatProvider
plt_single_ToString_string_System_IFormatProvider:
_p_149:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1212,3167
_p_150_plt_double_ToString_string_System_IFormatProvider_llvm:
	.no_dead_strip plt_double_ToString_string_System_IFormatProvider
plt_double_ToString_string_System_IFormatProvider:
_p_150:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1216,3172
_p_151_plt_System_Decimal_ToString_System_IFormatProvider_llvm:
	.no_dead_strip plt_System_Decimal_ToString_System_IFormatProvider
plt_System_Decimal_ToString_System_IFormatProvider:
_p_151:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1220,3177
_p_152_plt_System_Xml_XmlConvert_ToString_System_DateTime_System_Xml_XmlDateTimeSerializationMode_llvm:
	.no_dead_strip plt_System_Xml_XmlConvert_ToString_System_DateTime_System_Xml_XmlDateTimeSerializationMode
plt_System_Xml_XmlConvert_ToString_System_DateTime_System_Xml_XmlDateTimeSerializationMode:
_p_152:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1224,3182
_p_153_plt_System_Type_GetTypeCode_System_Type_llvm:
	.no_dead_strip plt_System_Type_GetTypeCode_System_Type
plt_System_Type_GetTypeCode_System_Type:
_p_153:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1228,3187
_p_154_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_154:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1232,3192
_p_155_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_155:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1236,3247
_p_156_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_156:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1240,3255
_p_157_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_157:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_Xml_Linq_got - . + 1244,3274
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 4
	.asciz "System.Xml.Linq"
	.asciz "143F1E95-86A8-453C-A5B0-C7F92B936B94"
	.asciz ""
	.asciz "31bf3856ad364e35"
	.align 3

	.long 1,2,0,5,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System.Xml"
	.asciz "4D3147BC-C2E3-4CDA-88A7-E0386EE9E899"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "143F1E95-86A8-453C-A5B0-C7F92B936B94"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "System.Xml.Linq"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_System_Xml_Linq_got
	.align 2
	.long _System_Xml_Linq__System_Xml_Linq_XAttribute__ctor_System_Xml_Linq_XAttribute
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 155,1252,158,280,11,387000831,0,5761
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_System_Xml_Linq_info
	.align 2
_mono_aot_module_System_Xml_Linq_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,1,4,0,1,4,0,1,4,1,4,1,4,0,1,4,0,1,4,0,1,4,0,1,4,0,1,4,0,1,4,0
	.byte 1,4,5,5,5,5,5,5,1,4,0,1,4,11,6,16,15,14,13,12,11,10,9,8,7,1,4,5,17,20,19,18
	.byte 8,0,0,0,0,0,0,0,2,6,21,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,12,27,26,25,25,25
	.byte 25,22,24,23,128,134,128,134,128,133,0,4,28,29,23,128,133,0,1,26,0,0,0,1,30,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,1,30,0,0,0,0,0,0,0,0,0,0,0,9,31,16,35,16,34,16,33,32
	.byte 36,0,0,0,0,0,0,0,6,37,38,39,39,23,128,133,0,0,0,6,37,38,5,5,23,128,133,0,1,40,0,2
	.byte 41,128,133,0,1,42,0,4,46,45,44,43,0,0,0,5,47,37,38,23,128,133,0,0,0,6,27,27,48,48,39,5
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,12,0,1,12,0,1,12,0,1,12,0,1,12,0,1
	.byte 12,0,1,12,0,1,12,2,23,37,1,12,0,1,12,0,1,12,0,1,12,1,49,1,12,3,4,4,50,1,12,0
	.byte 1,12,1,51,1,12,1,52,1,12,5,53,54,55,23,128,133,1,12,17,56,62,66,65,59,53,63,64,64,63,62,61
	.byte 60,59,58,57,64,1,12,10,54,55,4,4,23,128,133,37,38,23,128,133,1,12,6,54,55,4,23,128,133,5,1,12
	.byte 4,67,68,23,128,133,1,12,1,69,1,12,13,71,39,70,52,52,54,55,23,128,133,27,48,48,27,1,12,0,1,12
	.byte 0,1,12,0,1,12,3,74,73,72,1,12,1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,49,0
	.byte 0,0,0,0,0,0,0,0,0,0,8,128,136,54,128,135,55,53,23,128,133,5,0,2,128,137,128,133,0,0,0,0
	.byte 0,1,69,0,0,0,0,0,0,0,0,0,1,75,0,0,0,0,0,0,0,0,0,0,0,2,77,76,1,17,8,78
	.byte 83,82,81,80,53,79,84,1,17,1,80,1,17,1,84,1,17,0,1,17,0,1,17,4,79,79,85,79,1,17,2,87
	.byte 86,1,17,1,88,1,17,0,1,17,0,1,17,0,1,17,0,1,17,0,1,18,0,1,18,0,1,18,0,1,18,0
	.byte 1,18,2,90,89,1,18,8,98,97,96,95,94,93,92,91,1,18,0,1,18,1,99,1,18,4,103,102,101,100,0,0
	.byte 0,0,0,0,0,0,0,2,26,26,0,0,0,38,128,139,128,138,71,71,37,37,38,23,38,23,128,133,23,5,5,54
	.byte 54,55,23,55,23,128,133,23,37,37,38,23,38,23,128,133,23,105,105,104,104,39,39,48,48,0,0,0,0,0,2,26
	.byte 26,0,0,0,20,128,141,128,140,71,37,38,23,128,133,5,54,55,23,128,133,37,38,23,128,133,105,104,39,48,0,1
	.byte 26,0,0,0,0,0,1,106,0,3,5,39,71,0,1,53,0,2,5,5,0,0,0,2,5,5,0,2,5,5,0,4
	.byte 53,27,27,75,0,3,4,53,75,0,0,0,0,0,6,5,5,53,53,53,75,0,0,0,11,39,97,96,71,44,43,105
	.byte 104,48,53,53,0,1,107,0,0,0,5,5,54,55,23,128,133,0,0,0,16,5,45,44,45,45,43,44,45,39,96,97
	.byte 97,39,108,104,71,0,0,0,2,5,5,0,0,0,0,0,0,0,3,27,75,75,0,4,5,109,53,5,0,2,27,75
	.byte 0,0,0,0,0,6,5,71,71,71,5,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,71,71,71,71
	.byte 0,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,112,112,111,110,107,0,1,113,0,1,113,0,1
	.byte 114,0,1,114,0,1,115,0,1,115,0,2,5,5,0,2,5,5,1,25,0,1,25,8,116,116,114,116,117,116,113,115
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,118,123,122,121,120,119,118,118
	.byte 0,0,0,15,128,150,27,128,149,128,148,128,144,128,147,128,145,128,144,128,146,128,145,128,144,128,143,128,143,128,142,128
	.byte 142,0,1,124,0,6,26,27,27,92,92,52,0,0,0,26,27,52,52,51,5,5,50,125,125,93,105,105,95,104,104,94
	.byte 70,70,46,39,39,98,48,48,92,27,0,0,0,0,0,0,0,19,128,153,26,27,25,25,25,25,22,128,152,24,28,128
	.byte 151,29,23,128,133,23,128,134,128,134,128,133,0,5,128,154,128,133,128,134,128,134,128,133,0,0,0,0,0,1,124,0
	.byte 0,0,1,126,0,1,126,0,1,126,0,1,126,0,1,126,0,4,127,128,128,23,128,133,0,2,128,130,128,129,0,0
	.byte 0,1,128,131,0,0,0,0,0,0,0,2,128,129,128,129,0,1,128,132,5,30,0,1,255,255,255,255,255,193,0,13
	.byte 43,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,132,84,255,252,0,0,0,1,1,3,219,0,0,7
	.byte 4,2,119,1,2,2,130,146,1,1,17,4,2,97,1,3,2,130,146,1,1,17,7,132,128,255,252,0,0,0,1,1
	.byte 7,132,139,4,2,119,1,2,2,130,146,1,1,16,4,2,97,1,3,2,130,146,1,1,16,7,132,163,255,252,0,0
	.byte 0,1,1,7,132,174,255,252,0,0,0,1,1,3,219,0,0,16,255,252,0,0,0,1,1,3,219,0,0,17,255,254
	.byte 0,0,0,0,255,43,0,0,2,4,2,130,11,1,1,2,130,30,1,255,253,0,0,0,7,132,233,1,198,0,13,120
	.byte 1,2,130,30,1,0,255,253,0,0,0,7,132,233,1,198,0,13,121,1,2,130,30,1,0,255,253,0,0,0,7,132
	.byte 233,1,198,0,13,122,1,2,130,30,1,0,255,253,0,0,0,7,132,233,1,198,0,13,123,1,2,130,30,1,0,255
	.byte 253,0,0,0,7,132,233,1,198,0,13,124,1,2,130,30,1,0,255,253,0,0,0,7,132,233,1,198,0,13,125,1
	.byte 2,130,30,1,0,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,2,130,30,1,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,54,0,1,2,130,30,1,12,0,39,42,47,17,0,23,11,1,12,14,2,129,210,1,17,0,47
	.byte 16,1,4,15,17,0,89,17,0,57,17,0,69,17,0,79,17,0,127,17,0,115,17,0,103,17,0,53,14,6,1,1
	.byte 4,16,1,4,10,14,6,1,2,130,30,1,29,0,196,0,0,112,0,17,0,128,139,6,193,0,5,6,6,193,0,5
	.byte 8,6,193,0,5,7,23,2,128,151,1,11,1,18,11,2,130,146,1,6,255,254,0,0,0,0,202,0,0,21,6,255
	.byte 254,0,0,0,0,202,0,0,22,14,1,8,14,6,1,2,130,146,1,17,0,128,195,17,0,129,25,17,0,128,253,17
	.byte 0,128,229,17,0,128,207,6,255,254,0,0,0,0,202,0,0,29,6,255,254,0,0,0,0,202,0,0,25,11,1,11
	.byte 14,2,128,228,1,14,2,129,116,3,14,1,10,17,0,129,31,17,0,129,47,17,0,129,65,14,1,9,17,0,130,5
	.byte 11,1,27,14,1,13,14,1,12,14,1,4,11,1,4,16,2,130,146,1,136,199,6,255,254,0,0,0,0,202,0,0
	.byte 54,6,255,254,0,0,0,0,202,0,0,55,14,1,14,17,0,132,83,14,2,130,90,1,14,3,219,0,0,7,6,105
	.byte 50,105,30,3,219,0,0,7,34,255,254,0,0,0,0,255,43,0,0,1,16,1,12,38,6,95,50,95,6,255,254,0
	.byte 0,0,0,202,0,0,63,6,255,254,0,0,0,0,202,0,0,64,14,1,15,11,1,9,11,1,10,14,3,219,0,0
	.byte 10,4,2,130,50,1,1,1,12,16,7,134,225,135,229,16,1,12,33,11,1,16,17,0,132,233,17,0,132,237,14,3
	.byte 219,0,0,12,16,1,17,59,16,1,17,56,17,0,132,241,16,1,17,57,17,0,133,59,16,1,17,58,14,1,17,14
	.byte 1,16,14,3,219,0,0,13,11,1,17,14,2,128,232,1,14,2,129,137,3,14,2,129,103,3,14,1,27,14,1,5
	.byte 14,1,26,14,1,6,17,0,133,147,17,0,133,161,14,1,11,14,1,22,14,1,21,16,1,18,62,14,1,19,16,1
	.byte 18,63,11,1,26,11,1,6,14,2,129,58,3,6,195,0,10,142,17,0,134,49,17,0,134,57,6,195,0,10,140,6
	.byte 195,0,10,141,23,2,129,56,3,16,1,25,93,16,1,25,95,16,1,25,96,14,1,25,16,1,25,94,16,1,27,101
	.byte 34,255,254,0,0,0,0,255,43,0,0,2,14,3,219,0,0,17,6,128,233,50,128,233,30,3,219,0,0,17,14,1
	.byte 29,11,1,5,33,4,2,111,1,1,2,130,30,1,6,255,253,0,0,0,7,135,197,1,198,0,3,125,1,2,130,30
	.byte 1,0,4,2,112,1,1,2,130,30,1,6,255,253,0,0,0,7,135,226,1,198,0,3,126,1,2,130,30,1,0,14
	.byte 7,132,233,14,2,130,30,1,34,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130,30,1,34,255,253
	.byte 0,0,0,2,130,10,1,1,198,0,13,56,0,1,2,130,30,1,6,193,0,17,58,23,2,130,83,1,8,1,130,88
	.byte 8,2,104,128,204,8,2,128,184,104,8,3,131,192,139,100,138,200,8,4,136,224,136,72,129,52,137,180,8,3,130,16
	.byte 134,116,134,32,8,4,132,188,132,92,128,204,133,64,11,2,130,41,1,11,2,130,155,1,16,2,128,184,1,130,35,17
	.byte 0,134,167,11,2,130,144,1,11,2,130,49,1,11,2,130,44,1,11,2,130,38,1,8,6,130,32,129,184,129,68,128
	.byte 236,128,164,128,176,8,1,132,56,8,1,131,128,8,5,112,134,120,134,120,130,192,134,120,8,5,129,232,129,232,129,232
	.byte 112,129,232,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,30,109,111,110,111,95,99,114
	.byte 101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,49,0,7,25,109,111,110,111,95,97
	.byte 114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,122,3,12,3,127,3,128,133,3,193,0
	.byte 19,41,3,126,3,128,234,3,128,213,3,128,214,3,128,215,3,128,216,7,20,109,111,110,111,95,111,98,106,101,99,116
	.byte 95,110,101,119,95,102,97,115,116,0,3,193,0,11,190,3,193,0,11,205,3,193,0,19,79,3,193,0,11,212,7,42
	.byte 108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95
	.byte 116,114,97,109,112,111,108,105,110,101,0,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99
	.byte 105,102,105,99,0,3,193,0,19,181,3,193,0,19,131,3,26,3,128,235,3,128,236,3,128,211,3,28,3,128,212,3
	.byte 128,237,3,128,143,3,27,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101
	.byte 112,116,105,111,110,95,48,0,3,39,3,193,0,12,100,3,193,0,19,137,3,30,3,193,0,7,97,3,53,3,195,0
	.byte 13,35,3,195,0,12,248,3,54,3,55,3,128,210,3,31,3,51,3,40,3,59,3,49,3,56,3,128,200,3,79,3
	.byte 120,3,69,3,82,3,128,129,3,128,131,3,78,3,10,3,2,3,83,3,11,3,3,3,9,3,128,135,3,88,7,27
	.byte 109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,255,254,0
	.byte 0,0,0,255,43,0,0,1,3,117,3,84,3,195,0,14,158,3,85,3,195,0,14,156,3,128,130,3,89,3,87,3
	.byte 193,0,19,125,3,193,0,19,133,3,128,231,3,81,3,193,0,19,42,3,103,3,113,3,195,0,11,72,3,123,3,193
	.byte 0,19,135,3,255,254,0,0,0,0,202,0,0,78,15,1,17,3,193,0,12,116,3,255,254,0,0,0,0,202,0,0
	.byte 80,3,128,128,3,255,254,0,0,0,0,202,0,0,81,3,255,254,0,0,0,0,202,0,0,84,3,114,3,255,254,0
	.byte 0,0,0,202,0,0,85,3,255,254,0,0,0,0,202,0,0,83,3,128,134,3,193,0,7,155,3,195,0,14,179,3
	.byte 195,0,14,148,3,80,3,128,221,3,60,3,128,142,3,128,163,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110
	.byte 101,119,95,112,116,114,102,114,101,101,0,3,128,150,3,193,0,15,74,3,193,0,15,75,7,11,95,95,101,109,117,108
	.byte 95,108,114,101,109,0,3,128,151,3,128,149,3,128,154,3,128,155,3,128,156,3,128,158,3,128,159,3,128,178,3,195
	.byte 0,10,145,3,75,3,128,180,3,193,0,19,54,3,128,179,3,128,181,3,193,0,19,111,3,128,188,3,193,0,19,134
	.byte 3,128,217,3,128,218,3,255,254,0,0,0,0,255,43,0,0,2,3,128,238,3,61,3,41,3,128,222,3,68,3,1
	.byte 3,128,246,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104
	.byte 101,99,107,112,111,105,110,116,0,3,194,0,0,171,3,255,253,0,0,0,7,132,233,1,198,0,13,122,1,2,130,30
	.byte 1,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130,30,1,7,26,109,111,110,111,95,104,101
	.byte 108,112,101,114,95,108,100,115,116,114,95,109,115,99,111,114,108,105,98,0,3,255,253,0,0,0,7,132,233,1,198,0
	.byte 13,121,1,2,130,30,1,0,7,32,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98
	.byte 95,101,120,99,101,112,116,105,111,110,0,3,193,0,12,112,3,195,0,11,75,3,195,0,11,59,3,193,0,19,117,3
	.byte 193,0,19,13,3,193,0,16,89,3,193,0,16,42,3,195,0,11,65,3,193,0,20,107,7,24,109,111,110,111,95,111
	.byte 98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,255,253,0,0,0,2,130,10,1,1,198,0,13
	.byte 43,0,1,7,132,84,4,2,130,11,1,1,7,132,84,35,140,147,150,5,7,140,166,3,255,253,0,0,0,7,140,166
	.byte 1,198,0,13,121,1,7,132,84,0,35,140,147,192,0,92,41,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0
	.byte 1,7,132,84,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,0,1,2,0,131,148,130,24,130,144,130,148,0,6,32
	.byte 1,2,0,129,116,96,129,40,129,44,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,38,62,1,1,2,0,129,140,80,129,72,129,76,0,4,129,12,0,16,0,0,38
	.byte 62,1,1,2,0,129,140,80,129,72,129,76,0,4,129,12,0,16,0,0,6,92,1,2,0,128,216,128,128,128,148,128
	.byte 152,0,16,0,0,16,0,0,16,0,0,6,116,1,2,0,129,136,128,196,129,68,129,72,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,6,128,146,1,2,0,129,244,128,192,129,176,129,180,0,16,0,0,6,128,176,2,2,0,131
	.byte 116,129,12,131,40,131,44,2,0,132,108,131,164,132,32,132,36,0,38,128,208,1,1,2,0,129,168,92,129,100,129,104
	.byte 0,4,129,40,0,38,128,240,1,1,2,0,129,48,88,128,236,128,240,0,4,128,176,0,16,0,0,6,129,14,1,2
	.byte 0,130,252,129,248,130,184,130,188,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 38,129,45,1,1,2,0,130,244,128,204,130,148,130,152,0,4,130,84,0,6,129,77,1,2,0,128,184,104,108,112,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,129,101,1,2,0,129
	.byte 28,84,128,248,128,252,0,6,129,127,1,2,0,129,24,116,128,244,128,248,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,38,129,155,6,3,2,0,131,136,130,76,131
	.byte 68,131,72,2,0,134,44,132,240,133,232,133,236,2,0,136,16,134,212,135,204,135,208,0,4,130,184,0,4,131,8,1
	.byte 4,133,92,1,4,133,172,2,4,135,64,2,4,135,144,0,16,0,0,16,0,0,16,0,0,16,0,0,6,129,188,3
	.byte 2,0,130,16,129,76,129,204,129,208,2,0,131,96,130,156,131,28,131,32,2,0,132,92,131,152,132,24,132,28,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,38,129,45,1,1,2,0,129
	.byte 172,128,224,129,104,129,108,0,4,129,44,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,6,129,221,1,2,0,128,204,68,128,164,128,168,0,6,129,221,1,2,0,128,212
	.byte 72,128,172,128,176,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,2,129,247,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,38,129,188,2,2,2
	.byte 0,132,212,131,128,132,116,132,120,2,0,134,72,130,192,133,16,133,20,0,8,132,48,1,4,132,52,0,6,130,20,2
	.byte 2,0,128,192,112,116,120,2,0,129,232,112,128,196,128,200,0,16,0,0,16,0,0,16,0,0,3,130,44,0,1,11
	.byte 4,19,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,132,84,1,0,1,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,38,130,68,1,1,2,0,129,52,88,128,240,128,244,0,4,128,180,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,23,128,144,12,0
	.byte 0,4,193,0,16,157,193,0,16,172,193,0,18,174,193,0,16,170,193,0,16,156,193,0,16,128,193,0,16,129,193,0
	.byte 16,130,193,0,16,131,193,0,16,132,193,0,16,133,193,0,16,134,193,0,16,135,193,0,16,136,193,0,16,137,193,0
	.byte 16,138,193,0,16,139,193,0,16,158,193,0,16,140,193,0,16,141,193,0,16,142,193,0,16,143,193,0,16,160,23,128
	.byte 144,12,0,0,4,193,0,16,157,193,0,16,172,193,0,18,174,193,0,16,170,193,0,16,156,193,0,16,128,193,0,16
	.byte 129,193,0,16,130,193,0,16,131,193,0,16,132,193,0,16,133,193,0,16,134,193,0,16,135,193,0,16,136,193,0,16
	.byte 137,193,0,16,138,193,0,16,139,193,0,16,158,193,0,16,140,193,0,16,141,193,0,16,142,193,0,16,143,193,0,16
	.byte 160,8,128,228,14,48,8,0,4,13,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,7,9,128,160
	.byte 44,0,0,4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,17,18,9,128,160,44,0,0
	.byte 4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,21,23,10,128,168,48,0,0,4,128,144
	.byte 193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,0,0,29,11,128,160,32,0,0,4,193,0,18,178
	.byte 193,0,18,175,193,0,18,174,193,0,18,172,38,39,33,35,37,36,32,4,128,160,20,0,0,4,45,193,0,18,175,193
	.byte 0,18,174,193,0,18,172,10,128,160,52,0,0,4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196
	.byte 128,209,50,57,58,9,128,160,56,0,0,4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209
	.byte 66,67,13,128,236,94,64,8,0,4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,77,86
	.byte 90,93,92,91,11,128,160,32,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,102,103,97,99,101
	.byte 100,96,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,11,128,160,40,0,0,4
	.byte 193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,112,113,107,109,111,110,106,5,128,160,16,0,0,4,124,121
	.byte 193,0,18,174,118,119,4,128,228,125,16,16,0,4,128,137,128,136,193,0,18,174,128,132,9,128,228,128,147,40,8,0
	.byte 4,128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,0,0,6,128,152,8,0,0,1,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,128,152,128,149,23,128,144,12,0,0,4,193,0,16,157,193,0,16
	.byte 172,193,0,18,174,193,0,16,170,193,0,16,156,193,0,16,128,193,0,16,129,193,0,16,130,193,0,16,131,193,0,16
	.byte 132,193,0,16,133,193,0,16,134,193,0,16,135,193,0,16,136,193,0,16,137,193,0,16,138,193,0,16,139,193,0,16
	.byte 158,193,0,16,140,193,0,16,141,193,0,16,142,193,0,16,143,193,0,16,160,8,128,144,8,0,0,1,193,0,18,178
	.byte 193,0,18,175,193,0,18,174,193,0,18,172,128,157,128,160,128,154,128,159,55,128,160,48,0,0,4,193,0,18,178,193
	.byte 0,18,175,193,0,18,174,193,0,18,172,195,0,12,255,195,0,13,28,128,194,195,0,13,26,195,0,13,25,195,0,13
	.byte 21,195,0,13,20,195,0,13,19,195,0,13,18,195,0,13,17,195,0,13,16,128,193,128,192,128,186,128,185,128,184,195
	.byte 0,13,10,128,189,128,187,195,0,13,6,128,183,195,0,13,4,195,0,13,3,128,191,128,190,195,0,13,0,128,182,195
	.byte 0,12,243,128,176,195,0,12,241,195,0,12,240,128,175,128,174,128,173,128,172,128,171,195,0,12,234,128,170,195,0,12
	.byte 232,195,0,12,231,195,0,12,230,128,169,128,168,128,167,128,166,195,0,12,225,128,165,128,164,128,161,128,162,128,177,8
	.byte 128,160,32,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,0,23,128,144
	.byte 12,0,0,4,193,0,16,157,193,0,16,172,193,0,18,174,193,0,16,170,193,0,16,156,193,0,16,128,193,0,16,129
	.byte 193,0,16,130,193,0,16,131,193,0,16,132,193,0,16,133,193,0,16,134,193,0,16,135,193,0,16,136,193,0,16,137
	.byte 193,0,16,138,193,0,16,139,193,0,16,158,193,0,16,140,193,0,16,141,193,0,16,142,193,0,16,143,193,0,16,160
	.byte 4,128,196,128,220,12,16,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,9,128,160,48,0,0,4
	.byte 128,144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,128,224,128,226,9,128,224,44,4,0,4,128
	.byte 144,193,0,18,175,193,0,18,174,193,0,18,172,128,195,128,196,128,209,128,229,128,232,4,128,152,8,0,0,1,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,11,128,160,48,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,128,245,128,246,128,240,128,242,128,244,128,243,128,239,4,128,136,8,16,0,1,193,0,18,178,193,0
	.byte 18,175,193,0,18,174,193,0,18,172,4,128,144,24,0,1,1,193,0,21,37,193,0,21,36,193,0,18,174,193,0,21
	.byte 34,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_3:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_5:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_4:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM10=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM12=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_11:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM15=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM16=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM16
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM17=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM18=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_10:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM19=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM20=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM20
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM21=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM22=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_9:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM23=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM23
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM24=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM25=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM26=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_13:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM27=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM28=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM29=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM29
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM30=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM30
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM31=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_12:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM32=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM33=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM34=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM35=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM35
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM36=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM37=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_8:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM38=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM39=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM40=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM41=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM41
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM42=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM43=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM44=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM45=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM46=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM47=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM48=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM49=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM50=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_7:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM51=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM52=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM53=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM54=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM55=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM56=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_6:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM57=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM58=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM59=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM60=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_2:

	.byte 5
	.asciz "System_Xml_Linq_XObject"

	.byte 32,16
LDIFF_SYM61=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2,35,0,6
	.asciz "owner"

LDIFF_SYM62=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,8,6
	.asciz "baseuri"

LDIFF_SYM63=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM63
	.byte 2,35,12,6
	.asciz "line"

LDIFF_SYM64=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,24,6
	.asciz "column"

LDIFF_SYM65=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,28,6
	.asciz "Changing"

LDIFF_SYM66=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,16,6
	.asciz "Changed"

LDIFF_SYM67=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,20,0,7
	.asciz "System_Xml_Linq_XObject"

LDIFF_SYM68=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM69=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM70=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_1:

	.byte 5
	.asciz "System_Xml_Linq_XNode"

	.byte 40,16
LDIFF_SYM71=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,0,6
	.asciz "previous"

LDIFF_SYM72=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,32,6
	.asciz "next"

LDIFF_SYM73=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,36,0,7
	.asciz "System_Xml_Linq_XNode"

LDIFF_SYM74=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM75=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM76=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_0:

	.byte 5
	.asciz "System_Xml_Linq_XContainer"

	.byte 48,16
LDIFF_SYM77=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,0,6
	.asciz "first"

LDIFF_SYM78=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,40,6
	.asciz "last"

LDIFF_SYM79=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,44,0,7
	.asciz "System_Xml_Linq_XContainer"

LDIFF_SYM80=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM81=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM82=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_14:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM83=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM84=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM85=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM85
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM86=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM86
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM87=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_15:

	.byte 17
	.asciz "System_Collections_IEnumerator"

	.byte 8,7
	.asciz "System_Collections_IEnumerator"

LDIFF_SYM88=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM89=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM90=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_16:

	.byte 17
	.asciz "System_IDisposable"

	.byte 8,7
	.asciz "System_IDisposable"

LDIFF_SYM91=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM91
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM92=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM93=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2
	.asciz "System.Xml.Linq.XContainer:CheckChildType"
	.long _System_Xml_Linq_XContainer_CheckChildType_object_bool_0
	.long Lme_19

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM94=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 1,86,3
	.asciz "o"

LDIFF_SYM95=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 1,90,3
	.asciz "addFirst"

LDIFF_SYM96=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 3,123,204,0,11
	.asciz "oc"

LDIFF_SYM97=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 1,85,11
	.asciz ""

LDIFF_SYM98=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM99=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM100=Lfde0_end - Lfde0_start
	.long LDIFF_SYM100
Lfde0_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XContainer_CheckChildType_object_bool_0

LDIFF_SYM101=Lme_19 - _System_Xml_Linq_XContainer_CheckChildType_object_bool_0
	.long LDIFF_SYM101
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,112,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_17:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM102=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM103=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM103
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM104=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2
	.asciz "System.Xml.Linq.XContainer:Add"
	.long _System_Xml_Linq_XContainer_Add_object_0
	.long Lme_1a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM105=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 1,86,3
	.asciz "content"

LDIFF_SYM106=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,123,24,11
	.asciz "o"

LDIFF_SYM107=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 1,85,11
	.asciz ""

LDIFF_SYM108=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2,123,8,11
	.asciz "node"

LDIFF_SYM109=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 1,84,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM110=Lfde1_end - Lfde1_start
	.long LDIFF_SYM110
Lfde1_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XContainer_Add_object_0

LDIFF_SYM111=Lme_1a - _System_Xml_Linq_XContainer_Add_object_0
	.long LDIFF_SYM111
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_19:

	.byte 5
	.asciz "System_Xml_Linq_XDeclaration"

	.byte 20,16
LDIFF_SYM112=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,35,0,6
	.asciz "encoding"

LDIFF_SYM113=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM113
	.byte 2,35,8,6
	.asciz "standalone"

LDIFF_SYM114=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 2,35,12,6
	.asciz "version"

LDIFF_SYM115=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2,35,16,0,7
	.asciz "System_Xml_Linq_XDeclaration"

LDIFF_SYM116=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM117=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM118=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_18:

	.byte 5
	.asciz "System_Xml_Linq_XDocument"

	.byte 52,16
LDIFF_SYM119=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,0,6
	.asciz "xmldecl"

LDIFF_SYM120=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 2,35,48,0,7
	.asciz "System_Xml_Linq_XDocument"

LDIFF_SYM121=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM122=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM123=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM123
LTDIE_20:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM124=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM125=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM126=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_21:

	.byte 5
	.asciz "System_Xml_Linq_XDocumentType"

	.byte 56,16
LDIFF_SYM127=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,0,6
	.asciz "name"

LDIFF_SYM128=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM128
	.byte 2,35,40,6
	.asciz "pubid"

LDIFF_SYM129=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,44,6
	.asciz "sysid"

LDIFF_SYM130=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,48,6
	.asciz "intSubset"

LDIFF_SYM131=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,52,0,7
	.asciz "System_Xml_Linq_XDocumentType"

LDIFF_SYM132=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM133=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM134=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2
	.asciz "System.Xml.Linq.XDocument:get_DocumentType"
	.long _System_Xml_Linq_XDocument_get_DocumentType_0
	.long Lme_30

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM135=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 1,90,11
	.asciz "o"

LDIFF_SYM136=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM136
	.byte 1,86,11
	.asciz ""

LDIFF_SYM137=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM137
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM138=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM139=Lfde2_end - Lfde2_start
	.long LDIFF_SYM139
Lfde2_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XDocument_get_DocumentType_0

LDIFF_SYM140=Lme_30 - _System_Xml_Linq_XDocument_get_DocumentType_0
	.long LDIFF_SYM140
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_26:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM141=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM142=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM143=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_25:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM144=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM144
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM145=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM146=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM146
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM147=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM147
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM148=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM148
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM149=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM150=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM151=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM152=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM152
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM153=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM154=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM154
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM155=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM156=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM157=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM157
LTDIE_24:

	.byte 5
	.asciz "System_Xml_Linq_XNamespace"

	.byte 16,16
LDIFF_SYM158=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM158
	.byte 2,35,0,6
	.asciz "uri"

LDIFF_SYM159=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,8,6
	.asciz "table"

LDIFF_SYM160=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,12,0,7
	.asciz "System_Xml_Linq_XNamespace"

LDIFF_SYM161=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM162=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM163=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_23:

	.byte 5
	.asciz "System_Xml_Linq_XName"

	.byte 16,16
LDIFF_SYM164=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,0,6
	.asciz "local"

LDIFF_SYM165=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 2,35,8,6
	.asciz "ns"

LDIFF_SYM166=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,12,0,7
	.asciz "System_Xml_Linq_XName"

LDIFF_SYM167=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM168=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM169=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_27:

	.byte 5
	.asciz "System_Xml_Linq_XAttribute"

	.byte 48,16
LDIFF_SYM170=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,0,6
	.asciz "name"

LDIFF_SYM171=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,32,6
	.asciz "value"

LDIFF_SYM172=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,36,6
	.asciz "next"

LDIFF_SYM173=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,40,6
	.asciz "previous"

LDIFF_SYM174=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,44,0,7
	.asciz "System_Xml_Linq_XAttribute"

LDIFF_SYM175=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM176=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM176
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM177=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_22:

	.byte 5
	.asciz "System_Xml_Linq_XElement"

	.byte 64,16
LDIFF_SYM178=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,0,6
	.asciz "name"

LDIFF_SYM179=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,48,6
	.asciz "attr_first"

LDIFF_SYM180=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,52,6
	.asciz "attr_last"

LDIFF_SYM181=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,56,6
	.asciz "explicit_is_empty"

LDIFF_SYM182=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,60,0,7
	.asciz "System_Xml_Linq_XElement"

LDIFF_SYM183=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM184=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM184
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM185=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2
	.asciz "System.Xml.Linq.XDocument:get_Root"
	.long _System_Xml_Linq_XDocument_get_Root_0
	.long Lme_32

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM186=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 1,90,11
	.asciz "o"

LDIFF_SYM187=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 1,86,11
	.asciz ""

LDIFF_SYM188=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM189=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM190=Lfde3_end - Lfde3_start
	.long LDIFF_SYM190
Lfde3_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XDocument_get_Root_0

LDIFF_SYM191=Lme_32 - _System_Xml_Linq_XDocument_get_Root_0
	.long LDIFF_SYM191
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_28:

	.byte 5
	.asciz "System_IO_TextReader"

	.byte 8,16
LDIFF_SYM192=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,0,0,7
	.asciz "System_IO_TextReader"

LDIFF_SYM193=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM194=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM195=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_29:

	.byte 8
	.asciz "System_Xml_Linq_LoadOptions"

	.byte 4
LDIFF_SYM196=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "PreserveWhitespace"

	.byte 1,9
	.asciz "SetBaseUri"

	.byte 2,9
	.asciz "SetLineInfo"

	.byte 4,0,7
	.asciz "System_Xml_Linq_LoadOptions"

LDIFF_SYM197=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM198=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM199=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_31:

	.byte 8
	.asciz "System_Xml_ConformanceLevel"

	.byte 4
LDIFF_SYM200=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 9
	.asciz "Auto"

	.byte 0,9
	.asciz "Fragment"

	.byte 1,9
	.asciz "Document"

	.byte 2,0,7
	.asciz "System_Xml_ConformanceLevel"

LDIFF_SYM201=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM201
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM202=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM203=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM203
LTDIE_32:

	.byte 5
	.asciz "System_Xml_XmlNameTable"

	.byte 8,16
LDIFF_SYM204=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlNameTable"

LDIFF_SYM205=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM205
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM206=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM206
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM207=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM207
LTDIE_34:

	.byte 5
	.asciz "System_Xml_XmlResolver"

	.byte 8,16
LDIFF_SYM208=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM208
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlResolver"

LDIFF_SYM209=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM210=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM211=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_35:

	.byte 5
	.asciz "System_Collections_ArrayList"

	.byte 20,16
LDIFF_SYM212=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM213=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM214=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM215=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,35,16,0,7
	.asciz "System_Collections_ArrayList"

LDIFF_SYM216=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM216
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM217=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM218=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM218
LTDIE_38:

	.byte 5
	.asciz "_DictionaryNode"

	.byte 20,16
LDIFF_SYM219=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM219
	.byte 2,35,0,6
	.asciz "key"

LDIFF_SYM220=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,8,6
	.asciz "value"

LDIFF_SYM221=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,12,6
	.asciz "next"

LDIFF_SYM222=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,16,0,7
	.asciz "_DictionaryNode"

LDIFF_SYM223=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM224=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM225=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM225
LTDIE_39:

	.byte 17
	.asciz "System_Collections_IComparer"

	.byte 8,7
	.asciz "System_Collections_IComparer"

LDIFF_SYM226=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM226
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM227=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM227
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM228=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM228
LTDIE_37:

	.byte 5
	.asciz "System_Collections_Specialized_ListDictionary"

	.byte 24,16
LDIFF_SYM229=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,0,6
	.asciz "count"

LDIFF_SYM230=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,16,6
	.asciz "version"

LDIFF_SYM231=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,20,6
	.asciz "head"

LDIFF_SYM232=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,8,6
	.asciz "comparer"

LDIFF_SYM233=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,12,0,7
	.asciz "System_Collections_Specialized_ListDictionary"

LDIFF_SYM234=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM234
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM235=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM235
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM236=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM236
LTDIE_36:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

	.byte 12,16
LDIFF_SYM237=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM238=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

LDIFF_SYM239=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM240=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM241=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_40:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

	.byte 9,16
LDIFF_SYM242=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM242
	.byte 2,35,0,6
	.asciz "enable_upa_check"

LDIFF_SYM243=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

LDIFF_SYM244=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM244
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM245=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM245
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM246=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_41:

	.byte 5
	.asciz "System_Xml_Schema_ValidationEventHandler"

	.byte 52,16
LDIFF_SYM247=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,0,0,7
	.asciz "System_Xml_Schema_ValidationEventHandler"

LDIFF_SYM248=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM248
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM249=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM249
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM250=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM250
LTDIE_43:

	.byte 5
	.asciz "_HashKeys"

	.byte 12,16
LDIFF_SYM251=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM252=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 2,35,8,0,7
	.asciz "_HashKeys"

LDIFF_SYM253=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM253
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM254=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM255=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM255
LTDIE_44:

	.byte 5
	.asciz "_HashValues"

	.byte 12,16
LDIFF_SYM256=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM257=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,8,0,7
	.asciz "_HashValues"

LDIFF_SYM258=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM258
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM259=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM259
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM260=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM260
LTDIE_45:

	.byte 17
	.asciz "System_Collections_IHashCodeProvider"

	.byte 8,7
	.asciz "System_Collections_IHashCodeProvider"

LDIFF_SYM261=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM261
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM262=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM263=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_46:

	.byte 17
	.asciz "System_Collections_IEqualityComparer"

	.byte 8,7
	.asciz "System_Collections_IEqualityComparer"

LDIFF_SYM264=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM265=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM265
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM266=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM266
LTDIE_47:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM267=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM267
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM268=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM268
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM269=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM269
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM270=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM270
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM271=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM271
LTDIE_42:

	.byte 5
	.asciz "System_Collections_Hashtable"

	.byte 52,16
LDIFF_SYM272=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM273=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,8,6
	.asciz "hashes"

LDIFF_SYM274=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM274
	.byte 2,35,12,6
	.asciz "hashKeys"

LDIFF_SYM275=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM275
	.byte 2,35,16,6
	.asciz "hashValues"

LDIFF_SYM276=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,35,20,6
	.asciz "hcpRef"

LDIFF_SYM277=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,24,6
	.asciz "comparerRef"

LDIFF_SYM278=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,28,6
	.asciz "equalityComparer"

LDIFF_SYM279=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM279
	.byte 2,35,32,6
	.asciz "inUse"

LDIFF_SYM280=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM280
	.byte 2,35,36,6
	.asciz "modificationCount"

LDIFF_SYM281=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,35,40,6
	.asciz "loadFactor"

LDIFF_SYM282=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,35,44,6
	.asciz "threshold"

LDIFF_SYM283=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,35,48,0,7
	.asciz "System_Collections_Hashtable"

LDIFF_SYM284=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM284
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM285=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM285
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM286=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM286
LTDIE_33:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaSet"

	.byte 80,16
LDIFF_SYM287=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 2,35,0,6
	.asciz "nameTable"

LDIFF_SYM288=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM288
	.byte 2,35,8,6
	.asciz "xmlResolver"

LDIFF_SYM289=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM289
	.byte 2,35,12,6
	.asciz "schemas"

LDIFF_SYM290=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,16,6
	.asciz "attributes"

LDIFF_SYM291=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM291
	.byte 2,35,20,6
	.asciz "elements"

LDIFF_SYM292=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,35,24,6
	.asciz "types"

LDIFF_SYM293=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,35,28,6
	.asciz "settings"

LDIFF_SYM294=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,32,6
	.asciz "isCompiled"

LDIFF_SYM295=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,60,6
	.asciz "<CompilationId>k__BackingField"

LDIFF_SYM296=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,64,6
	.asciz "ValidationEventHandler"

LDIFF_SYM297=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,35,36,6
	.asciz "global_attribute_groups"

LDIFF_SYM298=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM298
	.byte 2,35,40,6
	.asciz "global_groups"

LDIFF_SYM299=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM299
	.byte 2,35,44,6
	.asciz "global_notations"

LDIFF_SYM300=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,48,6
	.asciz "global_identity_constraints"

LDIFF_SYM301=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,52,6
	.asciz "global_ids"

LDIFF_SYM302=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,56,0,7
	.asciz "System_Xml_Schema_XmlSchemaSet"

LDIFF_SYM303=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM303
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM304=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM304
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM305=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM305
LTDIE_48:

	.byte 8
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

	.byte 4
LDIFF_SYM306=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ProcessInlineSchema"

	.byte 1,9
	.asciz "ProcessSchemaLocation"

	.byte 2,9
	.asciz "ReportValidationWarnings"

	.byte 4,9
	.asciz "ProcessIdentityConstraints"

	.byte 8,9
	.asciz "AllowXmlAttributes"

	.byte 16,0,7
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

LDIFF_SYM307=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM307
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM308=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM308
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM309=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM309
LTDIE_49:

	.byte 8
	.asciz "System_Xml_ValidationType"

	.byte 4
LDIFF_SYM310=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "Auto"

	.byte 1,9
	.asciz "DTD"

	.byte 2,9
	.asciz "XDR"

	.byte 3,9
	.asciz "Schema"

	.byte 4,0,7
	.asciz "System_Xml_ValidationType"

LDIFF_SYM311=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM311
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM312=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM312
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM313=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM313
LTDIE_30:

	.byte 5
	.asciz "System_Xml_XmlReaderSettings"

	.byte 60,16
LDIFF_SYM314=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 2,35,0,6
	.asciz "checkCharacters"

LDIFF_SYM315=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM315
	.byte 2,35,24,6
	.asciz "closeInput"

LDIFF_SYM316=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,35,25,6
	.asciz "conformance"

LDIFF_SYM317=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM317
	.byte 2,35,28,6
	.asciz "ignoreComments"

LDIFF_SYM318=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM318
	.byte 2,35,32,6
	.asciz "ignoreProcessingInstructions"

LDIFF_SYM319=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM319
	.byte 2,35,33,6
	.asciz "ignoreWhitespace"

LDIFF_SYM320=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM320
	.byte 2,35,34,6
	.asciz "lineNumberOffset"

LDIFF_SYM321=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 2,35,36,6
	.asciz "linePositionOffset"

LDIFF_SYM322=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,35,40,6
	.asciz "prohibitDtd"

LDIFF_SYM323=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,35,44,6
	.asciz "nameTable"

LDIFF_SYM324=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 2,35,8,6
	.asciz "schemas"

LDIFF_SYM325=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 2,35,12,6
	.asciz "schemasNeedsInitialization"

LDIFF_SYM326=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM326
	.byte 2,35,45,6
	.asciz "validationFlags"

LDIFF_SYM327=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM327
	.byte 2,35,48,6
	.asciz "validationType"

LDIFF_SYM328=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2,35,52,6
	.asciz "xmlResolver"

LDIFF_SYM329=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,35,16,6
	.asciz "isReadOnly"

LDIFF_SYM330=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM330
	.byte 2,35,56,6
	.asciz "isAsync"

LDIFF_SYM331=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 2,35,57,6
	.asciz "ValidationEventHandler"

LDIFF_SYM332=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM332
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReaderSettings"

LDIFF_SYM333=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM333
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM334=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM334
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM335=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM335
LTDIE_51:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM336=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM336
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM337=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM338=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM338
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM339=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM339
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM340=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM340
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM341=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM341
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM342=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM342
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM343=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM343
LTDIE_53:

	.byte 8
	.asciz "_CommandState"

	.byte 4
LDIFF_SYM344=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM344
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ReadElementContentAsBase64"

	.byte 1,9
	.asciz "ReadContentAsBase64"

	.byte 2,9
	.asciz "ReadElementContentAsBinHex"

	.byte 3,9
	.asciz "ReadContentAsBinHex"

	.byte 4,0,7
	.asciz "_CommandState"

LDIFF_SYM345=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM345
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM346=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM346
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM347=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM347
LTDIE_52:

	.byte 5
	.asciz "System_Xml_XmlReaderBinarySupport"

	.byte 24,16
LDIFF_SYM348=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM348
	.byte 2,35,0,6
	.asciz "reader"

LDIFF_SYM349=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM349
	.byte 2,35,8,6
	.asciz "base64CacheStartsAt"

LDIFF_SYM350=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM350
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM351=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM351
	.byte 2,35,16,6
	.asciz "hasCache"

LDIFF_SYM352=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM352
	.byte 2,35,20,6
	.asciz "dontReset"

LDIFF_SYM353=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM353
	.byte 2,35,21,0,7
	.asciz "System_Xml_XmlReaderBinarySupport"

LDIFF_SYM354=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM354
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM355=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM355
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM356=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM356
LTDIE_50:

	.byte 5
	.asciz "System_Xml_XmlReader"

	.byte 24,16
LDIFF_SYM357=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM357
	.byte 2,35,0,6
	.asciz "readStringBuffer"

LDIFF_SYM358=LTDIE_51_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM358
	.byte 2,35,8,6
	.asciz "binary"

LDIFF_SYM359=LTDIE_52_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM359
	.byte 2,35,12,6
	.asciz "settings"

LDIFF_SYM360=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM360
	.byte 2,35,16,6
	.asciz "asyncRunning"

LDIFF_SYM361=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM361
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReader"

LDIFF_SYM362=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM362
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM363=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM363
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM364=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM364
	.byte 2
	.asciz "System.Xml.Linq.XDocument:Load"
	.long _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0
	.long Lme_34

	.byte 2,118,16,3
	.asciz "textReader"

LDIFF_SYM365=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM365
	.byte 2,123,20,3
	.asciz "options"

LDIFF_SYM366=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,123,24,11
	.asciz "s"

LDIFF_SYM367=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM367
	.byte 0,11
	.asciz "r"

LDIFF_SYM368=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM368
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM369=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM369
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM370=Lfde4_end - Lfde4_start
	.long LDIFF_SYM370
Lfde4_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0

LDIFF_SYM371=Lme_34 - _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0
	.long LDIFF_SYM371
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_57:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM372=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM372
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM373=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM373
LTDIE_57_POINTER:

	.byte 13
LDIFF_SYM374=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM374
LTDIE_57_REFERENCE:

	.byte 14
LDIFF_SYM375=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM375
LTDIE_58:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM376=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM376
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM377=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM377
LTDIE_58_POINTER:

	.byte 13
LDIFF_SYM378=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM378
LTDIE_58_REFERENCE:

	.byte 14
LDIFF_SYM379=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM379
LTDIE_56:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM380=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM380
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM381=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM381
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM382=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM382
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM383=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM383
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM384=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM384
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM385=LTDIE_58_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM385
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM386=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM386
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM387=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM387
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM388=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM388
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM389=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM389
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM390=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM390
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM391=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM391
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM392=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM392
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM393=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM393
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM394=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM394
LTDIE_56_POINTER:

	.byte 13
LDIFF_SYM395=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM395
LTDIE_56_REFERENCE:

	.byte 14
LDIFF_SYM396=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM396
LTDIE_59:

	.byte 8
	.asciz "System_Xml_NewLineHandling"

	.byte 4
LDIFF_SYM397=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM397
	.byte 9
	.asciz "Replace"

	.byte 0,9
	.asciz "Entitize"

	.byte 1,9
	.asciz "None"

	.byte 2,0,7
	.asciz "System_Xml_NewLineHandling"

LDIFF_SYM398=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM398
LTDIE_59_POINTER:

	.byte 13
LDIFF_SYM399=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM399
LTDIE_59_REFERENCE:

	.byte 14
LDIFF_SYM400=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM400
LTDIE_60:

	.byte 8
	.asciz "System_Xml_XmlOutputMethod"

	.byte 4
LDIFF_SYM401=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM401
	.byte 9
	.asciz "Xml"

	.byte 0,9
	.asciz "Html"

	.byte 1,9
	.asciz "Text"

	.byte 2,9
	.asciz "AutoDetect"

	.byte 3,0,7
	.asciz "System_Xml_XmlOutputMethod"

LDIFF_SYM402=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM402
LTDIE_60_POINTER:

	.byte 13
LDIFF_SYM403=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM403
LTDIE_60_REFERENCE:

	.byte 14
LDIFF_SYM404=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM404
LTDIE_61:

	.byte 8
	.asciz "System_Xml_NamespaceHandling"

	.byte 4
LDIFF_SYM405=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM405
	.byte 9
	.asciz "Default"

	.byte 0,9
	.asciz "OmitDuplicates"

	.byte 1,0,7
	.asciz "System_Xml_NamespaceHandling"

LDIFF_SYM406=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM406
LTDIE_61_POINTER:

	.byte 13
LDIFF_SYM407=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM407
LTDIE_61_REFERENCE:

	.byte 14
LDIFF_SYM408=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM408
LTDIE_55:

	.byte 5
	.asciz "System_Xml_XmlWriterSettings"

	.byte 52,16
LDIFF_SYM409=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM409
	.byte 2,35,0,6
	.asciz "checkCharacters"

LDIFF_SYM410=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM410
	.byte 2,35,20,6
	.asciz "closeOutput"

LDIFF_SYM411=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM411
	.byte 2,35,21,6
	.asciz "conformance"

LDIFF_SYM412=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM412
	.byte 2,35,24,6
	.asciz "encoding"

LDIFF_SYM413=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM413
	.byte 2,35,8,6
	.asciz "indent"

LDIFF_SYM414=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM414
	.byte 2,35,28,6
	.asciz "indentChars"

LDIFF_SYM415=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM415
	.byte 2,35,12,6
	.asciz "newLineChars"

LDIFF_SYM416=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM416
	.byte 2,35,16,6
	.asciz "newLineOnAttributes"

LDIFF_SYM417=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM417
	.byte 2,35,29,6
	.asciz "newLineHandling"

LDIFF_SYM418=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM418
	.byte 2,35,32,6
	.asciz "omitXmlDeclaration"

LDIFF_SYM419=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM419
	.byte 2,35,36,6
	.asciz "outputMethod"

LDIFF_SYM420=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM420
	.byte 2,35,40,6
	.asciz "isReadOnly"

LDIFF_SYM421=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM421
	.byte 2,35,44,6
	.asciz "isAsync"

LDIFF_SYM422=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM422
	.byte 2,35,45,6
	.asciz "<NamespaceHandling>k__BackingField"

LDIFF_SYM423=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM423
	.byte 2,35,48,0,7
	.asciz "System_Xml_XmlWriterSettings"

LDIFF_SYM424=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM424
LTDIE_55_POINTER:

	.byte 13
LDIFF_SYM425=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM425
LTDIE_55_REFERENCE:

	.byte 14
LDIFF_SYM426=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM426
LTDIE_54:

	.byte 5
	.asciz "System_Xml_XmlWriter"

	.byte 16,16
LDIFF_SYM427=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM427
	.byte 2,35,0,6
	.asciz "settings"

LDIFF_SYM428=LTDIE_55_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM428
	.byte 2,35,8,6
	.asciz "asyncRunning"

LDIFF_SYM429=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM429
	.byte 2,35,12,0,7
	.asciz "System_Xml_XmlWriter"

LDIFF_SYM430=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM430
LTDIE_54_POINTER:

	.byte 13
LDIFF_SYM431=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM431
LTDIE_54_REFERENCE:

	.byte 14
LDIFF_SYM432=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM432
	.byte 2
	.asciz "System.Xml.Linq.XDocument:WriteTo"
	.long _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0
	.long Lme_38

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM433=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM433
	.byte 1,86,3
	.asciz "writer"

LDIFF_SYM434=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM434
	.byte 1,90,11
	.asciz "node"

LDIFF_SYM435=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM435
	.byte 1,85,11
	.asciz ""

LDIFF_SYM436=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM436
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM437=Lfde5_end - Lfde5_start
	.long LDIFF_SYM437
Lfde5_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0

LDIFF_SYM438=Lme_38 - _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0
	.long LDIFF_SYM438
	.byte 12,13,0,72,14,8,135,2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_62:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM439=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM439
LTDIE_62_POINTER:

	.byte 13
LDIFF_SYM440=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM440
LTDIE_62_REFERENCE:

	.byte 14
LDIFF_SYM441=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM441
	.byte 2
	.asciz "System.Xml.Linq.XElement:LookupPrefix"
	.long _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0
	.long Lme_53

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM442=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM442
	.byte 2,123,16,3
	.asciz "ns"

LDIFF_SYM443=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM443
	.byte 2,123,20,3
	.asciz "w"

LDIFF_SYM444=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM444
	.byte 2,123,24,11
	.asciz "prefix"

LDIFF_SYM445=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM445
	.byte 1,84,11
	.asciz "a"

LDIFF_SYM446=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM446
	.byte 1,86,11
	.asciz ""

LDIFF_SYM447=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM447
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM448=Lfde6_end - Lfde6_start
	.long LDIFF_SYM448
Lfde6_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0

LDIFF_SYM449=Lme_53 - _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0
	.long LDIFF_SYM449
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XElement:WriteTo"
	.long _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0
	.long Lme_55

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM450=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM450
	.byte 1,86,3
	.asciz "writer"

LDIFF_SYM451=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM451
	.byte 1,90,11
	.asciz "prefix"

LDIFF_SYM452=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM452
	.byte 2,123,8,11
	.asciz "createdNS"

LDIFF_SYM453=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM453
	.byte 2,123,12,11
	.asciz "a"

LDIFF_SYM454=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM454
	.byte 1,85,11
	.asciz ""

LDIFF_SYM455=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM455
	.byte 2,123,16,11
	.asciz "apfix"

LDIFF_SYM456=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM456
	.byte 1,84,11
	.asciz "node"

LDIFF_SYM457=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM457
	.byte 2,123,20,11
	.asciz ""

LDIFF_SYM458=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM458
	.byte 2,123,24,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM459=Lfde7_end - Lfde7_start
	.long LDIFF_SYM459
Lfde7_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0

LDIFF_SYM460=Lme_55 - _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0
	.long LDIFF_SYM460
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,96,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XElement:GetNamespaceOfPrefix"
	.long _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0
	.long Lme_56

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM461=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM461
	.byte 1,86,3
	.asciz "prefix"

LDIFF_SYM462=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM462
	.byte 1,90,11
	.asciz "el"

LDIFF_SYM463=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM463
	.byte 1,86,11
	.asciz "a"

LDIFF_SYM464=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM464
	.byte 1,85,11
	.asciz ""

LDIFF_SYM465=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM465
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM466=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM466
	.byte 1,84,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM467=Lfde8_end - Lfde8_start
	.long LDIFF_SYM467
Lfde8_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0

LDIFF_SYM468=Lme_56 - _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0
	.long LDIFF_SYM468
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_63:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM469=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM469
LTDIE_63_POINTER:

	.byte 13
LDIFF_SYM470=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM470
LTDIE_63_REFERENCE:

	.byte 14
LDIFF_SYM471=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM471
	.byte 2
	.asciz "System.Xml.Linq.XElement:GetPrefixOfNamespace"
	.long _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0
	.long Lme_57

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM472=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM472
	.byte 1,86,3
	.asciz "ns"

LDIFF_SYM473=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM473
	.byte 1,90,11
	.asciz "prefix"

LDIFF_SYM474=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM474
	.byte 1,85,11
	.asciz ""

LDIFF_SYM475=LTDIE_63_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM475
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM476=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM476
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM477=Lfde9_end - Lfde9_start
	.long LDIFF_SYM477
Lfde9_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0

LDIFF_SYM478=Lme_57 - _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0
	.long LDIFF_SYM478
	.byte 12,13,0,72,14,8,135,2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde9_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XElement:OnAddingObject"
	.long _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0
	.long Lme_59

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM479=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM479
	.byte 3,123,212,0,3
	.asciz "o"

LDIFF_SYM480=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM480
	.byte 1,86,3
	.asciz "rejectAttribute"

LDIFF_SYM481=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM481
	.byte 3,123,216,0,3
	.asciz "refNode"

LDIFF_SYM482=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM482
	.byte 3,123,220,0,3
	.asciz "addFirst"

LDIFF_SYM483=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM483
	.byte 0,11
	.asciz "a"

LDIFF_SYM484=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM484
	.byte 1,84,11
	.asciz "ia"

LDIFF_SYM485=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM485
	.byte 1,90,11
	.asciz ""

LDIFF_SYM486=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM486
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM487=Lfde10_end - Lfde10_start
	.long LDIFF_SYM487
Lfde10_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0

LDIFF_SYM488=Lme_59 - _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0
	.long LDIFF_SYM488
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,134,6,136,5,138,4,139,3,142,1,68,14,136,1,68,13,11
	.align 2
Lfde10_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_64:

	.byte 5
	.asciz "_<GetPrefixOfNamespaceCore>c__Iterator3"

	.byte 40,16
LDIFF_SYM489=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM489
	.byte 2,35,0,6
	.asciz "<el>__0"

LDIFF_SYM490=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM490
	.byte 2,35,8,6
	.asciz "$locvar0"

LDIFF_SYM491=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM491
	.byte 2,35,12,6
	.asciz "<a>__1"

LDIFF_SYM492=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM492
	.byte 2,35,16,6
	.asciz "ns"

LDIFF_SYM493=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM493
	.byte 2,35,20,6
	.asciz "$this"

LDIFF_SYM494=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM494
	.byte 2,35,24,6
	.asciz "$current"

LDIFF_SYM495=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM495
	.byte 2,35,28,6
	.asciz "$disposing"

LDIFF_SYM496=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM496
	.byte 2,35,32,6
	.asciz "$PC"

LDIFF_SYM497=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM497
	.byte 2,35,36,0,7
	.asciz "_<GetPrefixOfNamespaceCore>c__Iterator3"

LDIFF_SYM498=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM498
LTDIE_64_POINTER:

	.byte 13
LDIFF_SYM499=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM499
LTDIE_64_REFERENCE:

	.byte 14
LDIFF_SYM500=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM500
LTDIE_65:

	.byte 5
	.asciz "System_UInt32"

	.byte 12,16
LDIFF_SYM501=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM501
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM502=LDIE_U4 - Ldebug_info_start
	.long LDIFF_SYM502
	.byte 2,35,8,0,7
	.asciz "System_UInt32"

LDIFF_SYM503=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM503
LTDIE_65_POINTER:

	.byte 13
LDIFF_SYM504=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM504
LTDIE_65_REFERENCE:

	.byte 14
LDIFF_SYM505=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM505
	.byte 2
	.asciz "System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator3:MoveNext"
	.long _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0
	.long Lme_6c

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM506=LTDIE_64_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM506
	.byte 2,123,16,11
	.asciz ""

LDIFF_SYM507=LDIE_U4 - Ldebug_info_start
	.long LDIFF_SYM507
	.byte 1,90,11
	.asciz ""

LDIFF_SYM508=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM508
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM509=Lfde11_end - Lfde11_start
	.long LDIFF_SYM509
Lfde11_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0

LDIFF_SYM510=Lme_6c - _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0
	.long LDIFF_SYM510
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde11_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XElement/<GetPrefixOfNamespaceCore>c__Iterator3:Dispose"
	.long _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0
	.long Lme_6d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM511=LTDIE_64_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM511
	.byte 2,123,16,11
	.asciz "V_0"

LDIFF_SYM512=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM512
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM513=Lfde12_end - Lfde12_start
	.long LDIFF_SYM513
Lfde12_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0

LDIFF_SYM514=Lme_6d - _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_Dispose_0
	.long LDIFF_SYM514
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde12_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XNamespace:Get"
	.long _System_Xml_Linq_XNamespace_Get_string_0
	.long Lme_81

	.byte 2,118,16,3
	.asciz "namespaceName"

LDIFF_SYM515=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM515
	.byte 1,90,11
	.asciz ""

LDIFF_SYM516=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM516
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM517=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM517
	.byte 2,123,4,11
	.asciz "ret"

LDIFF_SYM518=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM518
	.byte 2,123,8,11
	.asciz ""

LDIFF_SYM519=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM519
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM520=Lfde13_end - Lfde13_start
	.long LDIFF_SYM520
Lfde13_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNamespace_Get_string_0

LDIFF_SYM521=Lme_81 - _System_Xml_Linq_XNamespace_Get_string_0
	.long LDIFF_SYM521
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde13_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XNamespace:GetName"
	.long _System_Xml_Linq_XNamespace_GetName_string_0
	.long Lme_82

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM522=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM522
	.byte 1,86,3
	.asciz "localName"

LDIFF_SYM523=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM523
	.byte 1,90,11
	.asciz ""

LDIFF_SYM524=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM524
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM525=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM525
	.byte 2,123,4,11
	.asciz "ret"

LDIFF_SYM526=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM526
	.byte 2,123,8,11
	.asciz ""

LDIFF_SYM527=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM527
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM528=Lfde14_end - Lfde14_start
	.long LDIFF_SYM528
Lfde14_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNamespace_GetName_string_0

LDIFF_SYM529=Lme_82 - _System_Xml_Linq_XNamespace_GetName_string_0
	.long LDIFF_SYM529
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde14_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_66:

	.byte 5
	.asciz "System_Xml_Linq_XNodeEqualityComparer"

	.byte 8,16
LDIFF_SYM530=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM530
	.byte 2,35,0,0,7
	.asciz "System_Xml_Linq_XNodeEqualityComparer"

LDIFF_SYM531=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM531
LTDIE_66_POINTER:

	.byte 13
LDIFF_SYM532=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM532
LTDIE_66_REFERENCE:

	.byte 14
LDIFF_SYM533=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM533
LTDIE_67:

	.byte 8
	.asciz "System_Xml_XmlNodeType"

	.byte 4
LDIFF_SYM534=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM534
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "Element"

	.byte 1,9
	.asciz "Attribute"

	.byte 2,9
	.asciz "Text"

	.byte 3,9
	.asciz "CDATA"

	.byte 4,9
	.asciz "EntityReference"

	.byte 5,9
	.asciz "Entity"

	.byte 6,9
	.asciz "ProcessingInstruction"

	.byte 7,9
	.asciz "Comment"

	.byte 8,9
	.asciz "Document"

	.byte 9,9
	.asciz "DocumentType"

	.byte 10,9
	.asciz "DocumentFragment"

	.byte 11,9
	.asciz "Notation"

	.byte 12,9
	.asciz "Whitespace"

	.byte 13,9
	.asciz "SignificantWhitespace"

	.byte 14,9
	.asciz "EndElement"

	.byte 15,9
	.asciz "EndEntity"

	.byte 16,9
	.asciz "XmlDeclaration"

	.byte 17,0,7
	.asciz "System_Xml_XmlNodeType"

LDIFF_SYM535=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM535
LTDIE_67_POINTER:

	.byte 13
LDIFF_SYM536=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM536
LTDIE_67_REFERENCE:

	.byte 14
LDIFF_SYM537=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM537
LTDIE_68:

	.byte 5
	.asciz "System_Xml_Linq_XComment"

	.byte 44,16
LDIFF_SYM538=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM538
	.byte 2,35,0,6
	.asciz "value"

LDIFF_SYM539=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM539
	.byte 2,35,40,0,7
	.asciz "System_Xml_Linq_XComment"

LDIFF_SYM540=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM540
LTDIE_68_POINTER:

	.byte 13
LDIFF_SYM541=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM541
LTDIE_68_REFERENCE:

	.byte 14
LDIFF_SYM542=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM542
LTDIE_69:

	.byte 5
	.asciz "System_Xml_Linq_XProcessingInstruction"

	.byte 48,16
LDIFF_SYM543=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM543
	.byte 2,35,0,6
	.asciz "name"

LDIFF_SYM544=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM544
	.byte 2,35,40,6
	.asciz "data"

LDIFF_SYM545=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM545
	.byte 2,35,44,0,7
	.asciz "System_Xml_Linq_XProcessingInstruction"

LDIFF_SYM546=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM546
LTDIE_69_POINTER:

	.byte 13
LDIFF_SYM547=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM547
LTDIE_69_REFERENCE:

	.byte 14
LDIFF_SYM548=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM548
	.byte 2
	.asciz "System.Xml.Linq.XNodeEqualityComparer:Equals"
	.long _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0
	.long Lme_99

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM549=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM549
	.byte 3,123,248,0,3
	.asciz "x"

LDIFF_SYM550=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM550
	.byte 3,123,252,0,3
	.asciz "y"

LDIFF_SYM551=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM551
	.byte 3,123,128,1,11
	.asciz ""

LDIFF_SYM552=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM552
	.byte 2,123,0,11
	.asciz "doc1"

LDIFF_SYM553=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM553
	.byte 2,123,4,11
	.asciz "doc2"

LDIFF_SYM554=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM554
	.byte 2,123,8,11
	.asciz "id2"

LDIFF_SYM555=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM555
	.byte 1,90,11
	.asciz "n"

LDIFF_SYM556=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM556
	.byte 2,123,12,11
	.asciz ""

LDIFF_SYM557=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM557
	.byte 2,123,16,11
	.asciz ""

LDIFF_SYM558=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM558
	.byte 2,123,20,11
	.asciz "e1"

LDIFF_SYM559=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM559
	.byte 2,123,24,11
	.asciz "e2"

LDIFF_SYM560=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM560
	.byte 2,123,28,11
	.asciz "ia2"

LDIFF_SYM561=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM561
	.byte 1,86,11
	.asciz "n"

LDIFF_SYM562=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM562
	.byte 2,123,32,11
	.asciz ""

LDIFF_SYM563=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM563
	.byte 2,123,36,11
	.asciz "ie2"

LDIFF_SYM564=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM564
	.byte 1,85,11
	.asciz "n"

LDIFF_SYM565=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM565
	.byte 2,123,40,11
	.asciz ""

LDIFF_SYM566=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM566
	.byte 2,123,44,11
	.asciz "c1"

LDIFF_SYM567=LTDIE_68_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM567
	.byte 2,123,48,11
	.asciz "c2"

LDIFF_SYM568=LTDIE_68_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM568
	.byte 2,123,52,11
	.asciz "p1"

LDIFF_SYM569=LTDIE_69_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM569
	.byte 2,123,56,11
	.asciz "p2"

LDIFF_SYM570=LTDIE_69_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM570
	.byte 2,123,60,11
	.asciz "d1"

LDIFF_SYM571=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM571
	.byte 1,84,11
	.asciz "d2"

LDIFF_SYM572=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM572
	.byte 3,123,192,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM573=Lfde15_end - Lfde15_start
	.long LDIFF_SYM573
Lfde15_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0

LDIFF_SYM574=Lme_99 - _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0
	.long LDIFF_SYM574
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,176,1,68,13,11
	.align 2
Lfde15_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XNodeEqualityComparer:GetHashCode"
	.long _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0
	.long Lme_9e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM575=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM575
	.byte 1,86,3
	.asciz "obj"

LDIFF_SYM576=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM576
	.byte 1,90,11
	.asciz "h"

LDIFF_SYM577=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM577
	.byte 1,85,11
	.asciz ""

LDIFF_SYM578=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM578
	.byte 1,84,11
	.asciz "doc"

LDIFF_SYM579=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM579
	.byte 2,123,0,11
	.asciz "n"

LDIFF_SYM580=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM580
	.byte 2,123,4,11
	.asciz ""

LDIFF_SYM581=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM581
	.byte 2,123,8,11
	.asciz "el"

LDIFF_SYM582=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM582
	.byte 2,123,12,11
	.asciz "a"

LDIFF_SYM583=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM583
	.byte 2,123,16,11
	.asciz ""

LDIFF_SYM584=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM584
	.byte 2,123,20,11
	.asciz "n"

LDIFF_SYM585=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM585
	.byte 2,123,24,11
	.asciz ""

LDIFF_SYM586=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM586
	.byte 2,123,28,11
	.asciz "pi"

LDIFF_SYM587=LTDIE_69_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM587
	.byte 2,123,32,11
	.asciz "dtd"

LDIFF_SYM588=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM588
	.byte 2,123,36,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM589=Lfde16_end - Lfde16_start
	.long LDIFF_SYM589
Lfde16_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0

LDIFF_SYM590=Lme_9e - _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0
	.long LDIFF_SYM590
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13,11
	.align 2
Lfde16_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_71:

	.byte 8
	.asciz "System_Xml_ReadState"

	.byte 4
LDIFF_SYM591=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM591
	.byte 9
	.asciz "Initial"

	.byte 0,9
	.asciz "Interactive"

	.byte 1,9
	.asciz "Error"

	.byte 2,9
	.asciz "EndOfFile"

	.byte 3,9
	.asciz "Closed"

	.byte 4,0,7
	.asciz "System_Xml_ReadState"

LDIFF_SYM592=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM592
LTDIE_71_POINTER:

	.byte 13
LDIFF_SYM593=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM593
LTDIE_71_REFERENCE:

	.byte 14
LDIFF_SYM594=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM594
LTDIE_72:

	.byte 5
	.asciz "System_Xml_NameTable"

	.byte 20,16
LDIFF_SYM595=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM595
	.byte 2,35,0,6
	.asciz "count"

LDIFF_SYM596=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM596
	.byte 2,35,12,6
	.asciz "buckets"

LDIFF_SYM597=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM597
	.byte 2,35,8,6
	.asciz "size"

LDIFF_SYM598=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM598
	.byte 2,35,16,0,7
	.asciz "System_Xml_NameTable"

LDIFF_SYM599=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM599
LTDIE_72_POINTER:

	.byte 13
LDIFF_SYM600=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM600
LTDIE_72_REFERENCE:

	.byte 14
LDIFF_SYM601=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM601
LTDIE_70:

	.byte 5
	.asciz "System_Xml_Linq_XNodeReader"

	.byte 48,16
LDIFF_SYM602=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM602
	.byte 2,35,0,6
	.asciz "state"

LDIFF_SYM603=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM603
	.byte 2,35,36,6
	.asciz "node"

LDIFF_SYM604=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM604
	.byte 2,35,24,6
	.asciz "start"

LDIFF_SYM605=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM605
	.byte 2,35,28,6
	.asciz "attr"

LDIFF_SYM606=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM606
	.byte 2,35,40,6
	.asciz "attr_value"

LDIFF_SYM607=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM607
	.byte 2,35,44,6
	.asciz "end_element"

LDIFF_SYM608=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM608
	.byte 2,35,45,6
	.asciz "name_table"

LDIFF_SYM609=LTDIE_72_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM609
	.byte 2,35,32,0,7
	.asciz "System_Xml_Linq_XNodeReader"

LDIFF_SYM610=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM610
LTDIE_70_POINTER:

	.byte 13
LDIFF_SYM611=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM611
LTDIE_70_REFERENCE:

	.byte 14
LDIFF_SYM612=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM612
	.byte 2
	.asciz "System.Xml.Linq.XNodeReader:GetXAttribute"
	.long _System_Xml_Linq_XNodeReader_GetXAttribute_int_0
	.long Lme_b2

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM613=LTDIE_70_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM613
	.byte 1,86,3
	.asciz "idx"

LDIFF_SYM614=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM614
	.byte 2,123,28,11
	.asciz "el"

LDIFF_SYM615=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM615
	.byte 1,85,11
	.asciz "i"

LDIFF_SYM616=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM616
	.byte 1,84,11
	.asciz "a"

LDIFF_SYM617=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM617
	.byte 1,90,11
	.asciz ""

LDIFF_SYM618=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM618
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM619=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM619
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM620=Lfde17_end - Lfde17_start
	.long LDIFF_SYM620
Lfde17_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNodeReader_GetXAttribute_int_0

LDIFF_SYM621=Lme_b2 - _System_Xml_Linq_XNodeReader_GetXAttribute_int_0
	.long LDIFF_SYM621
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde17_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XNodeReader:GetAttribute"
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_0
	.long Lme_bd

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM622=LTDIE_70_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM622
	.byte 2,123,24,3
	.asciz "name"

LDIFF_SYM623=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM623
	.byte 1,90,11
	.asciz "a_bak"

LDIFF_SYM624=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM624
	.byte 2,123,0,11
	.asciz "av_bak"

LDIFF_SYM625=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM625
	.byte 2,123,4,11
	.asciz ""

LDIFF_SYM626=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM626
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM627=Lfde18_end - Lfde18_start
	.long LDIFF_SYM627
Lfde18_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_0

LDIFF_SYM628=Lme_bd - _System_Xml_Linq_XNodeReader_GetAttribute_string_0
	.long LDIFF_SYM628
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde18_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XNodeReader:GetAttribute"
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0
	.long Lme_be

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM629=LTDIE_70_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM629
	.byte 2,123,24,3
	.asciz "local"

LDIFF_SYM630=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM630
	.byte 2,123,28,3
	.asciz "ns"

LDIFF_SYM631=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM631
	.byte 1,90,11
	.asciz "a_bak"

LDIFF_SYM632=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM632
	.byte 2,123,0,11
	.asciz "av_bak"

LDIFF_SYM633=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM633
	.byte 2,123,4,11
	.asciz ""

LDIFF_SYM634=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM634
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM635=Lfde19_end - Lfde19_start
	.long LDIFF_SYM635
Lfde19_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0

LDIFF_SYM636=Lme_be - _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0
	.long LDIFF_SYM636
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde19_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_73:

	.byte 8
	.asciz "System_TypeCode"

	.byte 4
LDIFF_SYM637=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM637
	.byte 9
	.asciz "Empty"

	.byte 0,9
	.asciz "Object"

	.byte 1,9
	.asciz "DBNull"

	.byte 2,9
	.asciz "Boolean"

	.byte 3,9
	.asciz "Char"

	.byte 4,9
	.asciz "SByte"

	.byte 5,9
	.asciz "Byte"

	.byte 6,9
	.asciz "Int16"

	.byte 7,9
	.asciz "UInt16"

	.byte 8,9
	.asciz "Int32"

	.byte 9,9
	.asciz "UInt32"

	.byte 10,9
	.asciz "Int64"

	.byte 11,9
	.asciz "UInt64"

	.byte 12,9
	.asciz "Single"

	.byte 13,9
	.asciz "Double"

	.byte 14,9
	.asciz "Decimal"

	.byte 15,9
	.asciz "DateTime"

	.byte 16,9
	.asciz "String"

	.byte 18,0,7
	.asciz "System_TypeCode"

LDIFF_SYM638=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM638
LTDIE_73_POINTER:

	.byte 13
LDIFF_SYM639=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM639
LTDIE_73_REFERENCE:

	.byte 14
LDIFF_SYM640=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM640
LTDIE_74:

	.byte 5
	.asciz "System_Double"

	.byte 16,16
LDIFF_SYM641=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM641
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM642=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM642
	.byte 2,35,8,0,7
	.asciz "System_Double"

LDIFF_SYM643=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM643
LTDIE_74_POINTER:

	.byte 13
LDIFF_SYM644=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM644
LTDIE_74_REFERENCE:

	.byte 14
LDIFF_SYM645=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM645
	.byte 2
	.asciz "System.Xml.Linq.XUtil:ToString"
	.long _System_Xml_Linq_XUtil_ToString_object
	.long Lme_e9

	.byte 2,118,16,3
	.asciz "o"

LDIFF_SYM646=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM646
	.byte 1,90,11
	.asciz ""

LDIFF_SYM647=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM647
	.byte 1,86,11
	.asciz ""

LDIFF_SYM648=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM648
	.byte 2,125,0,11
	.asciz ""

LDIFF_SYM649=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM649
	.byte 2,125,16,11
	.asciz ""

LDIFF_SYM650=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM650
	.byte 2,125,24,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM651=Lfde20_end - Lfde20_start
	.long LDIFF_SYM651
Lfde20_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XUtil_ToString_object

LDIFF_SYM652=Lme_e9 - _System_Xml_Linq_XUtil_ToString_object
	.long LDIFF_SYM652
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,96
	.align 2
Lfde20_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_75:

	.byte 5
	.asciz "_<ExpandArray>c__Iterator0"

	.byte 48,16
LDIFF_SYM653=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM653
	.byte 2,35,0,6
	.asciz "o"

LDIFF_SYM654=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM654
	.byte 2,35,8,6
	.asciz "<n>__0"

LDIFF_SYM655=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM655
	.byte 2,35,12,6
	.asciz "$locvar0"

LDIFF_SYM656=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM656
	.byte 2,35,16,6
	.asciz "<obj>__1"

LDIFF_SYM657=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM657
	.byte 2,35,20,6
	.asciz "$locvar1"

LDIFF_SYM658=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM658
	.byte 2,35,24,6
	.asciz "$locvar2"

LDIFF_SYM659=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM659
	.byte 2,35,28,6
	.asciz "<oo>__2"

LDIFF_SYM660=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM660
	.byte 2,35,32,6
	.asciz "$current"

LDIFF_SYM661=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM661
	.byte 2,35,36,6
	.asciz "$disposing"

LDIFF_SYM662=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM662
	.byte 2,35,40,6
	.asciz "$PC"

LDIFF_SYM663=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM663
	.byte 2,35,44,0,7
	.asciz "_<ExpandArray>c__Iterator0"

LDIFF_SYM664=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM664
LTDIE_75_POINTER:

	.byte 13
LDIFF_SYM665=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM665
LTDIE_75_REFERENCE:

	.byte 14
LDIFF_SYM666=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM666
	.byte 2
	.asciz "System.Xml.Linq.XUtil/<ExpandArray>c__Iterator0:MoveNext"
	.long _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext
	.long Lme_f1

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM667=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM667
	.byte 3,123,212,0,11
	.asciz ""

LDIFF_SYM668=LDIE_U4 - Ldebug_info_start
	.long LDIFF_SYM668
	.byte 1,90,11
	.asciz ""

LDIFF_SYM669=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM669
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM670=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM670
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM671=Lfde21_end - Lfde21_start
	.long LDIFF_SYM671
Lfde21_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext

LDIFF_SYM672=Lme_f1 - _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext
	.long LDIFF_SYM672
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13,11
	.align 2
Lfde21_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Xml.Linq.XUtil/<ExpandArray>c__Iterator0:Dispose"
	.long _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose
	.long Lme_f2

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM673=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM673
	.byte 2,123,60,11
	.asciz "V_0"

LDIFF_SYM674=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM674
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM675=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM675
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM676=Lfde22_end - Lfde22_start
	.long LDIFF_SYM676
Lfde22_start:

	.long 0
	.align 2
	.long _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose

LDIFF_SYM677=Lme_f2 - _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_Dispose
	.long LDIFF_SYM677
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,80,68,13,11
	.align 2
Lfde22_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_76:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM678=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM678
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM679=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM679
LTDIE_76_POINTER:

	.byte 13
LDIFF_SYM680=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM680
LTDIE_76_REFERENCE:

	.byte 14
LDIFF_SYM681=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM681
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_f7

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM682=LTDIE_76_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM682
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM683=Lfde23_end - Lfde23_start
	.long LDIFF_SYM683
Lfde23_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM684=Lme_f7 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM684
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde23_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_77:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM685=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM685
LTDIE_77_POINTER:

	.byte 13
LDIFF_SYM686=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM686
LTDIE_77_REFERENCE:

	.byte 14
LDIFF_SYM687=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM687
LTDIE_78:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM688=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM688
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM689=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM689
LTDIE_78_POINTER:

	.byte 13
LDIFF_SYM690=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM690
LTDIE_78_REFERENCE:

	.byte 14
LDIFF_SYM691=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM691
LTDIE_79:

	.byte 5
	.asciz "System_Char"

	.byte 10,16
LDIFF_SYM692=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM692
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM693=LDIE_CHAR - Ldebug_info_start
	.long LDIFF_SYM693
	.byte 2,35,8,0,7
	.asciz "System_Char"

LDIFF_SYM694=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM694
LTDIE_79_POINTER:

	.byte 13
LDIFF_SYM695=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM695
LTDIE_79_REFERENCE:

	.byte 14
LDIFF_SYM696=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM696
LTDIE_80:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM697=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM697
LTDIE_80_POINTER:

	.byte 13
LDIFF_SYM698=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM698
LTDIE_80_REFERENCE:

	.byte 14
LDIFF_SYM699=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM699
	.byte 2
	.asciz "System.Linq.Enumerable:All<char>"
	.long _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0
	.long Lme_10d

	.byte 2,118,16,3
	.asciz "source"

LDIFF_SYM700=LTDIE_77_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM700
	.byte 1,86,3
	.asciz "predicate"

LDIFF_SYM701=LTDIE_78_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM701
	.byte 1,90,11
	.asciz "element"

LDIFF_SYM702=LDIE_CHAR - Ldebug_info_start
	.long LDIFF_SYM702
	.byte 2,123,0,11
	.asciz ""

LDIFF_SYM703=LTDIE_80_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM703
	.byte 2,123,4,11
	.asciz ""

LDIFF_SYM704=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM704
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM705=Lfde24_end - Lfde24_start
	.long LDIFF_SYM705
Lfde24_start:

	.long 0
	.align 2
	.long _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0

LDIFF_SYM706=Lme_10d - _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0
	.long LDIFF_SYM706
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde24_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/System.Xml.Linq/System.Xml.Linq"
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/System.Core/System.Linq"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "XContainer.cs"

	.byte 1,0,0
	.asciz "XDocument.cs"

	.byte 1,0,0
	.asciz "XElement.cs"

	.byte 1,0,0
	.asciz "XNamespace.cs"

	.byte 1,0,0
	.asciz "XNodeEqualityComparer.cs"

	.byte 1,0,0
	.asciz "XNodeReader.cs"

	.byte 1,0,0
	.asciz "XUtil.cs"

	.byte 1,0,0
	.asciz "Array.cs"

	.byte 2,0,0
	.asciz "Enumerable.cs"

	.byte 3,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XContainer_CheckChildType_object_bool_0

	.byte 3,56,4,2,1,3,56,2,48,1,3,2,2,160,1,1,3,1,2,168,1,1,3,1,2,208,1,1,3,4,2,204
	.byte 2,1,2,212,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XContainer_Add_object_0

	.byte 3,197,0,4,2,1,3,197,0,2,40,1,189,3,2,2,220,0,1,3,2,2,48,1,8,118,8,173,2,140,1,1
	.byte 0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XDocument_get_DocumentType_0

	.byte 3,197,0,4,3,1,3,197,0,2,32,1,3,1,2,224,0,1,3,1,2,200,0,1,3,1,2,196,1,1,2,40
	.byte 1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XDocument_get_Root_0

	.byte 3,210,0,4,3,1,3,210,0,2,32,1,3,1,2,224,0,1,3,1,2,200,0,1,3,1,2,196,1,1,2,40
	.byte 1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XDocument_Load_System_IO_TextReader_System_Xml_Linq_LoadOptions_0

	.byte 3,249,0,4,3,1,3,249,0,2,36,1,3,1,2,36,1,8,61,3,2,2,52,1,2,216,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XDocument_WriteTo_System_Xml_XmlWriter_0

	.byte 3,234,1,4,3,1,3,234,1,2,36,1,3,1,2,36,1,3,2,2,60,1,243,3,1,2,224,0,1,2,160,1
	.byte 1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement_LookupPrefix_string_System_Xml_XmlWriter_0

	.byte 3,155,5,4,4,1,3,155,5,2,48,1,3,1,2,228,0,1,3,1,2,220,0,1,3,1,2,212,0,1,3,3
	.byte 2,48,1,3,3,2,128,1,1,2,16,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement_WriteTo_System_Xml_XmlWriter_0

	.byte 3,185,5,4,4,1,3,185,5,2,52,1,3,1,2,40,1,131,131,3,2,2,36,1,3,2,2,204,0,1,3,1
	.byte 2,224,0,1,3,1,2,52,1,8,173,3,2,2,240,0,1,3,2,2,208,0,1,243,131,3,1,2,208,0,1,3
	.byte 4,2,192,1,1,3,1,2,228,0,1,3,2,2,148,1,1,187,8,62,2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement_GetNamespaceOfPrefix_string_0

	.byte 3,226,5,4,4,1,3,226,5,2,36,1,75,3,1,2,228,0,1,3,1,2,144,1,1,3,125,2,204,1,1,190
	.byte 2,24,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement_GetPrefixOfNamespace_System_Xml_Linq_XNamespace_0

	.byte 3,235,5,4,4,1,3,235,5,2,36,1,3,1,2,228,0,1,8,229,3,1,2,136,1,1,2,24,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement_OnAddingObject_object_bool_System_Xml_Linq_XNode_bool_0

	.byte 3,172,6,4,4,1,3,172,6,2,52,1,3,3,2,192,2,1,3,1,2,204,0,1,187,3,1,2,236,0,1,243
	.byte 3,1,2,184,1,1,131,132,3,1,2,176,1,1,3,1,2,184,1,1,133,3,113,2,16,1,2,200,0,1,0,1
	.byte 1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XElement__GetPrefixOfNamespaceCorec__Iterator3_MoveNext_0

	.byte 3,243,5,4,4,1,3,243,5,2,240,0,1,8,117,3,1,2,184,1,1,3,1,2,236,0,1,3,125,2,208,2
	.byte 1,3,4,2,228,0,1,2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNamespace_Get_string_0

	.byte 3,62,4,5,1,3,62,2,48,1,3,2,2,48,1,3,1,2,56,1,3,1,2,44,1,3,2,2,44,1,2,60
	.byte 1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNamespace_GetName_string_0

	.byte 3,202,0,4,5,1,3,202,0,2,52,1,187,3,1,2,36,1,8,174,3,1,2,36,1,3,1,2,44,1,8,174
	.byte 2,60,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNodeEqualityComparer_Equals_System_Xml_Linq_XNode_System_Xml_Linq_XNode_0

	.byte 3,42,4,6,1,3,42,2,60,1,187,8,61,131,244,3,1,2,52,1,243,3,2,2,132,1,1,3,1,2,60,1
	.byte 3,1,2,204,0,1,243,243,3,1,2,56,1,3,1,2,224,0,1,3,1,2,52,1,187,3,1,2,196,0,1,3
	.byte 2,2,136,1,1,3,2,2,60,1,3,1,2,200,0,1,3,1,2,228,0,1,131,243,3,1,2,56,1,3,1,2
	.byte 224,0,1,3,1,2,52,1,187,3,1,2,196,0,1,3,2,2,136,1,1,3,1,2,48,1,243,3,1,2,56,1
	.byte 3,1,2,224,0,1,3,1,2,52,1,187,3,1,2,196,0,1,3,2,2,136,1,1,3,2,2,192,0,1,3,1
	.byte 2,60,1,3,1,2,208,0,1,188,3,1,2,60,1,3,1,2,208,0,1,3,2,2,200,0,1,3,1,2,60,1
	.byte 3,1,2,204,0,1,3,5,2,140,1,1,3,2,2,148,1,1,2,204,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNodeEqualityComparer_GetHashCode_System_Xml_Linq_XNode_0

	.byte 3,142,1,4,6,1,3,142,1,2,52,1,131,131,8,61,3,2,2,244,0,1,3,1,2,192,0,1,187,3,1,2
	.byte 232,0,1,3,3,2,144,1,1,3,1,2,192,0,1,8,117,3,1,2,232,0,1,3,1,2,152,1,1,3,1,2
	.byte 228,0,1,3,3,2,144,1,1,3,1,2,220,0,1,76,3,1,2,192,0,1,3,1,2,192,0,1,76,3,1,2
	.byte 192,0,1,3,4,2,156,1,1,76,3,3,2,212,0,1,2,32,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNodeReader_GetXAttribute_int_0

	.byte 3,162,1,4,7,1,3,162,1,2,36,1,8,173,131,3,1,2,208,0,1,131,131,75,3,1,2,228,0,1,8,61
	.byte 3,1,2,136,1,1,2,24,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_0

	.byte 3,186,3,4,7,1,3,186,3,2,48,1,187,188,8,61,3,2,2,216,0,1,187,2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XNodeReader_GetAttribute_string_string_0

	.byte 3,199,3,4,7,1,3,199,3,2,52,1,187,188,8,61,3,2,2,220,0,1,187,2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XUtil_ToString_object

	.byte 3,59,4,8,1,3,59,2,224,0,1,133,3,2,2,200,0,1,3,2,2,60,1,3,2,2,216,0,1,3,2,2
	.byte 244,0,1,3,2,2,232,0,1,3,3,2,240,0,1,8,230,3,1,2,196,0,1,3,1,2,212,0,1,3,1,2
	.byte 196,0,1,3,1,2,236,0,1,3,107,2,28,1,2,60,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Xml_Linq_XUtil__ExpandArrayc__Iterator0_MoveNext

	.byte 3,225,0,4,8,1,3,225,0,2,248,0,1,3,1,2,196,0,1,8,61,3,1,2,44,1,3,1,2,204,0,1
	.byte 3,1,2,44,1,3,1,2,164,1,1,3,1,2,172,2,1,3,1,2,188,1,1,3,2,2,208,4,1,3,1,2
	.byte 48,1,2,44,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,9,1,3,207,0,2,32,1,2,252,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Linq_Enumerable_All_char_System_Collections_Generic_IEnumerable_1_char_System_Func_2_char_bool_0

	.byte 3,243,0,4,10,1,3,243,0,2,36,1,188,3,1,2,224,0,1,8,173,3,2,2,136,1,1,2,24,1,0,1
	.byte 1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
