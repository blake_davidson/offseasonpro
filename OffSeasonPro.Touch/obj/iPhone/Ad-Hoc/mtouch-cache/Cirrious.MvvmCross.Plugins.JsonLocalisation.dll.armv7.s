	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__:
Leh_func_begin1:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	ldr	r9, [r0]
	ldr	r9, [r9, #56]
	blx	r9
	cmp	r0, #0
	ldrne	r1, [r0, #8]
	cmpne	r1, #0
	beq	LBB1_2
	ldr	r1, [r7, #8]
	ldr	r2, [r1, #12]
	cmp	r2, #0
	popeq	{r7, pc}
	bl	_p_1_plt_string_Format_string_object___llvm
LBB1_2:
	pop	{r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider__ctor:
Leh_func_begin2:
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool:
Leh_func_begin3:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
Ltmp5:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC3_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC3_0+8))
LPC3_0:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	bl	_p_3_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm
	str	r6, [r5, #8]
	strb	r4, [r5, #12]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string:
Leh_func_begin4:
	push	{r4, r7, lr}
Ltmp6:
	add	r7, sp, #4
Ltmp7:
Ltmp8:
	mov	r4, r0
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	_p_4_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string_llvm
	mov	r1, r0
	ldr	r0, [r4, #8]
	ldr	r2, [r0]
	ldr	r2, [r7, #8]
	bl	_p_5_plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string_llvm
	pop	{r4, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_GetText_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_GetText_string_string_string:
Leh_func_begin5:
	push	{r4, r5, r6, r7, lr}
Ltmp9:
	add	r7, sp, #12
Ltmp10:
	push	{r10}
Ltmp11:
	sub	sp, sp, #4
	mov	r10, r0
	mov	r0, #0
	str	r0, [sp]
	mov	r0, r1
	mov	r1, r2
	mov	r2, r3
	bl	_p_4_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string_llvm
	mov	r4, r0
	ldr	r0, [r10, #8]
	mov	r2, sp
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_6_plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string__llvm
	tst	r0, #255
	beq	LBB5_2
	ldr	r4, [sp]
	b	LBB5_3
LBB5_2:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC5_0+8))
	mov	r1, r4
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC5_0+8))
LPC5_0:
	add	r5, pc, r5
	ldr	r0, [r5, #20]
	bl	_p_7_plt_string_Concat_string_string_llvm
	mov	r6, r0
	ldr	r0, [r5, #24]
	mov	r1, #0
	bl	_p_8_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r6
	bl	_p_9_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm
	ldrb	r0, [r10, #12]
	cmp	r0, #0
	beq	LBB5_4
LBB5_3:
	mov	r0, r4
	sub	sp, r7, #16
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB5_4:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC5_1+8))
	mov	r1, #49
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC5_1+8))
LPC5_1:
	ldr	r0, [pc, r0]
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r4
	bl	_p_7_plt_string_Concat_string_string_llvm
	mov	r1, r0
	movw	r0, #118
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool:
Leh_func_begin6:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
Ltmp14:
	bl	_p_13_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool_llvm
	pop	{r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert:
Leh_func_begin7:
	push	{r7, lr}
Ltmp15:
	mov	r7, sp
Ltmp16:
	push	{r8}
Ltmp17:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC7_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #28]
	mov	r8, r0
	bl	_p_14_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider__ctor_bool
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider__ctor_bool:
Leh_func_begin8:
	push	{r7, lr}
Ltmp18:
	mov	r7, sp
Ltmp19:
Ltmp20:
	bl	_p_19_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool_llvm
	pop	{r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string:
Leh_func_begin9:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
	push	{r10}
Ltmp23:
	mov	r4, r3
	mov	r10, r2
	mov	r6, r1
	mov	r5, r0
	mov	r2, r4
	bl	_p_20_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string_llvm
	mov	r3, r0
	cmp	r3, #0
	ldrne	r0, [r3, #8]
	cmpne	r0, #0
	beq	LBB9_2
	mov	r0, r5
	mov	r1, r6
	mov	r2, r10
	bl	_p_21_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB9_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC9_0+8))
	mov	r1, #113
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC9_0+8))
LPC9_0:
	ldr	r0, [pc, r0]
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r4
	bl	_p_7_plt_string_Concat_string_string_llvm
	mov	r1, r0
	movw	r0, #208
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string:
Leh_func_begin10:
	push	{r4, r5, r6, r7, lr}
Ltmp24:
	add	r7, sp, #12
Ltmp25:
	push	{r10}
Ltmp26:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC10_0+8))
	mov	r10, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC10_0+8))
LPC10_0:
	add	r0, pc, r0
	ldr	r0, [r0, #48]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #1
	mov	r4, r0
	bl	_p_22_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool_llvm
	mov	r0, r6
	mov	r1, r5
	mov	r2, r10
	mov	r3, r4
	bl	_p_23_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider:
Leh_func_begin11:
	push	{r7, lr}
Ltmp27:
	mov	r7, sp
Ltmp28:
Ltmp29:
	push	{r3}
	bl	_p_24_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider_llvm
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider:
Leh_func_begin12:
	push	{r7, lr}
Ltmp30:
	mov	r7, sp
Ltmp31:
Ltmp32:
	add	r9, r0, #8
	stm	r9, {r1, r2, r3}
	ldr	r1, [r7, #8]
	str	r1, [r0, #20]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC12_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC12_0+8))
LPC12_0:
	add	r1, pc, r1
	ldr	r1, [r1, #52]
	ldr	r1, [r1]
	bl	_p_25_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string_llvm
	pop	{r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_get_TextProvider
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_get_TextProvider:
Leh_func_begin13:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_GetResourceFilePath_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_GetResourceFilePath_string_string:
Leh_func_begin14:
	push	{r4, r5, r6, r7, lr}
Ltmp33:
	add	r7, sp, #12
Ltmp34:
	push	{r10, r11}
Ltmp35:
	mov	r5, r1
	mov	r4, r0
	mov	r10, r2
	cmp	r5, #0
	ldrne	r0, [r5, #8]
	cmpne	r0, #0
	beq	LBB14_2
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC14_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC14_0+8))
LPC14_0:
	add	r1, pc, r1
	ldr	r0, [r1, #24]
	ldr	r11, [r1, #56]
	mov	r1, #3
	bl	_p_8_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r2, [r4, #12]
	mov	r1, #0
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r6]
	mov	r1, #1
	mov	r2, r5
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r6]
	mov	r1, #2
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	mov	r0, r11
	b	LBB14_3
LBB14_2:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC14_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC14_1+8))
LPC14_1:
	add	r1, pc, r1
	ldr	r0, [r1, #24]
	ldr	r5, [r1, #60]
	mov	r1, #2
	bl	_p_8_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r2, [r4, #12]
	mov	r1, #0
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r6]
	mov	r1, #1
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	mov	r0, r5
LBB14_3:
	mov	r1, r6
	bl	_p_1_plt_string_Format_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader:
Leh_func_begin15:
	push	{r4, r7, lr}
Ltmp36:
	add	r7, sp, #4
Ltmp37:
	push	{r8}
Ltmp38:
	mov	r4, r0
	ldr	r0, [r4, #16]
	cmp	r0, #0
	bne	LBB15_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC15_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC15_0+8))
LPC15_0:
	add	r0, pc, r0
	ldr	r0, [r0, #64]
	mov	r8, r0
	bl	_p_26_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm
LBB15_2:
	str	r0, [r4, #16]
	ldr	r0, [r4, #16]
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool:
Leh_func_begin16:
	push	{r7, lr}
Ltmp39:
	mov	r7, sp
Ltmp40:
Ltmp41:
	bl	_p_19_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool_llvm
	pop	{r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string:
Leh_func_begin17:
	push	{r4, r5, r6, r7, lr}
Ltmp42:
	add	r7, sp, #12
Ltmp43:
	push	{r8, r10}
Ltmp44:
	mov	r4, r3
	mov	r10, r2
	mov	r6, r1
	mov	r5, r0
	bl	_p_27_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC17_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC17_0+8))
LPC17_0:
	add	r1, pc, r1
	ldr	r1, [r1, #68]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r4
	blx	r2
	mov	r3, r0
	cmp	r3, #0
	ldrne	r0, [r3, #8]
	cmpne	r0, #0
	beq	LBB17_2
	mov	r0, r5
	mov	r1, r6
	mov	r2, r10
	bl	_p_21_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_llvm
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
LBB17_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC17_1+8))
	mov	r1, #113
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC17_1+8))
LPC17_1:
	ldr	r0, [pc, r0]
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r4
	bl	_p_7_plt_string_Concat_string_string_llvm
	mov	r1, r0
	movw	r0, #208
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader_EnsureLoaded
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader_EnsureLoaded:
Leh_func_begin18:
	ldrb	r1, [r0, #8]
	cmp	r1, #0
	moveq	r1, #1
	strbeq	r1, [r0, #8]
	bx	lr
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__ctor:
Leh_func_begin19:
	bx	lr
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__cctor:
Leh_func_begin20:
	push	{r4, r7, lr}
Ltmp45:
	add	r7, sp, #4
Ltmp46:
Ltmp47:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC20_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC20_0+8))
LPC20_0:
	add	r4, pc, r4
	ldr	r0, [r4, #72]
	bl	_p_28_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #76]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string:
Leh_func_begin21:
	push	{r4, r5, r6, r7, lr}
Ltmp48:
	add	r7, sp, #12
Ltmp49:
	push	{r10, r11}
Ltmp50:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC21_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC21_0+8))
LPC21_0:
	add	r0, pc, r0
	ldr	r0, [r0, #80]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB21_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB21_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB21_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB21_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB21_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB21_7
LBB21_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB21_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current:
Leh_func_begin22:
	push	{r4, r7, lr}
Ltmp51:
	add	r7, sp, #4
Ltmp52:
	push	{r8}
Ltmp53:
	sub	sp, sp, #8
	bic	sp, sp, #7
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC22_0+8))
	mov	r1, sp
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC22_0+8))
LPC22_0:
	add	r4, pc, r4
	ldr	r2, [r4, #84]
	mov	r8, r2
	bl	_p_30_plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current_llvm
	ldr	r0, [r4, #44]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp]
	str	r1, [r0, #8]
	ldr	r1, [sp, #4]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array:
Leh_func_begin23:
	mov	r2, r1
	mvn	r3, #1
	strd	r2, r3, [r0]
	bx	lr
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current:
Leh_func_begin24:
	push	{r4, r5, r7, lr}
Ltmp54:
	add	r7, sp, #8
Ltmp55:
	push	{r8}
Ltmp56:
	sub	sp, sp, #12
	bic	sp, sp, #7
	mov	r5, r1
	ldr	r1, [r0, #4]
	cmn	r1, #2
	beq	LBB24_3
	ldr	r1, [r0, #4]
	cmn	r1, #1
	beq	LBB24_4
	ldr	r3, [r0]
	ldr	r1, [r0]
	ldr	r1, [r1, #12]
	ldr	r2, [r0, #4]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC24_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #88]
	ldr	r4, [r3]
	sub	r1, r1, #1
	sub	r2, r1, r2
	mov	r1, sp
	mov	r8, r0
	mov	r0, r3
	bl	_p_31_plt_System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int_llvm
	ldr	r0, [sp]
	str	r0, [r5]
	ldr	r0, [sp, #4]
	str	r0, [r5, #4]
	sub	sp, r7, #12
	pop	{r8}
	pop	{r4, r5, r7, pc}
LBB24_3:
	movw	r0, #47632
	b	LBB24_5
LBB24_4:
	movw	r0, #47718
LBB24_5:
	bl	_p_32_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose:
Leh_func_begin25:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext:
Leh_func_begin26:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	ldreq	r1, [r0]
	ldreq	r1, [r1, #12]
	streq	r1, [r0, #4]
	mov	r1, #0
	ldr	r2, [r0, #4]
	cmn	r2, #1
	beq	LBB26_2
	ldr	r1, [r0, #4]
	cmp	r1, #0
	sub	r2, r1, #1
	movne	r1, #1
	str	r2, [r0, #4]
LBB26_2:
	mov	r0, r1
	bx	lr
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset:
Leh_func_begin27:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__IEnumerable_GetEnumerator_System_Collections_Generic_KeyValuePair_2_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__IEnumerable_GetEnumerator_System_Collections_Generic_KeyValuePair_2_string_string:
Leh_func_begin28:
	push	{r4, r7, lr}
Ltmp57:
	add	r7, sp, #4
Ltmp58:
	push	{r8}
Ltmp59:
	sub	sp, sp, #16
	bic	sp, sp, #7
	mov	r1, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC28_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got-(LPC28_0+8))
LPC28_0:
	add	r0, pc, r0
	ldr	r4, [r0, #84]
	mov	r0, #0
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
	mov	r8, r4
	bl	_p_33_plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int
	.align	2
_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int:
Leh_func_begin29:
	push	{r7, lr}
Ltmp60:
	mov	r7, sp
Ltmp61:
Ltmp62:
	sub	sp, sp, #8
	bic	sp, sp, #7
	ldr	r3, [r0, #12]
	cmp	r3, r2
	addhi	r2, r0, r2, lsl #3
	ldrhi	r0, [r2, #16]
	ldrhi	r2, [r2, #20]
	stmhi	sp, {r0, r2}
	ldrhi	r0, [sp]
	strhi	r0, [r1]
	ldrhi	r0, [sp, #4]
	strhi	r0, [r1, #4]
	movhi	sp, r7
	pophi	{r7, pc}
	movw	r0, #11703
	bl	_p_32_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end29:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got,300,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_GetText_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider__ctor_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_get_TextProvider
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_GetResourceFilePath_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader_EnsureLoaded
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__IEnumerable_GetEnumerator_System_Collections_Generic_KeyValuePair_2_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	30
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	5
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	6
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	7
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	8
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	9
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	10
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	11
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	14
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	15
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	17
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	18
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	19
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	21
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	23
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	24
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	25
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	26
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	27
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	28
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	29
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	35
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	37
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	38
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	39
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	40
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	41
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	42
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	43
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	45
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
Lset30 = Leh_func_end29-Leh_func_begin29
	.long	Lset30
Lset31 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset31
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin18:
	.byte	0

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0
_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0:

	.byte 128,64,45,233,13,112,160,225,112,9,45,233,84,208,77,226,13,176,160,225,0,64,160,225,1,80,160,225,2,96,160,225
	.byte 72,48,139,229,0,0,160,227,8,0,139,229,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227
	.byte 20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,160,227,32,0,139,229,4,0,160,225
bl _p_15

	.byte 0,32,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 20
	.byte 0,0,159,231,0,48,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 24
	.byte 3,48,159,231,2,0,160,225,72,16,155,229,0,32,146,229,3,128,160,225,4,224,143,226,76,240,18,229,0,0,0,0
	.byte 0,32,160,225,16,16,139,226,2,0,160,225,0,224,210,229
bl _p_16

	.byte 24,0,0,234,16,0,139,226,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 28
	.byte 1,16,159,231,12,0,128,226,0,16,144,229,8,16,139,229,4,0,144,229,12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 32
	.byte 0,0,159,231,8,48,155,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 32
	.byte 0,0,159,231,12,192,155,229,4,0,160,225,5,16,160,225,6,32,160,225,0,192,141,229
bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string

	.byte 16,0,139,226,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 28
	.byte 8,128,159,231
bl _p_17

	.byte 255,0,0,226,0,0,80,227,221,255,255,26,0,0,0,235,14,0,0,234,8,208,77,226,68,224,139,229,16,0,139,226
	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 28
	.byte 1,16,159,231,56,0,139,229,0,224,208,229,56,0,155,229,0,16,160,227,0,16,128,229,8,208,141,226,68,192,155,229
	.byte 12,240,160,225,84,208,139,226,112,9,189,232,128,128,189,232

Lme_d:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string
_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string:

	.byte 128,64,45,233,13,112,160,225,48,13,45,233,68,208,77,226,13,176,160,225,44,0,139,229,48,16,139,229,2,160,160,225
	.byte 0,0,160,227,0,0,139,229,0,0,160,227,4,0,139,229,0,0,160,227,8,0,139,229,0,0,160,227,16,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 92
	.byte 0,0,159,231,56,0,139,229,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 96
	.byte 1,16,159,231,0,32,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 92
	.byte 2,32,159,231,10,0,160,225,0,224,218,229
bl _p_38

	.byte 0,32,160,225,56,16,155,229,48,0,155,229
bl _p_37

	.byte 0,0,139,229,0,80,160,227,48,0,155,229
bl _p_36

	.byte 0,32,160,225,0,16,155,229,2,0,160,225,0,32,146,229,15,224,160,225,92,240,146,229,0,64,160,225,0,0,80,227
	.byte 2,0,0,26,0,0,160,227,12,0,139,229,65,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 88
	.byte 0,0,159,231
bl _p_2

	.byte 56,0,139,229,4,16,160,225
bl _p_35

	.byte 56,0,155,229,4,0,139,229,4,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,52,240,145,229,0,80,160,225
	.byte 0,0,0,235,15,0,0,234,36,224,139,229,4,0,155,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 84
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,36,192,155,229,12,240,160,225,12,80,139,229,29,0,0,234
	.byte 20,0,155,229,20,0,155,229,8,0,139,229,56,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . -12
	.byte 0,0,159,231,181,16,160,227
bl _p_10

	.byte 60,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 12
	.byte 0,0,159,231,1,16,160,227
bl _p_8

	.byte 16,0,139,229,0,48,160,225,0,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 56,0,155,229,60,16,155,229,16,32,155,229
bl _p_34
bl _p_12

	.byte 12,0,155,229,68,208,139,226,48,13,189,232,128,128,189,232

Lme_10:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string
_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,104,208,77,226,13,176,160,225,80,0,139,229,84,16,139,229,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,4,0,139,229,0,0,160,227,8,0,139,229,0,0,160,227,12,0,139,229,0,0,160,227
	.byte 16,0,139,229,80,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,60,240,145,229,0,16,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 116
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,12,0,139,229,116,0,0,234,12,32,155,229,24,16,139,226
	.byte 2,0,160,225,0,32,146,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 112
	.byte 8,128,159,231,4,224,143,226,12,240,18,229,0,0,0,0,24,0,155,229,0,0,139,229,28,0,155,229,4,0,139,229
	.byte 80,0,155,229,16,0,144,229,96,0,139,229,80,0,155,229,8,0,144,229,88,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 32
	.byte 0,0,159,231,36,176,139,229,11,0,160,225,0,0,144,229,32,0,139,229,92,0,139,229,80,48,155,229,84,16,155,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 32
	.byte 0,0,159,231,44,176,139,229,11,0,160,225,4,0,144,229,40,0,139,229,0,32,160,225,3,0,160,225,0,48,147,229
	.byte 15,224,160,225,56,240,147,229,0,48,160,225,88,16,155,229,92,32,155,229,96,192,155,229,12,0,160,225,0,192,156,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 108
	.byte 8,128,159,231,4,224,143,226,24,240,28,229,0,0,0,0,57,0,0,234,20,0,155,229,20,0,155,229,8,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 104
	.byte 0,0,159,231,88,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 12
	.byte 0,0,159,231,3,16,160,227
bl _p_8

	.byte 16,0,139,229,0,48,160,225,84,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 16,48,155,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 32
	.byte 0,0,159,231,52,176,139,229,11,0,160,225,0,0,144,229,48,0,139,229,0,32,160,225,3,0,160,225,1,16,160,227
	.byte 0,48,147,229,15,224,160,225,128,240,147,229,16,0,155,229,92,0,139,229,8,0,155,229
bl _p_41

	.byte 0,32,160,225,92,48,155,229,3,0,160,225,2,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229,88,0,155,229
	.byte 16,16,155,229
bl _p_40
bl _p_39

	.byte 76,0,139,229,0,0,80,227,1,0,0,10,76,0,155,229
bl _p_12

	.byte 255,255,255,234,12,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 100
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,125,255,255,26,0,0,0,235
	.byte 15,0,0,234,72,224,139,229,12,0,155,229,0,0,80,227,9,0,0,10,12,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 84
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,72,192,155,229,12,240,160,225,104,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_16:
.text
ut_37:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current

.text
ut_38:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array

.text
ut_39:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current

.text
ut_40:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose

.text
ut_41:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext

.text
ut_42:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset

.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_GetText_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider__ctor_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_get_TextProvider
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_GetResourceFilePath_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader_EnsureLoaded
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__IEnumerable_GetEnumerator_System_Collections_Generic_KeyValuePair_2_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider__ctor
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_GetText_string_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider__ctor_bool
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_get_TextProvider
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_GetResourceFilePath_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_LoadJsonFromResource_string_string_string
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader_EnsureLoaded
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_PluginLoader__cctor
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_get_Current
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_Dispose
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_MoveNext
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_System_Collections_IEnumerator_Reset
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__IEnumerable_GetEnumerator_System_Collections_Generic_KeyValuePair_2_string_string
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_JsonLocalisation__System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:

	.long 37

	bl ut_37

	.long 38

	bl ut_38

	.long 39

	bl ut_39

	.long 40

	bl ut_40

	.long 41

	bl ut_41

	.long 42

	bl ut_42
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 46,10,5,2
	.short 0, 10, 24, 34, 48
	.byte 0,0,0,0,0,1,2,2,3,2,14,2,255,255,255,255,240,19,9,2,2,8,3,2,0,48,2,12,6,3,2,3
	.byte 3,3,0,0,0,0,0,87,255,255,255,255,169,90,4,2,99,2,2,2,255,255,255,255,151,109
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,0,0,0,0,0,0,0
	.long 0,0,0,0,0,230,41,0
	.long 0,0,0,210,40,0,0,0
	.long 0,0,0,0,0,0,0,270
	.long 43,0,190,39,19,291,45,0
	.long 0,0,0,130,35,0,0,0
	.long 0,150,37,0,170,38,0,0
	.long 0,0,250,42,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 15,31,0,32,0,33,0,34
	.long 0,35,130,36,0,37,150,38
	.long 170,39,190,40,210,41,230,42
	.long 250,43,270,44,0,45,291
.section __TEXT, __const
	.align 3
class_name_table:

	.short 19, 3, 0, 9, 0, 0, 0, 0
	.short 0, 0, 0, 10, 0, 1, 19, 0
	.short 0, 5, 20, 0, 0, 6, 0, 0
	.short 0, 0, 0, 2, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 8, 0, 4
	.short 0, 7, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 33,10,4,2
	.short 0, 11, 22, 33
	.byte 129,56,2,1,1,1,6,3,7,12,12,129,113,6,6,3,7,4,4,12,5,3,129,167,1,4,22,22,5,5,4,4
	.byte 5,129,243,2,11
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 46,10,5,2
	.short 0, 11, 27, 38, 54
	.byte 0,0,0,0,0,131,237,3,3,3,3,131,252,3,255,255,255,252,1,132,2,14,3,3,27,3,3,0,132,58,3,27
	.byte 3,3,3,3,3,3,0,0,0,0,0,132,109,255,255,255,251,147,132,112,3,3,132,121,3,3,3,255,255,255,251,126
	.byte 132,133
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 29,12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,112,68,13,11,29,12
	.byte 13,0,72,14,8,135,2,68,14,28,132,7,133,6,136,5,138,4,139,3,142,1,68,14,96,68,13,11,23,12,13,0
	.byte 72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,120,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 10,10,1,2
	.short 0
	.byte 132,136,7,7,7,26,26,29,29,27,29

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_plt:
_p_1_plt_string_Format_string_object___llvm:
	.no_dead_strip plt_string_Format_string_object__
plt_string_Format_string_object__:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 132,523
_p_2_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 136,528
_p_3_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string__ctor
plt_System_Collections_Generic_Dictionary_2_string_string__ctor:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 140,551
_p_4_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider_MakeLookupKey_string_string_string:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 144,562
_p_5_plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string
plt_System_Collections_Generic_Dictionary_2_string_string_set_Item_string_string:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 148,567
_p_6_plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string__llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string_
plt_System_Collections_Generic_Dictionary_2_string_string_TryGetValue_string_string_:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 152,578
_p_7_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 156,589
_p_8_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 160,594
_p_9_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 164,620
_p_10_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 168,625
_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 172,645
_p_12_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 176,678
_p_13_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider__ctor_bool:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 180,706
_p_14_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 184,708
_p_15_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_get_JsonConvert:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 188,720
_p_16_plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator
plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 192,722
_p_17_plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext
plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 196,733

.set _p_18_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string_llvm, _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider_AddOrReplace_string_string_string_string

.set _p_19_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool_llvm, _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider__ctor_bool
_p_20_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 208,748
_p_21_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 212,750

.set _p_22_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool_llvm, _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider__ctor_bool

.set _p_23_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_llvm, _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider
_p_24_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder__ctor_string_string_Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader_Cirrious_MvvmCross_Localization_IMvxTextProvider:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 224,756
_p_25_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 228,758
_p_26_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 232,760
_p_27_plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader
plt_Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxContentJsonDictionaryTextProvider_get_ResourceLoader:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 236,772
_p_28_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 240,774
_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 244,800
_p_30_plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current
plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string_get_Current:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 248,838
_p_31_plt_System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int_llvm:
	.no_dead_strip plt_System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int
plt_System_Array_InternalArray__get_Item_System_Collections_Generic_KeyValuePair_2_string_string_int:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 252,859
_p_32_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 256,881
_p_33_plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array
plt_System_Array_InternalEnumerator_1_System_Collections_Generic_KeyValuePair_2_string_string__ctor_System_Array:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 260,910
_p_34_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 264,931
_p_35_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamReader__ctor_System_IO_Stream
plt_System_IO_StreamReader__ctor_System_IO_Stream:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 268,936
_p_36_plt_System_Reflection_Assembly_Load_string_llvm:
	.no_dead_strip plt_System_Reflection_Assembly_Load_string
plt_System_Reflection_Assembly_Load_string:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 272,941
_p_37_plt_string_Concat_string_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string_string
plt_string_Concat_string_string_string:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 276,946
_p_38_plt_string_Replace_string_string_llvm:
	.no_dead_strip plt_string_Replace_string_string
plt_string_Replace_string_string:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 280,951
_p_39_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 284,956
_p_40_plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object__:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 288,995
_p_41_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got - . + 292,1000
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 4
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation"
	.asciz "AC918399-8FFC-4B27-9F44-7C3E8E92D2CF"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader"
	.asciz "D6FEFB8F-01C7-4230-ACE4-ABE563457664"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "AC918399-8FFC-4B27-9F44-7C3E8E92D2CF"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_JsonLocalisation_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation__Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider_GetText_string_string_string_object__
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 33,300,42,46,11,387000831,0,1372
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_JsonLocalisation_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_JsonLocalisation_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,1,4,0,0,0,2,6,5,0,0,0,1,7,0,7,8,9,10,11,11,10,10,0,0,0,0
	.byte 0,6,26,27,26,25,24,6,0,1,12,0,0,0,1,13,0,0,0,10,32,31,11,11,30,29,6,11,28,24,0,4
	.byte 14,6,15,6,0,1,16,0,0,0,1,17,1,10,0,1,10,0,1,10,2,19,18,0,1,20,0,2,11,21,0,0
	.byte 0,1,22,0,0,0,0,0,0,0,2,21,21,0,1,23,4,2,97,1,3,2,130,146,1,2,130,146,1,3,219,0
	.byte 0,3,255,252,0,0,0,1,1,7,112,4,2,130,11,1,1,3,219,0,0,3,255,253,0,0,0,7,128,139,1,198
	.byte 0,13,120,1,3,219,0,0,3,0,255,253,0,0,0,7,128,139,1,198,0,13,121,1,3,219,0,0,3,0,255,253
	.byte 0,0,0,7,128,139,1,198,0,13,122,1,3,219,0,0,3,0,255,253,0,0,0,7,128,139,1,198,0,13,123,1
	.byte 3,219,0,0,3,0,255,253,0,0,0,7,128,139,1,198,0,13,124,1,3,219,0,0,3,0,255,253,0,0,0,7
	.byte 128,139,1,198,0,13,125,1,3,219,0,0,3,0,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,3,219
	.byte 0,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,3,219,0,0,3,12,0,39,42,47,14,3,219
	.byte 0,0,1,17,0,1,14,6,1,2,130,123,1,34,255,254,0,0,0,0,255,43,0,0,1,34,255,254,0,0,0,0
	.byte 255,43,0,0,2,6,255,254,0,0,0,0,255,43,0,0,2,14,3,219,0,0,2,14,3,219,0,0,3,14,1,9
	.byte 16,2,130,146,1,136,199,17,0,129,105,17,0,129,79,34,255,254,0,0,0,0,255,43,0,0,3,6,194,0,1,59
	.byte 14,1,10,16,1,10,8,33,14,7,128,139,34,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,3,219,0
	.byte 0,3,34,255,253,0,0,0,2,130,10,1,1,198,0,13,56,0,1,3,219,0,0,3,6,193,0,17,58,14,2,128
	.byte 228,1,17,0,128,173,17,0,128,177,6,193,0,5,8,17,0,128,231,6,1,6,255,254,0,0,0,0,202,0,0,45
	.byte 6,255,254,0,0,0,0,202,0,0,44,3,193,0,19,128,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101
	.byte 119,95,102,97,115,116,0,3,255,254,0,0,0,0,202,0,0,19,3,195,0,0,9,3,255,254,0,0,0,0,202,0
	.byte 0,21,3,255,254,0,0,0,0,202,0,0,22,3,193,0,19,133,7,23,109,111,110,111,95,97,114,114,97,121,95,110
	.byte 101,119,95,115,112,101,99,105,102,105,99,0,3,194,0,1,123,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108
	.byte 100,115,116,114,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116
	.byte 105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111
	.byte 110,0,3,8,3,255,254,0,0,0,0,255,43,0,0,1,3,12,3,255,254,0,0,0,0,202,0,0,28,3,255,254
	.byte 0,0,0,0,202,0,0,32,3,9,3,11,3,17,3,14,3,26,3,19,3,20,3,23,3,255,254,0,0,0,0,255
	.byte 43,0,0,3,3,25,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0
	.byte 7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107
	.byte 112,111,105,110,116,0,3,255,253,0,0,0,7,128,139,1,198,0,13,122,1,3,219,0,0,3,0,3,255,253,0,0
	.byte 0,2,130,10,1,1,198,0,13,54,0,1,3,219,0,0,3,7,26,109,111,110,111,95,104,101,108,112,101,114,95,108
	.byte 100,115,116,114,95,109,115,99,111,114,108,105,98,0,3,255,253,0,0,0,7,128,139,1,198,0,13,121,1,3,219,0
	.byte 0,3,0,3,194,0,0,83,3,193,0,7,97,3,193,0,8,31,3,193,0,19,134,3,193,0,19,113,7,36,109,111
	.byte 110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108,101,95,101,120,99,101,112,116,105
	.byte 111,110,0,3,194,0,1,124,3,194,0,0,80,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,6,0,1,2,0,129,136,128,184,129,72,129,76,0,16,0,0,16,0,0,6,30,2,2,0,129,88,128,248,129
	.byte 20,129,24,0,20,4,2,130,64,1,128,148,129,96,129,96,0,16,0,0,16,0,0,16,0,0,16,0,0,6,60,2
	.byte 0,20,4,2,130,64,1,128,192,129,112,129,112,2,0,130,212,128,128,130,144,130,148,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,0,128,144,8,0,0,1,0,128,144,8,0,0,1,7,128,144
	.byte 8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,5,6,0,7,128,160,16,0,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,10,6,10,10,128,160,16,0,0,4,193,0,18,178,193,0,18,175
	.byte 193,0,18,174,193,0,18,172,10,6,10,13,14,0,10,128,160,16,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,10,6,10,16,14,16,8,128,160,24,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0
	.byte 18,172,22,23,24,0,10,128,160,20,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,10,6,10
	.byte 27,14,27,5,128,196,30,9,4,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,28,98,111,101,104
	.byte 109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_4:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_3:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider"

	.byte 8,16
LDIFF_SYM6=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_ResourceLoader_MvxResourceProvider"

LDIFF_SYM7=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_2:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider"

	.byte 8,16
LDIFF_SYM10=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProvider"

LDIFF_SYM11=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM11
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM12=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM13=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_6:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM14=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM15=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM15
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM16=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM16
LTDIE_8:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM17=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM18=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM19=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM20=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM20
LTDIE_7:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM21=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM22=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM22
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM23=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM24=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM25=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_5:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM26=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM26
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM27=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM28=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM29=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM30=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM31=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM32=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM33=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM34=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM35=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM36=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM37=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM38=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM39=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM39
LTDIE_9:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM40=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM41=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM41
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM42=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM43=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM44=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM44
LTDIE_1:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider"

	.byte 16,16
LDIFF_SYM45=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,0,6
	.asciz "_entries"

LDIFF_SYM46=LTDIE_5_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,8,6
	.asciz "_maskErrors"

LDIFF_SYM47=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,12,0,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxDictionaryTextProvider"

LDIFF_SYM48=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM49=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM50=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider"

	.byte 16,16
LDIFF_SYM51=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider"

LDIFF_SYM52=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM53=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM54=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation.MvxJsonDictionaryTextProvider:LoadJsonFromText"
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0
	.long Lme_d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM55=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM55
	.byte 1,84,3
	.asciz "namespaceKey"

LDIFF_SYM56=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM56
	.byte 1,85,3
	.asciz "typeKey"

LDIFF_SYM57=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 1,86,3
	.asciz "rawJson"

LDIFF_SYM58=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 3,123,200,0,11
	.asciz "V_0"

LDIFF_SYM59=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM59
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM60=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,123,8,11
	.asciz "V_2"

LDIFF_SYM61=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM62=Lfde0_end - Lfde0_start
	.long LDIFF_SYM62
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0

LDIFF_SYM63=Lme_d - _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxJsonDictionaryTextProvider_LoadJsonFromText_string_string_string_0
	.long LDIFF_SYM63
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,112,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_10:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider"

	.byte 16,16
LDIFF_SYM64=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider"

LDIFF_SYM65=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM66=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM66
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM67=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_17:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM68=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM69=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM70=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM71=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_16:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM72=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM73=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM74=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM75=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_15:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM76=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM77=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM78=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM79=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_19:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM80=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM81=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM82=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM83=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM83
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM84=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM84
LTDIE_18:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM85=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM86=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM87=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM88=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM89=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM90=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_14:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM91=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM92=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM93=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM94=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM95=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM96=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM97=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM98=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM99=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM100=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM100
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM101=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM102=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM103=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM103
LTDIE_13:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM104=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM105=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM106=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM107=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM108=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM109=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_12:

	.byte 5
	.asciz "System_Func`4"

	.byte 52,16
LDIFF_SYM110=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,0,0,7
	.asciz "System_Func`4"

LDIFF_SYM111=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM111
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM112=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM113=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_20:

	.byte 5
	.asciz "System_Action`3"

	.byte 52,16
LDIFF_SYM114=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 2,35,0,0,7
	.asciz "System_Action`3"

LDIFF_SYM115=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM115
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM116=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM117=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_24:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM118=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM119=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM120=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM121=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM122=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_28:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM123=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM124=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM125=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM126=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_27:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM127=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM128=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM128
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM129=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM130=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM131=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM132=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM133=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM134=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_26:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM135=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM136=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM137=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM138=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_25:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM139=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM139
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM140=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM141=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM142=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_23:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM143=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM143
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM144=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM144
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM145=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM146=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_22:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM149=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM150=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_21:

	.byte 5
	.asciz "System_Threading_AutoResetEvent"

	.byte 20,16
LDIFF_SYM153=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,0,0,7
	.asciz "System_Threading_AutoResetEvent"

LDIFF_SYM154=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM155=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM156=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_11:

	.byte 5
	.asciz "System_IO_Stream"

	.byte 20,16
LDIFF_SYM157=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,0,6
	.asciz "async_read"

LDIFF_SYM158=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM158
	.byte 2,35,8,6
	.asciz "async_write"

LDIFF_SYM159=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,12,6
	.asciz "async_event"

LDIFF_SYM160=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,16,0,7
	.asciz "System_IO_Stream"

LDIFF_SYM161=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM162=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM163=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_30:

	.byte 5
	.asciz "System_IO_TextReader"

	.byte 8,16
LDIFF_SYM164=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,0,0,7
	.asciz "System_IO_TextReader"

LDIFF_SYM165=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM165
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM166=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM166
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM167=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_32:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM168=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM169=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM170=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM170
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM171=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM171
LTDIE_33:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM172=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM173=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM174=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM175=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_31:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM176=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM177=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM178=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM179=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM180=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM181=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM182=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM183=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM184=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM185=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM186=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM187=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM188=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM189=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM190=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM190
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM191=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM192=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM192
LTDIE_35:

	.byte 5
	.asciz "System_Text_DecoderFallbackBuffer"

	.byte 8,16
LDIFF_SYM193=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallbackBuffer"

LDIFF_SYM194=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM195=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM196=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM196
LTDIE_34:

	.byte 5
	.asciz "System_Text_Decoder"

	.byte 16,16
LDIFF_SYM197=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,35,0,6
	.asciz "fallback"

LDIFF_SYM198=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM198
	.byte 2,35,8,6
	.asciz "fallback_buffer"

LDIFF_SYM199=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM199
	.byte 2,35,12,0,7
	.asciz "System_Text_Decoder"

LDIFF_SYM200=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM200
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM201=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM201
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM202=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_36:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM203=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM204=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM205=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM206=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM207=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM208=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM209=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM210=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_37:

	.byte 17
	.asciz "System_Threading_Tasks_IDecoupledTask"

	.byte 8,7
	.asciz "System_Threading_Tasks_IDecoupledTask"

LDIFF_SYM211=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM212=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM212
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM213=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_29:

	.byte 5
	.asciz "System_IO_StreamReader"

	.byte 56,16
LDIFF_SYM214=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,0,6
	.asciz "input_buffer"

LDIFF_SYM215=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,35,8,6
	.asciz "decoded_buffer"

LDIFF_SYM216=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,35,12,6
	.asciz "encoding"

LDIFF_SYM217=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM217
	.byte 2,35,16,6
	.asciz "decoder"

LDIFF_SYM218=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM218
	.byte 2,35,20,6
	.asciz "line_builder"

LDIFF_SYM219=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM219
	.byte 2,35,24,6
	.asciz "base_stream"

LDIFF_SYM220=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,28,6
	.asciz "decoded_count"

LDIFF_SYM221=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,36,6
	.asciz "pos"

LDIFF_SYM222=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,40,6
	.asciz "buffer_size"

LDIFF_SYM223=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM223
	.byte 2,35,44,6
	.asciz "do_checks"

LDIFF_SYM224=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM224
	.byte 2,35,48,6
	.asciz "mayBlock"

LDIFF_SYM225=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 2,35,52,6
	.asciz "async_task"

LDIFF_SYM226=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,32,6
	.asciz "leave_open"

LDIFF_SYM227=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,53,6
	.asciz "foundCR"

LDIFF_SYM228=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,54,0,7
	.asciz "System_IO_StreamReader"

LDIFF_SYM229=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM229
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM230=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM230
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM231=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM231
LTDIE_39:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM232=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM232
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM233=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM233
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM234=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM234
LTDIE_38:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM235=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM236=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM237=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM238=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM239=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM239
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM240=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM240
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM241=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM241
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM242=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM242
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM243=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM244=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM245=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM246=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM246
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM247=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM248=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM249=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM249
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM250=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM250
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM251=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation.MvxEmbeddedJsonDictionaryTextProvider:GetTextFromEmbeddedResource"
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string
	.long Lme_10

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM252=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 0,3
	.asciz "namespaceKey"

LDIFF_SYM253=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM253
	.byte 2,123,48,3
	.asciz "resourcePath"

LDIFF_SYM254=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM254
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM255=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM256=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM257=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 1,84,11
	.asciz "V_3"

LDIFF_SYM258=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,123,4,11
	.asciz "V_4"

LDIFF_SYM259=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,123,8,11
	.asciz "V_5"

LDIFF_SYM260=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM260
	.byte 2,123,12,11
	.asciz "V_6"

LDIFF_SYM261=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM261
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM262=Lfde1_end - Lfde1_start
	.long LDIFF_SYM262
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string

LDIFF_SYM263=Lme_10 - _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxEmbeddedJsonDictionaryTextProvider_GetTextFromEmbeddedResource_string_string
	.long LDIFF_SYM263
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,136,5,138,4,139,3,142,1,68,14,96,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_41:

	.byte 17
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_IMvxJsonDictionaryTextLoader"

LDIFF_SYM264=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM265=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM265
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM266=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM266
LTDIE_42:

	.byte 17
	.asciz "Cirrious_MvvmCross_Localization_IMvxTextProvider"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Localization_IMvxTextProvider"

LDIFF_SYM267=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM267
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM268=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM269=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM269
LTDIE_40:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder"

	.byte 24,16
LDIFF_SYM270=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,0,6
	.asciz "_generalNamespaceKey"

LDIFF_SYM271=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,8,6
	.asciz "_rootFolderForResources"

LDIFF_SYM272=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,12,6
	.asciz "_textLoader"

LDIFF_SYM273=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,16,6
	.asciz "_textProvider"

LDIFF_SYM274=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM274
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder"

LDIFF_SYM275=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM276=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM276
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM277=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM277
LTDIE_43:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM278=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM278
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM279=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM279
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM280=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM280
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.JsonLocalisation.MvxTextProviderBuilder:LoadResources"
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string
	.long Lme_16

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM281=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 3,123,208,0,3
	.asciz "whichLocalisationFolder"

LDIFF_SYM282=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 3,123,212,0,11
	.asciz "V_0"

LDIFF_SYM283=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM284=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,123,8,11
	.asciz "V_2"

LDIFF_SYM285=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,123,12,11
	.asciz "V_3"

LDIFF_SYM286=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM287=Lfde2_end - Lfde2_start
	.long LDIFF_SYM287
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string

LDIFF_SYM288=Lme_16 - _Cirrious_MvvmCross_Plugins_JsonLocalisation_MvxTextProviderBuilder_LoadResources_string
	.long LDIFF_SYM288
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,120,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
