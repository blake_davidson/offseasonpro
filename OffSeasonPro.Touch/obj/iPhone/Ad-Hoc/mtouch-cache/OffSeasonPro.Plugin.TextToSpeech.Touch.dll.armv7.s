	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor:
Leh_func_begin1:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
Ltmp2:
	sub	sp, sp, #4
	mov	r4, r0
	mov	r0, #0
	str	r0, [sp]
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC1_0+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC1_0+8))
LPC1_0:
	add	r6, pc, r6
	ldr	r0, [r6, #16]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_2_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm
	str	r5, [r4, #20]
	bl	_p_3_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm
	ldr	r1, [r0]
	mov	r2, #0
	mov	r1, #7
	bl	_p_4_plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int_llvm
	tst	r0, #255
	beq	LBB1_2
	ldr	r0, [r6, #20]
	bl	_p_5_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_6_plt_MonoTouch_AVFoundation_AVSpeechSynthesizer__ctor_llvm
	str	r5, [r4, #12]
	ldr	r5, [r4, #12]
	ldr	r0, [r6, #24]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #28]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #32]
	str	r0, [r1, #28]
	ldr	r0, [r6, #36]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_8_plt_MonoTouch_AVFoundation_AVSpeechSynthesizer_add_DidFinishSpeechUtterance_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_llvm
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
LBB1_2:
	mov	r0, #5
	bl	_p_9_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm
	ldr	r1, [r6, #40]
	bl	_p_10_plt_System_IO_Path_Combine_string_string_llvm
	mov	r4, r0
	bl	_p_11_plt_System_IO_Directory_Exists_string_llvm
	tst	r0, #255
	bne	LBB1_4
	mov	r0, r4
	bl	_p_14_plt_System_IO_Directory_CreateDirectory_string_llvm
LBB1_4:
	mov	r0, #0
	str	r0, [sp]
	bl	_p_12_plt_MonoTouch_AVFoundation_AVAudioSession_SharedInstance_llvm
	mov	r4, r0
	bl	_p_13_plt_MonoTouch_AVFoundation_AVAudioSession_get_CategoryPlayback_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, sp
	ldr	r3, [r0, #76]
	mov	r0, r4
	blx	r3
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end1:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_get_Texts
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_get_Texts:
Leh_func_begin2:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end2:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_set_Texts_System_Collections_Generic_Dictionary_2_string_string
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_set_Texts_System_Collections_Generic_Dictionary_2_string_string:
Leh_func_begin3:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end3:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_Play_string_System_Action
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_Play_string_System_Action:
Leh_func_begin4:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
	push	{r10}
Ltmp5:
	mov	r10, r2
	mov	r6, r1
	mov	r5, r0
	bl	_p_3_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm
	ldr	r1, [r0]
	mov	r2, #0
	mov	r1, #7
	bl	_p_4_plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int_llvm
	tst	r0, #255
	beq	LBB4_2
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC4_1+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC4_1+8))
LPC4_1:
	add	r0, pc, r0
	ldr	r0, [r0, #56]
	bl	_p_5_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r6
	mov	r4, r0
	bl	_p_18_plt_MonoTouch_AVFoundation_AVSpeechUtterance__ctor_string_llvm
	ldr	r0, [r4]
	movw	r1, #39322
	movt	r1, #15897
	ldr	r2, [r0, #80]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	movw	r1, #26214
	movt	r1, #16230
	ldr	r2, [r0, #76]
	mov	r0, r4
	blx	r2
	str	r10, [r5, #16]
	ldr	r0, [r5, #12]
	ldr	r1, [r0]
	ldr	r2, [r1, #76]
	mov	r1, r4
	blx	r2
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB4_2:
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC4_0+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC4_0+8))
LPC4_0:
	add	r4, pc, r4
	ldr	r1, [r4, #44]
	ldr	r3, [r4, #48]
	ldr	r2, [r3]
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_15_plt_string_Replace_string_string_llvm
	ldr	r1, [r4, #52]
	bl	_p_16_plt_string_Concat_string_string_llvm
	mov	r1, r0
	mov	r0, r5
	mov	r2, r10
	bl	_p_17_plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end4:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action:
Leh_func_begin5:
	push	{r4, r5, r6, r7, lr}
Ltmp6:
	add	r7, sp, #12
Ltmp7:
	push	{r10, r11}
Ltmp8:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC5_1+8))
	mov	r10, r0
	mov	r4, r2
	mov	r6, r1
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC5_1+8))
LPC5_1:
	add	r5, pc, r5
	ldr	r0, [r5, #60]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r11, r0
	str	r4, [r11, #8]
	ldr	r0, [r10, #20]
	ldr	r1, [r0]
	mov	r1, r6
	bl	_p_19_plt_System_Collections_Generic_Dictionary_2_string_string_ContainsKey_string_llvm
	tst	r0, #255
	beq	LBB5_7
	ldr	r0, [r10, #20]
	ldr	r1, [r0]
	mov	r1, r6
	bl	_p_20_plt_System_Collections_Generic_Dictionary_2_string_string_get_Item_string_llvm
	mov	r4, r0
	ldr	r0, [r5, #64]
	bl	_p_5_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r6, r0
	bl	_p_21_plt_MonoTouch_Foundation_NSUrl__ctor_string_llvm
	mov	r0, r6
	bl	_p_22_plt_MonoTouch_AVFoundation_AVAudioPlayer_FromUrl_MonoTouch_Foundation_NSUrl_llvm
	str	r0, [r10, #8]
	ldr	r0, [r10, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #84]
	movw	r1, #13107
	movt	r1, #16243
	blx	r2
	ldr	r6, [r10, #8]
	cmp	r11, #0
	beq	LBB5_8
	ldr	r0, [r5, #68]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r11, [r1, #16]
	ldr	r0, [r5, #72]
	str	r0, [r1, #20]
	ldr	r0, [r5, #76]
	str	r0, [r1, #28]
	ldr	r0, [r5, #80]
	str	r0, [r1, #12]
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_23_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_FinishedPlaying_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_llvm
	ldr	r0, [r10, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	ldr	r6, [r5, #84]
	ldr	r4, [r10, #8]
	ldr	r1, [r6]
	cmp	r1, #0
	bne	LBB5_4
	ldr	r0, [r5, #92]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #108]
	str	r1, [r0, #20]
	ldr	r1, [r5, #112]
	str	r1, [r0, #28]
	ldr	r1, [r5, #104]
	str	r1, [r0, #12]
	str	r0, [r6]
	ldr	r0, [r5, #84]
	ldr	r1, [r0]
LBB5_4:
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_24_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_BeginInterruption_System_EventHandler_llvm
	ldr	r4, [r10, #8]
	ldr	r0, [r5, #88]
	ldr	r1, [r0]
	cmp	r1, #0
	bne	LBB5_6
	ldr	r0, [r5, #92]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #96]
	str	r1, [r0, #20]
	ldr	r1, [r5, #100]
	str	r1, [r0, #28]
	ldr	r1, [r5, #104]
	str	r1, [r0, #12]
	ldr	r1, [r5, #88]
	str	r0, [r1]
	ldr	r0, [r5, #88]
	ldr	r1, [r0]
LBB5_6:
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_25_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_EndInterruption_System_EventHandler_llvm
	ldr	r0, [r10, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #76]
	blx	r1
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB5_7:
	ldr	r0, [r11, #8]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp9:
LBB5_8:
	ldr	r0, LCPI5_0
LPC5_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI5_0:
	.long	Ltmp9-(LPC5_0+8)
	.end_data_region
Leh_func_end5:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__MvxTouchTextToSpeechm__0_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__MvxTouchTextToSpeechm__0_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs:
Leh_func_begin6:
	push	{r7, lr}
Ltmp10:
	mov	r7, sp
Ltmp11:
Ltmp12:
	ldr	r0, [r0, #16]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r7, pc}
Leh_func_end6:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__2_object_System_EventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__2_object_System_EventArgs:
Leh_func_begin7:
	push	{r7, lr}
Ltmp13:
	mov	r7, sp
Ltmp14:
Ltmp15:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC7_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #116]
	bl	_p_26_plt_System_Console_WriteLine_string_llvm
	pop	{r7, pc}
Leh_func_end7:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__3_object_System_EventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__3_object_System_EventArgs:
Leh_func_begin8:
	push	{r7, lr}
Ltmp16:
	mov	r7, sp
Ltmp17:
Ltmp18:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC8_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC8_0+8))
LPC8_0:
	add	r0, pc, r0
	ldr	r0, [r0, #120]
	bl	_p_26_plt_System_Console_WriteLine_string_llvm
	pop	{r7, pc}
Leh_func_end8:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin__ctor
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin__ctor:
Leh_func_begin9:
	bx	lr
Leh_func_end9:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin_Load
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin_Load:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp19:
	add	r7, sp, #8
Ltmp20:
	push	{r8}
Ltmp21:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC10_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC10_0+8))
LPC10_0:
	add	r5, pc, r5
	ldr	r0, [r5, #124]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_27_plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor_llvm
	ldr	r0, [r5, #128]
	mov	r8, r0
	mov	r0, r4
	bl	_p_28_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__ctor
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__ctor:
Leh_func_begin11:
	bx	lr
Leh_func_end11:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__m__1_object_MonoTouch_AVFoundation_AVStatusEventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__m__1_object_MonoTouch_AVFoundation_AVStatusEventArgs:
Leh_func_begin12:
	push	{r7, lr}
Ltmp22:
	mov	r7, sp
Ltmp23:
Ltmp24:
	ldr	r0, [r0, #8]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r7, pc}
Leh_func_end12:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp25:
	add	r7, sp, #12
Ltmp26:
	push	{r10, r11}
Ltmp27:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC13_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r0, [r0, #132]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB13_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB13_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB13_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB13_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB13_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB13_7
LBB13_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB13_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end13:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs:
Leh_func_begin14:
	push	{r4, r5, r6, r7, lr}
Ltmp28:
	add	r7, sp, #12
Ltmp29:
Ltmp30:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC14_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC14_0+8))
LPC14_0:
	add	r0, pc, r0
	ldr	r0, [r0, #132]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB14_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB14_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB14_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB14_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB14_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB14_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end14:

	.private_extern	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVStatusEventArgs
	.align	2
_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVStatusEventArgs:
Leh_func_begin15:
	push	{r4, r5, r6, r7, lr}
Ltmp31:
	add	r7, sp, #12
Ltmp32:
Ltmp33:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC15_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got-(LPC15_0+8))
LPC15_0:
	add	r0, pc, r0
	ldr	r0, [r0, #132]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB15_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB15_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB15_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB15_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB15_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB15_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end15:

.zerofill __DATA,__bss,_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got,308,4
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_get_Texts
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_set_Texts_System_Collections_Generic_Dictionary_2_string_string
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_Play_string_System_Action
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__MvxTouchTextToSpeechm__0_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__2_object_System_EventArgs
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__3_object_System_EventArgs
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin__ctor
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin_Load
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__ctor
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__m__1_object_MonoTouch_AVFoundation_AVStatusEventArgs
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	.no_dead_strip	_OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVStatusEventArgs
	.no_dead_strip	_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	16
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	5
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	6
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	8
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	9
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	10
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	11
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	12
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	18
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	19
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	20
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
Lset16 = Leh_func_end15-Leh_func_begin15
	.long	Lset16
Lset17 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset17
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "OffSeasonPro.Plugin.TextToSpeech.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action
_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,120,208,77,226,13,176,160,225,96,0,139,229,1,96,160,225,100,32,139,229
	.byte 104,48,139,229,0,0,160,227,0,0,139,229,0,0,160,227,4,0,139,229,0,0,160,227,8,0,139,229,0,0,160,227
	.byte 12,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227
	.byte 40,0,139,229
bl _p_3

	.byte 0,48,160,225,7,16,160,227,0,32,160,227,0,224,211,229
bl _p_4

	.byte 255,0,0,226,0,0,80,227,131,0,0,26,8,16,139,226,6,0,160,225,0,224,214,229
bl _p_38

	.byte 102,0,0,234,8,0,139,226,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 124
	.byte 1,16,159,231,12,0,128,226,0,16,144,229,0,16,139,229,4,0,144,229,4,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 136
	.byte 0,0,159,231,4,64,155,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 136
	.byte 0,0,159,231,0,160,155,229,5,0,160,227
bl _p_9

	.byte 28,0,139,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 28
	.byte 1,16,159,231
bl _p_10

	.byte 0,80,160,225,4,16,160,225
bl _p_10

	.byte 0,80,160,225,0,0,160,227,32,0,203,229,5,0,160,225
bl _p_37

	.byte 255,0,0,226,0,0,80,227,42,0,0,26
bl _p_36

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 132
	.byte 0,0,159,231
bl _p_5

	.byte 112,0,139,229
bl _p_35

	.byte 112,0,155,229,36,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 128
	.byte 0,0,159,231,10,16,160,225
bl _p_16

	.byte 0,16,160,225,36,0,155,229,5,32,160,225,36,48,155,229,0,224,211,229
bl _p_34

	.byte 1,0,160,227,32,0,203,229,16,0,0,234,44,0,155,229,44,0,155,229,40,0,139,229,104,16,155,229,1,0,160,225
	.byte 112,16,139,229,15,224,160,225,12,240,145,229,112,0,155,229
bl _p_33

	.byte 92,0,139,229,0,0,80,227,1,0,0,10,92,0,155,229
bl _p_32

	.byte 33,0,0,235,50,0,0,234,1,0,0,234,1,0,160,227,32,0,203,229,32,0,219,229,0,0,80,227,15,0,0,10
	.byte 96,0,155,229,20,32,144,229,2,0,160,225,4,16,160,225,0,224,210,229
bl _p_19

	.byte 255,0,0,226,0,0,80,227,6,0,0,26,96,0,155,229,20,48,144,229,3,0,160,225,4,16,160,225,5,32,160,225
	.byte 0,224,211,229
bl _p_31

	.byte 8,0,139,226,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 124
	.byte 8,128,159,231
bl _p_30

	.byte 255,0,0,226,0,0,80,227,143,255,255,26,0,0,0,235,12,0,0,234,88,224,139,229,8,0,139,226,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 124
	.byte 1,16,159,231,68,0,139,229,0,224,208,229,68,0,155,229,0,16,160,227,0,16,128,229,88,192,155,229,12,240,160,225
	.byte 100,0,155,229,0,16,160,225,15,224,160,225,12,240,145,229,100,0,155,229,120,208,139,226,112,13,189,232,128,128,189,232

Lme_4:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_get_Texts
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_set_Texts_System_Collections_Generic_Dictionary_2_string_string
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_Play_string_System_Action
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__MvxTouchTextToSpeechm__0_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__2_object_System_EventArgs
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__3_object_System_EventArgs
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin__ctor
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin_Load
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__ctor
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__m__1_object_MonoTouch_AVFoundation_AVStatusEventArgs
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
.no_dead_strip _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVStatusEventArgs

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_get_Texts
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_set_Texts_System_Collections_Generic_Dictionary_2_string_string
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_Play_string_System_Action
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__MvxTouchTextToSpeechm__0_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__2_object_System_EventArgs
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3m__3_object_System_EventArgs
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin__ctor
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_Plugin_Load
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__ctor
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__PlayMp3c__AnonStorey0__m__1_object_MonoTouch_AVFoundation_AVStatusEventArgs
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
	bl _OffSeasonPro_Plugin_TextToSpeech_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_AVFoundation_AVStatusEventArgs
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 21,10,3,2
	.short 0, 10, 24
	.byte 1,9,2,2,6,10,22,2,3,3,62,4,2,255,255,255,255,188,0,0,0,0,70,3,76
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,115,20,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,94,18,11,0
	.long 0,0,103,19,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 7,14,0,15,0,16,0,17
	.long 0,18,94,19,103,20,115
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 2, 0, 0, 0, 0
	.short 0, 0, 0, 3, 0, 4, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 38,10,4,2
	.short 0, 10, 21, 32
	.byte 127,2,1,1,1,6,5,6,5,5,128,165,3,3,6,3,5,4,4,6,5,128,209,6,5,5,4,5,5,4,5,5
	.byte 129,1,4,4,12,1,6,3,5
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 21,10,3,2
	.short 0, 11, 27
	.byte 130,173,3,3,3,3,31,3,3,3,3,130,231,3,3,255,255,255,253,19,0,0,0,0,130,240,3,130,246
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,152,1,68,13
	.byte 11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 4,10,1,2
	.short 0
	.byte 130,249,7,31,19

.text
	.align 4
plt:
_mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_plt:
_p_1_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 152,298
_p_2_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string__ctor
plt_System_Collections_Generic_Dictionary_2_string_string__ctor:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 156,321
_p_3_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice
plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 160,332
_p_4_plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int
plt_MonoTouch_UIKit_UIDevice_CheckSystemVersion_int_int:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 164,337
_p_5_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 168,342
_p_6_plt_MonoTouch_AVFoundation_AVSpeechSynthesizer__ctor_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVSpeechSynthesizer__ctor
plt_MonoTouch_AVFoundation_AVSpeechSynthesizer__ctor:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 172,369
_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 176,374
_p_8_plt_MonoTouch_AVFoundation_AVSpeechSynthesizer_add_DidFinishSpeechUtterance_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVSpeechSynthesizer_add_DidFinishSpeechUtterance_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs
plt_MonoTouch_AVFoundation_AVSpeechSynthesizer_add_DidFinishSpeechUtterance_System_EventHandler_1_MonoTouch_AVFoundation_AVSpeechSynthesizerUteranceEventArgs:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 180,419
_p_9_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm:
	.no_dead_strip plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder
plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 184,424
_p_10_plt_System_IO_Path_Combine_string_string_llvm:
	.no_dead_strip plt_System_IO_Path_Combine_string_string
plt_System_IO_Path_Combine_string_string:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 188,427
_p_11_plt_System_IO_Directory_Exists_string_llvm:
	.no_dead_strip plt_System_IO_Directory_Exists_string
plt_System_IO_Directory_Exists_string:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 192,430
_p_12_plt_MonoTouch_AVFoundation_AVAudioSession_SharedInstance_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioSession_SharedInstance
plt_MonoTouch_AVFoundation_AVAudioSession_SharedInstance:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 196,433
_p_13_plt_MonoTouch_AVFoundation_AVAudioSession_get_CategoryPlayback_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioSession_get_CategoryPlayback
plt_MonoTouch_AVFoundation_AVAudioSession_get_CategoryPlayback:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 200,438
_p_14_plt_System_IO_Directory_CreateDirectory_string_llvm:
	.no_dead_strip plt_System_IO_Directory_CreateDirectory_string
plt_System_IO_Directory_CreateDirectory_string:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 204,443
_p_15_plt_string_Replace_string_string_llvm:
	.no_dead_strip plt_string_Replace_string_string
plt_string_Replace_string_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 208,446
_p_16_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 212,449
_p_17_plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action
plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_PlayMp3_string_System_Action:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 216,452
_p_18_plt_MonoTouch_AVFoundation_AVSpeechUtterance__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVSpeechUtterance__ctor_string
plt_MonoTouch_AVFoundation_AVSpeechUtterance__ctor_string:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 220,457
_p_19_plt_System_Collections_Generic_Dictionary_2_string_string_ContainsKey_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_ContainsKey_string
plt_System_Collections_Generic_Dictionary_2_string_string_ContainsKey_string:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 224,462
_p_20_plt_System_Collections_Generic_Dictionary_2_string_string_get_Item_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_get_Item_string
plt_System_Collections_Generic_Dictionary_2_string_string_get_Item_string:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 228,473
_p_21_plt_MonoTouch_Foundation_NSUrl__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSUrl__ctor_string
plt_MonoTouch_Foundation_NSUrl__ctor_string:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 232,484
_p_22_plt_MonoTouch_AVFoundation_AVAudioPlayer_FromUrl_MonoTouch_Foundation_NSUrl_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioPlayer_FromUrl_MonoTouch_Foundation_NSUrl
plt_MonoTouch_AVFoundation_AVAudioPlayer_FromUrl_MonoTouch_Foundation_NSUrl:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 236,489
_p_23_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_FinishedPlaying_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioPlayer_add_FinishedPlaying_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs
plt_MonoTouch_AVFoundation_AVAudioPlayer_add_FinishedPlaying_System_EventHandler_1_MonoTouch_AVFoundation_AVStatusEventArgs:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 240,494
_p_24_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_BeginInterruption_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioPlayer_add_BeginInterruption_System_EventHandler
plt_MonoTouch_AVFoundation_AVAudioPlayer_add_BeginInterruption_System_EventHandler:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 244,499
_p_25_plt_MonoTouch_AVFoundation_AVAudioPlayer_add_EndInterruption_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_AVFoundation_AVAudioPlayer_add_EndInterruption_System_EventHandler
plt_MonoTouch_AVFoundation_AVAudioPlayer_add_EndInterruption_System_EventHandler:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 248,504
_p_26_plt_System_Console_WriteLine_string_llvm:
	.no_dead_strip plt_System_Console_WriteLine_string
plt_System_Console_WriteLine_string:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 252,509
_p_27_plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
plt_OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 256,512
_p_28_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 260,517
_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 264,529
_p_30_plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext
plt_System_Collections_Generic_Dictionary_2_Enumerator_string_string_MoveNext:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 268,567
_p_31_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string
plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 272,578
_p_32_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 276,589
_p_33_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 280,617
_p_34_plt_System_Net_WebClient_DownloadFile_string_string_llvm:
	.no_dead_strip plt_System_Net_WebClient_DownloadFile_string_string
plt_System_Net_WebClient_DownloadFile_string_string:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 284,656
_p_35_plt_System_Net_WebClient__ctor_llvm:
	.no_dead_strip plt_System_Net_WebClient__ctor
plt_System_Net_WebClient__ctor:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 288,661
_p_36_plt__class_init_System_Net_WebClient_llvm:
	.no_dead_strip plt__class_init_System_Net_WebClient
plt__class_init_System_Net_WebClient:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 292,666
_p_37_plt_System_IO_File_Exists_string_llvm:
	.no_dead_strip plt_System_IO_File_Exists_string
plt_System_IO_File_Exists_string:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 296,671
_p_38_plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator
plt_System_Collections_Generic_Dictionary_2_string_string_GetEnumerator:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got - . + 300,674
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "OffSeasonPro.Plugin.TextToSpeech.Touch"
	.asciz "EF673B61-405B-4688-85B8-C7512A30D2B4"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "EF673B61-405B-4688-85B8-C7512A30D2B4"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "OffSeasonPro.Plugin.TextToSpeech.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_OffSeasonPro_Plugin_TextToSpeech_Touch_got
	.align 2
	.long _OffSeasonPro_Plugin_TextToSpeech_Touch__OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 38,308,39,21,11,387000831,0,833
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_OffSeasonPro_Plugin_TextToSpeech_Touch_info
	.align 2
_mono_aot_module_OffSeasonPro_Plugin_TextToSpeech_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,7,4,10,9,8,7,6,5,0,0,0,0,0,4,14,13,12,11,0,8,34,37,37,10,36,35,34,34,0,20
	.byte 15,26,28,27,23,22,26,25,24,23,22,22,21,21,20,19,18,17,16,21,0,0,0,1,29,0,1,30,0,0,0,2
	.byte 32,31,0,0,0,0,0,1,33,0,1,33,0,1,33,4,1,97,3,1,130,146,1,130,146,3,219,0,0,4,255,252
	.byte 0,0,0,1,1,7,79,255,252,0,0,0,1,1,3,219,0,0,2,255,252,0,0,0,1,1,3,219,0,0,5,12
	.byte 1,39,42,47,14,3,219,0,0,1,14,2,129,62,2,14,3,219,0,0,2,6,193,0,0,7,50,193,0,0,7,30
	.byte 3,219,0,0,2,17,1,15,17,1,1,16,1,130,146,136,199,17,1,5,14,2,129,61,2,14,2,4,1,14,2,30
	.byte 2,14,3,219,0,0,5,6,193,0,0,13,50,193,0,0,13,30,3,219,0,0,5,16,2,2,1,5,16,2,2,1
	.byte 6,14,1,130,63,6,193,0,0,9,50,193,0,0,9,30,1,130,63,6,193,0,0,8,50,193,0,0,8,17,1,128
	.byte 133,17,1,128,185,14,2,2,1,34,255,254,0,0,0,1,255,43,0,0,1,33,14,3,219,0,0,3,17,1,23,14
	.byte 2,128,226,4,14,3,219,0,0,4,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116
	.byte 0,3,255,254,0,0,0,1,202,0,0,11,3,194,0,4,148,3,194,0,4,151,7,24,109,111,110,111,95,111,98,106
	.byte 101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,194,0,8,82,7,42,108,108,118,109,95,116,104,114
	.byte 111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105
	.byte 110,101,0,3,194,0,8,86,3,144,189,3,135,38,3,134,144,3,194,0,0,22,3,194,0,0,21,3,134,139,3,147
	.byte 113,3,147,133,3,193,0,0,6,3,194,0,8,76,3,255,254,0,0,0,1,202,0,0,30,3,255,254,0,0,0,1
	.byte 202,0,0,34,3,194,0,1,32,3,194,0,0,13,3,194,0,0,1,3,194,0,0,3,3,194,0,0,5,3,142,68
	.byte 3,193,0,0,1,3,255,254,0,0,0,1,255,43,0,0,1,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105
	.byte 110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,255,254,0,0,0,1,202,0
	.byte 0,32,3,255,254,0,0,0,1,202,0,0,31,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101
	.byte 120,99,101,112,116,105,111,110,0,7,36,109,111,110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110
	.byte 105,97,98,108,101,95,101,120,99,101,112,116,105,111,110,0,3,196,0,6,223,3,196,0,6,218,15,2,128,226,4,3
	.byte 134,165,3,255,254,0,0,0,1,202,0,0,22,16,0,0,16,0,0,16,0,0,16,0,0,38,0,1,2,0,44,3
	.byte 1,130,64,129,96,129,156,129,156,2,0,130,152,128,152,130,96,130,100,1,4,129,220,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1
	.byte 8,128,232,24,8,0,4,146,178,146,175,146,174,146,172,193,0,0,2,193,0,0,3,193,0,0,4,193,0,0,5,5
	.byte 128,144,8,0,0,1,146,178,146,175,146,174,146,172,193,0,0,11,4,128,160,12,0,0,4,146,178,146,175,146,174,146
	.byte 172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_5:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_4:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM10=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM12=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_3:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM15=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM16=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM17=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM18=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM19=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM20=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM21=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_2:

	.byte 5
	.asciz "MonoTouch_AVFoundation_AVAudioPlayer"

	.byte 24,16
LDIFF_SYM24=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM25=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,20,0,7
	.asciz "MonoTouch_AVFoundation_AVAudioPlayer"

LDIFF_SYM26=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM27=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM28=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_6:

	.byte 5
	.asciz "MonoTouch_AVFoundation_AVSpeechSynthesizer"

	.byte 24,16
LDIFF_SYM29=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM30=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,20,0,7
	.asciz "MonoTouch_AVFoundation_AVSpeechSynthesizer"

LDIFF_SYM31=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM32=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM32
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM33=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM33
LTDIE_12:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM34=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM35=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM35
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM36=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM37=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_11:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM38=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM39=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM39
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM40=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM40
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM41=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_10:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM42=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM43=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM44=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM44
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM45=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM45
LTDIE_14:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM46=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM47=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM48=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM49=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM50=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_13:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM51=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM52=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM53=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM54=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM55=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM56=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_9:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM57=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM58=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM59=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM59
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM60=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM61=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM62=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM63=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM63
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM64=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM65=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM66=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM67=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM68=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM69=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_8:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM70=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM71=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM72=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM73=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM74=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM75=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_7:

	.byte 5
	.asciz "System_Action"

	.byte 52,16
LDIFF_SYM76=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,0,0,7
	.asciz "System_Action"

LDIFF_SYM77=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM78=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM79=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_16:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM80=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM81=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM82=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_17:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM83=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM84=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM85=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM85
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM86=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM86
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM87=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_15:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM88=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM88
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM89=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM89
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM90=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM91=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM92=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM93=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM94=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM95=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM96=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM97=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM98=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM99=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM100=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM101=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_0:

	.byte 5
	.asciz "OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech"

	.byte 24,16
LDIFF_SYM102=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2,35,0,6
	.asciz "_player"

LDIFF_SYM103=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,8,6
	.asciz "_speechSynthesizer"

LDIFF_SYM104=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,12,6
	.asciz "_finished"

LDIFF_SYM105=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,16,6
	.asciz "<Texts>k__BackingField"

LDIFF_SYM106=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,20,0,7
	.asciz "OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech"

LDIFF_SYM107=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM108=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM109=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_20:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM110=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM111=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM112=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM113=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM114=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_22:

	.byte 5
	.asciz "System_ComponentModel_ListEntry"

	.byte 20,16
LDIFF_SYM115=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2,35,0,6
	.asciz "key"

LDIFF_SYM116=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM116
	.byte 2,35,8,6
	.asciz "value"

LDIFF_SYM117=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 2,35,12,6
	.asciz "next"

LDIFF_SYM118=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,35,16,0,7
	.asciz "System_ComponentModel_ListEntry"

LDIFF_SYM119=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM119
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM120=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM121=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_21:

	.byte 5
	.asciz "System_ComponentModel_EventHandlerList"

	.byte 16,16
LDIFF_SYM122=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 2,35,0,6
	.asciz "entries"

LDIFF_SYM123=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,8,6
	.asciz "null_entry"

LDIFF_SYM124=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,12,0,7
	.asciz "System_ComponentModel_EventHandlerList"

LDIFF_SYM125=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM126=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM127=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_23:

	.byte 17
	.asciz "System_ComponentModel_ISite"

	.byte 8,7
	.asciz "System_ComponentModel_ISite"

LDIFF_SYM128=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM129=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM130=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM130
LTDIE_19:

	.byte 5
	.asciz "System_ComponentModel_Component"

	.byte 20,16
LDIFF_SYM131=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,0,6
	.asciz "event_handlers"

LDIFF_SYM132=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM132
	.byte 2,35,12,6
	.asciz "mySite"

LDIFF_SYM133=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM133
	.byte 2,35,16,0,7
	.asciz "System_ComponentModel_Component"

LDIFF_SYM134=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM135=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM136=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_24:

	.byte 17
	.asciz "System_Net_ICredentials"

	.byte 8,7
	.asciz "System_Net_ICredentials"

LDIFF_SYM137=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM138=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM139=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_29:

	.byte 5
	.asciz "_HashKeys"

	.byte 12,16
LDIFF_SYM140=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM140
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM141=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM141
	.byte 2,35,8,0,7
	.asciz "_HashKeys"

LDIFF_SYM142=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM143=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM144=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_30:

	.byte 5
	.asciz "_HashValues"

	.byte 12,16
LDIFF_SYM145=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM146=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM146
	.byte 2,35,8,0,7
	.asciz "_HashValues"

LDIFF_SYM147=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM148=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM149=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM149
LTDIE_31:

	.byte 17
	.asciz "System_Collections_IHashCodeProvider"

	.byte 8,7
	.asciz "System_Collections_IHashCodeProvider"

LDIFF_SYM150=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_32:

	.byte 17
	.asciz "System_Collections_IComparer"

	.byte 8,7
	.asciz "System_Collections_IComparer"

LDIFF_SYM153=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM153
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM154=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM155=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_33:

	.byte 17
	.asciz "System_Collections_IEqualityComparer"

	.byte 8,7
	.asciz "System_Collections_IEqualityComparer"

LDIFF_SYM156=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM157=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM157
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM158=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_34:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM159=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM160=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM161=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM162=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM163=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_28:

	.byte 5
	.asciz "System_Collections_Hashtable"

	.byte 52,16
LDIFF_SYM164=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM165=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 2,35,8,6
	.asciz "hashes"

LDIFF_SYM166=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,12,6
	.asciz "hashKeys"

LDIFF_SYM167=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 2,35,16,6
	.asciz "hashValues"

LDIFF_SYM168=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,20,6
	.asciz "hcpRef"

LDIFF_SYM169=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,24,6
	.asciz "comparerRef"

LDIFF_SYM170=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,28,6
	.asciz "equalityComparer"

LDIFF_SYM171=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,32,6
	.asciz "inUse"

LDIFF_SYM172=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,36,6
	.asciz "modificationCount"

LDIFF_SYM173=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,40,6
	.asciz "loadFactor"

LDIFF_SYM174=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,44,6
	.asciz "threshold"

LDIFF_SYM175=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM175
	.byte 2,35,48,0,7
	.asciz "System_Collections_Hashtable"

LDIFF_SYM176=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM176
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM177=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM178=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM178
LTDIE_35:

	.byte 5
	.asciz "__Item"

	.byte 16,16
LDIFF_SYM179=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,0,6
	.asciz "key"

LDIFF_SYM180=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,8,6
	.asciz "value"

LDIFF_SYM181=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,12,0,7
	.asciz "__Item"

LDIFF_SYM182=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM182
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM183=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM184=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM184
LTDIE_36:

	.byte 5
	.asciz "System_Collections_ArrayList"

	.byte 20,16
LDIFF_SYM185=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM186=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM187=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM188=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,16,0,7
	.asciz "System_Collections_ArrayList"

LDIFF_SYM189=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM189
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM190=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM190
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM191=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_37:

	.byte 5
	.asciz "_KeysCollection"

	.byte 12,16
LDIFF_SYM192=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,0,6
	.asciz "m_collection"

LDIFF_SYM193=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,35,8,0,7
	.asciz "_KeysCollection"

LDIFF_SYM194=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM195=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM196=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM196
LTDIE_27:

	.byte 5
	.asciz "System_Collections_Specialized_NameObjectCollectionBase"

	.byte 44,16
LDIFF_SYM197=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,35,0,6
	.asciz "m_ItemsContainer"

LDIFF_SYM198=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM198
	.byte 2,35,8,6
	.asciz "m_NullKeyItem"

LDIFF_SYM199=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM199
	.byte 2,35,12,6
	.asciz "m_ItemsArray"

LDIFF_SYM200=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 2,35,16,6
	.asciz "m_hashprovider"

LDIFF_SYM201=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,35,20,6
	.asciz "m_comparer"

LDIFF_SYM202=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 2,35,24,6
	.asciz "m_defCapacity"

LDIFF_SYM203=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,36,6
	.asciz "m_readonly"

LDIFF_SYM204=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,40,6
	.asciz "keyscoll"

LDIFF_SYM205=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,28,6
	.asciz "equality_comparer"

LDIFF_SYM206=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,32,0,7
	.asciz "System_Collections_Specialized_NameObjectCollectionBase"

LDIFF_SYM207=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM207
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM208=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM209=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_26:

	.byte 5
	.asciz "System_Collections_Specialized_NameValueCollection"

	.byte 52,16
LDIFF_SYM210=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2,35,0,6
	.asciz "cachedAllKeys"

LDIFF_SYM211=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM211
	.byte 2,35,44,6
	.asciz "cachedAll"

LDIFF_SYM212=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2,35,48,0,7
	.asciz "System_Collections_Specialized_NameValueCollection"

LDIFF_SYM213=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM214=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM214
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM215=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM215
LTDIE_25:

	.byte 5
	.asciz "System_Net_WebHeaderCollection"

	.byte 60,16
LDIFF_SYM216=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,35,0,6
	.asciz "headerRestriction"

LDIFF_SYM217=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM217
	.byte 2,35,52,0,7
	.asciz "System_Net_WebHeaderCollection"

LDIFF_SYM218=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM218
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM219=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM220=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM220
LTDIE_39:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM221=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM222=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM223=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM224=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM225=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM225
LTDIE_40:

	.byte 5
	.asciz "System_UriParser"

	.byte 16,16
LDIFF_SYM226=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,0,6
	.asciz "scheme_name"

LDIFF_SYM227=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,8,6
	.asciz "default_port"

LDIFF_SYM228=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,12,0,7
	.asciz "System_UriParser"

LDIFF_SYM229=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM229
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM230=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM230
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM231=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM231
LTDIE_38:

	.byte 5
	.asciz "System_Uri"

	.byte 80,16
LDIFF_SYM232=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,0,6
	.asciz "isUnixFilePath"

LDIFF_SYM233=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,52,6
	.asciz "source"

LDIFF_SYM234=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,8,6
	.asciz "scheme"

LDIFF_SYM235=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,12,6
	.asciz "host"

LDIFF_SYM236=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,16,6
	.asciz "port"

LDIFF_SYM237=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,56,6
	.asciz "path"

LDIFF_SYM238=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2,35,20,6
	.asciz "query"

LDIFF_SYM239=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM239
	.byte 2,35,24,6
	.asciz "fragment"

LDIFF_SYM240=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM240
	.byte 2,35,28,6
	.asciz "userinfo"

LDIFF_SYM241=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM241
	.byte 2,35,32,6
	.asciz "isUnc"

LDIFF_SYM242=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM242
	.byte 2,35,60,6
	.asciz "isOpaquePart"

LDIFF_SYM243=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,61,6
	.asciz "isAbsoluteUri"

LDIFF_SYM244=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,62,6
	.asciz "scope_id"

LDIFF_SYM245=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,64,6
	.asciz "userEscaped"

LDIFF_SYM246=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM246
	.byte 2,35,72,6
	.asciz "cachedAbsoluteUri"

LDIFF_SYM247=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,36,6
	.asciz "cachedToString"

LDIFF_SYM248=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,40,6
	.asciz "cachedLocalPath"

LDIFF_SYM249=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,35,44,6
	.asciz "cachedHashCode"

LDIFF_SYM250=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 2,35,76,6
	.asciz "parser"

LDIFF_SYM251=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,48,0,7
	.asciz "System_Uri"

LDIFF_SYM252=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM252
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM253=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM253
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM254=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_42:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM255=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM256=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM256
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM257=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM257
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM258=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM258
LTDIE_43:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM259=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM260=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM260
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM261=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM261
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM262=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_41:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM263=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM263
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM264=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM264
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM265=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM266=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM266
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM267=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM267
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM268=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM268
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM269=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM270=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM271=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM272=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM273=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM274=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM274
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM275=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM275
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM276=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM277=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM277
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM278=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM278
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM279=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM279
LTDIE_44:

	.byte 17
	.asciz "System_Net_IWebProxy"

	.byte 8,7
	.asciz "System_Net_IWebProxy"

LDIFF_SYM280=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM281=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM281
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM282=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM282
LTDIE_47:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM283=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM283
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM284=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM284
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM285=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM285
LTDIE_50:

	.byte 5
	.asciz "System_UInt64"

	.byte 16,16
LDIFF_SYM286=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM287=LDIE_U8 - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 2,35,8,0,7
	.asciz "System_UInt64"

LDIFF_SYM288=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM288
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM289=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM289
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM290=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM290
LTDIE_49:

	.byte 5
	.asciz "_Node"

	.byte 44,16
LDIFF_SYM291=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM291
	.byte 2,35,0,6
	.asciz "Marked"

LDIFF_SYM292=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,35,32,6
	.asciz "Key"

LDIFF_SYM293=LDIE_U8 - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,35,36,6
	.asciz "SubKey"

LDIFF_SYM294=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,8,6
	.asciz "Data"

LDIFF_SYM295=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,16,6
	.asciz "Next"

LDIFF_SYM296=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,28,0,7
	.asciz "_Node"

LDIFF_SYM297=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM297
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM298=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM298
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM299=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM299
LTDIE_48:

	.byte 5
	.asciz "System_Collections_Concurrent_SplitOrderedList`2"

	.byte 36,16
LDIFF_SYM300=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,0,6
	.asciz "head"

LDIFF_SYM301=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,8,6
	.asciz "tail"

LDIFF_SYM302=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,12,6
	.asciz "buckets"

LDIFF_SYM303=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM303
	.byte 2,35,16,6
	.asciz "count"

LDIFF_SYM304=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2,35,24,6
	.asciz "size"

LDIFF_SYM305=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,35,28,6
	.asciz "slim"

LDIFF_SYM306=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 2,35,32,6
	.asciz "comparer"

LDIFF_SYM307=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 2,35,20,0,7
	.asciz "System_Collections_Concurrent_SplitOrderedList`2"

LDIFF_SYM308=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM308
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM309=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM309
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM310=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM310
LTDIE_46:

	.byte 5
	.asciz "System_Collections_Concurrent_ConcurrentDictionary`2"

	.byte 16,16
LDIFF_SYM311=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 2,35,0,6
	.asciz "comparer"

LDIFF_SYM312=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM312
	.byte 2,35,8,6
	.asciz "internalDictionary"

LDIFF_SYM313=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 2,35,12,0,7
	.asciz "System_Collections_Concurrent_ConcurrentDictionary`2"

LDIFF_SYM314=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM314
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM315=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM315
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM316=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM316
LTDIE_57:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM317=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM317
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM318=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM318
LTDIE_57_POINTER:

	.byte 13
LDIFF_SYM319=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM319
LTDIE_57_REFERENCE:

	.byte 14
LDIFF_SYM320=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM320
LTDIE_56:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM321=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM322=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM323=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM324=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM325=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM326=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_56_POINTER:

	.byte 13
LDIFF_SYM327=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM327
LTDIE_56_REFERENCE:

	.byte 14
LDIFF_SYM328=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM328
LTDIE_55:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM329=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM330=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM330
LTDIE_55_POINTER:

	.byte 13
LDIFF_SYM331=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM331
LTDIE_55_REFERENCE:

	.byte 14
LDIFF_SYM332=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM332
LTDIE_54:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM333=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM333
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM334=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM334
LTDIE_54_POINTER:

	.byte 13
LDIFF_SYM335=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM335
LTDIE_54_REFERENCE:

	.byte 14
LDIFF_SYM336=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM336
LTDIE_53:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM337=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM338=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM338
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM339=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM339
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM340=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM340
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM341=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM341
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM342=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM342
LTDIE_52:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM343=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM343
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM344=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM344
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM345=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM345
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM346=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM346
LTDIE_51:

	.byte 5
	.asciz "System_Threading_ManualResetEvent"

	.byte 20,16
LDIFF_SYM347=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM347
	.byte 2,35,0,0,7
	.asciz "System_Threading_ManualResetEvent"

LDIFF_SYM348=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM348
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM349=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM349
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM350=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM350
LTDIE_59:

	.byte 5
	.asciz "System_Threading_TimerCallback"

	.byte 52,16
LDIFF_SYM351=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM351
	.byte 2,35,0,0,7
	.asciz "System_Threading_TimerCallback"

LDIFF_SYM352=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM352
LTDIE_59_POINTER:

	.byte 13
LDIFF_SYM353=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM353
LTDIE_59_REFERENCE:

	.byte 14
LDIFF_SYM354=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM354
LTDIE_58:

	.byte 5
	.asciz "System_Threading_Timer"

	.byte 48,16
LDIFF_SYM355=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM355
	.byte 2,35,0,6
	.asciz "callback"

LDIFF_SYM356=LTDIE_59_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM356
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM357=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM357
	.byte 2,35,16,6
	.asciz "due_time_ms"

LDIFF_SYM358=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM358
	.byte 2,35,20,6
	.asciz "period_ms"

LDIFF_SYM359=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM359
	.byte 2,35,28,6
	.asciz "next_run"

LDIFF_SYM360=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM360
	.byte 2,35,36,6
	.asciz "disposed"

LDIFF_SYM361=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM361
	.byte 2,35,44,0,7
	.asciz "System_Threading_Timer"

LDIFF_SYM362=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM362
LTDIE_58_POINTER:

	.byte 13
LDIFF_SYM363=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM363
LTDIE_58_REFERENCE:

	.byte 14
LDIFF_SYM364=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM364
LTDIE_45:

	.byte 5
	.asciz "System_Threading_CancellationTokenSource"

	.byte 32,16
LDIFF_SYM365=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM365
	.byte 2,35,0,6
	.asciz "canceled"

LDIFF_SYM366=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,35,24,6
	.asciz "disposed"

LDIFF_SYM367=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM367
	.byte 2,35,25,6
	.asciz "currId"

LDIFF_SYM368=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM368
	.byte 2,35,28,6
	.asciz "callbacks"

LDIFF_SYM369=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM369
	.byte 2,35,8,6
	.asciz "linkedTokens"

LDIFF_SYM370=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM370
	.byte 2,35,12,6
	.asciz "handle"

LDIFF_SYM371=LTDIE_51_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM371
	.byte 2,35,16,6
	.asciz "timer"

LDIFF_SYM372=LTDIE_58_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM372
	.byte 2,35,20,0,7
	.asciz "System_Threading_CancellationTokenSource"

LDIFF_SYM373=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM373
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM374=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM374
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM375=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM375
LTDIE_60:

	.byte 5
	.asciz "System_Net_DownloadProgressChangedEventHandler"

	.byte 52,16
LDIFF_SYM376=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM376
	.byte 2,35,0,0,7
	.asciz "System_Net_DownloadProgressChangedEventHandler"

LDIFF_SYM377=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM377
LTDIE_60_POINTER:

	.byte 13
LDIFF_SYM378=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM378
LTDIE_60_REFERENCE:

	.byte 14
LDIFF_SYM379=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM379
LTDIE_18:

	.byte 5
	.asciz "System_Net_WebClient"

	.byte 60,16
LDIFF_SYM380=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM380
	.byte 2,35,0,6
	.asciz "credentials"

LDIFF_SYM381=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM381
	.byte 2,35,20,6
	.asciz "headers"

LDIFF_SYM382=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM382
	.byte 2,35,24,6
	.asciz "responseHeaders"

LDIFF_SYM383=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM383
	.byte 2,35,28,6
	.asciz "baseAddress"

LDIFF_SYM384=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM384
	.byte 2,35,32,6
	.asciz "queryString"

LDIFF_SYM385=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM385
	.byte 2,35,36,6
	.asciz "is_busy"

LDIFF_SYM386=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM386
	.byte 2,35,56,6
	.asciz "async"

LDIFF_SYM387=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM387
	.byte 2,35,57,6
	.asciz "proxySet"

LDIFF_SYM388=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM388
	.byte 2,35,58,6
	.asciz "encoding"

LDIFF_SYM389=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM389
	.byte 2,35,40,6
	.asciz "proxy"

LDIFF_SYM390=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM390
	.byte 2,35,44,6
	.asciz "cts"

LDIFF_SYM391=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM391
	.byte 2,35,48,6
	.asciz "DownloadProgressChanged"

LDIFF_SYM392=LTDIE_60_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM392
	.byte 2,35,52,0,7
	.asciz "System_Net_WebClient"

LDIFF_SYM393=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM393
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM394=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM394
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM395=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM395
LTDIE_62:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM396=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM396
LTDIE_62_POINTER:

	.byte 13
LDIFF_SYM397=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM397
LTDIE_62_REFERENCE:

	.byte 14
LDIFF_SYM398=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM398
LTDIE_61:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM399=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM399
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM400=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM400
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM401=LTDIE_61_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM401
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM402=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM402
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM403=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM403
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM404=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM404
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM405=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM405
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM406=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM406
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM407=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM407
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM408=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM408
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM409=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM409
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM410=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM410
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM411=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM411
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM412=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM412
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM413=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM413
LTDIE_61_POINTER:

	.byte 13
LDIFF_SYM414=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM414
LTDIE_61_REFERENCE:

	.byte 14
LDIFF_SYM415=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM415
	.byte 2
	.asciz "OffSeasonPro.Plugin.TextToSpeech.Touch.MvxTouchTextToSpeech:TextToSpeech"
	.long _OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action
	.long Lme_4

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM416=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM416
	.byte 3,123,224,0,3
	.asciz "textsAndFiles"

LDIFF_SYM417=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM417
	.byte 1,86,3
	.asciz "Finished"

LDIFF_SYM418=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM418
	.byte 3,123,228,0,3
	.asciz "ErrorOccured"

LDIFF_SYM419=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM419
	.byte 3,123,232,0,11
	.asciz "V_0"

LDIFF_SYM420=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM420
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM421=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM421
	.byte 2,123,8,11
	.asciz "V_2"

LDIFF_SYM422=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM422
	.byte 1,84,11
	.asciz "V_3"

LDIFF_SYM423=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM423
	.byte 1,90,11
	.asciz "V_4"

LDIFF_SYM424=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM424
	.byte 2,123,28,11
	.asciz "V_5"

LDIFF_SYM425=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM425
	.byte 1,85,11
	.asciz "V_6"

LDIFF_SYM426=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM426
	.byte 2,123,32,11
	.asciz "V_7"

LDIFF_SYM427=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM427
	.byte 2,123,36,11
	.asciz "V_8"

LDIFF_SYM428=LTDIE_61_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM428
	.byte 2,123,40,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM429=Lfde0_end - Lfde0_start
	.long LDIFF_SYM429
Lfde0_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action

LDIFF_SYM430=Lme_4 - _OffSeasonPro_Plugin_TextToSpeech_Touch_MvxTouchTextToSpeech_TextToSpeech_System_Collections_Generic_Dictionary_2_string_string_System_Action_System_Action
	.long LDIFF_SYM430
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,152,1,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
