	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string:
Leh_func_begin1:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	bl	_p_1_plt_System_IO_File_Exists_string_llvm
	pop	{r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_FolderExists_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_FolderExists_string:
Leh_func_begin2:
	push	{r7, lr}
Ltmp3:
	mov	r7, sp
Ltmp4:
Ltmp5:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	bl	_p_2_plt_System_IO_Directory_Exists_string_llvm
	pop	{r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_PathCombine_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_PathCombine_string_string:
Leh_func_begin3:
	push	{r7, lr}
Ltmp6:
	mov	r7, sp
Ltmp7:
Ltmp8:
	mov	r0, r1
	mov	r1, r2
	bl	_p_3_plt_System_IO_Path_Combine_string_string_llvm
	pop	{r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_EnsureFolderExists_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_EnsureFolderExists_string:
Leh_func_begin4:
	push	{r4, r7, lr}
Ltmp9:
	add	r7, sp, #4
Ltmp10:
Ltmp11:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	mov	r4, r0
	bl	_p_2_plt_System_IO_Directory_Exists_string_llvm
	tst	r0, #255
	popne	{r4, r7, pc}
	mov	r0, r4
	bl	_p_4_plt_System_IO_Directory_CreateDirectory_string_llvm
	pop	{r4, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_GetFilesIn_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_GetFilesIn_string:
Leh_func_begin5:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
Ltmp14:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	bl	_p_5_plt_System_IO_Directory_GetFiles_string_llvm
	pop	{r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFile_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFile_string:
Leh_func_begin6:
	push	{r7, lr}
Ltmp15:
	mov	r7, sp
Ltmp16:
Ltmp17:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	bl	_p_6_plt_System_IO_File_Delete_string_llvm
	pop	{r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFolder_string_bool
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFolder_string_bool:
Leh_func_begin7:
	push	{r4, r7, lr}
Ltmp18:
	add	r7, sp, #4
Ltmp19:
Ltmp20:
	mov	r4, r2
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	mov	r1, r4
	bl	_p_7_plt_System_IO_Directory_Delete_string_bool_llvm
	pop	{r4, r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadTextFile_string_string_
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadTextFile_string_string_:
Leh_func_begin8:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
	push	{r10, r11}
Ltmp23:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC8_1+8))
	mov	r6, r0
	mov	r10, r2
	mov	r11, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC8_1+8))
LPC8_1:
	add	r5, pc, r5
	ldr	r0, [r5, #16]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	mov	r1, #0
	str	r1, [r4, #8]
	cmp	r4, #0
	beq	LBB8_2
	ldr	r0, [r5, #20]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r1, r11
	str	r4, [r2, #16]
	ldr	r0, [r5, #24]
	str	r0, [r2, #20]
	ldr	r0, [r5, #28]
	str	r0, [r2, #28]
	ldr	r0, [r5, #32]
	str	r0, [r2, #12]
	mov	r0, r6
	bl	_p_10_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm
	ldr	r1, [r4, #8]
	str	r1, [r10]
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp24:
LBB8_2:
	ldr	r0, LCPI8_0
LPC8_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_9_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI8_0:
	.long	Ltmp24-(LPC8_0+8)
	.end_data_region
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_byte___
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_byte___:
Leh_func_begin9:
	push	{r4, r5, r6, r7, lr}
Ltmp25:
	add	r7, sp, #12
Ltmp26:
	push	{r10, r11}
Ltmp27:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC9_1+8))
	mov	r6, r0
	mov	r10, r2
	mov	r11, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC9_1+8))
LPC9_1:
	add	r5, pc, r5
	ldr	r0, [r5, #36]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	mov	r1, #0
	str	r1, [r4, #8]
	cmp	r4, #0
	beq	LBB9_2
	ldr	r0, [r5, #20]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r1, r11
	str	r4, [r2, #16]
	ldr	r0, [r5, #40]
	str	r0, [r2, #20]
	ldr	r0, [r5, #44]
	str	r0, [r2, #28]
	ldr	r0, [r5, #32]
	str	r0, [r2, #12]
	mov	r0, r6
	bl	_p_10_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm
	ldr	r1, [r4, #8]
	str	r1, [r10]
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp28:
LBB9_2:
	ldr	r0, LCPI9_0
LPC9_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_9_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI9_0:
	.long	Ltmp28-(LPC9_0+8)
	.end_data_region
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool:
Leh_func_begin10:
	push	{r7, lr}
Ltmp29:
	mov	r7, sp
Ltmp30:
Ltmp31:
	bl	_p_10_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm
	pop	{r7, pc}
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_string:
Leh_func_begin11:
	push	{r4, r5, r6, r7, lr}
Ltmp32:
	add	r7, sp, #12
Ltmp33:
	push	{r10, r11}
Ltmp34:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC11_1+8))
	mov	r11, r0
	mov	r4, r2
	mov	r10, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC11_1+8))
LPC11_1:
	add	r5, pc, r5
	ldr	r0, [r5, #48]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	str	r4, [r6, #8]
	cmp	r6, #0
	beq	LBB11_2
	ldr	r0, [r5, #52]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r1, r10
	str	r6, [r2, #16]
	ldr	r0, [r5, #56]
	str	r0, [r2, #20]
	ldr	r0, [r5, #60]
	str	r0, [r2, #28]
	ldr	r0, [r5, #64]
	str	r0, [r2, #12]
	mov	r0, r11
	bl	_p_11_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp35:
LBB11_2:
	ldr	r0, LCPI11_0
LPC11_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_9_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI11_0:
	.long	Ltmp35-(LPC11_0+8)
	.end_data_region
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Collections_Generic_IEnumerable_1_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Collections_Generic_IEnumerable_1_byte:
Leh_func_begin12:
	push	{r4, r5, r6, r7, lr}
Ltmp36:
	add	r7, sp, #12
Ltmp37:
	push	{r10, r11}
Ltmp38:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC12_1+8))
	mov	r11, r0
	mov	r4, r2
	mov	r10, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC12_1+8))
LPC12_1:
	add	r5, pc, r5
	ldr	r0, [r5, #68]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	str	r4, [r6, #8]
	cmp	r6, #0
	beq	LBB12_2
	ldr	r0, [r5, #52]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r1, r10
	str	r6, [r2, #16]
	ldr	r0, [r5, #72]
	str	r0, [r2, #20]
	ldr	r0, [r5, #76]
	str	r0, [r2, #28]
	ldr	r0, [r5, #64]
	str	r0, [r2, #12]
	mov	r0, r11
	bl	_p_11_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp39:
LBB12_2:
	ldr	r0, LCPI12_0
LPC12_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_9_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI12_0:
	.long	Ltmp39-(LPC12_0+8)
	.end_data_region
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Action_1_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Action_1_System_IO_Stream:
Leh_func_begin13:
	push	{r7, lr}
Ltmp40:
	mov	r7, sp
Ltmp41:
Ltmp42:
	bl	_p_11_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_llvm
	pop	{r7, pc}
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_NativePath_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_NativePath_string:
Leh_func_begin14:
	push	{r7, lr}
Ltmp43:
	mov	r7, sp
Ltmp44:
Ltmp45:
	ldr	r2, [r0]
	ldr	r2, [r2, #108]
	blx	r2
	pop	{r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__ctor:
Leh_func_begin15:
	bx	lr
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore_FullPath_string
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore_FullPath_string:
Leh_func_begin16:
	push	{r4, r5, r7, lr}
Ltmp46:
	add	r7, sp, #8
Ltmp47:
Ltmp48:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC16_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC16_0+8))
LPC16_0:
	add	r5, pc, r5
	ldr	r1, [r5, #80]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_15_plt_string_StartsWith_string_llvm
	tst	r0, #255
	beq	LBB16_2
	ldr	r0, [r5, #80]
	ldr	r1, [r0, #8]
	mov	r0, r4
	bl	_p_17_plt_string_Substring_int_llvm
	pop	{r4, r5, r7, pc}
LBB16_2:
	mov	r0, #5
	bl	_p_16_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm
	mov	r1, r4
	bl	_p_3_plt_System_IO_Path_Combine_string_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore__ctor:
Leh_func_begin17:
	bx	lr
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin_Load
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin_Load:
Leh_func_begin18:
	push	{r4, r7, lr}
Ltmp49:
	add	r7, sp, #4
Ltmp50:
	push	{r8}
Ltmp51:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC18_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC18_0+8))
LPC18_0:
	add	r4, pc, r4
	ldr	r0, [r4, #84]
	bl	_p_18_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #88]
	mov	r8, r1
	bl	_p_19_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin__ctor:
Leh_func_begin19:
	bx	lr
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__ctor:
Leh_func_begin20:
	bx	lr
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__ctor:
Leh_func_begin21:
	bx	lr
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__ctor:
Leh_func_begin22:
	bx	lr
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__ctor:
Leh_func_begin23:
	bx	lr
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin24:
	push	{r4, r5, r7, lr}
Ltmp52:
	add	r7, sp, #8
Ltmp53:
Ltmp54:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC24_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #116]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB24_2
	bl	_p_26_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB24_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB24_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB24_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB24_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB24_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin25:
	push	{r4, r5, r7, lr}
Ltmp55:
	add	r7, sp, #8
Ltmp56:
Ltmp57:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC25_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC25_0+8))
LPC25_0:
	add	r0, pc, r0
	ldr	r0, [r0, #116]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB25_2
	bl	_p_26_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB25_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB25_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB25_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB25_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB25_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current:
Leh_func_begin26:
	push	{r4, r5, r7, lr}
Ltmp58:
	add	r7, sp, #8
Ltmp59:
	push	{r8}
Ltmp60:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC26_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC26_0+8))
LPC26_0:
	add	r5, pc, r5
	ldr	r1, [r5, #120]
	mov	r8, r1
	bl	_p_27_plt_System_Array_InternalEnumerator_1_byte_get_Current_llvm
	mov	r4, r0
	ldr	r0, [r5, #124]
	bl	_p_28_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	strb	r4, [r0, #8]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array:
Leh_func_begin27:
	mov	r2, r1
	mvn	r3, #1
	strd	r2, r3, [r0]
	bx	lr
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current:
Leh_func_begin28:
	push	{r7, lr}
Ltmp61:
	mov	r7, sp
Ltmp62:
	push	{r8}
Ltmp63:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	beq	LBB28_3
	ldr	r1, [r0, #4]
	cmn	r1, #1
	beq	LBB28_4
	ldr	r2, [r0]
	ldr	r1, [r0]
	ldr	r1, [r1, #12]
	ldr	r9, [r0, #4]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC28_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC28_0+8))
LPC28_0:
	add	r3, pc, r3
	ldr	r3, [r3, #128]
	ldr	r0, [r2]
	sub	r1, r1, #1
	mov	r0, r2
	sub	r1, r1, r9
	mov	r8, r3
	bl	_p_29_plt_System_Array_InternalArray__get_Item_byte_int_llvm
	pop	{r8}
	pop	{r7, pc}
LBB28_3:
	movw	r0, #47632
	b	LBB28_5
LBB28_4:
	movw	r0, #47718
LBB28_5:
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose:
Leh_func_begin29:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end29:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext:
Leh_func_begin30:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	ldreq	r1, [r0]
	ldreq	r1, [r1, #12]
	streq	r1, [r0, #4]
	mov	r1, #0
	ldr	r2, [r0, #4]
	cmn	r2, #1
	beq	LBB30_2
	ldr	r1, [r0, #4]
	cmp	r1, #0
	sub	r2, r1, #1
	movne	r1, #1
	str	r2, [r0, #4]
LBB30_2:
	mov	r0, r1
	bx	lr
Leh_func_end30:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset:
Leh_func_begin31:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end31:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_byte:
Leh_func_begin32:
	push	{r4, r7, lr}
Ltmp64:
	add	r7, sp, #4
Ltmp65:
	push	{r8}
Ltmp66:
	sub	sp, sp, #16
	bic	sp, sp, #7
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC32_0+8))
	mov	r1, r0
	mov	r0, #0
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC32_0+8))
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
LPC32_0:
	add	r4, pc, r4
	ldr	r2, [r4, #120]
	mov	r8, r2
	bl	_p_33_plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	ldr	r0, [r4, #120]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end32:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_Count
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_Count:
Leh_func_begin33:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end33:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly:
Leh_func_begin34:
	mov	r0, #1
	bx	lr
Leh_func_end34:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Clear
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Clear:
Leh_func_begin35:
	push	{r7, lr}
Ltmp67:
	mov	r7, sp
Ltmp68:
Ltmp69:
	movw	r0, #45355
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end35:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Add_byte_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Add_byte_byte:
Leh_func_begin36:
	push	{r7, lr}
Ltmp70:
	mov	r7, sp
Ltmp71:
Ltmp72:
	movw	r0, #45403
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end36:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Remove_byte_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Remove_byte_byte:
Leh_func_begin37:
	push	{r7, lr}
Ltmp73:
	mov	r7, sp
Ltmp74:
Ltmp75:
	movw	r0, #45403
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end37:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Contains_byte_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Contains_byte_byte:
Leh_func_begin38:
	push	{r4, r5, r6, r7, lr}
Ltmp76:
	add	r7, sp, #12
Ltmp77:
	push	{r10, r11}
Ltmp78:
	sub	sp, sp, #4
	mov	r2, r0
	strb	r1, [sp, #3]
	ldr	r0, [r2]
	ldrb	r0, [r0, #22]
	cmp	r0, #1
	bhi	LBB38_6
	ldr	r11, [r2, #12]
	mov	r0, #0
	cmp	r11, #1
	blt	LBB38_5
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC38_0+8))
	add	r5, r2, #16
	mov	r6, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC38_0+8))
LPC38_0:
	add	r0, pc, r0
	ldr	r4, [r0, #124]
LBB38_3:
	ldrb	r10, [r5, r6]
	mov	r0, r4
	bl	_p_28_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	add	r0, sp, #3
	strb	r10, [r1, #8]
	bl	_p_36_plt_byte_Equals_object_llvm
	mov	r1, r0
	mov	r0, #1
	tst	r1, #255
	bne	LBB38_5
	add	r6, r6, #1
	mov	r0, #0
	cmp	r6, r11
	blt	LBB38_3
LBB38_5:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB38_6:
	movw	r0, #45267
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #646
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end38:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_CopyTo_byte_byte___int
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_CopyTo_byte_byte___int:
Leh_func_begin39:
	push	{r4, r7, lr}
Ltmp79:
	add	r7, sp, #4
Ltmp80:
Ltmp81:
	sub	sp, sp, #4
	mov	r9, r2
	mov	r2, r1
	cmp	r2, #0
	beq	LBB39_6
	ldr	r1, [r0]
	ldrb	r1, [r1, #22]
	cmp	r1, #2
	bhs	LBB39_8
	ldr	r1, [r0, #8]
	cmp	r1, #0
	addeq	r1, r0, #12
	ldr	r4, [r1]
	ldr	r3, [r2, #8]
	mov	r1, #0
	cmp	r3, #0
	add	r4, r4, r9
	ldrne	r1, [r3, #4]
	ldr	r3, [r2, #8]
	cmp	r3, #0
	addeq	r3, r2, #12
	ldr	r3, [r3]
	add	r1, r3, r1
	cmp	r4, r1
	bgt	LBB39_7
	ldr	r1, [r2]
	ldrb	r1, [r1, #22]
	cmp	r1, #2
	bhs	LBB39_8
	cmp	r9, #0
	blt	LBB39_10
	ldr	r3, [r0, #8]
	mov	r1, #0
	cmp	r3, #0
	ldrne	r1, [r3, #4]
	ldr	r3, [r0, #8]
	cmp	r3, #0
	addeq	r3, r0, #12
	ldr	r3, [r3]
	str	r3, [sp]
	mov	r3, r9
	bl	_p_37_plt_System_Array_Copy_System_Array_int_System_Array_int_int_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
LBB39_6:
	movw	r0, #11691
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #519
	b	LBB39_9
LBB39_7:
	movw	r0, #45463
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #518
	b	LBB39_9
LBB39_8:
	movw	r0, #45267
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #646
LBB39_9:
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
LBB39_10:
	movw	r0, #11703
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r4, r0
	movw	r0, #45658
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r2, r0
	movw	r0, #520
	mov	r1, r4
	movt	r0, #512
	bl	_p_38_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end39:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__get_Item_byte_int
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__get_Item_byte_int:
Leh_func_begin40:
	push	{r7, lr}
Ltmp82:
	mov	r7, sp
Ltmp83:
Ltmp84:
	ldr	r2, [r0, #12]
	cmp	r2, r1
	addhi	r0, r1, r0
	ldrbhi	r0, [r0, #16]
	pophi	{r7, pc}
	movw	r0, #11703
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end40:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_Resize_byte_byte____int
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_Resize_byte_byte____int:
Leh_func_begin41:
	push	{r4, r5, r6, r7, lr}
Ltmp85:
	add	r7, sp, #12
Ltmp86:
	push	{r10, r11}
Ltmp87:
	sub	sp, sp, #4
	mov	r6, r1
	mov	r10, r0
	cmp	r6, #0
	blt	LBB41_11
	ldr	r4, [r10]
	cmp	r4, #0
	beq	LBB41_5
	ldr	r5, [r4, #12]
	cmp	r5, r6
	beq	LBB41_10
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC41_0+8))
	mov	r1, r6
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC41_0+8))
LPC41_0:
	add	r0, pc, r0
	ldr	r0, [r0, #100]
	bl	_p_22_plt__jit_icall_mono_array_new_specific_llvm
	mov	r11, r0
	cmp	r5, r6
	mov	r0, r5
	movge	r0, r6
	cmp	r0, #8
	ble	LBB41_6
	str	r0, [sp]
	mov	r0, r4
	mov	r1, #0
	mov	r2, r11
	mov	r3, #0
	bl	_p_39_plt_System_Array_FastCopy_System_Array_int_System_Array_int_int_llvm
	b	LBB41_9
LBB41_5:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC41_1+8))
	mov	r1, r6
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC41_1+8))
LPC41_1:
	add	r0, pc, r0
	ldr	r0, [r0, #100]
	bl	_p_22_plt__jit_icall_mono_array_new_specific_llvm
	str	r0, [r10]
	b	LBB41_10
LBB41_6:
	cmp	r0, #1
	blt	LBB41_9
	mvn	r0, r5
	mvn	r1, r6
	add	r2, r11, #16
	cmp	r0, r1
	movgt	r1, r0
	mvn	r0, r1
	add	r1, r4, #16
LBB41_8:
	ldrb	r3, [r1], #1
	subs	r0, r0, #1
	strb	r3, [r2], #1
	bne	LBB41_8
LBB41_9:
	str	r11, [r10]
LBB41_10:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB41_11:
	movw	r0, #47356
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_32_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end41:

	.private_extern	_Cirrious_MvvmCross_Plugins_File_Touch__System_Linq_Enumerable_EmptyOf_1_byte__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_File_Touch__System_Linq_Enumerable_EmptyOf_1_byte__cctor:
Leh_func_begin42:
	push	{r4, r7, lr}
Ltmp88:
	add	r7, sp, #4
Ltmp89:
Ltmp90:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC42_0+8))
	mov	r1, #0
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got-(LPC42_0+8))
LPC42_0:
	add	r4, pc, r4
	ldr	r0, [r4, #100]
	bl	_p_22_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r4, #132]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end42:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_type_info_2,4,2
.zerofill __DATA,__bss,_type_info_3,4,2
.zerofill __DATA,__bss,_type_info_4,4,2
.zerofill __DATA,__bss,_type_info_5,4,2
.zerofill __DATA,__bss,_type_info_6,4,2
.zerofill __DATA,__bss,_type_info_7,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got,376,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_FolderExists_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_PathCombine_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_EnsureFolderExists_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_GetFilesIn_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFile_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFolder_string_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadTextFile_string_string_
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_byte___
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Collections_Generic_IEnumerable_1_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Action_1_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_NativePath_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore_FullPath_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin_Load
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_Count
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Clear
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Add_byte_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Remove_byte_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Contains_byte_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_CopyTo_byte_byte___int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__get_Item_byte_int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Array_Resize_byte_byte____int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File_Touch__System_Linq_Enumerable_EmptyOf_1_byte__cctor
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	43
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	14
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	18
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	19
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	20
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	21
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	22
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	23
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	25
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	27
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	29
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	36
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	37
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	40
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	41
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	42
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	43
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	44
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	45
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	46
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	55
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	56
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	57
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	58
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	59
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	60
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	61
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	63
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	64
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	65
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
Lset43 = Leh_func_end42-Leh_func_begin42
	.long	Lset43
Lset44 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset44
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin29:
	.byte	0

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.File.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0:

	.byte 128,64,45,233,13,112,160,225,96,13,45,233,60,208,77,226,13,176,160,225,0,160,160,225,32,16,139,229,36,32,139,229
	.byte 40,48,203,229,0,0,160,227,0,0,139,229,0,0,160,227,4,0,203,229,0,0,160,227,8,0,139,229,32,16,155,229
	.byte 10,0,160,225,0,32,154,229,15,224,160,225,108,240,146,229,0,96,160,225,36,16,155,229,10,0,160,225,0,32,154,229
	.byte 15,224,160,225,108,240,146,229,0,80,160,225,6,0,160,225
bl _p_1

	.byte 0,16,160,225,255,0,1,226,5,16,203,229,0,0,80,227,2,0,0,26,0,0,160,227,4,0,203,229,79,0,0,234
	.byte 5,0,160,225
bl _p_1

	.byte 255,0,0,226,0,0,80,227,0,0,160,19,1,0,160,3,5,0,203,229,0,0,80,227,12,0,0,26,40,0,219,229
	.byte 0,0,80,227,0,0,160,19,1,0,160,3,5,0,203,229,0,0,80,227,2,0,0,26,5,0,160,225
bl _p_6

	.byte 2,0,0,234,0,0,160,227,4,0,203,229,57,0,0,234,6,0,160,225,5,16,160,225
bl _p_12

	.byte 1,0,160,227,4,0,203,229,51,0,0,234,12,0,155,229,12,0,155,229,0,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 164
	.byte 0,0,159,231,48,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 160
	.byte 0,0,159,231,3,16,160,227
bl _p_22

	.byte 8,0,139,229,0,48,160,225,32,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 8,48,155,229,36,32,155,229,3,0,160,225,1,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229,8,0,155,229
	.byte 52,0,139,229,0,0,155,229
bl _p_42

	.byte 0,32,160,225,52,48,155,229,3,0,160,225,2,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229,48,0,155,229
	.byte 8,16,155,229
bl _p_41

	.byte 0,0,160,227,4,0,203,229
bl _p_40

	.byte 28,0,139,229,0,0,80,227,1,0,0,10,28,0,155,229
bl _p_32

	.byte 255,255,255,234,4,0,219,229,60,208,139,226,96,13,189,232,128,128,189,232

Lme_d:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,32,13,45,233,24,208,77,226,13,176,160,225,0,80,160,225,20,16,139,229,2,160,160,225
	.byte 0,0,160,227,0,0,139,229,0,0,160,227,4,0,203,229,5,0,160,225,20,16,155,229,0,32,149,229,15,224,160,225
	.byte 108,240,146,229,0,80,160,225
bl _p_1

	.byte 255,0,0,226,0,0,80,227,0,0,160,19,1,0,160,3,4,0,203,229,255,0,0,226,0,0,80,227,1,0,0,26
	.byte 5,0,160,225
bl _p_6

	.byte 5,0,160,225
bl _p_13

	.byte 0,0,139,229,0,16,155,229,10,0,160,225,15,224,160,225,12,240,154,229,0,0,0,235,20,0,0,234,16,224,139,229
	.byte 0,0,155,229,0,0,80,227,0,0,160,19,1,0,160,3,4,0,203,229,255,0,0,226,0,0,80,227,9,0,0,26
	.byte 0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,24,208,139,226,32,13,189,232
	.byte 128,128,189,232

Lme_10:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0:

	.byte 128,64,45,233,13,112,160,225,32,13,45,233,24,208,77,226,13,176,160,225,0,80,160,225,20,16,139,229,2,160,160,225
	.byte 0,0,160,227,0,0,139,229,0,0,160,227,5,0,203,229,5,0,160,225,20,16,155,229,0,32,149,229,15,224,160,225
	.byte 108,240,146,229,0,80,160,225
bl _p_1

	.byte 5,0,203,229,255,0,0,226,0,0,80,227,2,0,0,26,0,0,160,227,4,0,203,229,30,0,0,234,5,0,160,225
bl _p_14

	.byte 0,0,139,229,0,16,155,229,10,0,160,225,15,224,160,225,12,240,154,229,4,0,203,229,0,0,0,235,20,0,0,234
	.byte 16,224,139,229,0,0,155,229,0,0,80,227,0,0,160,19,1,0,160,3,5,0,203,229,255,0,0,226,0,0,80,227
	.byte 9,0,0,26,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,4,0,219,229,24,208,139,226
	.byte 32,13,189,232,128,128,189,232

Lme_11:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,20,0,139,229,24,16,139,229,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,5,0,203,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 80
	.byte 0,0,159,231
bl _p_8

	.byte 32,0,139,229,24,16,155,229
bl _p_20

	.byte 32,0,155,229,0,0,139,229,0,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,52,240,145,229,0,16,160,225
	.byte 20,0,155,229,8,16,128,229,0,0,0,235,20,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,0,0,160,19
	.byte 1,0,160,3,5,0,203,229,255,0,0,226,0,0,80,227,9,0,0,26,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,1,0,160,227,4,0,203,229
	.byte 255,0,0,226,40,208,139,226,0,9,189,232,128,128,189,232

Lme_18:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,32,13,45,233,40,208,77,226,13,176,160,225,28,0,139,229,1,160,160,225,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,5,0,203,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 84
	.byte 0,0,159,231
bl _p_8

	.byte 32,0,139,229,10,16,160,225
bl _p_21

	.byte 32,0,155,229,0,0,139,229,10,0,160,225,0,16,154,229,15,224,160,225,120,240,145,229,24,16,139,229,20,0,139,229
	.byte 24,16,155,229,0,0,80,227,2,0,0,170,1,0,113,227,3,0,0,10,1,0,0,234,0,0,81,227,0,0,0,10
	.byte 57,0,0,235,0,16,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 88
	.byte 0,0,159,231
bl _p_22

	.byte 0,80,160,225,0,192,155,229,12,48,149,229,12,0,160,225,5,16,160,225,0,32,160,227,0,192,156,229,15,224,160,225
	.byte 76,240,156,229,12,16,149,229,1,0,80,225,0,0,160,19,1,0,160,3,5,0,203,229,255,0,0,226,0,0,80,227
	.byte 3,0,0,26,0,0,160,227,4,0,203,229,6,0,0,235,26,0,0,234,28,0,155,229,8,80,128,229,1,0,160,227
	.byte 4,0,203,229,0,0,0,235,20,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227,0,0,160,19,1,0,160,3
	.byte 5,0,203,229,255,0,0,226,0,0,80,227,9,0,0,26,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,4,0,219,229,40,208,139,226
	.byte 32,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_43

	.byte 128,2,0,2

Lme_1a:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,20,0,139,229,24,16,139,229,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,4,0,203,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 92
	.byte 0,0,159,231
bl _p_8

	.byte 32,0,139,229,24,16,155,229
bl _p_23

	.byte 32,0,155,229,0,0,139,229,0,32,155,229,20,0,155,229,8,16,144,229,2,0,160,225,0,32,146,229,15,224,160,225
	.byte 92,240,146,229,0,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,116,240,145,229,0,0,0,235,20,0,0,234
	.byte 16,224,139,229,0,0,155,229,0,0,80,227,0,0,160,19,1,0,160,3,4,0,203,229,255,0,0,226,0,0,80,227
	.byte 9,0,0,26,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,40,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_1c:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0
_Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,20,0,139,229,24,16,139,229,0,0,160,227
	.byte 0,0,139,229,0,0,160,227,4,0,203,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 96
	.byte 0,0,159,231
bl _p_8

	.byte 32,0,139,229,24,16,155,229
bl _p_24

	.byte 32,0,155,229,0,0,139,229,0,0,155,229,32,0,139,229,20,0,155,229,8,0,144,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 100
	.byte 8,128,159,231
bl _p_25

	.byte 0,16,160,225,32,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,72,240,146,229,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,15,224,160,225,84,240,145,229,0,0,0,235,20,0,0,234,16,224,139,229,0,0,155,229,0,0,80,227
	.byte 0,0,160,19,1,0,160,3,4,0,203,229,255,0,0,226,0,0,80,227,9,0,0,26,0,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,40,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_1e:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_47

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_45

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_46

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_45
bl _p_44

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_26:
.text
ut_40:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current

.text
ut_41:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array

.text
ut_42:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current

.text
ut_43:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose

.text
ut_44:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext

.text
ut_45:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset

.text
	.align 2
	.no_dead_strip _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0
_System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,40,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,139,229
	.byte 0,0,160,227,8,0,139,229,0,0,90,227,194,0,0,10,10,64,160,225,28,160,139,229,0,0,90,227,22,0,0,10
	.byte 28,0,155,229,0,96,144,229,180,1,214,225,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 148
	.byte 1,16,159,231,1,0,80,225,13,0,0,58,16,0,150,229,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 148
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,32,0,139,229,1,0,0,234,0,0,160,227,32,0,139,229,32,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,12,0,139,229,0,0,0,234,12,64,139,229,12,96,155,229,6,0,160,225
	.byte 0,0,80,227,45,0,0,10,6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 140
	.byte 8,128,159,231,4,224,143,226,76,240,17,229,0,0,0,0,0,0,80,227,5,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 120
	.byte 0,0,159,231,0,0,144,229,131,0,0,234,6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 140
	.byte 8,128,159,231,4,224,143,226,76,240,17,229,0,0,0,0,0,16,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 88
	.byte 0,0,159,231
bl _p_22

	.byte 0,0,139,229,0,16,160,225,6,0,160,225,0,32,160,227,0,48,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 144
	.byte 8,128,159,231,4,224,143,226,32,240,19,229,0,0,0,0,0,0,155,229,102,0,0,234,0,80,160,227,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 120
	.byte 0,0,159,231,0,0,144,229,0,0,139,229,10,0,160,225,0,16,154,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 124
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,8,0,139,229,41,0,0,234,8,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 136
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,4,0,203,229,0,0,155,229,12,0,144,229,0,0,85,225
	.byte 16,0,0,26,0,0,85,227,7,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 88
	.byte 0,0,159,231,4,16,160,227
bl _p_22

	.byte 0,0,139,229,6,0,0,234,133,16,160,225,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 128
	.byte 8,128,159,231,11,0,160,225
bl _p_34

	.byte 0,0,155,229,5,16,160,225,1,80,133,226,12,32,144,229,1,0,82,225,60,0,0,155,1,0,128,224,16,0,128,226
	.byte 4,16,219,229,0,16,192,229,8,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 132
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,200,255,255,26,0,0,0,235
	.byte 15,0,0,234,24,224,139,229,8,0,155,229,0,0,80,227,9,0,0,10,8,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,24,192,155,229,12,240,160,225,0,0,155,229,12,0,144,229
	.byte 0,0,85,225,6,0,0,10,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 128
	.byte 8,128,159,231,11,0,160,225,5,16,160,225
bl _p_34

	.byte 0,0,155,229,40,208,139,226,112,13,189,232,128,128,189,232,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 152
	.byte 0,0,159,231,157,23,0,227
bl _p_35

	.byte 0,16,160,225,7,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_32

	.byte 14,16,160,225,0,0,159,229
bl _p_43

	.byte 88,2,0,2

Lme_2f:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_FolderExists_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_PathCombine_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_EnsureFolderExists_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_GetFilesIn_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFile_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFolder_string_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadTextFile_string_string_
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_byte___
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Collections_Generic_IEnumerable_1_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Action_1_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_NativePath_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore_FullPath_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin_Load
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_Count
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Clear
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Add_byte_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Remove_byte_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Contains_byte_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_CopyTo_byte_byte___int
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__get_Item_byte_int
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_Resize_byte_byte____int
.no_dead_strip _Cirrious_MvvmCross_Plugins_File_Touch__System_Linq_Enumerable_EmptyOf_1_byte__cctor

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_FolderExists_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_PathCombine_string_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_EnsureFolderExists_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_GetFilesIn_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFile_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_DeleteFolder_string_bool
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadTextFile_string_string_
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_byte___
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Collections_Generic_IEnumerable_1_byte
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFile_string_System_Action_1_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_NativePath_string
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__ctor
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore_FullPath_string
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_MvxTouchFileStore__ctor
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin_Load
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_Touch_Plugin__ctor
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__ctor
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__ctor
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0
	bl _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__ctor
	bl _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_File_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_get_Current
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_Dispose
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_MoveNext
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	bl _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_Count
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Clear
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Add_byte_byte
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Remove_byte_byte
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_Contains_byte_byte
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__ICollection_CopyTo_byte_byte___int
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_InternalArray__get_Item_byte_int
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Array_Resize_byte_byte____int
	bl _Cirrious_MvvmCross_Plugins_File_Touch__System_Linq_Enumerable_EmptyOf_1_byte__cctor
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:

	.long 40

	bl ut_40

	.long 41

	bl ut_41

	.long 42

	bl ut_42

	.long 43

	bl ut_43

	.long 44

	bl ut_44

	.long 45

	bl ut_45
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 66,10,7,2
	.short 0, 10, 24, 34, 52, 66, 77
	.byte 1,2,2,2,2,2,2,2,7,7,31,7,7,2,4,255,255,255,255,205,53,3,3,2,65,2,4,2,2,4,2,5
	.byte 2,4,94,255,255,255,255,162,0,0,0,0,99,3,3,255,255,255,255,151,107,4,2,3,2,2,2,4,255,255,255,255
	.byte 130,0,0,0,0,0,0,128,144,2,2,2,2,128,154,4,255,255,255,255,98,128,160,3,12
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 37,0,0,0,0,0,0,0
	.long 0,0,392,55,43,0,0,0
	.long 0,0,0,247,40,0,0,0
	.long 0,0,0,0,218,38,0,484
	.long 63,0,0,0,0,0,0,0
	.long 266,41,0,0,0,0,0,0
	.long 0,0,0,0,304,43,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,342,45,0,0,0,0
	.long 396,56,0,181,36,38,193,37
	.long 40,0,0,0,285,42,37,381
	.long 47,39,404,58,0,323,44,0
	.long 361,46,0,400,57,0,424,59
	.long 41,444,60,0,464,61,42,504
	.long 64,0,524,65,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 34,32,0,33,0,34,0,35
	.long 0,36,181,37,193,38,218,39
	.long 0,40,247,41,266,42,285,43
	.long 304,44,323,45,342,46,361,47
	.long 381,48,0,49,0,50,0,51
	.long 0,52,0,53,0,54,0,55
	.long 392,56,396,57,400,58,404,59
	.long 424,60,444,61,464,62,0,63
	.long 484,64,504,65,524
.section __TEXT, __const
	.align 3
class_name_table:

	.short 19, 0, 0, 0, 0, 5, 0, 0
	.short 0, 0, 0, 3, 20, 1, 19, 0
	.short 0, 7, 0, 0, 0, 0, 0, 0
	.short 0, 2, 21, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 4
	.short 0, 6, 0, 8, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 46,10,5,2
	.short 0, 11, 22, 33, 44
	.byte 130,31,2,1,1,1,4,6,5,5,6,130,66,5,5,4,6,5,5,6,4,5,130,116,3,4,12,5,5,7,5,5
	.byte 12,130,175,4,5,21,14,20,21,14,29,20,131,87,4,2,21,7,3
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 66,10,7,2
	.short 0, 11, 27, 38, 58, 73, 84
	.byte 134,101,3,3,3,3,3,3,3,3,3,134,131,3,3,3,16,255,255,255,249,100,134,159,14,13,3,134,192,3,3,3
	.byte 3,11,3,18,3,13,134,255,255,255,255,249,1,0,0,0,0,135,12,3,3,255,255,255,248,238,135,49,3,3,3,3
	.byte 3,3,3,255,255,255,248,186,0,0,0,0,0,0,135,85,3,3,3,3,135,100,3,255,255,255,248,153,135,106,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 29,12,13,0,72,14,8,135,2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11,27,12
	.byte 13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11,23,12,13,0,72,14
	.byte 8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11,27,12,13,0,72,14,8,135,2,68,14,24,133,6
	.byte 136,5,138,4,139,3,142,1,68,14,64,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1
	.byte 68,14,48,68,13,11,31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1
	.byte 68,14,72,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 135,115,7,84,87,27,23,23,23

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_plt:
_p_1_plt_System_IO_File_Exists_string_llvm:
	.no_dead_strip plt_System_IO_File_Exists_string
plt_System_IO_File_Exists_string:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 184,897
_p_2_plt_System_IO_Directory_Exists_string_llvm:
	.no_dead_strip plt_System_IO_Directory_Exists_string
plt_System_IO_Directory_Exists_string:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 188,902
_p_3_plt_System_IO_Path_Combine_string_string_llvm:
	.no_dead_strip plt_System_IO_Path_Combine_string_string
plt_System_IO_Path_Combine_string_string:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 192,907
_p_4_plt_System_IO_Directory_CreateDirectory_string_llvm:
	.no_dead_strip plt_System_IO_Directory_CreateDirectory_string
plt_System_IO_Directory_CreateDirectory_string:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 196,912
_p_5_plt_System_IO_Directory_GetFiles_string_llvm:
	.no_dead_strip plt_System_IO_Directory_GetFiles_string
plt_System_IO_Directory_GetFiles_string:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 200,917
_p_6_plt_System_IO_File_Delete_string_llvm:
	.no_dead_strip plt_System_IO_File_Delete_string
plt_System_IO_File_Delete_string:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 204,922
_p_7_plt_System_IO_Directory_Delete_string_bool_llvm:
	.no_dead_strip plt_System_IO_Directory_Delete_string_bool
plt_System_IO_Directory_Delete_string_bool:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 208,927
_p_8_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 212,932
_p_9_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 216,955
_p_10_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool
plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 220,1000
_p_11_plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream
plt_Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 224,1005
_p_12_plt_System_IO_File_Move_string_string_llvm:
	.no_dead_strip plt_System_IO_File_Move_string_string
plt_System_IO_File_Move_string_string:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 228,1010
_p_13_plt_System_IO_File_OpenWrite_string_llvm:
	.no_dead_strip plt_System_IO_File_OpenWrite_string
plt_System_IO_File_OpenWrite_string:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 232,1015
_p_14_plt_System_IO_File_OpenRead_string_llvm:
	.no_dead_strip plt_System_IO_File_OpenRead_string
plt_System_IO_File_OpenRead_string:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 236,1020
_p_15_plt_string_StartsWith_string_llvm:
	.no_dead_strip plt_string_StartsWith_string
plt_string_StartsWith_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 240,1025
_p_16_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm:
	.no_dead_strip plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder
plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 244,1030
_p_17_plt_string_Substring_int_llvm:
	.no_dead_strip plt_string_Substring_int
plt_string_Substring_int:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 248,1035
_p_18_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 252,1040
_p_19_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_Cirrious_MvvmCross_Plugins_File_IMvxFileStore
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_Cirrious_MvvmCross_Plugins_File_IMvxFileStore:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 256,1066
_p_20_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamReader__ctor_System_IO_Stream
plt_System_IO_StreamReader__ctor_System_IO_Stream:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 260,1078
_p_21_plt_System_IO_BinaryReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_BinaryReader__ctor_System_IO_Stream
plt_System_IO_BinaryReader__ctor_System_IO_Stream:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 264,1083
_p_22_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 268,1088
_p_23_plt_System_IO_StreamWriter__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamWriter__ctor_System_IO_Stream
plt_System_IO_StreamWriter__ctor_System_IO_Stream:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 272,1114
_p_24_plt_System_IO_BinaryWriter__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_BinaryWriter__ctor_System_IO_Stream
plt_System_IO_BinaryWriter__ctor_System_IO_Stream:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 276,1119
_p_25_plt_System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte
plt_System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 280,1124
_p_26_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 284,1136
_p_27_plt_System_Array_InternalEnumerator_1_byte_get_Current_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_byte_get_Current
plt_System_Array_InternalEnumerator_1_byte_get_Current:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 288,1174
_p_28_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 292,1194
_p_29_plt_System_Array_InternalArray__get_Item_byte_int_llvm:
	.no_dead_strip plt_System_Array_InternalArray__get_Item_byte_int
plt_System_Array_InternalArray__get_Item_byte_int:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 296,1224
_p_30_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 300,1245
_p_31_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 304,1274
_p_32_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 308,1307
_p_33_plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array
plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 312,1335
_p_34_plt_System_Array_Resize_byte_byte____int_llvm:
	.no_dead_strip plt_System_Array_Resize_byte_byte____int
plt_System_Array_Resize_byte_byte____int:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 316,1355
_p_35_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 320,1376
_p_36_plt_byte_Equals_object_llvm:
	.no_dead_strip plt_byte_Equals_object
plt_byte_Equals_object:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 324,1396
_p_37_plt_System_Array_Copy_System_Array_int_System_Array_int_int_llvm:
	.no_dead_strip plt_System_Array_Copy_System_Array_int_System_Array_int_int
plt_System_Array_Copy_System_Array_int_System_Array_int_int:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 328,1401
_p_38_plt__jit_icall_mono_create_corlib_exception_2_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_2
plt__jit_icall_mono_create_corlib_exception_2:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 332,1406
_p_39_plt_System_Array_FastCopy_System_Array_int_System_Array_int_int_llvm:
	.no_dead_strip plt_System_Array_FastCopy_System_Array_int_System_Array_int_int
plt_System_Array_FastCopy_System_Array_int_System_Array_int_int:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 336,1439
_p_40_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 340,1444
_p_41_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 344,1483
_p_42_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 348,1488
_p_43_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 352,1493
_p_44_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 356,1528
_p_45_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 360,1583
_p_46_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 364,1591
_p_47_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got - . + 368,1610
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 4
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Plugins.File.Touch"
	.asciz "E76F7A16-783F-46B4-B6AE-998E5ED93453"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "E76F7A16-783F-46B4-B6AE-998E5ED93453"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.File.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_Touch__Cirrious_MvvmCross_Plugins_File_MvxFileStore_Exists_string
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 46,376,48,66,11,387000831,0,2204
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_File_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_File_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,4,8,7,6,5,0,5,9,8,11,10,5,0,0,0
	.byte 5,12,16,15,14,13,0,5,17,16,19,18,13,0,0,0,2,44,43,0,0,0,1,45,0,1,45,0,0,0,2,20
	.byte 20,0,0,0,2,22,21,0,0,0,0,0,2,23,45,0,0,0,3,24,25,45,0,0,0,2,26,45,0,0,0,3
	.byte 27,28,45,0,1,29,0,1,29,0,0,0,2,31,30,0,0,0,1,32,0,0,0,0,0,0,0,2,30,30,0,16
	.byte 40,40,38,33,38,25,39,33,34,37,25,35,36,45,35,41,0,0,0,0,0,0,0,0,0,0,0,2,31,42,0,0
	.byte 0,1,42,0,2,25,25,4,1,35,1,2,130,27,1,7,128,167,2,33,25,255,252,0,0,0,1,1,3,219,0,0
	.byte 1,255,252,0,0,0,1,1,3,219,0,0,2,5,30,0,1,255,255,255,255,255,193,0,13,43,255,253,0,0,0,2
	.byte 130,10,1,1,198,0,13,43,0,1,7,128,205,4,2,130,11,1,1,2,130,27,1,255,253,0,0,0,7,128,237,1
	.byte 198,0,13,120,1,2,130,27,1,0,255,253,0,0,0,7,128,237,1,198,0,13,121,1,2,130,27,1,0,255,253,0
	.byte 0,0,7,128,237,1,198,0,13,122,1,2,130,27,1,0,255,253,0,0,0,7,128,237,1,198,0,13,123,1,2,130
	.byte 27,1,0,255,253,0,0,0,7,128,237,1,198,0,13,124,1,2,130,27,1,0,255,253,0,0,0,7,128,237,1,198
	.byte 0,13,125,1,2,130,27,1,0,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,2,130,27,1,255,254,0
	.byte 0,0,2,255,43,0,0,1,193,0,13,41,193,0,13,42,193,0,13,44,255,253,0,0,0,2,130,10,1,1,198,0
	.byte 13,45,0,1,2,130,27,1,255,253,0,0,0,2,130,10,1,1,198,0,13,46,0,1,2,130,27,1,255,253,0,0
	.byte 0,2,130,10,1,1,198,0,13,47,0,1,2,130,27,1,255,253,0,0,0,2,130,10,1,1,198,0,13,48,0,1
	.byte 2,130,27,1,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130,27,1,255,253,0,0,0,2,130,10
	.byte 1,1,198,0,13,112,0,1,2,130,27,1,255,253,0,0,0,7,128,167,0,198,0,0,234,1,2,130,27,1,0,12
	.byte 2,39,42,47,14,2,5,2,14,3,219,0,0,1,6,194,0,0,25,50,194,0,0,25,30,3,219,0,0,1,14,2
	.byte 6,2,6,194,0,0,27,50,194,0,0,27,14,2,7,2,14,3,219,0,0,2,6,194,0,0,29,50,194,0,0,29
	.byte 30,3,219,0,0,2,14,2,8,2,6,194,0,0,31,50,194,0,0,31,17,2,79,14,2,3,2,34,255,254,0,0
	.byte 0,2,255,43,0,0,2,14,2,128,228,1,14,2,128,198,1,14,6,1,2,130,27,1,14,2,128,230,1,14,2,128
	.byte 199,1,34,255,254,0,0,0,2,255,43,0,0,1,33,14,7,128,237,14,2,130,27,1,34,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,54,0,1,2,130,27,1,16,7,128,167,109,4,2,111,1,1,2,130,27,1,6,255,253,0,0
	.byte 0,7,130,210,1,198,0,3,125,1,2,130,27,1,0,34,255,253,0,0,0,2,130,10,1,1,198,0,13,112,0,1
	.byte 2,130,27,1,6,193,0,5,8,4,2,112,1,1,2,130,27,1,6,255,253,0,0,0,7,131,9,1,198,0,3,126
	.byte 1,2,130,27,1,0,4,2,108,1,1,2,130,27,1,6,255,253,0,0,0,7,131,38,1,198,0,3,109,1,2,130
	.byte 27,1,0,6,255,253,0,0,0,7,131,38,1,198,0,3,114,1,2,130,27,1,0,23,7,131,38,12,0,34,255,253
	.byte 0,0,0,2,130,10,1,1,198,0,13,56,0,1,2,130,27,1,14,6,1,2,130,123,1,17,2,1,6,193,0,17
	.byte 58,3,193,0,6,165,3,193,0,6,144,3,193,0,7,38,3,193,0,6,139,3,193,0,6,149,3,193,0,6,164,3
	.byte 193,0,6,143,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,7,42,108,108,118
	.byte 109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97
	.byte 109,112,111,108,105,110,101,0,3,194,0,0,18,3,194,0,0,17,3,193,0,6,166,3,193,0,6,169,3,193,0,6
	.byte 168,3,193,0,19,107,3,193,0,16,189,3,193,0,19,53,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101
	.byte 119,95,112,116,114,102,114,101,101,0,3,255,254,0,0,0,2,255,43,0,0,2,3,193,0,7,97,3,193,0,6,110
	.byte 7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,193,0,7,123,3
	.byte 193,0,6,124,3,255,254,0,0,0,2,255,43,0,0,1,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110
	.byte 116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,255,253,0,0,0,7,128,237,1
	.byte 198,0,13,122,1,2,130,27,1,0,7,27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102
	.byte 114,101,101,95,98,111,120,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130,27,1,7,26,109
	.byte 111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,95,109,115,99,111,114,108,105,98,0,7,30,109,111,110,111
	.byte 95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,49,0,7,25,109,111,110
	.byte 111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,255,253,0,0,0,7,128,237
	.byte 1,198,0,13,121,1,2,130,27,1,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,112,0,1,2,130,27,1
	.byte 7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,3,193,0,13,231,3,193,0,13,85,7,30
	.byte 109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,50,0,3
	.byte 193,0,13,70,7,36,109,111,110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108,101
	.byte 95,101,120,99,101,112,116,105,111,110,0,3,195,0,1,125,3,195,0,0,80,7,32,109,111,110,111,95,97,114,99,104
	.byte 95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,0,7,24,109,111,110,111,95,111
	.byte 98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,255,253,0,0,0,2,130,10,1,1,198,0,13
	.byte 43,0,1,7,128,205,4,2,130,11,1,1,7,128,205,35,134,19,150,5,7,134,38,3,255,253,0,0,0,7,134,38
	.byte 1,198,0,13,121,1,7,128,205,0,35,134,19,192,0,92,41,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0
	.byte 1,7,128,205,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,6,0,1,0,12,4,2,130,64,1,60,129,4,129,4,0,16,0,0,6
	.byte 30,1,2,0,128,236,128,128,128,148,128,152,0,6,30,1,2,0,128,228,116,128,140,128,144,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,6,58,1,2,0,128,208,84,120,124,0,16,0,0,38,82,1,1,2,0
	.byte 129,104,84,129,16,129,20,0,4,128,248,0,16,0,0,6,58,1,2,0,128,224,84,128,136,128,140,0,16,0,0,6
	.byte 58,1,2,0,129,0,84,128,168,128,172,0,16,0,0,16,0,0,3,110,0,1,11,4,19,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,43,0,1,7,128,205,1,0,1,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,6,128,134,1,2,0,131,0,129,216,130,188,130,192,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,20,128,152,8,0,0
	.byte 1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,194,0,0,8,194,0,0,9,194,0,0,10,194,0,0
	.byte 11,194,0,0,12,194,0,0,13,194,0,0,14,194,0,0,1,194,0,0,2,194,0,0,3,194,0,0,15,194,0,0
	.byte 4,194,0,0,5,194,0,0,6,194,0,0,7,0,20,128,192,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,194,0,0,8,194,0,0,9,194,0,0,10,194,0,0,11,194,0,0,12,194,0,0,13,194,0,0
	.byte 14,194,0,0,1,194,0,0,2,194,0,0,3,194,0,0,15,194,0,0,4,194,0,0,5,194,0,0,6,194,0,0
	.byte 7,194,0,0,20,5,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,194,0,0,22
	.byte 4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,98,111,101,104
	.byte 109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_File_MvxFileStore"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_File_MvxFileStore"

LDIFF_SYM7=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_3:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM10=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM11=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM11
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM12=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM13=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_2:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM14=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM14
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM15=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM16=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM16
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM17=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM18=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_5:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM19=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM20=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM21=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_6:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM24=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM25=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM26=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_4:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM27=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM28=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM29=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM30=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM31=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM32=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM33=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM34=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM35=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM36=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM37=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM38=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM39=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM40=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM41=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore:TryMove"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0
	.long Lme_d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM44=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 1,90,3
	.asciz "from"

LDIFF_SYM45=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,123,32,3
	.asciz "to"

LDIFF_SYM46=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,123,36,3
	.asciz "deleteExistingTo"

LDIFF_SYM47=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,123,40,11
	.asciz "V_0"

LDIFF_SYM48=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM49=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM50=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,123,0,11
	.asciz "V_3"

LDIFF_SYM51=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,123,4,11
	.asciz "V_4"

LDIFF_SYM52=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,123,5,11
	.asciz "V_5"

LDIFF_SYM53=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM54=Lfde0_end - Lfde0_start
	.long LDIFF_SYM54
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0

LDIFF_SYM55=Lme_d - _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryMove_string_string_bool_0
	.long LDIFF_SYM55
	.byte 12,13,0,72,14,8,135,2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_12:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM56=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM56
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM57=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM58=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM59=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_11:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM60=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM61=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM62=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM63=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_10:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM64=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM65=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM66=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM66
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM67=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_14:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM68=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM69=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM69
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM70=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM71=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM72=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM72
LTDIE_13:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM73=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM74=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM75=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM76=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM77=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM78=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_9:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM79=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM80=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM81=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM82=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM83=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM84=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM85=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM86=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM87=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM88=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM88
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM89=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM90=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM91=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM91
LTDIE_8:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM92=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM93=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM94=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM95=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM96=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM96
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM97=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_7:

	.byte 5
	.asciz "System_Action`1"

	.byte 52,16
LDIFF_SYM98=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,0,0,7
	.asciz "System_Action`1"

LDIFF_SYM99=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM100=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM101=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_17:

	.byte 5
	.asciz "System_Func`4"

	.byte 52,16
LDIFF_SYM102=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2,35,0,0,7
	.asciz "System_Func`4"

LDIFF_SYM103=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM103
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM104=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM104
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM105=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM105
LTDIE_18:

	.byte 5
	.asciz "System_Action`3"

	.byte 52,16
LDIFF_SYM106=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,0,0,7
	.asciz "System_Action`3"

LDIFF_SYM107=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM108=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM109=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_22:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM110=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM111=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM112=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM113=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM114=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_26:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM115=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM116=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM117=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM118=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_25:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM119=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM120=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM121=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM122=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM123=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM124=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM125=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM126=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_24:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM127=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM128=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM129=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM130=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM130
LTDIE_23:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM131=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM132=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM133=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM134=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_21:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM135=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM136=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM136
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM137=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM137
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM138=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM139=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM140=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_20:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM141=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM141
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM142=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM143=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM144=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_19:

	.byte 5
	.asciz "System_Threading_AutoResetEvent"

	.byte 20,16
LDIFF_SYM145=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,0,0,7
	.asciz "System_Threading_AutoResetEvent"

LDIFF_SYM146=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_16:

	.byte 5
	.asciz "System_IO_Stream"

	.byte 20,16
LDIFF_SYM149=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,6
	.asciz "async_read"

LDIFF_SYM150=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,8,6
	.asciz "async_write"

LDIFF_SYM151=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,12,6
	.asciz "async_event"

LDIFF_SYM152=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM152
	.byte 2,35,16,0,7
	.asciz "System_IO_Stream"

LDIFF_SYM153=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM153
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM154=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM155=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_27:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeFileHandle"

	.byte 24,16
LDIFF_SYM156=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeFileHandle"

LDIFF_SYM157=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM157
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM158=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM159=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_28:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM160=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM161=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM161
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM162=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM163=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM164=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM164
LTDIE_29:

	.byte 8
	.asciz "System_IO_FileAccess"

	.byte 4
LDIFF_SYM165=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 9
	.asciz "Read"

	.byte 1,9
	.asciz "Write"

	.byte 2,9
	.asciz "ReadWrite"

	.byte 3,0,7
	.asciz "System_IO_FileAccess"

LDIFF_SYM166=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM166
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM167=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM168=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_15:

	.byte 5
	.asciz "System_IO_FileStream"

	.byte 76,16
LDIFF_SYM169=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,0,6
	.asciz "buf"

LDIFF_SYM170=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,20,6
	.asciz "name"

LDIFF_SYM171=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,24,6
	.asciz "safeHandle"

LDIFF_SYM172=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,28,6
	.asciz "append_startpos"

LDIFF_SYM173=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,32,6
	.asciz "handle"

LDIFF_SYM174=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,40,6
	.asciz "access"

LDIFF_SYM175=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM175
	.byte 2,35,44,6
	.asciz "owner"

LDIFF_SYM176=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,48,6
	.asciz "async"

LDIFF_SYM177=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,49,6
	.asciz "canseek"

LDIFF_SYM178=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,50,6
	.asciz "anonymous"

LDIFF_SYM179=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,51,6
	.asciz "buf_dirty"

LDIFF_SYM180=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,52,6
	.asciz "buf_size"

LDIFF_SYM181=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,56,6
	.asciz "buf_length"

LDIFF_SYM182=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,60,6
	.asciz "buf_offset"

LDIFF_SYM183=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,64,6
	.asciz "buf_start"

LDIFF_SYM184=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,68,0,7
	.asciz "System_IO_FileStream"

LDIFF_SYM185=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM185
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM186=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM186
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM187=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore:WriteFileCommon"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0
	.long Lme_10

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM188=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 1,85,3
	.asciz "path"

LDIFF_SYM189=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,123,20,3
	.asciz "streamAction"

LDIFF_SYM190=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM191=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM191
	.byte 1,85,11
	.asciz "V_1"

LDIFF_SYM192=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,123,0,11
	.asciz "V_2"

LDIFF_SYM193=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM194=Lfde1_end - Lfde1_start
	.long LDIFF_SYM194
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0

LDIFF_SYM195=Lme_10 - _Cirrious_MvvmCross_Plugins_File_MvxFileStore_WriteFileCommon_string_System_Action_1_System_IO_Stream_0
	.long LDIFF_SYM195
	.byte 12,13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_30:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM196=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM197=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM198=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM199=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM199
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore:TryReadFileCommon"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	.long Lme_11

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM200=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 1,85,3
	.asciz "path"

LDIFF_SYM201=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,123,20,3
	.asciz "streamAction"

LDIFF_SYM202=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM203=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 1,85,11
	.asciz "V_1"

LDIFF_SYM204=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,123,0,11
	.asciz "V_2"

LDIFF_SYM205=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,123,4,11
	.asciz "V_3"

LDIFF_SYM206=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,123,5,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM207=Lfde2_end - Lfde2_start
	.long LDIFF_SYM207
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0

LDIFF_SYM208=Lme_11 - _Cirrious_MvvmCross_Plugins_File_MvxFileStore_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	.long LDIFF_SYM208
	.byte 12,13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_31:

	.byte 5
	.asciz "_<>c__DisplayClass1"

	.byte 12,16
LDIFF_SYM209=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 2,35,0,6
	.asciz "result"

LDIFF_SYM210=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClass1"

LDIFF_SYM211=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM212=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM212
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM213=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_33:

	.byte 5
	.asciz "System_IO_TextReader"

	.byte 8,16
LDIFF_SYM214=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,0,0,7
	.asciz "System_IO_TextReader"

LDIFF_SYM215=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM215
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM216=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM216
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM217=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_35:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM218=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM218
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM219=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM220=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM220
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM221=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM221
LTDIE_36:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM222=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM223=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM224=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM225=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM225
LTDIE_34:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM226=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM227=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM228=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM229=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM230=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM231=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM232=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM233=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM234=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM235=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM236=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM237=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM238=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM239=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM239
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM240=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM241=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM242=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_38:

	.byte 5
	.asciz "System_Text_DecoderFallbackBuffer"

	.byte 8,16
LDIFF_SYM243=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallbackBuffer"

LDIFF_SYM244=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM244
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM245=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM245
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM246=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_37:

	.byte 5
	.asciz "System_Text_Decoder"

	.byte 16,16
LDIFF_SYM247=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,0,6
	.asciz "fallback"

LDIFF_SYM248=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,8,6
	.asciz "fallback_buffer"

LDIFF_SYM249=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,35,12,0,7
	.asciz "System_Text_Decoder"

LDIFF_SYM250=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM250
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM251=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM251
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM252=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM252
LTDIE_39:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM253=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM253
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM254=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM254
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM255=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM256=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM257=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM258=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM258
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM259=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM259
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM260=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM260
LTDIE_40:

	.byte 17
	.asciz "System_Threading_Tasks_IDecoupledTask"

	.byte 8,7
	.asciz "System_Threading_Tasks_IDecoupledTask"

LDIFF_SYM261=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM261
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM262=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM263=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_32:

	.byte 5
	.asciz "System_IO_StreamReader"

	.byte 56,16
LDIFF_SYM264=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM264
	.byte 2,35,0,6
	.asciz "input_buffer"

LDIFF_SYM265=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,8,6
	.asciz "decoded_buffer"

LDIFF_SYM266=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM266
	.byte 2,35,12,6
	.asciz "encoding"

LDIFF_SYM267=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM267
	.byte 2,35,16,6
	.asciz "decoder"

LDIFF_SYM268=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM268
	.byte 2,35,20,6
	.asciz "line_builder"

LDIFF_SYM269=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,24,6
	.asciz "base_stream"

LDIFF_SYM270=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,28,6
	.asciz "decoded_count"

LDIFF_SYM271=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,36,6
	.asciz "pos"

LDIFF_SYM272=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,40,6
	.asciz "buffer_size"

LDIFF_SYM273=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,44,6
	.asciz "do_checks"

LDIFF_SYM274=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM274
	.byte 2,35,48,6
	.asciz "mayBlock"

LDIFF_SYM275=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM275
	.byte 2,35,52,6
	.asciz "async_task"

LDIFF_SYM276=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,35,32,6
	.asciz "leave_open"

LDIFF_SYM277=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,53,6
	.asciz "foundCR"

LDIFF_SYM278=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,54,0,7
	.asciz "System_IO_StreamReader"

LDIFF_SYM279=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM279
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM280=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM281=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore/<>c__DisplayClass1:<TryReadTextFile>b__0"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0
	.long Lme_18

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM282=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,123,20,3
	.asciz "stream"

LDIFF_SYM283=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,123,24,11
	.asciz "V_0"

LDIFF_SYM284=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM285=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,123,4,11
	.asciz "V_2"

LDIFF_SYM286=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,123,5,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM287=Lfde3_end - Lfde3_start
	.long LDIFF_SYM287
Lfde3_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0

LDIFF_SYM288=Lme_18 - _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass1__TryReadTextFileb__0_System_IO_Stream_0
	.long LDIFF_SYM288
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_41:

	.byte 5
	.asciz "_<>c__DisplayClass4"

	.byte 12,16
LDIFF_SYM289=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM289
	.byte 2,35,0,6
	.asciz "result"

LDIFF_SYM290=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClass4"

LDIFF_SYM291=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM291
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM292=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM292
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM293=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM293
LTDIE_42:

	.byte 5
	.asciz "System_IO_BinaryReader"

	.byte 32,16
LDIFF_SYM294=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,0,6
	.asciz "m_stream"

LDIFF_SYM295=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,8,6
	.asciz "m_encoding"

LDIFF_SYM296=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,12,6
	.asciz "m_buffer"

LDIFF_SYM297=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,35,16,6
	.asciz "decoder"

LDIFF_SYM298=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM298
	.byte 2,35,20,6
	.asciz "charBuffer"

LDIFF_SYM299=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM299
	.byte 2,35,24,6
	.asciz "m_disposed"

LDIFF_SYM300=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,28,6
	.asciz "leave_open"

LDIFF_SYM301=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,29,0,7
	.asciz "System_IO_BinaryReader"

LDIFF_SYM302=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM302
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM303=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM303
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM304=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore/<>c__DisplayClass4:<TryReadBinaryFile>b__3"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0
	.long Lme_1a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM305=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,123,28,3
	.asciz "stream"

LDIFF_SYM306=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM307=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM308=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM308
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM309=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 2,123,4,11
	.asciz "V_3"

LDIFF_SYM310=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 2,123,5,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM311=Lfde4_end - Lfde4_start
	.long LDIFF_SYM311
Lfde4_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0

LDIFF_SYM312=Lme_1a - _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass4__TryReadBinaryFileb__3_System_IO_Stream_0
	.long LDIFF_SYM312
	.byte 12,13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_43:

	.byte 5
	.asciz "_<>c__DisplayClass7"

	.byte 12,16
LDIFF_SYM313=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 2,35,0,6
	.asciz "contents"

LDIFF_SYM314=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClass7"

LDIFF_SYM315=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM315
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM316=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM316
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM317=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM317
LTDIE_46:

	.byte 17
	.asciz "System_IFormatProvider"

	.byte 8,7
	.asciz "System_IFormatProvider"

LDIFF_SYM318=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM318
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM319=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM319
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM320=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM320
LTDIE_45:

	.byte 5
	.asciz "System_IO_TextWriter"

	.byte 16,16
LDIFF_SYM321=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 2,35,0,6
	.asciz "CoreNewLine"

LDIFF_SYM322=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,35,8,6
	.asciz "internalFormatProvider"

LDIFF_SYM323=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,35,12,0,7
	.asciz "System_IO_TextWriter"

LDIFF_SYM324=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM324
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM325=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM325
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM326=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_44:

	.byte 5
	.asciz "System_IO_StreamWriter"

	.byte 48,16
LDIFF_SYM327=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM327
	.byte 2,35,0,6
	.asciz "internalEncoding"

LDIFF_SYM328=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2,35,16,6
	.asciz "internalStream"

LDIFF_SYM329=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,35,20,6
	.asciz "byte_buf"

LDIFF_SYM330=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM330
	.byte 2,35,24,6
	.asciz "decode_buf"

LDIFF_SYM331=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 2,35,28,6
	.asciz "byte_pos"

LDIFF_SYM332=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM332
	.byte 2,35,36,6
	.asciz "decode_pos"

LDIFF_SYM333=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM333
	.byte 2,35,40,6
	.asciz "iflush"

LDIFF_SYM334=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM334
	.byte 2,35,44,6
	.asciz "preamble_done"

LDIFF_SYM335=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM335
	.byte 2,35,45,6
	.asciz "leave_open"

LDIFF_SYM336=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM336
	.byte 2,35,46,6
	.asciz "async_task"

LDIFF_SYM337=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2,35,32,0,7
	.asciz "System_IO_StreamWriter"

LDIFF_SYM338=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM338
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM339=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM339
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM340=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM340
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore/<>c__DisplayClass7:<WriteFile>b__6"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0
	.long Lme_1c

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM341=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM341
	.byte 2,123,20,3
	.asciz "stream"

LDIFF_SYM342=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM342
	.byte 2,123,24,11
	.asciz "V_0"

LDIFF_SYM343=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM343
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM344=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM344
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM345=Lfde5_end - Lfde5_start
	.long LDIFF_SYM345
Lfde5_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0

LDIFF_SYM346=Lme_1c - _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClass7__WriteFileb__6_System_IO_Stream_0
	.long LDIFF_SYM346
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_48:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM347=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM347
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM348=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM348
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM349=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM349
LTDIE_47:

	.byte 5
	.asciz "_<>c__DisplayClassa"

	.byte 12,16
LDIFF_SYM350=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM350
	.byte 2,35,0,6
	.asciz "contents"

LDIFF_SYM351=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM351
	.byte 2,35,8,0,7
	.asciz "_<>c__DisplayClassa"

LDIFF_SYM352=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM352
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM353=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM353
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM354=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM354
LTDIE_49:

	.byte 5
	.asciz "System_IO_BinaryWriter"

	.byte 24,16
LDIFF_SYM355=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM355
	.byte 2,35,0,6
	.asciz "OutStream"

LDIFF_SYM356=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM356
	.byte 2,35,8,6
	.asciz "m_encoding"

LDIFF_SYM357=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM357
	.byte 2,35,12,6
	.asciz "buffer"

LDIFF_SYM358=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM358
	.byte 2,35,16,6
	.asciz "disposed"

LDIFF_SYM359=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM359
	.byte 2,35,20,6
	.asciz "leave_open"

LDIFF_SYM360=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM360
	.byte 2,35,21,0,7
	.asciz "System_IO_BinaryWriter"

LDIFF_SYM361=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM361
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM362=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM362
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM363=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM363
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.File.MvxFileStore/<>c__DisplayClassa:<WriteFile>b__9"
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0
	.long Lme_1e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM364=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM364
	.byte 2,123,20,3
	.asciz "stream"

LDIFF_SYM365=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM365
	.byte 2,123,24,11
	.asciz "V_0"

LDIFF_SYM366=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM367=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM367
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM368=Lfde6_end - Lfde6_start
	.long LDIFF_SYM368
Lfde6_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0

LDIFF_SYM369=Lme_1e - _Cirrious_MvvmCross_Plugins_File_MvxFileStore__c__DisplayClassa__WriteFileb__9_System_IO_Stream_0
	.long LDIFF_SYM369
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_50:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM370=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM370
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM371=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM371
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM372=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM372
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM373=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM373
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_26

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM374=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM374
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM375=Lfde7_end - Lfde7_start
	.long LDIFF_SYM375
Lfde7_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM376=Lme_26 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM376
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_51:

	.byte 17
	.asciz "System_Collections_Generic_ICollection`1"

	.byte 8,7
	.asciz "System_Collections_Generic_ICollection`1"

LDIFF_SYM377=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM377
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM378=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM378
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM379=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM379
LTDIE_52:

	.byte 5
	.asciz "System_Byte"

	.byte 9,16
LDIFF_SYM380=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM380
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM381=LDIE_U1 - Ldebug_info_start
	.long LDIFF_SYM381
	.byte 2,35,8,0,7
	.asciz "System_Byte"

LDIFF_SYM382=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM382
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM383=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM383
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM384=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM384
LTDIE_53:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM385=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM385
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM386=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM386
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM387=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM387
	.byte 2
	.asciz "System.Linq.Enumerable:ToArray<byte>"
	.long _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0
	.long Lme_2f

	.byte 2,118,16,3
	.asciz "source"

LDIFF_SYM388=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM388
	.byte 1,90,11
	.asciz "array"

LDIFF_SYM389=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM389
	.byte 2,123,0,11
	.asciz "collection"

LDIFF_SYM390=LTDIE_51_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM390
	.byte 1,86,11
	.asciz "pos"

LDIFF_SYM391=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM391
	.byte 1,85,11
	.asciz "element"

LDIFF_SYM392=LDIE_U1 - Ldebug_info_start
	.long LDIFF_SYM392
	.byte 2,123,4,11
	.asciz ""

LDIFF_SYM393=LTDIE_53_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM393
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM394=Lfde8_end - Lfde8_start
	.long LDIFF_SYM394
Lfde8_start:

	.long 0
	.align 2
	.long _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0

LDIFF_SYM395=Lme_2f - _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0
	.long LDIFF_SYM395
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/System.Core/System.Linq"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Array.cs"

	.byte 1,0,0
	.asciz "Enumerable.cs"

	.byte 2,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,2,1,3,207,0,2,32,1,2,252,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Linq_Enumerable_ToArray_byte_System_Collections_Generic_IEnumerable_1_byte_0

	.byte 3,202,22,4,3,1,3,202,22,2,48,1,3,1,2,164,1,1,131,3,1,2,44,1,8,118,3,1,2,196,0,1
	.byte 3,1,2,44,1,77,75,8,117,3,1,2,224,0,1,131,3,1,2,36,1,76,8,231,3,3,2,168,1,1,131,8
	.byte 230,2,200,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
