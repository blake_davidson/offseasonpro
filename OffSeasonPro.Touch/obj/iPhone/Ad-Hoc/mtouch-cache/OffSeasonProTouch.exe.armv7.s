	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor:
Leh_func_begin1:
	bx	lr
Leh_func_end1:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_EmailPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_EmailPluginBootstrap__ctor:
Leh_func_begin2:
	bx	lr
Leh_func_end2:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_FilePluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_FilePluginBootstrap__ctor:
Leh_func_begin3:
	bx	lr
Leh_func_end3:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonLocalisationPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonLocalisationPluginBootstrap__ctor:
Leh_func_begin4:
	bx	lr
Leh_func_end4:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonPluginBootstrap__ctor:
Leh_func_begin5:
	bx	lr
Leh_func_end5:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MessengerPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MessengerPluginBootstrap__ctor:
Leh_func_begin6:
	bx	lr
Leh_func_end6:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MusicPlayerPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MusicPlayerPluginBootstrap__ctor:
Leh_func_begin7:
	bx	lr
Leh_func_end7:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_ResourceLoaderPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_ResourceLoaderPluginBootstrap__ctor:
Leh_func_begin8:
	bx	lr
Leh_func_end8:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_TextToSpeechPluginBootstrap__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_TextToSpeechPluginBootstrap__ctor:
Leh_func_begin9:
	bx	lr
Leh_func_end9:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_Background
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_Background:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp0:
	add	r7, sp, #8
Ltmp1:
Ltmp2:
	movw	r4, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC10_0+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC10_0+8))
LPC10_0:
	add	r4, pc, r4
	add	r5, r4, #20
	bl	_p_1_plt_OffSeasonPro_Touch_Constants_get_IsTallScreen_llvm
	tst	r0, #255
	addeq	r5, r4, #16
	ldr	r0, [r5]
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_ConcreteBackground
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_ConcreteBackground:
Leh_func_begin11:
	push	{r4, r5, r7, lr}
Ltmp3:
	add	r7, sp, #8
Ltmp4:
Ltmp5:
	movw	r4, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC11_0+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC11_0+8))
LPC11_0:
	add	r4, pc, r4
	add	r5, r4, #28
	bl	_p_1_plt_OffSeasonPro_Touch_Constants_get_IsTallScreen_llvm
	tst	r0, #255
	addeq	r5, r4, #24
	ldr	r0, [r5]
	pop	{r4, r5, r7, pc}
Leh_func_end11:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen:
Leh_func_begin12:
	push	{r4, r7, lr}
Ltmp6:
	add	r7, sp, #4
Ltmp7:
Ltmp8:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r4, #0
	str	r4, [sp, #12]
	str	r4, [sp, #8]
	str	r4, [sp, #4]
	str	r4, [sp]
	bl	_p_2_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #84]
	mov	r1, sp
	blx	r2
	vldr	s0, LCPI12_0
	vldr	s2, [sp, #12]
	vcmpe.f32	s2, s0
	vmrs	APSR_nzcv, fpscr
	movge	r4, #1
	mov	r0, r4
	sub	sp, r7, #4
	pop	{r4, r7, pc}
	.align	2
	.data_region
LCPI12_0:
	.long	1141768192
	.end_data_region
Leh_func_end12:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Constants__cctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Constants__cctor:
Leh_func_begin13:
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC13_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r1, [r0, #32]
	ldr	r2, [r0, #36]
	str	r1, [r2]
	ldr	r2, [r0, #44]
	ldr	r1, [r0, #40]
	str	r1, [r2]
	ldr	r2, [r0, #52]
	ldr	r1, [r0, #48]
	str	r1, [r2]
	ldr	r2, [r0, #60]
	ldr	r1, [r0, #56]
	str	r1, [r2]
	ldrd	r0, r1, [r0, #64]
	str	r0, [r1]
	bx	lr
Leh_func_end13:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string:
Leh_func_begin14:
	push	{r7, lr}
Ltmp9:
	mov	r7, sp
Ltmp10:
Ltmp11:
	bl	_p_3_plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_llvm
	pop	{r7, pc}
Leh_func_end14:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction:
Leh_func_begin15:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
Ltmp14:
	bl	_p_4_plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction_llvm
	pop	{r7, pc}
Leh_func_end15:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView:
Leh_func_begin16:
	push	{r4, r5, r6, r7, lr}
Ltmp15:
	add	r7, sp, #12
Ltmp16:
Ltmp17:
	sub	sp, sp, #28
	bic	sp, sp, #7
	bl	_p_5_plt_CrossUI_Touch_Dialog_Elements_BadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC16_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC16_0+8))
LPC16_0:
	add	r6, pc, r6
	ldr	r0, [r6, #72]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	movw	r1, #0
	mov	r5, r0
	movw	r0, #52429
	movt	r1, #17279
	movt	r0, #15820
	str	r0, [sp]
	mov	r0, r5
	mov	r2, r1
	mov	r3, r1
	bl	_p_7_plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB16_2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	ldr	r0, [r6, #60]
	movw	r1, #0
	movt	r1, #16880
	ldr	r0, [r0]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #300]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #284]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	mov	r0, #202
	mov	r1, #202
	mov	r2, #202
	bl	_p_10_plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #292]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r1, #0
	str	r1, [sp, #20]
	str	r1, [sp, #16]
	movw	r1, #52429
	movt	r1, #16204
	str	r1, [sp, #16]
	str	r1, [sp, #20]
	str	r1, [sp, #12]
	ldr	r2, [sp, #16]
	str	r2, [sp, #8]
	ldr	r1, [r0]
	ldr	r2, [sp, #12]
	ldr	r3, [r1, #280]
	ldr	r1, [sp, #8]
	blx	r3
LBB16_2:
	mov	r0, r4
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end16:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool:
Leh_func_begin17:
	push	{r7, lr}
Ltmp18:
	mov	r7, sp
Ltmp19:
Ltmp20:
	bl	_p_11_plt_CrossUI_Touch_Dialog_Elements_BooleanElement__ctor_string_bool_llvm
	pop	{r7, pc}
Leh_func_end17:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
Ltmp23:
	sub	sp, sp, #28
	bic	sp, sp, #7
	bl	_p_12_plt_CrossUI_Touch_Dialog_Elements_BooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC18_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC18_0+8))
LPC18_0:
	add	r6, pc, r6
	ldr	r0, [r6, #72]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	movw	r1, #0
	mov	r5, r0
	movw	r0, #52429
	movt	r1, #17279
	movt	r0, #15820
	str	r0, [sp]
	mov	r0, r5
	mov	r2, r1
	mov	r3, r1
	bl	_p_7_plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB18_2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	ldr	r0, [r6, #60]
	movw	r1, #0
	movt	r1, #16800
	ldr	r0, [r0]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #300]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #284]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	mov	r0, #202
	mov	r1, #202
	mov	r2, #202
	bl	_p_10_plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #292]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r1, #0
	str	r1, [sp, #20]
	str	r1, [sp, #16]
	movw	r1, #39322
	movt	r1, #16153
	str	r1, [sp, #16]
	str	r1, [sp, #20]
	str	r1, [sp, #12]
	ldr	r2, [sp, #16]
	str	r2, [sp, #8]
	ldr	r1, [r0]
	ldr	r2, [sp, #12]
	ldr	r3, [r1, #280]
	ldr	r1, [sp, #8]
	blx	r3
LBB18_2:
	mov	r0, r4
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end18:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string_string:
Leh_func_begin19:
	push	{r4, r7, lr}
Ltmp24:
	add	r7, sp, #4
Ltmp25:
Ltmp26:
	mov	r4, r0
	bl	_p_13_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_string_llvm
	mvn	r0, #0
	str	r0, [r4, #92]
	ldr	r0, [r4, #40]
	str	r0, [r4, #88]
	pop	{r4, r7, pc}
Leh_func_end19:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string:
Leh_func_begin20:
	push	{r4, r7, lr}
Ltmp27:
	add	r7, sp, #4
Ltmp28:
Ltmp29:
	mov	r4, r0
	bl	_p_14_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_llvm
	mvn	r0, #0
	str	r0, [r4, #92]
	ldr	r0, [r4, #40]
	str	r0, [r4, #88]
	pop	{r4, r7, pc}
Leh_func_end20:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string:
Leh_func_begin21:
	push	{r4, r7, lr}
Ltmp30:
	add	r7, sp, #4
Ltmp31:
Ltmp32:
	mov	r4, r0
	bl	_p_15_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_llvm
	mvn	r0, #0
	str	r0, [r4, #92]
	ldr	r0, [r4, #40]
	str	r0, [r4, #88]
	pop	{r4, r7, pc}
Leh_func_end21:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_Alignment
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_Alignment:
Leh_func_begin22:
	ldr	r0, [r0, #88]
	bx	lr
Leh_func_end22:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_Alignment_MonoTouch_UIKit_UITextAlignment
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_Alignment_MonoTouch_UIKit_UITextAlignment:
Leh_func_begin23:
	str	r1, [r0, #88]
	bx	lr
Leh_func_end23:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_MaxLength
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_MaxLength:
Leh_func_begin24:
	ldr	r0, [r0, #92]
	bx	lr
Leh_func_end24:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_MaxLength_int
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_MaxLength_int:
Leh_func_begin25:
	str	r1, [r0, #92]
	bx	lr
Leh_func_end25:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_CellKey
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_CellKey:
Leh_func_begin26:
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC26_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC26_0+8))
LPC26_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r0, [r0]
	bx	lr
Leh_func_end26:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_GetCellImpl_MonoTouch_UIKit_UITableView:
Leh_func_begin27:
	push	{r4, r5, r6, r7, lr}
Ltmp33:
	add	r7, sp, #12
Ltmp34:
Ltmp35:
	sub	sp, sp, #28
	bic	sp, sp, #7
	bl	_p_16_plt_CrossUI_Touch_Dialog_Elements_EntryElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC27_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC27_0+8))
LPC27_0:
	add	r6, pc, r6
	ldr	r0, [r6, #72]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	movw	r1, #0
	mov	r5, r0
	movw	r0, #52429
	movt	r1, #17279
	movt	r0, #15820
	str	r0, [sp]
	mov	r0, r5
	mov	r2, r1
	mov	r3, r1
	bl	_p_7_plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB27_2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	ldr	r0, [r6, #60]
	mov	r1, #1107296256
	ldr	r0, [r0]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #300]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #228]
	blx	r1
	mov	r5, r0
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #92]
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #92]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #228]
	blx	r1
	movw	r5, #52429
	mov	r1, #0
	movt	r5, #16204
	str	r1, [sp, #20]
	str	r1, [sp, #16]
	str	r5, [sp, #16]
	str	r5, [sp, #20]
	str	r5, [sp, #12]
	ldr	r1, [sp, #16]
	str	r1, [sp, #8]
	ldr	r1, [r0]
	ldr	r2, [sp, #12]
	ldr	r3, [r1, #88]
	ldr	r1, [sp, #8]
	blx	r3
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #228]
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #108]
	mov	r1, #0
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	mov	r6, r0
	mov	r0, #202
	mov	r1, #202
	mov	r2, #202
	bl	_p_10_plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int_llvm
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #292]
	mov	r0, r6
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #296]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #228]
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #84]
	mov	r1, r5
	blx	r2
LBB27_2:
	mov	r0, r4
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end27:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__cctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__cctor:
Leh_func_begin28:
	push	{r4, r5, r7, lr}
Ltmp36:
	add	r7, sp, #8
Ltmp37:
	push	{r10, r11}
Ltmp38:
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC28_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC28_0+8))
LPC28_0:
	add	r5, pc, r5
	ldrd	r10, r11, [r5, #80]
	mov	r0, r11
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r10
	mov	r4, r0
	bl	_p_17_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	ldr	r0, [r5, #76]
	str	r4, [r0]
	pop	{r10, r11}
	pop	{r4, r5, r7, pc}
Leh_func_end28:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__CreateTextFieldm__0_MonoTouch_UIKit_UITextField_MonoTouch_Foundation_NSRange_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__CreateTextFieldm__0_MonoTouch_UIKit_UITextField_MonoTouch_Foundation_NSRange_string:
Leh_func_begin29:
	push	{r4, r5, r6, r7, lr}
Ltmp39:
	add	r7, sp, #12
Ltmp40:
	push	{r10}
Ltmp41:
	sub	sp, sp, #8
	bic	sp, sp, #7
	mov	r4, r0
	stm	sp, {r2, r3}
	mov	r5, r1
	mov	r0, #1
	ldr	r1, [r4, #92]
	cmn	r1, #1
	beq	LBB29_6
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC29_0+8))
	ldr	r10, [r7, #8]
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC29_0+8))
LPC29_0:
	add	r6, pc, r6
	ldr	r0, [r6, #88]
	ldr	r1, [r0]
	mov	r1, r10
	bl	_p_18_plt_string_Contains_string_llvm
	mov	r1, #1
	tst	r0, #255
	bne	LBB29_3
	ldr	r0, [r6, #92]
	ldr	r1, [r0]
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_19_plt_string_Equals_string_llvm
	uxtb	r1, r0
LBB29_3:
	mov	r0, #0
	cmp	r1, #0
	beq	LBB29_6
	ldr	r0, [r4, #92]
	cmp	r0, #0
	mov	r0, r1
	ble	LBB29_6
	ldr	r0, [r5]
	ldr	r1, [r0, #344]
	mov	r0, r5
	blx	r1
	ldr	r0, [r0, #8]
	ldr	r1, [r10, #8]
	add	r0, r1, r0
	ldr	r1, [sp, #4]
	ldr	r2, [r4, #92]
	sub	r1, r0, r1
	mov	r0, #0
	cmp	r1, r2
	movle	r0, #1
LBB29_6:
	sub	sp, r7, #16
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end29:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement__ctor_string_string_MonoTouch_UIKit_UIImage
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement__ctor_string_string_MonoTouch_UIKit_UIImage:
Leh_func_begin30:
	push	{r4, r5, r7, lr}
Ltmp42:
	add	r7, sp, #8
Ltmp43:
Ltmp44:
	mov	r4, r3
	mov	r5, r0
	bl	_p_20_plt_CrossUI_Touch_Dialog_Elements_RadioElement__ctor_string_string_llvm
	str	r4, [r5, #60]
	pop	{r4, r5, r7, pc}
Leh_func_end30:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement_GetCellImpl_MonoTouch_UIKit_UITableView:
Leh_func_begin31:
	push	{r4, r5, r6, r7, lr}
Ltmp45:
	add	r7, sp, #12
Ltmp46:
	push	{r10, r11}
Ltmp47:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r11, r0
	bl	_p_21_plt_CrossUI_Touch_Dialog_Elements_Element_GetCell_MonoTouch_UIKit_UITableView_llvm
	movw	r4, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC31_0+8))
	mov	r10, r0
	movt	r4, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC31_0+8))
LPC31_0:
	add	r4, pc, r4
	ldr	r6, [r4, #96]
	ldr	r1, [r4, #100]
	mov	r0, r1
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, #3
	mov	r2, r6
	mov	r5, r0
	bl	_p_22_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_string_llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	ldr	r1, [r11, #16]
	ldr	r2, [r0]
	ldr	r2, [r2, #308]
	blx	r2
	ldr	r0, [r4, #72]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	movw	r1, #0
	mov	r6, r0
	movw	r0, #52429
	movt	r1, #17279
	movt	r0, #15820
	str	r0, [sp]
	mov	r0, r6
	mov	r2, r1
	mov	r3, r1
	bl	_p_7_plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single_llvm
	ldr	r0, [r5]
	mov	r1, r6
	ldr	r2, [r0, #256]
	mov	r0, r5
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	cmp	r0, #0
	beq	LBB31_2
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	ldr	r0, [r4, #68]
	movw	r1, #0
	movt	r1, #16800
	ldr	r0, [r0]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #300]
	mov	r0, r6
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #284]
	mov	r0, r6
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	mov	r0, #202
	mov	r1, #202
	mov	r2, #202
	bl	_p_10_plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int_llvm
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #292]
	mov	r0, r6
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	mov	r1, #0
	str	r1, [sp, #20]
	str	r1, [sp, #16]
	movw	r1, #52429
	movt	r1, #16204
	str	r1, [sp, #16]
	str	r1, [sp, #20]
	str	r1, [sp, #12]
	ldr	r2, [sp, #16]
	str	r2, [sp, #8]
	ldr	r1, [r0]
	ldr	r2, [sp, #12]
	ldr	r3, [r1, #280]
	ldr	r1, [sp, #8]
	blx	r3
LBB31_2:
	ldr	r0, [r10]
	ldr	r1, [r0, #276]
	mov	r0, r10
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #272]
	mov	r0, r5
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r0, #300]
	mov	r0, r5
	blx	r1
	ldr	r1, [r11, #60]
	ldr	r2, [r0]
	ldr	r2, [r2, #264]
	blx	r2
	mov	r0, r5
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end31:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace__ctor:
Leh_func_begin32:
	bx	lr
Leh_func_end32:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string:
Leh_func_begin33:
	bx	lr
Leh_func_end33:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string:
Leh_func_begin34:
	bx	lr
Leh_func_end34:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ctor:
Leh_func_begin35:
	push	{r4, r5, r6, r7, lr}
Ltmp48:
	add	r7, sp, #12
Ltmp49:
	push	{r8}
Ltmp50:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC35_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC35_1+8))
LPC35_1:
	add	r6, pc, r6
	ldr	r0, [r6, #104]
	mov	r8, r0
	bl	_p_23_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorSource_llvm
	mov	r4, r0
	cmp	r5, #0
	beq	LBB35_2
	ldr	r0, [r6, #108]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #112]
	str	r0, [r1, #20]
	ldr	r0, [r6, #116]
	str	r0, [r1, #28]
	ldr	r0, [r6, #120]
	str	r0, [r1, #12]
	ldr	r0, [r6, #124]
	ldr	r2, [r4]
	mov	r8, r0
	mov	r0, r4
	sub	r2, r2, #56
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp51:
LBB35_2:
	ldr	r0, LCPI35_0
LPC35_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI35_0:
	.long	Ltmp51-(LPC35_0+8)
	.end_data_region
Leh_func_end35:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer_ShowError_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer_ShowError_string:
Leh_func_begin36:
	push	{r4, r5, r6, r7, lr}
Ltmp52:
	add	r7, sp, #12
Ltmp53:
	push	{r10, r11}
Ltmp54:
	sub	sp, sp, #8
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC36_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC36_0+8))
LPC36_0:
	add	r0, pc, r0
	ldr	r6, [r0, #128]
	ldr	r10, [r0, #132]
	ldr	r0, [r0, #136]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r11, #0
	mov	r1, r6
	mov	r2, r4
	mov	r3, #0
	mov	r5, r0
	strd	r10, r11, [sp]
	bl	_p_26_plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string___llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #264]
	mov	r0, r5
	blx	r1
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end36:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ErrorDisplayerm__1_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ErrorDisplayerm__1_object_OffSeasonPro_Core_Models_ErrorEventArgs:
Leh_func_begin37:
	push	{r7, lr}
Ltmp55:
	mov	r7, sp
Ltmp56:
Ltmp57:
	ldr	r1, [r2]
	ldr	r1, [r2, #8]
	bl	_p_27_plt_OffSeasonPro_Touch_ErrorDisplayer_ShowError_string_llvm
	pop	{r7, pc}
Leh_func_end37:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__ctor:
Leh_func_begin38:
	bx	lr
Leh_func_end38:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIButton:
Leh_func_begin39:
	push	{r4, r5, r6, r7, lr}
Ltmp58:
	add	r7, sp, #12
Ltmp59:
Ltmp60:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC39_1+8))
	mov	r4, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC39_1+8))
LPC39_1:
	add	r6, pc, r6
	ldr	r0, [r6, #140]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #8]
	cmp	r5, #0
	ldr	r4, [r5, #8]
	beq	LBB39_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #148]
	str	r5, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #152]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_28_plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp61:
LBB39_2:
	ldr	r0, LCPI39_0
LPC39_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI39_0:
	.long	Ltmp61-(LPC39_0+8)
	.end_data_region
Leh_func_end39:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIBarButtonItem
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIBarButtonItem:
Leh_func_begin40:
	push	{r4, r5, r6, r7, lr}
Ltmp62:
	add	r7, sp, #12
Ltmp63:
Ltmp64:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC40_1+8))
	mov	r4, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC40_1+8))
LPC40_1:
	add	r6, pc, r6
	ldr	r0, [r6, #160]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #8]
	cmp	r5, #0
	ldr	r4, [r5, #8]
	beq	LBB40_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #164]
	str	r5, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #168]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_29_plt_MonoTouch_UIKit_UIBarButtonItem_add_Clicked_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp65:
LBB40_2:
	ldr	r0, LCPI40_0
LPC40_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI40_0:
	.long	Ltmp65-(LPC40_0+8)
	.end_data_region
Leh_func_end40:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextField
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextField:
Leh_func_begin41:
	push	{r4, r5, r6, r7, lr}
Ltmp66:
	add	r7, sp, #12
Ltmp67:
Ltmp68:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC41_1+8))
	mov	r5, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC41_1+8))
LPC41_1:
	add	r6, pc, r6
	ldr	r0, [r6, #172]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #8]
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #344]
	blx	r1
	ldr	r1, [r6, #92]
	ldr	r1, [r1]
	bl	_p_30_plt_string_Concat_string_string_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #340]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #8]
	cmp	r4, #0
	beq	LBB41_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #176]
	str	r0, [r1, #20]
	ldr	r0, [r6, #180]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_31_plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp69:
LBB41_2:
	ldr	r0, LCPI41_0
LPC41_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI41_0:
	.long	Ltmp69-(LPC41_0+8)
	.end_data_region
Leh_func_end41:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextView:
Leh_func_begin42:
	push	{r4, r5, r6, r7, lr}
Ltmp70:
	add	r7, sp, #12
Ltmp71:
Ltmp72:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC42_1+8))
	mov	r5, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC42_1+8))
LPC42_1:
	add	r6, pc, r6
	ldr	r0, [r6, #184]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #8]
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #304]
	blx	r1
	ldr	r1, [r6, #92]
	ldr	r1, [r1]
	bl	_p_30_plt_string_Concat_string_string_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #300]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #8]
	cmp	r4, #0
	beq	LBB42_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #188]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #192]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_32_plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp73:
LBB42_2:
	ldr	r0, LCPI42_0
LPC42_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI42_0:
	.long	Ltmp73-(LPC42_0+8)
	.end_data_region
Leh_func_end42:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UILabel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UILabel:
Leh_func_begin43:
	push	{r4, r7, lr}
Ltmp74:
	add	r7, sp, #4
Ltmp75:
Ltmp76:
	mov	r4, r1
	ldr	r0, [r4]
	ldr	r1, [r0, #312]
	mov	r0, r4
	blx	r1
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC43_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC43_0+8))
LPC43_0:
	add	r1, pc, r1
	ldr	r1, [r1, #92]
	ldr	r1, [r1]
	bl	_p_30_plt_string_Concat_string_string_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #308]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end43:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIImageView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIImageView:
Leh_func_begin44:
	push	{r4, r5, r6, r7, lr}
Ltmp77:
	add	r7, sp, #12
Ltmp78:
Ltmp79:
	mov	r4, r1
	ldr	r0, [r4]
	ldr	r1, [r0, #268]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC44_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC44_0+8))
LPC44_0:
	add	r0, pc, r0
	ldr	r0, [r0, #196]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_33_plt_MonoTouch_UIKit_UIImage__ctor_MonoTouch_CoreGraphics_CGImage_llvm
	ldr	r0, [r4]
	mov	r1, r6
	ldr	r2, [r0, #264]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r6, r7, pc}
Leh_func_end44:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIDatePicker
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIDatePicker:
Leh_func_begin45:
	push	{r4, r5, r6, r7, lr}
Ltmp80:
	add	r7, sp, #12
Ltmp81:
Ltmp82:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC45_1+8))
	mov	r5, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC45_1+8))
LPC45_1:
	add	r6, pc, r6
	ldr	r0, [r6, #200]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #8]
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #296]
	blx	r1
	ldr	r1, [r0]
	vmov.f64	d0, #1.000000e+00
	ldr	r3, [r1, #76]
	vmov	r1, r2, d0
	blx	r3
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #292]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #8]
	cmp	r4, #0
	beq	LBB45_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #204]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #208]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_34_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp83:
LBB45_2:
	ldr	r0, LCPI45_0
LPC45_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI45_0:
	.long	Ltmp83-(LPC45_0+8)
	.end_data_region
Leh_func_end45:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISlider
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISlider:
Leh_func_begin46:
	push	{r4, r5, r6, r7, lr}
Ltmp84:
	add	r7, sp, #12
Ltmp85:
Ltmp86:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC46_1+8))
	mov	r5, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC46_1+8))
LPC46_1:
	add	r6, pc, r6
	ldr	r0, [r6, #212]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #8]
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #308]
	blx	r1
	vmov	s2, r0
	ldr	r0, [r5]
	vmov.f32	s0, #1.000000e+00
	vadd.f32	s0, s2, s0
	vmov	r1, s0
	ldr	r2, [r0, #304]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #8]
	cmp	r4, #0
	beq	LBB46_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #216]
	str	r0, [r1, #20]
	ldr	r0, [r6, #220]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_34_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp87:
LBB46_2:
	ldr	r0, LCPI46_0
LPC46_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI46_0:
	.long	Ltmp87-(LPC46_0+8)
	.end_data_region
Leh_func_end46:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISwitch
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISwitch:
Leh_func_begin47:
	push	{r4, r5, r6, r7, lr}
Ltmp88:
	add	r7, sp, #12
Ltmp89:
Ltmp90:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC47_1+8))
	mov	r5, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC47_1+8))
LPC47_1:
	add	r6, pc, r6
	ldr	r0, [r6, #224]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #8]
	ldr	r5, [r4, #8]
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #296]
	blx	r1
	tst	r0, #255
	ldr	r0, [r5]
	mov	r1, #0
	moveq	r1, #1
	ldr	r2, [r0, #292]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #8]
	cmp	r4, #0
	beq	LBB47_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #228]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #232]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_34_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp91:
LBB47_2:
	ldr	r0, LCPI47_0
LPC47_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI47_0:
	.long	Ltmp91-(LPC47_0+8)
	.end_data_region
Leh_func_end47:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Collections_Specialized_INotifyCollectionChanged
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Collections_Specialized_INotifyCollectionChanged:
Leh_func_begin48:
	push	{r4, r5, r6, r7, lr}
Ltmp92:
	add	r7, sp, #12
Ltmp93:
	push	{r8}
Ltmp94:
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC48_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC48_0+8))
LPC48_0:
	add	r5, pc, r5
	ldr	r6, [r5, #236]
	ldr	r1, [r6]
	cmp	r1, #0
	bne	LBB48_2
	ldr	r0, [r5, #244]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #248]
	str	r1, [r0, #20]
	ldr	r1, [r5, #252]
	str	r1, [r0, #28]
	ldr	r1, [r5, #256]
	str	r1, [r0, #12]
	str	r0, [r6]
	ldr	r0, [r5, #236]
	ldr	r1, [r0]
LBB48_2:
	ldr	r2, [r4]
	ldr	r0, [r5, #240]
	sub	r2, r2, #68
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end48:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Windows_Input_ICommand
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Windows_Input_ICommand:
Leh_func_begin49:
	push	{r4, r5, r6, r7, lr}
Ltmp95:
	add	r7, sp, #12
Ltmp96:
	push	{r8}
Ltmp97:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC49_1+8))
	mov	r4, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC49_1+8))
LPC49_1:
	add	r6, pc, r6
	ldr	r0, [r6, #260]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #8]
	cmp	r5, #0
	ldr	r4, [r5, #8]
	beq	LBB49_2
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #264]
	str	r0, [r1, #20]
	ldr	r0, [r6, #268]
	str	r0, [r1, #28]
	ldr	r0, [r6, #156]
	str	r0, [r1, #12]
	ldr	r2, [r4]
	ldr	r0, [r6, #272]
	sub	r2, r2, #20
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp98:
LBB49_2:
	ldr	r0, LCPI49_0
LPC49_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI49_0:
	.long	Ltmp98-(LPC49_0+8)
	.end_data_region
Leh_func_end49:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includem__9_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includem__9_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin50:
	push	{r4, r5, r6, r7, lr}
Ltmp99:
	add	r7, sp, #12
Ltmp100:
	push	{r10, r11}
Ltmp101:
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC50_0+8))
	mov	r6, r1
	mov	r1, #5
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC50_0+8))
LPC50_0:
	add	r11, pc, r11
	ldr	r0, [r11, #280]
	ldr	r10, [r11, #276]
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r0, [r6]
	ldr	r4, [r6, #16]
	ldr	r0, [r11, #284]
	bl	_p_36_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #0
	str	r4, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r2, [r6, #12]
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r4, [r6, #24]
	ldr	r0, [r11, #288]
	bl	_p_36_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #2
	str	r4, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r2, [r6, #8]
	ldr	r0, [r5]
	mov	r1, #3
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r4, [r6, #20]
	ldr	r0, [r11, #288]
	bl	_p_36_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #4
	str	r4, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	mov	r1, r5
	bl	_p_37_plt_string_Format_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end50:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Application__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Application__ctor:
Leh_func_begin51:
	bx	lr
Leh_func_end51:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Application_Main_string__
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Application_Main_string__:
Leh_func_begin52:
	push	{r7, lr}
Ltmp102:
	mov	r7, sp
Ltmp103:
Ltmp104:
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC52_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC52_0+8))
LPC52_0:
	add	r1, pc, r1
	ldr	r2, [r1, #292]
	mov	r1, #0
	bl	_p_38_plt_MonoTouch_UIKit_UIApplication_Main_string___string_string_llvm
	pop	{r7, pc}
Leh_func_end52:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin53:
	push	{r7, lr}
Ltmp105:
	mov	r7, sp
Ltmp106:
Ltmp107:
	bl	_p_39_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm
	pop	{r7, pc}
Leh_func_end53:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin54:
	push	{r7, lr}
Ltmp108:
	mov	r7, sp
Ltmp109:
Ltmp110:
	bl	_p_40_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm
	pop	{r7, pc}
Leh_func_end54:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_RequestClose_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_RequestClose_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin55:
	push	{r4, r5, r6, r7, lr}
Ltmp111:
	add	r7, sp, #12
Ltmp112:
	push	{r8, r10, r11}
Ltmp113:
	mov	r11, r1
	ldr	r1, [r0]
	ldr	r1, [r1, #124]
	blx	r1
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #216]
	mov	r0, r4
	blx	r1
	mov	r10, #0
	cmp	r0, #0
	beq	LBB55_3
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC55_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC55_0+8))
	ldr	r2, [r0]
LPC55_0:
	add	r1, pc, r1
	ldr	r1, [r1, #316]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB55_3
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	mov	r10, r0
LBB55_3:
	ldr	r0, [r4]
	ldr	r1, [r0, #204]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	bl	_p_41_plt_MonoTouch_UIKit_UINavigationBar_GetTitleTextAttributes_llvm
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC55_1+8))
	mov	r6, r0
	movw	r1, #0
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC55_1+8))
	movt	r1, #16840
LPC55_1:
	add	r5, pc, r5
	ldr	r0, [r5, #296]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	str	r0, [r6, #8]
	ldr	r0, [r4]
	ldr	r1, [r0, #204]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	mov	r1, r6
	bl	_p_42_plt_MonoTouch_UIKit_UINavigationBar_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_llvm
	cmp	r10, #0
	beq	LBB55_6
	ldr	r0, [r10]
	ldr	r6, [r5, #300]
	sub	r0, r0, #44
	mov	r8, r6
	ldr	r1, [r0]
	mov	r0, r10
	blx	r1
	cmp	r0, r11
	bne	LBB55_7
	ldr	r0, [r5, #280]
	ldr	r5, [r5, #304]
	mov	r1, #2
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r0, [r11]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r6]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r10]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r6]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	mov	r0, r5
	mov	r1, r6
	bl	_p_43_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r2, [r0, #188]
	mov	r0, r4
	blx	r2
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB55_6:
	ldr	r0, [r5, #280]
	ldr	r4, [r5, #312]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r0, [r11]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r5]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r4
	mov	r1, r5
	b	LBB55_8
LBB55_7:
	ldr	r0, [r5, #280]
	ldr	r5, [r5, #308]
	mov	r1, #2
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r0, [r11]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r4]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r10]
	mov	r8, r6
	sub	r0, r0, #44
	ldr	r1, [r0]
	mov	r0, r10
	blx	r1
	ldr	r0, [r0]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	mov	r0, r5
	mov	r1, r4
LBB55_8:
	bl	_p_43_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end55:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter:
Leh_func_begin56:
	push	{r4, r5, r7, lr}
Ltmp114:
	add	r7, sp, #8
Ltmp115:
Ltmp116:
	mov	r4, r2
	mov	r5, r0
	bl	_p_44_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm
	str	r4, [r5, #28]
	pop	{r4, r5, r7, pc}
Leh_func_end56:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Setup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Setup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry:
Leh_func_begin57:
	push	{r4, r5, r7, lr}
Ltmp117:
	add	r7, sp, #8
Ltmp118:
	push	{r8}
Ltmp119:
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC57_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC57_0+8))
LPC57_0:
	add	r5, pc, r5
	ldr	r0, [r5, #344]
	ldr	r1, [r4]
	mov	r8, r0
	mov	r0, r4
	bl	_p_53_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_File_Touch_Plugin_llvm
	ldr	r0, [r5, #348]
	mov	r8, r0
	mov	r0, r4
	bl	_p_54_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end57:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Setup_InitializeLastChance
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Setup_InitializeLastChance:
Leh_func_begin58:
	push	{r4, r5, r7, lr}
Ltmp120:
	add	r7, sp, #8
Ltmp121:
	push	{r8}
Ltmp122:
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC58_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC58_0+8))
LPC58_0:
	add	r5, pc, r5
	ldr	r0, [r5, #352]
	bl	_p_55_plt__jit_icall_mono_object_new_ptrfree_llvm
	bl	_p_56_plt_OffSeasonPro_Touch_ErrorDisplayer__ctor_llvm
	mov	r0, r4
	bl	_p_57_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance_llvm
	ldr	r1, [r5, #356]
	ldr	r0, [r4, #28]
	mov	r8, r1
	bl	_p_58_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IViewModelCloser_OffSeasonPro_Core_Interfaces_IViewModelCloser_llvm
	ldr	r0, [r5, #360]
	ldr	r0, [r0]
	ldr	r1, [r0]
	bl	_p_59_plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end58:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource__ctor:
Leh_func_begin59:
	push	{r4, r5, r7, lr}
Ltmp123:
	add	r7, sp, #8
Ltmp124:
Ltmp125:
	mov	r4, r0
	bl	_p_60_plt_MonoTouch_UIKit_UICollectionViewSource__ctor_llvm
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC59_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC59_0+8))
LPC59_0:
	add	r5, pc, r5
	ldr	r0, [r5, #364]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #368]
	ldr	r1, [r1]
	str	r1, [r0, #8]
	str	r0, [r4, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end59:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_Rows
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_Rows:
Leh_func_begin60:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end60:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_Rows_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_Rows_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement:
Leh_func_begin61:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end61:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_FontSize
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_FontSize:
Leh_func_begin62:
	vldr	s0, [r0, #24]
	vmov	r0, s0
	bx	lr
Leh_func_end62:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_FontSize_single
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_FontSize_single:
Leh_func_begin63:
	str	r1, [r0, #24]
	bx	lr
Leh_func_end63:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_ImageViewSize
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_ImageViewSize:
Leh_func_begin64:
	push	{r7}
Ltmp126:
	mov	r7, sp
Ltmp127:
Ltmp128:
	sub	sp, sp, #12
	bic	sp, sp, #7
	ldr	r2, [r0, #28]
	ldr	r0, [r0, #32]
	str	r0, [sp, #4]
	str	r2, [sp]
	ldr	r0, [sp]
	str	r0, [r1]
	ldr	r0, [sp, #4]
	str	r0, [r1, #4]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end64:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_ImageViewSize_System_Drawing_SizeF
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_ImageViewSize_System_Drawing_SizeF:
Leh_func_begin65:
	push	{r7}
Ltmp129:
	mov	r7, sp
Ltmp130:
Ltmp131:
	sub	sp, sp, #12
	bic	sp, sp, #7
	stm	sp, {r1, r2}
	ldr	r1, [sp]
	str	r1, [r0, #28]
	ldr	r1, [sp, #4]
	str	r1, [r0, #32]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end65:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_NumberOfSections_MonoTouch_UIKit_UICollectionView:
Leh_func_begin66:
	mov	r0, #1
	bx	lr
Leh_func_end66:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int:
Leh_func_begin67:
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end67:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ShouldHighlightItem_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ShouldHighlightItem_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin68:
	mov	r0, #1
	bx	lr
Leh_func_end68:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemHighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemHighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin69:
	push	{r7, lr}
Ltmp132:
	mov	r7, sp
Ltmp133:
Ltmp134:
	ldr	r0, [r1]
	ldr	r3, [r0, #288]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	cmp	r0, #0
	beq	LBB69_3
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #6
	bls	LBB69_4
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC69_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC69_2+8))
	ldr	r1, [r1, #8]
LPC69_2:
	add	r2, pc, r2
	ldr	r2, [r2, #372]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB69_5
LBB69_3:
	ldr	r1, [r0]
	ldr	r0, [r0, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #176]
	mov	r1, #1056964608
	blx	r2
	pop	{r7, pc}
Ltmp135:
LBB69_4:
	ldr	r0, LCPI69_1
LPC69_1:
	add	r1, pc, r0
	b	LBB69_6
Ltmp136:
LBB69_5:
	ldr	r0, LCPI69_0
LPC69_0:
	add	r1, pc, r0
LBB69_6:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI69_0:
	.long	Ltmp136-(LPC69_0+8)
LCPI69_1:
	.long	Ltmp135-(LPC69_1+8)
	.end_data_region
Leh_func_end69:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemUnhighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemUnhighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin70:
	push	{r4, r5, r7, lr}
Ltmp137:
	add	r7, sp, #8
Ltmp138:
Ltmp139:
	mov	r5, r0
	ldr	r0, [r1]
	mov	r4, r2
	ldr	r2, [r0, #288]
	mov	r0, r1
	mov	r1, r4
	blx	r2
	cmp	r0, #0
	beq	LBB70_3
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #7
	blo	LBB70_5
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC70_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC70_2+8))
	ldr	r1, [r1, #8]
LPC70_2:
	add	r2, pc, r2
	ldr	r2, [r2, #372]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB70_6
LBB70_3:
	ldr	r1, [r0]
	ldr	r0, [r0, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #176]
	mov	r1, #1065353216
	blx	r2
	ldr	r5, [r5, #20]
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	ldr	r1, [r5]
	ldr	r1, [r5, #12]
	cmp	r0, r1
	bhs	LBB70_8
	ldr	r1, [r5, #8]
	add	r0, r1, r0, lsl #2
	ldr	r0, [r0, #16]
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r4, r5, r7, pc}
Ltmp140:
LBB70_5:
	ldr	r0, LCPI70_1
LPC70_1:
	add	r1, pc, r0
	b	LBB70_7
Ltmp141:
LBB70_6:
	ldr	r0, LCPI70_0
LPC70_0:
	add	r1, pc, r0
LBB70_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB70_8:
	movw	r0, #11703
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_62_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_63_plt__jit_icall_mono_arch_throw_exception_llvm
	.align	2
	.data_region
LCPI70_0:
	.long	Ltmp141-(LPC70_0+8)
LCPI70_1:
	.long	Ltmp140-(LPC70_1+8)
	.end_data_region
Leh_func_end70:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction:
Leh_func_begin71:
	mov	r3, r2
	mov	r2, r1
	strd	r2, r3, [r0, #8]
	bx	lr
Leh_func_end71:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Image
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Image:
Leh_func_begin72:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end72:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Image_MonoTouch_UIKit_UIImage
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Image_MonoTouch_UIKit_UIImage:
Leh_func_begin73:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end73:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Tapped
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Tapped:
Leh_func_begin74:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end74:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Tapped_MonoTouch_Foundation_NSAction
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Tapped_MonoTouch_Foundation_NSAction:
Leh_func_begin75:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end75:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_get_ImageView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_get_ImageView:
Leh_func_begin76:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end76:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_set_ImageView_MonoTouch_UIKit_UIImageView
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_set_ImageView_MonoTouch_UIKit_UIImageView:
Leh_func_begin77:
	str	r1, [r0, #52]
	bx	lr
Leh_func_end77:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell__cctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell__cctor:
Leh_func_begin78:
	push	{r4, r5, r6, r7, lr}
Ltmp142:
	add	r7, sp, #12
Ltmp143:
Ltmp144:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC78_0+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC78_0+8))
LPC78_0:
	add	r6, pc, r6
	ldr	r0, [r6, #84]
	ldr	r4, [r6, #376]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_17_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	ldr	r0, [r6, #380]
	str	r5, [r0]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end78:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ctor:
Leh_func_begin79:
	push	{r7, lr}
Ltmp145:
	mov	r7, sp
Ltmp146:
Ltmp147:
	bl	_p_64_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_llvm
	pop	{r7, pc}
Leh_func_end79:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel:
Leh_func_begin80:
	push	{r7, lr}
Ltmp148:
	mov	r7, sp
Ltmp149:
Ltmp150:
	bl	_p_65_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #7
	blo	LBB80_3
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC80_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC80_2+8))
	ldr	r1, [r1, #8]
LPC80_2:
	add	r2, pc, r2
	ldr	r2, [r2, #384]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB80_4
	pop	{r7, pc}
Ltmp151:
LBB80_3:
	ldr	r0, LCPI80_1
LPC80_1:
	add	r1, pc, r0
	b	LBB80_5
Ltmp152:
LBB80_4:
	ldr	r0, LCPI80_0
LPC80_0:
	add	r1, pc, r0
LBB80_5:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI80_0:
	.long	Ltmp152-(LPC80_0+8)
LCPI80_1:
	.long	Ltmp151-(LPC80_1+8)
	.end_data_region
Leh_func_end80:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_set_ViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_set_ViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel:
Leh_func_begin81:
	push	{r7, lr}
Ltmp153:
	mov	r7, sp
Ltmp154:
Ltmp155:
	bl	_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r7, pc}
Leh_func_end81:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet:
Leh_func_begin82:
	push	{r4, r5, r6, r7, lr}
Ltmp156:
	add	r7, sp, #12
Ltmp157:
	push	{r10, r11}
Ltmp158:
	sub	sp, sp, #4
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC82_1+8))
	mov	r10, r0
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC82_1+8))
LPC82_1:
	add	r11, pc, r11
	ldr	r0, [r11, #388]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	str	r10, [r6, #12]
	bl	_p_67_plt__class_init_MonoTouch_UIKit_UIImagePickerController_llvm
	ldr	r0, [r11, #392]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r4, r0
	bl	_p_68_plt_MonoTouch_UIKit_UIImagePickerController__ctor_llvm
	mov	r0, r10
	str	r4, [r6, #8]
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	mov	r4, r0
	ldr	r0, [r11, #396]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_70_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_llvm
	ldr	r0, [r6, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #228]
	mov	r1, r5
	blx	r2
	ldr	r0, [r6, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #220]
	mov	r1, #1
	blx	r2
	mov	r0, #1
	bl	_p_71_plt_MonoTouch_UIKit_UIImagePickerController_IsSourceTypeAvailable_MonoTouch_UIKit_UIImagePickerControllerSourceType_llvm
	tst	r0, #255
	beq	LBB82_3
	ldr	r0, [r11, #400]
	str	r10, [sp]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r10, [r11, #92]
	mov	r5, r0
	ldr	r0, [r11, #404]
	str	r6, [r5, #12]
	ldr	r4, [r10]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r6, r0
	bl	_p_72_plt_MonoTouch_UIKit_UIActionSheet__ctor_string_llvm
	str	r6, [r5, #8]
	ldr	r0, [r5, #8]
	ldr	r1, [r11, #408]
	ldr	r2, [r0]
	ldr	r2, [r2, #276]
	blx	r2
	ldr	r0, [r5, #8]
	ldr	r1, [r11, #412]
	ldr	r2, [r0]
	ldr	r2, [r2, #276]
	blx	r2
	ldr	r0, [r5, #8]
	ldr	r1, [r11, #416]
	ldr	r2, [r0]
	ldr	r2, [r2, #276]
	blx	r2
	ldr	r0, [r5, #8]
	ldr	r1, [r10]
	ldr	r2, [r0]
	ldr	r2, [r2, #276]
	blx	r2
	ldr	r0, [r5, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #280]
	mov	r1, #2
	blx	r2
	ldr	r0, [r5, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #288]
	mvn	r1, #0
	blx	r2
	ldr	r6, [r5, #8]
	cmp	r5, #0
	beq	LBB82_5
	ldr	r0, [r11, #420]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r11, #424]
	str	r0, [r1, #20]
	ldr	r0, [r11, #428]
	str	r0, [r1, #28]
	ldr	r0, [r11, #432]
	str	r0, [r1, #12]
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_73_plt_MonoTouch_UIKit_UIActionSheet_add_Clicked_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_llvm
	ldr	r5, [r5, #8]
	ldr	r0, [sp]
	ldr	r1, [r0]
	ldr	r1, [r1, #180]
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #268]
	mov	r0, r5
	blx	r2
	b	LBB82_4
LBB82_3:
	ldr	r0, [r6, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #224]
	mov	r1, #0
	blx	r2
	ldr	r1, [r6, #8]
	ldr	r0, [r10]
	mov	r2, #1
	mov	r3, #0
	ldr	r6, [r0, #104]
	mov	r0, r10
	blx	r6
LBB82_4:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp159:
LBB82_5:
	ldr	r0, LCPI82_0
LPC82_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI82_0:
	.long	Ltmp159-(LPC82_0+8)
	.end_data_region
Leh_func_end82:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PrefersStatusBarHidden
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PrefersStatusBarHidden:
Leh_func_begin83:
	mov	r0, #1
	bx	lr
Leh_func_end83:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidAppear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidAppear_bool:
Leh_func_begin84:
	push	{r4, r7, lr}
Ltmp160:
	add	r7, sp, #4
Ltmp161:
Ltmp162:
	mov	r4, r0
	bl	_p_74_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #200]
	mov	r1, #0
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end84:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewWillDisappear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewWillDisappear_bool:
Leh_func_begin85:
	push	{r4, r7, lr}
Ltmp163:
	add	r7, sp, #4
Ltmp164:
Ltmp165:
	mov	r4, r0
	bl	_p_75_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #200]
	mov	r1, #1
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end85:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string:
Leh_func_begin86:
	push	{r4, r5, r7, lr}
Ltmp166:
	add	r7, sp, #8
Ltmp167:
	push	{r8}
Ltmp168:
	mov	r5, r1
	mov	r4, r0
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_76_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings_llvm
	ldr	r1, [r0]
	str	r5, [r0, #32]
	mov	r0, r4
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_76_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings_llvm
	ldr	r1, [r0]
	mov	r1, #0
	strb	r1, [r0, #41]
	mov	r0, r4
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_77_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC86_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC86_0+8))
LPC86_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end86:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__D_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__D_object_System_EventArgs:
Leh_func_begin87:
	push	{r4, r5, r6, r7, lr}
Ltmp169:
	add	r7, sp, #12
Ltmp170:
	push	{r10, r11}
Ltmp171:
	sub	sp, sp, #16
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC87_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC87_0+8))
LPC87_0:
	add	r5, pc, r5
	ldr	r1, [r5, #440]
	str	r1, [sp, #12]
	ldr	r1, [r5, #444]
	str	r1, [sp, #8]
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	mov	r4, r0
	ldr	r0, [r5, #448]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r6, r0
	bl	_p_78_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_llvm
	ldr	r0, [r5, #452]
	ldr	r10, [r5, #416]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r11, r0
	ldr	r2, [r5, #132]
	mov	r1, #0
	ldr	r0, [r11]
	ldr	r3, [r0, #128]
	mov	r0, r11
	blx	r3
	ldr	r0, [r5, #136]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	ldr	r1, [sp, #12]
	ldr	r2, [sp, #8]
	mov	r3, r6
	mov	r4, r0
	strd	r10, r11, [sp]
	bl	_p_26_plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string___llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #264]
	mov	r0, r4
	blx	r1
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end87:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__E_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__E_object_System_EventArgs:
Leh_func_begin88:
	push	{r7, lr}
Ltmp172:
	mov	r7, sp
Ltmp173:
	push	{r8}
Ltmp174:
	bl	_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_77_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC88_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC88_0+8))
LPC88_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end88:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__F_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__F_object_System_EventArgs:
Leh_func_begin89:
	push	{r7, lr}
Ltmp175:
	mov	r7, sp
Ltmp176:
Ltmp177:
	bl	_p_79_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet_llvm
	pop	{r7, pc}
Leh_func_end89:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel:
Leh_func_begin90:
	push	{r4, r5, r7, lr}
Ltmp178:
	add	r7, sp, #8
Ltmp179:
Ltmp180:
	mov	r4, r1
	mov	r5, r0
	bl	_p_80_plt_MonoTouch_UIKit_UIAlertViewDelegate__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end90:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int:
Leh_func_begin91:
	push	{r7, lr}
Ltmp181:
	mov	r7, sp
Ltmp182:
	push	{r8}
Ltmp183:
	cmp	r2, #1
	bne	LBB91_2
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	bl	_p_81_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC91_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC91_0+8))
LPC91_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB91_2:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end91:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel:
Leh_func_begin92:
	push	{r4, r5, r7, lr}
Ltmp184:
	add	r7, sp, #8
Ltmp185:
Ltmp186:
	mov	r4, r1
	mov	r5, r0
	bl	_p_82_plt_MonoTouch_UIKit_UIImagePickerControllerDelegate__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end92:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_FinishedPickingMedia_MonoTouch_UIKit_UIImagePickerController_MonoTouch_Foundation_NSDictionary
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_FinishedPickingMedia_MonoTouch_UIKit_UIImagePickerController_MonoTouch_Foundation_NSDictionary:
Leh_func_begin93:
	push	{r4, r5, r6, r7, lr}
Ltmp187:
	add	r7, sp, #12
Ltmp188:
	push	{r8, r10, r11}
Ltmp189:
	sub	sp, sp, #4
	mov	r10, r1
	mov	r1, #0
	mov	r4, r2
	mov	r5, r0
	str	r1, [sp]
	bl	_p_67_plt__class_init_MonoTouch_UIKit_UIImagePickerController_llvm
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC93_0+8))
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC93_0+8))
LPC93_0:
	add	r11, pc, r11
	ldr	r0, [r11, #456]
	ldr	r1, [r0]
	ldr	r0, [r4]
	ldr	r2, [r0, #232]
	mov	r0, r4
	blx	r2
	mov	r6, r0
	cmp	r6, #0
	beq	LBB93_2
	ldr	r1, [r6]
	ldr	r0, [r11, #464]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #8]
	cmp	r1, r0
	movne	r6, #0
LBB93_2:
	cmp	r6, #0
	beq	LBB93_6
	mov	r0, #5
	bl	_p_83_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm
	ldr	r1, [r11, #460]
	bl	_p_84_plt_System_IO_Path_Combine_string_string_llvm
	mov	r4, r0
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_85_plt_MonoTouch_UIKit_UIImage_AsJPEG_llvm
	mov	r1, #0
	mov	r3, sp
	mov	r2, #0
	str	r1, [sp]
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_86_plt_MonoTouch_Foundation_NSData_Save_string_bool_MonoTouch_Foundation_NSError__llvm
	tst	r0, #255
	beq	LBB93_5
	ldr	r0, [r5, #20]
	ldr	r1, [r0]
	bl	_p_76_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings_llvm
	ldr	r1, [r0]
	str	r4, [r0, #32]
	ldr	r0, [r5, #20]
	ldr	r1, [r0]
	bl	_p_76_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings_llvm
	ldr	r1, [r0]
	mov	r1, #1
	strb	r1, [r0, #41]
	ldr	r0, [r5, #20]
	ldr	r1, [r0]
	bl	_p_77_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	ldr	r1, [r11, #436]
	sub	r2, r2, #36
	mov	r8, r1
	mov	r1, #0
	ldr	r2, [r2]
	blx	r2
LBB93_5:
	ldr	r0, [r10]
	mov	r1, #1
	mov	r2, #0
	ldr	r3, [r0, #100]
	mov	r0, r10
	blx	r3
LBB93_6:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end93:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_Canceled_MonoTouch_UIKit_UIImagePickerController
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_Canceled_MonoTouch_UIKit_UIImagePickerController:
Leh_func_begin94:
	push	{r7, lr}
Ltmp190:
	mov	r7, sp
Ltmp191:
Ltmp192:
	ldr	r0, [r1]
	mov	r2, #0
	ldr	r3, [r0, #100]
	mov	r0, r1
	mov	r1, #1
	blx	r3
	pop	{r7, pc}
Leh_func_end94:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor:
Leh_func_begin95:
	push	{r7, lr}
Ltmp193:
	mov	r7, sp
Ltmp194:
Ltmp195:
	mov	r1, #1
	bl	_p_87_plt_MonoTouch_MediaPlayer_MPMediaPickerController__ctor_MonoTouch_MediaPlayer_MPMediaType_llvm
	pop	{r7, pc}
Leh_func_end95:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PrefersStatusBarHidden
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PrefersStatusBarHidden:
Leh_func_begin96:
	mov	r0, #1
	bx	lr
Leh_func_end96:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PreferredStatusBarStyle
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PreferredStatusBarStyle:
Leh_func_begin97:
	mov	r0, #0
	bx	lr
Leh_func_end97:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_ViewDidLoad
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_ViewDidLoad:
Leh_func_begin98:
	push	{r7, lr}
Ltmp196:
	mov	r7, sp
Ltmp197:
Ltmp198:
	bl	_p_88_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	pop	{r7, pc}
Leh_func_end98:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor:
Leh_func_begin99:
	push	{r7, lr}
Ltmp199:
	mov	r7, sp
Ltmp200:
Ltmp201:
	mov	r1, #1
	mov	r2, #0
	mov	r3, #1
	bl	_p_89_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	pop	{r7, pc}
Leh_func_end99:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_CrossUI_Touch_Dialog_Elements_RootElement_bool:
Leh_func_begin100:
	push	{r7, lr}
Ltmp202:
	mov	r7, sp
Ltmp203:
Ltmp204:
	mov	r3, r2
	mov	r2, r1
	mov	r1, #1
	bl	_p_89_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	pop	{r7, pc}
Leh_func_end100:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool:
Leh_func_begin101:
	push	{r7, lr}
Ltmp205:
	mov	r7, sp
Ltmp206:
Ltmp207:
	bl	_p_89_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm
	pop	{r7, pc}
Leh_func_end101:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_get_ViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_get_ViewModel:
Leh_func_begin102:
	push	{r7, lr}
Ltmp208:
	mov	r7, sp
Ltmp209:
Ltmp210:
	bl	_p_90_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #7
	blo	LBB102_3
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC102_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC102_2+8))
	ldr	r1, [r1, #8]
LPC102_2:
	add	r2, pc, r2
	ldr	r2, [r2, #468]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB102_4
	pop	{r7, pc}
Ltmp211:
LBB102_3:
	ldr	r0, LCPI102_1
LPC102_1:
	add	r1, pc, r0
	b	LBB102_5
Ltmp212:
LBB102_4:
	ldr	r0, LCPI102_0
LPC102_0:
	add	r1, pc, r0
LBB102_5:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI102_0:
	.long	Ltmp212-(LPC102_0+8)
LCPI102_1:
	.long	Ltmp211-(LPC102_1+8)
	.end_data_region
Leh_func_end102:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_set_ViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_set_ViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel:
Leh_func_begin103:
	push	{r7, lr}
Ltmp213:
	mov	r7, sp
Ltmp214:
Ltmp215:
	bl	_p_91_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r7, pc}
Leh_func_end103:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidLoad
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidLoad:
Leh_func_begin104:
	push	{r4, r5, r6, r7, lr}
Ltmp216:
	add	r7, sp, #12
Ltmp217:
	push	{r8, r10, r11}
Ltmp218:
	sub	sp, sp, #40
	bic	sp, sp, #7
	movw	r10, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC104_2+8))
	mov	r11, r0
	movt	r10, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC104_2+8))
LPC104_2:
	add	r10, pc, r10
	ldr	r0, [r10, #472]
	ldr	r1, [r10, #476]
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #16]
	ldr	r0, [r10, #480]
	ldr	r1, [r10, #484]
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #20]
	mov	r0, r11
	bl	_p_93_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad_llvm
	ldr	r0, [r10, #492]
	ldr	r5, [r10, #488]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r4, r0
	bl	_p_94_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm
	ldr	r0, [r11]
	mov	r1, r4
	ldr	r2, [r0, #60]
	mov	r0, r11
	blx	r2
	tst	r0, #255
	beq	LBB104_2
	ldr	r0, [r11]
	mov	r1, #0
	ldr	r2, [r0, #148]
	mov	r0, r11
	blx	r2
LBB104_2:
	ldr	r0, [r10, #500]
	ldr	r4, [r10, #496]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r11
	mov	r2, r4
	mov	r3, #1
	mov	r5, r0
	bl	_p_95_plt_OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool_llvm
	str	r5, [r11, #144]
	ldr	r0, [r10, #504]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_96_plt_MonoTouch_UIKit_UIColor_get_LightGray_llvm
	str	r0, [r4, #12]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	str	r0, [r4, #16]
	mov	r0, #0
	str	r0, [sp, #28]
	str	r0, [sp, #24]
	movw	r0, #52429
	movt	r0, #16204
	str	r0, [sp, #24]
	str	r0, [sp, #28]
	str	r0, [sp, #36]
	ldr	r1, [sp, #24]
	str	r1, [sp, #32]
	ldr	r1, [r10, #508]
	ldr	r0, [sp, #32]
	str	r1, [sp, #4]
	str	r0, [r4, #20]
	ldr	r0, [sp, #36]
	str	r0, [r4, #24]
	ldr	r0, [r10, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r11, [r5, #16]
	ldr	r0, [r10, #512]
	str	r0, [r5, #20]
	ldr	r0, [r10, #516]
	str	r0, [r5, #28]
	ldr	r0, [r10, #156]
	str	r0, [r5, #12]
	str	r0, [sp, #8]
	ldr	r0, [r10, #520]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	ldr	r1, [sp, #4]
	mov	r2, #0
	mov	r3, r5
	str	r0, [sp, #12]
	mov	r6, r0
	bl	_p_97_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_string_MonoTouch_UIKit_UIBarButtonItemStyle_System_EventHandler_llvm
	mov	r0, r6
	mov	r1, r4
	mov	r2, #0
	bl	_p_98_plt_MonoTouch_UIKit_UIBarItem_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_MonoTouch_UIKit_UIControlState_llvm
	ldr	r1, [r10, #416]
	ldr	r0, [r10, #144]
	str	r1, [sp, #4]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r10, #524]
	str	r11, [r5, #16]
	str	r0, [r5, #20]
	ldr	r0, [r10, #528]
	str	r0, [r5, #28]
	ldr	r0, [sp, #8]
	str	r0, [r5, #12]
	ldr	r0, [r10, #520]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	ldr	r1, [sp, #4]
	mov	r2, #0
	mov	r3, r5
	mov	r6, r0
	bl	_p_97_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_string_MonoTouch_UIKit_UIBarButtonItemStyle_System_EventHandler_llvm
	mov	r0, r6
	mov	r1, r4
	mov	r2, #0
	bl	_p_98_plt_MonoTouch_UIKit_UIBarItem_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_MonoTouch_UIKit_UIControlState_llvm
	ldr	r0, [r10, #532]
	mov	r1, #3
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r2, [sp, #12]
	mov	r1, #0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r10, #520]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, #5
	mov	r5, r0
	bl	_p_99_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem_llvm
	ldr	r0, [r5]
	movw	r1, #0
	movt	r1, #16968
	ldr	r2, [r0, #108]
	mov	r0, r5
	blx	r2
	ldr	r0, [r4]
	mov	r1, #1
	mov	r2, r5
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	mov	r1, #2
	mov	r2, r6
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r11]
	mov	r1, r4
	mov	r2, #0
	ldr	r3, [r0, #108]
	mov	r0, r11
	blx	r3
	ldr	r0, [r11]
	ldr	r1, [r0, #160]
	mov	r0, r11
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #196]
	blx	r1
	mov	r4, r0
	ldr	r0, [r10, #536]
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #0
	mov	r3, #0
	ldr	r5, [r0, #264]
	mov	r0, r4
	blx	r5
	ldr	r0, [r11]
	ldr	r1, [r0, #180]
	mov	r0, r11
	blx	r1
	mov	r4, r0
	bl	_p_101_plt_OffSeasonPro_Touch_Constants_get_Background_llvm
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
	bl	_p_102_plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	ldr	r0, [r10, #540]
	mov	r8, r0
	mov	r0, r11
	bl	_p_103_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_OffSeasonPro_Touch_Views_SettingsView_llvm
	str	r0, [sp, #12]
	ldr	r1, [r11, #144]
	ldr	r3, [r10, #544]
	ldr	r2, [r0]
	mov	r8, r3
	bl	_p_104_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_Bind_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Touch_BindableProgress_llvm
	mov	r11, r0
	ldr	r0, [r10, #548]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB104_4
	ldr	r2, [r1]
	ldr	r0, [r10, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB104_7
LBB104_4:
	ldr	r5, [sp, #16]
	mov	r0, r5
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r10, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r4, r0
	ldr	r0, [r10, #556]
	mov	r1, #1
	str	r0, [sp, #8]
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	mov	r1, #0
	mov	r2, r5
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r10, #560]
	mov	r1, r6
	mov	r6, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_108_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r11]
	mov	r0, r11
	bl	_p_109_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_llvm
	mov	r10, r0
	ldr	r0, [r6, #564]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB104_6
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB104_8
LBB104_6:
	ldr	r11, [sp, #20]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r6, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r4, r0
	ldr	r0, [sp, #8]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r6
	mov	r6, r0
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r5, #568]
	mov	r1, r6
	mov	r8, r0
	mov	r0, r4
	bl	_p_110_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_111_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_llvm
	ldr	r0, [sp, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #52]
	blx	r1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp219:
LBB104_7:
	ldr	r0, LCPI104_0
LPC104_0:
	add	r1, pc, r0
	b	LBB104_9
Ltmp220:
LBB104_8:
	ldr	r0, LCPI104_1
LPC104_1:
	add	r1, pc, r0
LBB104_9:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI104_0:
	.long	Ltmp219-(LPC104_0+8)
LCPI104_1:
	.long	Ltmp220-(LPC104_1+8)
	.end_data_region
Leh_func_end104:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_PrefersStatusBarHidden
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_PrefersStatusBarHidden:
Leh_func_begin105:
	mov	r0, #1
	bx	lr
Leh_func_end105:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidAppear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidAppear_bool:
Leh_func_begin106:
	push	{r4, r7, lr}
Ltmp221:
	add	r7, sp, #4
Ltmp222:
Ltmp223:
	mov	r4, r0
	bl	_p_112_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #200]
	mov	r1, #0
	blx	r2
	mov	r0, r4
	bl	_p_113_plt_OffSeasonPro_Touch_Views_SettingsView_ResetDisplay_llvm
	pop	{r4, r7, pc}
Leh_func_end106:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewWillDisappear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewWillDisappear_bool:
Leh_func_begin107:
	push	{r4, r7, lr}
Ltmp224:
	add	r7, sp, #4
Ltmp225:
Ltmp226:
	mov	r4, r0
	bl	_p_114_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #200]
	mov	r1, #1
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end107:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ResetDisplay
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ResetDisplay:
Leh_func_begin108:
	push	{r4, r5, r6, r7, lr}
Ltmp227:
	add	r7, sp, #12
Ltmp228:
	push	{r8, r10, r11}
Ltmp229:
	sub	sp, sp, #16
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC108_2+8))
	mov	r10, r0
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC108_2+8))
LPC108_2:
	add	r11, pc, r11
	ldr	r0, [r11, #576]
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
	mov	r4, r0
	ldr	r0, [r11, #580]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_115_plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage_llvm
	ldr	r0, [r11, #584]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	str	r0, [sp, #12]
	bl	_p_116_plt_CrossUI_Touch_Dialog_Elements_Section__ctor_MonoTouch_UIKit_UIView_llvm
	ldr	r0, [r11, #588]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r11, #592]
	str	r0, [sp, #4]
	mov	r6, r0
	ldr	r4, [r11, #596]
	ldr	r1, [r1]
	str	r1, [r0, #8]
	ldr	r0, [r11, #600]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r0, #2
	ldr	r2, [r11, #604]
	mov	r1, r10
	str	r0, [r5, #88]
	str	r0, [r5, #92]
	ldr	r0, [r11, #608]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r1, r0
	mov	r0, r6
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [r11, #600]
	ldr	r4, [r11, #612]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r0, #2
	mov	r6, r10
	str	r0, [r5, #88]
	str	r0, [r5, #92]
	mov	r1, r6
	ldr	r0, [r11, #608]
	ldr	r2, [r11, #616]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	ldr	r10, [sp, #4]
	mov	r1, r0
	mov	r0, r10
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [r11, #600]
	ldr	r4, [r11, #620]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r0, #2
	mov	r1, r6
	str	r0, [r5, #88]
	str	r0, [r5, #92]
	ldr	r0, [r11, #608]
	ldr	r2, [r11, #624]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r1, r0
	mov	r0, r10
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [r11, #600]
	ldr	r4, [r11, #628]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r1, #2
	mov	r0, #3
	str	r1, [r5, #88]
	str	r0, [r5, #92]
	mov	r1, r6
	ldr	r0, [r11, #608]
	ldr	r2, [r11, #632]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r1, r0
	mov	r0, r10
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [r11, #600]
	ldr	r4, [r11, #636]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r1, #2
	mov	r0, #3
	str	r1, [r5, #88]
	str	r0, [r5, #92]
	mov	r1, r6
	ldr	r0, [r11, #608]
	ldr	r2, [r11, #640]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r1, r0
	mov	r0, r10
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [r11, #600]
	ldr	r4, [r11, #644]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, #4
	bl	_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm
	mov	r0, #2
	mov	r1, #3
	mov	r4, r6
	strd	r0, r1, [r5, #88]
	mov	r1, r4
	ldr	r0, [r11, #608]
	ldr	r2, [r11, #648]
	mov	r8, r0
	mov	r0, r5
	bl	_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r5, r10
	mov	r1, r0
	mov	r0, r5
	bl	_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm
	ldr	r0, [sp, #12]
	mov	r1, r5
	bl	_p_121_plt_CrossUI_Touch_Dialog_Elements_Section_AddAll_System_Collections_Generic_IEnumerable_1_CrossUI_Touch_Dialog_Elements_Element_llvm
	ldr	r5, [r11, #652]
	mov	r0, r4
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_123_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r10, [r0, #40]
	ldr	r0, [r11, #656]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	mov	r2, r10
	mov	r6, r0
	mov	r5, r11
	bl	_p_124_plt_OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool_llvm
	ldr	r0, [r5, #664]
	ldr	r2, [r5, #660]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r6
	bl	_p_125_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyBooleanElement_OffSeasonPro_Touch_Controls_MyBooleanElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	mov	r1, r0
	ldr	r0, [sp, #12]
	bl	_p_126_plt_CrossUI_Touch_Dialog_Elements_Section_Add_CrossUI_Touch_Dialog_Elements_Element_llvm
	mov	r0, r4
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_123_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r6, [r0, #41]
	mov	r0, r4
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_123_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #32]
	cmp	r6, #0
	beq	LBB108_3
	bl	_p_131_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	str	r0, [sp, #4]
	cmp	r4, #0
	beq	LBB108_9
	ldr	r0, [r5, #668]
	str	r0, [sp]
	ldr	r0, [r5, #672]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	ldr	r0, [r5, #700]
	str	r4, [r6, #16]
	str	r0, [r6, #20]
	ldr	r0, [r5, #704]
	str	r0, [r6, #28]
	ldr	r0, [r5, #684]
	str	r0, [r6, #12]
	ldr	r0, [r5, #688]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	ldr	r1, [sp, #4]
	ldr	r2, [sp]
	mov	r3, r6
	str	r4, [sp, #8]
	mov	r11, r5
	mov	r10, r0
	bl	_p_127_plt_OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction_llvm
	ldr	r6, [sp, #12]
	mov	r1, r10
	mov	r0, r6
	b	LBB108_8
LBB108_3:
	cmp	r0, #0
	ldrne	r0, [r0, #8]
	cmpne	r0, #0
	beq	LBB108_5
	mov	r0, r4
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_123_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #32]
	b	LBB108_6
LBB108_5:
	bl	_p_101_plt_OffSeasonPro_Touch_Constants_get_Background_llvm
LBB108_6:
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
	mov	r6, r0
	cmp	r4, #0
	beq	LBB108_10
	mov	r11, r5
	str	r4, [sp, #8]
	ldr	r0, [r11, #672]
	ldr	r10, [r11, #668]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r11, #676]
	str	r4, [r5, #16]
	str	r0, [r5, #20]
	ldr	r0, [r11, #680]
	str	r0, [r5, #28]
	ldr	r0, [r11, #684]
	str	r0, [r5, #12]
	ldr	r0, [r11, #688]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r6
	mov	r2, r10
	mov	r3, r5
	mov	r4, r0
	bl	_p_127_plt_OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction_llvm
	ldr	r6, [sp, #12]
	mov	r1, r4
	mov	r0, r6
LBB108_8:
	bl	_p_126_plt_CrossUI_Touch_Dialog_Elements_Section_Add_CrossUI_Touch_Dialog_Elements_Element_llvm
	ldr	r0, [r11, #696]
	ldr	r4, [r11, #692]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_128_plt_CrossUI_Touch_Dialog_Elements_RootElement__ctor_string_llvm
	mov	r0, r5
	mov	r1, r6
	bl	_p_129_plt_CrossUI_Touch_Dialog_Elements_RootElement_Add_CrossUI_Touch_Dialog_Elements_Section_llvm
	ldr	r4, [sp, #8]
	mov	r1, r5
	ldr	r0, [r4]
	ldr	r2, [r0, #284]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #184]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #360]
	mov	r1, #0
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #184]
	mov	r0, r4
	blx	r1
	mov	r4, r0
	bl	_p_130_plt_MonoTouch_UIKit_UIColor_get_Clear_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp230:
LBB108_9:
	ldr	r0, LCPI108_0
LPC108_0:
	add	r1, pc, r0
	b	LBB108_11
Ltmp231:
LBB108_10:
	ldr	r0, LCPI108_1
LPC108_1:
	add	r1, pc, r0
LBB108_11:
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI108_0:
	.long	Ltmp230-(LPC108_0+8)
LCPI108_1:
	.long	Ltmp231-(LPC108_1+8)
	.end_data_region
Leh_func_end108:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__11_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__11_object_System_EventArgs:
Leh_func_begin109:
	push	{r7, lr}
Ltmp232:
	mov	r7, sp
Ltmp233:
	push	{r8}
Ltmp234:
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_132_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC109_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC109_0+8))
LPC109_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end109:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__12_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__12_object_System_EventArgs:
Leh_func_begin110:
	push	{r7, lr}
Ltmp235:
	mov	r7, sp
Ltmp236:
	push	{r8}
Ltmp237:
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_133_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC110_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC110_0+8))
LPC110_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end110:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__get_InputAccessoryViewm__13_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__get_InputAccessoryViewm__13_object_System_EventArgs:
Leh_func_begin111:
	push	{r7, lr}
Ltmp238:
	mov	r7, sp
Ltmp239:
Ltmp240:
	ldr	r1, [r0]
	ldr	r1, [r1, #180]
	blx	r1
	ldr	r1, [r0]
	mov	r1, #1
	bl	_p_134_plt_MonoTouch_UIKit_UIView_EndEditing_bool_llvm
	pop	{r7, pc}
Leh_func_end111:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__14
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__14:
Leh_func_begin112:
	push	{r7, lr}
Ltmp241:
	mov	r7, sp
Ltmp242:
	push	{r8}
Ltmp243:
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_135_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC112_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC112_0+8))
LPC112_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end112:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__15
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__15:
Leh_func_begin113:
	push	{r7, lr}
Ltmp244:
	mov	r7, sp
Ltmp245:
	push	{r8}
Ltmp246:
	bl	_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_135_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC113_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC113_0+8))
LPC113_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end113:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__ctor:
Leh_func_begin114:
	push	{r7, lr}
Ltmp247:
	mov	r7, sp
Ltmp248:
Ltmp249:
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC114_0+8))
	mov	r2, #0
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC114_0+8))
LPC114_0:
	add	r1, pc, r1
	ldr	r1, [r1, #708]
	bl	_p_136_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	pop	{r7, pc}
Leh_func_end114:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel:
Leh_func_begin115:
	push	{r7, lr}
Ltmp250:
	mov	r7, sp
Ltmp251:
Ltmp252:
	bl	_p_65_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #7
	blo	LBB115_3
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC115_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC115_2+8))
	ldr	r1, [r1, #8]
LPC115_2:
	add	r2, pc, r2
	ldr	r2, [r2, #712]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB115_4
	pop	{r7, pc}
Ltmp253:
LBB115_3:
	ldr	r0, LCPI115_1
LPC115_1:
	add	r1, pc, r0
	b	LBB115_5
Ltmp254:
LBB115_4:
	ldr	r0, LCPI115_0
LPC115_0:
	add	r1, pc, r0
LBB115_5:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI115_0:
	.long	Ltmp254-(LPC115_0+8)
LCPI115_1:
	.long	Ltmp253-(LPC115_1+8)
	.end_data_region
Leh_func_end115:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel:
Leh_func_begin116:
	push	{r7, lr}
Ltmp255:
	mov	r7, sp
Ltmp256:
Ltmp257:
	bl	_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r7, pc}
Leh_func_end116:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_Val
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_Val:
Leh_func_begin117:
	ldr	r0, [r0, #116]
	bx	lr
Leh_func_end117:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_Val_int
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_Val_int:
Leh_func_begin118:
	push	{r7, lr}
Ltmp258:
	mov	r7, sp
Ltmp259:
Ltmp260:
	mov	r2, r0
	str	r1, [r2, #116]
	ldr	r0, [r2, #80]
	ldr	r1, [r2, #116]
	ldr	r2, [r0]
	mov	r2, #1
	bl	_p_137_plt_FlipNumbers_FlipNumbersView_SetValue_int_bool_llvm
	pop	{r7, pc}
Leh_func_end118:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ResizeLabel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ResizeLabel:
Leh_func_begin119:
	ldrb	r0, [r0, #120]
	bx	lr
Leh_func_end119:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ResizeLabel_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ResizeLabel_bool:
Leh_func_begin120:
	push	{r4, r7, lr}
Ltmp261:
	add	r7, sp, #4
Ltmp262:
Ltmp263:
	mov	r4, r0
	strb	r1, [r4, #120]
	ldr	r0, [r4, #96]
	ldr	r1, [r0]
	ldr	r1, [r1, #312]
	blx	r1
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC120_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC120_0+8))
LPC120_0:
	add	r1, pc, r1
	ldr	r1, [r1, #716]
	bl	_p_138_plt_string_op_Equality_string_string_llvm
	mov	r1, r0
	ldr	r0, [r4, #96]
	tst	r1, #255
	ldr	r2, [r0]
	ldr	r2, [r2, #268]
	beq	LBB120_2
	mov	r1, #2
	b	LBB120_3
LBB120_2:
	mov	r1, #1
LBB120_3:
	blx	r2
	ldr	r0, [r4, #96]
	ldr	r1, [r0]
	ldr	r2, [r1, #264]
	mov	r1, #1
	blx	r2
	ldr	r0, [r4, #96]
	ldr	r1, [r0]
	ldr	r1, [r1, #136]
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end120:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_lblCurrentPosition
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_lblCurrentPosition:
Leh_func_begin121:
	ldr	r0, [r0, #96]
	bx	lr
Leh_func_end121:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_lblCurrentPosition_MonoTouch_UIKit_UILabel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_lblCurrentPosition_MonoTouch_UIKit_UILabel:
Leh_func_begin122:
	str	r1, [r0, #96]
	bx	lr
Leh_func_end122:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnPauseResume
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnPauseResume:
Leh_func_begin123:
	ldr	r0, [r0, #100]
	bx	lr
Leh_func_end123:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnPauseResume_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnPauseResume_MonoTouch_UIKit_UIButton:
Leh_func_begin124:
	str	r1, [r0, #100]
	bx	lr
Leh_func_end124:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnReset
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnReset:
Leh_func_begin125:
	ldr	r0, [r0, #104]
	bx	lr
Leh_func_end125:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnReset_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnReset_MonoTouch_UIKit_UIButton:
Leh_func_begin126:
	str	r1, [r0, #104]
	bx	lr
Leh_func_end126:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnBack
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnBack:
Leh_func_begin127:
	ldr	r0, [r0, #108]
	bx	lr
Leh_func_end127:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnBack_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnBack_MonoTouch_UIKit_UIButton:
Leh_func_begin128:
	str	r1, [r0, #108]
	bx	lr
Leh_func_end128:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidDisappear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidDisappear_bool:
Leh_func_begin129:
	push	{r4, r7, lr}
Ltmp264:
	add	r7, sp, #4
Ltmp265:
	push	{r8}
Ltmp266:
	mov	r4, r0
	bl	_p_139_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool_llvm
	mov	r0, r4
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_141_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC129_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC129_0+8))
LPC129_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	bl	_p_142_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #100]
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end129:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidLoad
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidLoad:
Leh_func_begin130:
	push	{r4, r5, r6, r7, lr}
Ltmp267:
	add	r7, sp, #12
Ltmp268:
	push	{r8, r10, r11}
Ltmp269:
	sub	sp, sp, #152
	bic	sp, sp, #7
	mov	r11, r0
	mov	r0, #0
	str	r0, [sp, #116]
	str	r0, [sp, #112]
	movw	r4, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC130_16+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC130_16+8))
LPC130_16:
	add	r4, pc, r4
	ldr	r10, [r4, #724]
	ldr	r0, [r4, #720]
	mov	r1, r10
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	ldr	r5, [r4, #484]
	str	r0, [sp, #4]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #8]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #60]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #12]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #16]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #52]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	ldr	r6, [r4, #732]
	ldr	r1, [r4, #736]
	str	r0, [sp, #56]
	mov	r0, r6
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #48]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	ldr	r1, [r4, #736]
	str	r0, [sp, #20]
	mov	r0, r6
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #24]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	ldr	r1, [r4, #736]
	str	r0, [sp, #28]
	mov	r0, r6
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #32]
	ldr	r0, [r4, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #36]
	ldr	r0, [r4, #720]
	mov	r1, r10
	mov	r6, r4
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #40]
	ldr	r0, [r6, #728]
	mov	r1, r5
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	str	r0, [sp, #44]
	mov	r0, r11
	bl	_p_143_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad_llvm
	mov	r0, r11
	bl	_p_144_plt_OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod_llvm
	ldr	r0, [r6, #740]
	mov	r8, r0
	bl	_p_145_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_llvm
	mov	r5, r0
	cmp	r11, #0
	beq	LBB130_41
	ldr	r0, [r6, #744]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #748]
	mov	r3, #0
	str	r11, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #752]
	str	r0, [r1, #28]
	ldr	r0, [r6, #756]
	str	r0, [r1, #12]
	ldr	r0, [r6, #764]
	ldr	r2, [r5]
	mov	r8, r0
	mov	r0, r5
	sub	r2, r2, #12
	ldr	r4, [r2]
	mov	r2, #0
	blx	r4
	str	r0, [r11, #84]
	ldr	r0, [r6, #492]
	ldr	r5, [r6, #488]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r4, r0
	bl	_p_94_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm
	ldr	r0, [r11]
	mov	r1, r4
	ldr	r2, [r0, #60]
	mov	r0, r11
	blx	r2
	tst	r0, #255
	beq	LBB130_3
	ldr	r0, [r11]
	mov	r1, #0
	ldr	r2, [r0, #148]
	mov	r0, r11
	blx	r2
LBB130_3:
	mov	r0, r11
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_146_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount_llvm
	mov	r4, r0
	ldr	r0, [r6, #768]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_147_plt_FlipNumbers_FlipNumbersView__ctor_int_llvm
	str	r5, [r11, #80]
	ldr	r0, [r11]
	ldr	r1, [r0, #180]
	mov	r0, r11
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #216]
	add	r1, sp, #112
	blx	r2
	vldr	s0, LCPI130_17
	vldr	s2, [sp, #116]
	vadd.f32	s0, s2, s0
	vstr	s0, [sp, #116]
	ldr	r0, [r11, #80]
	ldr	r2, [sp, #116]
	ldr	r1, [sp, #112]
	str	r2, [sp, #76]
	str	r1, [sp, #72]
	ldr	r1, [r0]
	ldr	r2, [sp, #76]
	ldr	r3, [r1, #212]
	ldr	r1, [sp, #72]
	blx	r3
	ldr	r0, [r11]
	ldr	r1, [r0, #180]
	mov	r0, r11
	blx	r1
	ldr	r1, [r11, #80]
	ldr	r2, [r0]
	ldr	r2, [r2, #148]
	blx	r2
	ldr	r0, [r6, #772]
	mov	r8, r0
	mov	r0, r11
	bl	_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_OffSeasonPro_Touch_Views_WorkoutView_llvm
	ldr	r2, [r6, #776]
	str	r0, [sp, #64]
	ldr	r1, [r0]
	mov	r1, r11
	mov	r8, r2
	bl	_p_149_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Touch_Views_WorkoutView_llvm
	str	r0, [sp]
	ldr	r0, [r6, #780]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_6
	b	LBB130_5
	.align	2
	.data_region
LCPI130_17:
	.long	3256877056
LBB130_5:
	.end_data_region
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_42
LBB130_6:
	ldr	r10, [sp, #4]
	mov	r0, r10
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r6, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r5, r0
	ldr	r0, [r6, #556]
	mov	r1, #1
	str	r0, [sp, #68]
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #784]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_150_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp]
	ldr	r2, [r0]
	bl	_p_151_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_llvm
	str	r0, [sp, #4]
	ldr	r0, [r6, #788]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_8
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_43
LBB130_8:
	ldr	r10, [sp, #8]
	mov	r0, r10
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r6, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #4]
	ldr	r2, [r0]
	bl	_p_153_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [r6, #796]
	ldr	r1, [r11, #96]
	mov	r8, r0
	ldr	r0, [sp, #64]
	bl	_p_154_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UILabel_MonoTouch_UIKit_UILabel_llvm
	str	r0, [sp, #8]
	ldr	r0, [r6, #800]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_10
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_44
LBB130_10:
	ldr	r10, [sp, #60]
	mov	r0, r10
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #8]
	ldr	r2, [r0]
	bl	_p_155_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UILabel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [r6, #804]
	ldr	r1, [r11, #100]
	str	r0, [sp, #60]
	mov	r8, r0
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	str	r0, [sp, #8]
	ldr	r0, [r6, #808]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_12
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_45
LBB130_12:
	ldr	r10, [sp, #12]
	mov	r0, r10
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #8]
	ldr	r2, [r0]
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r1, [r11, #100]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	mov	r5, r0
	ldr	r0, [r6, #812]
	ldr	r1, [r5]
	ldr	r1, [r5, #20]
	ldr	r2, [r1]
	str	r0, [r1, #8]
	ldr	r0, [r6, #816]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_14
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_46
LBB130_14:
	mov	r10, r11
	ldr	r11, [sp, #16]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	str	r0, [sp, #12]
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r2, r11
	mov	r1, #0
	mov	r11, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	ldr	r0, [sp, #12]
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r1, [r11, #104]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	str	r0, [sp, #16]
	ldr	r0, [r6, #820]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_16
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_47
LBB130_16:
	mov	r10, r11
	ldr	r11, [sp, #52]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r2, r11
	mov	r1, #0
	mov	r11, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #16]
	ldr	r2, [r0]
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r1, [r11, #108]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	str	r0, [sp, #52]
	ldr	r0, [r6, #824]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_18
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_48
LBB130_18:
	mov	r10, r11
	ldr	r11, [sp, #56]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r2, r11
	mov	r1, #0
	mov	r11, r10
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #52]
	ldr	r2, [r0]
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r1, [r11, #108]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	mov	r10, r0
	ldr	r0, [r6, #828]
	str	r0, [sp, #52]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_20
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_49
LBB130_20:
	ldr	r5, [sp, #48]
	str	r11, [sp, #56]
	mov	r0, r5
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r6, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r11, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r5
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #832]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r11
	bl	_p_158_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_159_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object_llvm
	mov	r10, r0
	ldr	r0, [r6, #836]
	str	r0, [sp, #48]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_22
	ldr	r2, [r1]
	ldr	r0, [r6, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_50
LBB130_22:
	ldr	r11, [sp, #20]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r6, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r5, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r5
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [sp, #56]
	ldr	r1, [r0, #104]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	mov	r10, r0
	ldr	r0, [sp, #52]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	mov	r5, r6
	cmp	r1, #0
	beq	LBB130_24
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_51
LBB130_24:
	ldr	r6, [sp, #24]
	mov	r0, r6
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r11, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r6
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #832]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r11
	bl	_p_158_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_159_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object_llvm
	mov	r10, r0
	ldr	r0, [sp, #48]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_26
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_52
LBB130_26:
	ldr	r11, [sp, #28]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r6, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r6
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [sp, #56]
	ldr	r1, [r0, #100]
	ldr	r8, [sp, #60]
	ldr	r0, [sp, #64]
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	mov	r10, r0
	ldr	r0, [sp, #52]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_28
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_53
LBB130_28:
	ldr	r6, [sp, #32]
	mov	r0, r6
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r11, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r6
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #832]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r11
	bl	_p_158_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_159_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object_llvm
	mov	r10, r0
	ldr	r0, [sp, #48]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_30
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_54
LBB130_30:
	ldr	r11, [sp, #36]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r6, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r6
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [r5, #776]
	ldr	r1, [sp, #56]
	mov	r8, r0
	ldr	r0, [sp, #64]
	bl	_p_149_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Touch_Views_WorkoutView_llvm
	mov	r10, r0
	ldr	r0, [r5, #840]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_32
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_55
LBB130_32:
	ldr	r11, [sp, #40]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r6, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #784]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r6
	bl	_p_150_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r10]
	mov	r0, r10
	bl	_p_151_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_llvm
	str	r0, [sp, #60]
	ldr	r0, [r5, #844]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB130_34
	ldr	r2, [r1]
	ldr	r0, [r5, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB130_56
LBB130_34:
	ldr	r11, [sp, #44]
	mov	r0, r11
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	ldr	r1, [r5, #552]
	bl	_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm
	mov	r10, r0
	ldr	r0, [sp, #68]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r6, [sp, #56]
	mov	r1, #0
	mov	r2, r11
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r5, #792]
	mov	r1, r4
	mov	r8, r0
	mov	r0, r10
	bl	_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #60]
	ldr	r2, [r0]
	bl	_p_153_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm
	ldr	r0, [sp, #64]
	ldr	r1, [r0]
	ldr	r1, [r1, #52]
	blx	r1
	mov	r0, r6
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_160_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r10, [r0, #41]
	ldr	r0, [r6]
	ldr	r1, [r0, #180]
	mov	r0, r6
	blx	r1
	mov	r4, r0
	mov	r0, r6
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_160_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #32]
	cmp	r10, #0
	beq	LBB130_36
	bl	_p_131_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	b	LBB130_40
LBB130_36:
	cmp	r0, #0
	ldrne	r0, [r0, #8]
	cmpne	r0, #0
	beq	LBB130_38
	mov	r0, r6
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_160_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #32]
	b	LBB130_39
LBB130_38:
	bl	_p_162_plt_OffSeasonPro_Touch_Constants_get_ConcreteBackground_llvm
LBB130_39:
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
LBB130_40:
	bl	_p_102_plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #256]
	mov	r0, r4
	blx	r2
	ldr	r11, [r5, #36]
	ldr	r4, [r6, #96]
	movw	r1, #0
	movt	r1, #16948
	ldr	r0, [r11]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #300]
	mov	r0, r4
	blx	r2
	ldr	r0, [r6, #96]
	mov	r10, #0
	ldr	r1, [r0]
	ldr	r2, [r1, #272]
	mov	r1, #0
	blx	r2
	ldr	r0, [r6, #96]
	ldr	r1, [r0]
	ldr	r2, [r1, #268]
	mov	r1, #1
	blx	r2
	ldr	r4, [r6, #96]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #284]
	mov	r0, r4
	blx	r2
	movw	r5, #52429
	ldr	r0, [r6, #96]
	str	r10, [sp, #124]
	str	r10, [sp, #120]
	movt	r5, #16204
	str	r5, [sp, #120]
	str	r5, [sp, #124]
	str	r5, [sp, #84]
	ldr	r1, [sp, #120]
	str	r1, [sp, #80]
	ldr	r1, [r0]
	ldr	r2, [sp, #84]
	ldr	r3, [r1, #280]
	ldr	r1, [sp, #80]
	blx	r3
	ldr	r4, [r6, #108]
	ldr	r0, [r11]
	mov	r1, #1107296256
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #316]
	mov	r0, r4
	blx	r2
	ldr	r4, [r6, #108]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #108]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #108]
	str	r10, [sp, #132]
	str	r10, [sp, #128]
	str	r5, [sp, #128]
	str	r5, [sp, #132]
	str	r5, [sp, #92]
	ldr	r1, [sp, #128]
	str	r1, [sp, #88]
	ldr	r1, [r0]
	ldr	r2, [sp, #92]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #88]
	blx	r3
	ldr	r4, [r6, #108]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #100]
	ldr	r0, [r11]
	mov	r1, #1107296256
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #316]
	mov	r0, r4
	blx	r2
	ldr	r4, [r6, #100]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #100]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #100]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #100]
	str	r10, [sp, #140]
	str	r10, [sp, #136]
	str	r5, [sp, #136]
	str	r5, [sp, #140]
	str	r5, [sp, #100]
	ldr	r1, [sp, #136]
	str	r1, [sp, #96]
	ldr	r1, [r0]
	ldr	r2, [sp, #100]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #96]
	blx	r3
	ldr	r4, [r6, #104]
	ldr	r0, [r11]
	mov	r1, #1107296256
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #316]
	mov	r0, r4
	blx	r2
	ldr	r4, [r6, #104]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #104]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r4, [r6, #104]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #104]
	str	r10, [sp, #148]
	str	r10, [sp, #144]
	str	r5, [sp, #144]
	str	r5, [sp, #148]
	str	r5, [sp, #108]
	ldr	r1, [sp, #144]
	str	r1, [sp, #104]
	ldr	r1, [r0]
	ldr	r2, [sp, #108]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #104]
	blx	r3
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp270:
LBB130_41:
	ldr	r0, LCPI130_16
LPC130_15:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp271:
LBB130_42:
	ldr	r0, LCPI130_1
LPC130_0:
	add	r1, pc, r0
	b	LBB130_57
Ltmp272:
LBB130_43:
	ldr	r0, LCPI130_2
LPC130_1:
	add	r1, pc, r0
	b	LBB130_57
Ltmp273:
LBB130_44:
	ldr	r0, LCPI130_3
LPC130_2:
	add	r1, pc, r0
	b	LBB130_57
Ltmp274:
LBB130_45:
	ldr	r0, LCPI130_4
LPC130_3:
	add	r1, pc, r0
	b	LBB130_57
Ltmp275:
LBB130_46:
	ldr	r0, LCPI130_5
LPC130_4:
	add	r1, pc, r0
	b	LBB130_57
Ltmp276:
LBB130_47:
	ldr	r0, LCPI130_6
LPC130_5:
	add	r1, pc, r0
	b	LBB130_57
Ltmp277:
LBB130_48:
	ldr	r0, LCPI130_7
LPC130_6:
	add	r1, pc, r0
	b	LBB130_57
Ltmp278:
LBB130_49:
	ldr	r0, LCPI130_8
LPC130_7:
	add	r1, pc, r0
	b	LBB130_57
Ltmp279:
LBB130_50:
	ldr	r0, LCPI130_9
LPC130_8:
	add	r1, pc, r0
	b	LBB130_57
Ltmp280:
LBB130_51:
	ldr	r0, LCPI130_10
LPC130_9:
	add	r1, pc, r0
	b	LBB130_57
Ltmp281:
LBB130_52:
	ldr	r0, LCPI130_11
LPC130_10:
	add	r1, pc, r0
	b	LBB130_57
Ltmp282:
LBB130_53:
	ldr	r0, LCPI130_12
LPC130_11:
	add	r1, pc, r0
	b	LBB130_57
Ltmp283:
LBB130_54:
	ldr	r0, LCPI130_13
LPC130_12:
	add	r1, pc, r0
	b	LBB130_57
Ltmp284:
LBB130_55:
	ldr	r0, LCPI130_14
LPC130_13:
	add	r1, pc, r0
	b	LBB130_57
Ltmp285:
LBB130_56:
	ldr	r0, LCPI130_15
LPC130_14:
	add	r1, pc, r0
LBB130_57:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI130_1:
	.long	Ltmp271-(LPC130_0+8)
LCPI130_2:
	.long	Ltmp272-(LPC130_1+8)
LCPI130_3:
	.long	Ltmp273-(LPC130_2+8)
LCPI130_4:
	.long	Ltmp274-(LPC130_3+8)
LCPI130_5:
	.long	Ltmp275-(LPC130_4+8)
LCPI130_6:
	.long	Ltmp276-(LPC130_5+8)
LCPI130_7:
	.long	Ltmp277-(LPC130_6+8)
LCPI130_8:
	.long	Ltmp278-(LPC130_7+8)
LCPI130_9:
	.long	Ltmp279-(LPC130_8+8)
LCPI130_10:
	.long	Ltmp280-(LPC130_9+8)
LCPI130_11:
	.long	Ltmp281-(LPC130_10+8)
LCPI130_12:
	.long	Ltmp282-(LPC130_11+8)
LCPI130_13:
	.long	Ltmp283-(LPC130_12+8)
LCPI130_14:
	.long	Ltmp284-(LPC130_13+8)
LCPI130_15:
	.long	Ltmp285-(LPC130_14+8)
LCPI130_16:
	.long	Ltmp270-(LPC130_15+8)
	.end_data_region
Leh_func_end130:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_PrefersStatusBarHidden
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_PrefersStatusBarHidden:
Leh_func_begin131:
	mov	r0, #1
	bx	lr
Leh_func_end131:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidAppear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidAppear_bool:
Leh_func_begin132:
	push	{r4, r7, lr}
Ltmp286:
	add	r7, sp, #4
Ltmp287:
	push	{r8}
Ltmp288:
	mov	r4, r0
	bl	_p_74_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool_llvm
	ldrb	r0, [r4, #114]
	cmp	r0, #0
	beq	LBB132_2
	mov	r0, #0
	strb	r0, [r4, #114]
	mov	r0, r4
	bl	_p_144_plt_OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod_llvm
	b	LBB132_3
LBB132_2:
	mov	r0, r4
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_163_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC132_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC132_0+8))
LPC132_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB132_3:
	bl	_p_142_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #100]
	mov	r1, #1
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end132:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_OnMessageReceived_OffSeasonPro_Core_Messages_DisplayMessage
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_OnMessageReceived_OffSeasonPro_Core_Messages_DisplayMessage:
Leh_func_begin133:
	push	{r4, r7, lr}
Ltmp289:
	add	r7, sp, #4
Ltmp290:
Ltmp291:
	mov	r4, r0
	ldr	r0, [r1]
	ldr	r0, [r1, #12]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC133_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC133_0+8))
LPC133_0:
	add	r1, pc, r1
	ldr	r1, [r1, #848]
	bl	_p_138_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	ldrbne	r0, [r4, #112]
	cmpne	r0, #0
	beq	LBB133_2
	ldr	r0, [r4, #88]
	ldr	r1, [r0]
	ldr	r2, [r1, #76]
	mov	r1, r4
	blx	r2
	mov	r0, #1
	strb	r0, [r4, #114]
LBB133_2:
	pop	{r4, r7, pc}
Leh_func_end133:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod:
Leh_func_begin134:
	push	{r4, r5, r6, r7, lr}
Ltmp292:
	add	r7, sp, #12
Ltmp293:
	push	{r10}
Ltmp294:
	movw	r6, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC134_1+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC134_1+8))
LPC134_1:
	add	r6, pc, r6
	ldr	r0, [r6, #852]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_164_plt_GoogleAdMobAds_GADInterstitial__ctor_llvm
	ldr	r1, [r6, #856]
	ldr	r0, [r5]
	ldr	r2, [r0, #108]
	mov	r0, r5
	blx	r2
	str	r5, [r4, #88]
	ldr	r5, [r4, #88]
	bl	_p_165_plt_GoogleAdMobAds_GADRequest_get_Request_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #84]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #88]
	cmp	r4, #0
	beq	LBB134_4
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #860]
	ldr	r10, [r6, #156]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #864]
	str	r0, [r1, #28]
	str	r10, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_166_plt_GoogleAdMobAds_GADInterstitial_add_DidReceiveAd_System_EventHandler_llvm
	ldr	r0, [r6, #868]
	ldr	r5, [r4, #88]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #872]
	str	r0, [r1, #20]
	ldr	r0, [r6, #876]
	str	r0, [r1, #28]
	ldr	r0, [r6, #880]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_167_plt_GoogleAdMobAds_GADInterstitial_add_DidFailToReceiveAd_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_llvm
	ldr	r5, [r6, #884]
	ldr	r4, [r4, #88]
	ldr	r1, [r5]
	cmp	r1, #0
	bne	LBB134_3
	ldr	r0, [r6, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #888]
	str	r1, [r0, #20]
	ldr	r1, [r6, #892]
	str	r1, [r0, #28]
	str	r10, [r0, #12]
	str	r0, [r5]
	ldr	r0, [r6, #884]
	ldr	r1, [r0]
LBB134_3:
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_168_plt_GoogleAdMobAds_GADInterstitial_add_DidDismissScreen_System_EventHandler_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Ltmp295:
LBB134_4:
	ldr	r0, LCPI134_0
LPC134_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI134_0:
	.long	Ltmp295-(LPC134_0+8)
	.end_data_region
Leh_func_end134:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ReleaseDesignerOutlets
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ReleaseDesignerOutlets:
Leh_func_begin135:
	push	{r4, r7, lr}
Ltmp296:
	add	r7, sp, #4
Ltmp297:
Ltmp298:
	mov	r4, r0
	ldr	r0, [r4, #96]
	cmp	r0, #0
	beq	LBB135_2
	ldr	r0, [r4, #96]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #96]
LBB135_2:
	ldr	r0, [r4, #100]
	cmp	r0, #0
	beq	LBB135_4
	ldr	r0, [r4, #100]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #100]
LBB135_4:
	ldr	r0, [r4, #104]
	cmp	r0, #0
	beq	LBB135_6
	ldr	r0, [r4, #104]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #104]
LBB135_6:
	ldr	r0, [r4, #108]
	cmp	r0, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4, #108]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #108]
	pop	{r4, r7, pc}
Leh_func_end135:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__16_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__16_object_System_EventArgs:
Leh_func_begin136:
	push	{r7, lr}
Ltmp299:
	mov	r7, sp
Ltmp300:
Ltmp301:
	ldr	r0, [r0, #92]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #0
	blx	r2
	pop	{r7, pc}
Leh_func_end136:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__17_object_MonoTouch_iAd_AdErrorEventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__17_object_MonoTouch_iAd_AdErrorEventArgs:
Leh_func_begin137:
	push	{r7, lr}
Ltmp302:
	mov	r7, sp
Ltmp303:
Ltmp304:
	ldr	r0, [r0, #92]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #1
	blx	r2
	pop	{r7, pc}
Leh_func_end137:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__18_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__18_object_System_EventArgs:
Leh_func_begin138:
	mov	r1, #1
	strb	r1, [r0, #112]
	bx	lr
Leh_func_end138:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__19_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__19_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs:
Leh_func_begin139:
	mov	r1, #1
	strb	r1, [r0, #113]
	bx	lr
Leh_func_end139:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__1A_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__1A_object_System_EventArgs:
Leh_func_begin140:
	bx	lr
Leh_func_end140:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ctor:
Leh_func_begin141:
	push	{r4, r7, lr}
Ltmp305:
	add	r7, sp, #4
Ltmp306:
Ltmp307:
	mov	r4, r0
	bl	_p_170_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance_llvm
	str	r0, [r4, #80]
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC141_0+8))
	mov	r2, #0
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC141_0+8))
LPC141_0:
	add	r0, pc, r0
	ldr	r1, [r0, #896]
	mov	r0, r4
	bl	_p_136_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	pop	{r4, r7, pc}
Leh_func_end141:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_ViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_ViewModel:
Leh_func_begin142:
	push	{r7, lr}
Ltmp308:
	mov	r7, sp
Ltmp309:
Ltmp310:
	bl	_p_65_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldrh	r2, [r1, #12]
	cmp	r2, #7
	blo	LBB142_3
	movw	r2, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC142_2+8))
	movt	r2, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC142_2+8))
	ldr	r1, [r1, #8]
LPC142_2:
	add	r2, pc, r2
	ldr	r2, [r2, #900]
	ldr	r1, [r1, #24]
	cmp	r1, r2
	bne	LBB142_4
	pop	{r7, pc}
Ltmp311:
LBB142_3:
	ldr	r0, LCPI142_1
LPC142_1:
	add	r1, pc, r0
	b	LBB142_5
Ltmp312:
LBB142_4:
	ldr	r0, LCPI142_0
LPC142_0:
	add	r1, pc, r0
LBB142_5:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI142_0:
	.long	Ltmp312-(LPC142_0+8)
LCPI142_1:
	.long	Ltmp311-(LPC142_1+8)
	.end_data_region
Leh_func_end142:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_ViewModel_OffSeasonPro_Core_ViewModels_HomeViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_ViewModel_OffSeasonPro_Core_ViewModels_HomeViewModel:
Leh_func_begin143:
	push	{r7, lr}
Ltmp313:
	mov	r7, sp
Ltmp314:
Ltmp315:
	bl	_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r7, pc}
Leh_func_end143:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnAction
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnAction:
Leh_func_begin144:
	ldr	r0, [r0, #84]
	bx	lr
Leh_func_end144:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnAction_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnAction_MonoTouch_UIKit_UIButton:
Leh_func_begin145:
	str	r1, [r0, #84]
	bx	lr
Leh_func_end145:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnPlaylists
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnPlaylists:
Leh_func_begin146:
	ldr	r0, [r0, #88]
	bx	lr
Leh_func_end146:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnPlaylists_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnPlaylists_MonoTouch_UIKit_UIButton:
Leh_func_begin147:
	str	r1, [r0, #88]
	bx	lr
Leh_func_end147:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnSettings
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnSettings:
Leh_func_begin148:
	ldr	r0, [r0, #92]
	bx	lr
Leh_func_end148:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnSettings_MonoTouch_UIKit_UIButton
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnSettings_MonoTouch_UIKit_UIButton:
Leh_func_begin149:
	str	r1, [r0, #92]
	bx	lr
Leh_func_end149:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewWillAppear_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewWillAppear_bool:
Leh_func_begin150:
	push	{r4, r7, lr}
Ltmp316:
	add	r7, sp, #4
Ltmp317:
Ltmp318:
	mov	r4, r0
	bl	_p_171_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #160]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	mov	r2, #0
	ldr	r3, [r1, #184]
	mov	r1, #1
	blx	r3
	pop	{r4, r7, pc}
Leh_func_end150:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewDidLoad
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewDidLoad:
Leh_func_begin151:
	push	{r4, r5, r6, r7, lr}
Ltmp319:
	add	r7, sp, #12
Ltmp320:
	push	{r8, r10, r11}
Ltmp321:
	sub	sp, sp, #56
	bic	sp, sp, #7
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC151_2+8))
	mov	r4, r0
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC151_2+8))
LPC151_2:
	add	r11, pc, r11
	ldr	r1, [r11, #484]
	ldr	r0, [r11, #904]
	bl	_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm
	mov	r10, r0
	mov	r0, r4
	bl	_p_143_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad_llvm
	ldr	r0, [r11, #492]
	ldr	r6, [r11, #488]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r5, r0
	bl	_p_94_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #60]
	mov	r0, r4
	blx	r2
	tst	r0, #255
	beq	LBB151_2
	ldr	r0, [r4]
	mov	r1, #0
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
LBB151_2:
	ldr	r0, [r11, #908]
	mov	r8, r0
	mov	r0, r4
	bl	_p_172_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_OffSeasonPro_Touch_Views_HomeView_llvm
	mov	r6, r0
	ldr	r1, [r4, #92]
	ldr	r0, [r11, #912]
	ldr	r2, [r6]
	mov	r8, r0
	mov	r0, r6
	bl	_p_173_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm
	str	r0, [sp, #4]
	ldr	r0, [r11, #916]
	bl	_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB151_4
	ldr	r2, [r1]
	ldr	r0, [r11, #572]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB151_6
LBB151_4:
	mov	r0, r10
	bl	_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r11, #556]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r10
	mov	r10, r0
	mov	r1, #0
	ldr	r0, [r10]
	ldr	r3, [r0, #128]
	mov	r0, r10
	blx	r3
	ldr	r0, [r11, #920]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r5
	bl	_p_174_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [sp, #4]
	ldr	r2, [r0]
	bl	_p_175_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_HomeViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_llvm
	ldr	r0, [r6]
	ldr	r1, [r0, #52]
	mov	r0, r6
	blx	r1
	ldr	r5, [r4, #84]
	cmp	r4, #0
	beq	LBB151_7
	ldr	r0, [r11, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r11, #924]
	ldr	r6, [r11, #156]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r11, #928]
	str	r0, [r1, #28]
	str	r6, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_28_plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler_llvm
	ldr	r5, [r4, #88]
	ldr	r0, [r11, #144]
	bl	_p_25_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r11, #932]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r11, #936]
	str	r0, [r1, #28]
	str	r6, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_28_plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	mov	r5, r0
	bl	_p_101_plt_OffSeasonPro_Touch_Constants_get_Background_llvm
	bl	_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm
	bl	_p_102_plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #256]
	mov	r0, r5
	blx	r2
	ldr	r6, [r11, #36]
	ldr	r5, [r4, #84]
	movw	r1, #0
	movt	r1, #17046
	ldr	r0, [r6]
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #316]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #88]
	ldr	r0, [r6]
	mov	r1, #1107296256
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #316]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #92]
	ldr	r0, [r6]
	mov	r1, #1107296256
	bl	_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #316]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #84]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #84]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #84]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	movw	r6, #52429
	mov	r10, #0
	ldr	r0, [r4, #84]
	movt	r6, #16204
	str	r10, [sp, #36]
	str	r10, [sp, #32]
	str	r6, [sp, #32]
	str	r6, [sp, #36]
	str	r6, [sp, #12]
	ldr	r1, [sp, #32]
	str	r1, [sp, #8]
	ldr	r1, [r0]
	ldr	r2, [sp, #12]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #8]
	blx	r3
	ldr	r5, [r4, #88]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #88]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #88]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	ldr	r0, [r4, #88]
	str	r10, [sp, #44]
	str	r10, [sp, #40]
	str	r6, [sp, #40]
	str	r6, [sp, #44]
	str	r6, [sp, #20]
	ldr	r1, [sp, #40]
	str	r1, [sp, #16]
	ldr	r1, [r0]
	ldr	r2, [sp, #20]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #16]
	blx	r3
	ldr	r5, [r4, #92]
	bl	_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #304]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #92]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #0
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	ldr	r5, [r4, #92]
	bl	_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, #1
	ldr	r3, [r0, #300]
	mov	r0, r5
	blx	r3
	ldr	r0, [r4, #92]
	str	r10, [sp, #52]
	str	r10, [sp, #48]
	str	r6, [sp, #48]
	str	r6, [sp, #52]
	str	r6, [sp, #28]
	ldr	r1, [sp, #48]
	str	r1, [sp, #24]
	ldr	r1, [r0]
	ldr	r2, [sp, #28]
	ldr	r3, [r1, #312]
	ldr	r1, [sp, #24]
	blx	r3
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp322:
LBB151_6:
	ldr	r0, LCPI151_0
LPC151_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp323:
LBB151_7:
	ldr	r0, LCPI151_1
LPC151_1:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI151_0:
	.long	Ltmp322-(LPC151_0+8)
LCPI151_1:
	.long	Ltmp323-(LPC151_1+8)
	.end_data_region
Leh_func_end151:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker:
Leh_func_begin152:
	push	{r4, r5, r6, r7, lr}
Ltmp324:
	add	r7, sp, #12
Ltmp325:
	push	{r10, r11}
Ltmp326:
	movw	r11, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC152_0+8))
	mov	r10, r0
	movt	r11, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC152_0+8))
LPC152_0:
	add	r11, pc, r11
	ldr	r0, [r11, #940]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_176_plt_OffSeasonPro_Touch_Views_MediaPickerView__ctor_llvm
	ldr	r6, [r10, #80]
	ldr	r0, [r11, #944]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r6
	mov	r2, r5
	mov	r4, r0
	bl	_p_177_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_178_plt_MonoTouch_MediaPlayer_MPMediaPickerController_set_Delegate_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate_llvm
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r2, [r0, #188]
	mov	r0, r5
	blx	r2
	ldr	r0, [r5]
	ldr	r1, [r11, #948]
	ldr	r2, [r0, #184]
	mov	r0, r5
	blx	r2
	ldr	r0, [r10]
	mov	r1, r5
	mov	r2, #1
	mov	r3, #0
	ldr	r6, [r0, #104]
	mov	r0, r10
	blx	r6
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end152:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_PrefersStatusBarHidden
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_PrefersStatusBarHidden:
Leh_func_begin153:
	mov	r0, #1
	bx	lr
Leh_func_end153:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ReleaseDesignerOutlets
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ReleaseDesignerOutlets:
Leh_func_begin154:
	push	{r4, r7, lr}
Ltmp327:
	add	r7, sp, #4
Ltmp328:
Ltmp329:
	mov	r4, r0
	ldr	r0, [r4, #84]
	cmp	r0, #0
	beq	LBB154_2
	ldr	r0, [r4, #84]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #84]
LBB154_2:
	ldr	r0, [r4, #88]
	cmp	r0, #0
	beq	LBB154_4
	ldr	r0, [r4, #88]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #88]
LBB154_4:
	ldr	r0, [r4, #92]
	cmp	r0, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4, #92]
	ldr	r1, [r0]
	bl	_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm
	mov	r0, #0
	str	r0, [r4, #92]
	pop	{r4, r7, pc}
Leh_func_end154:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1B_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1B_object_System_EventArgs:
Leh_func_begin155:
	push	{r4, r5, r6, r7, lr}
Ltmp330:
	add	r7, sp, #12
Ltmp331:
	push	{r8, r10, r11}
Ltmp332:
	sub	sp, sp, #16
	mov	r6, r0
	bl	_p_179_plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_180_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	bl	_p_181_plt_OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout_llvm
	tst	r0, #255
	beq	LBB155_2
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC155_1+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC155_1+8))
LPC155_1:
	add	r5, pc, r5
	ldr	r0, [r5, #952]
	str	r0, [sp, #12]
	ldr	r0, [r5, #956]
	str	r0, [sp, #8]
	mov	r0, r6
	bl	_p_179_plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel_llvm
	mov	r4, r0
	ldr	r0, [r5, #960]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r6, r0
	bl	_p_183_plt_OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel_llvm
	ldr	r0, [r5, #452]
	ldr	r10, [r5, #964]
	mov	r1, #1
	bl	_p_35_plt__jit_icall_mono_array_new_specific_llvm
	mov	r11, r0
	ldr	r2, [r5, #968]
	mov	r1, #0
	ldr	r0, [r11]
	ldr	r3, [r0, #128]
	mov	r0, r11
	blx	r3
	ldr	r0, [r5, #136]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	ldr	r1, [sp, #12]
	ldr	r2, [sp, #8]
	mov	r3, r6
	mov	r4, r0
	strd	r10, r11, [sp]
	bl	_p_26_plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string___llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #264]
	mov	r0, r4
	blx	r1
	b	LBB155_3
LBB155_2:
	mov	r0, r6
	bl	_p_179_plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_182_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC155_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC155_0+8))
LPC155_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB155_3:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end155:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1C_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1C_object_System_EventArgs:
Leh_func_begin156:
	push	{r7, lr}
Ltmp333:
	mov	r7, sp
Ltmp334:
Ltmp335:
	bl	_p_184_plt_OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker_llvm
	pop	{r7, pc}
Leh_func_end156:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel:
Leh_func_begin157:
	push	{r4, r5, r7, lr}
Ltmp336:
	add	r7, sp, #8
Ltmp337:
Ltmp338:
	mov	r4, r1
	mov	r5, r0
	bl	_p_80_plt_MonoTouch_UIKit_UIAlertViewDelegate__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end157:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int:
Leh_func_begin158:
	push	{r4, r7, lr}
Ltmp339:
	add	r7, sp, #4
Ltmp340:
	push	{r8}
Ltmp341:
	mov	r4, r0
	cmp	r2, #0
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
	beq	LBB158_2
	bl	_p_180_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings_llvm
	bl	_p_185_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
LBB158_2:
	bl	_p_182_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC158_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC158_0+8))
LPC158_0:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end158:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool:
Leh_func_begin159:
	push	{r4, r5, r6, r7, lr}
Ltmp342:
	add	r7, sp, #12
Ltmp343:
	push	{r10}
Ltmp344:
	mov	r4, r1
	mov	r6, r0
	mov	r10, r3
	mov	r5, r2
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	add	r1, r6, #12
	stm	r1, {r0, r4, r5}
	strb	r10, [r6, #24]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end159:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_get_Visible
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_get_Visible:
Leh_func_begin160:
	ldr	r0, [r0, #8]
	cmp	r0, #0
	movne	r0, #1
	bx	lr
Leh_func_end160:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_set_Visible_bool
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_set_Visible_bool:
Leh_func_begin161:
	push	{r4, r5, r6, r7, lr}
Ltmp345:
	add	r7, sp, #12
Ltmp346:
	push	{r8}
Ltmp347:
	mov	r4, r0
	ldr	r0, [r4, #8]
	cmp	r0, #0
	movne	r0, #1
	cmp	r0, r1
	beq	LBB161_9
	cmp	r1, #0
	beq	LBB161_3
	ldr	r6, [r4, #12]
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC161_3+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC161_3+8))
LPC161_3:
	add	r0, pc, r0
	ldr	r0, [r0, #980]
	bl	_p_6_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r6
	mov	r5, r0
	bl	_p_186_plt_MBProgressHUD_MTMBProgressHUD__ctor_MonoTouch_UIKit_UIView_llvm
	ldr	r1, [r4, #20]
	ldr	r0, [r5]
	ldr	r2, [r0, #448]
	mov	r0, r5
	blx	r2
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r2, [r0, #352]
	mov	r0, r5
	blx	r2
	str	r5, [r4, #8]
	ldr	r0, [r4, #12]
	ldr	r1, [r4, #8]
	ldr	r2, [r0]
	ldr	r2, [r2, #148]
	blx	r2
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #292]
	mov	r1, #1
	blx	r2
	b	LBB161_9
LBB161_3:
	ldr	r0, [r4, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #288]
	mov	r1, #1
	blx	r2
	mov	r0, #0
	str	r0, [r4, #8]
	ldrb	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB161_9
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	LBB161_6
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC161_5+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC161_5+8))
	ldr	r2, [r0]
LPC161_5:
	add	r1, pc, r1
	ldr	r1, [r1, #976]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #20]
	cmp	r2, r1
	bne	LBB161_10
LBB161_6:
	ldr	r1, [r0]
	bl	_p_65_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel_llvm
	cmp	r0, #0
	beq	LBB161_8
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC161_4+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC161_4+8))
	ldr	r2, [r0]
LPC161_4:
	add	r1, pc, r1
	ldr	r1, [r1, #972]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #20]
	cmp	r2, r1
	bne	LBB161_11
LBB161_8:
	ldr	r1, [r0]
	bl	_p_77_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC161_2+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC161_2+8))
LPC161_2:
	add	r1, pc, r1
	ldr	r1, [r1, #436]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB161_9:
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp348:
LBB161_10:
	ldr	r0, LCPI161_0
LPC161_0:
	add	r1, pc, r0
	b	LBB161_12
Ltmp349:
LBB161_11:
	ldr	r0, LCPI161_1
LPC161_1:
	add	r1, pc, r0
LBB161_12:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI161_0:
	.long	Ltmp348-(LPC161_0+8)
LCPI161_1:
	.long	Ltmp349-(LPC161_1+8)
	.end_data_region
Leh_func_end161:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate__ctor:
Leh_func_begin162:
	push	{r7, lr}
Ltmp350:
	mov	r7, sp
Ltmp351:
Ltmp352:
	bl	_p_187_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor_llvm
	pop	{r7, pc}
Leh_func_end162:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication:
Leh_func_begin163:
	bx	lr
Leh_func_end163:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_OnResignActivation_MonoTouch_UIKit_UIApplication
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_OnResignActivation_MonoTouch_UIKit_UIApplication:
Leh_func_begin164:
	push	{r4, r5, r7, lr}
Ltmp353:
	add	r7, sp, #8
Ltmp354:
	push	{r8}
Ltmp355:
	ldr	r0, [r0, #24]
	ldr	r1, [r0]
	ldr	r1, [r1, #272]
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #152]
	blx	r1
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC164_4+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC164_4+8))
LPC164_4:
	add	r5, pc, r5
	ldr	r1, [r5, #984]
	mov	r8, r1
	bl	_p_188_plt_System_Linq_Enumerable_Last_MonoTouch_UIKit_UIViewController_System_Collections_Generic_IEnumerable_1_MonoTouch_UIKit_UIViewController_llvm
	mov	r4, r0
	cmp	r4, #0
	beq	LBB164_3
	ldr	r0, [r4]
	ldr	r0, [r0]
	ldrh	r1, [r0, #12]
	cmp	r1, #7
	blo	LBB164_12
	ldr	r0, [r0, #8]
	ldr	r1, [r5, #988]
	ldr	r0, [r0, #24]
	cmp	r0, r1
	bne	LBB164_12
LBB164_3:
	cmp	r4, #0
	beq	LBB164_12
	cmp	r4, #0
	beq	LBB164_7
	ldr	r0, [r4]
	ldr	r0, [r0]
	ldrh	r1, [r0, #12]
	cmp	r1, #7
	blo	LBB164_13
	ldr	r0, [r0, #8]
	ldr	r1, [r5, #988]
	ldr	r0, [r0, #24]
	cmp	r0, r1
	bne	LBB164_14
LBB164_7:
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	ldrb	r0, [r0, #60]
	cmp	r0, #0
	beq	LBB164_12
	cmp	r4, #0
	beq	LBB164_11
	ldr	r0, [r4]
	ldr	r0, [r0]
	ldrh	r1, [r0, #12]
	cmp	r1, #6
	bls	LBB164_15
	ldr	r0, [r0, #8]
	ldr	r1, [r5, #988]
	ldr	r0, [r0, #24]
	cmp	r0, r1
	bne	LBB164_16
LBB164_11:
	mov	r0, r4
	bl	_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm
	ldr	r1, [r0]
	bl	_p_189_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #436]
	sub	r2, r2, #36
	mov	r8, r1
	mov	r1, #0
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
LBB164_12:
	pop	{r8}
	pop	{r4, r5, r7, pc}
Ltmp356:
LBB164_13:
	ldr	r0, LCPI164_1
LPC164_1:
	add	r1, pc, r0
	b	LBB164_17
Ltmp357:
LBB164_14:
	ldr	r0, LCPI164_0
LPC164_0:
	add	r1, pc, r0
	b	LBB164_17
Ltmp358:
LBB164_15:
	ldr	r0, LCPI164_3
LPC164_3:
	add	r1, pc, r0
	b	LBB164_17
Ltmp359:
LBB164_16:
	ldr	r0, LCPI164_2
LPC164_2:
	add	r1, pc, r0
LBB164_17:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI164_0:
	.long	Ltmp357-(LPC164_0+8)
LCPI164_1:
	.long	Ltmp356-(LPC164_1+8)
LCPI164_2:
	.long	Ltmp359-(LPC164_2+8)
LCPI164_3:
	.long	Ltmp358-(LPC164_3+8)
	.end_data_region
Leh_func_end164:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication:
Leh_func_begin165:
	push	{r7, lr}
Ltmp360:
	mov	r7, sp
Ltmp361:
Ltmp362:
	bl	_p_190_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication_llvm
	pop	{r7, pc}
Leh_func_end165:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__ctor:
Leh_func_begin166:
	bx	lr
Leh_func_end166:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__m__2_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__m__2_object_System_EventArgs:
Leh_func_begin167:
	push	{r4, r7, lr}
Ltmp363:
	add	r7, sp, #4
Ltmp364:
Ltmp365:
	ldr	r4, [r0, #8]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #292]
	mov	r1, #0
	blx	r2
	mov	r1, r0
	ldr	r0, [r4]
	mov	r2, #0
	ldr	r3, [r0, #308]
	mov	r0, r4
	blx	r3
	pop	{r4, r7, pc}
Leh_func_end167:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__ctor:
Leh_func_begin168:
	bx	lr
Leh_func_end168:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__m__3_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__m__3_object_System_EventArgs:
Leh_func_begin169:
	push	{r4, r7, lr}
Ltmp366:
	add	r7, sp, #4
Ltmp367:
Ltmp368:
	ldr	r4, [r0, #8]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #84]
	blx	r1
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC169_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC169_0+8))
LPC169_0:
	add	r1, pc, r1
	ldr	r1, [r1, #92]
	ldr	r1, [r1]
	bl	_p_30_plt_string_Concat_string_string_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #80]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end169:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__ctor:
Leh_func_begin170:
	bx	lr
Leh_func_end170:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__m__4_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__m__4_object_System_EventArgs:
Leh_func_begin171:
	push	{r7, lr}
Ltmp369:
	mov	r7, sp
Ltmp370:
Ltmp371:
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC171_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC171_0+8))
LPC171_0:
	add	r1, pc, r1
	ldr	r1, [r1, #92]
	ldr	r1, [r1]
	ldr	r2, [r0]
	ldr	r2, [r2, #340]
	blx	r2
	pop	{r7, pc}
Leh_func_end171:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__ctor:
Leh_func_begin172:
	bx	lr
Leh_func_end172:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__m__5_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__m__5_object_System_EventArgs:
Leh_func_begin173:
	push	{r7, lr}
Ltmp372:
	mov	r7, sp
Ltmp373:
Ltmp374:
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC173_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC173_0+8))
LPC173_0:
	add	r1, pc, r1
	ldr	r1, [r1, #92]
	ldr	r1, [r1]
	ldr	r2, [r0]
	ldr	r2, [r2, #300]
	blx	r2
	pop	{r7, pc}
Leh_func_end173:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__ctor:
Leh_func_begin174:
	bx	lr
Leh_func_end174:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__m__6_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__m__6_object_System_EventArgs:
Leh_func_begin175:
	push	{r4, r7, lr}
Ltmp375:
	add	r7, sp, #4
Ltmp376:
Ltmp377:
	sub	sp, sp, #12
	bic	sp, sp, #7
	ldr	r4, [r0, #8]
	bl	_p_191_plt__class_init_System_DateTime_llvm
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC175_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC175_0+8))
LPC175_0:
	add	r0, pc, r0
	ldr	r1, [r0, #992]
	ldr	r0, [r1]
	ldr	r1, [r1, #4]
	stm	sp, {r0, r1}
	ldm	sp, {r0, r1}
	bl	_p_192_plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #292]
	mov	r0, r4
	blx	r2
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end175:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__ctor:
Leh_func_begin176:
	bx	lr
Leh_func_end176:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__m__7_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__m__7_object_System_EventArgs:
Leh_func_begin177:
	push	{r7, lr}
Ltmp378:
	mov	r7, sp
Ltmp379:
Ltmp380:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #304]
	mov	r1, #1065353216
	blx	r2
	pop	{r7, pc}
Leh_func_end177:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__ctor:
Leh_func_begin178:
	bx	lr
Leh_func_end178:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__m__8_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__m__8_object_System_EventArgs:
Leh_func_begin179:
	push	{r7, lr}
Ltmp381:
	mov	r7, sp
Ltmp382:
Ltmp383:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #292]
	mov	r1, #0
	blx	r2
	pop	{r7, pc}
Leh_func_end179:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__ctor:
Leh_func_begin180:
	bx	lr
Leh_func_end180:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__m__A_object_System_EventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__m__A_object_System_EventArgs:
Leh_func_begin181:
	push	{r4, r5, r7, lr}
Ltmp384:
	add	r7, sp, #8
Ltmp385:
	push	{r8}
Ltmp386:
	mov	r4, r0
	ldr	r0, [r4, #8]
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC181_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC181_0+8))
LPC181_0:
	add	r5, pc, r5
	ldr	r1, [r5, #996]
	sub	r2, r2, #16
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	tst	r0, #255
	beq	LBB181_2
	ldr	r0, [r4, #8]
	ldr	r1, [r5, #436]
	ldr	r2, [r0]
	mov	r8, r1
	mov	r1, #0
	sub	r2, r2, #36
	ldr	r2, [r2]
	blx	r2
LBB181_2:
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end181:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__ctor:
Leh_func_begin182:
	bx	lr
Leh_func_end182:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__B
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__B:
Leh_func_begin183:
	push	{r4, r7, lr}
Ltmp387:
	add	r7, sp, #4
Ltmp388:
Ltmp389:
	ldr	r4, [r0, #12]
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC183_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC183_0+8))
LPC183_0:
	add	r1, pc, r1
	ldr	r1, [r1, #1000]
	bl	_p_30_plt_string_Concat_string_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_193_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string_llvm
	pop	{r4, r7, pc}
Leh_func_end183:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__C
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__C:
Leh_func_begin184:
	push	{r7, lr}
Ltmp390:
	mov	r7, sp
Ltmp391:
Ltmp392:
	mov	r1, r0
	ldr	r0, [r1, #12]
	ldr	r1, [r1, #8]
	bl	_p_193_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string_llvm
	pop	{r7, pc}
Leh_func_end184:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStorey9__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStorey9__ctor:
Leh_func_begin185:
	bx	lr
Leh_func_end185:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__ctor
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__ctor:
Leh_func_begin186:
	bx	lr
Leh_func_end186:

	.private_extern	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__m__10_object_MonoTouch_UIKit_UIButtonEventArgs
	.align	2
_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__m__10_object_MonoTouch_UIKit_UIButtonEventArgs:
Leh_func_begin187:
	push	{r4, r7, lr}
Ltmp393:
	add	r7, sp, #4
Ltmp394:
Ltmp395:
	mov	r4, r0
	ldr	r0, [r2]
	ldr	r0, [r2, #8]
	cmp	r0, #2
	bhi	LBB187_4
	cmp	r0, #1
	bne	LBB187_3
	ldr	r0, [r4, #12]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #224]
	mov	r1, #0
	b	LBB187_6
LBB187_3:
	cmp	r0, #2
	bne	LBB187_5
LBB187_4:
	ldr	r0, [r4, #8]
	mov	r2, #1
	ldr	r1, [r0]
	ldr	r3, [r1, #264]
	mov	r1, #2
	blx	r3
	pop	{r4, r7, pc}
LBB187_5:
	ldr	r0, [r4, #12]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #224]
	mov	r1, #1
LBB187_6:
	blx	r2
	ldr	r0, [r4, #12]
	mov	r3, #0
	ldr	r0, [r0, #12]
	ldr	r1, [r4, #12]
	ldr	r1, [r1, #8]
	ldr	r2, [r0]
	ldr	r4, [r2, #104]
	mov	r2, #1
	blx	r4
	pop	{r4, r7, pc}
Leh_func_end187:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_invoke_TResult__this__
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_invoke_TResult__this__:
Leh_func_begin188:
	push	{r4, r7, lr}
Ltmp396:
	add	r7, sp, #4
Ltmp397:
Ltmp398:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC188_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC188_0+8))
LPC188_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB188_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB188_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB188_4
	ldr	r1, [r0, #12]
	blx	r1
LBB188_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB188_6
	blx	r1
	pop	{r4, r7, pc}
LBB188_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end188:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs:
Leh_func_begin189:
	push	{r4, r5, r6, r7, lr}
Ltmp399:
	add	r7, sp, #12
Ltmp400:
Ltmp401:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC189_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC189_0+8))
LPC189_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB189_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB189_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB189_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB189_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB189_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB189_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end189:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UIButtonEventArgs
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UIButtonEventArgs:
Leh_func_begin190:
	push	{r4, r5, r6, r7, lr}
Ltmp402:
	add	r7, sp, #12
Ltmp403:
Ltmp404:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC190_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC190_0+8))
LPC190_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB190_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB190_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB190_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB190_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB190_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB190_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end190:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_Action_1_OffSeasonPro_Core_Messages_DisplayMessage_invoke_void__this___T_OffSeasonPro_Core_Messages_DisplayMessage
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_Action_1_OffSeasonPro_Core_Messages_DisplayMessage_invoke_void__this___T_OffSeasonPro_Core_Messages_DisplayMessage:
Leh_func_begin191:
	push	{r4, r5, r7, lr}
Ltmp405:
	add	r7, sp, #8
Ltmp406:
Ltmp407:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC191_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC191_0+8))
LPC191_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB191_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB191_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB191_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB191_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB191_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB191_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end191:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_iAd_AdErrorEventArgs
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_iAd_AdErrorEventArgs:
Leh_func_begin192:
	push	{r4, r5, r6, r7, lr}
Ltmp408:
	add	r7, sp, #12
Ltmp409:
Ltmp410:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC192_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC192_0+8))
LPC192_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB192_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB192_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB192_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB192_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB192_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB192_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end192:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_invoke_void__this___object_TEventArgs_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_invoke_void__this___object_TEventArgs_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs:
Leh_func_begin193:
	push	{r4, r5, r6, r7, lr}
Ltmp411:
	add	r7, sp, #12
Ltmp412:
Ltmp413:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC193_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC193_0+8))
LPC193_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB193_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB193_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB193_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB193_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB193_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB193_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end193:

	.private_extern	_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
	.align	2
_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__:
Leh_func_begin194:
	push	{r4, r7, lr}
Ltmp414:
	add	r7, sp, #4
Ltmp415:
Ltmp416:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProTouch_got-(LPC194_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProTouch_got-(LPC194_0+8))
LPC194_0:
	add	r0, pc, r0
	ldr	r0, [r0, #1004]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB194_2
	bl	_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB194_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB194_4
	ldr	r1, [r0, #12]
	blx	r1
LBB194_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB194_6
	blx	r1
	pop	{r4, r7, pc}
LBB194_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end194:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_mono_aot_OffSeasonProTouch_got,2100,4
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_EmailPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_FilePluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonLocalisationPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MessengerPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MusicPlayerPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_ResourceLoaderPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_TextToSpeechPluginBootstrap__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_Background
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_ConcreteBackground
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Constants__cctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_Alignment
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_Alignment_MonoTouch_UIKit_UITextAlignment
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_MaxLength
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_MaxLength_int
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_CellKey
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__cctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__CreateTextFieldm__0_MonoTouch_UIKit_UITextField_MonoTouch_Foundation_NSRange_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement__ctor_string_string_MonoTouch_UIKit_UIImage
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement_GetCellImpl_MonoTouch_UIKit_UITableView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer_ShowError_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ErrorDisplayerm__1_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIBarButtonItem
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextField
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UILabel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIImageView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIDatePicker
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISlider
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISwitch
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Collections_Specialized_INotifyCollectionChanged
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Windows_Input_ICommand
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includem__9_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Application__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Application_Main_string__
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_RequestClose_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Setup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Setup_InitializeLastChance
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_Rows
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_Rows_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_FontSize
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_FontSize_single
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_ImageViewSize
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_ImageViewSize_System_Drawing_SizeF
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ShouldHighlightItem_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemHighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemUnhighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Image
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Image_MonoTouch_UIKit_UIImage
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Tapped
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Tapped_MonoTouch_Foundation_NSAction
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_get_ImageView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_set_ImageView_MonoTouch_UIKit_UIImageView
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell__cctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_set_ViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PrefersStatusBarHidden
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidAppear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewWillDisappear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__D_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__E_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__F_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_FinishedPickingMedia_MonoTouch_UIKit_UIImagePickerController_MonoTouch_Foundation_NSDictionary
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_Canceled_MonoTouch_UIKit_UIImagePickerController
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PrefersStatusBarHidden
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PreferredStatusBarStyle
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_ViewDidLoad
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_get_ViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_set_ViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidLoad
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_PrefersStatusBarHidden
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidAppear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewWillDisappear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ResetDisplay
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__11_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__12_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__get_InputAccessoryViewm__13_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__14
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__15
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_Val
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_Val_int
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ResizeLabel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ResizeLabel_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_lblCurrentPosition
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_lblCurrentPosition_MonoTouch_UIKit_UILabel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnPauseResume
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnPauseResume_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnReset
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnReset_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnBack
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnBack_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidDisappear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidLoad
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_PrefersStatusBarHidden
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidAppear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_OnMessageReceived_OffSeasonPro_Core_Messages_DisplayMessage
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ReleaseDesignerOutlets
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__16_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__17_object_MonoTouch_iAd_AdErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__18_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__19_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__1A_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_ViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_ViewModel_OffSeasonPro_Core_ViewModels_HomeViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnAction
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnAction_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnPlaylists
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnPlaylists_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnSettings
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnSettings_MonoTouch_UIKit_UIButton
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewWillAppear_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewDidLoad
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_PrefersStatusBarHidden
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ReleaseDesignerOutlets
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1B_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1C_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_get_Visible
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_set_Visible_bool
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_OnResignActivation_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__m__2_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__m__3_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__m__4_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__m__5_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__m__6_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__m__7_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__m__8_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__m__A_object_System_EventArgs
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__B
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__C
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStorey9__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__ctor
	.no_dead_strip	_OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__m__10_object_MonoTouch_UIKit_UIButtonEventArgs
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_invoke_TResult__this__
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UIButtonEventArgs
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_Action_1_OffSeasonPro_Core_Messages_DisplayMessage_invoke_void__this___T_OffSeasonPro_Core_Messages_DisplayMessage
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_iAd_AdErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_invoke_void__this___object_TEventArgs_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	.no_dead_strip	_OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
	.no_dead_strip	_mono_aot_OffSeasonProTouch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	195
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	21
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	22
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	23
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	24
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	25
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	27
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	28
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	29
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	30
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	31
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	32
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	33
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	34
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	36
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	37
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	38
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	39
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	40
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	41
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	42
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	43
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	44
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	45
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	46
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	47
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	48
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	49
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	50
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	51
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	52
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	53
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	54
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	55
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	56
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	57
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	59
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	60
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	61
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	62
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	63
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	64
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	65
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	66
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	67
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	68
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	69
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	70
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	71
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	72
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	74
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	75
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	76
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	77
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	78
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	80
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	81
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	83
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	84
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	85
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	86
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	88
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	89
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	90
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	91
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	92
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	93
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	94
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	95
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	96
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	97
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	98
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	99
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	100
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	101
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	102
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	103
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	104
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	105
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	106
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	107
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	108
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	109
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	111
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	112
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	113
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	114
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	115
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	116
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	117
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	118
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	119
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	120
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	121
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	122
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	123
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	124
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	125
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	126
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	127
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	128
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	129
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	130
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	131
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	132
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	133
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	134
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	135
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	136
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	137
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	138
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	139
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	140
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	142
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	143
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	144
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
	.long	145
Lset137 = Lmono_eh_func_begin137-mono_eh_frame
	.long	Lset137
	.long	146
Lset138 = Lmono_eh_func_begin138-mono_eh_frame
	.long	Lset138
	.long	147
Lset139 = Lmono_eh_func_begin139-mono_eh_frame
	.long	Lset139
	.long	148
Lset140 = Lmono_eh_func_begin140-mono_eh_frame
	.long	Lset140
	.long	149
Lset141 = Lmono_eh_func_begin141-mono_eh_frame
	.long	Lset141
	.long	150
Lset142 = Lmono_eh_func_begin142-mono_eh_frame
	.long	Lset142
	.long	151
Lset143 = Lmono_eh_func_begin143-mono_eh_frame
	.long	Lset143
	.long	152
Lset144 = Lmono_eh_func_begin144-mono_eh_frame
	.long	Lset144
	.long	153
Lset145 = Lmono_eh_func_begin145-mono_eh_frame
	.long	Lset145
	.long	154
Lset146 = Lmono_eh_func_begin146-mono_eh_frame
	.long	Lset146
	.long	155
Lset147 = Lmono_eh_func_begin147-mono_eh_frame
	.long	Lset147
	.long	156
Lset148 = Lmono_eh_func_begin148-mono_eh_frame
	.long	Lset148
	.long	157
Lset149 = Lmono_eh_func_begin149-mono_eh_frame
	.long	Lset149
	.long	158
Lset150 = Lmono_eh_func_begin150-mono_eh_frame
	.long	Lset150
	.long	159
Lset151 = Lmono_eh_func_begin151-mono_eh_frame
	.long	Lset151
	.long	160
Lset152 = Lmono_eh_func_begin152-mono_eh_frame
	.long	Lset152
	.long	161
Lset153 = Lmono_eh_func_begin153-mono_eh_frame
	.long	Lset153
	.long	162
Lset154 = Lmono_eh_func_begin154-mono_eh_frame
	.long	Lset154
	.long	163
Lset155 = Lmono_eh_func_begin155-mono_eh_frame
	.long	Lset155
	.long	164
Lset156 = Lmono_eh_func_begin156-mono_eh_frame
	.long	Lset156
	.long	165
Lset157 = Lmono_eh_func_begin157-mono_eh_frame
	.long	Lset157
	.long	166
Lset158 = Lmono_eh_func_begin158-mono_eh_frame
	.long	Lset158
	.long	167
Lset159 = Lmono_eh_func_begin159-mono_eh_frame
	.long	Lset159
	.long	168
Lset160 = Lmono_eh_func_begin160-mono_eh_frame
	.long	Lset160
	.long	169
Lset161 = Lmono_eh_func_begin161-mono_eh_frame
	.long	Lset161
	.long	170
Lset162 = Lmono_eh_func_begin162-mono_eh_frame
	.long	Lset162
	.long	172
Lset163 = Lmono_eh_func_begin163-mono_eh_frame
	.long	Lset163
	.long	173
Lset164 = Lmono_eh_func_begin164-mono_eh_frame
	.long	Lset164
	.long	174
Lset165 = Lmono_eh_func_begin165-mono_eh_frame
	.long	Lset165
	.long	175
Lset166 = Lmono_eh_func_begin166-mono_eh_frame
	.long	Lset166
	.long	176
Lset167 = Lmono_eh_func_begin167-mono_eh_frame
	.long	Lset167
	.long	177
Lset168 = Lmono_eh_func_begin168-mono_eh_frame
	.long	Lset168
	.long	178
Lset169 = Lmono_eh_func_begin169-mono_eh_frame
	.long	Lset169
	.long	179
Lset170 = Lmono_eh_func_begin170-mono_eh_frame
	.long	Lset170
	.long	180
Lset171 = Lmono_eh_func_begin171-mono_eh_frame
	.long	Lset171
	.long	181
Lset172 = Lmono_eh_func_begin172-mono_eh_frame
	.long	Lset172
	.long	182
Lset173 = Lmono_eh_func_begin173-mono_eh_frame
	.long	Lset173
	.long	183
Lset174 = Lmono_eh_func_begin174-mono_eh_frame
	.long	Lset174
	.long	184
Lset175 = Lmono_eh_func_begin175-mono_eh_frame
	.long	Lset175
	.long	185
Lset176 = Lmono_eh_func_begin176-mono_eh_frame
	.long	Lset176
	.long	186
Lset177 = Lmono_eh_func_begin177-mono_eh_frame
	.long	Lset177
	.long	187
Lset178 = Lmono_eh_func_begin178-mono_eh_frame
	.long	Lset178
	.long	188
Lset179 = Lmono_eh_func_begin179-mono_eh_frame
	.long	Lset179
	.long	189
Lset180 = Lmono_eh_func_begin180-mono_eh_frame
	.long	Lset180
	.long	190
Lset181 = Lmono_eh_func_begin181-mono_eh_frame
	.long	Lset181
	.long	191
Lset182 = Lmono_eh_func_begin182-mono_eh_frame
	.long	Lset182
	.long	192
Lset183 = Lmono_eh_func_begin183-mono_eh_frame
	.long	Lset183
	.long	193
Lset184 = Lmono_eh_func_begin184-mono_eh_frame
	.long	Lset184
	.long	194
Lset185 = Lmono_eh_func_begin185-mono_eh_frame
	.long	Lset185
	.long	195
Lset186 = Lmono_eh_func_begin186-mono_eh_frame
	.long	Lset186
	.long	196
Lset187 = Lmono_eh_func_begin187-mono_eh_frame
	.long	Lset187
	.long	198
Lset188 = Lmono_eh_func_begin188-mono_eh_frame
	.long	Lset188
	.long	199
Lset189 = Lmono_eh_func_begin189-mono_eh_frame
	.long	Lset189
	.long	200
Lset190 = Lmono_eh_func_begin190-mono_eh_frame
	.long	Lset190
	.long	201
Lset191 = Lmono_eh_func_begin191-mono_eh_frame
	.long	Lset191
	.long	202
Lset192 = Lmono_eh_func_begin192-mono_eh_frame
	.long	Lset192
	.long	203
Lset193 = Lmono_eh_func_begin193-mono_eh_frame
	.long	Lset193
	.long	204
Lset194 = Lmono_eh_func_begin194-mono_eh_frame
	.long	Lset194
Lset195 = Leh_func_end194-Leh_func_begin194
	.long	Lset195
Lset196 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset196
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0

Lmono_eh_func_begin27:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	5
	.byte	138
	.byte	6

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin30:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin31:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin45:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin46:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin51:
	.byte	0

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin58:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin60:
	.byte	0

Lmono_eh_func_begin61:
	.byte	0

Lmono_eh_func_begin62:
	.byte	0

Lmono_eh_func_begin63:
	.byte	0

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin66:
	.byte	0

Lmono_eh_func_begin67:
	.byte	0

Lmono_eh_func_begin68:
	.byte	0

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin70:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin71:
	.byte	0

Lmono_eh_func_begin72:
	.byte	0

Lmono_eh_func_begin73:
	.byte	0

Lmono_eh_func_begin74:
	.byte	0

Lmono_eh_func_begin75:
	.byte	0

Lmono_eh_func_begin76:
	.byte	0

Lmono_eh_func_begin77:
	.byte	0

Lmono_eh_func_begin78:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin80:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin81:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin82:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin83:
	.byte	0

Lmono_eh_func_begin84:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin85:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin86:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin88:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin89:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin92:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin93:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin95:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin96:
	.byte	0

Lmono_eh_func_begin97:
	.byte	0

Lmono_eh_func_begin98:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin101:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin102:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin103:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin105:
	.byte	0

Lmono_eh_func_begin106:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin107:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin108:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin109:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin110:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin113:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin114:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin115:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin116:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin117:
	.byte	0

Lmono_eh_func_begin118:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin119:
	.byte	0

Lmono_eh_func_begin120:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin121:
	.byte	0

Lmono_eh_func_begin122:
	.byte	0

Lmono_eh_func_begin123:
	.byte	0

Lmono_eh_func_begin124:
	.byte	0

Lmono_eh_func_begin125:
	.byte	0

Lmono_eh_func_begin126:
	.byte	0

Lmono_eh_func_begin127:
	.byte	0

Lmono_eh_func_begin128:
	.byte	0

Lmono_eh_func_begin129:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin131:
	.byte	0

Lmono_eh_func_begin132:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin135:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin136:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin137:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin138:
	.byte	0

Lmono_eh_func_begin139:
	.byte	0

Lmono_eh_func_begin140:
	.byte	0

Lmono_eh_func_begin141:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin142:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin143:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin144:
	.byte	0

Lmono_eh_func_begin145:
	.byte	0

Lmono_eh_func_begin146:
	.byte	0

Lmono_eh_func_begin147:
	.byte	0

Lmono_eh_func_begin148:
	.byte	0

Lmono_eh_func_begin149:
	.byte	0

Lmono_eh_func_begin150:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin151:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin152:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin153:
	.byte	0

Lmono_eh_func_begin154:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin155:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin156:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin157:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin158:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin159:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin160:
	.byte	0

Lmono_eh_func_begin161:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin162:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin163:
	.byte	0

Lmono_eh_func_begin164:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin165:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin166:
	.byte	0

Lmono_eh_func_begin167:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin168:
	.byte	0

Lmono_eh_func_begin169:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin170:
	.byte	0

Lmono_eh_func_begin171:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin172:
	.byte	0

Lmono_eh_func_begin173:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin174:
	.byte	0

Lmono_eh_func_begin175:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin176:
	.byte	0

Lmono_eh_func_begin177:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin178:
	.byte	0

Lmono_eh_func_begin179:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin180:
	.byte	0

Lmono_eh_func_begin181:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin182:
	.byte	0

Lmono_eh_func_begin183:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin184:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin185:
	.byte	0

Lmono_eh_func_begin186:
	.byte	0

Lmono_eh_func_begin187:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin188:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin189:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin190:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin191:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin192:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin193:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin194:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "OffSeasonProTouch.exe"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF
_OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,96,208,77,226,13,176,160,225,0,160,160,225,72,16,139,229,76,32,139,229
	.byte 80,48,139,229,128,224,157,229,84,224,139,229,88,0,154,229,2,0,80,227,16,0,0,26,20,234,155,237,206,10,183,238
	.byte 192,235,183,238,4,234,139,237,4,234,155,237,206,10,183,238,0,26,159,237,0,0,0,234,0,0,32,65,193,26,183,238
	.byte 65,11,48,238,192,235,183,238,5,234,139,237,5,234,155,237,206,10,183,238,192,235,183,238,20,234,139,237,72,0,155,229
	.byte 48,0,139,229,76,0,155,229,52,0,139,229,80,0,155,229,56,0,139,229,84,0,155,229,60,0,139,229,10,0,160,225
	.byte 48,16,155,229,52,32,155,229,56,48,155,229,60,192,155,229,0,192,141,229
bl _p_199

	.byte 0,96,160,225,6,32,160,225,88,16,154,229,2,0,160,225,0,32,146,229,15,224,160,225,72,241,146,229,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 48
	.byte 0,0,159,231,0,0,144,229,0,10,159,237,0,0,0,234,0,0,208,65,192,10,183,238,192,235,183,238,0,234,141,237
	.byte 0,16,157,229
bl _p_8

	.byte 0,16,160,225,6,0,160,225,0,32,150,229,15,224,160,225,76,241,146,229,6,0,160,225,0,16,150,229,15,224,160,225
	.byte 228,240,145,229,0,32,160,225,0,10,159,237,0,0,0,234,0,0,128,63,192,10,183,238,2,0,160,225,192,235,183,238
	.byte 0,234,141,237,0,16,157,229,0,32,146,229,15,224,160,225,84,240,146,229,6,0,160,225,0,16,150,229,15,224,160,225
	.byte 228,240,145,229,0,32,160,225,0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,2,0,160,225,192,235,183,238
	.byte 0,234,141,237,0,16,157,229,0,32,146,229,15,224,160,225,80,240,146,229,6,0,160,225,0,16,150,229,15,224,160,225
	.byte 228,240,145,229,92,0,139,229
bl _p_9

	.byte 0,16,160,225,0,16,145,229,15,224,160,225,92,240,145,229,0,16,160,225,92,32,155,229,2,0,160,225,0,32,146,229
	.byte 15,224,160,225,92,240,146,229
bl _p_96

	.byte 0,16,160,225,6,0,160,225,0,32,150,229,15,224,160,225,80,241,146,229,6,0,160,225,0,16,150,229,15,224,160,225
	.byte 228,240,145,229,0,48,160,225,0,26,159,237,0,0,0,234,205,204,76,63,193,26,183,238,0,10,159,237,0,0,0,234
	.byte 205,204,76,63,192,10,183,238,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,193,235,183,238,8,234,139,237
	.byte 192,235,183,238,9,234,139,237,8,234,155,237,206,10,183,238,192,235,183,238,10,234,139,237,10,234,155,237,206,10,183,238
	.byte 192,235,183,238,6,234,139,237,9,234,155,237,206,10,183,238,192,235,183,238,11,234,139,237,11,234,155,237,206,10,183,238
	.byte 192,235,183,238,7,234,139,237,24,0,155,229,64,0,139,229,28,0,155,229,68,0,139,229,3,0,160,225,64,16,155,229
	.byte 68,32,155,229,0,48,147,229,15,224,160,225,88,240,147,229,6,80,160,225,6,0,160,225,0,224,214,229
bl _p_198

	.byte 88,0,139,229,0,0,90,227,48,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1012
	.byte 0,0,159,231
bl _p_25

	.byte 0,16,160,225,88,0,155,229,16,160,129,229,0,32,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1008
	.byte 2,32,159,231,20,32,129,229,0,32,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1004
	.byte 2,32,159,231,28,32,129,229,0,32,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1000
	.byte 2,32,159,231,12,32,129,229
bl _p_197

	.byte 0,64,160,225,0,0,84,227,9,0,0,10,0,0,148,229,0,0,144,229,8,0,144,229,12,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 996
	.byte 1,16,159,231,1,0,80,225,7,0,0,27,5,0,160,225,4,16,160,225,0,224,213,229
bl _p_195

	.byte 6,0,160,225,96,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_196

	.byte 93,2,0,2,14,16,160,225,0,0,159,229
bl _p_196

	.byte 6,2,0,2

Lme_1a:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0
_OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,88,208,77,226,13,176,160,225,36,0,139,229,40,16,139,229,44,32,139,229
	.byte 48,48,139,229,104,224,157,229,52,224,139,229,59,0,0,234,8,0,155,229,36,0,155,229,56,0,139,229,44,0,155,229
	.byte 60,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1020
	.byte 0,0,159,231,64,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 268
	.byte 0,0,159,231,2,16,160,227
bl _p_35

	.byte 12,0,139,229,72,0,139,229,12,0,155,229,80,0,139,229,40,0,155,229,76,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1016
	.byte 0,0,159,231
bl _p_36

	.byte 0,32,160,225,76,0,155,229,80,48,155,229,8,0,130,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225
	.byte 128,240,147,229,72,0,155,229,16,0,139,229,68,0,139,229,16,48,155,229,48,32,155,229,3,0,160,225,1,16,160,227
	.byte 0,48,147,229,15,224,160,225,128,240,147,229,56,0,155,229,60,32,155,229,64,48,155,229,68,192,155,229,2,16,160,227
	.byte 0,192,141,229
bl _p_201
bl _p_200

	.byte 32,0,139,229,0,0,80,227,1,0,0,10,32,0,155,229
bl _p_63

	.byte 255,255,255,234,88,208,139,226,0,9,189,232,128,128,189,232

Lme_23:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Setup_CreateApp_0
_OffSeasonPro_Touch_Setup_CreateApp_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,72,208,77,226,13,176,160,225,52,0,139,229,0,0,160,227,4,0,139,229
	.byte 0,0,160,227,8,0,139,229,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,0,160,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 308
	.byte 10,160,159,231
bl _p_45

	.byte 64,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 312
	.byte 0,0,159,231
bl _p_25

	.byte 64,16,155,229,60,0,139,229
bl _p_46

	.byte 60,0,155,229,56,0,139,229,0,16,160,225,0,224,209,229
bl _p_47

	.byte 56,32,155,229,4,16,139,226,2,0,160,225,0,224,210,229
bl _p_48

	.byte 46,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 0,0,159,231,16,0,155,229,0,0,139,229
bl _p_52

	.byte 0,80,160,225,0,64,160,227,26,0,0,234,12,0,149,229,4,0,80,225,67,0,0,155,4,1,160,225,0,0,133,224
	.byte 16,0,128,226,0,96,144,229,10,0,160,225,0,16,155,229
bl _p_30

	.byte 0,160,160,225,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 324
	.byte 1,16,159,231
bl _p_30

	.byte 0,160,160,225,6,16,160,225
bl _p_30

	.byte 0,160,160,225,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 328
	.byte 1,16,159,231
bl _p_30

	.byte 0,160,160,225,1,64,132,226,12,0,149,229,0,0,84,225,225,255,255,186,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 308
	.byte 1,16,159,231,10,0,160,225
bl _p_30

	.byte 0,160,160,225,4,0,139,226,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 8,128,159,231
bl _p_51

	.byte 255,0,0,226,0,0,80,227,199,255,255,26,0,0,0,235,9,0,0,234,48,224,139,229,4,0,139,226,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 1,16,159,231,36,0,139,229,0,224,208,229,48,192,155,229,12,240,160,225,10,0,160,225
bl _p_49

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 320
	.byte 0,0,159,231
bl _p_25

	.byte 56,0,139,229
bl _p_50

	.byte 56,0,155,229,72,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_196

	.byte 88,2,0,2

Lme_3a:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
_OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,56,208,77,226,13,176,160,225,36,0,139,229,1,96,160,225,40,32,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 368
	.byte 0,0,159,231,0,16,144,229,6,0,160,225,40,32,155,229,0,48,150,229,15,224,160,225,40,241,147,229,0,96,160,225
	.byte 0,0,86,227,12,0,0,10,0,0,150,229,0,0,144,229,188,16,208,225,7,0,81,227,66,0,0,59,8,0,144,229
	.byte 24,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 360
	.byte 1,16,159,231,1,0,80,225,58,0,0,27,6,64,160,225,36,0,155,229,20,0,144,229,48,0,139,229,40,0,155,229
	.byte 0,16,160,225,0,16,145,229,15,224,160,225,84,240,145,229,48,16,155,229,1,160,160,225,20,0,139,229,0,224,218,229
	.byte 12,16,154,229,20,0,155,229,1,0,80,225,32,0,0,42,8,0,154,229,20,16,155,229,1,17,160,225,1,0,128,224
	.byte 16,0,128,226,0,80,144,229,16,80,139,229,36,0,155,229,6,234,144,237,206,10,183,238,192,235,183,238,6,234,139,237
	.byte 6,234,155,237,206,10,183,238,28,0,128,226,0,16,144,229,28,16,139,229,4,0,144,229,32,0,139,229,4,0,160,225
	.byte 5,16,160,225,192,235,183,238,2,234,13,237,8,32,29,229,28,48,155,229,32,192,155,229,0,192,141,229,0,224,212,229
bl _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF

	.byte 4,0,160,225,56,208,139,226,112,13,189,232,128,128,189,232,183,13,2,227
bl _p_61

	.byte 0,16,160,225,8,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_63

	.byte 0,80,160,227,213,255,255,234,14,16,160,225,0,0,159,229
bl _p_196

	.byte 93,2,0,2

Lme_49:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF
_OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,44,208,77,226,13,176,160,225,0,160,160,225,16,16,139,229,20,32,139,229
	.byte 24,48,139,229,64,224,157,229,28,224,139,229,10,0,160,225,16,16,155,229,20,32,155,229,24,48,155,229,28,192,155,229
	.byte 0,192,141,229
bl _p_204

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 568
	.byte 0,0,159,231
bl _p_6

	.byte 36,0,139,229
bl _p_203

	.byte 36,0,155,229,52,0,138,229,0,16,160,225,0,16,145,229,15,224,160,225,228,240,145,229,32,0,139,229
bl _p_161

	.byte 0,16,160,225,0,16,145,229,15,224,160,225,92,240,145,229,0,16,160,225,32,32,155,229,2,0,160,225,0,32,146,229
	.byte 15,224,160,225,96,240,146,229,52,16,154,229,1,0,160,225,0,16,145,229,15,224,160,225,228,240,145,229,0,32,160,225
	.byte 0,10,159,237,0,0,0,234,0,0,128,63,192,10,183,238,2,0,160,225,192,235,183,238,0,234,141,237,0,16,157,229
	.byte 0,32,146,229,15,224,160,225,100,240,146,229,52,16,154,229,1,0,160,225,0,16,145,229,15,224,160,225,228,240,145,229
	.byte 0,32,160,225,0,10,159,237,0,0,0,234,0,0,64,64,192,10,183,238,2,0,160,225,192,235,183,238,0,234,141,237
	.byte 0,16,157,229,0,32,146,229,15,224,160,225,104,240,146,229,52,16,154,229,1,0,160,225,0,16,145,229,15,224,160,225
	.byte 228,240,145,229,0,32,160,225,1,16,160,227,0,32,146,229,15,224,160,225,108,240,146,229,52,32,154,229,2,0,160,225
	.byte 0,16,160,227,0,32,146,229,15,224,160,225,160,240,146,229,10,0,160,225,0,16,154,229,15,224,160,225,8,241,145,229
	.byte 0,32,160,225,52,16,154,229,2,0,160,225,0,32,146,229,15,224,160,225,148,240,146,229,44,208,139,226,0,13,189,232
	.byte 128,128,189,232

Lme_4f:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF
_OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,84,208,77,226,13,176,160,225,56,0,139,229,1,160,160,225,60,32,139,229
	.byte 64,48,139,229,104,224,157,229,68,224,139,229,56,0,155,229,52,32,144,229,0,224,218,229,8,16,154,229,2,0,160,225
	.byte 0,32,146,229,15,224,160,225,8,241,146,229,56,0,155,229,52,0,144,229,76,0,139,229,0,58,159,237,0,0,0,234
	.byte 0,0,0,0,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,0,194,42,183,238,16,234,155,237,206,10,183,238
	.byte 192,235,183,238,4,234,139,237,4,234,155,237,206,26,183,238,17,234,155,237,206,10,183,238,192,235,183,238,5,234,139,237
	.byte 5,234,155,237,206,10,183,238,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,160,227,32,0,139,229
	.byte 0,0,160,227,36,0,139,229,24,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229,194,235,183,238,2,234,13,237
	.byte 8,32,29,229,193,235,183,238,2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_205

	.byte 76,192,155,229,24,0,155,229,40,0,139,229,28,0,155,229,44,0,139,229,32,0,155,229,48,0,139,229,36,0,155,229
	.byte 52,0,139,229,12,0,160,225,72,0,139,229,40,16,155,229,44,32,155,229,48,48,155,229,52,0,155,229,0,0,141,229
	.byte 72,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,84,208,139,226,0,13,189,232,128,128,189,232

Lme_52:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad
_OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,124,223,77,226,13,176,160,225,0,160,160,225,0,0,160,227,16,0,139,229
	.byte 0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,160,227,32,0,139,229
	.byte 0,0,160,227,36,0,139,229,0,0,160,227,40,0,139,229,0,0,160,227,44,0,139,229,0,0,160,227,52,0,139,229
	.byte 0,0,160,227,56,0,139,229,0,0,160,227,60,0,139,229,0,0,160,227,64,0,139,229,0,0,160,227,76,0,139,229
	.byte 0,0,160,227,80,0,139,229,0,0,160,227,84,0,139,229,0,0,160,227,88,0,139,229,0,0,160,227,92,0,139,229
	.byte 0,0,160,227,96,0,139,229,0,0,160,227,100,0,139,229,0,0,160,227,104,0,139,229,0,0,160,227,108,0,139,229
	.byte 0,0,160,227,112,0,139,229,0,0,160,227,116,0,139,229,0,0,160,227,120,0,139,229,0,0,160,227,124,0,139,229
	.byte 0,0,160,227,128,0,139,229,0,0,160,227,132,0,139,229,0,0,160,227,136,0,139,229,10,0,160,225
bl _p_143

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 476
	.byte 0,0,159,231,116,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 480
	.byte 0,0,159,231
bl _p_25

	.byte 116,17,155,229,112,1,139,229
bl _p_94

	.byte 112,17,155,229,10,0,160,225,0,32,154,229,15,224,160,225,60,240,146,229,255,0,0,226,0,0,80,227,4,0,0,10
	.byte 10,0,160,225,0,16,160,227,0,32,154,229,15,224,160,225,148,240,146,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1104
	.byte 0,0,159,231
bl _p_6

	.byte 144,1,139,229
bl _p_216

	.byte 144,1,155,229,0,96,160,225,0,10,159,237,0,0,0,234,0,0,48,65,192,10,183,238,192,235,183,238,39,234,139,237
	.byte 0,224,208,229,39,234,155,237,206,10,183,238,192,235,183,238,6,234,134,237,0,26,159,237,0,0,0,234,0,0,42,67
	.byte 193,26,183,238,0,10,159,237,0,0,0,234,0,0,72,67,192,10,183,238,0,0,160,227,160,0,139,229,0,0,160,227
	.byte 164,0,139,229,193,235,183,238,42,234,139,237,192,235,183,238,43,234,139,237,42,234,155,237,206,10,183,238,192,235,183,238
	.byte 44,234,139,237,44,234,155,237,206,10,183,238,192,235,183,238,40,234,139,237,43,234,155,237,206,10,183,238,192,235,183,238
	.byte 45,234,139,237,45,234,155,237,206,10,183,238,192,235,183,238,41,234,139,237,160,0,155,229,56,1,139,229,164,0,155,229
	.byte 60,1,139,229,0,224,214,229,28,0,134,226,56,17,155,229,0,16,128,229,60,17,155,229,4,16,128,229,80,96,138,229
	.byte 0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,98,11,139,237,0,10,159,237,0,0,0,234,0,0,0,0
	.byte 192,10,183,238,96,11,139,237,10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225,16,16,139,226
	.byte 2,0,160,225,0,32,146,229,15,224,160,225,252,240,146,229,6,234,155,237,206,10,183,238,192,235,183,238,46,234,139,237
	.byte 46,234,155,237,206,10,183,238,94,11,139,237,10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225
	.byte 32,16,139,226,2,0,160,225,0,32,146,229,15,224,160,225,252,240,146,229,94,27,155,237,96,43,155,237,98,59,155,237
	.byte 11,234,155,237,206,10,183,238,192,235,183,238,47,234,139,237,47,234,155,237,206,10,183,238,0,0,160,227,192,0,139,229
	.byte 0,0,160,227,196,0,139,229,0,0,160,227,200,0,139,229,0,0,160,227,204,0,139,229,192,0,139,226,195,235,183,238
	.byte 2,234,13,237,8,16,29,229,194,235,183,238,2,234,13,237,8,32,29,229,193,235,183,238,2,234,13,237,8,48,29,229
	.byte 192,235,183,238,0,234,141,237
bl _p_205

	.byte 192,0,155,229,64,1,139,229,196,0,155,229,68,1,139,229,200,0,155,229,72,1,139,229,204,0,155,229,76,1,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1100
	.byte 0,0,159,231
bl _p_6

	.byte 116,1,139,229
bl _p_215

	.byte 116,1,155,229,0,80,160,225,5,48,160,225,0,26,159,237,0,0,0,234,0,0,42,67,193,26,183,238,0,10,159,237
	.byte 0,0,0,234,0,0,72,67,192,10,183,238,0,0,160,227,208,0,139,229,0,0,160,227,212,0,139,229,193,235,183,238
	.byte 54,234,139,237,192,235,183,238,55,234,139,237,54,234,155,237,206,10,183,238,192,235,183,238,56,234,139,237,56,234,155,237
	.byte 206,10,183,238,192,235,183,238,52,234,139,237,55,234,155,237,206,10,183,238,192,235,183,238,57,234,139,237,57,234,155,237
	.byte 206,10,183,238,192,235,183,238,53,234,139,237,208,0,155,229,80,1,139,229,212,0,155,229,84,1,139,229,3,0,160,225
	.byte 80,17,155,229,84,33,155,229,0,48,147,229,15,224,160,225,80,240,147,229,5,0,160,225,0,16,160,227,0,32,149,229
	.byte 15,224,160,225,76,240,146,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1096
	.byte 0,0,159,231
bl _p_6

	.byte 112,1,139,229,64,17,155,229,68,33,155,229,72,49,155,229,76,193,155,229,0,192,141,229,4,80,141,229
bl _p_214

	.byte 112,1,155,229,84,0,138,229,0,48,160,225,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1092
	.byte 1,16,159,231,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 368
	.byte 0,0,159,231,0,32,144,229,3,0,160,225,0,224,211,229
bl _p_213

	.byte 84,32,154,229,2,0,160,225,0,16,160,227,0,32,146,229,15,224,160,225,8,241,146,229,84,32,154,229,80,16,154,229
	.byte 2,0,160,225,0,224,210,229
bl _p_212

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 312
	.byte 0,0,159,231
bl _p_25

	.byte 0,32,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1088
	.byte 0,0,159,231,0,0,144,229,8,0,130,229,48,32,139,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 4
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_211

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1084
	.byte 1,16,159,231,48,0,155,229,0,32,160,225,0,224,210,229
bl _p_211

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1080
	.byte 1,16,159,231,48,0,155,229,0,32,160,225,0,224,210,229
bl _p_211

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 12
	.byte 1,16,159,231,48,0,155,229,0,32,160,225,0,224,210,229
bl _p_211

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1076
	.byte 1,16,159,231,48,0,155,229,0,32,160,225,0,224,210,229
bl _p_211

	.byte 48,64,155,229,52,16,139,226,48,0,155,229,0,32,160,225,0,224,210,229
bl _p_48

	.byte 115,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1072
	.byte 0,0,159,231
bl _p_25

	.byte 68,0,139,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 1,16,159,231,64,16,155,229,8,16,128,229,12,160,128,229,80,0,154,229,0,16,160,225,0,224,209,229,20,0,144,229
	.byte 112,1,139,229
bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen

	.byte 112,17,155,229,255,0,0,226,1,64,160,225,0,0,80,227,46,0,0,10,68,0,155,229,8,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 988
	.byte 1,16,159,231
bl _p_30
bl _p_100

	.byte 116,1,139,229,68,0,155,229,0,0,80,227,241,1,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 660
	.byte 0,0,159,231
bl _p_25

	.byte 68,16,155,229,16,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1068
	.byte 1,16,159,231,20,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1064
	.byte 1,16,159,231,28,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 672
	.byte 1,16,159,231,12,16,128,229,148,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1052
	.byte 0,0,159,231
bl _p_25

	.byte 116,17,155,229,148,33,155,229,112,1,139,229
bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction

	.byte 112,1,155,229,0,80,160,225,40,0,0,234,68,0,155,229,8,0,144,229
bl _p_100

	.byte 116,1,139,229,68,0,155,229,0,0,80,227,199,1,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 660
	.byte 0,0,159,231
bl _p_25

	.byte 68,16,155,229,16,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1060
	.byte 1,16,159,231,20,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1056
	.byte 1,16,159,231,28,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 672
	.byte 1,16,159,231,12,16,128,229,148,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1052
	.byte 0,0,159,231
bl _p_25

	.byte 116,17,155,229,148,33,155,229,112,1,139,229
bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction

	.byte 112,1,155,229,0,80,160,225,4,0,160,225,5,16,160,225,0,224,212,229
bl _p_209

	.byte 52,0,139,226,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 8,128,159,231
bl _p_51

	.byte 255,0,0,226,0,0,80,227,130,255,255,26,0,0,0,235,11,0,0,234,8,208,77,226,52,225,139,229,52,0,139,226
	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 316
	.byte 1,16,159,231,248,0,139,229,0,224,208,229,8,208,141,226,52,193,155,229,12,240,160,225,10,0,160,225,0,16,154,229
	.byte 15,224,160,225,180,240,145,229,0,32,160,225,84,16,154,229,2,0,160,225,0,224,210,229
bl _p_207

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1048
	.byte 0,0,159,231
bl _p_6

	.byte 232,1,139,229,3,16,160,227
bl _p_208

	.byte 232,1,155,229,72,0,139,229,192,1,139,229,10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225
	.byte 76,16,139,226,2,0,160,225,0,32,146,229,15,224,160,225,252,240,146,229,21,234,155,237,206,10,183,238,192,235,183,238
	.byte 63,234,139,237,63,234,155,237,206,10,183,238,120,11,139,237,92,16,139,226,72,0,155,229,0,32,160,225,0,32,146,229
	.byte 15,224,160,225,252,240,146,229,120,11,155,237,25,234,155,237,206,26,183,238,193,235,183,238,64,234,139,237,64,234,155,237
	.byte 206,26,183,238,65,11,48,238,0,26,159,237,0,0,0,234,0,0,160,64,193,26,183,238,65,11,48,238,118,11,139,237
	.byte 0,10,159,237,0,0,0,234,0,0,160,64,192,10,183,238,116,11,139,237,108,16,139,226,72,0,155,229,0,32,160,225
	.byte 0,32,146,229,15,224,160,225,252,240,146,229,29,234,155,237,206,10,183,238,192,235,183,238,65,234,139,237,65,234,155,237
	.byte 206,10,183,238,114,11,139,237,124,16,139,226,72,0,155,229,0,32,160,225,0,32,146,229,15,224,160,225,252,240,146,229
	.byte 114,27,155,237,116,43,155,237,118,59,155,237,34,234,155,237,206,10,183,238,192,235,183,238,66,234,139,237,66,234,155,237
	.byte 206,10,183,238,0,0,160,227,12,1,139,229,0,0,160,227,16,1,139,229,0,0,160,227,20,1,139,229,0,0,160,227
	.byte 24,1,139,229,67,15,139,226,195,235,183,238,0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229
	.byte 193,235,183,238,0,234,141,237,0,48,157,229,192,235,183,238,0,234,141,237
bl _p_205

	.byte 192,193,155,229,12,1,155,229,88,1,139,229,16,1,155,229,92,1,139,229,20,1,155,229,96,1,139,229,24,1,155,229
	.byte 100,1,139,229,12,0,160,225,188,1,139,229,88,17,155,229,92,33,155,229,96,49,155,229,100,1,155,229,0,0,141,229
	.byte 188,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229
bl _p_96

	.byte 0,16,160,225,72,0,155,229,0,32,160,225,0,32,146,229,15,224,160,225,152,240,146,229,0,0,90,227,254,0,0,11
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 132
	.byte 0,0,159,231
bl _p_25

	.byte 0,16,160,225,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1044
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1040
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 144
	.byte 0,0,159,231,12,0,129,229,72,0,155,229,0,32,160,225,0,224,210,229
bl _p_28

	.byte 10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225,72,16,155,229,0,224,210,229
bl _p_207

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 492
	.byte 0,0,159,231
bl _p_25

	.byte 184,1,139,229,140,0,139,229
bl _p_96

	.byte 0,16,160,225,184,1,155,229,12,16,128,229
bl _p_9

	.byte 0,16,160,225,140,0,155,229,16,16,128,229,0,26,159,237,0,0,0,234,205,204,76,63,193,26,183,238,0,10,159,237
	.byte 0,0,0,234,205,204,76,63,192,10,183,238,0,16,160,227,28,17,139,229,0,16,160,227,32,17,139,229,193,235,183,238
	.byte 73,234,139,237,192,235,183,238,74,234,139,237,73,234,155,237,206,10,183,238,192,235,183,238,71,234,139,237,74,234,155,237
	.byte 206,10,183,238,192,235,183,238,72,234,139,237,28,17,155,229,104,17,139,229,32,17,155,229,108,17,139,229,20,0,128,226
	.byte 104,17,155,229,0,16,128,229,108,17,155,229,4,16,128,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 404
	.byte 0,0,159,231,176,1,139,229,0,0,90,227,165,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 132
	.byte 0,0,159,231
bl _p_25

	.byte 16,160,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1036
	.byte 1,16,159,231,20,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1032
	.byte 1,16,159,231,28,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 144
	.byte 1,16,159,231,12,16,128,229,180,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 508
	.byte 0,0,159,231
bl _p_6

	.byte 176,17,155,229,180,49,155,229,172,1,139,229,0,32,160,227
bl _p_97

	.byte 172,1,155,229,144,0,139,229,0,48,160,225,140,16,155,229,0,32,160,227,0,224,211,229
bl _p_98

	.byte 0,0,90,227,124,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 132
	.byte 0,0,159,231
bl _p_25

	.byte 16,160,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1028
	.byte 1,16,159,231,20,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1024
	.byte 1,16,159,231,28,16,128,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 144
	.byte 1,16,159,231,12,16,128,229,168,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 508
	.byte 0,0,159,231
bl _p_6

	.byte 168,33,155,229,164,1,139,229,15,16,160,227
bl _p_206

	.byte 164,1,155,229,148,0,139,229,160,1,139,229
bl _p_96

	.byte 0,16,160,225,160,33,155,229,2,0,160,225,0,32,146,229,15,224,160,225,88,240,146,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 520
	.byte 0,0,159,231,3,16,160,227
bl _p_35

	.byte 0,48,160,225,144,1,139,229,3,0,160,225,0,16,160,227,148,32,155,229,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 144,1,155,229,152,1,139,229,148,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 508
	.byte 0,0,159,231
bl _p_6

	.byte 156,1,139,229,5,16,160,227
bl _p_99

	.byte 156,1,155,229,152,0,139,229,0,32,160,225,0,10,159,237,0,0,0,234,0,0,72,66,192,10,183,238,2,0,160,225
	.byte 192,235,183,238,0,234,141,237,0,16,157,229,0,32,146,229,15,224,160,225,108,240,146,229,152,49,155,229,3,0,160,225
	.byte 1,16,160,227,152,32,155,229,0,48,147,229,15,224,160,225,128,240,147,229,148,49,155,229,3,0,160,225,116,1,139,229
	.byte 3,0,160,225,2,16,160,227,144,32,155,229,0,48,147,229,15,224,160,225,128,240,147,229,116,17,155,229,10,0,160,225
	.byte 0,32,160,227,0,48,154,229,15,224,160,225,108,240,147,229,84,32,154,229,2,0,160,225,0,16,160,227,0,32,146,229
	.byte 15,224,160,225,48,241,146,229,84,0,154,229,112,1,139,229
bl _p_130

	.byte 0,16,160,225,112,33,155,229,2,0,160,225,0,32,146,229,15,224,160,225,0,241,146,229,84,16,154,229,1,0,160,225
	.byte 0,16,145,229,15,224,160,225,36,241,145,229,124,223,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_196

	.byte 6,2,0,2

Lme_57:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView
_OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView:

	.byte 128,64,45,233,13,112,160,225,32,13,45,233,104,208,77,226,13,176,160,225,0,160,160,225,148,0,154,229,0,0,80,227
	.byte 183,0,0,26,0,58,159,237,0,0,0,234,0,0,0,0,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,0
	.byte 194,42,183,238,0,26,159,237,0,0,0,234,0,0,160,67,193,26,183,238,0,10,159,237,0,0,0,234,0,0,216,65
	.byte 192,10,183,238,0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,160,227
	.byte 32,0,139,229,20,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229,194,235,183,238,2,234,13,237,8,32,29,229
	.byte 193,235,183,238,2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_205

	.byte 20,0,155,229,52,0,139,229,24,0,155,229,56,0,139,229,28,0,155,229,60,0,139,229,32,0,155,229,64,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1120
	.byte 0,0,159,231
bl _p_6

	.byte 100,0,139,229,52,16,155,229,56,32,155,229,60,48,155,229,64,192,155,229,0,192,141,229
bl _p_220

	.byte 100,0,155,229,148,0,138,229,96,0,139,229
bl _p_9

	.byte 0,16,160,225,96,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,0,241,146,229,0,58,159,237,0,0,0,234
	.byte 0,0,127,67,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,64,194,42,183,238,0,26,159,237,0,0,0,234
	.byte 0,0,104,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,184,65,192,10,183,238,0,0,160,227,36,0,139,229
	.byte 0,0,160,227,40,0,139,229,0,0,160,227,44,0,139,229,0,0,160,227,48,0,139,229,36,0,139,226,195,235,183,238
	.byte 0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238,0,234,141,237,0,48,157,229
	.byte 192,235,183,238,0,234,141,237
bl _p_205

	.byte 36,0,155,229,68,0,139,229,40,0,155,229,72,0,139,229,44,0,155,229,76,0,139,229,48,0,155,229,80,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1048
	.byte 0,0,159,231
bl _p_6

	.byte 92,0,139,229,68,16,155,229,72,32,155,229,76,48,155,229,80,192,155,229,0,192,141,229
bl _p_219

	.byte 92,0,155,229,0,80,160,225,88,0,139,229
bl _p_218

	.byte 0,16,160,225,88,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,0,241,146,229,16,80,139,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1116
	.byte 1,16,159,231,5,0,160,225,0,32,160,227,0,48,149,229,15,224,160,225,52,241,147,229
bl _p_9

	.byte 0,16,160,225,5,0,160,225,0,32,160,227,0,48,149,229,15,224,160,225,48,241,147,229,0,0,90,227,34,0,0,11
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 132
	.byte 0,0,159,231
bl _p_25

	.byte 0,16,160,225,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1112
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1108
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 144
	.byte 0,0,159,231,12,0,129,229,5,0,160,225,0,224,213,229
bl _p_217

	.byte 148,32,154,229,2,0,160,225,5,16,160,225,0,32,146,229,15,224,160,225,148,240,146,229,148,0,154,229,104,208,139,226
	.byte 32,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_196

	.byte 6,2,0,2

Lme_6e:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_Views_WorkoutView_LoadIAd
_OffSeasonPro_Touch_Views_WorkoutView_LoadIAd:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,104,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,16,0,139,229
	.byte 0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1148
	.byte 0,0,159,231
bl _p_6

	.byte 92,0,139,229
bl _p_223

	.byte 92,0,155,229,92,0,138,229,0,96,160,225,88,0,139,229,0,0,90,227,154,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 132
	.byte 0,0,159,231
bl _p_25

	.byte 0,16,160,225,88,32,155,229,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1144
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1140
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 144
	.byte 0,0,159,231,12,0,129,229,2,0,160,225,0,224,210,229
bl _p_222

	.byte 0,0,90,227,126,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1136
	.byte 0,0,159,231
bl _p_25

	.byte 0,16,160,225,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1132
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1128
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1124
	.byte 0,0,159,231,12,0,129,229,6,0,160,225,0,224,214,229
bl _p_221

	.byte 0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,20,11,139,237
bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen

	.byte 20,11,155,237,255,0,0,226,6,80,160,225,8,11,139,237,0,0,80,227,3,0,0,10,8,11,155,237,8,11,139,237
	.byte 6,66,0,227,2,0,0,234,8,11,155,237,8,11,139,237,174,65,0,227,8,11,155,237,24,11,139,237,16,74,14,238
	.byte 206,234,184,238,206,10,183,238,20,11,139,237,10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225
	.byte 16,16,139,226,2,0,160,225,0,32,146,229,15,224,160,225,252,240,146,229,20,43,155,237,24,59,155,237,6,234,155,237
	.byte 206,10,183,238,192,235,183,238,10,234,139,237,10,234,155,237,206,26,183,238,0,10,159,237,0,0,0,234,0,0,72,66
	.byte 192,10,183,238,0,0,160,227,44,0,139,229,0,0,160,227,48,0,139,229,0,0,160,227,52,0,139,229,0,0,160,227
	.byte 56,0,139,229,44,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229,194,235,183,238,2,234,13,237,8,32,29,229
	.byte 193,235,183,238,2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_205

	.byte 44,0,155,229,60,0,139,229,48,0,155,229,64,0,139,229,52,0,155,229,68,0,139,229,56,0,155,229,72,0,139,229
	.byte 5,0,160,225,60,16,155,229,64,32,155,229,68,48,155,229,72,192,155,229,0,192,141,229,0,192,149,229,15,224,160,225
	.byte 220,240,156,229,10,0,160,225,0,16,154,229,15,224,160,225,180,240,145,229,0,32,160,225,6,16,160,225,0,32,146,229
	.byte 15,224,160,225,148,240,146,229,6,0,160,225,1,16,160,227,0,32,150,229,15,224,160,225,164,240,146,229,104,208,139,226
	.byte 112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_196

	.byte 6,2,0,2

Lme_8d:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary
_OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,92,208,77,226,13,176,160,225,0,160,160,225,32,16,139,229,36,32,139,229
bl _p_2

	.byte 0,32,160,225,16,16,139,226,2,0,160,225,0,32,146,229,15,224,160,225,84,240,146,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1168
	.byte 0,0,159,231
bl _p_6

	.byte 80,0,139,229,16,16,155,229,20,32,155,229,24,48,155,229,28,192,155,229,0,192,141,229
bl _p_228

	.byte 80,0,155,229,24,0,138,229,76,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1164
	.byte 0,0,159,231
bl _p_25

	.byte 76,32,155,229,72,0,139,229,10,16,160,225
bl _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow

	.byte 72,0,155,229,68,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1160
	.byte 0,0,159,231
bl _p_25

	.byte 68,32,155,229,64,0,139,229,10,16,160,225
bl _OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter

	.byte 64,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,188,240,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1156
	.byte 8,128,159,231
bl _p_225

	.byte 0,32,160,225,0,16,160,227,0,32,146,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 1152
	.byte 8,128,159,231,4,224,143,226,8,240,18,229,0,0,0,0,24,0,154,229,60,0,139,229
bl _p_101
bl _p_100
bl _p_102

	.byte 0,16,160,225,60,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,0,241,146,229,24,16,154,229,1,0,160,225
	.byte 0,16,145,229,15,224,160,225,8,241,145,229
bl _p_224

	.byte 56,0,139,229,125,0,160,227,142,16,160,227,142,32,160,227
bl _p_10

	.byte 0,16,160,225,56,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,76,240,146,229
bl _p_224

	.byte 48,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 492
	.byte 0,0,159,231
bl _p_25

	.byte 52,0,139,229,44,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProTouch_got - . + 24
	.byte 0,0,159,231,0,0,144,229,0,10,159,237,0,0,0,234,0,0,200,65,192,10,183,238,192,235,183,238,0,234,141,237
	.byte 0,16,157,229
bl _p_8

	.byte 0,16,160,225,52,0,155,229,8,16,128,229,40,0,139,229,240,0,160,227,240,16,160,227,240,32,160,227
bl _p_10

	.byte 0,48,160,225,40,0,155,229,44,16,155,229,48,32,155,229,12,48,128,229,2,0,160,225,0,32,146,229,15,224,160,225
	.byte 80,240,146,229,1,0,160,227,92,208,139,226,0,13,189,232,128,128,189,232

Lme_ab:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_EmailPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_FilePluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonLocalisationPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MessengerPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MusicPlayerPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_ResourceLoaderPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_TextToSpeechPluginBootstrap__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_Background
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_ConcreteBackground
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Constants__cctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_Alignment
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_Alignment_MonoTouch_UIKit_UITextAlignment
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_MaxLength
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_MaxLength_int
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_CellKey
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_GetCellImpl_MonoTouch_UIKit_UITableView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__cctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__CreateTextFieldm__0_MonoTouch_UIKit_UITextField_MonoTouch_Foundation_NSRange_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement__ctor_string_string_MonoTouch_UIKit_UIImage
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement_GetCellImpl_MonoTouch_UIKit_UITableView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer_ShowError_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ErrorDisplayerm__1_object_OffSeasonPro_Core_Models_ErrorEventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIBarButtonItem
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextField
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UILabel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIImageView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIDatePicker
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISlider
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISwitch
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Collections_Specialized_INotifyCollectionChanged
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Windows_Input_ICommand
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includem__9_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Application__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Application_Main_string__
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_RequestClose_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Setup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Setup_InitializeLastChance
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_Rows
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_Rows_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_FontSize
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_FontSize_single
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_ImageViewSize
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_ImageViewSize_System_Drawing_SizeF
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ShouldHighlightItem_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemHighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemUnhighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Image
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Image_MonoTouch_UIKit_UIImage
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Tapped
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Tapped_MonoTouch_Foundation_NSAction
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_get_ImageView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_set_ImageView_MonoTouch_UIKit_UIImageView
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell__cctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_set_ViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PrefersStatusBarHidden
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidAppear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewWillDisappear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__D_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__E_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__F_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_FinishedPickingMedia_MonoTouch_UIKit_UIImagePickerController_MonoTouch_Foundation_NSDictionary
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_Canceled_MonoTouch_UIKit_UIImagePickerController
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PrefersStatusBarHidden
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PreferredStatusBarStyle
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_ViewDidLoad
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_CrossUI_Touch_Dialog_Elements_RootElement_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_get_ViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_set_ViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidLoad
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_PrefersStatusBarHidden
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidAppear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewWillDisappear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ResetDisplay
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__11_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__12_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__get_InputAccessoryViewm__13_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__14
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__15
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_Val
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_Val_int
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ResizeLabel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ResizeLabel_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_lblCurrentPosition
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_lblCurrentPosition_MonoTouch_UIKit_UILabel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnPauseResume
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnPauseResume_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnReset
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnReset_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnBack
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnBack_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidDisappear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidLoad
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_PrefersStatusBarHidden
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidAppear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_OnMessageReceived_OffSeasonPro_Core_Messages_DisplayMessage
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ReleaseDesignerOutlets
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__16_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__17_object_MonoTouch_iAd_AdErrorEventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__18_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__19_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__1A_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_ViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_ViewModel_OffSeasonPro_Core_ViewModels_HomeViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnAction
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnAction_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnPlaylists
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnPlaylists_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnSettings
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnSettings_MonoTouch_UIKit_UIButton
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewWillAppear_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewDidLoad
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_PrefersStatusBarHidden
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ReleaseDesignerOutlets
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1B_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1C_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_get_Visible
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_set_Visible_bool
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_OnResignActivation_MonoTouch_UIKit_UIApplication
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__m__2_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__m__3_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__m__4_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__m__5_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__m__6_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__m__7_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__m__8_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__m__A_object_System_EventArgs
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__B
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__C
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStorey9__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__ctor
.no_dead_strip _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__m__10_object_MonoTouch_UIKit_UIButtonEventArgs
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_invoke_TResult__this__
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UIButtonEventArgs
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_Action_1_OffSeasonPro_Core_Messages_DisplayMessage_invoke_void__this___T_OffSeasonPro_Core_Messages_DisplayMessage
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_iAd_AdErrorEventArgs
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_invoke_void__this___object_TEventArgs_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
.no_dead_strip _OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_EmailPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_FilePluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonLocalisationPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_JsonPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MessengerPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_MusicPlayerPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_ResourceLoaderPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_TextToSpeechPluginBootstrap__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_Background
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_ConcreteBackground
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Constants__cctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_Alignment
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_Alignment_MonoTouch_UIKit_UITextAlignment
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_MaxLength
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_set_MaxLength_int
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_get_CellKey
	bl _OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement_GetCellImpl_MonoTouch_UIKit_UITableView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__cctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__CreateTextFieldm__0_MonoTouch_UIKit_UITextField_MonoTouch_Foundation_NSRange_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement__ctor_string_string_MonoTouch_UIKit_UIImage
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyRadioElement_GetCellImpl_MonoTouch_UIKit_UITableView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	bl _OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0
	bl _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer_ShowError_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_ErrorDisplayer__ErrorDisplayerm__1_object_OffSeasonPro_Core_Models_ErrorEventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIBarButtonItem
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextField
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UITextView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UILabel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIImageView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UIDatePicker
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISlider
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_MonoTouch_UIKit_UISwitch
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Collections_Specialized_INotifyCollectionChanged
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude_Include_System_Windows_Input_ICommand
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includem__9_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Application__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Application_Main_string__
	bl _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter_RequestClose_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter
	bl _OffSeasonPro_Touch_Setup_CreateApp_0
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Setup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Setup_InitializeLastChance
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_Rows
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_Rows_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_FontSize
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_FontSize_single
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_get_ImageViewSize
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_set_ImageViewSize_System_Drawing_SizeF
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ShouldHighlightItem_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemHighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundSource_ItemUnhighlighted_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Image
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Image_MonoTouch_UIKit_UIImage
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_get_Tapped
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement_set_Tapped_MonoTouch_Foundation_NSAction
	bl _OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_get_ImageView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell_set_ImageView_MonoTouch_UIKit_UIImageView
	bl _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageCell__cctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_set_ViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	bl _OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PrefersStatusBarHidden
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidAppear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ViewWillDisappear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__D_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__E_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadm__F_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_FinishedPickingMedia_MonoTouch_UIKit_UIImagePickerController_MonoTouch_Foundation_NSDictionary
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate_Canceled_MonoTouch_UIKit_UIImagePickerController
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PrefersStatusBarHidden
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_PreferredStatusBarStyle
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView_ViewDidLoad
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_CrossUI_Touch_Dialog_Elements_RootElement_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_get_ViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_set_ViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel
	bl _OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidLoad
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_PrefersStatusBarHidden
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewDidAppear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ViewWillDisappear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView_ResetDisplay
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__11_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ViewDidLoadm__12_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__get_InputAccessoryViewm__13_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__14
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_SettingsView__ResetDisplaym__15
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_Val
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_Val_int
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_ResizeLabel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_ResizeLabel_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_lblCurrentPosition
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_lblCurrentPosition_MonoTouch_UIKit_UILabel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnPauseResume
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnPauseResume_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnReset
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnReset_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_get_btnBack
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_set_btnBack_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidDisappear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidLoad
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_PrefersStatusBarHidden
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ViewDidAppear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_OnMessageReceived_OffSeasonPro_Core_Messages_DisplayMessage
	bl _OffSeasonPro_Touch_Views_WorkoutView_LoadIAd
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView_ReleaseDesignerOutlets
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__16_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadIAdm__17_object_MonoTouch_iAd_AdErrorEventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__18_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__19_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_WorkoutView__LoadAdModm__1A_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_ViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_ViewModel_OffSeasonPro_Core_ViewModels_HomeViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnAction
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnAction_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnPlaylists
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnPlaylists_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_get_btnSettings
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_set_btnSettings_MonoTouch_UIKit_UIButton
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewWillAppear_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ViewDidLoad
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_PrefersStatusBarHidden
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ReleaseDesignerOutlets
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1B_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView__ViewDidLoadm__1C_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate_Clicked_MonoTouch_UIKit_UIAlertView_int
	bl _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_get_Visible
	bl _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress_set_Visible_bool
	bl _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate__ctor
	bl _OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary
	bl _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	bl _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_OnResignActivation_MonoTouch_UIKit_UIApplication
	bl _OffSeasonProTouch__OffSeasonPro_Touch_AppDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey0__m__2_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey1__m__3_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey2__m__4_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey3__m__5_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey4__m__6_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey5__m__7_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey6__m__8_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_LinkerPleaseInclude__Includec__AnonStorey7__m__A_object_System_EventArgs
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__B
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ViewDidLoadc__AnonStorey8__m__C
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStorey9__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__ctor
	bl _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView__ShowPhotoActionSheetc__AnonStoreyA__m__10_object_MonoTouch_UIKit_UIButtonEventArgs
	bl method_addresses
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_invoke_TResult__this__
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UIButtonEventArgs
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_Action_1_OffSeasonPro_Core_Messages_DisplayMessage_invoke_void__this___T_OffSeasonPro_Core_Messages_DisplayMessage
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_iAd_AdErrorEventArgs
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_invoke_void__this___object_TEventArgs_object_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
	bl _OffSeasonProTouch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 205,10,21,2
	.short 0, 10, 20, 30, 40, 51, 62, 73
	.short 84, 95, 106, 117, 128, 139, 151, 162
	.short 173, 184, 195, 206, 222
	.byte 1,2,2,2,2,2,2,2,2,2,24,5,3,13,2,2,4,2,4,3,65,3,3,3,3,3,4,14,5,6,114,2
	.byte 6,2,2,2,7,8,5,2,128,152,7,7,8,8,3,3,7,7,7,128,219,8,7,2,3,2,2,13,2,11,129,17
	.byte 5,4,2,2,2,2,2,2,2,129,42,2,3,3,4,2,2,2,2,2,129,69,3,3,3,6,2,3,2,81,16,129
	.byte 190,2,2,3,9,3,2,2,3,2,129,224,2,2,2,2,2,2,2,2,3,129,245,14,59,2,2,2,96,3,3,2
	.byte 130,175,3,4,4,2,2,2,2,4,2,130,202,2,2,2,2,2,2,3,128,238,2,131,204,4,18,32,2,2,2,2
	.byte 2,2,132,18,4,2,2,2,2,2,2,2,2,132,72,8,2,2,15,2,2,4,2,2,132,120,2,14,2,11,2,2
	.byte 2,2,3,132,162,3,2,3,2,4,2,2,2,2,132,186,5,2,4,2,2,2,255,255,255,251,53,132,205,4,132,213
	.byte 4,4,4,4
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,1324,203,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,1266,198,11,1288,200
	.long 0,0,0,0,1276,199,0,1312
	.long 202,0,1300,201,12,1346,204,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 7,198,1266,199,1276,200,1288,201
	.long 1300,202,1312,203,1324,204,1346
.section __TEXT, __const
	.align 3
class_name_table:

	.short 73, 0, 0, 0, 0, 5, 0, 7
	.short 0, 29, 76, 26, 0, 0, 0, 1
	.short 0, 0, 0, 20, 74, 0, 0, 28
	.short 0, 45, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 12, 0, 0, 0, 0
	.short 0, 34, 0, 0, 0, 14, 0, 0
	.short 0, 0, 0, 0, 0, 11, 0, 4
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 27, 0, 0, 0, 25
	.short 0, 0, 0, 9, 0, 13, 0, 2
	.short 0, 0, 0, 0, 0, 0, 0, 24
	.short 77, 22, 0, 43, 0, 35, 0, 10
	.short 78, 37, 0, 3, 75, 39, 0, 15
	.short 73, 18, 81, 42, 0, 30, 0, 0
	.short 0, 8, 0, 0, 0, 6, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 17, 0, 0, 0, 0, 0, 0
	.short 0, 32, 0, 19, 0, 0, 0, 0
	.short 0, 0, 0, 16, 80, 21, 0, 23
	.short 79, 31, 82, 33, 0, 36, 0, 38
	.short 0, 40, 0, 41, 0, 44, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 296,10,30,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 165
	.short 176, 187, 198, 209, 220, 231, 242, 253
	.short 264, 275, 286, 297, 308, 319
	.byte 133,76,2,1,1,1,3,3,4,4,4,133,103,4,4,4,4,4,4,4,4,5,133,144,4,4,4,7,4,5,12,6
	.byte 2,133,194,6,5,4,4,5,3,5,3,3,133,237,3,3,3,3,3,3,3,3,3,134,13,3,3,3,3,3,3,3
	.byte 3,3,134,44,5,4,2,2,4,3,3,3,5,134,79,7,4,5,4,4,5,4,4,4,134,124,4,6,6,4,4,4
	.byte 12,12,3,134,191,5,14,6,3,4,4,4,3,5,134,242,3,5,4,4,4,6,3,3,6,135,29,4,4,3,7,7
	.byte 4,5,4,7,135,78,7,4,4,4,4,3,5,4,2,135,117,5,2,2,7,4,12,12,7,7,135,181,12,7,12,5
	.byte 4,5,4,15,6,135,255,3,4,12,4,4,4,4,4,4,136,46,4,4,4,4,3,4,12,4,4,136,91,2,4,3
	.byte 4,4,2,2,4,4,136,124,7,4,7,7,4,12,6,3,3,136,183,12,12,4,12,12,7,12,7,12,137,29,7,12
	.byte 7,4,7,7,7,7,12,137,106,7,7,4,4,4,3,3,6,3,137,150,6,4,3,3,4,4,7,12,12,137,212,12
	.byte 3,3,3,3,3,4,4,4,137,255,3,4,4,4,4,4,12,3,7,138,49,4,1,5,5,2,2,5,4,4,138,83
	.byte 2,2,2,2,2,5,3,3,3,138,110,3,3,4,4,14,6,7,5,5,138,164,2,2,4,5,6,3,3,6,3,138
	.byte 201,5,5,12,3,3
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 205,10,21,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 165
	.short 176, 187, 198, 209, 225
	.byte 145,73,3,3,3,3,3,3,3,3,3,145,103,3,3,3,3,3,3,3,3,3,145,133,3,3,3,3,3,3,3,3
	.byte 3,145,163,3,3,3,3,3,14,3,3,3,145,204,3,3,3,3,3,3,3,3,3,145,234,3,3,3,3,3,3,3
	.byte 3,14,146,19,3,3,3,3,3,3,3,3,3,146,49,3,3,3,3,3,3,3,3,3,146,79,3,3,4,3,3,3
	.byte 3,15,3,146,122,3,3,3,3,3,3,3,3,3,146,152,3,3,3,3,3,3,3,3,3,146,182,4,3,3,3,3
	.byte 3,3,3,3,146,213,3,3,3,3,3,3,3,3,3,146,243,3,3,3,3,3,3,3,3,3,147,17,3,4,3,3
	.byte 3,3,3,3,3,147,48,3,3,3,3,3,3,3,3,3,147,78,3,3,3,3,3,3,3,3,3,147,108,3,4,3
	.byte 3,3,3,3,3,3,147,139,3,3,3,3,3,3,3,3,3,147,169,3,3,3,3,3,3,255,255,255,236,69,147,190
	.byte 3,147,196,3,3,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13
	.byte 11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,104,68,13,11,31,12,13,0,72,14,8
	.byte 135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,104,68,13,11,31,12,13,0,72,14,8
	.byte 135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11,25,12,13,0,72,14,8
	.byte 135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,64,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,136
	.byte 5,138,4,139,3,142,1,68,14,104,68,13,11,32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136
	.byte 5,138,4,139,3,142,1,68,14,144,4,68,13,11,28,12,13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4
	.byte 139,3,142,1,68,14,128,1,68,13,11,32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138
	.byte 4,139,3,142,1,68,14,136,1,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1
	.byte 68,14,112,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 45,10,5,2
	.short 0, 11, 22, 38, 50
	.byte 147,211,7,5,5,5,5,5,5,5,5,148,7,24,68,5,5,5,26,23,23,23,149,54,128,238,71,23,128,248,128,231
	.byte 56,61,128,166,129,81,155,184,128,237,57,23,79,23,23,23,23,23,157,206,23,23,23,23

.text
	.align 4
plt:
_mono_aot_OffSeasonProTouch_plt:

.set _p_1_plt_OffSeasonPro_Touch_Constants_get_IsTallScreen_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Constants_get_IsTallScreen
_p_2_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIScreen_get_MainScreen
plt_MonoTouch_UIKit_UIScreen_get_MainScreen:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1188,2796
_p_3_plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string
plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1192,2801
_p_4_plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
plt_CrossUI_Touch_Dialog_Elements_BadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1196,2806
_p_5_plt_CrossUI_Touch_Dialog_Elements_BadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_BadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView
plt_CrossUI_Touch_Dialog_Elements_BadgeElement_GetCellImpl_MonoTouch_UIKit_UITableView:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1200,2811
_p_6_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1204,2816
_p_7_plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single
plt_MonoTouch_UIKit_UIColor__ctor_single_single_single_single:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1208,2843
_p_8_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIFont_FromName_string_single
plt_MonoTouch_UIKit_UIFont_FromName_string_single:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1212,2848
_p_9_plt_MonoTouch_UIKit_UIColor_get_Black_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_Black
plt_MonoTouch_UIKit_UIColor_get_Black:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1216,2853
_p_10_plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int
plt_MonoTouch_UIKit_UIColor_FromRGB_int_int_int:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1220,2858
_p_11_plt_CrossUI_Touch_Dialog_Elements_BooleanElement__ctor_string_bool_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_BooleanElement__ctor_string_bool
plt_CrossUI_Touch_Dialog_Elements_BooleanElement__ctor_string_bool:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1224,2863
_p_12_plt_CrossUI_Touch_Dialog_Elements_BooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_BooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView
plt_CrossUI_Touch_Dialog_Elements_BooleanElement_GetCellImpl_MonoTouch_UIKit_UITableView:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1228,2868
_p_13_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_string
plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_string:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1232,2873
_p_14_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string
plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1236,2878
_p_15_plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string
plt_CrossUI_Touch_Dialog_Elements_EntryElement__ctor_string_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1240,2883
_p_16_plt_CrossUI_Touch_Dialog_Elements_EntryElement_GetCellImpl_MonoTouch_UIKit_UITableView_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement_GetCellImpl_MonoTouch_UIKit_UITableView
plt_CrossUI_Touch_Dialog_Elements_EntryElement_GetCellImpl_MonoTouch_UIKit_UITableView:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1244,2888
_p_17_plt_MonoTouch_Foundation_NSString__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSString__ctor_string
plt_MonoTouch_Foundation_NSString__ctor_string:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1248,2893
_p_18_plt_string_Contains_string_llvm:
	.no_dead_strip plt_string_Contains_string
plt_string_Contains_string:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1252,2898
_p_19_plt_string_Equals_string_llvm:
	.no_dead_strip plt_string_Equals_string
plt_string_Equals_string:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1256,2903
_p_20_plt_CrossUI_Touch_Dialog_Elements_RadioElement__ctor_string_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RadioElement__ctor_string_string
plt_CrossUI_Touch_Dialog_Elements_RadioElement__ctor_string_string:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1260,2908
_p_21_plt_CrossUI_Touch_Dialog_Elements_Element_GetCell_MonoTouch_UIKit_UITableView_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_Element_GetCell_MonoTouch_UIKit_UITableView
plt_CrossUI_Touch_Dialog_Elements_Element_GetCell_MonoTouch_UIKit_UITableView:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1264,2913
_p_22_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_string
plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_string:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1268,2918
_p_23_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorSource_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorSource
plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorSource:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1272,2923
_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1276,2935
_p_25_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1280,2980
_p_26_plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string___llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string__
plt_MonoTouch_UIKit_UIAlertView__ctor_string_string_MonoTouch_UIKit_UIAlertViewDelegate_string_string__:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1284,3003
_p_27_plt_OffSeasonPro_Touch_ErrorDisplayer_ShowError_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_ErrorDisplayer_ShowError_string
plt_OffSeasonPro_Touch_ErrorDisplayer_ShowError_string:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1288,3008
_p_28_plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_TouchUpInside_System_EventHandler:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1292,3010
_p_29_plt_MonoTouch_UIKit_UIBarButtonItem_add_Clicked_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIBarButtonItem_add_Clicked_System_EventHandler
plt_MonoTouch_UIKit_UIBarButtonItem_add_Clicked_System_EventHandler:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1296,3015
_p_30_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1300,3020
_p_31_plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1304,3025
_p_32_plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler
plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1308,3030
_p_33_plt_MonoTouch_UIKit_UIImage__ctor_MonoTouch_CoreGraphics_CGImage_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage__ctor_MonoTouch_CoreGraphics_CGImage
plt_MonoTouch_UIKit_UIImage__ctor_MonoTouch_CoreGraphics_CGImage:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1312,3035
_p_34_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1316,3040
_p_35_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1320,3045
_p_36_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1324,3071
_p_37_plt_string_Format_string_object___llvm:
	.no_dead_strip plt_string_Format_string_object__
plt_string_Format_string_object__:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1328,3101
_p_38_plt_MonoTouch_UIKit_UIApplication_Main_string___string_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplication_Main_string___string_string
plt_MonoTouch_UIKit_UIApplication_Main_string___string_string:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1332,3106
_p_39_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1336,3111
_p_40_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1340,3116
_p_41_plt_MonoTouch_UIKit_UINavigationBar_GetTitleTextAttributes_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINavigationBar_GetTitleTextAttributes
plt_MonoTouch_UIKit_UINavigationBar_GetTitleTextAttributes:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1344,3121
_p_42_plt_MonoTouch_UIKit_UINavigationBar_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINavigationBar_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes
plt_MonoTouch_UIKit_UINavigationBar_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1348,3126
_p_43_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1352,3131
_p_44_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1356,3136
_p_45_plt_MonoTouch_UIKit_UIFont_get_FamilyNames_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIFont_get_FamilyNames
plt_MonoTouch_UIKit_UIFont_get_FamilyNames:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1360,3141
_p_46_plt_System_Collections_Generic_List_1_string__ctor_System_Collections_Generic_IEnumerable_1_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_string__ctor_System_Collections_Generic_IEnumerable_1_string
plt_System_Collections_Generic_List_1_string__ctor_System_Collections_Generic_IEnumerable_1_string:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1364,3146
_p_47_plt_System_Collections_Generic_List_1_string_Sort_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_string_Sort
plt_System_Collections_Generic_List_1_string_Sort:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1368,3157
_p_48_plt_System_Collections_Generic_List_1_string_GetEnumerator_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_string_GetEnumerator
plt_System_Collections_Generic_List_1_string_GetEnumerator:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1372,3168
_p_49_plt_System_Console_WriteLine_string_llvm:
	.no_dead_strip plt_System_Console_WriteLine_string
plt_System_Console_WriteLine_string:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1376,3179
_p_50_plt_OffSeasonPro_Core_App__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_App__ctor
plt_OffSeasonPro_Core_App__ctor:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1380,3184
_p_51_plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext
plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1384,3189
_p_52_plt_MonoTouch_UIKit_UIFont_FontNamesForFamilyName_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIFont_FontNamesForFamilyName_string
plt_MonoTouch_UIKit_UIFont_FontNamesForFamilyName_string:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1388,3200
_p_53_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_File_Touch_Plugin_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_File_Touch_Plugin
plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_File_Touch_Plugin:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1392,3205
_p_54_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin
plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry_AddConventionalPlugin_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1396,3217
_p_55_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1400,3229
_p_56_plt_OffSeasonPro_Touch_ErrorDisplayer__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_ErrorDisplayer__ctor
plt_OffSeasonPro_Touch_ErrorDisplayer__ctor:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1404,3255
_p_57_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance
plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1408,3257
_p_58_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IViewModelCloser_OffSeasonPro_Core_Interfaces_IViewModelCloser_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IViewModelCloser_OffSeasonPro_Core_Interfaces_IViewModelCloser
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IViewModelCloser_OffSeasonPro_Core_Interfaces_IViewModelCloser:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1412,3262
_p_59_plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1416,3274
_p_60_plt_MonoTouch_UIKit_UICollectionViewSource__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewSource__ctor
plt_MonoTouch_UIKit_UICollectionViewSource__ctor:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1420,3279
_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1424,3284
_p_62_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1428,3313
_p_63_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1432,3346
_p_64_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1436,3374
_p_65_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1440,3379
_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1444,3384
_p_67_plt__class_init_MonoTouch_UIKit_UIImagePickerController_llvm:
	.no_dead_strip plt__class_init_MonoTouch_UIKit_UIImagePickerController
plt__class_init_MonoTouch_UIKit_UIImagePickerController:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1448,3389
_p_68_plt_MonoTouch_UIKit_UIImagePickerController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImagePickerController__ctor
plt_MonoTouch_UIKit_UIImagePickerController__ctor:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1452,3394
_p_69_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel
plt_OffSeasonPro_Touch_Views_BackgroundChooserView_get_ViewModel:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1456,3399

.set _p_70_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_PictureChosenDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
_p_71_plt_MonoTouch_UIKit_UIImagePickerController_IsSourceTypeAvailable_MonoTouch_UIKit_UIImagePickerControllerSourceType_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImagePickerController_IsSourceTypeAvailable_MonoTouch_UIKit_UIImagePickerControllerSourceType
plt_MonoTouch_UIKit_UIImagePickerController_IsSourceTypeAvailable_MonoTouch_UIKit_UIImagePickerControllerSourceType:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1464,3403
_p_72_plt_MonoTouch_UIKit_UIActionSheet__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIActionSheet__ctor_string
plt_MonoTouch_UIKit_UIActionSheet__ctor_string:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1468,3408
_p_73_plt_MonoTouch_UIKit_UIActionSheet_add_Clicked_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIActionSheet_add_Clicked_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs
plt_MonoTouch_UIKit_UIActionSheet_add_Clicked_System_EventHandler_1_MonoTouch_UIKit_UIButtonEventArgs:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1472,3413
_p_74_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1476,3418
_p_75_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1480,3423
_p_76_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings:
_p_76:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1484,3428
_p_77_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1488,3433

.set _p_78_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundChooserView_CustomBgDelegate__ctor_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel
_p_79_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet
plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ShowPhotoActionSheet:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1496,3440
_p_80_plt_MonoTouch_UIKit_UIAlertViewDelegate__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIAlertViewDelegate__ctor
plt_MonoTouch_UIKit_UIAlertViewDelegate__ctor:
_p_80:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1500,3442
_p_81_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail
plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1504,3447
_p_82_plt_MonoTouch_UIKit_UIImagePickerControllerDelegate__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImagePickerControllerDelegate__ctor
plt_MonoTouch_UIKit_UIImagePickerControllerDelegate__ctor:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1508,3452
_p_83_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm:
	.no_dead_strip plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder
plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder:
_p_83:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1512,3457
_p_84_plt_System_IO_Path_Combine_string_string_llvm:
	.no_dead_strip plt_System_IO_Path_Combine_string_string
plt_System_IO_Path_Combine_string_string:
_p_84:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1516,3462
_p_85_plt_MonoTouch_UIKit_UIImage_AsJPEG_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_AsJPEG
plt_MonoTouch_UIKit_UIImage_AsJPEG:
_p_85:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1520,3467
_p_86_plt_MonoTouch_Foundation_NSData_Save_string_bool_MonoTouch_Foundation_NSError__llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSData_Save_string_bool_MonoTouch_Foundation_NSError_
plt_MonoTouch_Foundation_NSData_Save_string_bool_MonoTouch_Foundation_NSError_:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1524,3472
_p_87_plt_MonoTouch_MediaPlayer_MPMediaPickerController__ctor_MonoTouch_MediaPlayer_MPMediaType_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaPickerController__ctor_MonoTouch_MediaPlayer_MPMediaType
plt_MonoTouch_MediaPlayer_MPMediaPickerController__ctor_MonoTouch_MediaPlayer_MPMediaType:
_p_87:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1528,3477
_p_88_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidLoad
plt_MonoTouch_UIKit_UIViewController_ViewDidLoad:
_p_88:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1532,3482
_p_89_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool
plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController__ctor_MonoTouch_UIKit_UITableViewStyle_CrossUI_Touch_Dialog_Elements_RootElement_bool:
_p_89:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1536,3487
_p_90_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel
plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_get_ViewModel:
_p_90:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1540,3492
_p_91_plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
plt_Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
_p_91:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1544,3497
_p_92_plt_System_Linq_Expressions_Expression_Parameter_System_Type_string_llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Parameter_System_Type_string
plt_System_Linq_Expressions_Expression_Parameter_System_Type_string:
_p_92:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1548,3502
_p_93_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad
plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidLoad:
_p_93:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1552,3507
_p_94_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_ObjCRuntime_Selector__ctor_string
plt_MonoTouch_ObjCRuntime_Selector__ctor_string:
_p_94:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1556,3512

.set _p_95_plt_OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_BindableProgress__ctor_MonoTouch_UIKit_UIViewController_string_bool
_p_96_plt_MonoTouch_UIKit_UIColor_get_LightGray_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_LightGray
plt_MonoTouch_UIKit_UIColor_get_LightGray:
_p_96:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1564,3520
_p_97_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_string_MonoTouch_UIKit_UIBarButtonItemStyle_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIBarButtonItem__ctor_string_MonoTouch_UIKit_UIBarButtonItemStyle_System_EventHandler
plt_MonoTouch_UIKit_UIBarButtonItem__ctor_string_MonoTouch_UIKit_UIBarButtonItemStyle_System_EventHandler:
_p_97:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1568,3525
_p_98_plt_MonoTouch_UIKit_UIBarItem_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_MonoTouch_UIKit_UIControlState_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIBarItem_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_MonoTouch_UIKit_UIControlState
plt_MonoTouch_UIKit_UIBarItem_SetTitleTextAttributes_MonoTouch_UIKit_UITextAttributes_MonoTouch_UIKit_UIControlState:
_p_98:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1572,3530
_p_99_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem
plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem:
_p_99:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1576,3535
_p_100_plt_MonoTouch_UIKit_UIImage_FromBundle_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_FromBundle_string
plt_MonoTouch_UIKit_UIImage_FromBundle_string:
_p_100:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1580,3540
_p_101_plt_OffSeasonPro_Touch_Constants_get_Background_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Constants_get_Background
plt_OffSeasonPro_Touch_Constants_get_Background:
_p_101:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1584,3545
_p_102_plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage
plt_MonoTouch_UIKit_UIColor_FromPatternImage_MonoTouch_UIKit_UIImage:
_p_102:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1588,3547
_p_103_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_OffSeasonPro_Touch_Views_SettingsView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_OffSeasonPro_Touch_Views_SettingsView
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_OffSeasonPro_Touch_Views_SettingsView:
_p_103:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1592,3552
_p_104_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_Bind_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Touch_BindableProgress_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_Bind_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Touch_BindableProgress
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_SettingsView_OffSeasonPro_Core_ViewModels_SettingsViewModel_Bind_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Touch_BindableProgress:
_p_104:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1596,3564
_p_105_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm:
	.no_dead_strip plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle
plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle:
_p_105:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1600,3576
_p_106_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo
plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo:
_p_106:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1604,3581
_p_107_plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type_llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type
plt_System_Linq_Expressions_Expression_Convert_System_Linq_Expressions_Expression_System_Type:
_p_107:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1608,3586
_p_108_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_108:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1612,3591
_p_109_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_BindableProgress_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_BindableProgress_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_BindableProgress_object:
_p_109:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1616,3603
_p_110_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_110:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1620,3614
_p_111_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_BindableProgress_OffSeasonPro_Core_ViewModels_SettingsViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_SettingsViewModel_object:
_p_111:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1624,3626
_p_112_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool
plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewDidAppear_bool:
_p_112:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1628,3637
_p_113_plt_OffSeasonPro_Touch_Views_SettingsView_ResetDisplay_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_SettingsView_ResetDisplay
plt_OffSeasonPro_Touch_Views_SettingsView_ResetDisplay:
_p_113:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1632,3642
_p_114_plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool
plt_Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController_ViewWillDisappear_bool:
_p_114:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1636,3644
_p_115_plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage
plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage:
_p_115:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1640,3649
_p_116_plt_CrossUI_Touch_Dialog_Elements_Section__ctor_MonoTouch_UIKit_UIView_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_Section__ctor_MonoTouch_UIKit_UIView
plt_CrossUI_Touch_Dialog_Elements_Section__ctor_MonoTouch_UIKit_UIView:
_p_116:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1644,3654

.set _p_117_plt_OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyEntryElement__ctor_string
_p_118_plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType
plt_CrossUI_Touch_Dialog_Elements_EntryElement_set_KeyboardType_MonoTouch_UIKit_UIKeyboardType:
_p_118:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1652,3661
_p_119_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyEntryElement_OffSeasonPro_Touch_Controls_MyEntryElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string:
_p_119:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1656,3666
_p_120_plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement
plt_System_Collections_Generic_List_1_CrossUI_Touch_Dialog_Elements_EntryElement_Add_CrossUI_Touch_Dialog_Elements_EntryElement:
_p_120:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1660,3678
_p_121_plt_CrossUI_Touch_Dialog_Elements_Section_AddAll_System_Collections_Generic_IEnumerable_1_CrossUI_Touch_Dialog_Elements_Element_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_Section_AddAll_System_Collections_Generic_IEnumerable_1_CrossUI_Touch_Dialog_Elements_Element
plt_CrossUI_Touch_Dialog_Elements_Section_AddAll_System_Collections_Generic_IEnumerable_1_CrossUI_Touch_Dialog_Elements_Element:
_p_121:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1664,3689
_p_122_plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel
plt_OffSeasonPro_Touch_Views_SettingsView_get_ViewModel:
_p_122:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1668,3694
_p_123_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings:
_p_123:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1672,3696

.set _p_124_plt_OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBooleanElement__ctor_string_bool
_p_125_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyBooleanElement_OffSeasonPro_Touch_Controls_MyBooleanElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyBooleanElement_OffSeasonPro_Touch_Controls_MyBooleanElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindExtensions_Bind_OffSeasonPro_Touch_Controls_MyBooleanElement_OffSeasonPro_Touch_Controls_MyBooleanElement_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string:
_p_125:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1680,3703
_p_126_plt_CrossUI_Touch_Dialog_Elements_Section_Add_CrossUI_Touch_Dialog_Elements_Element_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_Section_Add_CrossUI_Touch_Dialog_Elements_Element
plt_CrossUI_Touch_Dialog_Elements_Section_Add_CrossUI_Touch_Dialog_Elements_Element:
_p_126:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1684,3715

.set _p_127_plt_OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Controls_MyBadgeElement__ctor_MonoTouch_UIKit_UIImage_string_MonoTouch_Foundation_NSAction
_p_128_plt_CrossUI_Touch_Dialog_Elements_RootElement__ctor_string_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RootElement__ctor_string
plt_CrossUI_Touch_Dialog_Elements_RootElement__ctor_string:
_p_128:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1692,3722
_p_129_plt_CrossUI_Touch_Dialog_Elements_RootElement_Add_CrossUI_Touch_Dialog_Elements_Section_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_RootElement_Add_CrossUI_Touch_Dialog_Elements_Section
plt_CrossUI_Touch_Dialog_Elements_RootElement_Add_CrossUI_Touch_Dialog_Elements_Section:
_p_129:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1696,3727
_p_130_plt_MonoTouch_UIKit_UIColor_get_Clear_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_Clear
plt_MonoTouch_UIKit_UIColor_get_Clear:
_p_130:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1700,3732
_p_131_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_FromFile_string
plt_MonoTouch_UIKit_UIImage_FromFile_string:
_p_131:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1704,3737
_p_132_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings:
_p_132:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1708,3742
_p_133_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave:
_p_133:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1712,3747
_p_134_plt_MonoTouch_UIKit_UIView_EndEditing_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView_EndEditing_bool
plt_MonoTouch_UIKit_UIView_EndEditing_bool:
_p_134:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1716,3752
_p_135_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser:
_p_135:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1720,3757
_p_136_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_136:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1724,3762
_p_137_plt_FlipNumbers_FlipNumbersView_SetValue_int_bool_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumbersView_SetValue_int_bool
plt_FlipNumbers_FlipNumbersView_SetValue_int_bool:
_p_137:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1728,3767
_p_138_plt_string_op_Equality_string_string_llvm:
	.no_dead_strip plt_string_op_Equality_string_string
plt_string_op_Equality_string_string:
_p_138:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1732,3772
_p_139_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool:
_p_139:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1736,3777
_p_140_plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel
plt_OffSeasonPro_Touch_Views_WorkoutView_get_ViewModel:
_p_140:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1740,3782
_p_141_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave:
_p_141:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1744,3784
_p_142_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplication_get_SharedApplication
plt_MonoTouch_UIKit_UIApplication_get_SharedApplication:
_p_142:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1748,3789
_p_143_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad:
_p_143:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1752,3794
_p_144_plt_OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod
plt_OffSeasonPro_Touch_Views_WorkoutView_LoadAdMod:
_p_144:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1756,3799
_p_145_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger:
_p_145:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1760,3802
_p_146_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount:
_p_146:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1764,3814
_p_147_plt_FlipNumbers_FlipNumbersView__ctor_int_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumbersView__ctor_int
plt_FlipNumbers_FlipNumbersView__ctor_int:
_p_147:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1768,3819
_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_OffSeasonPro_Touch_Views_WorkoutView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_OffSeasonPro_Touch_Views_WorkoutView
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_OffSeasonPro_Touch_Views_WorkoutView:
_p_148:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1772,3824
_p_149_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Touch_Views_WorkoutView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Touch_Views_WorkoutView
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Touch_Views_WorkoutView:
_p_149:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1776,3836
_p_150_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_150:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1780,3848
_p_151_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Touch_Views_WorkoutView_object:
_p_151:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1784,3860
_p_152_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_152:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1788,3871
_p_153_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object:
_p_153:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1792,3883
_p_154_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UILabel_MonoTouch_UIKit_UILabel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UILabel_MonoTouch_UIKit_UILabel
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UILabel_MonoTouch_UIKit_UILabel:
_p_154:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1796,3894
_p_155_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UILabel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UILabel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UILabel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object:
_p_155:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1800,3906
_p_156_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_WorkoutView_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton:
_p_156:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1804,3917
_p_157_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_WorkoutViewModel_object:
_p_157:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1808,3929
_p_158_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_MonoTouch_UIKit_UIButton_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_158:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1812,3940
_p_159_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_WorkoutViewModel_For_System_Linq_Expressions_Expression_1_System_Func_2_MonoTouch_UIKit_UIButton_object:
_p_159:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1816,3952
_p_160_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings:
_p_160:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1820,3963
_p_161_plt_MonoTouch_UIKit_UIColor_get_DarkGray_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_DarkGray
plt_MonoTouch_UIKit_UIColor_get_DarkGray:
_p_161:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1824,3968
_p_162_plt_OffSeasonPro_Touch_Constants_get_ConcreteBackground_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Constants_get_ConcreteBackground
plt_OffSeasonPro_Touch_Constants_get_ConcreteBackground:
_p_162:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1828,3973
_p_163_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand:
_p_163:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1832,3975
_p_164_plt_GoogleAdMobAds_GADInterstitial__ctor_llvm:
	.no_dead_strip plt_GoogleAdMobAds_GADInterstitial__ctor
plt_GoogleAdMobAds_GADInterstitial__ctor:
_p_164:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1836,3980
_p_165_plt_GoogleAdMobAds_GADRequest_get_Request_llvm:
	.no_dead_strip plt_GoogleAdMobAds_GADRequest_get_Request
plt_GoogleAdMobAds_GADRequest_get_Request:
_p_165:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1840,3985
_p_166_plt_GoogleAdMobAds_GADInterstitial_add_DidReceiveAd_System_EventHandler_llvm:
	.no_dead_strip plt_GoogleAdMobAds_GADInterstitial_add_DidReceiveAd_System_EventHandler
plt_GoogleAdMobAds_GADInterstitial_add_DidReceiveAd_System_EventHandler:
_p_166:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1844,3990
_p_167_plt_GoogleAdMobAds_GADInterstitial_add_DidFailToReceiveAd_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs_llvm:
	.no_dead_strip plt_GoogleAdMobAds_GADInterstitial_add_DidFailToReceiveAd_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs
plt_GoogleAdMobAds_GADInterstitial_add_DidFailToReceiveAd_System_EventHandler_1_GoogleAdMobAds_GADInterstitialDidFailToReceiveAdWithErrorEventArgs:
_p_167:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1848,3995
_p_168_plt_GoogleAdMobAds_GADInterstitial_add_DidDismissScreen_System_EventHandler_llvm:
	.no_dead_strip plt_GoogleAdMobAds_GADInterstitial_add_DidDismissScreen_System_EventHandler
plt_GoogleAdMobAds_GADInterstitial_add_DidDismissScreen_System_EventHandler:
_p_168:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1852,4000
_p_169_plt_MonoTouch_Foundation_NSObject_Dispose_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSObject_Dispose
plt_MonoTouch_Foundation_NSObject_Dispose:
_p_169:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1856,4005
_p_170_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance:
_p_170:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1860,4010
_p_171_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool:
_p_171:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1864,4015
_p_172_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_OffSeasonPro_Touch_Views_HomeView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_OffSeasonPro_Touch_Views_HomeView
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingSet_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_OffSeasonPro_Touch_Views_HomeView:
_p_172:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1868,4020
_p_173_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescriptionSet_2_OffSeasonPro_Touch_Views_HomeView_OffSeasonPro_Core_ViewModels_HomeViewModel_Bind_MonoTouch_UIKit_UIButton_MonoTouch_UIKit_UIButton:
_p_173:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1872,4032
_p_174_plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_174:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1876,4044
_p_175_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_HomeViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_HomeViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxFluentBindingDescription_2_MonoTouch_UIKit_UIButton_OffSeasonPro_Core_ViewModels_HomeViewModel_To_System_Linq_Expressions_Expression_1_System_Func_2_OffSeasonPro_Core_ViewModels_HomeViewModel_object:
_p_175:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1880,4056

.set _p_176_plt_OffSeasonPro_Touch_Views_MediaPickerView__ctor_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Views_MediaPickerView__ctor
_p_177_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController:
_p_177:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1888,4069
_p_178_plt_MonoTouch_MediaPlayer_MPMediaPickerController_set_Delegate_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaPickerController_set_Delegate_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate
plt_MonoTouch_MediaPlayer_MPMediaPickerController_set_Delegate_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate:
_p_178:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1892,4074
_p_179_plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel
plt_OffSeasonPro_Touch_Views_HomeView_get_ViewModel:
_p_179:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1896,4079
_p_180_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings:
_p_180:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1900,4082
_p_181_plt_OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout
plt_OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout:
_p_181:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1904,4087
_p_182_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand
plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand:
_p_182:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1908,4092

.set _p_183_plt_OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Views_HomeView_ContinueWorkoutDelegate__ctor_OffSeasonPro_Core_ViewModels_HomeViewModel
_p_184_plt_OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker
plt_OffSeasonPro_Touch_Views_HomeView_ShowMediaPicker:
_p_184:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1916,4100
_p_185_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings:
_p_185:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1920,4103
_p_186_plt_MBProgressHUD_MTMBProgressHUD__ctor_MonoTouch_UIKit_UIView_llvm:
	.no_dead_strip plt_MBProgressHUD_MTMBProgressHUD__ctor_MonoTouch_UIKit_UIView
plt_MBProgressHUD_MTMBProgressHUD__ctor_MonoTouch_UIKit_UIView:
_p_186:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1924,4108
_p_187_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor
plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor:
_p_187:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1928,4113
_p_188_plt_System_Linq_Enumerable_Last_MonoTouch_UIKit_UIViewController_System_Collections_Generic_IEnumerable_1_MonoTouch_UIKit_UIViewController_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_Last_MonoTouch_UIKit_UIViewController_System_Collections_Generic_IEnumerable_1_MonoTouch_UIKit_UIViewController
plt_System_Linq_Enumerable_Last_MonoTouch_UIKit_UIViewController_System_Collections_Generic_IEnumerable_1_MonoTouch_UIKit_UIViewController:
_p_188:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1932,4118
_p_189_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout:
_p_189:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1936,4130
_p_190_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication:
_p_190:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1940,4135
_p_191_plt__class_init_System_DateTime_llvm:
	.no_dead_strip plt__class_init_System_DateTime
plt__class_init_System_DateTime:
_p_191:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1944,4140
_p_192_plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime
plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime:
_p_192:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1948,4145
_p_193_plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string
plt_OffSeasonPro_Touch_Views_BackgroundChooserView_ElementTapped_string:
_p_193:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1952,4150
_p_194_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_194:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1956,4152
_p_195_plt_MonoTouch_UIKit_UITextField_set_ShouldChangeCharacters_MonoTouch_UIKit_UITextFieldChange_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextField_set_ShouldChangeCharacters_MonoTouch_UIKit_UITextFieldChange
plt_MonoTouch_UIKit_UITextField_set_ShouldChangeCharacters_MonoTouch_UIKit_UITextFieldChange:
_p_195:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1960,4190
_p_196_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_196:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1964,4195
_p_197_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_197:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1968,4230
_p_198_plt_MonoTouch_UIKit_UITextField_get_ShouldChangeCharacters_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextField_get_ShouldChangeCharacters
plt_MonoTouch_UIKit_UITextField_get_ShouldChangeCharacters:
_p_198:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1972,4235
_p_199_plt_CrossUI_Touch_Dialog_Elements_EntryElement_CreateTextField_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_CrossUI_Touch_Dialog_Elements_EntryElement_CreateTextField_System_Drawing_RectangleF
plt_CrossUI_Touch_Dialog_Elements_EntryElement_CreateTextField_System_Drawing_RectangleF:
_p_199:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1976,4240
_p_200_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_200:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1980,4245
_p_201_plt_OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
plt_OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__:
_p_201:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1984,4284

.set _p_202_plt_OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF_llvm, _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF
_p_203_plt_MonoTouch_UIKit_UIImageView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor
plt_MonoTouch_UIKit_UIImageView__ctor:
_p_203:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1992,4288
_p_204_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF:
_p_204:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 1996,4293
_p_205_plt_System_Drawing_RectangleF__ctor_single_single_single_single_llvm:
	.no_dead_strip plt_System_Drawing_RectangleF__ctor_single_single_single_single
plt_System_Drawing_RectangleF__ctor_single_single_single_single:
_p_205:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2000,4298
_p_206_plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem_System_EventHandler
plt_MonoTouch_UIKit_UIBarButtonItem__ctor_MonoTouch_UIKit_UIBarButtonSystemItem_System_EventHandler:
_p_206:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2004,4303
_p_207_plt_MonoTouch_UIKit_UIView_Add_MonoTouch_UIKit_UIView_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView_Add_MonoTouch_UIKit_UIView
plt_MonoTouch_UIKit_UIView_Add_MonoTouch_UIKit_UIView:
_p_207:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2008,4308
_p_208_plt_MonoTouch_UIKit_UIButton__ctor_MonoTouch_UIKit_UIButtonType_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIButton__ctor_MonoTouch_UIKit_UIButtonType
plt_MonoTouch_UIKit_UIButton__ctor_MonoTouch_UIKit_UIButtonType:
_p_208:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2012,4313
_p_209_plt_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement_Add_OffSeasonPro_Touch_Views_BackgroundImageElement_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement_Add_OffSeasonPro_Touch_Views_BackgroundImageElement
plt_System_Collections_Generic_List_1_OffSeasonPro_Touch_Views_BackgroundImageElement_Add_OffSeasonPro_Touch_Views_BackgroundImageElement:
_p_209:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2016,4318

.set _p_210_plt_OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Views_BackgroundImageElement__ctor_MonoTouch_UIKit_UIImage_MonoTouch_Foundation_NSAction
_p_211_plt_System_Collections_Generic_List_1_string_Add_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_string_Add_string
plt_System_Collections_Generic_List_1_string_Add_string:
_p_211:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2024,4331
_p_212_plt_MonoTouch_UIKit_UICollectionView_set_Source_MonoTouch_UIKit_UICollectionViewSource_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionView_set_Source_MonoTouch_UIKit_UICollectionViewSource
plt_MonoTouch_UIKit_UICollectionView_set_Source_MonoTouch_UIKit_UICollectionViewSource:
_p_212:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2028,4342
_p_213_plt_MonoTouch_UIKit_UICollectionView_RegisterClassForCell_System_Type_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionView_RegisterClassForCell_System_Type_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UICollectionView_RegisterClassForCell_System_Type_MonoTouch_Foundation_NSString:
_p_213:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2032,4347
_p_214_plt_MonoTouch_UIKit_UICollectionView__ctor_System_Drawing_RectangleF_MonoTouch_UIKit_UICollectionViewLayout_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionView__ctor_System_Drawing_RectangleF_MonoTouch_UIKit_UICollectionViewLayout
plt_MonoTouch_UIKit_UICollectionView__ctor_System_Drawing_RectangleF_MonoTouch_UIKit_UICollectionViewLayout:
_p_214:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2036,4352
_p_215_plt_MonoTouch_UIKit_UICollectionViewFlowLayout__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewFlowLayout__ctor
plt_MonoTouch_UIKit_UICollectionViewFlowLayout__ctor:
_p_215:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2040,4357
_p_216_plt_OffSeasonPro_Touch_Views_BackgroundSource__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Touch_Views_BackgroundSource__ctor
plt_OffSeasonPro_Touch_Views_BackgroundSource__ctor:
_p_216:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2044,4362
_p_217_plt_MonoTouch_UIKit_UIControl_add_TouchDown_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_TouchDown_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_TouchDown_System_EventHandler:
_p_217:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2048,4364
_p_218_plt_MonoTouch_UIKit_UIColor_get_White_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_White
plt_MonoTouch_UIKit_UIColor_get_White:
_p_218:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2052,4369
_p_219_plt_MonoTouch_UIKit_UIButton__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIButton__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIButton__ctor_System_Drawing_RectangleF:
_p_219:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2056,4374
_p_220_plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF:
_p_220:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2060,4379
_p_221_plt_MonoTouch_iAd_ADBannerView_add_FailedToReceiveAd_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_iAd_ADBannerView_add_FailedToReceiveAd_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs
plt_MonoTouch_iAd_ADBannerView_add_FailedToReceiveAd_System_EventHandler_1_MonoTouch_iAd_AdErrorEventArgs:
_p_221:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2064,4384
_p_222_plt_MonoTouch_iAd_ADBannerView_add_AdLoaded_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_iAd_ADBannerView_add_AdLoaded_System_EventHandler
plt_MonoTouch_iAd_ADBannerView_add_AdLoaded_System_EventHandler:
_p_222:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2068,4389
_p_223_plt_MonoTouch_iAd_ADBannerView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_iAd_ADBannerView__ctor
plt_MonoTouch_iAd_ADBannerView__ctor:
_p_223:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2072,4394
_p_224_plt_MonoTouch_UIKit_UINavigationBar_get_Appearance_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINavigationBar_get_Appearance
plt_MonoTouch_UIKit_UINavigationBar_get_Appearance:
_p_224:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2076,4399
_p_225_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxAppStart_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxAppStart
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxAppStart:
_p_225:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2080,4404

.set _p_226_plt_OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_Setup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_OffSeasonPro_Touch_OffSeasonProPresenter

.set _p_227_plt_OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm, _OffSeasonProTouch__OffSeasonPro_Touch_OffSeasonProPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
_p_228_plt_MonoTouch_UIKit_UIWindow__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIWindow__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIWindow__ctor_System_Drawing_RectangleF:
_p_228:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProTouch_got - . + 2092,4420
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 19
	.asciz "OffSeasonProTouch"
	.asciz "8468ED37-9BF8-417D-9D10-9E0B69F4576D"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "OffSeasonPro.Core"
	.asciz "0FEC62C1-C92D-49F1-B5AB-67D8EB50DC54"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross"
	.asciz "066A9949-60EF-4499-87FB-90DB7F8EAEA9"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Touch"
	.asciz "1E871AF4-634E-4A45-AB5B-DCAE1A4CA1E3"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.File"
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.MvvmCross.Binding"
	.asciz "0CA1A6F2-0F7E-44D7-B6B8-AE8E0BEDDDE8"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "CrossUI.Touch"
	.asciz "327FA55C-F955-41C6-AADD-FF30B004E740"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.Messenger"
	.asciz "64D7401A-6B92-495B-BA98-86C03A7321B4"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Xamarin.Controls.FlipNumbers.iOS"
	.asciz "4BC091A3-507C-4008-B753-79F64B6FF609"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "GoogleAdMobAds"
	.asciz "489296E2-8F47-41D3-93BB-E03567808155"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "OffSeasonProPluginMusicPlayerPluginTouch"
	.asciz "A82964D4-5801-45F2-AB49-4CFD017E9B64"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "MBProgressHUD"
	.asciz "EC58D458-313A-4CE1-A118-DDC55579466A"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.CrossCore.Touch"
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.MvvmCross.Dialog.Touch"
	.asciz "BFC6638D-69D4-485A-A95A-5591B6B6D2B5"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "8468ED37-9BF8-417D-9D10-9E0B69F4576D"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "OffSeasonProTouch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_OffSeasonProTouch_got
	.align 2
	.long _OffSeasonProTouch__OffSeasonPro_Touch_Bootstrap_DownloadCachePluginBootstrap__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 296,2100,229,205,11,387000831,0,7745
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_OffSeasonProTouch_info
	.align 2
_mono_aot_module_OffSeasonProTouch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,11,2,5,4,1,11,2,7,6,1,11,0
	.byte 1,11,10,8,16,15,14,13,12,11,10,9,17,0,0,0,0,0,2,15,18,0,0,0,2,15,18,1,14,0,1,14
	.byte 0,1,14,0,1,14,0,1,14,0,1,14,0,1,14,0,1,14,1,19,1,14,6,15,129,0,128,255,128,254,128,253
	.byte 128,252,1,14,2,15,18,1,14,3,19,21,20,1,14,2,23,22,0,0,0,4,17,18,25,24,0,0,0,0,0,0
	.byte 0,3,129,2,70,129,1,0,6,26,30,29,28,27,31,0,3,34,33,32,0,0,0,0,0,5,35,39,38,37,36,0
	.byte 5,40,39,42,41,36,0,6,43,39,45,44,36,23,0,6,46,39,48,47,36,23,0,1,23,0,1,49,0,5,50,39
	.byte 52,51,36,0,5,53,39,55,54,36,0,5,56,39,58,57,36,0,8,59,64,63,62,61,60,59,59,0,6,65,39,67
	.byte 66,36,68,0,5,69,72,72,71,70,0,0,0,1,73,0,0,0,0,0,11,79,70,78,75,70,77,70,76,75,74,79
	.byte 0,0,0,9,80,81,82,84,85,80,82,82,83,0,2,87,86,0,3,88,90,89,0,2,92,91,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,1,93,0,1,93,0,2,95,93,0,0,0,0,0,0,0,0,0,0
	.byte 1,24,1,128,145,1,24,0,1,24,0,1,24,0,1,24,3,95,21,94,0,0,0,1,96,0,0,0,48,122,123,129
	.byte 23,129,22,129,21,129,20,95,81,129,19,4,129,18,129,17,6,129,16,129,15,82,128,250,128,168,129,14,129,13,128,171
	.byte 129,10,128,168,129,12,129,11,128,171,129,10,82,82,129,9,36,129,8,129,7,39,126,104,36,129,6,129,5,39,128,130
	.byte 36,129,4,129,3,39,128,130,128,133,128,130,0,14,97,108,107,106,105,23,104,103,102,101,23,100,99,98,0,0,0,0
	.byte 0,0,0,1,109,0,7,110,34,33,113,104,112,111,0,1,109,0,0,0,0,0,1,109,0,0,0,4,109,115,114,116
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,117,0,0,0,7,129,27,129,9,129,26,36,129,25
	.byte 129,24,39,0,36,118,128,143,128,142,128,139,128,138,128,141,128,140,128,139,128,138,128,137,128,136,128,135,128,134,128,130
	.byte 128,133,128,130,39,128,132,128,131,36,104,128,130,39,128,129,128,128,36,127,126,125,124,123,122,121,120,119,128,143,0,0
	.byte 0,0,0,0,0,47,128,144,128,172,128,171,128,176,128,175,128,168,128,167,128,174,128,173,128,172,128,171,128,170,128,169
	.byte 128,168,128,167,128,166,128,165,128,164,128,163,128,152,128,162,128,150,128,161,128,152,128,160,128,150,128,159,128,152,128,158
	.byte 128,150,128,157,128,152,128,156,128,150,128,155,128,152,128,154,128,150,128,153,128,152,128,151,128,150,128,149,128,148,128,147
	.byte 128,146,128,145,0,1,109,0,1,109,0,0,0,1,109,0,1,109,0,1,128,177,0,1,128,178,0,0,0,0,0,0
	.byte 0,0,0,1,128,179,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,109,0,126,128,180,128,143,128
	.byte 143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,128,143,9,9,9,9,128,198,128
	.byte 139,128,138,128,211,128,196,128,139,128,138,128,210,128,194,128,198,128,139,128,138,128,209,128,208,128,139,128,138,128,207,128
	.byte 201,128,198,128,139,128,138,128,209,128,208,128,139,128,138,128,207,128,201,128,198,128,139,128,138,128,209,128,208,128,139,128
	.byte 138,128,207,128,201,128,198,128,139,128,206,128,201,128,198,128,139,128,205,128,201,128,198,128,139,128,204,128,203,128,201,128
	.byte 198,128,139,128,202,128,201,128,198,128,139,128,200,128,199,128,198,128,139,128,138,128,197,128,196,128,139,128,138,128,195,128
	.byte 194,128,193,128,192,123,122,128,191,128,190,128,189,128,188,128,187,128,186,128,185,121,128,182,128,181,128,180,121,128,182,128
	.byte 184,128,183,121,128,182,128,184,128,183,121,128,182,128,184,128,183,121,128,182,121,128,182,121,128,182,121,128,182,121,128,182
	.byte 121,128,182,128,181,128,143,0,0,0,1,109,0,1,128,212,0,9,129,34,36,129,33,129,32,39,129,31,129,30,129,29
	.byte 129,28,0,17,128,213,39,128,223,128,222,36,128,221,128,221,128,220,128,219,128,218,128,217,39,128,216,128,215,36,128,214
	.byte 128,221,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,224,0,1,128,225,0,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,21,128,226,9,9,9,39,128,234,128,233,36,39,128,232,128,231,36,128,230,128,139,128,229,128
	.byte 228,128,227,123,122,121,128,143,0,3,128,235,128,236,128,237,0,0,0,0,0,8,34,128,242,113,128,241,128,240,128,239
	.byte 128,238,109,0,0,0,0,0,2,109,109,0,0,0,0,0,4,128,245,128,244,128,243,109,0,0,0,7,129,39,129,38
	.byte 129,37,129,36,129,35,126,9,0,0,0,5,128,246,128,247,128,247,109,128,247,0,0,0,0,0,0,0,0,0,1,23
	.byte 0,0,0,1,23,0,0,0,1,23,0,0,0,1,128,248,0,0,0,0,0,0,0,0,0,0,0,2,109,128,249,0
	.byte 0,0,1,128,250,0,0,0,0,0,0,0,0,0,1,128,251,0,1,128,251,0,1,128,251,0,1,128,251,0,1,128
	.byte 251,0,1,128,251,0,1,128,251,4,2,130,69,1,1,2,75,2,255,252,0,0,0,1,1,7,132,233,255,252,0,0
	.byte 0,1,1,3,219,0,0,10,255,252,0,0,0,1,1,3,219,0,0,14,255,252,0,0,0,1,1,3,219,0,0,18
	.byte 255,252,0,0,0,1,1,3,219,0,0,23,255,252,0,0,0,1,1,3,219,0,0,24,4,2,130,69,1,1,2,130
	.byte 146,1,255,252,0,0,0,1,1,7,133,56,12,0,39,42,47,17,0,79,17,0,1,17,0,128,227,17,0,128,153,17
	.byte 0,129,41,16,1,11,1,17,0,129,57,16,1,11,2,17,0,129,79,16,1,11,3,17,0,129,125,16,1,11,4,17
	.byte 0,129,153,16,1,11,5,14,2,128,181,3,16,1,14,8,17,0,129,187,14,2,29,3,17,0,129,217,16,2,130,146
	.byte 1,136,199,17,0,129,239,14,2,128,210,3,34,255,254,0,0,0,0,255,43,0,0,1,14,3,219,0,0,10,6,39
	.byte 50,39,30,3,219,0,0,10,6,196,0,0,16,17,0,130,73,17,0,130,101,14,2,128,170,3,14,1,35,14,2,130
	.byte 63,1,6,128,177,50,128,177,30,2,130,63,1,14,1,36,6,128,179,50,128,179,14,1,37,6,128,181,50,128,181,14
	.byte 1,38,6,128,183,50,128,183,14,2,128,194,3,14,1,39,6,128,185,50,128,185,14,1,40,6,128,187,50,128,187,14
	.byte 1,41,6,128,189,50,128,189,16,1,18,10,6,197,0,2,131,14,2,92,5,6,52,50,52,30,2,92,5,14,1,42
	.byte 6,128,191,50,128,191,6,197,0,9,123,17,0,130,107,14,6,1,2,130,123,1,14,2,90,5,14,2,130,90,1,17
	.byte 0,130,139,17,0,130,163,6,198,0,1,69,17,0,132,2,17,0,131,41,17,0,130,179,23,2,15,7,17,0,132,128
	.byte 14,3,219,0,0,11,14,3,219,0,0,12,14,2,15,4,17,0,132,208,17,0,132,212,34,255,254,0,0,0,0,255
	.byte 43,0,0,2,34,255,254,0,0,0,0,255,43,0,0,3,14,1,17,34,255,254,0,0,0,0,255,43,0,0,4,16
	.byte 2,3,8,1,14,3,219,0,0,13,4,2,130,50,1,1,1,23,16,7,134,202,135,229,11,1,24,17,0,132,216,16
	.byte 1,24,17,11,2,17,4,14,1,44,14,2,128,201,3,14,1,27,14,1,45,14,2,128,167,3,17,0,134,10,17,0
	.byte 134,32,17,0,133,252,14,3,219,0,0,14,6,128,197,50,128,197,30,3,219,0,0,14,6,197,0,9,126,17,0,134
	.byte 104,17,0,134,140,14,1,26,14,6,1,2,130,146,1,16,2,128,201,3,130,206,17,0,134,64,11,2,128,194,3,11
	.byte 2,21,4,19,0,194,0,0,33,0,17,0,135,87,19,0,193,0,0,150,0,17,0,135,91,17,0,132,250,14,2,123
	.byte 3,17,0,135,97,14,1,33,14,2,128,213,3,17,0,135,129,6,117,50,117,14,2,128,174,3,6,118,50,118,14,6
	.byte 1,2,128,174,3,17,0,135,139,34,255,254,0,0,0,0,255,43,0,0,5,34,255,254,0,0,0,0,255,43,0,0
	.byte 6,18,0,198,0,0,169,0,19,0,193,0,0,63,0,14,6,1,2,29,10,34,255,254,0,0,0,0,255,43,0,0
	.byte 7,18,0,202,0,1,3,0,34,255,254,0,0,0,0,255,43,0,0,8,11,2,129,31,1,17,0,135,169,14,2,129
	.byte 13,3,14,2,54,11,14,3,219,0,0,17,4,2,130,50,1,1,2,30,11,16,7,135,236,135,229,17,0,135,197,14
	.byte 1,14,17,0,135,215,34,255,254,0,0,0,0,255,43,0,0,9,17,0,136,15,17,0,136,25,17,0,136,73,17,0
	.byte 136,91,17,0,136,147,17,0,136,173,17,0,136,231,17,0,136,253,17,0,137,61,17,0,137,97,17,0,137,175,14,1
	.byte 13,17,0,137,227,34,255,254,0,0,0,0,255,43,0,0,10,17,0,138,29,14,2,59,3,6,121,50,121,30,2,59
	.byte 3,14,1,12,17,0,138,63,14,2,51,11,6,120,50,120,17,0,138,81,11,2,22,4,17,0,138,105,19,0,194,0
	.byte 0,30,0,17,0,138,139,19,0,193,0,0,176,0,19,0,193,0,0,16,0,17,0,138,145,34,255,254,0,0,0,0
	.byte 255,43,0,0,11,14,3,219,0,0,18,6,128,141,50,128,141,30,3,219,0,0,18,34,255,254,0,0,0,0,255,43
	.byte 0,0,12,6,255,254,0,0,0,0,255,43,0,0,12,14,2,2,13,34,255,254,0,0,0,0,255,43,0,0,13,34
	.byte 255,254,0,0,0,0,255,43,0,0,14,18,0,198,0,0,125,0,34,255,254,0,0,0,0,255,43,0,0,15,18,0
	.byte 202,0,1,57,0,34,255,254,0,0,0,0,255,43,0,0,16,34,255,254,0,0,0,0,255,43,0,0,17,18,0,202
	.byte 0,1,59,0,34,255,254,0,0,0,0,255,43,0,0,18,18,0,202,0,1,61,0,17,0,138,159,18,0,202,0,1
	.byte 64,0,18,0,202,0,1,65,0,18,0,202,0,1,66,0,18,0,202,0,1,67,0,34,255,254,0,0,0,0,255,43
	.byte 0,0,19,18,0,202,0,1,69,0,18,0,198,0,0,127,0,18,0,202,0,1,70,0,17,0,138,171,14,2,23,14
	.byte 17,0,138,185,6,128,147,50,128,147,14,3,219,0,0,24,6,128,148,50,128,148,30,3,219,0,0,24,16,1,30,39
	.byte 6,128,149,50,128,149,17,0,139,7,11,2,19,4,19,0,193,0,0,18,0,34,255,254,0,0,0,0,255,43,0,0
	.byte 20,34,255,254,0,0,0,0,255,43,0,0,21,18,0,202,0,1,98,0,34,255,254,0,0,0,0,255,43,0,0,22
	.byte 6,128,164,50,128,164,6,128,165,50,128,165,14,1,28,14,2,4,15,17,0,139,25,17,0,139,67,17,0,139,97,14
	.byte 1,32,17,0,139,195,17,0,139,203,11,2,18,4,11,2,25,7,14,2,5,16,34,255,254,0,0,0,0,255,43,0
	.byte 0,24,11,1,30,16,2,130,38,1,135,159,6,197,0,9,125,17,0,133,246,33,11,2,129,92,3,30,2,129,92,3
	.byte 50,30,6,30,14,2,129,92,3,14,2,84,2,17,0,129,253,50,96,6,96,50,95,6,95,50,94,6,94,14,2,128
	.byte 177,3,14,1,23,50,128,194,6,128,194,50,128,193,6,128,193,14,1,43,17,0,133,182,17,0,133,108,17,0,133,40
	.byte 4,2,130,50,1,1,2,130,146,1,16,7,138,128,135,229,19,0,194,0,0,24,0,14,2,128,178,3,14,2,129,3
	.byte 3,14,1,22,50,119,6,119,17,0,135,159,14,2,128,217,3,30,3,219,0,0,23,50,128,146,6,128,146,14,3,219
	.byte 0,0,23,50,128,145,6,128,145,14,2,129,52,3,6,198,0,1,54,34,255,254,0,0,0,0,255,43,0,0,23,14
	.byte 1,21,14,1,20,14,2,128,222,3,3,12,3,195,0,5,35,3,203,0,0,246,3,203,0,0,247,3,203,0,0,250
	.byte 7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,195,0,4,100
	.byte 3,195,0,4,163,3,195,0,4,102,3,195,0,4,110,3,203,0,0,189,3,203,0,0,192,3,203,0,1,16,3,203
	.byte 0,1,14,3,203,0,1,15,3,203,0,1,24,3,195,0,1,12,3,193,0,19,103,3,193,0,19,44,3,203,0,1
	.byte 184,3,203,0,0,117,3,195,0,5,103,3,255,254,0,0,0,0,255,43,0,0,1,7,42,108,108,118,109,95,116,104
	.byte 114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108
	.byte 105,110,101,0,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,195,0,3,253
	.byte 3,38,3,195,0,4,125,3,195,0,4,27,3,193,0,19,133,3,195,0,4,129,3,195,0,5,168,3,195,0,4,201
	.byte 3,195,0,4,127,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,7
	.byte 27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,193,0
	.byte 19,128,3,195,0,4,19,3,199,0,0,95,3,199,0,0,98,3,195,0,4,218,3,195,0,4,219,3,194,0,1,123
	.byte 3,199,0,0,126,3,195,0,4,159,3,255,254,0,0,0,0,202,0,0,131,3,255,254,0,0,0,0,202,0,0,132
	.byte 3,255,254,0,0,0,0,202,0,0,133,3,193,0,14,68,3,196,0,0,60,3,255,254,0,0,0,0,202,0,0,136
	.byte 3,195,0,4,164,3,255,254,0,0,0,0,255,43,0,0,2,3,255,254,0,0,0,0,255,43,0,0,3,7,23,109
	.byte 111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0,3,37,3,199,0,0,143,3,255
	.byte 254,0,0,0,0,255,43,0,0,4,3,200,0,0,16,3,195,0,6,185,7,26,109,111,110,111,95,104,101,108,112,101
	.byte 114,95,108,100,115,116,114,95,109,115,99,111,114,108,105,98,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99
	.byte 111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104
	.byte 114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,199,0,0,73,3,199,0,0,78,3,199,0,0,79,15,2,128
	.byte 201,3,3,195,0,5,2,3,86,3,99,3,195,0,5,7,3,195,0,3,221,3,195,0,3,230,3,209,0,0,113,3
	.byte 209,0,0,112,3,196,0,0,64,3,196,0,0,71,3,97,3,89,3,195,0,6,149,3,196,0,0,66,3,195,0,7
	.byte 51,3,193,0,16,189,3,193,0,7,38,3,195,0,4,204,3,195,0,0,123,3,195,0,6,65,3,195,0,6,28,3
	.byte 210,0,0,21,3,210,0,0,25,3,210,0,0,26,3,202,0,0,68,3,210,0,0,7,3,195,0,3,169,3,128,168
	.byte 3,195,0,4,104,3,195,0,4,24,3,195,0,4,51,3,195,0,4,26,3,195,0,4,206,3,10,3,195,0,4,114
	.byte 3,255,254,0,0,0,0,255,43,0,0,5,3,255,254,0,0,0,0,255,43,0,0,6,3,193,0,8,193,3,202,0
	.byte 0,69,3,202,0,0,56,3,255,254,0,0,0,0,255,43,0,0,7,3,255,254,0,0,0,0,202,0,1,2,3,255
	.byte 254,0,0,0,0,255,43,0,0,8,3,255,254,0,0,0,0,202,0,1,4,3,210,0,0,4,3,116,3,210,0,0
	.byte 3,3,195,0,7,32,3,203,0,2,2,3,20,3,203,0,1,1,3,255,254,0,0,0,0,255,43,0,0,9,3,255
	.byte 254,0,0,0,0,202,0,1,18,3,203,0,2,13,3,109,3,196,0,0,87,3,17,3,255,254,0,0,0,0,255,43
	.byte 0,0,10,3,203,0,2,12,3,15,3,203,0,1,202,3,203,0,1,213,3,195,0,4,101,3,195,0,4,207,3,196
	.byte 0,0,90,3,196,0,0,89,3,195,0,5,242,3,196,0,0,93,3,199,0,0,75,3,205,0,0,6,3,193,0,19
	.byte 41,3,209,0,0,115,3,123,3,196,0,0,121,3,195,0,4,14,3,209,0,0,116,3,128,143,3,255,254,0,0,0
	.byte 0,255,43,0,0,11,3,196,0,0,110,3,205,0,0,5,3,255,254,0,0,0,0,255,43,0,0,13,3,255,254,0
	.byte 0,0,0,255,43,0,0,14,3,255,254,0,0,0,0,255,43,0,0,15,3,255,254,0,0,0,0,202,0,1,56,3
	.byte 255,254,0,0,0,0,255,43,0,0,16,3,255,254,0,0,0,0,202,0,1,58,3,255,254,0,0,0,0,255,43,0
	.byte 0,17,3,255,254,0,0,0,0,202,0,1,60,3,255,254,0,0,0,0,255,43,0,0,18,3,255,254,0,0,0,0
	.byte 202,0,1,62,3,255,254,0,0,0,0,255,43,0,0,19,3,255,254,0,0,0,0,202,0,1,68,3,196,0,0,101
	.byte 3,195,0,4,103,3,11,3,196,0,0,102,3,206,0,0,165,3,206,0,0,5,3,206,0,0,178,3,206,0,0,180
	.byte 3,206,0,0,186,3,195,0,2,9,3,207,0,0,9,3,209,0,0,114,3,255,254,0,0,0,0,255,43,0,0,20
	.byte 3,255,254,0,0,0,0,255,43,0,0,21,3,255,254,0,0,0,0,255,43,0,0,22,3,255,254,0,0,0,0,202
	.byte 0,1,99,3,102,3,207,0,0,18,3,195,0,6,67,3,128,151,3,196,0,0,78,3,196,0,0,9,3,196,0,0
	.byte 80,3,128,166,3,128,161,3,196,0,0,8,3,208,0,0,12,3,199,0,0,11,3,255,254,0,0,0,0,255,43,0
	.byte 0,24,3,196,0,0,103,3,199,0,0,5,15,2,130,38,1,3,195,0,0,143,3,93,7,35,109,111,110,111,95,116
	.byte 104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,195
	.byte 0,5,145,7,32,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101
	.byte 112,116,105,111,110,0,3,193,0,16,63,3,195,0,5,144,3,203,0,1,20,7,36,109,111,110,111,95,116,104,114,101
	.byte 97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108,101,95,101,120,99,101,112,116,105,111,110,0,3,36,3,83
	.byte 3,195,0,7,29,3,195,0,6,214,3,195,0,2,170,3,195,0,4,25,3,195,0,5,234,3,195,0,4,55,3,255
	.byte 254,0,0,0,0,202,0,0,190,3,75,3,255,254,0,0,0,0,202,0,0,187,3,195,0,4,69,3,195,0,4,78
	.byte 3,195,0,4,72,3,195,0,6,219,3,62,3,195,0,4,123,3,195,0,4,105,3,195,0,4,58,3,195,0,5,205
	.byte 3,195,0,8,36,3,195,0,8,34,3,195,0,8,30,3,195,0,4,217,3,255,254,0,0,0,0,255,43,0,0,23
	.byte 3,58,3,55,3,195,0,6,51,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,33,1,0,8,4,2,130,68,1,44,48,48,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,6,57,1,2,0,129,172,128,152,129,128,129,132,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,89,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,2,121,0,16,0,0,16,0,0,2,128,147,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,6,128,173,1,2,0,136,40,133,248,135,244,135,248,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,128,206,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,128,235,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,129,12,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,255,255,255,255,255,255
	.byte 255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255
	.byte 255,255,255,255,255,255,255,4,128,196,13,8,20,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,15
	.byte 128,162,203,0,0,113,60,0,0,4,193,0,18,178,193,0,18,175,203,0,0,113,193,0,18,172,203,0,0,114,203,0
	.byte 0,131,203,0,0,125,203,0,0,124,203,0,0,123,16,203,0,0,116,203,0,0,115,203,0,0,110,203,0,0,109,203
	.byte 0,0,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,7,128,144,8,0,0,1,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,34,35,36,4,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193
	.byte 0,18,172,4,128,200,8,4,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,144,8,0,0
	.byte 1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,25,128,160,20,0,0,4,193,0,18,178,193,0,18,175
	.byte 193,0,18,174,193,0,18,172,199,0,0,96,199,0,0,97,199,0,0,101,199,0,0,102,199,0,0,102,199,0,0,101
	.byte 199,0,0,97,199,0,0,96,199,0,0,107,199,0,0,106,199,0,0,105,199,0,0,104,199,0,0,103,199,0,0,100
	.byte 199,0,0,99,56,199,0,0,94,199,0,0,93,199,0,0,92,199,0,0,91,57,60,128,160,32,0,0,4,193,0,18
	.byte 178,193,0,18,175,193,0,18,174,193,0,18,172,198,0,1,129,61,198,0,1,120,198,0,1,119,199,0,0,152,198,0
	.byte 1,117,198,0,1,116,198,0,1,115,198,0,1,114,198,0,1,113,198,0,1,112,198,0,1,111,198,0,1,110,198,0
	.byte 1,109,199,0,0,130,198,0,1,107,198,0,1,106,198,0,1,105,199,0,0,135,198,0,1,103,198,0,1,102,198,0
	.byte 1,101,198,0,1,100,198,0,1,99,198,0,1,98,198,0,1,97,198,0,1,96,198,0,1,95,198,0,1,94,198,0
	.byte 1,93,198,0,1,92,198,0,1,91,198,0,1,90,198,0,1,89,198,0,1,88,198,0,1,87,199,0,0,134,199,0
	.byte 0,132,59,199,0,0,129,199,0,0,151,199,0,0,150,199,0,0,149,199,0,0,148,199,0,0,147,199,0,0,146,199
	.byte 0,0,145,199,0,0,144,199,0,0,142,199,0,0,141,199,0,0,139,199,0,0,138,199,0,0,137,199,0,0,136,199
	.byte 0,0,133,60,21,128,162,195,0,2,8,36,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195
	.byte 0,2,4,195,0,2,9,195,0,2,20,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,70,74,195,0,6
	.byte 194,195,0,6,193,73,72,71,69,74,70,4,128,160,16,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0
	.byte 18,172,59,128,230,84,195,0,2,8,56,4,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0
	.byte 2,4,195,0,2,9,195,0,6,216,195,0,2,13,195,0,2,12,195,0,2,7,195,0,6,211,195,0,5,28,195,0
	.byte 5,27,195,0,5,26,195,0,5,235,195,0,5,217,195,0,5,218,195,0,5,208,195,0,5,219,195,0,5,220,195,0
	.byte 5,255,195,0,5,251,195,0,5,250,195,0,5,249,195,0,5,248,195,0,5,247,195,0,5,246,195,0,5,245,195,0
	.byte 5,238,195,0,5,237,195,0,5,233,195,0,5,232,195,0,5,231,195,0,5,230,195,0,5,229,195,0,5,228,195,0
	.byte 5,227,195,0,5,226,195,0,5,225,195,0,5,224,195,0,5,223,195,0,5,222,195,0,5,221,195,0,5,220,195,0
	.byte 5,219,195,0,5,218,195,0,5,217,195,0,5,216,195,0,5,215,195,0,5,214,195,0,5,213,195,0,5,212,195,0
	.byte 5,211,195,0,5,210,195,0,5,209,195,0,5,208,195,0,5,207,195,0,5,206,195,0,6,215,58,128,170,195,0,2
	.byte 8,88,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,209,0,0
	.byte 117,195,0,2,13,195,0,2,12,195,0,2,7,195,0,6,10,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6
	.byte 26,90,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,209,0
	.byte 0,115,92,91,209,0,0,114,88,195,0,6,27,195,0,6,23,195,0,6,22,195,0,6,21,195,0,6,20,195,0,6
	.byte 19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,209,0,0,118,209,0,0,119,209,0,0,120,209,0,0
	.byte 121,209,0,0,122,209,0,0,123,209,0,0,124,209,0,0,125,209,0,0,126,209,0,0,127,209,0,0,128,209,0,0
	.byte 129,199,0,0,80,199,0,0,81,199,0,0,78,199,0,0,79,199,0,0,76,199,0,0,77,199,0,0,82,199,0,0
	.byte 83,12,128,162,195,0,2,8,24,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4
	.byte 195,0,2,9,195,0,2,20,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,98,14,128,162,195,0,2,8
	.byte 24,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,195,0,2,20
	.byte 195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,101,100,195,0,7,53,41,128,162,195,0,2,8,52,0,0
	.byte 4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,195,0,6,70,195,0,2
	.byte 13,195,0,2,12,195,0,2,7,195,0,6,63,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6,26,103,104,195
	.byte 0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,195,0,6,32,195,0,6,31,195
	.byte 0,6,30,195,0,6,29,105,195,0,6,27,195,0,6,23,195,0,6,22,195,0,6,21,195,0,6,20,195,0,6,19
	.byte 195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,195,0,6,69,195,0,6,68,195,0,6,66,85,128,162,195
	.byte 0,2,8,128,152,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9
	.byte 210,0,0,8,195,0,2,13,195,0,2,12,195,0,2,7,195,0,7,190,195,0,5,28,195,0,5,27,111,195,0,6
	.byte 26,113,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,203,0,0,29,203,0,0,28,195,0,6,33,210,0
	.byte 0,6,115,114,210,0,0,5,112,203,0,0,46,195,0,6,23,195,0,6,22,195,0,6,21,195,0,6,20,195,0,6
	.byte 19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,195,0,7,194,203,0,0,55,203,0,0,54,203,0,0
	.byte 53,203,0,0,52,203,0,0,51,203,0,0,50,203,0,0,48,203,0,0,47,203,0,0,45,203,0,0,44,203,0,0
	.byte 43,203,0,0,42,203,0,0,41,203,0,0,40,203,0,0,39,203,0,0,38,203,0,0,37,203,0,0,36,203,0,0
	.byte 34,203,0,0,31,203,0,0,24,203,0,0,23,203,0,0,18,203,0,0,17,203,0,0,14,203,0,0,13,210,0,0
	.byte 9,210,0,0,10,210,0,0,11,210,0,0,12,210,0,0,13,210,0,0,14,210,0,0,15,210,0,0,16,210,0,0
	.byte 17,210,0,0,18,210,0,0,19,210,0,0,20,210,0,0,27,210,0,0,28,210,0,0,25,210,0,0,26,210,0,0
	.byte 23,210,0,0,24,210,0,0,29,210,0,0,30,58,128,226,195,0,2,8,124,4,0,4,195,0,2,18,193,0,18,175
	.byte 195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,209,0,0,117,195,0,2,13,195,0,2,12,195,0,2,7
	.byte 195,0,6,10,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6,26,128,139,195,0,6,39,195,0,6,38,195,0
	.byte 6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,128,137,209,0,0,112,128,140,209,0,0,114,128,138
	.byte 195,0,6,27,195,0,6,23,195,0,6,22,195,0,6,21,195,0,6,20,195,0,6,19,195,0,6,18,195,0,6,17
	.byte 195,0,6,16,195,0,6,15,209,0,0,118,209,0,0,119,209,0,0,120,209,0,0,121,209,0,0,122,209,0,0,123
	.byte 209,0,0,124,209,0,0,125,209,0,0,126,209,0,0,127,209,0,0,128,209,0,0,129,199,0,0,80,199,0,0,81
	.byte 199,0,0,78,199,0,0,79,199,0,0,76,199,0,0,77,199,0,0,82,199,0,0,83,58,128,170,195,0,2,8,96
	.byte 0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,209,0,0,117,195
	.byte 0,2,13,195,0,2,12,195,0,2,7,195,0,6,10,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6,26,128
	.byte 162,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,209,0,0
	.byte 115,209,0,0,112,209,0,0,113,128,159,128,160,195,0,6,27,195,0,6,23,195,0,6,22,195,0,6,21,195,0,6
	.byte 20,195,0,6,19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,209,0,0,118,209,0,0,119,209,0,0
	.byte 120,209,0,0,121,209,0,0,122,209,0,0,123,209,0,0,124,209,0,0,125,209,0,0,126,209,0,0,127,209,0,0
	.byte 128,209,0,0,129,199,0,0,80,199,0,0,81,199,0,0,78,199,0,0,79,199,0,0,76,199,0,0,77,199,0,0
	.byte 82,199,0,0,83,12,128,162,195,0,2,8,24,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172
	.byte 195,0,2,4,195,0,2,9,195,0,2,20,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,128,167,4,128
	.byte 160,28,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,19,128,162,195,0,2,8,28,0,0,4
	.byte 195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,195,0,2,20,195,0,2,13
	.byte 195,0,2,12,195,0,2,7,195,0,2,6,128,173,128,175,199,0,0,6,128,174,128,172,199,0,0,7,199,0,0,9
	.byte 199,0,0,10,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0
	.byte 0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172
	.byte 4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,16
	.byte 0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,16,0,0,4,193,0,18,178,193,0
	.byte 18,175,193,0,18,174,193,0,18,172,4,128,160,16,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18
	.byte 172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_5:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_7:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_6:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM10=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM12=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_12:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM15=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM16=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM17=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM18=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM19=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_11:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM20=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM21=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM22=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM22
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM23=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM23
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM24=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM25=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM26=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM27=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM28=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_10:

	.byte 5
	.asciz "MonoTouch_UIKit_UIResponder"

	.byte 24,16
LDIFF_SYM29=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,0,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM30=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,20,0,7
	.asciz "MonoTouch_UIKit_UIResponder"

LDIFF_SYM31=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM32=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM32
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM33=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM33
LTDIE_9:

	.byte 5
	.asciz "MonoTouch_UIKit_UIView"

	.byte 48,16
LDIFF_SYM34=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,0,6
	.asciz "__mt_BackgroundColor_var"

LDIFF_SYM35=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,24,6
	.asciz "__mt_Layer_var"

LDIFF_SYM36=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,28,6
	.asciz "__mt_Superview_var"

LDIFF_SYM37=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,32,6
	.asciz "__mt_Subviews_var"

LDIFF_SYM38=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,36,6
	.asciz "__mt_GestureRecognizers_var"

LDIFF_SYM39=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,40,6
	.asciz "__mt_TintColor_var"

LDIFF_SYM40=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIView"

LDIFF_SYM41=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_8:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewCell"

	.byte 72,16
LDIFF_SYM44=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,0,6
	.asciz "__mt_ImageView_var"

LDIFF_SYM45=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,48,6
	.asciz "__mt_TextLabel_var"

LDIFF_SYM46=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,52,6
	.asciz "__mt_DetailTextLabel_var"

LDIFF_SYM47=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,56,6
	.asciz "__mt_ContentView_var"

LDIFF_SYM48=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM49=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,64,6
	.asciz "__mt_AccessoryView_var"

LDIFF_SYM50=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,35,68,0,7
	.asciz "MonoTouch_UIKit_UITableViewCell"

LDIFF_SYM51=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM52=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM53=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_18:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM54=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM55=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM56=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM57=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_17:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM58=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM59=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM60=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM61=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_16:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM62=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM63=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM64=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM65=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_20:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM66=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM67=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM68=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM69=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM70=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_19:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM71=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM72=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM73=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM74=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM75=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM76=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_15:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM77=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM78=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM79=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM80=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM81=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM82=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM83=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM84=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM85=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM86=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM87=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM88=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM89=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_14:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM90=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM91=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM92=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM93=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM94=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM95=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_13:

	.byte 5
	.asciz "MonoTouch_Foundation_NSAction"

	.byte 52,16
LDIFF_SYM96=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSAction"

LDIFF_SYM97=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM98=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM99=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_21:

	.byte 17
	.asciz "System_Windows_Input_ICommand"

	.byte 8,7
	.asciz "System_Windows_Input_ICommand"

LDIFF_SYM100=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_4:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_Element"

	.byte 36,16
LDIFF_SYM103=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,0,6
	.asciz "_elementID"

LDIFF_SYM104=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,28,6
	.asciz "_lastAttachedCell"

LDIFF_SYM105=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,8,6
	.asciz "Tapped"

LDIFF_SYM106=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,12,6
	.asciz "_caption"

LDIFF_SYM107=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 2,35,16,6
	.asciz "_visible"

LDIFF_SYM108=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2,35,32,6
	.asciz "_selectedCommand"

LDIFF_SYM109=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 2,35,20,6
	.asciz "<ShouldDeselectAfterTouch>k__BackingField"

LDIFF_SYM110=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,33,6
	.asciz "<Parent>k__BackingField"

LDIFF_SYM111=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,24,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_Element"

LDIFF_SYM112=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM113=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM114=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_22:

	.byte 8
	.asciz "MonoTouch_UIKit_UITextAlignment"

	.byte 4
LDIFF_SYM115=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 9
	.asciz "Left"

	.byte 0,9
	.asciz "Center"

	.byte 1,9
	.asciz "Right"

	.byte 2,9
	.asciz "Justified"

	.byte 3,9
	.asciz "Natural"

	.byte 4,0,7
	.asciz "MonoTouch_UIKit_UITextAlignment"

LDIFF_SYM116=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM117=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM118=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_23:

	.byte 5
	.asciz "System_EventHandler"

	.byte 52,16
LDIFF_SYM119=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,0,0,7
	.asciz "System_EventHandler"

LDIFF_SYM120=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM121=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM122=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_3:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement"

	.byte 44,16
LDIFF_SYM123=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,0,6
	.asciz "_alignment"

LDIFF_SYM124=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,40,6
	.asciz "ValueChanged"

LDIFF_SYM125=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,35,36,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement"

LDIFF_SYM126=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM127=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM128=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_2:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement`1"

	.byte 48,16
LDIFF_SYM129=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,0,6
	.asciz "_value"

LDIFF_SYM130=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,44,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_ValueElement`1"

LDIFF_SYM131=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM131
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM132=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM133=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_24:

	.byte 8
	.asciz "MonoTouch_UIKit_UIKeyboardType"

	.byte 4
LDIFF_SYM134=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 9
	.asciz "Default"

	.byte 0,9
	.asciz "ASCIICapable"

	.byte 1,9
	.asciz "NumbersAndPunctuation"

	.byte 2,9
	.asciz "Url"

	.byte 3,9
	.asciz "NumberPad"

	.byte 4,9
	.asciz "PhonePad"

	.byte 5,9
	.asciz "NamePhonePad"

	.byte 6,9
	.asciz "EmailAddress"

	.byte 7,9
	.asciz "DecimalPad"

	.byte 8,9
	.asciz "Twitter"

	.byte 9,9
	.asciz "WebSearch"

	.byte 10,0,7
	.asciz "MonoTouch_UIKit_UIKeyboardType"

LDIFF_SYM135=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM136=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM137=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_25:

	.byte 8
	.asciz "MonoTouch_UIKit_UITextAutocapitalizationType"

	.byte 4
LDIFF_SYM138=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "Words"

	.byte 1,9
	.asciz "Sentences"

	.byte 2,9
	.asciz "AllCharacters"

	.byte 3,0,7
	.asciz "MonoTouch_UIKit_UITextAutocapitalizationType"

LDIFF_SYM139=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM140=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM141=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_26:

	.byte 8
	.asciz "MonoTouch_UIKit_UITextAutocorrectionType"

	.byte 4
LDIFF_SYM142=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM142
	.byte 9
	.asciz "Default"

	.byte 0,9
	.asciz "No"

	.byte 1,9
	.asciz "Yes"

	.byte 2,0,7
	.asciz "MonoTouch_UIKit_UITextAutocorrectionType"

LDIFF_SYM143=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM144=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM145=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_30:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM146=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_29:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM149=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM150=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM151=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM152=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM152
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM153=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM154=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM154
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM155=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM156=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM157=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM158=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM158
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM159=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM160=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM160
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM161=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM162=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM162
LTDIE_28:

	.byte 5
	.asciz "MonoTouch_UIKit_UIControl"

	.byte 52,16
LDIFF_SYM163=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 2,35,0,6
	.asciz "targets"

LDIFF_SYM164=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIControl"

LDIFF_SYM165=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM165
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM166=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM166
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM167=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_27:

	.byte 5
	.asciz "MonoTouch_UIKit_UITextField"

	.byte 68,16
LDIFF_SYM168=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,0,6
	.asciz "__mt_TextColor_var"

LDIFF_SYM169=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,52,6
	.asciz "__mt_Font_var"

LDIFF_SYM170=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,56,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM171=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,60,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM172=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UITextField"

LDIFF_SYM173=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM174=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM175=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_31:

	.byte 5
	.asciz "System_Func`1"

	.byte 52,16
LDIFF_SYM176=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,0,0,7
	.asciz "System_Func`1"

LDIFF_SYM177=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM178=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM178
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM179=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM179
LTDIE_1:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_EntryElement"

	.byte 88,16
LDIFF_SYM180=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,0,6
	.asciz "keyboardType"

LDIFF_SYM181=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,64,6
	.asciz "returnKeyType"

LDIFF_SYM182=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,68,6
	.asciz "autocapitalizationType"

LDIFF_SYM183=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,76,6
	.asciz "autocorrectionType"

LDIFF_SYM184=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,80,6
	.asciz "isPassword"

LDIFF_SYM185=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,84,6
	.asciz "_becomeResponder"

LDIFF_SYM186=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,85,6
	.asciz "_entry"

LDIFF_SYM187=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,48,6
	.asciz "Changed"

LDIFF_SYM188=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,52,6
	.asciz "ShouldReturn"

LDIFF_SYM189=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,56,6
	.asciz "_placeholder"

LDIFF_SYM190=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 2,35,60,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_EntryElement"

LDIFF_SYM191=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM192=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM192
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM193=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_0:

	.byte 5
	.asciz "OffSeasonPro_Touch_Controls_MyEntryElement"

	.byte 96,16
LDIFF_SYM194=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM194
	.byte 2,35,0,6
	.asciz "<Alignment>k__BackingField"

LDIFF_SYM195=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM195
	.byte 2,35,88,6
	.asciz "<MaxLength>k__BackingField"

LDIFF_SYM196=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,35,92,0,7
	.asciz "OffSeasonPro_Touch_Controls_MyEntryElement"

LDIFF_SYM197=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM198=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM199=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM199
	.byte 2
	.asciz "OffSeasonPro.Touch.Controls.MyEntryElement:CreateTextField"
	.long _OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF
	.long Lme_1a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM200=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 1,90,3
	.asciz "frame"

LDIFF_SYM201=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 3,123,200,0,11
	.asciz "V_0"

LDIFF_SYM202=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 1,86,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM203=Lfde0_end - Lfde0_start
	.long LDIFF_SYM203
Lfde0_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF

LDIFF_SYM204=Lme_1a - _OffSeasonPro_Touch_Controls_MyEntryElement_CreateTextField_System_Drawing_RectangleF
	.long LDIFF_SYM204
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_32:

	.byte 5
	.asciz "OffSeasonPro_Touch_DebugTrace"

	.byte 8,16
LDIFF_SYM205=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,0,0,7
	.asciz "OffSeasonPro_Touch_DebugTrace"

LDIFF_SYM206=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM206
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM207=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM207
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM208=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_33:

	.byte 8
	.asciz "Cirrious_CrossCore_Platform_MvxTraceLevel"

	.byte 4
LDIFF_SYM209=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 9
	.asciz "Diagnostic"

	.byte 0,9
	.asciz "Warning"

	.byte 1,9
	.asciz "Error"

	.byte 2,0,7
	.asciz "Cirrious_CrossCore_Platform_MvxTraceLevel"

LDIFF_SYM210=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM211=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM212=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2
	.asciz "OffSeasonPro.Touch.DebugTrace:Trace"
	.long _OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0
	.long Lme_23

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM213=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,123,36,3
	.asciz "level"

LDIFF_SYM214=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,123,40,3
	.asciz "tag"

LDIFF_SYM215=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,123,44,3
	.asciz "message"

LDIFF_SYM216=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,123,48,3
	.asciz "args"

LDIFF_SYM217=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM217
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM218=Lfde1_end - Lfde1_start
	.long LDIFF_SYM218
Lfde1_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0

LDIFF_SYM219=Lme_23 - _OffSeasonPro_Touch_DebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___0
	.long LDIFF_SYM219
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,104,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_37:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM220=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM221=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM221
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM222=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM222
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM223=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_38:

	.byte 8
	.asciz "_MvxSetupState"

	.byte 4
LDIFF_SYM224=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM224
	.byte 9
	.asciz "Uninitialized"

	.byte 0,9
	.asciz "InitializingPrimary"

	.byte 1,9
	.asciz "InitializedPrimary"

	.byte 2,9
	.asciz "InitializingSecondary"

	.byte 3,9
	.asciz "Initialized"

	.byte 4,0,7
	.asciz "_MvxSetupState"

LDIFF_SYM225=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM225
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM226=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM226
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM227=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM227
LTDIE_36:

	.byte 5
	.asciz "Cirrious_MvvmCross_Platform_MvxSetup"

	.byte 16,16
LDIFF_SYM228=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,0,6
	.asciz "StateChanged"

LDIFF_SYM229=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,8,6
	.asciz "_state"

LDIFF_SYM230=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,12,0,7
	.asciz "Cirrious_MvvmCross_Platform_MvxSetup"

LDIFF_SYM231=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM231
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM232=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM232
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM233=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM233
LTDIE_40:

	.byte 5
	.asciz "MonoTouch_UIKit_UIApplicationDelegate"

	.byte 20,16
LDIFF_SYM234=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIApplicationDelegate"

LDIFF_SYM235=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM235
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM236=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM236
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM237=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM237
LTDIE_41:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM238=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM239=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM240=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM241=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_39:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate"

	.byte 24,16
LDIFF_SYM242=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM242
	.byte 2,35,0,6
	.asciz "LifetimeChanged"

LDIFF_SYM243=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate"

LDIFF_SYM244=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM244
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM245=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM245
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM246=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_42:

	.byte 5
	.asciz "MonoTouch_UIKit_UIWindow"

	.byte 52,16
LDIFF_SYM247=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,0,6
	.asciz "__mt_RootViewController_var"

LDIFF_SYM248=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIWindow"

LDIFF_SYM249=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM249
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM250=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM250
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM251=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM251
LTDIE_43:

	.byte 17
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter"

LDIFF_SYM252=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM252
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM253=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM253
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM254=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_35:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup"

	.byte 28,16
LDIFF_SYM255=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 2,35,0,6
	.asciz "_applicationDelegate"

LDIFF_SYM256=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,16,6
	.asciz "_window"

LDIFF_SYM257=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,20,6
	.asciz "_presenter"

LDIFF_SYM258=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,35,24,0,7
	.asciz "Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup"

LDIFF_SYM259=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM259
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM260=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM260
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM261=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM261
LTDIE_46:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter"

	.byte 8,16
LDIFF_SYM262=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM262
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter"

LDIFF_SYM263=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM264=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM265=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM265
LTDIE_48:

	.byte 5
	.asciz "MonoTouch_UIKit_UIViewController"

	.byte 48,16
LDIFF_SYM266=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM266
	.byte 2,35,0,6
	.asciz "__mt_View_var"

LDIFF_SYM267=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM267
	.byte 2,35,24,6
	.asciz "__mt_ParentViewController_var"

LDIFF_SYM268=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM268
	.byte 2,35,28,6
	.asciz "__mt_NavigationItem_var"

LDIFF_SYM269=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,32,6
	.asciz "__mt_NavigationController_var"

LDIFF_SYM270=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,36,6
	.asciz "__mt_ToolbarItems_var"

LDIFF_SYM271=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,40,6
	.asciz "__mt_ChildViewControllers_var"

LDIFF_SYM272=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIViewController"

LDIFF_SYM273=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM273
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM274=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM275=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_47:

	.byte 5
	.asciz "MonoTouch_UIKit_UINavigationController"

	.byte 60,16
LDIFF_SYM276=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,35,0,6
	.asciz "__mt_ViewControllers_var"

LDIFF_SYM277=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,48,6
	.asciz "__mt_NavigationBar_var"

LDIFF_SYM278=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,52,6
	.asciz "__mt_Toolbar_var"

LDIFF_SYM279=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM279
	.byte 2,35,56,0,7
	.asciz "MonoTouch_UIKit_UINavigationController"

LDIFF_SYM280=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM281=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM281
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM282=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM282
LTDIE_45:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter"

	.byte 20,16
LDIFF_SYM283=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,35,0,6
	.asciz "_applicationDelegate"

LDIFF_SYM284=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,35,8,6
	.asciz "_window"

LDIFF_SYM285=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,35,12,6
	.asciz "<MasterNavigationController>k__BackingField"

LDIFF_SYM286=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter"

LDIFF_SYM287=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM287
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM288=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM288
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM289=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM289
LTDIE_44:

	.byte 5
	.asciz "OffSeasonPro_Touch_OffSeasonProPresenter"

	.byte 20,16
LDIFF_SYM290=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,0,0,7
	.asciz "OffSeasonPro_Touch_OffSeasonProPresenter"

LDIFF_SYM291=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM291
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM292=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM292
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM293=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM293
LTDIE_34:

	.byte 5
	.asciz "OffSeasonPro_Touch_Setup"

	.byte 32,16
LDIFF_SYM294=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,0,6
	.asciz "_presenter"

LDIFF_SYM295=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,28,0,7
	.asciz "OffSeasonPro_Touch_Setup"

LDIFF_SYM296=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM296
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM297=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM297
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM298=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM298
LTDIE_49:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM299=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM299
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM300=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM301=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM302=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM303=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM303
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM304=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM304
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM305=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2
	.asciz "OffSeasonPro.Touch.Setup:CreateApp"
	.long _OffSeasonPro_Touch_Setup_CreateApp_0
	.long Lme_3a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM306=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 0,11
	.asciz "V_0"

LDIFF_SYM307=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 1,90,11
	.asciz "V_1"

LDIFF_SYM308=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM308
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM309=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 2,123,0,11
	.asciz "V_3"

LDIFF_SYM310=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 2,123,4,11
	.asciz "V_4"

LDIFF_SYM311=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 1,86,11
	.asciz "V_5"

LDIFF_SYM312=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM312
	.byte 1,85,11
	.asciz "V_6"

LDIFF_SYM313=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 1,84,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM314=Lfde2_end - Lfde2_start
	.long LDIFF_SYM314
Lfde2_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Setup_CreateApp_0

LDIFF_SYM315=Lme_3a - _OffSeasonPro_Touch_Setup_CreateApp_0
	.long LDIFF_SYM315
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,104,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_51:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewSource"

	.byte 20,16
LDIFF_SYM316=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewSource"

LDIFF_SYM317=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM317
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM318=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM318
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM319=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM319
LTDIE_52:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM320=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM320
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM321=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM322=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM323=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM324=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM324
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM325=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM325
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM326=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_53:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM327=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM327
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM328=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM329=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM329
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM330=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM330
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM331=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM331
LTDIE_50:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_BackgroundSource"

	.byte 36,16
LDIFF_SYM332=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM332
	.byte 2,35,0,6
	.asciz "<Rows>k__BackingField"

LDIFF_SYM333=LTDIE_52_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM333
	.byte 2,35,20,6
	.asciz "<FontSize>k__BackingField"

LDIFF_SYM334=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM334
	.byte 2,35,24,6
	.asciz "<ImageViewSize>k__BackingField"

LDIFF_SYM335=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM335
	.byte 2,35,28,0,7
	.asciz "OffSeasonPro_Touch_Views_BackgroundSource"

LDIFF_SYM336=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM336
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM337=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM337
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM338=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM338
LTDIE_55:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollView"

	.byte 52,16
LDIFF_SYM339=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM339
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM340=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM340
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIScrollView"

LDIFF_SYM341=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM341
LTDIE_55_POINTER:

	.byte 13
LDIFF_SYM342=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM342
LTDIE_55_REFERENCE:

	.byte 14
LDIFF_SYM343=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM343
LTDIE_54:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionView"

	.byte 68,16
LDIFF_SYM344=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM344
	.byte 2,35,0,6
	.asciz "__mt_CollectionViewLayout_var"

LDIFF_SYM345=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM345
	.byte 2,35,52,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM346=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM346
	.byte 2,35,56,6
	.asciz "__mt_WeakDataSource_var"

LDIFF_SYM347=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM347
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM348=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM348
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UICollectionView"

LDIFF_SYM349=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM349
LTDIE_54_POINTER:

	.byte 13
LDIFF_SYM350=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM350
LTDIE_54_REFERENCE:

	.byte 14
LDIFF_SYM351=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM351
LTDIE_56:

	.byte 5
	.asciz "MonoTouch_Foundation_NSIndexPath"

	.byte 20,16
LDIFF_SYM352=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM352
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSIndexPath"

LDIFF_SYM353=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM353
LTDIE_56_POINTER:

	.byte 13
LDIFF_SYM354=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM354
LTDIE_56_REFERENCE:

	.byte 14
LDIFF_SYM355=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM355
LTDIE_59:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionReusableView"

	.byte 48,16
LDIFF_SYM356=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM356
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionReusableView"

LDIFF_SYM357=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM357
LTDIE_59_POINTER:

	.byte 13
LDIFF_SYM358=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM358
LTDIE_59_REFERENCE:

	.byte 14
LDIFF_SYM359=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM359
LTDIE_58:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewCell"

	.byte 52,16
LDIFF_SYM360=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM360
	.byte 2,35,0,6
	.asciz "__mt_ContentView_var"

LDIFF_SYM361=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM361
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewCell"

LDIFF_SYM362=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM362
LTDIE_58_POINTER:

	.byte 13
LDIFF_SYM363=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM363
LTDIE_58_REFERENCE:

	.byte 14
LDIFF_SYM364=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM364
LTDIE_60:

	.byte 5
	.asciz "MonoTouch_UIKit_UIImageView"

	.byte 52,16
LDIFF_SYM365=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM365
	.byte 2,35,0,6
	.asciz "__mt_Image_var"

LDIFF_SYM366=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIImageView"

LDIFF_SYM367=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM367
LTDIE_60_POINTER:

	.byte 13
LDIFF_SYM368=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM368
LTDIE_60_REFERENCE:

	.byte 14
LDIFF_SYM369=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM369
LTDIE_57:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_BackgroundImageCell"

	.byte 56,16
LDIFF_SYM370=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM370
	.byte 2,35,0,6
	.asciz "<ImageView>k__BackingField"

LDIFF_SYM371=LTDIE_60_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM371
	.byte 2,35,52,0,7
	.asciz "OffSeasonPro_Touch_Views_BackgroundImageCell"

LDIFF_SYM372=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM372
LTDIE_57_POINTER:

	.byte 13
LDIFF_SYM373=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM373
LTDIE_57_REFERENCE:

	.byte 14
LDIFF_SYM374=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM374
LTDIE_62:

	.byte 5
	.asciz "MonoTouch_UIKit_UIImage"

	.byte 20,16
LDIFF_SYM375=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM375
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIImage"

LDIFF_SYM376=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM376
LTDIE_62_POINTER:

	.byte 13
LDIFF_SYM377=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM377
LTDIE_62_REFERENCE:

	.byte 14
LDIFF_SYM378=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM378
LTDIE_61:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_BackgroundImageElement"

	.byte 16,16
LDIFF_SYM379=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM379
	.byte 2,35,0,6
	.asciz "<Image>k__BackingField"

LDIFF_SYM380=LTDIE_62_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM380
	.byte 2,35,8,6
	.asciz "<Tapped>k__BackingField"

LDIFF_SYM381=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM381
	.byte 2,35,12,0,7
	.asciz "OffSeasonPro_Touch_Views_BackgroundImageElement"

LDIFF_SYM382=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM382
LTDIE_61_POINTER:

	.byte 13
LDIFF_SYM383=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM383
LTDIE_61_REFERENCE:

	.byte 14
LDIFF_SYM384=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM384
	.byte 2
	.asciz "OffSeasonPro.Touch.Views.BackgroundSource:GetCell"
	.long _OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.long Lme_49

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM385=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM385
	.byte 2,123,36,3
	.asciz "collectionView"

LDIFF_SYM386=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM386
	.byte 1,86,3
	.asciz "indexPath"

LDIFF_SYM387=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM387
	.byte 2,123,40,11
	.asciz "V_0"

LDIFF_SYM388=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM388
	.byte 1,84,11
	.asciz "V_1"

LDIFF_SYM389=LTDIE_61_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM389
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM390=Lfde3_end - Lfde3_start
	.long LDIFF_SYM390
Lfde3_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath

LDIFF_SYM391=Lme_49 - _OffSeasonPro_Touch_Views_BackgroundSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.long LDIFF_SYM391
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "OffSeasonPro.Touch.Views.BackgroundImageCell:.ctor"
	.long _OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF
	.long Lme_4f

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM392=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM392
	.byte 1,90,3
	.asciz "frame"

LDIFF_SYM393=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM393
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM394=Lfde4_end - Lfde4_start
	.long LDIFF_SYM394
Lfde4_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF

LDIFF_SYM395=Lme_4f - _OffSeasonPro_Touch_Views_BackgroundImageCell__ctor_System_Drawing_RectangleF
	.long LDIFF_SYM395
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "OffSeasonPro.Touch.Views.BackgroundImageCell:UpdateRow"
	.long _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF
	.long Lme_52

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM396=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM396
	.byte 2,123,56,3
	.asciz "element"

LDIFF_SYM397=LTDIE_61_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM397
	.byte 1,90,3
	.asciz "fontSize"

LDIFF_SYM398=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM398
	.byte 0,3
	.asciz "imageViewSize"

LDIFF_SYM399=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM399
	.byte 3,123,192,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM400=Lfde5_end - Lfde5_start
	.long LDIFF_SYM400
Lfde5_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF

LDIFF_SYM401=Lme_52 - _OffSeasonPro_Touch_Views_BackgroundImageCell_UpdateRow_OffSeasonPro_Touch_Views_BackgroundImageElement_single_System_Drawing_SizeF
	.long LDIFF_SYM401
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,104,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_66:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM402=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM402
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM403=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM403
LTDIE_66_POINTER:

	.byte 13
LDIFF_SYM404=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM404
LTDIE_66_REFERENCE:

	.byte 14
LDIFF_SYM405=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM405
LTDIE_65:

	.byte 5
	.asciz "Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController"

	.byte 72,16
LDIFF_SYM406=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM406
	.byte 2,35,0,6
	.asciz "ViewDidLoadCalled"

LDIFF_SYM407=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM407
	.byte 2,35,48,6
	.asciz "ViewWillAppearCalled"

LDIFF_SYM408=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM408
	.byte 2,35,52,6
	.asciz "ViewDidAppearCalled"

LDIFF_SYM409=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM409
	.byte 2,35,56,6
	.asciz "ViewDidDisappearCalled"

LDIFF_SYM410=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM410
	.byte 2,35,60,6
	.asciz "ViewWillDisappearCalled"

LDIFF_SYM411=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM411
	.byte 2,35,64,6
	.asciz "DisposeCalled"

LDIFF_SYM412=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM412
	.byte 2,35,68,0,7
	.asciz "Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController"

LDIFF_SYM413=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM413
LTDIE_65_POINTER:

	.byte 13
LDIFF_SYM414=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM414
LTDIE_65_REFERENCE:

	.byte 14
LDIFF_SYM415=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM415
LTDIE_68:

	.byte 17
	.asciz "System_Collections_Generic_IDictionary`2"

	.byte 8,7
	.asciz "System_Collections_Generic_IDictionary`2"

LDIFF_SYM416=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM416
LTDIE_68_POINTER:

	.byte 13
LDIFF_SYM417=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM417
LTDIE_68_REFERENCE:

	.byte 14
LDIFF_SYM418=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM418
LTDIE_70:

	.byte 8
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

	.byte 4
LDIFF_SYM419=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM419
	.byte 9
	.asciz "Unknown"

	.byte 0,9
	.asciz "UserAction"

	.byte 1,9
	.asciz "Bookmark"

	.byte 2,9
	.asciz "AutomatedService"

	.byte 3,9
	.asciz "Other"

	.byte 4,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

LDIFF_SYM420=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM420
LTDIE_70_POINTER:

	.byte 13
LDIFF_SYM421=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM421
LTDIE_70_REFERENCE:

	.byte 14
LDIFF_SYM422=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM422
LTDIE_69:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

	.byte 16,16
LDIFF_SYM423=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM423
	.byte 2,35,0,6
	.asciz "<Type>k__BackingField"

LDIFF_SYM424=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM424
	.byte 2,35,12,6
	.asciz "<AdditionalInfo>k__BackingField"

LDIFF_SYM425=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM425
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

LDIFF_SYM426=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM426
LTDIE_69_POINTER:

	.byte 13
LDIFF_SYM427=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM427
LTDIE_69_REFERENCE:

	.byte 14
LDIFF_SYM428=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM428
LTDIE_67:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

	.byte 24,16
LDIFF_SYM429=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM429
	.byte 2,35,0,6
	.asciz "<ViewModelType>k__BackingField"

LDIFF_SYM430=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM430
	.byte 2,35,8,6
	.asciz "<ParameterValues>k__BackingField"

LDIFF_SYM431=LTDIE_68_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM431
	.byte 2,35,12,6
	.asciz "<PresentationValues>k__BackingField"

LDIFF_SYM432=LTDIE_68_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM432
	.byte 2,35,16,6
	.asciz "<RequestedBy>k__BackingField"

LDIFF_SYM433=LTDIE_69_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM433
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

LDIFF_SYM434=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM434
LTDIE_67_POINTER:

	.byte 13
LDIFF_SYM435=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM435
LTDIE_67_REFERENCE:

	.byte 14
LDIFF_SYM436=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM436
LTDIE_71:

	.byte 17
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

LDIFF_SYM437=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM437
LTDIE_71_POINTER:

	.byte 13
LDIFF_SYM438=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM438
LTDIE_71_REFERENCE:

	.byte 14
LDIFF_SYM439=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM439
LTDIE_64:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Views_MvxViewController"

	.byte 80,16
LDIFF_SYM440=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM440
	.byte 2,35,0,6
	.asciz "<Request>k__BackingField"

LDIFF_SYM441=LTDIE_67_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM441
	.byte 2,35,72,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM442=LTDIE_71_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM442
	.byte 2,35,76,0,7
	.asciz "Cirrious_MvvmCross_Touch_Views_MvxViewController"

LDIFF_SYM443=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM443
LTDIE_64_POINTER:

	.byte 13
LDIFF_SYM444=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM444
LTDIE_64_REFERENCE:

	.byte 14
LDIFF_SYM445=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM445
LTDIE_63:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_BackgroundChooserView"

	.byte 88,16
LDIFF_SYM446=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM446
	.byte 2,35,0,6
	.asciz "_backgroundSource"

LDIFF_SYM447=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM447
	.byte 2,35,80,6
	.asciz "_collectionViewUser"

LDIFF_SYM448=LTDIE_54_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM448
	.byte 2,35,84,0,7
	.asciz "OffSeasonPro_Touch_Views_BackgroundChooserView"

LDIFF_SYM449=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM449
LTDIE_63_POINTER:

	.byte 13
LDIFF_SYM450=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM450
LTDIE_63_REFERENCE:

	.byte 14
LDIFF_SYM451=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM451
LTDIE_73:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewLayout"

	.byte 20,16
LDIFF_SYM452=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM452
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewLayout"

LDIFF_SYM453=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM453
LTDIE_73_POINTER:

	.byte 13
LDIFF_SYM454=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM454
LTDIE_73_REFERENCE:

	.byte 14
LDIFF_SYM455=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM455
LTDIE_72:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewFlowLayout"

	.byte 20,16
LDIFF_SYM456=LTDIE_73 - Ldebug_info_start
	.long LDIFF_SYM456
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewFlowLayout"

LDIFF_SYM457=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM457
LTDIE_72_POINTER:

	.byte 13
LDIFF_SYM458=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM458
LTDIE_72_REFERENCE:

	.byte 14
LDIFF_SYM459=LTDIE_72 - Ldebug_info_start
	.long LDIFF_SYM459
LTDIE_74:

	.byte 5
	.asciz "_<ViewDidLoad>c__AnonStorey8"

	.byte 16,16
LDIFF_SYM460=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM460
	.byte 2,35,0,6
	.asciz "bg"

LDIFF_SYM461=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM461
	.byte 2,35,8,6
	.asciz "$this"

LDIFF_SYM462=LTDIE_63_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM462
	.byte 2,35,12,0,7
	.asciz "_<ViewDidLoad>c__AnonStorey8"

LDIFF_SYM463=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM463
LTDIE_74_POINTER:

	.byte 13
LDIFF_SYM464=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM464
LTDIE_74_REFERENCE:

	.byte 14
LDIFF_SYM465=LTDIE_74 - Ldebug_info_start
	.long LDIFF_SYM465
LTDIE_75:

	.byte 5
	.asciz "MonoTouch_UIKit_UIButton"

	.byte 56,16
LDIFF_SYM466=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM466
	.byte 2,35,0,6
	.asciz "__mt_Font_var"

LDIFF_SYM467=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM467
	.byte 2,35,52,0,7
	.asciz "MonoTouch_UIKit_UIButton"

LDIFF_SYM468=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM468
LTDIE_75_POINTER:

	.byte 13
LDIFF_SYM469=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM469
LTDIE_75_REFERENCE:

	.byte 14
LDIFF_SYM470=LTDIE_75 - Ldebug_info_start
	.long LDIFF_SYM470
LTDIE_77:

	.byte 5
	.asciz "MonoTouch_UIKit_UIFont"

	.byte 20,16
LDIFF_SYM471=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM471
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIFont"

LDIFF_SYM472=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM472
LTDIE_77_POINTER:

	.byte 13
LDIFF_SYM473=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM473
LTDIE_77_REFERENCE:

	.byte 14
LDIFF_SYM474=LTDIE_77 - Ldebug_info_start
	.long LDIFF_SYM474
LTDIE_78:

	.byte 5
	.asciz "MonoTouch_UIKit_UIColor"

	.byte 20,16
LDIFF_SYM475=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM475
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIColor"

LDIFF_SYM476=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM476
LTDIE_78_POINTER:

	.byte 13
LDIFF_SYM477=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM477
LTDIE_78_REFERENCE:

	.byte 14
LDIFF_SYM478=LTDIE_78 - Ldebug_info_start
	.long LDIFF_SYM478
LTDIE_76:

	.byte 5
	.asciz "MonoTouch_UIKit_UITextAttributes"

	.byte 28,16
LDIFF_SYM479=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM479
	.byte 2,35,0,6
	.asciz "Font"

LDIFF_SYM480=LTDIE_77_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM480
	.byte 2,35,8,6
	.asciz "TextColor"

LDIFF_SYM481=LTDIE_78_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM481
	.byte 2,35,12,6
	.asciz "TextShadowColor"

LDIFF_SYM482=LTDIE_78_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM482
	.byte 2,35,16,6
	.asciz "TextShadowOffset"

LDIFF_SYM483=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM483
	.byte 2,35,20,0,7
	.asciz "MonoTouch_UIKit_UITextAttributes"

LDIFF_SYM484=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM484
LTDIE_76_POINTER:

	.byte 13
LDIFF_SYM485=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM485
LTDIE_76_REFERENCE:

	.byte 14
LDIFF_SYM486=LTDIE_76 - Ldebug_info_start
	.long LDIFF_SYM486
LTDIE_80:

	.byte 5
	.asciz "MonoTouch_UIKit_UIBarItem"

	.byte 20,16
LDIFF_SYM487=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM487
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIBarItem"

LDIFF_SYM488=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM488
LTDIE_80_POINTER:

	.byte 13
LDIFF_SYM489=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM489
LTDIE_80_REFERENCE:

	.byte 14
LDIFF_SYM490=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM490
LTDIE_81:

	.byte 5
	.asciz "_Callback"

	.byte 24,16
LDIFF_SYM491=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM491
	.byte 2,35,0,6
	.asciz "container"

LDIFF_SYM492=LTDIE_79_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM492
	.byte 2,35,20,0,7
	.asciz "_Callback"

LDIFF_SYM493=LTDIE_81 - Ldebug_info_start
	.long LDIFF_SYM493
LTDIE_81_POINTER:

	.byte 13
LDIFF_SYM494=LTDIE_81 - Ldebug_info_start
	.long LDIFF_SYM494
LTDIE_81_REFERENCE:

	.byte 14
LDIFF_SYM495=LTDIE_81 - Ldebug_info_start
	.long LDIFF_SYM495
LTDIE_79:

	.byte 5
	.asciz "MonoTouch_UIKit_UIBarButtonItem"

	.byte 40,16
LDIFF_SYM496=LTDIE_80 - Ldebug_info_start
	.long LDIFF_SYM496
	.byte 2,35,0,6
	.asciz "clicked"

LDIFF_SYM497=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM497
	.byte 2,35,20,6
	.asciz "callback"

LDIFF_SYM498=LTDIE_81_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM498
	.byte 2,35,24,6
	.asciz "__mt_CustomView_var"

LDIFF_SYM499=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM499
	.byte 2,35,28,6
	.asciz "__mt_Target_var"

LDIFF_SYM500=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM500
	.byte 2,35,32,6
	.asciz "__mt_TintColor_var"

LDIFF_SYM501=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM501
	.byte 2,35,36,0,7
	.asciz "MonoTouch_UIKit_UIBarButtonItem"

LDIFF_SYM502=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM502
LTDIE_79_POINTER:

	.byte 13
LDIFF_SYM503=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM503
LTDIE_79_REFERENCE:

	.byte 14
LDIFF_SYM504=LTDIE_79 - Ldebug_info_start
	.long LDIFF_SYM504
	.byte 2
	.asciz "OffSeasonPro.Touch.Views.BackgroundChooserView:ViewDidLoad"
	.long _OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad
	.long Lme_57

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM505=LTDIE_63_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM505
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM506=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM506
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM507=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM507
	.byte 2,123,16,11
	.asciz "V_2"

LDIFF_SYM508=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM508
	.byte 2,123,32,11
	.asciz "V_3"

LDIFF_SYM509=LTDIE_72_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM509
	.byte 1,85,11
	.asciz "V_4"

LDIFF_SYM510=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM510
	.byte 1,84,11
	.asciz "V_5"

LDIFF_SYM511=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM511
	.byte 2,123,48,11
	.asciz "V_6"

LDIFF_SYM512=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM512
	.byte 2,123,52,11
	.asciz "V_7"

LDIFF_SYM513=LTDIE_74_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM513
	.byte 3,123,196,0,11
	.asciz "V_8"

LDIFF_SYM514=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM514
	.byte 3,123,200,0,11
	.asciz "V_9"

LDIFF_SYM515=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM515
	.byte 3,123,204,0,11
	.asciz "V_10"

LDIFF_SYM516=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM516
	.byte 3,123,220,0,11
	.asciz "V_11"

LDIFF_SYM517=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM517
	.byte 3,123,236,0,11
	.asciz "V_12"

LDIFF_SYM518=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM518
	.byte 3,123,252,0,11
	.asciz "V_13"

LDIFF_SYM519=LTDIE_76_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM519
	.byte 3,123,140,1,11
	.asciz "V_14"

LDIFF_SYM520=LTDIE_79_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM520
	.byte 3,123,144,1,11
	.asciz "V_15"

LDIFF_SYM521=LTDIE_79_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM521
	.byte 3,123,148,1,11
	.asciz "V_16"

LDIFF_SYM522=LTDIE_79_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM522
	.byte 3,123,152,1,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM523=Lfde6_end - Lfde6_start
	.long LDIFF_SYM523
Lfde6_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad

LDIFF_SYM524=Lme_57 - _OffSeasonPro_Touch_Views_BackgroundChooserView_ViewDidLoad
	.long LDIFF_SYM524
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,144,4,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_86:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewController"

	.byte 52,16
LDIFF_SYM525=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM525
	.byte 2,35,0,6
	.asciz "__mt_TableView_var"

LDIFF_SYM526=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM526
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UITableViewController"

LDIFF_SYM527=LTDIE_86 - Ldebug_info_start
	.long LDIFF_SYM527
LTDIE_86_POINTER:

	.byte 13
LDIFF_SYM528=LTDIE_86 - Ldebug_info_start
	.long LDIFF_SYM528
LTDIE_86_REFERENCE:

	.byte 14
LDIFF_SYM529=LTDIE_86 - Ldebug_info_start
	.long LDIFF_SYM529
LTDIE_87:

	.byte 8
	.asciz "MonoTouch_UIKit_UITableViewStyle"

	.byte 4
LDIFF_SYM530=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM530
	.byte 9
	.asciz "Plain"

	.byte 0,9
	.asciz "Grouped"

	.byte 1,0,7
	.asciz "MonoTouch_UIKit_UITableViewStyle"

LDIFF_SYM531=LTDIE_87 - Ldebug_info_start
	.long LDIFF_SYM531
LTDIE_87_POINTER:

	.byte 13
LDIFF_SYM532=LTDIE_87 - Ldebug_info_start
	.long LDIFF_SYM532
LTDIE_87_REFERENCE:

	.byte 14
LDIFF_SYM533=LTDIE_87 - Ldebug_info_start
	.long LDIFF_SYM533
LTDIE_88:

	.byte 5
	.asciz "System_Action`1"

	.byte 52,16
LDIFF_SYM534=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM534
	.byte 2,35,0,0,7
	.asciz "System_Action`1"

LDIFF_SYM535=LTDIE_88 - Ldebug_info_start
	.long LDIFF_SYM535
LTDIE_88_POINTER:

	.byte 13
LDIFF_SYM536=LTDIE_88 - Ldebug_info_start
	.long LDIFF_SYM536
LTDIE_88_REFERENCE:

	.byte 14
LDIFF_SYM537=LTDIE_88 - Ldebug_info_start
	.long LDIFF_SYM537
LTDIE_89:

	.byte 5
	.asciz "MonoTouch_UIKit_UISearchBar"

	.byte 56,16
LDIFF_SYM538=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM538
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM539=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM539
	.byte 2,35,48,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM540=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM540
	.byte 2,35,52,0,7
	.asciz "MonoTouch_UIKit_UISearchBar"

LDIFF_SYM541=LTDIE_89 - Ldebug_info_start
	.long LDIFF_SYM541
LTDIE_89_POINTER:

	.byte 13
LDIFF_SYM542=LTDIE_89 - Ldebug_info_start
	.long LDIFF_SYM542
LTDIE_89_REFERENCE:

	.byte 14
LDIFF_SYM543=LTDIE_89 - Ldebug_info_start
	.long LDIFF_SYM543
LTDIE_90:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableView"

	.byte 68,16
LDIFF_SYM544=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM544
	.byte 2,35,0,6
	.asciz "__mt_WeakDataSource_var"

LDIFF_SYM545=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM545
	.byte 2,35,52,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM546=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM546
	.byte 2,35,56,6
	.asciz "__mt_TableHeaderView_var"

LDIFF_SYM547=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM547
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM548=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM548
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UITableView"

LDIFF_SYM549=LTDIE_90 - Ldebug_info_start
	.long LDIFF_SYM549
LTDIE_90_POINTER:

	.byte 13
LDIFF_SYM550=LTDIE_90 - Ldebug_info_start
	.long LDIFF_SYM550
LTDIE_90_REFERENCE:

	.byte 14
LDIFF_SYM551=LTDIE_90 - Ldebug_info_start
	.long LDIFF_SYM551
LTDIE_92:

	.byte 5
	.asciz "MonoTouch_UIKit_UIActivityIndicatorView"

	.byte 48,16
LDIFF_SYM552=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM552
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIActivityIndicatorView"

LDIFF_SYM553=LTDIE_92 - Ldebug_info_start
	.long LDIFF_SYM553
LTDIE_92_POINTER:

	.byte 13
LDIFF_SYM554=LTDIE_92 - Ldebug_info_start
	.long LDIFF_SYM554
LTDIE_92_REFERENCE:

	.byte 14
LDIFF_SYM555=LTDIE_92 - Ldebug_info_start
	.long LDIFF_SYM555
LTDIE_93:

	.byte 5
	.asciz "MonoTouch_UIKit_UILabel"

	.byte 60,16
LDIFF_SYM556=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM556
	.byte 2,35,0,6
	.asciz "__mt_Font_var"

LDIFF_SYM557=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM557
	.byte 2,35,48,6
	.asciz "__mt_TextColor_var"

LDIFF_SYM558=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM558
	.byte 2,35,52,6
	.asciz "__mt_ShadowColor_var"

LDIFF_SYM559=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM559
	.byte 2,35,56,0,7
	.asciz "MonoTouch_UIKit_UILabel"

LDIFF_SYM560=LTDIE_93 - Ldebug_info_start
	.long LDIFF_SYM560
LTDIE_93_POINTER:

	.byte 13
LDIFF_SYM561=LTDIE_93 - Ldebug_info_start
	.long LDIFF_SYM561
LTDIE_93_REFERENCE:

	.byte 14
LDIFF_SYM562=LTDIE_93 - Ldebug_info_start
	.long LDIFF_SYM562
LTDIE_94:

	.byte 8
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshViewStatus"

	.byte 4
LDIFF_SYM563=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM563
	.byte 9
	.asciz "ReleaseToReload"

	.byte 0,9
	.asciz "PullToReload"

	.byte 1,9
	.asciz "Loading"

	.byte 2,0,7
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshViewStatus"

LDIFF_SYM564=LTDIE_94 - Ldebug_info_start
	.long LDIFF_SYM564
LTDIE_94_POINTER:

	.byte 13
LDIFF_SYM565=LTDIE_94 - Ldebug_info_start
	.long LDIFF_SYM565
LTDIE_94_REFERENCE:

	.byte 14
LDIFF_SYM566=LTDIE_94 - Ldebug_info_start
	.long LDIFF_SYM566
LTDIE_91:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshTableHeaderView"

	.byte 80,16
LDIFF_SYM567=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM567
	.byte 2,35,0,6
	.asciz "Activity"

LDIFF_SYM568=LTDIE_92_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM568
	.byte 2,35,48,6
	.asciz "LastUpdateLabel"

LDIFF_SYM569=LTDIE_93_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM569
	.byte 2,35,52,6
	.asciz "StatusLabel"

LDIFF_SYM570=LTDIE_93_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM570
	.byte 2,35,56,6
	.asciz "ArrowView"

LDIFF_SYM571=LTDIE_60_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM571
	.byte 2,35,60,6
	.asciz "status"

LDIFF_SYM572=LTDIE_94 - Ldebug_info_start
	.long LDIFF_SYM572
	.byte 2,35,64,6
	.asciz "IsFlipped"

LDIFF_SYM573=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM573
	.byte 2,35,68,6
	.asciz "lastUpdateTime"

LDIFF_SYM574=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM574
	.byte 2,35,72,0,7
	.asciz "CrossUI_Touch_Dialog_Utilities_RefreshTableHeaderView"

LDIFF_SYM575=LTDIE_91 - Ldebug_info_start
	.long LDIFF_SYM575
LTDIE_91_POINTER:

	.byte 13
LDIFF_SYM576=LTDIE_91 - Ldebug_info_start
	.long LDIFF_SYM576
LTDIE_91_REFERENCE:

	.byte 14
LDIFF_SYM577=LTDIE_91 - Ldebug_info_start
	.long LDIFF_SYM577
LTDIE_96:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM578=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM578
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM579=LTDIE_96 - Ldebug_info_start
	.long LDIFF_SYM579
LTDIE_96_POINTER:

	.byte 13
LDIFF_SYM580=LTDIE_96 - Ldebug_info_start
	.long LDIFF_SYM580
LTDIE_96_REFERENCE:

	.byte 14
LDIFF_SYM581=LTDIE_96 - Ldebug_info_start
	.long LDIFF_SYM581
LTDIE_97:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_Group"

	.byte 12,16
LDIFF_SYM582=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM582
	.byte 2,35,0,6
	.asciz "<Key>k__BackingField"

LDIFF_SYM583=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM583
	.byte 2,35,8,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_Group"

LDIFF_SYM584=LTDIE_97 - Ldebug_info_start
	.long LDIFF_SYM584
LTDIE_97_POINTER:

	.byte 13
LDIFF_SYM585=LTDIE_97 - Ldebug_info_start
	.long LDIFF_SYM585
LTDIE_97_REFERENCE:

	.byte 14
LDIFF_SYM586=LTDIE_97 - Ldebug_info_start
	.long LDIFF_SYM586
LTDIE_98:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM587=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM587
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM588=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM588
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM589=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM589
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM590=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM590
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM591=LTDIE_98 - Ldebug_info_start
	.long LDIFF_SYM591
LTDIE_98_POINTER:

	.byte 13
LDIFF_SYM592=LTDIE_98 - Ldebug_info_start
	.long LDIFF_SYM592
LTDIE_98_REFERENCE:

	.byte 14
LDIFF_SYM593=LTDIE_98 - Ldebug_info_start
	.long LDIFF_SYM593
LTDIE_95:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_Elements_RootElement"

	.byte 68,16
LDIFF_SYM594=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM594
	.byte 2,35,0,6
	.asciz "_summarySection"

LDIFF_SYM595=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM595
	.byte 2,35,56,6
	.asciz "_summaryElement"

LDIFF_SYM596=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM596
	.byte 2,35,60,6
	.asciz "CreateOnSelected"

LDIFF_SYM597=LTDIE_96_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM597
	.byte 2,35,36,6
	.asciz "TableView"

LDIFF_SYM598=LTDIE_90_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM598
	.byte 2,35,40,6
	.asciz "RadioSelectedChanged"

LDIFF_SYM599=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM599
	.byte 2,35,44,6
	.asciz "<Group>k__BackingField"

LDIFF_SYM600=LTDIE_97_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM600
	.byte 2,35,48,6
	.asciz "<UnevenRows>k__BackingField"

LDIFF_SYM601=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM601
	.byte 2,35,64,6
	.asciz "<NeedColorUpdate>k__BackingField"

LDIFF_SYM602=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM602
	.byte 2,35,65,6
	.asciz "<Sections>k__BackingField"

LDIFF_SYM603=LTDIE_98_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM603
	.byte 2,35,52,0,7
	.asciz "CrossUI_Touch_Dialog_Elements_RootElement"

LDIFF_SYM604=LTDIE_95 - Ldebug_info_start
	.long LDIFF_SYM604
LTDIE_95_POINTER:

	.byte 13
LDIFF_SYM605=LTDIE_95 - Ldebug_info_start
	.long LDIFF_SYM605
LTDIE_95_REFERENCE:

	.byte 14
LDIFF_SYM606=LTDIE_95 - Ldebug_info_start
	.long LDIFF_SYM606
LTDIE_99:

	.byte 5
	.asciz "_SearchTextEventHandler"

	.byte 52,16
LDIFF_SYM607=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM607
	.byte 2,35,0,0,7
	.asciz "_SearchTextEventHandler"

LDIFF_SYM608=LTDIE_99 - Ldebug_info_start
	.long LDIFF_SYM608
LTDIE_99_POINTER:

	.byte 13
LDIFF_SYM609=LTDIE_99 - Ldebug_info_start
	.long LDIFF_SYM609
LTDIE_99_REFERENCE:

	.byte 14
LDIFF_SYM610=LTDIE_99 - Ldebug_info_start
	.long LDIFF_SYM610
LTDIE_102:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

	.byte 20,16
LDIFF_SYM611=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM611
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

LDIFF_SYM612=LTDIE_102 - Ldebug_info_start
	.long LDIFF_SYM612
LTDIE_102_POINTER:

	.byte 13
LDIFF_SYM613=LTDIE_102 - Ldebug_info_start
	.long LDIFF_SYM613
LTDIE_102_REFERENCE:

	.byte 14
LDIFF_SYM614=LTDIE_102 - Ldebug_info_start
	.long LDIFF_SYM614
LTDIE_101:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewSource"

	.byte 20,16
LDIFF_SYM615=LTDIE_102 - Ldebug_info_start
	.long LDIFF_SYM615
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UITableViewSource"

LDIFF_SYM616=LTDIE_101 - Ldebug_info_start
	.long LDIFF_SYM616
LTDIE_101_POINTER:

	.byte 13
LDIFF_SYM617=LTDIE_101 - Ldebug_info_start
	.long LDIFF_SYM617
LTDIE_101_REFERENCE:

	.byte 14
LDIFF_SYM618=LTDIE_101 - Ldebug_info_start
	.long LDIFF_SYM618
LTDIE_100:

	.byte 5
	.asciz "_Source"

	.byte 32,16
LDIFF_SYM619=LTDIE_101 - Ldebug_info_start
	.long LDIFF_SYM619
	.byte 2,35,0,6
	.asciz "Container"

LDIFF_SYM620=LTDIE_85_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM620
	.byte 2,35,20,6
	.asciz "Root"

LDIFF_SYM621=LTDIE_95_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM621
	.byte 2,35,24,6
	.asciz "checkForRefresh"

LDIFF_SYM622=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM622
	.byte 2,35,28,0,7
	.asciz "_Source"

LDIFF_SYM623=LTDIE_100 - Ldebug_info_start
	.long LDIFF_SYM623
LTDIE_100_POINTER:

	.byte 13
LDIFF_SYM624=LTDIE_100 - Ldebug_info_start
	.long LDIFF_SYM624
LTDIE_100_REFERENCE:

	.byte 14
LDIFF_SYM625=LTDIE_100 - Ldebug_info_start
	.long LDIFF_SYM625
LTDIE_85:

	.byte 5
	.asciz "CrossUI_Touch_Dialog_DialogViewController"

	.byte 112,16
LDIFF_SYM626=LTDIE_86 - Ldebug_info_start
	.long LDIFF_SYM626
	.byte 2,35,0,6
	.asciz "Style"

LDIFF_SYM627=LTDIE_87 - Ldebug_info_start
	.long LDIFF_SYM627
	.byte 2,35,100,6
	.asciz "OnSelection"

LDIFF_SYM628=LTDIE_88_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM628
	.byte 2,35,52,6
	.asciz "searchBar"

LDIFF_SYM629=LTDIE_89_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM629
	.byte 2,35,56,6
	.asciz "tableView"

LDIFF_SYM630=LTDIE_90_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM630
	.byte 2,35,60,6
	.asciz "refreshView"

LDIFF_SYM631=LTDIE_91_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM631
	.byte 2,35,64,6
	.asciz "root"

LDIFF_SYM632=LTDIE_95_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM632
	.byte 2,35,68,6
	.asciz "pushing"

LDIFF_SYM633=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM633
	.byte 2,35,104,6
	.asciz "dirty"

LDIFF_SYM634=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM634
	.byte 2,35,105,6
	.asciz "reloading"

LDIFF_SYM635=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM635
	.byte 2,35,106,6
	.asciz "refreshRequested"

LDIFF_SYM636=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM636
	.byte 2,35,72,6
	.asciz "enableSearch"

LDIFF_SYM637=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM637
	.byte 2,35,107,6
	.asciz "originalSections"

LDIFF_SYM638=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM638
	.byte 2,35,76,6
	.asciz "originalElements"

LDIFF_SYM639=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM639
	.byte 2,35,80,6
	.asciz "SearchTextChanged"

LDIFF_SYM640=LTDIE_99_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM640
	.byte 2,35,84,6
	.asciz "TableSource"

LDIFF_SYM641=LTDIE_100_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM641
	.byte 2,35,88,6
	.asciz "ViewDissapearing"

LDIFF_SYM642=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM642
	.byte 2,35,92,6
	.asciz "<AutoHideSearch>k__BackingField"

LDIFF_SYM643=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM643
	.byte 2,35,108,6
	.asciz "<SearchPlaceholder>k__BackingField"

LDIFF_SYM644=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM644
	.byte 2,35,96,6
	.asciz "<Autorotate>k__BackingField"

LDIFF_SYM645=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM645
	.byte 2,35,109,0,7
	.asciz "CrossUI_Touch_Dialog_DialogViewController"

LDIFF_SYM646=LTDIE_85 - Ldebug_info_start
	.long LDIFF_SYM646
LTDIE_85_POINTER:

	.byte 13
LDIFF_SYM647=LTDIE_85 - Ldebug_info_start
	.long LDIFF_SYM647
LTDIE_85_REFERENCE:

	.byte 14
LDIFF_SYM648=LTDIE_85 - Ldebug_info_start
	.long LDIFF_SYM648
LTDIE_84:

	.byte 5
	.asciz "Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController"

	.byte 136,1,16
LDIFF_SYM649=LTDIE_85 - Ldebug_info_start
	.long LDIFF_SYM649
	.byte 2,35,0,6
	.asciz "ViewDidLoadCalled"

LDIFF_SYM650=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM650
	.byte 2,35,112,6
	.asciz "ViewWillAppearCalled"

LDIFF_SYM651=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM651
	.byte 2,35,116,6
	.asciz "ViewDidAppearCalled"

LDIFF_SYM652=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM652
	.byte 2,35,120,6
	.asciz "ViewDidDisappearCalled"

LDIFF_SYM653=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM653
	.byte 2,35,124,6
	.asciz "ViewWillDisappearCalled"

LDIFF_SYM654=LTDIE_66_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM654
	.byte 3,35,128,1,6
	.asciz "DisposeCalled"

LDIFF_SYM655=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM655
	.byte 3,35,132,1,0,7
	.asciz "Cirrious_MvvmCross_Dialog_Touch_EventSourceDialogViewController"

LDIFF_SYM656=LTDIE_84 - Ldebug_info_start
	.long LDIFF_SYM656
LTDIE_84_POINTER:

	.byte 13
LDIFF_SYM657=LTDIE_84 - Ldebug_info_start
	.long LDIFF_SYM657
LTDIE_84_REFERENCE:

	.byte 14
LDIFF_SYM658=LTDIE_84 - Ldebug_info_start
	.long LDIFF_SYM658
LTDIE_83:

	.byte 5
	.asciz "Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController"

	.byte 144,1,16
LDIFF_SYM659=LTDIE_84 - Ldebug_info_start
	.long LDIFF_SYM659
	.byte 2,35,0,6
	.asciz "<Request>k__BackingField"

LDIFF_SYM660=LTDIE_67_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM660
	.byte 3,35,136,1,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM661=LTDIE_71_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM661
	.byte 3,35,140,1,0,7
	.asciz "Cirrious_MvvmCross_Dialog_Touch_MvxDialogViewController"

LDIFF_SYM662=LTDIE_83 - Ldebug_info_start
	.long LDIFF_SYM662
LTDIE_83_POINTER:

	.byte 13
LDIFF_SYM663=LTDIE_83 - Ldebug_info_start
	.long LDIFF_SYM663
LTDIE_83_REFERENCE:

	.byte 14
LDIFF_SYM664=LTDIE_83 - Ldebug_info_start
	.long LDIFF_SYM664
LTDIE_104:

	.byte 5
	.asciz "MBProgressHUD_MTMBProgressHUD"

	.byte 76,16
LDIFF_SYM665=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM665
	.byte 2,35,0,6
	.asciz "__mt_CustomView_var"

LDIFF_SYM666=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM666
	.byte 2,35,48,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM667=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM667
	.byte 2,35,52,6
	.asciz "__mt_Color_var"

LDIFF_SYM668=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM668
	.byte 2,35,56,6
	.asciz "__mt_LabelFont_var"

LDIFF_SYM669=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM669
	.byte 2,35,60,6
	.asciz "__mt_LabelColor_var"

LDIFF_SYM670=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM670
	.byte 2,35,64,6
	.asciz "__mt_DetailsLabelFont_var"

LDIFF_SYM671=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM671
	.byte 2,35,68,6
	.asciz "__mt_DetailsLabelColor_var"

LDIFF_SYM672=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM672
	.byte 2,35,72,0,7
	.asciz "MBProgressHUD_MTMBProgressHUD"

LDIFF_SYM673=LTDIE_104 - Ldebug_info_start
	.long LDIFF_SYM673
LTDIE_104_POINTER:

	.byte 13
LDIFF_SYM674=LTDIE_104 - Ldebug_info_start
	.long LDIFF_SYM674
LTDIE_104_REFERENCE:

	.byte 14
LDIFF_SYM675=LTDIE_104 - Ldebug_info_start
	.long LDIFF_SYM675
LTDIE_103:

	.byte 5
	.asciz "OffSeasonPro_Touch_BindableProgress"

	.byte 28,16
LDIFF_SYM676=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM676
	.byte 2,35,0,6
	.asciz "_progress"

LDIFF_SYM677=LTDIE_104_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM677
	.byte 2,35,8,6
	.asciz "_parent"

LDIFF_SYM678=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM678
	.byte 2,35,12,6
	.asciz "_parentVC"

LDIFF_SYM679=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM679
	.byte 2,35,16,6
	.asciz "_labelText"

LDIFF_SYM680=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM680
	.byte 2,35,20,6
	.asciz "_closeViewWhenFinished"

LDIFF_SYM681=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM681
	.byte 2,35,24,0,7
	.asciz "OffSeasonPro_Touch_BindableProgress"

LDIFF_SYM682=LTDIE_103 - Ldebug_info_start
	.long LDIFF_SYM682
LTDIE_103_POINTER:

	.byte 13
LDIFF_SYM683=LTDIE_103 - Ldebug_info_start
	.long LDIFF_SYM683
LTDIE_103_REFERENCE:

	.byte 14
LDIFF_SYM684=LTDIE_103 - Ldebug_info_start
	.long LDIFF_SYM684
LTDIE_82:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_SettingsView"

	.byte 152,1,16
LDIFF_SYM685=LTDIE_83 - Ldebug_info_start
	.long LDIFF_SYM685
	.byte 2,35,0,6
	.asciz "_bindableProgress"

LDIFF_SYM686=LTDIE_103_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM686
	.byte 3,35,144,1,6
	.asciz "dismiss"

LDIFF_SYM687=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM687
	.byte 3,35,148,1,0,7
	.asciz "OffSeasonPro_Touch_Views_SettingsView"

LDIFF_SYM688=LTDIE_82 - Ldebug_info_start
	.long LDIFF_SYM688
LTDIE_82_POINTER:

	.byte 13
LDIFF_SYM689=LTDIE_82 - Ldebug_info_start
	.long LDIFF_SYM689
LTDIE_82_REFERENCE:

	.byte 14
LDIFF_SYM690=LTDIE_82 - Ldebug_info_start
	.long LDIFF_SYM690
	.byte 2
	.asciz "OffSeasonPro.Touch.Views.SettingsView:get_InputAccessoryView"
	.long _OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView
	.long Lme_6e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM691=LTDIE_82_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM691
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM692=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM692
	.byte 2,123,16,11
	.asciz "V_1"

LDIFF_SYM693=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM693
	.byte 1,85,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM694=Lfde7_end - Lfde7_start
	.long LDIFF_SYM694
Lfde7_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView

LDIFF_SYM695=Lme_6e - _OffSeasonPro_Touch_Views_SettingsView_get_InputAccessoryView
	.long LDIFF_SYM695
	.byte 12,13,0,72,14,8,135,2,68,14,24,133,6,136,5,138,4,139,3,142,1,68,14,128,1,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_107:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM696=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM696
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM697=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM697
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM698=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM698
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM699=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM699
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM700=LTDIE_107 - Ldebug_info_start
	.long LDIFF_SYM700
LTDIE_107_POINTER:

	.byte 13
LDIFF_SYM701=LTDIE_107 - Ldebug_info_start
	.long LDIFF_SYM701
LTDIE_107_REFERENCE:

	.byte 14
LDIFF_SYM702=LTDIE_107 - Ldebug_info_start
	.long LDIFF_SYM702
LTDIE_108:

	.byte 5
	.asciz "System_Double"

	.byte 16,16
LDIFF_SYM703=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM703
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM704=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM704
	.byte 2,35,8,0,7
	.asciz "System_Double"

LDIFF_SYM705=LTDIE_108 - Ldebug_info_start
	.long LDIFF_SYM705
LTDIE_108_POINTER:

	.byte 13
LDIFF_SYM706=LTDIE_108 - Ldebug_info_start
	.long LDIFF_SYM706
LTDIE_108_REFERENCE:

	.byte 14
LDIFF_SYM707=LTDIE_108 - Ldebug_info_start
	.long LDIFF_SYM707
LTDIE_106:

	.byte 5
	.asciz "FlipNumbers_FlipNumbersView"

	.byte 68,16
LDIFF_SYM708=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM708
	.byte 2,35,0,6
	.asciz "flipNumbers"

LDIFF_SYM709=LTDIE_107_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM709
	.byte 2,35,48,6
	.asciz "digitsCount"

LDIFF_SYM710=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM710
	.byte 2,35,52,6
	.asciz "currentValue"

LDIFF_SYM711=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM711
	.byte 2,35,56,6
	.asciz "<FlipDuration>k__BackingField"

LDIFF_SYM712=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM712
	.byte 2,35,60,0,7
	.asciz "FlipNumbers_FlipNumbersView"

LDIFF_SYM713=LTDIE_106 - Ldebug_info_start
	.long LDIFF_SYM713
LTDIE_106_POINTER:

	.byte 13
LDIFF_SYM714=LTDIE_106 - Ldebug_info_start
	.long LDIFF_SYM714
LTDIE_106_REFERENCE:

	.byte 14
LDIFF_SYM715=LTDIE_106 - Ldebug_info_start
	.long LDIFF_SYM715
LTDIE_110:

	.byte 5
	.asciz "System_Action"

	.byte 52,16
LDIFF_SYM716=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM716
	.byte 2,35,0,0,7
	.asciz "System_Action"

LDIFF_SYM717=LTDIE_110 - Ldebug_info_start
	.long LDIFF_SYM717
LTDIE_110_POINTER:

	.byte 13
LDIFF_SYM718=LTDIE_110 - Ldebug_info_start
	.long LDIFF_SYM718
LTDIE_110_REFERENCE:

	.byte 14
LDIFF_SYM719=LTDIE_110 - Ldebug_info_start
	.long LDIFF_SYM719
LTDIE_109:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_Messenger_MvxSubscriptionToken"

	.byte 32,16
LDIFF_SYM720=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM720
	.byte 2,35,0,6
	.asciz "_dependentObjects"

LDIFF_SYM721=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM721
	.byte 2,35,8,6
	.asciz "_disposeMe"

LDIFF_SYM722=LTDIE_110_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM722
	.byte 2,35,12,6
	.asciz "<Id>k__BackingField"

LDIFF_SYM723=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM723
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_Plugins_Messenger_MvxSubscriptionToken"

LDIFF_SYM724=LTDIE_109 - Ldebug_info_start
	.long LDIFF_SYM724
LTDIE_109_POINTER:

	.byte 13
LDIFF_SYM725=LTDIE_109 - Ldebug_info_start
	.long LDIFF_SYM725
LTDIE_109_REFERENCE:

	.byte 14
LDIFF_SYM726=LTDIE_109 - Ldebug_info_start
	.long LDIFF_SYM726
LTDIE_111:

	.byte 5
	.asciz "GoogleAdMobAds_GADInterstitial"

	.byte 24,16
LDIFF_SYM727=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM727
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM728=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM728
	.byte 2,35,20,0,7
	.asciz "GoogleAdMobAds_GADInterstitial"

LDIFF_SYM729=LTDIE_111 - Ldebug_info_start
	.long LDIFF_SYM729
LTDIE_111_POINTER:

	.byte 13
LDIFF_SYM730=LTDIE_111 - Ldebug_info_start
	.long LDIFF_SYM730
LTDIE_111_REFERENCE:

	.byte 14
LDIFF_SYM731=LTDIE_111 - Ldebug_info_start
	.long LDIFF_SYM731
LTDIE_112:

	.byte 5
	.asciz "MonoTouch_iAd_ADBannerView"

	.byte 52,16
LDIFF_SYM732=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM732
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM733=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM733
	.byte 2,35,48,0,7
	.asciz "MonoTouch_iAd_ADBannerView"

LDIFF_SYM734=LTDIE_112 - Ldebug_info_start
	.long LDIFF_SYM734
LTDIE_112_POINTER:

	.byte 13
LDIFF_SYM735=LTDIE_112 - Ldebug_info_start
	.long LDIFF_SYM735
LTDIE_112_REFERENCE:

	.byte 14
LDIFF_SYM736=LTDIE_112 - Ldebug_info_start
	.long LDIFF_SYM736
LTDIE_105:

	.byte 5
	.asciz "OffSeasonPro_Touch_Views_WorkoutView"

	.byte 124,16
LDIFF_SYM737=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM737
	.byte 2,35,0,6
	.asciz "_flipper"

LDIFF_SYM738=LTDIE_106_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM738
	.byte 2,35,80,6
	.asciz "_token"

LDIFF_SYM739=LTDIE_109_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM739
	.byte 2,35,84,6
	.asciz "_adMobView"

LDIFF_SYM740=LTDIE_111_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM740
	.byte 2,35,88,6
	.asciz "_iAdBannerView"

LDIFF_SYM741=LTDIE_112_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM741
	.byte 2,35,92,6
	.asciz "_didInterstitialLoad"

LDIFF_SYM742=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM742
	.byte 2,35,112,6
	.asciz "_didLoadInterFailed"

LDIFF_SYM743=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM743
	.byte 2,35,113,6
	.asciz "_viewingAd"

LDIFF_SYM744=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM744
	.byte 2,35,114,6
	.asciz "_val"

LDIFF_SYM745=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM745
	.byte 2,35,116,6
	.asciz "_resizeLabel"

LDIFF_SYM746=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM746
	.byte 2,35,120,6
	.asciz "<lblCurrentPosition>k__BackingField"

LDIFF_SYM747=LTDIE_93_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM747
	.byte 2,35,96,6
	.asciz "<btnPauseResume>k__BackingField"

LDIFF_SYM748=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM748
	.byte 2,35,100,6
	.asciz "<btnReset>k__BackingField"

LDIFF_SYM749=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM749
	.byte 2,35,104,6
	.asciz "<btnBack>k__BackingField"

LDIFF_SYM750=LTDIE_75_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM750
	.byte 2,35,108,0,7
	.asciz "OffSeasonPro_Touch_Views_WorkoutView"

LDIFF_SYM751=LTDIE_105 - Ldebug_info_start
	.long LDIFF_SYM751
LTDIE_105_POINTER:

	.byte 13
LDIFF_SYM752=LTDIE_105 - Ldebug_info_start
	.long LDIFF_SYM752
LTDIE_105_REFERENCE:

	.byte 14
LDIFF_SYM753=LTDIE_105 - Ldebug_info_start
	.long LDIFF_SYM753
	.byte 2
	.asciz "OffSeasonPro.Touch.Views.WorkoutView:LoadIAd"
	.long _OffSeasonPro_Touch_Views_WorkoutView_LoadIAd
	.long Lme_8d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM754=LTDIE_105_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM754
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM755=LTDIE_112_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM755
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM756=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM756
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM757=Lfde8_end - Lfde8_start
	.long LDIFF_SYM757
Lfde8_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_Views_WorkoutView_LoadIAd

LDIFF_SYM758=Lme_8d - _OffSeasonPro_Touch_Views_WorkoutView_LoadIAd
	.long LDIFF_SYM758
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,136,1,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_113:

	.byte 5
	.asciz "OffSeasonPro_Touch_AppDelegate"

	.byte 28,16
LDIFF_SYM759=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM759
	.byte 2,35,0,6
	.asciz "_window"

LDIFF_SYM760=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM760
	.byte 2,35,24,0,7
	.asciz "OffSeasonPro_Touch_AppDelegate"

LDIFF_SYM761=LTDIE_113 - Ldebug_info_start
	.long LDIFF_SYM761
LTDIE_113_POINTER:

	.byte 13
LDIFF_SYM762=LTDIE_113 - Ldebug_info_start
	.long LDIFF_SYM762
LTDIE_113_REFERENCE:

	.byte 14
LDIFF_SYM763=LTDIE_113 - Ldebug_info_start
	.long LDIFF_SYM763
LTDIE_114:

	.byte 5
	.asciz "MonoTouch_UIKit_UIApplication"

	.byte 24,16
LDIFF_SYM764=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM764
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIApplication"

LDIFF_SYM765=LTDIE_114 - Ldebug_info_start
	.long LDIFF_SYM765
LTDIE_114_POINTER:

	.byte 13
LDIFF_SYM766=LTDIE_114 - Ldebug_info_start
	.long LDIFF_SYM766
LTDIE_114_REFERENCE:

	.byte 14
LDIFF_SYM767=LTDIE_114 - Ldebug_info_start
	.long LDIFF_SYM767
LTDIE_115:

	.byte 5
	.asciz "MonoTouch_Foundation_NSDictionary"

	.byte 28,16
LDIFF_SYM768=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM768
	.byte 2,35,0,6
	.asciz "__mt_Keys_var"

LDIFF_SYM769=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM769
	.byte 2,35,20,6
	.asciz "__mt_Values_var"

LDIFF_SYM770=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM770
	.byte 2,35,24,0,7
	.asciz "MonoTouch_Foundation_NSDictionary"

LDIFF_SYM771=LTDIE_115 - Ldebug_info_start
	.long LDIFF_SYM771
LTDIE_115_POINTER:

	.byte 13
LDIFF_SYM772=LTDIE_115 - Ldebug_info_start
	.long LDIFF_SYM772
LTDIE_115_REFERENCE:

	.byte 14
LDIFF_SYM773=LTDIE_115 - Ldebug_info_start
	.long LDIFF_SYM773
LTDIE_116:

	.byte 17
	.asciz "Cirrious_MvvmCross_ViewModels_IMvxAppStart"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_ViewModels_IMvxAppStart"

LDIFF_SYM774=LTDIE_116 - Ldebug_info_start
	.long LDIFF_SYM774
LTDIE_116_POINTER:

	.byte 13
LDIFF_SYM775=LTDIE_116 - Ldebug_info_start
	.long LDIFF_SYM775
LTDIE_116_REFERENCE:

	.byte 14
LDIFF_SYM776=LTDIE_116 - Ldebug_info_start
	.long LDIFF_SYM776
	.byte 2
	.asciz "OffSeasonPro.Touch.AppDelegate:FinishedLaunching"
	.long _OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary
	.long Lme_ab

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM777=LTDIE_113_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM777
	.byte 1,90,3
	.asciz "app"

LDIFF_SYM778=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM778
	.byte 0,3
	.asciz "options"

LDIFF_SYM779=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM779
	.byte 0,11
	.asciz "V_0"

LDIFF_SYM780=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM780
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM781=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM781
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM782=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM782
	.byte 0,11
	.asciz "V_3"

LDIFF_SYM783=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM783
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM784=Lfde9_end - Lfde9_start
	.long LDIFF_SYM784
Lfde9_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary

LDIFF_SYM785=Lme_ab - _OffSeasonPro_Touch_AppDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication_MonoTouch_Foundation_NSDictionary
	.long LDIFF_SYM785
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,112,68,13,11
	.align 2
Lfde9_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
