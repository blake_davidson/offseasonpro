	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration:
Leh_func_begin1:
	vldr	d0, [r0, #60]
	vmov	r0, r1, d0
	bx	lr
Leh_func_end1:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_FlipDuration_double
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_FlipDuration_double:
Leh_func_begin2:
	vmov	d0, r1, r2
	vstr	d0, [r0, #60]
	bx	lr
Leh_func_end2:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_Value
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_Value:
Leh_func_begin3:
	ldr	r0, [r0, #56]
	bx	lr
Leh_func_end3:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int:
Leh_func_begin4:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	ldr	r2, [r0, #56]
	cmp	r2, r1
	popeq	{r7, pc}
	str	r1, [r0, #56]
	bl	_p_1_plt_FlipNumbers_FlipNumbersView_OnCurrentValueChanged_llvm
	pop	{r7, pc}
Leh_func_end4:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView__ctor_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView__ctor_int:
Leh_func_begin5:
	push	{r4, r5, r7, lr}
Ltmp3:
	add	r7, sp, #8
Ltmp4:
Ltmp5:
	mov	r4, r1
	mov	r5, r0
	bl	_p_2_plt_MonoTouch_UIKit_UIView__ctor_llvm
	movw	r1, #39322
	mov	r0, r5
	movt	r1, #39321
	str	r1, [r0, #60]!
	movw	r1, #39321
	movt	r1, #16329
	str	r1, [r0, #4]
	mov	r0, r5
	mov	r1, r4
	str	r4, [r5, #52]
	bl	_p_3_plt_FlipNumbers_FlipNumbersView_InitDigitsView_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end5:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_SetValue_int_bool
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_SetValue_int_bool:
Leh_func_begin6:
	push	{r4, r5, r7, lr}
Ltmp6:
	add	r7, sp, #8
Ltmp7:
Ltmp8:
	mov	r4, r1
	mov	r5, r0
	cmp	r2, #0
	beq	LBB6_2
	ldr	r1, [r5, #56]
	mov	r0, r5
	mov	r2, r4
	bl	_p_5_plt_FlipNumbers_FlipNumbersView_Flip_int_int_llvm
	str	r4, [r5, #56]
	pop	{r4, r5, r7, pc}
LBB6_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_4_plt_FlipNumbers_FlipNumbersView_set_Value_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end6:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp9:
	add	r7, sp, #12
Ltmp10:
	push	{r10, r11}
Ltmp11:
	sub	sp, sp, #20
	mov	r4, r0
	mov	r5, r2
	ldr	r2, [r4, #52]
	bl	_p_6_plt_FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int_llvm
	str	r0, [sp, #4]
	ldr	r2, [r4, #52]
	mov	r0, r4
	mov	r1, r5
	bl	_p_6_plt_FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int_llvm
	mov	r3, r4
	str	r0, [sp]
	ldr	r0, [r3, #52]
	cmp	r0, #1
	blt	LBB7_8
	ldr	r5, [sp, #4]
	ldr	r10, [sp]
	mov	r0, #16
	mov	r11, #0
	str	r0, [sp, #16]
LBB7_2:
	ldr	r0, [sp, #4]
	ldr	r0, [r0, #8]
	cmp	r0, r11
	bls	LBB7_9
	ldr	r0, [sp]
	ldrh	r1, [r5, #12]
	ldr	r0, [r0, #8]
	cmp	r0, r11
	bls	LBB7_10
	ldrh	r0, [r10, #12]
	str	r0, [sp, #12]
	cmp	r1, r0
	beq	LBB7_7
	ldr	r0, [r3, #48]
	ldr	r2, [r0]
	ldr	r2, [r0, #12]
	cmp	r11, r2
	bhs	LBB7_12
	ldr	r0, [r0, #8]
	ldr	r2, [sp, #16]
	mov	r4, r3
	ldr	r6, [r2, r0]
	vldr	d0, [r3, #60]
	ldr	r0, [r6]
	vstr	d0, [r6, #76]
	mov	r0, r3
	bl	_p_8_plt_FlipNumbers_FlipNumbersView_ConvertToDigit_char_llvm
	ldr	r1, [sp, #12]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	_p_8_plt_FlipNumbers_FlipNumbersView_ConvertToDigit_char_llvm
	ldr	r1, [sp, #8]
	mov	r2, r0
	mov	r0, r6
	bl	_p_9_plt_FlipNumbers_FlipNumberView_Flip_int_int_llvm
	mov	r3, r4
LBB7_7:
	ldr	r0, [sp, #16]
	add	r5, r5, #2
	add	r10, r10, #2
	add	r11, r11, #1
	add	r0, r0, #4
	str	r0, [sp, #16]
	ldr	r0, [r3, #52]
	cmp	r11, r0
	blt	LBB7_2
LBB7_8:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp12:
LBB7_9:
	ldr	r0, LCPI7_0
LPC7_0:
	add	r1, pc, r0
	b	LBB7_11
Ltmp13:
LBB7_10:
	ldr	r0, LCPI7_1
LPC7_1:
	add	r1, pc, r0
LBB7_11:
	movw	r0, #600
	movt	r0, #512
	bl	_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB7_12:
	movw	r0, #11703
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
	.align	2
	.data_region
LCPI7_0:
	.long	Ltmp12-(LPC7_0+8)
LCPI7_1:
	.long	Ltmp13-(LPC7_1+8)
	.end_data_region
Leh_func_end7:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged:
Leh_func_begin8:
	push	{r4, r5, r6, r7, lr}
Ltmp14:
	add	r7, sp, #12
Ltmp15:
	push	{r10, r11}
Ltmp16:
	sub	sp, sp, #4
	mov	r11, r0
	ldr	r1, [r11, #56]
	ldr	r2, [r11, #52]
	bl	_p_6_plt_FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int_llvm
	str	r0, [sp]
	ldr	r0, [r11, #52]
	cmp	r0, #1
	blt	LBB8_5
	ldr	r10, [sp]
	mov	r5, #0
	mov	r4, #16
LBB8_2:
	ldr	r0, [r11, #48]
	ldr	r1, [r0]
	ldr	r1, [r0, #12]
	cmp	r5, r1
	bhs	LBB8_6
	ldr	r0, [r0, #8]
	ldr	r6, [r4, r0]
	ldr	r0, [sp]
	ldr	r0, [r0, #8]
	cmp	r0, r5
	bls	LBB8_7
	ldrh	r1, [r10, #12]
	mov	r0, r11
	bl	_p_8_plt_FlipNumbers_FlipNumbersView_ConvertToDigit_char_llvm
	mov	r1, r0
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_13_plt_FlipNumbers_FlipNumberView_set_Digit_int_llvm
	ldr	r0, [r11, #52]
	add	r5, r5, #1
	add	r4, r4, #4
	add	r10, r10, #2
	cmp	r5, r0
	blt	LBB8_2
LBB8_5:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB8_6:
	movw	r0, #11703
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp17:
LBB8_7:
	ldr	r0, LCPI8_0
LPC8_0:
	add	r1, pc, r0
	movw	r0, #600
	movt	r0, #512
	bl	_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI8_0:
	.long	Ltmp17-(LPC8_0+8)
	.end_data_region
Leh_func_end8:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char:
Leh_func_begin9:
	push	{r7, lr}
Ltmp18:
	mov	r7, sp
Ltmp19:
Ltmp20:
	sub	sp, sp, #4
	add	r0, sp, #2
	strh	r1, [sp, #2]
	bl	_p_14_plt_char_ToString_llvm
	bl	_p_15_plt_int_Parse_string_llvm
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end9:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int:
Leh_func_begin10:
	push	{r4, r7, lr}
Ltmp21:
	add	r7, sp, #4
Ltmp22:
Ltmp23:
	sub	sp, sp, #4
	mov	r0, sp
	str	r1, [sp]
	mov	r4, r2
	bl	_p_16_plt_int_ToString_llvm
	ldr	r1, [r0]
	mov	r2, #48
	mov	r1, r4
	bl	_p_17_plt_string_PadLeft_int_char_llvm
	ldr	r1, [r0, #8]
	ldr	r2, [r0]
	sub	r1, r1, r4
	bl	_p_18_plt_string_Substring_int_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end10:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int:
Leh_func_begin11:
	push	{r4, r5, r7, lr}
Ltmp24:
	add	r7, sp, #8
Ltmp25:
Ltmp26:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5, #48]
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	ldr	r0, [r5, #60]
	ldr	r1, [r0]
	mov	r1, r4
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end11:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_CurrentDigit
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_CurrentDigit:
Leh_func_begin12:
	ldr	r0, [r0, #72]
	bx	lr
Leh_func_end12:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_CurrentDigit_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_CurrentDigit_int:
Leh_func_begin13:
	str	r1, [r0, #72]
	bx	lr
Leh_func_end13:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_AnimationDuration
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_AnimationDuration:
Leh_func_begin14:
	vldr	d0, [r0, #76]
	vmov	r0, r1, d0
	bx	lr
Leh_func_end14:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_AnimationDuration_double
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_AnimationDuration_double:
Leh_func_begin15:
	vmov	d0, r1, r2
	vstr	d0, [r0, #76]
	bx	lr
Leh_func_end15:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_InitSubviews
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_InitSubviews:
Leh_func_begin16:
	push	{r4, r5, r6, r7, lr}
Ltmp27:
	add	r7, sp, #12
Ltmp28:
	push	{r10}
Ltmp29:
	movw	r10, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC16_0+8))
	mov	r4, r0
	movt	r10, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC16_0+8))
LPC16_0:
	add	r10, pc, r10
	ldr	r0, [r10, #16]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_21_plt_FlipNumbers_TopFlipPartView__ctor_llvm
	str	r5, [r4, #52]
	ldr	r0, [r10, #16]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_21_plt_FlipNumbers_TopFlipPartView__ctor_llvm
	str	r5, [r4, #48]
	ldr	r5, [r10, #20]
	mov	r0, r5
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r6, r0
	bl	_p_22_plt_FlipNumbers_BottomFlipPartView__ctor_llvm
	mov	r0, r5
	str	r6, [r4, #56]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_22_plt_FlipNumbers_BottomFlipPartView__ctor_llvm
	str	r5, [r4, #60]
	ldr	r0, [r10, #24]
	bl	_p_23_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	ldr	r1, [r0]
	mov	r2, #0
	ldr	r3, [r1, #76]
	mov	r1, #0
	blx	r3
	mov	r5, r0
	ldr	r0, [r10, #28]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_24_plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage_llvm
	str	r6, [r4, #64]
	ldr	r0, [r4, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #1
	blx	r2
	ldr	r0, [r4, #56]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #1
	blx	r2
	ldr	r1, [r4, #64]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	ldr	r1, [r4, #48]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	ldr	r1, [r4, #52]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	ldr	r1, [r4, #60]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	ldr	r1, [r4, #56]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end16:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int:
Leh_func_begin17:
	push	{r4, r5, r7, lr}
Ltmp30:
	add	r7, sp, #8
Ltmp31:
Ltmp32:
	cmp	r2, #0
	mov	r4, r0
	moveq	r2, #10
	sub	r0, r2, r1
	cmp	r0, #1
	addlt	r0, r0, #10
	sub	r0, r0, #1
	strd	r0, r1, [r4, #68]
	mov	r1, #10
	ldr	r0, [r4, #72]
	bl	_p_25_plt__jit_icall___emul_op_irem_llvm
	mov	r5, r0
	ldr	r0, [r4, #72]
	mov	r1, #10
	add	r0, r0, #1
	bl	_p_25_plt__jit_icall___emul_op_irem_llvm
	mov	r2, r0
	mov	r0, r4
	mov	r1, r5
	bl	_p_26_plt_FlipNumbers_FlipNumberView_ChangeDigit_int_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end17:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp33:
	add	r7, sp, #12
Ltmp34:
Ltmp35:
	mov	r4, r0
	mov	r5, r1
	mov	r6, r2
	ldr	r0, [r4, #48]
	ldr	r1, [r0]
	mov	r1, r6
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	ldr	r0, [r4, #56]
	ldr	r1, [r0]
	mov	r1, r6
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	ldr	r0, [r4, #52]
	ldr	r1, [r0]
	mov	r1, r5
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	ldr	r0, [r4, #60]
	ldr	r1, [r0]
	mov	r1, r5
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	mov	r0, r4
	bl	_p_27_plt_FlipNumbers_FlipNumberView_AnimateTopPart_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end18:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnHalfFlipAnumationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnHalfFlipAnumationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs:
Leh_func_begin19:
	push	{r4, r7, lr}
Ltmp36:
	add	r7, sp, #4
Ltmp37:
Ltmp38:
	mov	r4, r0
	ldr	r0, [r4, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #1
	blx	r2
	mov	r0, r4
	bl	_p_28_plt_FlipNumbers_FlipNumberView_AnimateBottomPart_llvm
	pop	{r4, r7, pc}
Leh_func_end19:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnFlipAnimationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnFlipAnimationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs:
Leh_func_begin20:
	push	{r4, r5, r7, lr}
Ltmp39:
	add	r7, sp, #8
Ltmp40:
Ltmp41:
	mov	r4, r0
	ldr	r0, [r4, #56]
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	mov	r1, #1
	blx	r2
	ldr	r0, [r4, #60]
	ldr	r1, [r4, #56]
	ldr	r2, [r1]
	ldr	r1, [r1, #60]
	ldr	r2, [r0]
	bl	_p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm
	ldr	r0, [r4, #68]
	cmp	r0, #1
	poplt	{r4, r5, r7, pc}
	ldr	r0, [r4, #72]
	mov	r1, #10
	add	r0, r0, #1
	str	r0, [r4, #72]
	ldr	r0, [r4, #72]
	bl	_p_25_plt__jit_icall___emul_op_irem_llvm
	mov	r5, r0
	ldr	r0, [r4, #72]
	mov	r1, #10
	add	r0, r0, #1
	bl	_p_25_plt__jit_icall___emul_op_irem_llvm
	mov	r2, r0
	mov	r0, r4
	mov	r1, r5
	bl	_p_26_plt_FlipNumbers_FlipNumberView_ChangeDigit_int_int_llvm
	ldr	r0, [r4, #68]
	sub	r0, r0, #1
	str	r0, [r4, #68]
	pop	{r4, r5, r7, pc}
Leh_func_end20:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView__cctor
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView__cctor:
Leh_func_begin21:
	push	{r7}
Ltmp42:
	mov	r7, sp
Ltmp43:
Ltmp44:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r0, #0
	str	r0, [sp, #4]
	str	r0, [sp]
	movw	r0, #0
	movt	r0, #17026
	str	r0, [sp]
	movw	r0, #0
	movt	r0, #17076
	str	r0, [sp, #4]
	str	r0, [sp, #12]
	ldr	r1, [sp]
	str	r1, [sp, #8]
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC21_0+8))
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC21_0+8))
	ldr	r1, [sp, #8]
LPC21_0:
	add	r0, pc, r0
	ldr	r0, [r0, #32]
	str	r1, [r0]
	ldr	r1, [sp, #12]
	str	r1, [r0, #4]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end21:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_get_Digit
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_get_Digit:
Leh_func_begin22:
	ldr	r0, [r0, #60]
	bx	lr
Leh_func_end22:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int:
Leh_func_begin23:
	push	{r4, r7, lr}
Ltmp45:
	add	r7, sp, #4
Ltmp46:
Ltmp47:
	str	r1, [r0, #60]
	cmp	r0, #0
	ldr	r4, [r0, #52]
	beq	LBB23_2
	add	r0, r0, #60
	bl	_p_16_plt_int_ToString_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #308]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Ltmp48:
LBB23_2:
	ldr	r0, LCPI23_0
LPC23_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI23_0:
	.long	Ltmp48-(LPC23_0+8)
	.end_data_region
Leh_func_end23:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor:
Leh_func_begin24:
	push	{r4, r7, lr}
Ltmp49:
	add	r7, sp, #4
Ltmp50:
Ltmp51:
	mov	r4, r0
	bl	_p_2_plt_MonoTouch_UIKit_UIView__ctor_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #264]
	mov	r0, r4
	blx	r1
	mov	r0, r4
	bl	_p_29_plt_FlipNumbers_FlipPartView_ApplyStyles_llvm
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r2, [r0, #180]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end24:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_ApplyStyles
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_ApplyStyles:
Leh_func_begin25:
	push	{r4, r5, r6, r7, lr}
Ltmp52:
	add	r7, sp, #12
Ltmp53:
Ltmp54:
	mov	r4, r0
	ldr	r5, [r4, #56]
	bl	_p_30_plt_MonoTouch_UIKit_UIColor_get_Clear_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #256]
	mov	r0, r5
	blx	r2
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC25_0+8))
	movw	r1, #0
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC25_0+8))
	movt	r1, #17032
LPC25_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	bl	_p_31_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB25_2
	ldr	r6, [r4, #52]
	bl	_p_30_plt_MonoTouch_UIKit_UIColor_get_Clear_llvm
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #256]
	mov	r0, r6
	blx	r2
	ldr	r0, [r4, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #300]
	mov	r1, r5
	blx	r2
	ldr	r0, [r4, #52]
	ldr	r1, [r0]
	ldr	r2, [r1, #276]
	mov	r1, #1
	blx	r2
	ldr	r5, [r4, #52]
	bl	_p_32_plt_MonoTouch_UIKit_UIColor_get_White_llvm
	mov	r1, r0
	ldr	r0, [r5]
	ldr	r2, [r0, #292]
	mov	r0, r5
	blx	r2
	ldr	r4, [r4, #52]
	bl	_p_33_plt_MonoTouch_UIKit_UIColor_get_Black_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #76]
	mov	r1, #1048576000
	blx	r2
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #284]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r6, r7, pc}
LBB25_2:
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC25_1+8))
	mov	r1, #163
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC25_1+8))
LPC25_1:
	ldr	r0, [pc, r0]
	bl	_p_34_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #576
	movt	r0, #512
	bl	_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_12_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end25:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_InitSubviews
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_InitSubviews:
Leh_func_begin26:
	push	{r4, r5, r6, r7, lr}
Ltmp55:
	add	r7, sp, #12
Ltmp56:
Ltmp57:
	movw	r6, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC26_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC26_0+8))
LPC26_0:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_35_plt_MonoTouch_UIKit_UIImageView__ctor_llvm
	str	r5, [r4, #48]
	ldr	r0, [r6, #40]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_2_plt_MonoTouch_UIKit_UIView__ctor_llvm
	str	r5, [r4, #56]
	ldr	r0, [r4, #56]
	ldr	r1, [r0]
	ldr	r2, [r1, #180]
	mov	r1, #1
	blx	r2
	ldr	r0, [r6, #44]
	bl	_p_20_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_36_plt_MonoTouch_UIKit_UILabel__ctor_llvm
	str	r5, [r4, #52]
	ldr	r0, [r4, #56]
	ldr	r1, [r4, #52]
	ldr	r2, [r0]
	ldr	r2, [r2, #148]
	blx	r2
	ldr	r1, [r4, #48]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	ldr	r1, [r4, #56]
	ldr	r0, [r4]
	ldr	r2, [r0, #148]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r6, r7, pc}
Leh_func_end26:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__cctor
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__cctor:
Leh_func_begin27:
	push	{r7}
Ltmp58:
	mov	r7, sp
Ltmp59:
Ltmp60:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r0, #0
	str	r0, [sp, #4]
	str	r0, [sp]
	movw	r0, #0
	movt	r0, #17026
	str	r0, [sp]
	movw	r0, #0
	movt	r0, #16948
	str	r0, [sp, #4]
	str	r0, [sp, #12]
	ldr	r1, [sp]
	str	r1, [sp, #8]
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC27_0+8))
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC27_0+8))
	ldr	r1, [sp, #8]
LPC27_0:
	add	r0, pc, r0
	ldr	r0, [r0, #48]
	str	r1, [r0]
	ldr	r1, [sp, #12]
	str	r1, [r0, #4]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end27:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor:
Leh_func_begin28:
	push	{r7, lr}
Ltmp61:
	mov	r7, sp
Ltmp62:
Ltmp63:
	bl	_p_37_plt_FlipNumbers_FlipPartView__ctor_llvm
	pop	{r7, pc}
Leh_func_end28:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView_InitSubviews
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView_InitSubviews:
Leh_func_begin29:
	push	{r4, r7, lr}
Ltmp64:
	add	r7, sp, #4
Ltmp65:
Ltmp66:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r4, r0
	bl	_p_38_plt_FlipNumbers_FlipPartView_InitSubviews_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #228]
	mov	r0, r4
	blx	r1
	mov	r1, #0
	str	r1, [sp, #12]
	str	r1, [sp, #8]
	mov	r1, #1056964608
	str	r1, [sp, #8]
	mov	r1, #1065353216
	str	r1, [sp, #12]
	str	r1, [sp, #4]
	ldr	r2, [sp, #8]
	str	r2, [sp]
	ldr	r1, [r0]
	ldr	r3, [r1, #120]
	ldm	sp, {r1, r2}
	blx	r3
	ldr	r4, [r4, #48]
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC29_0+8))
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC29_0+8))
LPC29_0:
	add	r0, pc, r0
	ldr	r0, [r0, #52]
	bl	_p_23_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	ldr	r1, [r0]
	mov	r2, #0
	ldr	r3, [r1, #76]
	mov	r1, #9
	blx	r3
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #264]
	mov	r0, r4
	blx	r2
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end29:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor:
Leh_func_begin30:
	push	{r7, lr}
Ltmp67:
	mov	r7, sp
Ltmp68:
Ltmp69:
	bl	_p_37_plt_FlipNumbers_FlipPartView__ctor_llvm
	pop	{r7, pc}
Leh_func_end30:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView_InitSubviews
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView_InitSubviews:
Leh_func_begin31:
	push	{r4, r7, lr}
Ltmp70:
	add	r7, sp, #4
Ltmp71:
Ltmp72:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r4, r0
	bl	_p_38_plt_FlipNumbers_FlipPartView_InitSubviews_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #228]
	mov	r0, r4
	blx	r1
	mov	r1, #0
	mov	r2, #1056964608
	str	r1, [sp, #12]
	str	r1, [sp, #8]
	str	r2, [sp, #8]
	str	r1, [sp, #12]
	str	r1, [sp, #4]
	ldr	r2, [sp, #8]
	str	r2, [sp]
	ldr	r1, [r0]
	ldr	r3, [r1, #120]
	ldm	sp, {r1, r2}
	blx	r3
	ldr	r4, [r4, #48]
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC31_0+8))
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC31_0+8))
LPC31_0:
	add	r0, pc, r0
	ldr	r0, [r0, #56]
	bl	_p_23_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	ldr	r1, [r0]
	mov	r2, #0
	ldr	r3, [r1, #76]
	mov	r1, #9
	blx	r3
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #264]
	mov	r0, r4
	blx	r2
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end31:

	.private_extern	_Xamarin_Controls_FlipNumbers_iOS__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.align	2
_Xamarin_Controls_FlipNumbers_iOS__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs:
Leh_func_begin32:
	push	{r4, r5, r6, r7, lr}
Ltmp73:
	add	r7, sp, #12
Ltmp74:
Ltmp75:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC32_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got-(LPC32_0+8))
LPC32_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB32_2
	bl	_p_39_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB32_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB32_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB32_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB32_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB32_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end32:

.zerofill __DATA,__bss,_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got,356,4
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_FlipDuration_double
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_Value
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView__ctor_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_SetValue_int_bool
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_CurrentDigit
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_CurrentDigit_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_AnimationDuration
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_AnimationDuration_double
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_InitSubviews
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnHalfFlipAnumationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnFlipAnimationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView__cctor
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_get_Digit
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_ApplyStyles
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_InitSubviews
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__cctor
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView_InitSubviews
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView_InitSubviews
	.no_dead_strip	_Xamarin_Controls_FlipNumbers_iOS__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	.no_dead_strip	_mono_aot_Xamarin_Controls_FlipNumbers_iOS_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	33
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	8
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	9
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	10
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	11
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	12
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	13
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	14
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	15
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	17
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	19
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	20
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	23
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	24
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	26
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	27
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	28
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	29
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	30
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	31
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	32
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	33
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	34
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	36
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	37
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	40
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
Lset33 = Leh_func_end32-Leh_func_begin32
	.long	Lset33
Lset34 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset34
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin31:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin32:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Xamarin.Controls.FlipNumbers.iOS.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumbersView_InitDigitsView_int
_FlipNumbers_FlipNumbersView_InitDigitsView_int:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,184,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 8,0,139,229,0,0,160,227,12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 64
	.byte 0,0,159,231
bl _p_43

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 60
	.byte 1,16,159,231,0,16,145,229,8,16,128,229,48,0,134,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 56
	.byte 0,0,159,231,0,16,144,229,112,16,139,229,4,0,144,229,116,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 20
	.byte 0,0,159,231,0,16,144,229,8,16,139,229,4,0,144,229,12,0,139,229,2,234,155,237,206,10,183,238,192,235,183,238
	.byte 5,234,139,237,5,234,155,237,206,10,183,238,16,170,14,238,206,234,184,238,206,26,183,238,1,11,32,238,0,26,159,237
	.byte 0,0,0,234,0,0,0,0,193,26,183,238,1,0,74,226,16,10,14,238,206,234,184,238,206,58,183,238,65,43,176,238
	.byte 3,43,34,238,64,27,176,238,2,27,49,238,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 20
	.byte 0,0,159,231,0,16,144,229,8,16,139,229,4,0,144,229,12,0,139,229,3,234,155,237,206,10,183,238,192,235,183,238
	.byte 6,234,139,237,6,234,155,237,206,10,183,238,0,0,160,227,28,0,139,229,0,0,160,227,32,0,139,229,193,235,183,238
	.byte 9,234,139,237,192,235,183,238,10,234,139,237,9,234,155,237,206,10,183,238,192,235,183,238,11,234,139,237,11,234,155,237
	.byte 206,10,183,238,192,235,183,238,7,234,139,237,10,234,155,237,206,10,183,238,192,235,183,238,12,234,139,237,12,234,155,237
	.byte 206,10,183,238,192,235,183,238,8,234,139,237,28,0,155,229,120,0,139,229,32,0,155,229,124,0,139,229,0,0,160,227
	.byte 52,0,139,229,0,0,160,227,56,0,139,229,0,0,160,227,60,0,139,229,0,0,160,227,64,0,139,229,52,0,139,226
	.byte 112,16,155,229,116,32,155,229,120,48,155,229,124,192,155,229,0,192,141,229
bl _p_40

	.byte 52,0,155,229,128,0,139,229,56,0,155,229,132,0,139,229,60,0,155,229,136,0,139,229,64,0,155,229,140,0,139,229
	.byte 6,0,160,225,128,16,155,229,132,32,155,229,136,48,155,229,140,192,155,229,0,192,141,229,0,192,150,229,15,224,160,225
	.byte 248,240,156,229,0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,192,235,183,238,4,234,139,237,0,64,160,227
	.byte 123,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 52
	.byte 0,0,159,231
bl _p_20

	.byte 176,0,139,229
bl _p_42

	.byte 176,0,155,229,0,80,160,225,15,11,150,237,64,11,176,238,64,11,176,238,64,11,176,238,0,224,208,229,64,11,176,238
	.byte 19,11,133,237,5,0,160,225,0,16,160,227,0,224,213,229
bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int

	.byte 48,32,150,229,2,0,160,225,5,16,160,225,0,224,210,229
bl _p_41

	.byte 4,234,155,237,206,26,183,238,0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,0,0,160,227,68,0,139,229
	.byte 0,0,160,227,72,0,139,229,193,235,183,238,19,234,139,237,192,235,183,238,20,234,139,237,19,234,155,237,206,10,183,238
	.byte 192,235,183,238,21,234,139,237,21,234,155,237,206,10,183,238,192,235,183,238,17,234,139,237,20,234,155,237,206,10,183,238
	.byte 192,235,183,238,22,234,139,237,22,234,155,237,206,10,183,238,192,235,183,238,18,234,139,237,68,0,155,229,144,0,139,229
	.byte 72,0,155,229,148,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 20
	.byte 0,0,159,231,0,16,144,229,152,16,139,229,4,0,144,229,156,0,139,229,0,0,160,227,92,0,139,229,0,0,160,227
	.byte 96,0,139,229,0,0,160,227,100,0,139,229,0,0,160,227,104,0,139,229,92,0,139,226,144,16,155,229,148,32,155,229
	.byte 152,48,155,229,156,192,155,229,0,192,141,229
bl _p_40

	.byte 92,0,155,229,160,0,139,229,96,0,155,229,164,0,139,229,100,0,155,229,168,0,139,229,104,0,155,229,172,0,139,229
	.byte 5,0,160,225,160,16,155,229,164,32,155,229,168,48,155,229,172,192,155,229,0,192,141,229,0,192,149,229,15,224,160,225
	.byte 220,240,156,229,6,0,160,225,5,16,160,225,0,32,150,229,15,224,160,225,148,240,146,229,4,234,155,237,206,10,183,238
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 20
	.byte 0,0,159,231,0,16,144,229,8,16,139,229,4,0,144,229,12,0,139,229,2,234,155,237,206,26,183,238,193,235,183,238
	.byte 27,234,139,237,27,234,155,237,206,26,183,238,1,11,48,238,192,235,183,238,4,234,139,237,1,64,132,226,10,0,84,225
	.byte 129,255,255,186,184,208,139,226,112,13,189,232,128,128,189,232

Lme_6:
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumberView__ctor
_FlipNumbers_FlipNumberView__ctor:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,204,208,77,226,13,176,160,225,0,160,160,225,56,0,139,226,0,16,160,227
	.byte 64,32,160,227
bl _p_47

	.byte 10,0,160,225
bl _p_2

	.byte 10,0,160,225
bl _p_46
bl _p_45

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 68
	.byte 1,16,159,231,56,0,139,226,64,32,160,227
bl _p_44

	.byte 0,10,159,237,0,0,0,234,0,0,122,67,192,10,183,238,192,235,183,238,30,234,139,237,0,10,159,237,0,0,0,234
	.byte 0,0,128,63,192,10,183,238,30,234,155,237,206,26,183,238,65,27,177,238,1,11,128,238,192,235,183,238,25,234,139,237
	.byte 10,0,160,225,0,16,154,229,15,224,160,225,228,240,145,229,196,0,139,229,56,16,139,226,124,0,139,226,64,32,160,227
bl _p_44

	.byte 196,192,155,229,12,0,160,225,192,0,139,229,124,16,155,229,128,32,155,229,132,48,155,229,136,0,155,229,0,0,141,229
	.byte 140,0,155,229,4,0,141,229,144,0,155,229,8,0,141,229,148,0,155,229,12,0,141,229,152,0,155,229,16,0,141,229
	.byte 156,0,155,229,20,0,141,229,160,0,155,229,24,0,141,229,164,0,155,229,28,0,141,229,168,0,155,229,32,0,141,229
	.byte 172,0,155,229,36,0,141,229,176,0,155,229,40,0,141,229,180,0,155,229,44,0,141,229,184,0,155,229,48,0,141,229
	.byte 192,0,155,229,0,192,156,229,15,224,160,225,112,240,156,229,204,208,139,226,0,13,189,232,128,128,189,232

Lme_10:
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumberView_LayoutSubviews
_FlipNumbers_FlipNumberView_LayoutSubviews:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,83,223,77,226,13,176,160,225,0,160,160,225,48,0,154,229,68,1,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 56
	.byte 0,0,159,231,0,16,144,229,144,16,139,229,4,0,144,229,148,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,152,16,139,229,4,0,144,229,156,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227
	.byte 20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,16,0,139,226,144,16,155,229,148,32,155,229
	.byte 152,48,155,229,156,192,155,229,0,192,141,229
bl _p_40

	.byte 68,193,155,229,16,0,155,229,160,0,139,229,20,0,155,229,164,0,139,229,24,0,155,229,168,0,139,229,28,0,155,229
	.byte 172,0,139,229,12,0,160,225,64,1,139,229,160,16,155,229,164,32,155,229,168,48,155,229,172,0,155,229,0,0,141,229
	.byte 64,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229,52,0,154,229,60,1,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 56
	.byte 0,0,159,231,0,16,144,229,176,16,139,229,4,0,144,229,180,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,184,16,139,229,4,0,144,229,188,0,139,229,0,0,160,227,32,0,139,229,0,0,160,227
	.byte 36,0,139,229,0,0,160,227,40,0,139,229,0,0,160,227,44,0,139,229,32,0,139,226,176,16,155,229,180,32,155,229
	.byte 184,48,155,229,188,192,155,229,0,192,141,229
bl _p_40

	.byte 60,193,155,229,32,0,155,229,192,0,139,229,36,0,155,229,196,0,139,229,40,0,155,229,200,0,139,229,44,0,155,229
	.byte 204,0,139,229,12,0,160,225,56,1,139,229,192,16,155,229,196,32,155,229,200,48,155,229,204,0,155,229,0,0,141,229
	.byte 56,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229,56,0,154,229,52,1,139,229,0,26,159,237,0,0,0,234
	.byte 0,0,0,0,193,26,183,238,0,10,159,237,0,0,0,234,0,0,52,66,192,10,183,238,0,0,160,227,48,0,139,229
	.byte 0,0,160,227,52,0,139,229,193,235,183,238,14,234,139,237,192,235,183,238,15,234,139,237,14,234,155,237,206,10,183,238
	.byte 192,235,183,238,16,234,139,237,16,234,155,237,206,10,183,238,192,235,183,238,12,234,139,237,15,234,155,237,206,10,183,238
	.byte 192,235,183,238,17,234,139,237,17,234,155,237,206,10,183,238,192,235,183,238,13,234,139,237,48,0,155,229,208,0,139,229
	.byte 52,0,155,229,212,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,216,16,139,229,4,0,144,229,220,0,139,229,0,0,160,227,72,0,139,229,0,0,160,227
	.byte 76,0,139,229,0,0,160,227,80,0,139,229,0,0,160,227,84,0,139,229,72,0,139,226,208,16,155,229,212,32,155,229
	.byte 216,48,155,229,220,192,155,229,0,192,141,229
bl _p_40

	.byte 52,193,155,229,72,0,155,229,224,0,139,229,76,0,155,229,228,0,139,229,80,0,155,229,232,0,139,229,84,0,155,229
	.byte 236,0,139,229,12,0,160,225,48,1,139,229,224,16,155,229,228,32,155,229,232,48,155,229,236,0,155,229,0,0,141,229
	.byte 48,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229,60,0,154,229,44,1,139,229,0,26,159,237,0,0,0,234
	.byte 0,0,0,0,193,26,183,238,0,10,159,237,0,0,0,234,0,0,52,66,192,10,183,238,0,0,160,227,88,0,139,229
	.byte 0,0,160,227,92,0,139,229,193,235,183,238,24,234,139,237,192,235,183,238,25,234,139,237,24,234,155,237,206,10,183,238
	.byte 192,235,183,238,26,234,139,237,26,234,155,237,206,10,183,238,192,235,183,238,22,234,139,237,25,234,155,237,206,10,183,238
	.byte 192,235,183,238,27,234,139,237,27,234,155,237,206,10,183,238,192,235,183,238,23,234,139,237,88,0,155,229,240,0,139,229
	.byte 92,0,155,229,244,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,248,16,139,229,4,0,144,229,252,0,139,229,0,0,160,227,112,0,139,229,0,0,160,227
	.byte 116,0,139,229,0,0,160,227,120,0,139,229,0,0,160,227,124,0,139,229,112,0,139,226,240,16,155,229,244,32,155,229
	.byte 248,48,155,229,252,192,155,229,0,192,141,229
bl _p_40

	.byte 44,193,155,229,112,0,155,229,0,1,139,229,116,0,155,229,4,1,139,229,120,0,155,229,8,1,139,229,124,0,155,229
	.byte 12,1,139,229,12,0,160,225,40,1,139,229,0,17,155,229,4,33,155,229,8,49,155,229,12,1,155,229,0,0,141,229
	.byte 40,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229,64,0,154,229,36,1,139,229,0,58,159,237,0,0,0,234
	.byte 0,0,160,64,195,58,183,238,0,42,159,237,0,0,0,234,0,0,36,66,194,42,183,238,0,26,159,237,0,0,0,234
	.byte 0,0,92,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,128,64,192,10,183,238,0,0,160,227,128,0,139,229
	.byte 0,0,160,227,132,0,139,229,0,0,160,227,136,0,139,229,0,0,160,227,140,0,139,229,128,0,139,226,195,235,183,238
	.byte 0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238,0,234,141,237,0,48,157,229
	.byte 192,235,183,238,0,234,141,237
bl _p_48

	.byte 36,193,155,229,128,0,155,229,16,1,139,229,132,0,155,229,20,1,139,229,136,0,155,229,24,1,139,229,140,0,155,229
	.byte 28,1,139,229,12,0,160,225,32,1,139,229,16,17,155,229,20,33,155,229,24,49,155,229,28,1,155,229,0,0,141,229
	.byte 32,1,155,229,0,192,156,229,15,224,160,225,220,240,156,229,83,223,139,226,0,13,189,232,128,128,189,232

Lme_12:
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumberView_AnimateTopPart
_FlipNumbers_FlipNumberView_AnimateTopPart:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,69,223,77,226,13,176,160,225,0,160,160,225,52,32,154,229,2,0,160,225
	.byte 0,16,160,227,0,32,146,229,15,224,160,225,164,240,146,229,0,58,159,237,0,0,0,234,0,0,0,0,195,58,183,238
	.byte 0,42,159,237,0,0,0,234,0,0,128,63,194,42,183,238,0,26,159,237,0,0,0,234,0,0,0,0,193,26,183,238
	.byte 0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,128,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229
	.byte 194,235,183,238,2,234,13,237,8,32,29,229,193,235,183,238,2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_53

	.byte 0,58,159,237,0,0,0,234,219,15,201,191,195,58,183,238,0,42,159,237,0,0,0,234,0,0,128,63,194,42,183,238
	.byte 0,26,159,237,0,0,0,234,0,0,0,0,193,26,183,238,0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238
	.byte 192,0,139,226,195,235,183,238,0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238
	.byte 0,234,141,237,0,48,157,229,192,235,183,238,0,234,141,237
bl _p_53
bl _p_52

	.byte 0,192,160,225,10,0,160,225,8,1,139,229,128,16,155,229,132,32,155,229,136,48,155,229,140,0,155,229,0,0,141,229
	.byte 144,0,155,229,4,0,141,229,148,0,155,229,8,0,141,229,152,0,155,229,12,0,141,229,156,0,155,229,16,0,141,229
	.byte 160,0,155,229,20,0,141,229,164,0,155,229,24,0,141,229,168,0,155,229,28,0,141,229,172,0,155,229,32,0,141,229
	.byte 176,0,155,229,36,0,141,229,180,0,155,229,40,0,141,229,184,0,155,229,44,0,141,229,188,0,155,229,48,0,141,229
	.byte 192,0,155,229,52,0,141,229,196,0,155,229,56,0,141,229,200,0,155,229,60,0,141,229,204,0,155,229,64,0,141,229
	.byte 208,0,155,229,68,0,141,229,212,0,155,229,72,0,141,229,216,0,155,229,76,0,141,229,220,0,155,229,80,0,141,229
	.byte 224,0,155,229,84,0,141,229,228,0,155,229,88,0,141,229,232,0,155,229,92,0,141,229,236,0,155,229,96,0,141,229
	.byte 240,0,155,229,100,0,141,229,244,0,155,229,104,0,141,229,248,0,155,229,108,0,141,229,252,0,155,229,112,0,141,229
	.byte 8,1,155,229,116,192,141,229
bl _p_51

	.byte 0,1,139,229,4,1,139,229,0,0,90,227,43,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 88
	.byte 0,0,159,231
bl _p_43

	.byte 0,16,160,225,4,33,155,229,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 84
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 80
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 76
	.byte 0,0,159,231,12,0,129,229,2,0,160,225,0,224,210,229
bl _p_49

	.byte 52,16,154,229,1,0,160,225,0,16,145,229,15,224,160,225,228,240,145,229,0,48,160,225,0,17,155,229,0,32,159,229
	.byte 0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 72
	.byte 2,32,159,231,3,0,160,225,0,48,147,229,15,224,160,225,76,240,147,229,69,223,139,226,0,13,189,232,128,128,189,232
	.byte 14,16,160,225,0,0,159,229
bl _p_50

	.byte 6,2,0,2

Lme_15:
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumberView_AnimateBottomPart
_FlipNumbers_FlipNumberView_AnimateBottomPart:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,69,223,77,226,13,176,160,225,0,160,160,225,56,32,154,229,2,0,160,225
	.byte 0,16,160,227,0,32,146,229,15,224,160,225,164,240,146,229,0,58,159,237,0,0,0,234,219,15,201,63,195,58,183,238
	.byte 0,42,159,237,0,0,0,234,0,0,128,63,194,42,183,238,0,26,159,237,0,0,0,234,0,0,0,0,193,26,183,238
	.byte 0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238,128,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229
	.byte 194,235,183,238,2,234,13,237,8,32,29,229,193,235,183,238,2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_53

	.byte 0,58,159,237,0,0,0,234,0,0,0,0,195,58,183,238,0,42,159,237,0,0,0,234,0,0,128,63,194,42,183,238
	.byte 0,26,159,237,0,0,0,234,0,0,0,0,193,26,183,238,0,10,159,237,0,0,0,234,0,0,0,0,192,10,183,238
	.byte 192,0,139,226,195,235,183,238,0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238
	.byte 0,234,141,237,0,48,157,229,192,235,183,238,0,234,141,237
bl _p_53
bl _p_52

	.byte 0,192,160,225,10,0,160,225,8,1,139,229,128,16,155,229,132,32,155,229,136,48,155,229,140,0,155,229,0,0,141,229
	.byte 144,0,155,229,4,0,141,229,148,0,155,229,8,0,141,229,152,0,155,229,12,0,141,229,156,0,155,229,16,0,141,229
	.byte 160,0,155,229,20,0,141,229,164,0,155,229,24,0,141,229,168,0,155,229,28,0,141,229,172,0,155,229,32,0,141,229
	.byte 176,0,155,229,36,0,141,229,180,0,155,229,40,0,141,229,184,0,155,229,44,0,141,229,188,0,155,229,48,0,141,229
	.byte 192,0,155,229,52,0,141,229,196,0,155,229,56,0,141,229,200,0,155,229,60,0,141,229,204,0,155,229,64,0,141,229
	.byte 208,0,155,229,68,0,141,229,212,0,155,229,72,0,141,229,216,0,155,229,76,0,141,229,220,0,155,229,80,0,141,229
	.byte 224,0,155,229,84,0,141,229,228,0,155,229,88,0,141,229,232,0,155,229,92,0,141,229,236,0,155,229,96,0,141,229
	.byte 240,0,155,229,100,0,141,229,244,0,155,229,104,0,141,229,248,0,155,229,108,0,141,229,252,0,155,229,112,0,141,229
	.byte 8,1,155,229,116,192,141,229
bl _p_51

	.byte 0,1,139,229,4,1,139,229,0,0,90,227,43,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 88
	.byte 0,0,159,231
bl _p_43

	.byte 0,16,160,225,4,33,155,229,16,160,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 100
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 96
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 76
	.byte 0,0,159,231,12,0,129,229,2,0,160,225,0,224,210,229
bl _p_49

	.byte 56,16,154,229,1,0,160,225,0,16,145,229,15,224,160,225,228,240,145,229,0,48,160,225,0,17,155,229,0,32,159,229
	.byte 0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 92
	.byte 2,32,159,231,3,0,160,225,0,48,147,229,15,224,160,225,76,240,147,229,69,223,139,226,0,13,189,232,128,128,189,232
	.byte 14,16,160,225,0,0,159,229
bl _p_50

	.byte 6,2,0,2

Lme_16:
.text
	.align 2
	.no_dead_strip _FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString
_FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,224,208,77,226,13,176,160,225,56,0,139,229,60,16,139,229,64,32,139,229
	.byte 68,48,139,229,240,0,141,226,72,16,139,226,52,32,160,227,0,48,144,229,0,48,129,229,4,0,128,226,4,16,129,226
	.byte 4,32,82,226,249,255,255,26,73,15,141,226,124,16,139,226,64,32,160,227,0,48,144,229,0,48,129,229,4,0,128,226
	.byte 4,16,129,226,4,32,82,226,249,255,255,26,100,225,157,229,188,224,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 108
	.byte 0,0,159,231
bl _p_56

	.byte 216,0,139,229,0,48,160,225,56,0,155,229,19,11,144,237,64,11,176,238,64,11,176,238,0,27,159,237,1,0,0,234
	.byte 0,0,0,0,0,0,0,64,1,11,128,238,3,0,160,225,2,11,13,237,8,16,29,229,4,32,29,229,0,48,147,229
	.byte 15,224,160,225,84,240,147,229,216,32,155,229,2,0,160,225,0,16,160,227,212,32,139,229,0,32,146,229,15,224,160,225
	.byte 88,240,146,229,212,32,155,229,0,10,159,237,0,0,0,234,0,0,128,63,192,10,183,238,2,0,160,225,192,235,183,238
	.byte 2,234,13,237,8,16,29,229,208,32,139,229,0,32,146,229,15,224,160,225,80,240,146,229,208,32,155,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 104
	.byte 1,16,159,231,2,0,160,225,204,32,139,229,0,32,146,229,15,224,160,225,76,240,146,229,60,0,155,229,64,16,155,229
	.byte 68,32,155,229,72,48,155,229,76,192,155,229,0,192,141,229,80,192,155,229,4,192,141,229,84,192,155,229,8,192,141,229
	.byte 88,192,155,229,12,192,141,229,92,192,155,229,16,192,141,229,96,192,155,229,20,192,141,229,100,192,155,229,24,192,141,229
	.byte 104,192,155,229,28,192,141,229,108,192,155,229,32,192,141,229,112,192,155,229,36,192,141,229,116,192,155,229,40,192,141,229
	.byte 120,192,155,229,44,192,141,229
bl _p_55

	.byte 0,16,160,225,204,32,155,229,2,0,160,225,200,32,139,229,0,32,146,229,15,224,160,225,108,240,146,229,124,0,155,229
	.byte 128,16,155,229,132,32,155,229,136,48,155,229,140,192,155,229,0,192,141,229,144,192,155,229,4,192,141,229,148,192,155,229
	.byte 8,192,141,229,152,192,155,229,12,192,141,229,156,192,155,229,16,192,141,229,160,192,155,229,20,192,141,229,164,192,155,229
	.byte 24,192,141,229,168,192,155,229,28,192,141,229,172,192,155,229,32,192,141,229,176,192,155,229,36,192,141,229,180,192,155,229
	.byte 40,192,141,229,184,192,155,229,44,192,141,229
bl _p_55

	.byte 0,16,160,225,200,32,155,229,2,0,160,225,196,32,139,229,0,32,146,229,15,224,160,225,104,240,146,229,188,0,155,229
bl _p_54

	.byte 0,16,160,225,196,32,155,229,2,0,160,225,192,32,139,229,0,32,146,229,15,224,160,225,100,240,146,229,192,0,155,229
	.byte 224,208,139,226,0,9,189,232,128,128,189,232

Lme_19:
.text
	.align 2
	.no_dead_strip _FlipNumbers_TopFlipPartView_LayoutSubviews
_FlipNumbers_TopFlipPartView_LayoutSubviews:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,156,208,77,226,13,176,160,225,0,160,160,225,48,0,154,229,148,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 56
	.byte 0,0,159,231,0,16,144,229,64,16,139,229,4,0,144,229,68,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,72,16,139,229,4,0,144,229,76,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227
	.byte 20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,16,0,139,226,64,16,155,229,68,32,155,229
	.byte 72,48,155,229,76,192,155,229,0,192,141,229
bl _p_40

	.byte 148,192,155,229,16,0,155,229,80,0,139,229,20,0,155,229,84,0,139,229,24,0,155,229,88,0,139,229,28,0,155,229
	.byte 92,0,139,229,12,0,160,225,144,0,139,229,80,16,155,229,84,32,155,229,88,48,155,229,92,0,155,229,0,0,141,229
	.byte 144,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,56,0,154,229,140,0,139,229,0,58,159,237,0,0,0,234
	.byte 0,0,160,64,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,0,194,42,183,238,0,26,159,237,0,0,0,234
	.byte 0,0,92,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,38,66,192,10,183,238,0,0,160,227,32,0,139,229
	.byte 0,0,160,227,36,0,139,229,0,0,160,227,40,0,139,229,0,0,160,227,44,0,139,229,32,0,139,226,195,235,183,238
	.byte 0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238,0,234,141,237,0,48,157,229
	.byte 192,235,183,238,0,234,141,237
bl _p_48

	.byte 140,192,155,229,32,0,155,229,96,0,139,229,36,0,155,229,100,0,139,229,40,0,155,229,104,0,139,229,44,0,155,229
	.byte 108,0,139,229,12,0,160,225,136,0,139,229,96,16,155,229,100,32,155,229,104,48,155,229,108,0,155,229,0,0,141,229
	.byte 136,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,52,0,154,229,132,0,139,229,0,58,159,237,0,0,0,234
	.byte 0,0,0,0,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,193,194,42,183,238,0,26,159,237,0,0,0,234
	.byte 0,0,92,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,180,66,192,10,183,238,0,0,160,227,48,0,139,229
	.byte 0,0,160,227,52,0,139,229,0,0,160,227,56,0,139,229,0,0,160,227,60,0,139,229,48,0,139,226,195,235,183,238
	.byte 0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238,0,234,141,237,0,48,157,229
	.byte 192,235,183,238,0,234,141,237
bl _p_48

	.byte 132,192,155,229,48,0,155,229,112,0,139,229,52,0,155,229,116,0,139,229,56,0,155,229,120,0,139,229,60,0,155,229
	.byte 124,0,139,229,12,0,160,225,128,0,139,229,112,16,155,229,116,32,155,229,120,48,155,229,124,0,155,229,0,0,141,229
	.byte 128,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,156,208,139,226,0,13,189,232,128,128,189,232

Lme_23:
.text
	.align 2
	.no_dead_strip _FlipNumbers_BottomFlipPartView_LayoutSubviews
_FlipNumbers_BottomFlipPartView_LayoutSubviews:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,156,208,77,226,13,176,160,225,0,160,160,225,52,0,154,229,148,0,139,229
	.byte 0,58,159,237,0,0,0,234,0,0,0,0,195,58,183,238,0,42,159,237,0,0,0,234,0,0,84,194,194,42,183,238
	.byte 0,26,159,237,0,0,0,234,0,0,92,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,180,66,192,10,183,238
	.byte 0,0,160,227,16,0,139,229,0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229
	.byte 16,0,139,226,195,235,183,238,2,234,13,237,8,16,29,229,194,235,183,238,2,234,13,237,8,32,29,229,193,235,183,238
	.byte 2,234,13,237,8,48,29,229,192,235,183,238,0,234,141,237
bl _p_48

	.byte 148,192,155,229,16,0,155,229,64,0,139,229,20,0,155,229,68,0,139,229,24,0,155,229,72,0,139,229,28,0,155,229
	.byte 76,0,139,229,12,0,160,225,144,0,139,229,64,16,155,229,68,32,155,229,72,48,155,229,76,0,155,229,0,0,141,229
	.byte 144,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,56,0,154,229,140,0,139,229,0,58,159,237,0,0,0,234
	.byte 0,0,160,64,195,58,183,238,0,42,159,237,0,0,0,234,0,0,0,0,194,42,183,238,0,26,159,237,0,0,0,234
	.byte 0,0,92,66,193,26,183,238,0,10,159,237,0,0,0,234,0,0,40,66,192,10,183,238,0,0,160,227,32,0,139,229
	.byte 0,0,160,227,36,0,139,229,0,0,160,227,40,0,139,229,0,0,160,227,44,0,139,229,32,0,139,226,195,235,183,238
	.byte 0,234,141,237,0,16,157,229,194,235,183,238,0,234,141,237,0,32,157,229,193,235,183,238,0,234,141,237,0,48,157,229
	.byte 192,235,183,238,0,234,141,237
bl _p_48

	.byte 140,192,155,229,32,0,155,229,80,0,139,229,36,0,155,229,84,0,139,229,40,0,155,229,88,0,139,229,44,0,155,229
	.byte 92,0,139,229,12,0,160,225,136,0,139,229,80,16,155,229,84,32,155,229,88,48,155,229,92,0,155,229,0,0,141,229
	.byte 136,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,48,0,154,229,132,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 56
	.byte 0,0,159,231,0,16,144,229,96,16,139,229,4,0,144,229,100,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 36
	.byte 0,0,159,231,0,16,144,229,104,16,139,229,4,0,144,229,108,0,139,229,0,0,160,227,48,0,139,229,0,0,160,227
	.byte 52,0,139,229,0,0,160,227,56,0,139,229,0,0,160,227,60,0,139,229,48,0,139,226,96,16,155,229,100,32,155,229
	.byte 104,48,155,229,108,192,155,229,0,192,141,229
bl _p_40

	.byte 132,192,155,229,48,0,155,229,112,0,139,229,52,0,155,229,116,0,139,229,56,0,155,229,120,0,139,229,60,0,155,229
	.byte 124,0,139,229,12,0,160,225,128,0,139,229,112,16,155,229,116,32,155,229,120,48,155,229,124,0,155,229,0,0,141,229
	.byte 128,0,155,229,0,192,156,229,15,224,160,225,220,240,156,229,156,208,139,226,0,13,189,232,128,128,189,232

Lme_26:
.text
	.align 2
	.no_dead_strip _wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr
_wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr:

	.byte 128,64,45,233,13,112,160,225,64,9,45,233,107,223,77,226,13,176,160,225,144,1,139,229,1,96,160,225,148,33,139,229
	.byte 152,49,139,229,0,0,160,227,120,0,139,229,0,0,160,227,124,0,139,229,148,1,155,229,0,0,80,227,96,0,0,10
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 48
	.byte 0,0,159,231,0,0,144,229,0,0,80,227,0,0,0,10
bl _p_57

	.byte 0,16,150,229,144,0,139,226,64,32,160,227
bl _p_44

	.byte 4,0,134,226,0,16,144,229,208,0,139,226,64,32,160,227
bl _p_44

	.byte 8,0,134,226,0,192,144,229,144,1,155,229,160,1,139,229,144,16,155,229,148,32,155,229,152,48,155,229,156,0,155,229
	.byte 0,0,141,229,160,0,155,229,4,0,141,229,164,0,155,229,8,0,141,229,168,0,155,229,12,0,141,229,172,0,155,229
	.byte 16,0,141,229,176,0,155,229,20,0,141,229,180,0,155,229,24,0,141,229,184,0,155,229,28,0,141,229,188,0,155,229
	.byte 32,0,141,229,192,0,155,229,36,0,141,229,196,0,155,229,40,0,141,229,200,0,155,229,44,0,141,229,204,0,155,229
	.byte 48,0,141,229,208,0,155,229,52,0,141,229,212,0,155,229,56,0,141,229,216,0,155,229,60,0,141,229,220,0,155,229
	.byte 64,0,141,229,224,0,155,229,68,0,141,229,228,0,155,229,72,0,141,229,232,0,155,229,76,0,141,229,236,0,155,229
	.byte 80,0,141,229,240,0,155,229,84,0,141,229,244,0,155,229,88,0,141,229,248,0,155,229,92,0,141,229,252,0,155,229
	.byte 96,0,141,229,0,1,155,229,100,0,141,229,4,1,155,229,104,0,141,229,8,1,155,229,108,0,141,229,12,1,155,229
	.byte 112,0,141,229,160,1,155,229,116,192,141,229,152,193,155,229,60,255,47,225,120,0,139,229,6,0,0,234,128,0,155,229
	.byte 128,0,155,229,124,0,139,229,148,1,155,229,124,16,155,229,0,16,128,229,255,255,255,234,120,0,155,229,85,0,0,234
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 48
	.byte 0,0,159,231,0,0,144,229,0,0,80,227,81,0,0,26,0,16,150,229,68,15,139,226,64,32,160,227
bl _p_44

	.byte 4,0,134,226,0,16,144,229,84,15,139,226,64,32,160,227
bl _p_44

	.byte 8,0,134,226,0,192,144,229,144,1,155,229,160,1,139,229,16,17,155,229,20,33,155,229,24,49,155,229,28,1,155,229
	.byte 0,0,141,229,32,1,155,229,4,0,141,229,36,1,155,229,8,0,141,229,40,1,155,229,12,0,141,229,44,1,155,229
	.byte 16,0,141,229,48,1,155,229,20,0,141,229,52,1,155,229,24,0,141,229,56,1,155,229,28,0,141,229,60,1,155,229
	.byte 32,0,141,229,64,1,155,229,36,0,141,229,68,1,155,229,40,0,141,229,72,1,155,229,44,0,141,229,76,1,155,229
	.byte 48,0,141,229,80,1,155,229,52,0,141,229,84,1,155,229,56,0,141,229,88,1,155,229,60,0,141,229,92,1,155,229
	.byte 64,0,141,229,96,1,155,229,68,0,141,229,100,1,155,229,72,0,141,229,104,1,155,229,76,0,141,229,108,1,155,229
	.byte 80,0,141,229,112,1,155,229,84,0,141,229,116,1,155,229,88,0,141,229,120,1,155,229,92,0,141,229,124,1,155,229
	.byte 96,0,141,229,128,1,155,229,100,0,141,229,132,1,155,229,104,0,141,229,136,1,155,229,108,0,141,229,140,1,155,229
	.byte 112,0,141,229,160,1,155,229,116,192,141,229,152,193,155,229,60,255,47,225,120,0,139,229,107,223,139,226,64,9,189,232
	.byte 128,128,189,232
bl _p_57

	.byte 171,255,255,234

Lme_29:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_FlipDuration_double
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_Value
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView__ctor_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_SetValue_int_bool
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_CurrentDigit
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_CurrentDigit_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_AnimationDuration
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_AnimationDuration_double
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_InitSubviews
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnHalfFlipAnumationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnFlipAnimationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView__cctor
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_get_Digit
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_ApplyStyles
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_InitSubviews
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__cctor
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView_InitSubviews
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView_InitSubviews
.no_dead_strip _Xamarin_Controls_FlipNumbers_iOS__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_FlipDuration_double
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_Value
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView__ctor_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_SetValue_int_bool
	bl _FlipNumbers_FlipNumbersView_InitDigitsView_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_CurrentDigit
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_CurrentDigit_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_get_AnimationDuration
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_AnimationDuration_double
	bl _FlipNumbers_FlipNumberView__ctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_InitSubviews
	bl _FlipNumbers_FlipNumberView_LayoutSubviews
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int
	bl _FlipNumbers_FlipNumberView_AnimateTopPart
	bl _FlipNumbers_FlipNumberView_AnimateBottomPart
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnHalfFlipAnumationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_OnFlipAnimationFinished_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	bl _FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView__cctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_get_Digit
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_ApplyStyles
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_InitSubviews
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__cctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView_InitSubviews
	bl _FlipNumbers_TopFlipPartView_LayoutSubviews
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor
	bl _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView_InitSubviews
	bl _FlipNumbers_BottomFlipPartView_LayoutSubviews
	bl method_addresses
	bl _Xamarin_Controls_FlipNumbers_iOS__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
	bl _wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 42,10,5,2
	.short 0, 10, 20, 30, 44
	.byte 1,2,2,2,2,2,2,10,2,2,29,2,3,3,3,3,3,4,9,9,71,3,8,8,3,3,5,4,3,3,114,4
	.byte 6,4,2,3,4,2,3,255,255,255,255,114,128,146,3
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,165
	.long 41,0,0,0,0,153,40,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 2,40,153,41,165
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 2, 11, 0, 0, 5
	.short 0, 0, 0, 6, 0, 4, 0, 3
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 31,10,4,2
	.short 0, 11, 22, 33
	.byte 128,184,2,1,1,1,3,3,3,5,4,128,211,5,5,4,4,4,1,3,14,6,129,7,5,3,6,2,2,6,3,2
	.byte 2,129,42
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 42,10,5,2
	.short 0, 11, 22, 33, 48
	.byte 131,47,3,3,3,3,3,3,3,3,3,131,77,3,3,3,3,3,3,3,3,3,131,107,3,3,3,3,3,3,3,3
	.byte 3,131,137,3,3,3,3,3,4,3,3,255,255,255,252,94,131,166,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,216,1,68,13
	.byte 11,26,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,224,1,68,13,11,26,12,13,0
	.byte 72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,224,2,68,13,11,26,12,13,0,72,14,8,135,2
	.byte 68,14,20,136,5,138,4,139,3,142,1,68,14,168,2,68,13,11,24,12,13,0,72,14,8,135,2,68,14,16,136,4
	.byte 139,3,142,1,68,14,240,1,68,13,11,26,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68
	.byte 14,176,1,68,13,11,26,12,13,0,72,14,8,135,2,68,14,20,134,5,136,4,139,3,142,1,68,14,192,3,68,13
	.byte 11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 6,10,1,2
	.short 0
	.byte 131,187,7,128,243,128,241,128,245,128,241

.text
	.align 4
plt:
_mono_aot_Xamarin_Controls_FlipNumbers_iOS_plt:

.set _p_1_plt_FlipNumbers_FlipNumbersView_OnCurrentValueChanged_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_OnCurrentValueChanged
_p_2_plt_MonoTouch_UIKit_UIView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView__ctor
plt_MonoTouch_UIKit_UIView__ctor:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 128,303
_p_3_plt_FlipNumbers_FlipNumbersView_InitDigitsView_int_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumbersView_InitDigitsView_int
plt_FlipNumbers_FlipNumbersView_InitDigitsView_int:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 132,308

.set _p_4_plt_FlipNumbers_FlipNumbersView_set_Value_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_set_Value_int

.set _p_5_plt_FlipNumbers_FlipNumbersView_Flip_int_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_Flip_int_int

.set _p_6_plt_FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertDigitToString_int_int
_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 148,316

.set _p_8_plt_FlipNumbers_FlipNumbersView_ConvertToDigit_char_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_ConvertToDigit_char

.set _p_9_plt_FlipNumbers_FlipNumberView_Flip_int_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_Flip_int_int
_p_10_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 160,365
_p_11_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 164,394
_p_12_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 168,427

.set _p_13_plt_FlipNumbers_FlipNumberView_set_Digit_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_set_Digit_int
_p_14_plt_char_ToString_llvm:
	.no_dead_strip plt_char_ToString
plt_char_ToString:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 176,457
_p_15_plt_int_Parse_string_llvm:
	.no_dead_strip plt_int_Parse_string
plt_int_Parse_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 180,462
_p_16_plt_int_ToString_llvm:
	.no_dead_strip plt_int_ToString
plt_int_ToString:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 184,467
_p_17_plt_string_PadLeft_int_char_llvm:
	.no_dead_strip plt_string_PadLeft_int_char
plt_string_PadLeft_int_char:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 188,472
_p_18_plt_string_Substring_int_llvm:
	.no_dead_strip plt_string_Substring_int
plt_string_Substring_int:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 192,477

.set _p_19_plt_FlipNumbers_FlipPartView_set_Digit_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView_set_Digit_int
_p_20_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 200,484

.set _p_21_plt_FlipNumbers_TopFlipPartView__ctor_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_TopFlipPartView__ctor

.set _p_22_plt_FlipNumbers_BottomFlipPartView__ctor_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_BottomFlipPartView__ctor
_p_23_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_FromFile_string
plt_MonoTouch_UIKit_UIImage_FromFile_string:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 212,515
_p_24_plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage
plt_MonoTouch_UIKit_UIImageView__ctor_MonoTouch_UIKit_UIImage:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 216,520
_p_25_plt__jit_icall___emul_op_irem_llvm:
	.no_dead_strip plt__jit_icall___emul_op_irem
plt__jit_icall___emul_op_irem:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 220,525

.set _p_26_plt_FlipNumbers_FlipNumberView_ChangeDigit_int_int_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumberView_ChangeDigit_int_int
_p_27_plt_FlipNumbers_FlipNumberView_AnimateTopPart_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumberView_AnimateTopPart
plt_FlipNumbers_FlipNumberView_AnimateTopPart:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 228,544
_p_28_plt_FlipNumbers_FlipNumberView_AnimateBottomPart_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumberView_AnimateBottomPart
plt_FlipNumbers_FlipNumberView_AnimateBottomPart:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 232,546
_p_29_plt_FlipNumbers_FlipPartView_ApplyStyles_llvm:
	.no_dead_strip plt_FlipNumbers_FlipPartView_ApplyStyles
plt_FlipNumbers_FlipPartView_ApplyStyles:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 236,548
_p_30_plt_MonoTouch_UIKit_UIColor_get_Clear_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_Clear
plt_MonoTouch_UIKit_UIColor_get_Clear:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 240,550
_p_31_plt_MonoTouch_UIKit_UIFont_FromName_string_single_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIFont_FromName_string_single
plt_MonoTouch_UIKit_UIFont_FromName_string_single:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 244,555
_p_32_plt_MonoTouch_UIKit_UIColor_get_White_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_White
plt_MonoTouch_UIKit_UIColor_get_White:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 248,560
_p_33_plt_MonoTouch_UIKit_UIColor_get_Black_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_get_Black
plt_MonoTouch_UIKit_UIColor_get_Black:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 252,565
_p_34_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 256,570
_p_35_plt_MonoTouch_UIKit_UIImageView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor
plt_MonoTouch_UIKit_UIImageView__ctor:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 260,590
_p_36_plt_MonoTouch_UIKit_UILabel__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UILabel__ctor
plt_MonoTouch_UIKit_UILabel__ctor:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 264,595

.set _p_37_plt_FlipNumbers_FlipPartView__ctor_llvm, _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipPartView__ctor
_p_38_plt_FlipNumbers_FlipPartView_InitSubviews_llvm:
	.no_dead_strip plt_FlipNumbers_FlipPartView_InitSubviews
plt_FlipNumbers_FlipPartView_InitSubviews:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 272,602
_p_39_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 276,604
_p_40_plt_System_Drawing_RectangleF__ctor_System_Drawing_PointF_System_Drawing_SizeF_llvm:
	.no_dead_strip plt_System_Drawing_RectangleF__ctor_System_Drawing_PointF_System_Drawing_SizeF
plt_System_Drawing_RectangleF__ctor_System_Drawing_PointF_System_Drawing_SizeF:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 280,642
_p_41_plt_System_Collections_Generic_List_1_FlipNumbers_FlipNumberView_Add_FlipNumbers_FlipNumberView_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_FlipNumbers_FlipNumberView_Add_FlipNumbers_FlipNumberView
plt_System_Collections_Generic_List_1_FlipNumbers_FlipNumberView_Add_FlipNumbers_FlipNumberView:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 284,647
_p_42_plt_FlipNumbers_FlipNumberView__ctor_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumberView__ctor
plt_FlipNumbers_FlipNumberView__ctor:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 288,658
_p_43_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 292,660
_p_44_plt_string_memcpy_byte__byte__int_llvm:
	.no_dead_strip plt_string_memcpy_byte__byte__int
plt_string_memcpy_byte__byte__int:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 296,683
_p_45_plt__class_init_MonoTouch_CoreAnimation_CATransform3D_llvm:
	.no_dead_strip plt__class_init_MonoTouch_CoreAnimation_CATransform3D
plt__class_init_MonoTouch_CoreAnimation_CATransform3D:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 300,688
_p_46_plt_FlipNumbers_FlipNumberView_InitSubviews_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumberView_InitSubviews
plt_FlipNumbers_FlipNumberView_InitSubviews:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 304,692
_p_47_plt_string_memset_byte__int_int_llvm:
	.no_dead_strip plt_string_memset_byte__int_int
plt_string_memset_byte__int_int:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 308,694
_p_48_plt_System_Drawing_RectangleF__ctor_single_single_single_single_llvm:
	.no_dead_strip plt_System_Drawing_RectangleF__ctor_single_single_single_single
plt_System_Drawing_RectangleF__ctor_single_single_single_single:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 312,699
_p_49_plt_MonoTouch_CoreAnimation_CAAnimation_add_AnimationStopped_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_CoreAnimation_CAAnimation_add_AnimationStopped_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs
plt_MonoTouch_CoreAnimation_CAAnimation_add_AnimationStopped_System_EventHandler_1_MonoTouch_CoreAnimation_CAAnimationStateEventArgs:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 316,704
_p_50_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 320,709
_p_51_plt_FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString
plt_FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 324,744
_p_52_plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_get_Linear_llvm:
	.no_dead_strip plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_get_Linear
plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_get_Linear:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 328,746
_p_53_plt_MonoTouch_CoreAnimation_CATransform3D_MakeRotation_single_single_single_single_llvm:
	.no_dead_strip plt_MonoTouch_CoreAnimation_CATransform3D_MakeRotation_single_single_single_single
plt_MonoTouch_CoreAnimation_CATransform3D_MakeRotation_single_single_single_single:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 332,751
_p_54_plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_FromName_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_FromName_MonoTouch_Foundation_NSString
plt_MonoTouch_CoreAnimation_CAMediaTimingFunction_FromName_MonoTouch_Foundation_NSString:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 336,756
_p_55_plt_MonoTouch_Foundation_NSValue_FromCATransform3D_MonoTouch_CoreAnimation_CATransform3D_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSValue_FromCATransform3D_MonoTouch_CoreAnimation_CATransform3D
plt_MonoTouch_Foundation_NSValue_FromCATransform3D_MonoTouch_CoreAnimation_CATransform3D:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 340,761
_p_56_plt_MonoTouch_CoreAnimation_CABasicAnimation_FromKeyPath_string_llvm:
	.no_dead_strip plt_MonoTouch_CoreAnimation_CABasicAnimation_FromKeyPath_string
plt_MonoTouch_CoreAnimation_CABasicAnimation_FromKeyPath_string:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 344,766
_p_57_plt__jit_icall_mono_thread_force_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_force_interruption_checkpoint
plt__jit_icall_mono_thread_force_interruption_checkpoint:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got - . + 348,771
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "Xamarin.Controls.FlipNumbers.iOS"
	.asciz "4BC091A3-507C-4008-B753-79F64B6FF609"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "4BC091A3-507C-4008-B753-79F64B6FF609"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Xamarin.Controls.FlipNumbers.iOS"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Xamarin_Controls_FlipNumbers_iOS_got
	.align 2
	.long _Xamarin_Controls_FlipNumbers_iOS__FlipNumbers_FlipNumbersView_get_FlipDuration
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 31,356,58,42,11,387000831,0,2173
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Xamarin_Controls_FlipNumbers_iOS_info
	.align 2
_mono_aot_module_Xamarin_Controls_FlipNumbers_iOS_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,19,18,17,8,8,16,8,8,0,0,0,0,0,0,0,0,1
	.byte 3,0,1,3,0,1,3,0,1,3,0,1,3,0,1,3,1,20,1,3,6,4,7,6,5,5,4,1,3,6,17,12
	.byte 17,12,12,12,1,3,0,1,3,0,1,3,5,25,24,23,22,21,1,3,5,25,28,27,22,26,1,3,0,1,3,0
	.byte 1,3,2,30,29,1,3,1,8,1,4,0,1,4,0,1,4,0,1,4,1,9,1,4,3,7,11,10,1,4,1,12
	.byte 0,0,0,1,13,0,2,17,12,0,0,0,1,14,0,2,17,12,0,1,15,0,2,15,15,255,252,0,0,0,1,1
	.byte 3,219,0,0,2,255,252,0,0,0,4,11,32,3,28,17,2,31,2,17,2,31,2,28,12,0,39,42,47,14,1,5
	.byte 14,1,6,17,0,1,14,2,129,13,2,16,1,3,7,17,0,128,151,14,2,128,217,2,14,2,129,12,2,16,1,4
	.byte 16,17,0,129,192,17,0,129,242,33,14,1,3,16,2,101,2,129,48,4,2,130,50,1,1,1,3,16,7,128,243,135
	.byte 229,14,3,219,0,0,1,16,2,31,2,90,17,0,39,30,3,219,0,0,2,50,24,6,24,14,3,219,0,0,2,17
	.byte 0,73,50,25,6,25,17,0,128,133,17,0,113,3,9,3,194,0,5,202,3,7,3,4,3,8,3,11,7,42,108,108
	.byte 118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114
	.byte 97,109,112,111,108,105,110,101,0,3,10,3,20,7,26,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114
	.byte 95,109,115,99,111,114,108,105,98,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101
	.byte 120,99,101,112,116,105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99
	.byte 101,112,116,105,111,110,0,3,12,3,193,0,14,33,3,193,0,17,110,3,193,0,17,114,3,193,0,19,105,3,193,0
	.byte 19,53,3,29,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3
	.byte 34,3,37,3,194,0,4,207,3,194,0,7,32,7,14,95,95,101,109,117,108,95,111,112,95,105,114,101,109,0,3,21
	.byte 3,22,3,23,3,31,3,194,0,4,101,3,194,0,4,163,3,194,0,4,105,3,194,0,4,102,7,17,109,111,110,111
	.byte 95,104,101,108,112,101,114,95,108,100,115,116,114,0,3,194,0,7,29,3,194,0,7,11,3,30,3,32,7,35,109,111
	.byte 110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110
	.byte 116,0,3,194,0,2,169,3,255,254,0,0,0,0,202,0,0,10,3,17,7,20,109,111,110,111,95,111,98,106,101,99
	.byte 116,95,110,101,119,95,102,97,115,116,0,3,193,0,19,181,15,2,31,2,3,18,3,193,0,19,177,3,194,0,2,170
	.byte 3,194,0,0,44,7,32,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120
	.byte 99,101,112,116,105,111,110,0,3,26,3,194,0,0,70,3,194,0,1,44,3,194,0,0,71,3,194,0,2,49,3,194
	.byte 0,8,138,7,41,109,111,110,111,95,116,104,114,101,97,100,95,102,111,114,99,101,95,105,110,116,101,114,114,117,112,116
	.byte 105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2
	.byte 33,0,16,0,0,2,60,0,16,0,0,16,0,0,2,87,0,2,87,0,16,0,0,16,0,0,2,114,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,128,139,0,16,0,0,16
	.byte 0,0,2,128,139,0,16,0,0,6,128,166,1,0,128,128,4,2,130,64,1,64,129,160,129,160,0,0,128,144,8,0
	.byte 0,1,58,128,162,194,0,2,8,68,0,0,4,194,0,2,18,193,0,18,175,194,0,2,8,193,0,18,172,194,0,2
	.byte 4,194,0,2,9,194,0,6,0,194,0,2,13,194,0,2,12,194,0,2,7,194,0,5,201,194,0,5,28,194,0,5
	.byte 27,194,0,5,26,194,0,5,235,194,0,5,217,194,0,5,218,194,0,5,208,194,0,5,219,194,0,5,220,194,0,5
	.byte 255,194,0,5,251,194,0,5,250,194,0,5,249,194,0,5,248,194,0,5,247,194,0,5,246,194,0,5,245,194,0,5
	.byte 238,194,0,5,237,194,0,5,233,194,0,5,232,194,0,5,231,194,0,5,230,194,0,5,229,194,0,5,228,194,0,5
	.byte 227,194,0,5,226,194,0,5,225,194,0,5,224,194,0,5,223,194,0,5,222,194,0,5,221,194,0,5,220,194,0,5
	.byte 219,194,0,5,218,194,0,5,217,194,0,5,216,194,0,5,215,194,0,5,214,194,0,5,213,194,0,5,212,194,0,5
	.byte 211,194,0,5,210,194,0,5,209,194,0,5,208,194,0,5,207,194,0,5,206,58,128,166,27,194,0,2,8,84,8,0
	.byte 4,194,0,2,18,193,0,18,175,194,0,2,8,193,0,18,172,194,0,2,4,194,0,2,9,194,0,6,0,194,0,2
	.byte 13,194,0,2,12,194,0,2,7,194,0,5,201,194,0,5,28,194,0,5,27,194,0,5,26,194,0,5,235,194,0,5
	.byte 217,194,0,5,218,194,0,5,208,194,0,5,219,194,0,5,220,194,0,5,255,194,0,5,251,19,194,0,5,249,194,0
	.byte 5,248,194,0,5,247,194,0,5,246,194,0,5,245,194,0,5,238,194,0,5,237,194,0,5,233,194,0,5,232,194,0
	.byte 5,231,194,0,5,230,194,0,5,229,194,0,5,228,194,0,5,227,194,0,5,226,194,0,5,225,194,0,5,224,194,0
	.byte 5,223,194,0,5,222,194,0,5,221,194,0,5,220,194,0,5,219,194,0,5,218,194,0,5,217,194,0,5,216,194,0
	.byte 5,215,194,0,5,214,194,0,5,213,194,0,5,212,194,0,5,211,194,0,5,210,194,0,5,209,194,0,5,208,194,0
	.byte 5,207,194,0,5,206,59,128,166,33,194,0,2,8,64,8,0,4,194,0,2,18,193,0,18,175,194,0,2,8,193,0
	.byte 18,172,194,0,2,4,194,0,2,9,194,0,6,0,194,0,2,13,194,0,2,12,194,0,2,7,194,0,5,201,194,0
	.byte 5,28,194,0,5,27,194,0,5,26,194,0,5,235,194,0,5,217,194,0,5,218,194,0,5,208,194,0,5,219,194,0
	.byte 5,220,194,0,5,255,194,0,5,251,194,0,5,250,194,0,5,249,194,0,5,248,194,0,5,247,194,0,5,246,194,0
	.byte 5,245,194,0,5,238,194,0,5,237,194,0,5,233,194,0,5,232,194,0,5,231,194,0,5,230,194,0,5,229,194,0
	.byte 5,228,194,0,5,227,194,0,5,226,194,0,5,225,194,0,5,224,194,0,5,223,194,0,5,222,194,0,5,221,194,0
	.byte 5,220,194,0,5,219,194,0,5,218,194,0,5,217,194,0,5,216,194,0,5,215,194,0,5,214,194,0,5,213,194,0
	.byte 5,212,194,0,5,211,194,0,5,210,194,0,5,209,194,0,5,208,194,0,5,207,194,0,5,206,32,59,128,226,194,0
	.byte 2,8,64,0,0,4,194,0,2,18,193,0,18,175,194,0,2,8,193,0,18,172,194,0,2,4,194,0,2,9,194,0
	.byte 6,0,194,0,2,13,194,0,2,12,194,0,2,7,194,0,5,201,194,0,5,28,194,0,5,27,194,0,5,26,194,0
	.byte 5,235,194,0,5,217,194,0,5,218,194,0,5,208,194,0,5,219,194,0,5,220,194,0,5,255,194,0,5,251,36,194
	.byte 0,5,249,194,0,5,248,194,0,5,247,194,0,5,246,194,0,5,245,194,0,5,238,194,0,5,237,194,0,5,233,194
	.byte 0,5,232,194,0,5,231,194,0,5,230,194,0,5,229,194,0,5,228,194,0,5,227,194,0,5,226,194,0,5,225,194
	.byte 0,5,224,194,0,5,223,194,0,5,222,194,0,5,221,194,0,5,220,194,0,5,219,194,0,5,218,194,0,5,217,194
	.byte 0,5,216,194,0,5,215,194,0,5,214,194,0,5,213,194,0,5,212,194,0,5,211,194,0,5,210,194,0,5,209,194
	.byte 0,5,208,194,0,5,207,194,0,5,206,35,59,128,226,194,0,2,8,64,0,0,4,194,0,2,18,193,0,18,175,194
	.byte 0,2,8,193,0,18,172,194,0,2,4,194,0,2,9,194,0,6,0,194,0,2,13,194,0,2,12,194,0,2,7,194
	.byte 0,5,201,194,0,5,28,194,0,5,27,194,0,5,26,194,0,5,235,194,0,5,217,194,0,5,218,194,0,5,208,194
	.byte 0,5,219,194,0,5,220,194,0,5,255,194,0,5,251,39,194,0,5,249,194,0,5,248,194,0,5,247,194,0,5,246
	.byte 194,0,5,245,194,0,5,238,194,0,5,237,194,0,5,233,194,0,5,232,194,0,5,231,194,0,5,230,194,0,5,229
	.byte 194,0,5,228,194,0,5,227,194,0,5,226,194,0,5,225,194,0,5,224,194,0,5,223,194,0,5,222,194,0,5,221
	.byte 194,0,5,220,194,0,5,219,194,0,5,218,194,0,5,217,194,0,5,216,194,0,5,215,194,0,5,214,194,0,5,213
	.byte 194,0,5,212,194,0,5,211,194,0,5,210,194,0,5,209,194,0,5,208,194,0,5,207,194,0,5,206,38,98,111,101
	.byte 104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_4:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_6:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_5:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM10=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM12=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_3:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM15=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM16=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM17=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM18=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM19=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM20=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM21=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_2:

	.byte 5
	.asciz "MonoTouch_UIKit_UIResponder"

	.byte 24,16
LDIFF_SYM24=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,0,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM25=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,20,0,7
	.asciz "MonoTouch_UIKit_UIResponder"

LDIFF_SYM26=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM27=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM28=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_1:

	.byte 5
	.asciz "MonoTouch_UIKit_UIView"

	.byte 48,16
LDIFF_SYM29=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,0,6
	.asciz "__mt_BackgroundColor_var"

LDIFF_SYM30=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,24,6
	.asciz "__mt_Layer_var"

LDIFF_SYM31=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,28,6
	.asciz "__mt_Superview_var"

LDIFF_SYM32=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,32,6
	.asciz "__mt_Subviews_var"

LDIFF_SYM33=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,36,6
	.asciz "__mt_GestureRecognizers_var"

LDIFF_SYM34=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,40,6
	.asciz "__mt_TintColor_var"

LDIFF_SYM35=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIView"

LDIFF_SYM36=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM37=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM38=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_8:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM39=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM40=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM41=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_7:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM44=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM45=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM46=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM47=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM48=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM49=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM50=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_9:

	.byte 5
	.asciz "System_Double"

	.byte 16,16
LDIFF_SYM51=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM52=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,8,0,7
	.asciz "System_Double"

LDIFF_SYM53=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM54=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM55=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_0:

	.byte 5
	.asciz "FlipNumbers_FlipNumbersView"

	.byte 68,16
LDIFF_SYM56=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM56
	.byte 2,35,0,6
	.asciz "flipNumbers"

LDIFF_SYM57=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,48,6
	.asciz "digitsCount"

LDIFF_SYM58=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,52,6
	.asciz "currentValue"

LDIFF_SYM59=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM59
	.byte 2,35,56,6
	.asciz "<FlipDuration>k__BackingField"

LDIFF_SYM60=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,35,60,0,7
	.asciz "FlipNumbers_FlipNumbersView"

LDIFF_SYM61=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM62=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM63=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_10:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM64=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM65=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM66=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM66
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM67=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM68=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_14:

	.byte 5
	.asciz "MonoTouch_UIKit_UIImageView"

	.byte 52,16
LDIFF_SYM69=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM69
	.byte 2,35,0,6
	.asciz "__mt_Image_var"

LDIFF_SYM70=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIImageView"

LDIFF_SYM71=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM72=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM72
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM73=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_15:

	.byte 5
	.asciz "MonoTouch_UIKit_UILabel"

	.byte 60,16
LDIFF_SYM74=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,0,6
	.asciz "__mt_Font_var"

LDIFF_SYM75=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,48,6
	.asciz "__mt_TextColor_var"

LDIFF_SYM76=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,52,6
	.asciz "__mt_ShadowColor_var"

LDIFF_SYM77=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,56,0,7
	.asciz "MonoTouch_UIKit_UILabel"

LDIFF_SYM78=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM79=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM80=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_13:

	.byte 5
	.asciz "FlipNumbers_FlipPartView"

	.byte 64,16
LDIFF_SYM81=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,0,6
	.asciz "backroundImageView"

LDIFF_SYM82=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,48,6
	.asciz "digitLabel"

LDIFF_SYM83=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,52,6
	.asciz "labelClipView"

LDIFF_SYM84=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,56,6
	.asciz "digit"

LDIFF_SYM85=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,60,0,7
	.asciz "FlipNumbers_FlipPartView"

LDIFF_SYM86=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM86
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM87=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM88=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_12:

	.byte 5
	.asciz "FlipNumbers_TopFlipPartView"

	.byte 64,16
LDIFF_SYM89=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM89
	.byte 2,35,0,0,7
	.asciz "FlipNumbers_TopFlipPartView"

LDIFF_SYM90=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM91=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM91
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM92=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_16:

	.byte 5
	.asciz "FlipNumbers_BottomFlipPartView"

	.byte 64,16
LDIFF_SYM93=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 2,35,0,0,7
	.asciz "FlipNumbers_BottomFlipPartView"

LDIFF_SYM94=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM95=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM96=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM96
LTDIE_11:

	.byte 5
	.asciz "FlipNumbers_FlipNumberView"

	.byte 84,16
LDIFF_SYM97=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,0,6
	.asciz "flipsLeft"

LDIFF_SYM98=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,68,6
	.asciz "behindTopFlipPart"

LDIFF_SYM99=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,48,6
	.asciz "topFlipPart"

LDIFF_SYM100=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM100
	.byte 2,35,52,6
	.asciz "bottomFlipPart"

LDIFF_SYM101=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM101
	.byte 2,35,56,6
	.asciz "behindBottomFlipPart"

LDIFF_SYM102=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2,35,60,6
	.asciz "dividerImageView"

LDIFF_SYM103=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,64,6
	.asciz "<CurrentDigit>k__BackingField"

LDIFF_SYM104=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,72,6
	.asciz "<AnimationDuration>k__BackingField"

LDIFF_SYM105=LDIE_R8 - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,76,0,7
	.asciz "FlipNumbers_FlipNumberView"

LDIFF_SYM106=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM106
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM107=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM108=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2
	.asciz "FlipNumbers.FlipNumbersView:InitDigitsView"
	.long _FlipNumbers_FlipNumbersView_InitDigitsView_int
	.long Lme_6

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM109=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 1,86,3
	.asciz "count"

LDIFF_SYM110=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM111=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,123,8,11
	.asciz "V_1"

LDIFF_SYM112=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,123,16,11
	.asciz "V_2"

LDIFF_SYM113=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM113
	.byte 1,84,11
	.asciz "V_3"

LDIFF_SYM114=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 1,85,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM115=Lfde0_end - Lfde0_start
	.long LDIFF_SYM115
Lfde0_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumbersView_InitDigitsView_int

LDIFF_SYM116=Lme_6 - _FlipNumbers_FlipNumbersView_InitDigitsView_int
	.long LDIFF_SYM116
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,216,1,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "FlipNumbers.FlipNumberView:.ctor"
	.long _FlipNumbers_FlipNumberView__ctor
	.long Lme_10

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM117=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM118=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,123,56,11
	.asciz "V_1"

LDIFF_SYM119=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 3,123,248,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM120=Lfde1_end - Lfde1_start
	.long LDIFF_SYM120
Lfde1_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumberView__ctor

LDIFF_SYM121=Lme_10 - _FlipNumbers_FlipNumberView__ctor
	.long LDIFF_SYM121
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,224,1,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "FlipNumbers.FlipNumberView:LayoutSubviews"
	.long _FlipNumbers_FlipNumberView_LayoutSubviews
	.long Lme_12

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM122=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 1,90,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM123=Lfde2_end - Lfde2_start
	.long LDIFF_SYM123
Lfde2_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumberView_LayoutSubviews

LDIFF_SYM124=Lme_12 - _FlipNumbers_FlipNumberView_LayoutSubviews
	.long LDIFF_SYM124
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,224,2,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_19:

	.byte 5
	.asciz "MonoTouch_CoreAnimation_CAAnimation"

	.byte 28,16
LDIFF_SYM125=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,35,0,6
	.asciz "__mt_TimingFunction_var"

LDIFF_SYM126=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM126
	.byte 2,35,20,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM127=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,24,0,7
	.asciz "MonoTouch_CoreAnimation_CAAnimation"

LDIFF_SYM128=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM129=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM130=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM130
LTDIE_18:

	.byte 5
	.asciz "MonoTouch_CoreAnimation_CAPropertyAnimation"

	.byte 28,16
LDIFF_SYM131=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,0,0,7
	.asciz "MonoTouch_CoreAnimation_CAPropertyAnimation"

LDIFF_SYM132=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM133=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM134=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_17:

	.byte 5
	.asciz "MonoTouch_CoreAnimation_CABasicAnimation"

	.byte 36,16
LDIFF_SYM135=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,0,6
	.asciz "__mt_From_var"

LDIFF_SYM136=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM136
	.byte 2,35,28,6
	.asciz "__mt_To_var"

LDIFF_SYM137=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM137
	.byte 2,35,32,0,7
	.asciz "MonoTouch_CoreAnimation_CABasicAnimation"

LDIFF_SYM138=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM139=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM140=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM140
	.byte 2
	.asciz "FlipNumbers.FlipNumberView:AnimateTopPart"
	.long _FlipNumbers_FlipNumberView_AnimateTopPart
	.long Lme_15

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM141=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM141
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM142=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM142
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM143=Lfde3_end - Lfde3_start
	.long LDIFF_SYM143
Lfde3_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumberView_AnimateTopPart

LDIFF_SYM144=Lme_15 - _FlipNumbers_FlipNumberView_AnimateTopPart
	.long LDIFF_SYM144
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,168,2,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "FlipNumbers.FlipNumberView:AnimateBottomPart"
	.long _FlipNumbers_FlipNumberView_AnimateBottomPart
	.long Lme_16

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM145=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM146=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM146
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM147=Lfde4_end - Lfde4_start
	.long LDIFF_SYM147
Lfde4_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumberView_AnimateBottomPart

LDIFF_SYM148=Lme_16 - _FlipNumbers_FlipNumberView_AnimateBottomPart
	.long LDIFF_SYM148
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,168,2,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_20:

	.byte 5
	.asciz "MonoTouch_Foundation_NSString"

	.byte 20,16
LDIFF_SYM149=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSString"

LDIFF_SYM150=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM152
	.byte 2
	.asciz "FlipNumbers.FlipNumberView:Create3DAnimation"
	.long _FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString
	.long Lme_19

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM153=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,123,56,3
	.asciz "fromValue"

LDIFF_SYM154=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM154
	.byte 2,123,60,3
	.asciz "toValue"

LDIFF_SYM155=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 3,123,252,0,3
	.asciz "timingFunctionName"

LDIFF_SYM156=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 3,123,188,1,11
	.asciz "V_0"

LDIFF_SYM157=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM158=Lfde5_end - Lfde5_start
	.long LDIFF_SYM158
Lfde5_start:

	.long 0
	.align 2
	.long _FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString

LDIFF_SYM159=Lme_19 - _FlipNumbers_FlipNumberView_Create3DAnimation_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_CoreAnimation_CATransform3D_MonoTouch_Foundation_NSString
	.long LDIFF_SYM159
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,240,1,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "FlipNumbers.TopFlipPartView:LayoutSubviews"
	.long _FlipNumbers_TopFlipPartView_LayoutSubviews
	.long Lme_23

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM160=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 1,90,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM161=Lfde6_end - Lfde6_start
	.long LDIFF_SYM161
Lfde6_start:

	.long 0
	.align 2
	.long _FlipNumbers_TopFlipPartView_LayoutSubviews

LDIFF_SYM162=Lme_23 - _FlipNumbers_TopFlipPartView_LayoutSubviews
	.long LDIFF_SYM162
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,176,1,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "FlipNumbers.BottomFlipPartView:LayoutSubviews"
	.long _FlipNumbers_BottomFlipPartView_LayoutSubviews
	.long Lme_26

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM163=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 1,90,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM164=Lfde7_end - Lfde7_start
	.long LDIFF_SYM164
Lfde7_start:

	.long 0
	.align 2
	.long _FlipNumbers_BottomFlipPartView_LayoutSubviews

LDIFF_SYM165=Lme_26 - _FlipNumbers_BottomFlipPartView_LayoutSubviews
	.long LDIFF_SYM165
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,176,1,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "(wrapper runtime-invoke) <Module>:runtime_invoke_object__this___CATransform3D_CATransform3D_object"
	.long _wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr
	.long Lme_29

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM166=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 3,123,144,3,3
	.asciz "params"

LDIFF_SYM167=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 1,86,3
	.asciz "exc"

LDIFF_SYM168=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 3,123,148,3,3
	.asciz "method"

LDIFF_SYM169=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 3,123,152,3,11
	.asciz "V_0"

LDIFF_SYM170=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 3,123,248,0,11
	.asciz "V_1"

LDIFF_SYM171=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 3,123,252,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM172=Lfde8_end - Lfde8_start
	.long LDIFF_SYM172
Lfde8_start:

	.long 0
	.align 2
	.long _wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr

LDIFF_SYM173=Lme_29 - _wrapper_runtime_invoke__Module_runtime_invoke_object__this___CATransform3D_CATransform3D_object_object_intptr_intptr_intptr
	.long LDIFF_SYM173
	.byte 12,13,0,72,14,8,135,2,68,14,20,134,5,136,4,139,3,142,1,68,14,192,3,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
