	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin1:
	add	r9, r0, #8
	stm	r9, {r1, r2, r3}
	bx	lr
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r8, r10, r11}
Ltmp2:
	sub	sp, sp, #4
	str	r0, [sp]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC2_0+8))
	mov	r4, r1
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC2_0+8))
LPC2_0:
	add	r6, pc, r6
	ldr	r10, [r6, #16]
	ldr	r5, [r6, #20]
	ldr	r2, [r5]
	cmp	r2, #0
	bne	LBB2_2
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #308]
	str	r1, [r0, #20]
	ldr	r1, [r6, #312]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	str	r0, [r5]
	ldr	r0, [r6, #20]
	ldr	r2, [r0]
LBB2_2:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #28]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_4
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #300]
	str	r1, [r0, #20]
	ldr	r1, [r6, #304]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	ldr	r1, [r6, #32]
	str	r0, [r1]
	ldr	r0, [r6, #32]
	ldr	r2, [r0]
LBB2_4:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #36]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_6
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #292]
	str	r1, [r0, #20]
	ldr	r1, [r6, #296]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	ldr	r1, [r6, #40]
	str	r0, [r1]
	ldr	r0, [r6, #40]
	ldr	r2, [r0]
LBB2_6:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	add	r3, r6, #44
	mov	r0, r4
	ldm	r3, {r1, r2, r3}
	bl	_p_2_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string_llvm
	add	r3, r6, #56
	mov	r0, r4
	ldm	r3, {r1, r2, r3}
	bl	_p_2_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string_llvm
	ldrd	r10, r11, [r6, #68]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_8
	ldr	r0, [r6, #236]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #284]
	str	r1, [r0, #20]
	ldr	r1, [r6, #288]
	str	r1, [r0, #28]
	ldr	r1, [r6, #248]
	str	r1, [r0, #12]
	ldr	r1, [r6, #72]
	str	r0, [r1]
	ldr	r0, [r6, #72]
	ldr	r2, [r0]
LBB2_8:
	ldr	r5, [r6, #76]
	mov	r0, r4
	mov	r1, r10
	mov	r8, r5
	bl	_p_3_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #80]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_10
	ldr	r0, [r6, #268]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #272]
	str	r1, [r0, #20]
	ldr	r1, [r6, #276]
	str	r1, [r0, #28]
	ldr	r1, [r6, #280]
	str	r1, [r0, #12]
	str	r0, [r11]
	ldr	r0, [r6, #84]
	ldr	r2, [r0]
LBB2_10:
	ldr	r0, [r6, #88]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_4_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #92]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_12
	ldr	r0, [r6, #252]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #256]
	str	r1, [r0, #20]
	ldr	r1, [r6, #260]
	str	r1, [r0, #28]
	ldr	r1, [r6, #264]
	str	r1, [r0, #12]
	ldr	r1, [r6, #96]
	str	r0, [r1]
	ldr	r0, [r6, #96]
	ldr	r2, [r0]
LBB2_12:
	ldr	r0, [r6, #100]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_5_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldr	r0, [r6, #104]
	ldr	r2, [r0]
	cmp	r2, #0
	bne	LBB2_14
	ldr	r0, [r6, #236]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #240]
	str	r1, [r0, #20]
	ldr	r1, [r6, #244]
	str	r1, [r0, #28]
	ldr	r1, [r6, #248]
	str	r1, [r0, #12]
	ldr	r1, [r6, #104]
	str	r0, [r1]
	ldr	r0, [r6, #104]
	ldr	r2, [r0]
LBB2_14:
	mov	r8, r5
	mov	r0, r4
	mov	r1, r10
	bl	_p_3_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldr	r5, [r6, #108]
	ldr	r2, [r5]
	cmp	r2, #0
	bne	LBB2_16
	ldr	r0, [r6, #220]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #224]
	str	r1, [r0, #20]
	ldr	r1, [r6, #228]
	str	r1, [r0, #28]
	ldr	r1, [r6, #232]
	str	r1, [r0, #12]
	str	r0, [r5]
	ldr	r0, [r6, #108]
	ldr	r2, [r0]
LBB2_16:
	ldr	r0, [r6, #112]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_6_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	add	r3, r6, #116
	mov	r0, r4
	ldm	r3, {r1, r2, r3}
	bl	_p_2_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string_llvm
	ldrd	r2, r3, [r6, #128]
	mov	r0, r4
	mov	r1, r2
	mov	r2, r3
	mov	r3, r10
	bl	_p_2_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string_llvm
	ldrd	r10, r11, [r6, #136]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_18
	ldr	r0, [r6, #204]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #208]
	str	r1, [r0, #20]
	ldr	r1, [r6, #212]
	str	r1, [r0, #28]
	ldr	r1, [r6, #216]
	str	r1, [r0, #12]
	str	r0, [r11]
	ldr	r0, [r6, #140]
	ldr	r2, [r0]
LBB2_18:
	ldr	r0, [r6, #144]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_7_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #148]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_20
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #196]
	str	r1, [r0, #20]
	ldr	r1, [r6, #200]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	ldr	r1, [r6, #152]
	str	r0, [r1]
	ldr	r0, [r6, #152]
	ldr	r2, [r0]
LBB2_20:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #156]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_22
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #188]
	str	r1, [r0, #20]
	ldr	r1, [r6, #192]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	ldr	r1, [r6, #160]
	str	r0, [r1]
	ldr	r0, [r6, #160]
	ldr	r2, [r0]
LBB2_22:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldrd	r10, r11, [r6, #164]
	ldr	r2, [r11]
	cmp	r2, #0
	bne	LBB2_24
	ldr	r0, [r6, #172]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #176]
	str	r1, [r0, #20]
	ldr	r1, [r6, #180]
	str	r1, [r0, #28]
	ldr	r1, [r6, #184]
	str	r1, [r0, #12]
	ldr	r1, [r6, #168]
	str	r0, [r1]
	ldr	r0, [r6, #168]
	ldr	r2, [r0]
LBB2_24:
	ldr	r0, [r6, #24]
	mov	r1, r10
	mov	r8, r0
	mov	r0, r4
	bl	_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm
	ldr	r1, [sp]
	ldr	r0, [r1, #8]
	cmp	r0, #0
	beq	LBB2_26
	ldr	r0, [r1, #8]
	mov	r1, r4
	ldr	r2, [r0, #12]
	blx	r2
LBB2_26:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
Leh_func_begin3:
	push	{r4, r5, r7, lr}
Ltmp3:
	add	r7, sp, #8
Ltmp4:
Ltmp5:
	mov	r4, r1
	mov	r5, r0
	bl	_p_9_plt_Cirrious_MvvmCross_Binding_MvxCoreBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_llvm
	ldr	r0, [r5, #12]
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r5, #12]
	mov	r1, r4
	ldr	r2, [r0, #12]
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillDefaultBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillDefaultBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin4:
	push	{r4, r5, r6, r7, lr}
Ltmp6:
	add	r7, sp, #12
Ltmp7:
	push	{r8, r10, r11}
Ltmp8:
	sub	sp, sp, #8
	str	r0, [sp, #4]
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC4_0+8))
	mov	r4, r1
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC4_0+8))
LPC4_0:
	add	r11, pc, r11
	add	r5, r11, #316
	ldm	r5, {r1, r2, r5}
	ldr	r0, [r4]
	sub	r0, r0, #68
	mov	r8, r5
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #328]
	ldr	r0, [r4]
	ldr	r2, [r11, #332]
	mov	r8, r5
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r6, [r11, #92]
	ldr	r1, [r11, #132]
	mov	r8, r5
	sub	r0, r0, #68
	mov	r2, r6
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #336]
	ldr	r0, [r4]
	mov	r8, r5
	mov	r2, r6
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r1, [r11, #340]
	mov	r8, r5
	mov	r2, r6
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #344]
	ldr	r0, [r4]
	mov	r8, r5
	mov	r2, r6
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #336]
	ldr	r0, [r4]
	mov	r8, r5
	mov	r2, r6
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r2, [r11, #352]
	ldr	r0, [r4]
	ldr	r1, [r11, #348]
	mov	r8, r5
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r2, [r11, #352]
	ldr	r0, [r4]
	ldr	r1, [r11, #356]
	mov	r8, r5
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r2, [r11, #364]
	ldr	r1, [r11, #360]
	mov	r8, r5
	str	r2, [sp]
	ldr	r0, [r4]
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #368]
	ldr	r0, [r4]
	ldr	r2, [r11, #372]
	mov	r8, r5
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r10, [r11, #60]
	ldr	r3, [r11, #64]
	ldr	r0, [r4]
	mov	r8, r5
	sub	r0, r0, #68
	mov	r1, r10
	mov	r2, r3
	ldr	r6, [r0]
	mov	r0, r4
	blx	r6
	ldrd	r2, r3, [r11, #48]
	ldr	r0, [r4]
	mov	r8, r5
	sub	r0, r0, #68
	mov	r1, r2
	mov	r2, r3
	ldr	r6, [r0]
	mov	r0, r4
	blx	r6
	ldrd	r2, r3, [r11, #120]
	ldr	r0, [r4]
	mov	r8, r5
	sub	r0, r0, #68
	mov	r1, r2
	mov	r2, r3
	ldr	r6, [r0]
	mov	r0, r4
	blx	r6
	ldr	r2, [r11, #64]
	ldr	r0, [r4]
	mov	r8, r5
	mov	r1, r10
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [r11, #376]
	ldr	r0, [r4]
	ldr	r6, [sp]
	mov	r8, r5
	sub	r0, r0, #68
	mov	r2, r6
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r1, [r11, #380]
	mov	r8, r5
	mov	r2, r6
	sub	r0, r0, #68
	ldr	r3, [r0]
	mov	r0, r4
	blx	r3
	ldr	r1, [sp, #4]
	ldr	r0, [r1, #16]
	cmp	r0, #0
	beq	LBB4_2
	ldr	r0, [r1, #16]
	mov	r1, r4
	ldr	r2, [r0, #12]
	blx	r2
LBB4_2:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__0_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__0_MonoTouch_UIKit_UIView:
Leh_func_begin5:
	push	{r4, r5, r7, lr}
Ltmp9:
	add	r7, sp, #8
Ltmp10:
Ltmp11:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC5_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC5_0+8))
LPC5_0:
	add	r0, pc, r0
	ldr	r0, [r0, #384]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_11_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__1_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__1_MonoTouch_UIKit_UIView:
Leh_func_begin6:
	push	{r4, r5, r7, lr}
Ltmp12:
	add	r7, sp, #8
Ltmp13:
Ltmp14:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC6_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC6_0+8))
LPC6_0:
	add	r0, pc, r0
	ldr	r0, [r0, #388]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_12_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__2_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__2_MonoTouch_UIKit_UIView:
Leh_func_begin7:
	push	{r4, r5, r7, lr}
Ltmp15:
	add	r7, sp, #8
Ltmp16:
Ltmp17:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC7_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #392]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_13_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__3_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__3_MonoTouch_UIKit_UITextField:
Leh_func_begin8:
	push	{r4, r5, r7, lr}
Ltmp18:
	add	r7, sp, #8
Ltmp19:
Ltmp20:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC8_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC8_0+8))
LPC8_0:
	add	r0, pc, r0
	ldr	r0, [r0, #396]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_14_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__4_MonoTouch_UIKit_UIDatePicker
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__4_MonoTouch_UIKit_UIDatePicker:
Leh_func_begin9:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
Ltmp23:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC9_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC9_0+8))
LPC9_0:
	add	r6, pc, r6
	ldrd	r0, r1, [r6, #60]
	bl	_p_15_plt_System_Type_GetProperty_string_llvm
	mov	r5, r0
	ldr	r0, [r6, #400]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r2, r5
	mov	r6, r0
	bl	_p_16_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__5_MonoTouch_UIKit_UILabel
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__5_MonoTouch_UIKit_UILabel:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp24:
	add	r7, sp, #8
Ltmp25:
Ltmp26:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC10_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC10_0+8))
LPC10_0:
	add	r0, pc, r0
	ldr	r0, [r0, #404]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_17_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__6_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__6_MonoTouch_UIKit_UITextField:
Leh_func_begin11:
	push	{r4, r5, r7, lr}
Ltmp27:
	add	r7, sp, #8
Ltmp28:
Ltmp29:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC11_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC11_0+8))
LPC11_0:
	add	r0, pc, r0
	ldr	r0, [r0, #408]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_18_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__7_MonoTouch_UIKit_UITextView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__7_MonoTouch_UIKit_UITextView:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp30:
	add	r7, sp, #8
Ltmp31:
Ltmp32:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC12_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC12_0+8))
LPC12_0:
	add	r0, pc, r0
	ldr	r0, [r0, #412]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_19_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__8_MonoTouch_UIKit_UIButton
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__8_MonoTouch_UIKit_UIButton:
Leh_func_begin13:
	push	{r4, r5, r7, lr}
Ltmp33:
	add	r7, sp, #8
Ltmp34:
Ltmp35:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC13_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r0, [r0, #416]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_20_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__9_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__9_MonoTouch_UIKit_UIView:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp36:
	add	r7, sp, #8
Ltmp37:
Ltmp38:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC14_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC14_0+8))
LPC14_0:
	add	r0, pc, r0
	ldr	r0, [r0, #420]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r2, #1
	mov	r3, #1
	mov	r5, r0
	bl	_p_21_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__a_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__a_MonoTouch_UIKit_UIView:
Leh_func_begin15:
	push	{r4, r5, r7, lr}
Ltmp39:
	add	r7, sp, #8
Ltmp40:
Ltmp41:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC15_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC15_0+8))
LPC15_0:
	add	r0, pc, r0
	ldr	r0, [r0, #420]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r2, #2
	mov	r3, #1
	mov	r5, r0
	bl	_p_21_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__b_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__b_MonoTouch_UIKit_UIView:
Leh_func_begin16:
	push	{r4, r5, r7, lr}
Ltmp42:
	add	r7, sp, #8
Ltmp43:
Ltmp44:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC16_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC16_0+8))
LPC16_0:
	add	r0, pc, r0
	ldr	r0, [r0, #420]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r2, #1
	mov	r3, #2
	mov	r5, r0
	bl	_p_21_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin17:
	push	{r4, r5, r6, r7, lr}
Ltmp45:
	add	r7, sp, #12
Ltmp46:
Ltmp47:
	mov	r4, r0
	bl	_p_22_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_23_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB17_3
	cmp	r4, #0
	beq	LBB17_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC17_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC17_1+8))
LPC17_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #428]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #432]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_25_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB17_3:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC17_2+8))
	mov	r1, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC17_2+8))
LPC17_2:
	add	r0, pc, r0
	ldr	r4, [r0, #440]
	ldr	r0, [r0, #444]
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp48:
LBB17_4:
	ldr	r0, LCPI17_0
LPC17_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI17_0:
	.long	Ltmp48-(LPC17_0+8)
	.end_data_region
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_DatePickerOnValueChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_DatePickerOnValueChanged_object_System_EventArgs:
Leh_func_begin18:
	push	{r4, r7, lr}
Ltmp49:
	add	r7, sp, #4
Ltmp50:
Ltmp51:
	mov	r4, r0
	bl	_p_23_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View_llvm
	mov	r1, r0
	cmp	r1, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4]
	ldr	r2, [r0, #112]
	mov	r0, r4
	blx	r2
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_get_DefaultMode:
Leh_func_begin19:
	mov	r0, #1
	bx	lr
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_Dispose_bool:
Leh_func_begin20:
	push	{r4, r5, r6, r7, lr}
Ltmp52:
	add	r7, sp, #12
Ltmp53:
Ltmp54:
	mov	r5, r1
	mov	r4, r0
	bl	_p_28_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	beq	LBB20_3
	mov	r0, r4
	bl	_p_23_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	cmp	r4, #0
	beq	LBB20_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC20_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC20_1+8))
LPC20_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #428]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #432]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_29_plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler_llvm
LBB20_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp55:
LBB20_4:
	ldr	r0, LCPI20_0
LPC20_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI20_0:
	.long	Ltmp55-(LPC20_0+8)
	.end_data_region
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View:
Leh_func_begin21:
	push	{r7, lr}
Ltmp56:
	mov	r7, sp
Ltmp57:
Ltmp58:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC21_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC21_1+8))
	ldr	r2, [r0]
LPC21_1:
	add	r1, pc, r1
	ldr	r1, [r1, #448]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB21_2
	pop	{r7, pc}
Ltmp59:
LBB21_2:
	ldr	r0, LCPI21_0
LPC21_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI21_0:
	.long	Ltmp59-(LPC21_0+8)
	.end_data_region
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView:
Leh_func_begin22:
	push	{r7, lr}
Ltmp60:
	mov	r7, sp
Ltmp61:
Ltmp62:
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	pop	{r7, pc}
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_DefaultMode:
Leh_func_begin23:
	mov	r0, #2
	bx	lr
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_TargetType:
Leh_func_begin24:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC24_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #452]
	bx	lr
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin25:
	push	{r7, lr}
Ltmp63:
	mov	r7, sp
Ltmp64:
Ltmp65:
	bl	_p_32_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo_llvm
	pop	{r7, pc}
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker:
Leh_func_begin26:
	push	{r4, r7, lr}
Ltmp66:
	add	r7, sp, #4
Ltmp67:
Ltmp68:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r0, #0
	str	r0, [sp, #4]
	str	r0, [sp]
	ldr	r0, [r1]
	ldr	r2, [r0, #296]
	mov	r0, r1
	blx	r2
	mov	r4, sp
	mov	r1, r4
	bl	_p_33_plt_MonoTouch_Foundation_NSDate_op_Implicit_MonoTouch_Foundation_NSDate_llvm
	add	r1, sp, #8
	mov	r0, r4
	bl	_p_34_plt_System_DateTime_get_Date_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC26_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC26_0+8))
LPC26_0:
	add	r0, pc, r0
	ldr	r0, [r0, #456]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_MakeSafeValue_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_MakeSafeValue_object:
Leh_func_begin27:
	push	{r7, lr}
Ltmp69:
	mov	r7, sp
Ltmp70:
Ltmp71:
	sub	sp, sp, #16
	bic	sp, sp, #7
	mov	r0, r1
	cmp	r0, #0
	bne	LBB27_2
	add	r0, sp, #8
	bl	_p_37_plt_System_DateTime_get_UtcNow_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC27_3+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC27_3+8))
LPC27_3:
	add	r0, pc, r0
	ldr	r0, [r0, #456]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
LBB27_2:
	ldr	r1, [r0]
	ldrb	r2, [r1, #22]
	cmp	r2, #0
	bne	LBB27_5
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC27_2+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC27_2+8))
	ldr	r1, [r1]
LPC27_2:
	add	r2, pc, r2
	ldr	r2, [r2, #460]
	ldr	r1, [r1]
	cmp	r1, r2
	bne	LBB27_6
	ldr	r1, [r0, #8]
	ldr	r0, [r0, #12]
	str	r0, [sp, #4]
	str	r1, [sp]
	ldm	sp, {r0, r1}
	bl	_p_36_plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime_llvm
	mov	sp, r7
	pop	{r7, pc}
Ltmp72:
LBB27_5:
	ldr	r0, LCPI27_0
LPC27_0:
	add	r1, pc, r0
	b	LBB27_7
Ltmp73:
LBB27_6:
	ldr	r0, LCPI27_1
LPC27_1:
	add	r1, pc, r0
LBB27_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI27_0:
	.long	Ltmp72-(LPC27_0+8)
LCPI27_1:
	.long	Ltmp73-(LPC27_1+8)
	.end_data_region
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin28:
	push	{r7, lr}
Ltmp74:
	mov	r7, sp
Ltmp75:
Ltmp76:
	bl	_p_32_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo_llvm
	pop	{r7, pc}
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker:
Leh_func_begin29:
	push	{r4, r5, r6, r7, lr}
Ltmp77:
	add	r7, sp, #12
Ltmp78:
Ltmp79:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r5, r1
	bl	_p_38_plt_MonoTouch_Foundation_NSCalendar_get_CurrentCalendar_llvm
	mov	r4, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #296]
	mov	r0, r5
	blx	r1
	mov	r2, r0
	ldr	r0, [r4]
	mov	r1, #224
	ldr	r3, [r0, #76]
	mov	r0, r4
	blx	r3
	mov	r5, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #84]
	mov	r0, r5
	blx	r1
	mov	r4, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #80]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #76]
	mov	r0, r5
	blx	r1
	mov	r3, r0
	mov	r0, #0
	mov	r1, r4
	mov	r2, r6
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
	bl	_p_39_plt_System_TimeSpan__ctor_int_int_int_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC29_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC29_0+8))
LPC29_0:
	add	r0, pc, r0
	ldr	r0, [r0, #464]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #12
	pop	{r4, r5, r6, r7, pc}
Leh_func_end29:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_get_TargetType:
Leh_func_begin30:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC30_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC30_0+8))
LPC30_0:
	add	r0, pc, r0
	ldr	r0, [r0, #468]
	bx	lr
Leh_func_end30:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_MakeSafeValue_object
	.align	3
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_MakeSafeValue_object:
Leh_func_begin31:
	push	{r4, r5, r6, r7, lr}
Ltmp80:
	add	r7, sp, #12
Ltmp81:
	push	{r10, r11}
Ltmp82:
	sub	sp, sp, #68
	bic	sp, sp, #7
	mov	r0, r1
	mov	r1, #0
	str	r1, [sp, #28]
	str	r1, [sp, #36]
	str	r1, [sp, #44]
	str	r1, [sp, #24]
	str	r1, [sp, #32]
	str	r1, [sp, #40]
	cmp	r0, #0
	bne	LBB31_2
	vldr	d0, LCPI31_0
	add	r0, sp, #48
	vmov	r1, r2, d0
	bl	_p_48_plt_System_TimeSpan_FromSeconds_double_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_4+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_4+8))
LPC31_4:
	add	r0, pc, r0
	ldr	r0, [r0, #464]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp, #48]
	str	r1, [r0, #8]
	ldr	r1, [sp, #52]
	str	r1, [r0, #12]
LBB31_2:
	ldr	r1, [r0]
	ldrb	r2, [r1, #22]
	cmp	r2, #0
	bne	LBB31_5
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_2+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_2+8))
	ldr	r1, [r1]
LPC31_2:
	add	r2, pc, r2
	ldr	r2, [r2, #472]
	ldr	r1, [r1]
	cmp	r1, r2
	bne	LBB31_6
	ldr	r1, [r0, #8]
	ldr	r0, [r0, #12]
	add	r6, sp, #32
	str	r0, [sp, #28]
	mov	r0, r6
	str	r1, [sp, #24]
	bl	_p_40_plt_System_DateTime_get_Now_llvm
	mov	r0, r6
	bl	_p_41_plt_System_DateTime_get_Year_llvm
	str	r0, [sp, #20]
	mov	r0, r6
	bl	_p_42_plt_System_DateTime_get_Month_llvm
	mov	r11, r0
	mov	r0, r6
	bl	_p_43_plt_System_DateTime_get_Day_llvm
	add	r5, sp, #24
	mov	r6, r0
	mov	r0, r5
	bl	_p_44_plt_System_TimeSpan_get_Hours_llvm
	mov	r4, r0
	mov	r0, r5
	bl	_p_45_plt_System_TimeSpan_get_Minutes_llvm
	mov	r10, r0
	mov	r0, r5
	bl	_p_46_plt_System_TimeSpan_get_Seconds_llvm
	stm	sp, {r4, r10}
	mov	r1, #2
	str	r0, [sp, #8]
	add	r0, sp, #40
	mov	r2, r11
	mov	r3, r6
	str	r1, [sp, #12]
	ldr	r1, [sp, #20]
	bl	_p_47_plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind_llvm
	ldr	r1, [sp, #44]
	ldr	r0, [sp, #40]
	str	r1, [sp, #60]
	str	r0, [sp, #56]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_3+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC31_3+8))
LPC31_3:
	add	r0, pc, r0
	ldr	r0, [r0, #456]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp, #56]
	str	r1, [r0, #8]
	ldr	r1, [sp, #60]
	str	r1, [r0, #12]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp83:
LBB31_5:
	ldr	r0, LCPI31_1
LPC31_0:
	add	r1, pc, r0
	b	LBB31_7
Ltmp84:
LBB31_6:
	ldr	r0, LCPI31_2
LPC31_1:
	add	r1, pc, r0
LBB31_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	3
	.data_region
LCPI31_0:
	.long	0
	.long	0
LCPI31_1:
	.long	Ltmp83-(LPC31_0+8)
LCPI31_2:
	.long	Ltmp84-(LPC31_1+8)
	.end_data_region
Leh_func_end31:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_View:
Leh_func_begin32:
	push	{r7, lr}
Ltmp85:
	mov	r7, sp
Ltmp86:
Ltmp87:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC32_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC32_0+8))
LPC32_0:
	add	r1, pc, r1
	ldr	r2, [r1, #476]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #16]
	mov	r1, #0
	cmp	r3, r2
	movne	r0, r1
	pop	{r7, pc}
Leh_func_end32:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel:
Leh_func_begin33:
	push	{r4, r7, lr}
Ltmp88:
	add	r7, sp, #4
Ltmp89:
Ltmp90:
	mov	r4, r1
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	cmp	r4, #0
	popne	{r4, r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC33_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC33_0+8))
LPC33_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #480]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end33:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_DefaultMode:
Leh_func_begin34:
	mov	r0, #2
	bx	lr
Leh_func_end34:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_TargetType:
Leh_func_begin35:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC35_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC35_0+8))
LPC35_0:
	add	r0, pc, r0
	ldr	r0, [r0, #484]
	bx	lr
Leh_func_end35:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_SetValueImpl_object_object:
Leh_func_begin36:
	push	{r7, lr}
Ltmp91:
	mov	r7, sp
Ltmp92:
Ltmp93:
	cmp	r1, #0
	beq	LBB36_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC36_3+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC36_3+8))
	ldr	r3, [r1]
LPC36_3:
	add	r0, pc, r0
	ldr	r0, [r0, #476]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #16]
	cmp	r3, r0
	bne	LBB36_5
LBB36_2:
	cmp	r1, #0
	popeq	{r7, pc}
	cmp	r2, #0
	beq	LBB36_4
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC36_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC36_2+8))
	ldr	r3, [r2]
LPC36_2:
	add	r0, pc, r0
	ldr	r0, [r0, #488]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #4]
	cmp	r3, r0
	bne	LBB36_6
LBB36_4:
	ldr	r0, [r1]
	ldr	r3, [r0, #308]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	pop	{r7, pc}
Ltmp94:
LBB36_5:
	ldr	r0, LCPI36_0
LPC36_0:
	add	r1, pc, r0
	b	LBB36_7
Ltmp95:
LBB36_6:
	ldr	r0, LCPI36_1
LPC36_1:
	add	r1, pc, r0
LBB36_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI36_0:
	.long	Ltmp94-(LPC36_0+8)
LCPI36_1:
	.long	Ltmp95-(LPC36_1+8)
	.end_data_region
Leh_func_end36:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin37:
	push	{r4, r5, r6, r7, lr}
Ltmp96:
	add	r7, sp, #12
Ltmp97:
Ltmp98:
	mov	r4, r0
	bl	_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_50_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB37_3
	cmp	r4, #0
	beq	LBB37_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC37_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC37_1+8))
LPC37_1:
	add	r6, pc, r6
	ldr	r0, [r6, #492]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #496]
	str	r0, [r1, #20]
	ldr	r0, [r6, #500]
	str	r0, [r1, #28]
	ldr	r0, [r6, #504]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_51_plt_MonoTouch_UIKit_UISearchBar_add_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_llvm
	pop	{r4, r5, r6, r7, pc}
LBB37_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC37_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC37_2+8))
LPC37_2:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #508]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp99:
LBB37_4:
	ldr	r0, LCPI37_0
LPC37_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI37_0:
	.long	Ltmp99-(LPC37_0+8)
	.end_data_region
Leh_func_end37:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_HandleSearchBarValueChanged_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_HandleSearchBarValueChanged_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs:
Leh_func_begin38:
	push	{r4, r7, lr}
Ltmp100:
	add	r7, sp, #4
Ltmp101:
Ltmp102:
	mov	r4, r0
	bl	_p_50_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #276]
	blx	r1
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end38:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_get_DefaultMode:
Leh_func_begin39:
	mov	r0, #1
	bx	lr
Leh_func_end39:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_Dispose_bool:
Leh_func_begin40:
	push	{r4, r5, r6, r7, lr}
Ltmp103:
	add	r7, sp, #12
Ltmp104:
Ltmp105:
	mov	r5, r1
	mov	r4, r0
	bl	_p_28_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	beq	LBB40_3
	mov	r0, r4
	bl	_p_50_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	cmp	r4, #0
	beq	LBB40_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC40_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC40_1+8))
LPC40_1:
	add	r6, pc, r6
	ldr	r0, [r6, #492]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #496]
	str	r0, [r1, #20]
	ldr	r0, [r6, #500]
	str	r0, [r1, #28]
	ldr	r0, [r6, #504]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_52_plt_MonoTouch_UIKit_UISearchBar_remove_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_llvm
LBB40_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp106:
LBB40_4:
	ldr	r0, LCPI40_0
LPC40_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI40_0:
	.long	Ltmp106-(LPC40_0+8)
	.end_data_region
Leh_func_end40:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View:
Leh_func_begin41:
	push	{r7, lr}
Ltmp107:
	mov	r7, sp
Ltmp108:
Ltmp109:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC41_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC41_0+8))
LPC41_0:
	add	r1, pc, r1
	ldr	r2, [r1, #512]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #20]
	mov	r1, #0
	cmp	r3, r2
	movne	r0, r1
	pop	{r7, pc}
Leh_func_end41:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField:
Leh_func_begin42:
	push	{r4, r5, r6, r7, lr}
Ltmp110:
	add	r7, sp, #12
Ltmp111:
Ltmp112:
	mov	r4, r1
	mov	r5, r0
	bl	_p_53_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding__ctor_object_llvm
	cmp	r5, #0
	beq	LBB42_2
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC42_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC42_1+8))
LPC42_1:
	add	r6, pc, r6
	ldr	r0, [r6, #516]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #520]
	str	r0, [r1, #20]
	ldr	r0, [r6, #524]
	str	r0, [r1, #28]
	ldr	r0, [r6, #528]
	str	r0, [r1, #12]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_54_plt_MonoTouch_UIKit_UITextField_set_ShouldReturn_MonoTouch_UIKit_UITextFieldCondition_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp113:
LBB42_2:
	ldr	r0, LCPI42_0
LPC42_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI42_0:
	.long	Ltmp113-(LPC42_0+8)
	.end_data_region
Leh_func_end42:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_HandleShouldReturn_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_HandleShouldReturn_MonoTouch_UIKit_UITextField:
Leh_func_begin43:
	push	{r4, r5, r6, r7, lr}
Ltmp114:
	add	r7, sp, #12
Ltmp115:
	push	{r8, r10, r11}
Ltmp116:
	mov	r4, r0
	mov	r5, r1
	mov	r10, #0
	ldr	r0, [r4, #16]
	cmp	r0, #0
	beq	LBB43_3
	ldr	r0, [r5]
	ldr	r1, [r0, #344]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	ldr	r0, [r4, #16]
	ldr	r2, [r0]
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC43_0+8))
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC43_0+8))
LPC43_0:
	add	r11, pc, r11
	ldr	r1, [r11, #532]
	sub	r2, r2, #16
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r6
	blx	r2
	tst	r0, #255
	beq	LBB43_3
	ldr	r0, [r5]
	ldr	r1, [r0, #76]
	mov	r0, r5
	blx	r1
	ldr	r0, [r4, #16]
	ldr	r2, [r0]
	ldr	r1, [r11, #536]
	sub	r2, r2, #36
	mov	r8, r1
	mov	r1, r6
	ldr	r2, [r2]
	blx	r2
	mov	r10, #1
LBB43_3:
	mov	r0, r10
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end43:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_DefaultMode:
Leh_func_begin44:
	mov	r0, #2
	bx	lr
Leh_func_end44:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_SetValue_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_SetValue_object:
Leh_func_begin45:
	mov	r9, #0
	cmp	r1, #0
	beq	LBB45_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC45_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC45_0+8))
	ldr	r12, [r1]
LPC45_0:
	add	r2, pc, r2
	ldr	r3, [r2, #540]
	ldrh	r2, [r12, #20]
	cmp	r2, r3
	blo	LBB45_3
	ldr	r2, [r12, #16]
	ldrb	r12, [r2, r3, asr #3]
	and	r3, r3, #7
	mov	r2, #1
	tst	r12, r2, lsl r3
	movne	r9, r1
LBB45_3:
	str	r9, [r0, #16]
	bx	lr
Leh_func_end45:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_TargetType:
Leh_func_begin46:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC46_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC46_0+8))
LPC46_0:
	add	r0, pc, r0
	ldr	r0, [r0, #544]
	bx	lr
Leh_func_end46:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_Dispose_bool:
Leh_func_begin47:
	push	{r7, lr}
Ltmp117:
	mov	r7, sp
Ltmp118:
Ltmp119:
	cmp	r1, #0
	beq	LBB47_2
	bl	_p_55_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r1, [r0]
	mov	r1, #0
	bl	_p_54_plt_MonoTouch_UIKit_UITextField_set_ShouldReturn_MonoTouch_UIKit_UITextFieldCondition_llvm
LBB47_2:
	pop	{r7, pc}
Leh_func_end47:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View:
Leh_func_begin48:
	push	{r7, lr}
Ltmp120:
	mov	r7, sp
Ltmp121:
Ltmp122:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC48_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC48_0+8))
LPC48_0:
	add	r1, pc, r1
	ldr	r2, [r1, #548]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #20]
	mov	r1, #0
	cmp	r3, r2
	movne	r0, r1
	pop	{r7, pc}
Leh_func_end48:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView:
Leh_func_begin49:
	push	{r7, lr}
Ltmp123:
	mov	r7, sp
Ltmp124:
Ltmp125:
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	pop	{r7, pc}
Leh_func_end49:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_EditTextOnChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_EditTextOnChanged_object_System_EventArgs:
Leh_func_begin50:
	push	{r4, r7, lr}
Ltmp126:
	add	r7, sp, #4
Ltmp127:
Ltmp128:
	mov	r4, r0
	bl	_p_56_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View_llvm
	cmp	r0, #0
	popeq	{r4, r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1, #304]
	blx	r1
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end50:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_DefaultMode:
Leh_func_begin51:
	mov	r0, #1
	bx	lr
Leh_func_end51:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SubscribeToEvents
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SubscribeToEvents:
Leh_func_begin52:
	push	{r4, r5, r6, r7, lr}
Ltmp129:
	add	r7, sp, #12
Ltmp130:
Ltmp131:
	mov	r4, r0
	bl	_p_56_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB52_3
	cmp	r4, #0
	beq	LBB52_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC52_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC52_1+8))
LPC52_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #552]
	str	r0, [r1, #20]
	ldr	r0, [r6, #556]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_57_plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler_llvm
	mov	r0, #1
	strb	r0, [r4, #24]
	pop	{r4, r5, r6, r7, pc}
LBB52_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC52_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC52_2+8))
LPC52_2:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #560]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp132:
LBB52_4:
	ldr	r0, LCPI52_0
LPC52_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI52_0:
	.long	Ltmp132-(LPC52_0+8)
	.end_data_region
Leh_func_end52:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_TargetType:
Leh_func_begin53:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC53_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC53_0+8))
LPC53_0:
	add	r0, pc, r0
	ldr	r0, [r0, #484]
	bx	lr
Leh_func_end53:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SetValueImpl_object_object:
Leh_func_begin54:
	push	{r7, lr}
Ltmp133:
	mov	r7, sp
Ltmp134:
Ltmp135:
	cmp	r1, #0
	beq	LBB54_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC54_3+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC54_3+8))
	ldr	r3, [r1]
LPC54_3:
	add	r0, pc, r0
	ldr	r0, [r0, #548]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #20]
	cmp	r3, r0
	bne	LBB54_5
LBB54_2:
	cmp	r1, #0
	popeq	{r7, pc}
	cmp	r2, #0
	beq	LBB54_4
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC54_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC54_2+8))
	ldr	r3, [r2]
LPC54_2:
	add	r0, pc, r0
	ldr	r0, [r0, #488]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #4]
	cmp	r3, r0
	bne	LBB54_6
LBB54_4:
	ldr	r0, [r1]
	ldr	r3, [r0, #300]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	pop	{r7, pc}
Ltmp136:
LBB54_5:
	ldr	r0, LCPI54_0
LPC54_0:
	add	r1, pc, r0
	b	LBB54_7
Ltmp137:
LBB54_6:
	ldr	r0, LCPI54_1
LPC54_1:
	add	r1, pc, r0
LBB54_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI54_0:
	.long	Ltmp136-(LPC54_0+8)
LCPI54_1:
	.long	Ltmp137-(LPC54_1+8)
	.end_data_region
Leh_func_end54:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_Dispose_bool:
Leh_func_begin55:
	push	{r4, r5, r6, r7, lr}
Ltmp138:
	add	r7, sp, #12
Ltmp139:
Ltmp140:
	mov	r4, r0
	cmp	r1, #0
	popeq	{r4, r5, r6, r7, pc}
	mov	r0, r4
	bl	_p_56_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	ldrbne	r0, [r4, #24]
	cmpne	r0, #0
	beq	LBB55_3
	cmp	r4, #0
	beq	LBB55_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC55_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC55_1+8))
LPC55_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #552]
	str	r0, [r1, #20]
	ldr	r0, [r6, #556]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_58_plt_MonoTouch_UIKit_UITextView_remove_Changed_System_EventHandler_llvm
	mov	r0, #0
	strb	r0, [r4, #24]
LBB55_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp141:
LBB55_4:
	ldr	r0, LCPI55_0
LPC55_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI55_0:
	.long	Ltmp141-(LPC55_0+8)
	.end_data_region
Leh_func_end55:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin56:
	push	{r4, r5, r6, r7, lr}
Ltmp142:
	add	r7, sp, #12
Ltmp143:
Ltmp144:
	mov	r4, r0
	bl	_p_59_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch__ctor_object_System_Reflection_PropertyInfo_llvm
	mov	r0, r4
	bl	_p_60_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB56_3
	cmp	r4, #0
	beq	LBB56_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC56_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC56_1+8))
LPC56_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #564]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #568]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_25_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB56_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC56_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC56_2+8))
LPC56_2:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #572]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp145:
LBB56_4:
	ldr	r0, LCPI56_0
LPC56_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI56_0:
	.long	Ltmp145-(LPC56_0+8)
	.end_data_region
Leh_func_end56:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_HandleValueChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_HandleValueChanged_object_System_EventArgs:
Leh_func_begin57:
	push	{r4, r5, r7, lr}
Ltmp146:
	add	r7, sp, #8
Ltmp147:
Ltmp148:
	mov	r4, r0
	bl	_p_60_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View_llvm
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1, #296]
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC57_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC57_0+8))
LPC57_0:
	add	r0, pc, r0
	ldr	r0, [r0, #576]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	strb	r5, [r1, #8]
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end57:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_get_DefaultMode:
Leh_func_begin58:
	mov	r0, #1
	bx	lr
Leh_func_end58:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_Dispose_bool:
Leh_func_begin59:
	push	{r4, r5, r6, r7, lr}
Ltmp149:
	add	r7, sp, #12
Ltmp150:
Ltmp151:
	mov	r5, r1
	mov	r4, r0
	bl	_p_28_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	beq	LBB59_3
	mov	r0, r4
	bl	_p_60_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	cmp	r4, #0
	beq	LBB59_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC59_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC59_1+8))
LPC59_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #564]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #568]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_29_plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler_llvm
LBB59_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp152:
LBB59_4:
	ldr	r0, LCPI59_0
LPC59_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI59_0:
	.long	Ltmp152-(LPC59_0+8)
	.end_data_region
Leh_func_end59:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView:
Leh_func_begin60:
	push	{r7, lr}
Ltmp153:
	mov	r7, sp
Ltmp154:
Ltmp155:
	bl	_p_61_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm
	pop	{r7, pc}
Leh_func_end60:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding_SetValueImpl_object_object:
Leh_func_begin61:
	push	{r4, r5, r7, lr}
Ltmp156:
	add	r7, sp, #8
Ltmp157:
Ltmp158:
	mov	r5, r2
	bl	_p_62_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View_llvm
	mov	r4, r0
	cmp	r4, #0
	popeq	{r4, r5, r7, pc}
	mov	r0, r5
	bl	_p_63_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ConvertToBoolean_object_llvm
	ldr	r1, [r4]
	ldr	r2, [r1, #164]
	uxtb	r1, r0
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end61:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint:
Leh_func_begin62:
	push	{r4, r5, r6, r7, lr}
Ltmp159:
	add	r7, sp, #12
Ltmp160:
	push	{r10, r11}
Ltmp161:
	mov	r10, r3
	mov	r11, r2
	mov	r5, r1
	mov	r4, r0
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC62_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC62_0+8))
LPC62_0:
	add	r0, pc, r0
	ldr	r0, [r0, #580]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r11
	mov	r3, r10
	mov	r6, r0
	bl	_p_64_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm
	str	r6, [r4, #24]
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end62:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_DefaultMode:
Leh_func_begin63:
	mov	r0, #2
	bx	lr
Leh_func_end63:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_TargetType:
Leh_func_begin64:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC64_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC64_0+8))
LPC64_0:
	add	r0, pc, r0
	ldr	r0, [r0, #544]
	bx	lr
Leh_func_end64:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_SetValueImpl_object_object:
Leh_func_begin65:
	push	{r7, lr}
Ltmp162:
	mov	r7, sp
Ltmp163:
Ltmp164:
	ldr	r9, [r0, #24]
	cmp	r2, #0
	beq	LBB65_3
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC65_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC65_2+8))
	ldr	r3, [r2]
LPC65_2:
	add	r1, pc, r1
	ldr	r1, [r1, #540]
	ldrh	r0, [r3, #20]
	cmp	r0, r1
	blo	LBB65_4
	ldr	r0, [r3, #16]
	ldrb	r3, [r0, r1, asr #3]
	and	r1, r1, #7
	mov	r0, #1
	tst	r3, r0, lsl r1
	beq	LBB65_5
LBB65_3:
	ldr	r0, [r9]
	str	r2, [r9, #8]
	pop	{r7, pc}
Ltmp165:
LBB65_4:
	ldr	r0, LCPI65_1
LPC65_1:
	add	r1, pc, r0
	b	LBB65_6
Ltmp166:
LBB65_5:
	ldr	r0, LCPI65_0
LPC65_0:
	add	r1, pc, r0
LBB65_6:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI65_0:
	.long	Ltmp166-(LPC65_0+8)
LCPI65_1:
	.long	Ltmp165-(LPC65_1+8)
	.end_data_region
Leh_func_end65:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_View:
Leh_func_begin66:
	push	{r7, lr}
Ltmp167:
	mov	r7, sp
Ltmp168:
Ltmp169:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC66_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC66_1+8))
	ldr	r2, [r0]
LPC66_1:
	add	r1, pc, r1
	ldr	r1, [r1, #448]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB66_2
	pop	{r7, pc}
Ltmp170:
LBB66_2:
	ldr	r0, LCPI66_0
LPC66_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI66_0:
	.long	Ltmp170-(LPC66_0+8)
	.end_data_region
Leh_func_end66:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView:
Leh_func_begin67:
	push	{r7, lr}
Ltmp171:
	mov	r7, sp
Ltmp172:
Ltmp173:
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	pop	{r7, pc}
Leh_func_end67:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_DefaultMode:
Leh_func_begin68:
	mov	r0, #2
	bx	lr
Leh_func_end68:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_TargetType:
Leh_func_begin69:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC69_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC69_0+8))
LPC69_0:
	add	r0, pc, r0
	ldr	r0, [r0, #584]
	bx	lr
Leh_func_end69:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_SetValueImpl_object_object:
Leh_func_begin70:
	push	{r4, r5, r6, r7, lr}
Ltmp174:
	add	r7, sp, #12
Ltmp175:
Ltmp176:
	mov	r0, r1
	mov	r4, r2
	cmp	r0, #0
	beq	LBB70_2
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC70_4+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC70_4+8))
	ldr	r2, [r0]
LPC70_4:
	add	r1, pc, r1
	ldr	r1, [r1, #448]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r1
	bne	LBB70_9
LBB70_2:
	ldr	r2, [r4]
	ldrb	r1, [r2, #22]
	cmp	r1, #0
	bne	LBB70_10
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC70_3+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC70_3+8))
	ldr	r2, [r2]
LPC70_3:
	add	r1, pc, r1
	ldr	r3, [r1, #588]
	ldr	r2, [r2]
	cmp	r2, r3
	bne	LBB70_11
	ldrb	r2, [r4, #8]
	cmp	r2, #1
	bls	LBB70_6
	ldr	r0, [r1, #444]
	ldr	r5, [r1, #592]
	mov	r1, #1
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	mov	r1, #0
	mov	r2, r4
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	mov	r0, #1
	mov	r1, r5
	mov	r2, r6
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
LBB70_6:
	ldr	r1, [r0]
	ldr	r2, [r1, #164]
	bne	LBB70_8
	mov	r1, #1
	blx	r2
	pop	{r4, r5, r6, r7, pc}
LBB70_8:
	mov	r1, #0
	blx	r2
	pop	{r4, r5, r6, r7, pc}
Ltmp177:
LBB70_9:
	ldr	r0, LCPI70_0
LPC70_0:
	add	r1, pc, r0
	b	LBB70_12
Ltmp178:
LBB70_10:
	ldr	r0, LCPI70_1
LPC70_1:
	add	r1, pc, r0
	b	LBB70_12
Ltmp179:
LBB70_11:
	ldr	r0, LCPI70_2
LPC70_2:
	add	r1, pc, r0
LBB70_12:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI70_0:
	.long	Ltmp177-(LPC70_0+8)
LCPI70_1:
	.long	Ltmp178-(LPC70_1+8)
LCPI70_2:
	.long	Ltmp179-(LPC70_2+8)
	.end_data_region
Leh_func_end70:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView:
Leh_func_begin71:
	push	{r7, lr}
Ltmp180:
	mov	r7, sp
Ltmp181:
Ltmp182:
	bl	_p_61_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm
	pop	{r7, pc}
Leh_func_end71:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding_SetValueImpl_object_object:
Leh_func_begin72:
	push	{r4, r5, r7, lr}
Ltmp183:
	add	r7, sp, #8
Ltmp184:
Ltmp185:
	mov	r5, r2
	bl	_p_62_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View_llvm
	mov	r4, r0
	cmp	r4, #0
	popeq	{r4, r5, r7, pc}
	mov	r0, r5
	bl	_p_63_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ConvertToBoolean_object_llvm
	tst	r0, #255
	ldr	r0, [r4]
	mov	r1, #0
	moveq	r1, #1
	ldr	r2, [r0, #164]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end72:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxBehaviourExtensions_Tap_MonoTouch_UIKit_UIView_uint_uint
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxBehaviourExtensions_Tap_MonoTouch_UIKit_UIView_uint_uint:
Leh_func_begin73:
	push	{r4, r5, r6, r7, lr}
Ltmp186:
	add	r7, sp, #12
Ltmp187:
	push	{r10}
Ltmp188:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC73_0+8))
	mov	r10, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC73_0+8))
LPC73_0:
	add	r0, pc, r0
	ldr	r0, [r0, #580]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, r5
	mov	r3, r10
	mov	r4, r0
	bl	_p_64_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm
	mov	r0, r4
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end73:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_get_Command
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_get_Command:
Leh_func_begin74:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end74:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_set_Command_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_set_Command_System_Windows_Input_ICommand:
Leh_func_begin75:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end75:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object:
Leh_func_begin76:
	push	{r7, lr}
Ltmp189:
	mov	r7, sp
Ltmp190:
	push	{r8}
Ltmp191:
	ldr	r0, [r0, #8]
	cmp	r0, #0
	beq	LBB76_2
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC76_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC76_0+8))
LPC76_0:
	add	r1, pc, r1
	ldr	r1, [r1, #536]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB76_2:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end76:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer:
Leh_func_begin77:
	push	{r4, r5, r7, lr}
Ltmp192:
	add	r7, sp, #8
Ltmp193:
Ltmp194:
	mov	r5, r1
	mov	r4, r2
	ldr	r0, [r5]
	ldr	r1, [r0, #244]
	mov	r0, r5
	blx	r1
	tst	r0, #255
	bne	LBB77_2
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r2, [r0, #240]
	mov	r0, r5
	blx	r2
LBB77_2:
	ldr	r0, [r5]
	mov	r1, r4
	ldr	r2, [r0, #112]
	mov	r0, r5
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end77:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor:
Leh_func_begin78:
	bx	lr
Leh_func_end78:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1_HandleGesture_T
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1_HandleGesture_T:
Leh_func_begin79:
	sub	sp, sp, #4
Ltmp195:
	str	r0, [sp]
	add	sp, sp, #4
	bx	lr
Leh_func_end79:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__ctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__ctor:
Leh_func_begin80:
	push	{r7, lr}
Ltmp196:
	mov	r7, sp
Ltmp197:
Ltmp198:
	sub	sp, sp, #4
	str	r0, [sp]
	bl	_p_65_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor_llvm
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end80:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour_HandleGesture_MonoTouch_UIKit_UITapGestureRecognizer
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour_HandleGesture_MonoTouch_UIKit_UITapGestureRecognizer:
Leh_func_begin81:
	push	{r7, lr}
Ltmp199:
	mov	r7, sp
Ltmp200:
Ltmp201:
	mov	r1, #0
	bl	_p_66_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object_llvm
	pop	{r7, pc}
Leh_func_end81:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint:
Leh_func_begin82:
	push	{r4, r5, r6, r7, lr}
Ltmp202:
	add	r7, sp, #12
Ltmp203:
	push	{r10, r11}
Ltmp204:
	sub	sp, sp, #8
	str	r3, [sp]
	str	r1, [sp, #4]
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC82_0+8))
	mov	r4, r2
	mov	r5, r0
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC82_0+8))
LPC82_0:
	add	r11, pc, r11
	ldr	r1, [r11, #596]
	bl	_p_67_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r6, r0
	ldr	r0, [r11, #600]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r6
	mov	r10, r0
	bl	_p_68_plt_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer__ctor_object_intptr_llvm
	ldr	r0, [r11, #604]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r10
	mov	r6, r0
	bl	_p_69_plt_MonoTouch_UIKit_UITapGestureRecognizer__ctor_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_llvm
	ldr	r0, [r6]
	mov	r1, r4
	ldr	r2, [r0, #80]
	mov	r0, r6
	blx	r2
	ldr	r0, [r6]
	ldr	r1, [sp]
	ldr	r2, [r0, #76]
	mov	r0, r6
	blx	r2
	ldr	r1, [sp, #4]
	mov	r0, r5
	mov	r2, r6
	bl	_p_70_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer_llvm
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end82:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ImageUrl
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ImageUrl:
Leh_func_begin83:
	push	{r7, lr}
Ltmp205:
	mov	r7, sp
Ltmp206:
Ltmp207:
	ldr	r0, [r0, #52]
	ldr	r1, [r0]
	bl	_p_71_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ImageUrl_llvm
	pop	{r7, pc}
Leh_func_end83:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ImageUrl_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ImageUrl_string:
Leh_func_begin84:
	push	{r7, lr}
Ltmp208:
	mov	r7, sp
Ltmp209:
Ltmp210:
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	bl	_p_72_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ImageUrl_string_llvm
	pop	{r7, pc}
Leh_func_end84:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_DefaultImagePath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_DefaultImagePath:
Leh_func_begin85:
	push	{r7, lr}
Ltmp211:
	mov	r7, sp
Ltmp212:
Ltmp213:
	ldr	r0, [r0, #52]
	ldr	r1, [r0]
	bl	_p_73_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_DefaultImagePath_llvm
	pop	{r7, pc}
Leh_func_end85:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_DefaultImagePath_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_DefaultImagePath_string:
Leh_func_begin86:
	push	{r7, lr}
Ltmp214:
	mov	r7, sp
Ltmp215:
Ltmp216:
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	bl	_p_74_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_DefaultImagePath_string_llvm
	pop	{r7, pc}
Leh_func_end86:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ErrorImagePath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ErrorImagePath:
Leh_func_begin87:
	push	{r7, lr}
Ltmp217:
	mov	r7, sp
Ltmp218:
Ltmp219:
	ldr	r0, [r0, #52]
	ldr	r1, [r0]
	bl	_p_75_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ErrorImagePath_llvm
	pop	{r7, pc}
Leh_func_end87:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ErrorImagePath_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ErrorImagePath_string:
Leh_func_begin88:
	push	{r7, lr}
Ltmp220:
	mov	r7, sp
Ltmp221:
Ltmp222:
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	bl	_p_76_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ErrorImagePath_string_llvm
	pop	{r7, pc}
Leh_func_end88:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Action
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Action:
Leh_func_begin89:
	push	{r4, r5, r7, lr}
Ltmp223:
	add	r7, sp, #8
Ltmp224:
Ltmp225:
	mov	r4, r1
	mov	r5, r0
	bl	_p_77_plt_MonoTouch_UIKit_UIImageView__ctor_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_78_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end89:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_intptr:
Leh_func_begin90:
	push	{r4, r7, lr}
Ltmp226:
	add	r7, sp, #4
Ltmp227:
Ltmp228:
	mov	r4, r0
	bl	_p_79_plt_MonoTouch_UIKit_UIImageView__ctor_intptr_llvm
	mov	r0, r4
	mov	r1, #0
	bl	_p_78_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action_llvm
	pop	{r4, r7, pc}
Leh_func_end90:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action:
Leh_func_begin91:
	push	{r4, r5, r6, r7, lr}
Ltmp229:
	add	r7, sp, #12
Ltmp230:
	push	{r10}
Ltmp231:
	mov	r4, r0
	mov	r10, r1
	cmp	r4, #0
	beq	LBB91_2
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC91_1+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC91_1+8))
LPC91_1:
	add	r5, pc, r5
	ldr	r0, [r5, #608]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	ldr	r0, [r5, #612]
	str	r4, [r6, #16]
	str	r0, [r6, #20]
	ldr	r0, [r5, #616]
	str	r0, [r6, #28]
	ldr	r0, [r5, #620]
	str	r0, [r6, #12]
	ldr	r0, [r5, #624]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r6
	mov	r2, r10
	mov	r5, r0
	bl	_p_80_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action_llvm
	str	r5, [r4, #52]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Ltmp232:
LBB91_2:
	ldr	r0, LCPI91_0
LPC91_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI91_0:
	.long	Ltmp232-(LPC91_0+8)
	.end_data_region
Leh_func_end91:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_Dispose_bool:
Leh_func_begin92:
	push	{r4, r5, r7, lr}
Ltmp233:
	add	r7, sp, #8
Ltmp234:
Ltmp235:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB92_2
	ldr	r0, [r5, #52]
	ldr	r1, [r0]
	bl	_p_82_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_Dispose_llvm
LBB92_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_81_plt_MonoTouch_UIKit_UIImageView_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end92:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__InitializeImageHelperb__0
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__InitializeImageHelperb__0:
Leh_func_begin93:
	bx	lr
Leh_func_end93:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ImageUrl
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ImageUrl:
Leh_func_begin94:
	push	{r7, lr}
Ltmp236:
	mov	r7, sp
Ltmp237:
	push	{r8}
Ltmp238:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC94_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC94_0+8))
LPC94_0:
	add	r1, pc, r1
	ldr	r1, [r1, #628]
	sub	r2, r2, #60
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end94:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ImageUrl_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ImageUrl_string:
Leh_func_begin95:
	push	{r7, lr}
Ltmp239:
	mov	r7, sp
Ltmp240:
	push	{r8}
Ltmp241:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC95_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC95_0+8))
LPC95_0:
	add	r3, pc, r3
	ldr	r3, [r3, #632]
	sub	r2, r2, #64
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end95:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_DefaultImagePath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_DefaultImagePath:
Leh_func_begin96:
	push	{r7, lr}
Ltmp242:
	mov	r7, sp
Ltmp243:
	push	{r8}
Ltmp244:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC96_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC96_0+8))
LPC96_0:
	add	r1, pc, r1
	ldr	r1, [r1, #636]
	sub	r2, r2, #40
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end96:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_DefaultImagePath_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_DefaultImagePath_string:
Leh_func_begin97:
	push	{r7, lr}
Ltmp245:
	mov	r7, sp
Ltmp246:
	push	{r8}
Ltmp247:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC97_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC97_0+8))
LPC97_0:
	add	r3, pc, r3
	ldr	r3, [r3, #640]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end97:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ErrorImagePath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ErrorImagePath:
Leh_func_begin98:
	push	{r7, lr}
Ltmp248:
	mov	r7, sp
Ltmp249:
	push	{r8}
Ltmp250:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC98_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC98_0+8))
LPC98_0:
	add	r1, pc, r1
	ldr	r1, [r1, #644]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end98:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ErrorImagePath_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ErrorImagePath_string:
Leh_func_begin99:
	push	{r7, lr}
Ltmp251:
	mov	r7, sp
Ltmp252:
	push	{r8}
Ltmp253:
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC99_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC99_0+8))
LPC99_0:
	add	r3, pc, r3
	ldr	r3, [r3, #648]
	sub	r2, r2, #64
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end99:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper__ctor_System_Func_1_MonoTouch_UIKit_UIImageView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper__ctor_System_Func_1_MonoTouch_UIKit_UIImageView:
Leh_func_begin100:
	push	{r4, r5, r6, r7, lr}
Ltmp254:
	add	r7, sp, #12
Ltmp255:
	push	{r8}
Ltmp256:
	mov	r5, r0
	str	r1, [r5, #8]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC100_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC100_1+8))
LPC100_1:
	add	r6, pc, r6
	ldr	r0, [r6, #652]
	mov	r8, r0
	bl	_p_83_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_llvm
	str	r0, [r5, #12]
	cmp	r5, #0
	ldr	r4, [r5, #12]
	beq	LBB100_2
	ldr	r0, [r6, #656]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #660]
	str	r5, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #664]
	str	r0, [r1, #28]
	ldr	r0, [r6, #668]
	str	r0, [r1, #12]
	ldr	r2, [r4]
	ldr	r0, [r6, #672]
	sub	r2, r2, #24
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp257:
LBB100_2:
	ldr	r0, LCPI100_0
LPC100_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI100_0:
	.long	Ltmp257-(LPC100_0+8)
	.end_data_region
Leh_func_end100:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose:
Leh_func_begin101:
	push	{r4, r7, lr}
Ltmp258:
	add	r7, sp, #4
Ltmp259:
Ltmp260:
	mov	r4, r0
	mov	r1, #1
	ldr	r0, [r4]
	ldr	r2, [r0, #52]
	mov	r0, r4
	blx	r2
	mov	r0, r4
	bl	_p_84_plt_System_GC_SuppressFinalize_object_llvm
	pop	{r4, r7, pc}
Leh_func_end101:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_ImageHelperOnImageChanged_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_ImageHelperOnImageChanged_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage:
Leh_func_begin102:
	push	{r4, r7, lr}
Ltmp261:
	add	r7, sp, #4
Ltmp262:
Ltmp263:
	ldr	r0, [r0, #8]
	mov	r4, r2
	ldr	r1, [r0, #12]
	blx	r1
	cmp	r0, #0
	beq	LBB102_2
	ldr	r1, [r4]
	ldr	r1, [r4, #8]
	cmp	r1, #0
	popeq	{r4, r7, pc}
	ldr	r1, [r4]
	ldr	r1, [r4, #8]
	ldr	r2, [r0]
	ldr	r2, [r2, #264]
	blx	r2
LBB102_2:
	pop	{r4, r7, pc}
Leh_func_end102:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose_bool:
Leh_func_begin103:
	push	{r7, lr}
Ltmp264:
	mov	r7, sp
Ltmp265:
	push	{r8}
Ltmp266:
	cmp	r1, #0
	beq	LBB103_2
	ldr	r0, [r0, #12]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC103_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC103_0+8))
LPC103_0:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB103_2:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end103:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView:
Leh_func_begin104:
	push	{r4, r5, r7, lr}
Ltmp267:
	add	r7, sp, #8
Ltmp268:
Ltmp269:
	mov	r4, r1
	mov	r5, r0
	bl	_p_85_plt_MonoTouch_UIKit_UITableViewSource__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end104:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr:
Leh_func_begin105:
	push	{r4, r7, lr}
Ltmp270:
	add	r7, sp, #4
Ltmp271:
Ltmp272:
	bl	_p_86_plt_MonoTouch_UIKit_UITableViewSource__ctor_intptr_llvm
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC105_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC105_0+8))
LPC105_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #680]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end105:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_TableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_TableView:
Leh_func_begin106:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end106:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectionChangedCommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectionChangedCommand:
Leh_func_begin107:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end107:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand:
Leh_func_begin108:
	str	r1, [r0, #32]
	bx	lr
Leh_func_end108:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_AccessoryTappedCommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_AccessoryTappedCommand:
Leh_func_begin109:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end109:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_AccessoryTappedCommand_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_AccessoryTappedCommand_System_Windows_Input_ICommand:
Leh_func_begin110:
	str	r1, [r0, #36]
	bx	lr
Leh_func_end110:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_AccessoryButtonTapped_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_AccessoryButtonTapped_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin111:
	push	{r4, r5, r6, r7, lr}
Ltmp273:
	add	r7, sp, #12
Ltmp274:
	push	{r8}
Ltmp275:
	ldr	r4, [r0, #36]
	cmp	r4, #0
	beq	LBB111_3
	ldr	r1, [r0]
	ldr	r3, [r1, #188]
	mov	r1, r2
	blx	r3
	ldr	r1, [r4]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC111_0+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC111_0+8))
LPC111_0:
	add	r6, pc, r6
	ldr	r0, [r6, #532]
	sub	r1, r1, #16
	ldr	r2, [r1]
	mov	r1, r5
	mov	r8, r0
	mov	r0, r4
	blx	r2
	tst	r0, #255
	beq	LBB111_3
	ldr	r1, [r4]
	ldr	r0, [r6, #536]
	sub	r1, r1, #36
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
LBB111_3:
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end111:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_RowSelected_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_RowSelected_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin112:
	push	{r4, r5, r7, lr}
Ltmp276:
	add	r7, sp, #8
Ltmp277:
	push	{r8}
Ltmp278:
	mov	r4, r0
	mov	r1, r2
	ldr	r0, [r4]
	ldr	r3, [r0, #188]
	mov	r0, r4
	blx	r3
	mov	r5, r0
	ldr	r0, [r4, #32]
	cmp	r0, #0
	beq	LBB112_2
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC112_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC112_0+8))
LPC112_0:
	add	r1, pc, r1
	ldr	r1, [r1, #536]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r5
	blx	r2
LBB112_2:
	mov	r0, r4
	mov	r1, r5
	bl	_p_88_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end112:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectedItem
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectedItem:
Leh_func_begin113:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end113:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object:
Leh_func_begin114:
	push	{r7, lr}
Ltmp279:
	mov	r7, sp
Ltmp280:
Ltmp281:
	mov	r3, r0
	str	r1, [r3, #24]
	ldr	r0, [r3, #28]
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r9, [r0, #12]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC114_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC114_0+8))
LPC114_0:
	add	r1, pc, r1
	ldr	r1, [r1, #684]
	ldr	r2, [r1]
	mov	r1, r3
	blx	r9
	pop	{r7, pc}
Leh_func_end114:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_add_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_add_SelectedItemChanged_System_EventHandler:
Leh_func_begin115:
	push	{r4, r5, r6, r7, lr}
Ltmp282:
	add	r7, sp, #12
Ltmp283:
	push	{r8, r10, r11}
Ltmp284:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #28]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC115_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC115_2+8))
LPC115_2:
	add	r0, pc, r0
	ldr	r10, [r0, #692]
	beq	LBB115_5
	ldr	r4, [r0, #688]
	b	LBB115_3
LBB115_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB115_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB115_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB115_8
	b	LBB115_2
LBB115_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB115_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB115_8
Ltmp285:
LBB115_7:
	ldr	r0, LCPI115_1
LPC115_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp286:
LBB115_8:
	ldr	r0, LCPI115_0
LPC115_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI115_0:
	.long	Ltmp286-(LPC115_0+8)
LCPI115_1:
	.long	Ltmp285-(LPC115_1+8)
	.end_data_region
Leh_func_end115:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_remove_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_remove_SelectedItemChanged_System_EventHandler:
Leh_func_begin116:
	push	{r4, r5, r6, r7, lr}
Ltmp287:
	add	r7, sp, #12
Ltmp288:
	push	{r8, r10, r11}
Ltmp289:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #28]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC116_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC116_2+8))
LPC116_2:
	add	r0, pc, r0
	ldr	r10, [r0, #692]
	beq	LBB116_5
	ldr	r4, [r0, #688]
	b	LBB116_3
LBB116_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB116_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB116_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB116_8
	b	LBB116_2
LBB116_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB116_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB116_8
Ltmp290:
LBB116_7:
	ldr	r0, LCPI116_1
LPC116_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp291:
LBB116_8:
	ldr	r0, LCPI116_0
LPC116_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI116_0:
	.long	Ltmp291-(LPC116_0+8)
LCPI116_1:
	.long	Ltmp290-(LPC116_1+8)
	.end_data_region
Leh_func_end116:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_GetCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_GetCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin117:
	push	{r4, r5, r6, r7, lr}
Ltmp292:
	add	r7, sp, #12
Ltmp293:
	push	{r8, r10, r11}
Ltmp294:
	mov	r4, r0
	mov	r5, r2
	mov	r11, r1
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #188]
	mov	r0, r4
	blx	r2
	mov	r10, r0
	ldr	r0, [r4]
	mov	r1, r11
	mov	r2, r5
	mov	r3, r10
	ldr	r6, [r0, #192]
	mov	r0, r4
	blx	r6
	mov	r5, r0
	mov	r0, #0
	cmp	r5, #0
	beq	LBB117_3
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC117_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC117_0+8))
	ldr	r2, [r5]
LPC117_0:
	add	r1, pc, r1
	ldr	r1, [r1, #700]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB117_3
	ldr	r0, [r2, #16]
	mov	r2, #1
	ldrb	r0, [r0, r1, asr #3]
	and	r1, r1, #7
	and	r0, r0, r2, lsl r1
	cmp	r0, #0
	movne	r0, r5
LBB117_3:
	cmp	r0, #0
	beq	LBB117_5
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC117_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC117_1+8))
LPC117_1:
	add	r1, pc, r1
	ldr	r1, [r1, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r10
	blx	r2
LBB117_5:
	mov	r0, r5
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end117:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_CellDisplayingEnded_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCell_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_CellDisplayingEnded_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCell_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin118:
	push	{r7, lr}
Ltmp295:
	mov	r7, sp
Ltmp296:
	push	{r8}
Ltmp297:
	mov	r0, #0
	cmp	r2, #0
	beq	LBB118_3
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC118_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC118_0+8))
	ldr	r9, [r2]
LPC118_0:
	add	r1, pc, r1
	ldr	r1, [r1, #700]
	ldrh	r3, [r9, #20]
	cmp	r3, r1
	blo	LBB118_3
	ldr	r0, [r9, #16]
	mov	r3, #1
	ldrb	r0, [r0, r1, asr #3]
	and	r1, r1, #7
	and	r0, r0, r3, lsl r1
	cmp	r0, #0
	moveq	r2, r0
	mov	r0, r2
LBB118_3:
	cmp	r0, #0
	beq	LBB118_5
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC118_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC118_1+8))
LPC118_1:
	add	r1, pc, r1
	ldr	r1, [r1, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB118_5:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end118:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_NumberOfSections_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_NumberOfSections_MonoTouch_UIKit_UITableView:
Leh_func_begin119:
	mov	r0, #1
	bx	lr
Leh_func_end119:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel__ctor_MonoTouch_UIKit_UIPickerView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel__ctor_MonoTouch_UIKit_UIPickerView:
Leh_func_begin120:
	push	{r4, r5, r7, lr}
Ltmp298:
	add	r7, sp, #8
Ltmp299:
Ltmp300:
	mov	r4, r1
	mov	r5, r0
	bl	_p_92_plt_MonoTouch_UIKit_UIPickerViewModel__ctor_llvm
	str	r4, [r5, #20]
	pop	{r4, r5, r7, pc}
Leh_func_end120:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Dispose_bool:
Leh_func_begin121:
	push	{r4, r5, r7, lr}
Ltmp301:
	add	r7, sp, #8
Ltmp302:
	push	{r8}
Ltmp303:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	ldrne	r0, [r5, #28]
	cmpne	r0, #0
	beq	LBB121_2
	ldr	r0, [r5, #28]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC121_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC121_0+8))
LPC121_0:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r5, #28]
LBB121_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_93_plt_MonoTouch_Foundation_NSObject_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end121:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_ItemsSource
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_ItemsSource:
Leh_func_begin122:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end122:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_ItemsSource_System_Collections_IEnumerable
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_ItemsSource_System_Collections_IEnumerable:
Leh_func_begin123:
	push	{r4, r5, r6, r7, lr}
Ltmp304:
	add	r7, sp, #12
Ltmp305:
	push	{r8, r10}
Ltmp306:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #24]
	cmp	r0, r5
	beq	LBB123_9
	ldr	r0, [r4, #28]
	cmp	r0, #0
	beq	LBB123_3
	ldr	r0, [r4, #28]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_1+8))
LPC123_1:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r4, #28]
LBB123_3:
	str	r5, [r4, #24]
	mov	r5, #0
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB123_6
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_0+8))
	ldr	r2, [r0]
LPC123_0:
	add	r1, pc, r1
	ldr	r1, [r1, #712]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB123_6
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	mov	r5, r0
LBB123_6:
	cmp	r5, #0
	beq	LBB123_8
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_2+8))
	mov	r0, r4
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC123_2+8))
LPC123_2:
	add	r6, pc, r6
	ldr	r1, [r6, #704]
	bl	_p_67_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r10, r0
	ldr	r0, [r6, #708]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r10
	mov	r6, r0
	bl	_p_94_plt_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs__ctor_object_intptr_llvm
	mov	r0, r5
	mov	r1, r6
	bl	_p_95_plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm
	str	r0, [r4, #28]
LBB123_8:
	ldr	r0, [r4]
	ldr	r1, [r0, #108]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #100]
	mov	r0, r4
	blx	r1
LBB123_9:
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end123:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin124:
	push	{r4, r5, r7, lr}
Ltmp307:
	add	r7, sp, #8
Ltmp308:
Ltmp309:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC124_0+8))
	mov	r4, r0
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC124_0+8))
LPC124_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r5, [r1, #716]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_96_plt_Cirrious_CrossCore_Mvx_Trace_string_object___llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #108]
	mov	r0, r4
	blx	r1
	pop	{r4, r5, r7, pc}
Leh_func_end124:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Reload
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Reload:
Leh_func_begin125:
	push	{r7, lr}
Ltmp310:
	mov	r7, sp
Ltmp311:
Ltmp312:
	ldr	r0, [r0, #20]
	ldr	r1, [r0]
	ldr	r2, [r1, #268]
	mov	r1, #0
	blx	r2
	pop	{r7, pc}
Leh_func_end125:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView:
Leh_func_begin126:
	mov	r0, #1
	bx	lr
Leh_func_end126:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int:
Leh_func_begin127:
	push	{r7, lr}
Ltmp313:
	mov	r7, sp
Ltmp314:
Ltmp315:
	mov	r1, r0
	mov	r0, #0
	ldr	r2, [r1, #24]
	cmp	r2, #0
	popeq	{r7, pc}
	ldr	r0, [r1, #24]
	bl	_p_97_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable_llvm
	pop	{r7, pc}
Leh_func_end127:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int:
Leh_func_begin128:
	push	{r4, r5, r7, lr}
Ltmp316:
	add	r7, sp, #8
Ltmp317:
Ltmp318:
	mov	r5, r0
	mov	r4, r2
	ldr	r0, [r5, #24]
	cmp	r0, #0
	beq	LBB128_2
	ldr	r0, [r5, #24]
	mov	r1, r4
	bl	_p_98_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int_llvm
	mov	r2, r0
	ldr	r0, [r5]
	mov	r1, r4
	ldr	r3, [r0, #104]
	mov	r0, r5
	blx	r3
	pop	{r4, r5, r7, pc}
LBB128_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC128_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC128_0+8))
LPC128_0:
	add	r0, pc, r0
	ldr	r0, [r0, #720]
	pop	{r4, r5, r7, pc}
Leh_func_end128:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_RowTitle_int_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_RowTitle_int_object:
Leh_func_begin129:
	push	{r7, lr}
Ltmp319:
	mov	r7, sp
Ltmp320:
Ltmp321:
	ldr	r0, [r2]
	ldr	r1, [r0, #32]
	mov	r0, r2
	blx	r1
	pop	{r7, pc}
Leh_func_end129:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int:
Leh_func_begin130:
	push	{r4, r5, r6, r7, lr}
Ltmp322:
	add	r7, sp, #12
Ltmp323:
	push	{r8}
Ltmp324:
	mov	r4, r0
	mov	r1, r2
	ldr	r0, [r4, #24]
	bl	_p_98_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int_llvm
	str	r0, [r4, #32]
	ldr	r0, [r4, #36]
	cmp	r0, #0
	beq	LBB130_2
	ldr	r3, [r0, #12]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC130_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC130_0+8))
LPC130_0:
	add	r1, pc, r1
	ldr	r1, [r1, #684]
	ldr	r2, [r1]
	mov	r1, r4
	blx	r3
LBB130_2:
	ldr	r5, [r4, #40]
	cmp	r5, #0
	beq	LBB130_5
	ldr	r1, [r4, #32]
	ldr	r2, [r5]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC130_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC130_1+8))
LPC130_1:
	add	r6, pc, r6
	ldr	r0, [r6, #532]
	sub	r2, r2, #16
	ldr	r2, [r2]
	mov	r8, r0
	mov	r0, r5
	blx	r2
	tst	r0, #255
	beq	LBB130_5
	ldr	r1, [r4, #32]
	ldr	r2, [r5]
	ldr	r0, [r6, #536]
	sub	r2, r2, #36
	mov	r8, r0
	mov	r0, r5
	ldr	r2, [r2]
	blx	r2
LBB130_5:
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end130:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedItem
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedItem:
Leh_func_begin131:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end131:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedItem_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedItem_object:
Leh_func_begin132:
	push	{r7, lr}
Ltmp325:
	mov	r7, sp
Ltmp326:
Ltmp327:
	str	r1, [r0, #32]
	ldr	r1, [r0]
	ldr	r1, [r1, #100]
	blx	r1
	pop	{r7, pc}
Leh_func_end132:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_add_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_add_SelectedItemChanged_System_EventHandler:
Leh_func_begin133:
	push	{r4, r5, r6, r7, lr}
Ltmp328:
	add	r7, sp, #12
Ltmp329:
	push	{r8, r10, r11}
Ltmp330:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #36]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC133_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC133_2+8))
LPC133_2:
	add	r0, pc, r0
	ldr	r10, [r0, #692]
	beq	LBB133_5
	ldr	r4, [r0, #688]
	b	LBB133_3
LBB133_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB133_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB133_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB133_8
	b	LBB133_2
LBB133_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB133_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB133_8
Ltmp331:
LBB133_7:
	ldr	r0, LCPI133_1
LPC133_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp332:
LBB133_8:
	ldr	r0, LCPI133_0
LPC133_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI133_0:
	.long	Ltmp332-(LPC133_0+8)
LCPI133_1:
	.long	Ltmp331-(LPC133_1+8)
	.end_data_region
Leh_func_end133:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_remove_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_remove_SelectedItemChanged_System_EventHandler:
Leh_func_begin134:
	push	{r4, r5, r6, r7, lr}
Ltmp333:
	add	r7, sp, #12
Ltmp334:
	push	{r8, r10, r11}
Ltmp335:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #36]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC134_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC134_2+8))
LPC134_2:
	add	r0, pc, r0
	ldr	r10, [r0, #692]
	beq	LBB134_5
	ldr	r4, [r0, #688]
	b	LBB134_3
LBB134_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB134_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB134_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB134_8
	b	LBB134_2
LBB134_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB134_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB134_8
Ltmp336:
LBB134_7:
	ldr	r0, LCPI134_1
LPC134_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp337:
LBB134_8:
	ldr	r0, LCPI134_0
LPC134_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI134_0:
	.long	Ltmp337-(LPC134_0+8)
LCPI134_1:
	.long	Ltmp336-(LPC134_1+8)
	.end_data_region
Leh_func_end134:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedChangedCommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedChangedCommand:
Leh_func_begin135:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end135:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedChangedCommand_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedChangedCommand_System_Windows_Input_ICommand:
Leh_func_begin136:
	str	r1, [r0, #40]
	bx	lr
Leh_func_end136:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_ShowSelectedItem
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_ShowSelectedItem:
Leh_func_begin137:
	push	{r4, r5, r7, lr}
Ltmp338:
	add	r7, sp, #8
Ltmp339:
Ltmp340:
	mov	r4, r0
	ldr	r0, [r4, #24]
	cmp	r0, #0
	beq	LBB137_2
	ldr	r0, [r4, #24]
	ldr	r1, [r4, #32]
	bl	_p_99_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_GetPosition_System_Collections_IEnumerable_object_llvm
	mov	r5, r0
	cmp	r5, #0
	poplt	{r4, r5, r7, pc}
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
	ldr	r1, [r1, #168]
	blx	r1
	tst	r0, #255
	ldr	r0, [r4, #20]
	mov	r3, #0
	mov	r2, #0
	moveq	r3, #1
	ldr	r1, [r0]
	ldr	r4, [r1, #264]
	mov	r1, r5
	blx	r4
LBB137_2:
	pop	{r4, r5, r7, pc}
Leh_func_end137:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView:
Leh_func_begin138:
	push	{r7, lr}
Ltmp341:
	mov	r7, sp
Ltmp342:
Ltmp343:
	bl	_p_100_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm
	pop	{r7, pc}
Leh_func_end138:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr:
Leh_func_begin139:
	push	{r4, r7, lr}
Ltmp344:
	add	r7, sp, #4
Ltmp345:
Ltmp346:
	bl	_p_101_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr_llvm
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC139_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC139_0+8))
LPC139_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #724]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end139:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_Dispose_bool:
Leh_func_begin140:
	push	{r4, r5, r7, lr}
Ltmp347:
	add	r7, sp, #8
Ltmp348:
	push	{r8}
Ltmp349:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	ldrne	r0, [r5, #44]
	cmpne	r0, #0
	beq	LBB140_2
	ldr	r0, [r5, #44]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC140_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC140_0+8))
LPC140_0:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r5, #44]
LBB140_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_93_plt_MonoTouch_Foundation_NSObject_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end140:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ItemsSource
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ItemsSource:
Leh_func_begin141:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end141:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ItemsSource_System_Collections_IEnumerable
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ItemsSource_System_Collections_IEnumerable:
Leh_func_begin142:
	push	{r4, r5, r6, r7, lr}
Ltmp350:
	add	r7, sp, #12
Ltmp351:
	push	{r8, r10}
Ltmp352:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #40]
	cmp	r0, r5
	beq	LBB142_9
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB142_3
	ldr	r0, [r4, #44]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_1+8))
LPC142_1:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r4, #44]
LBB142_3:
	str	r5, [r4, #40]
	mov	r5, #0
	ldr	r0, [r4, #40]
	cmp	r0, #0
	beq	LBB142_6
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_0+8))
	ldr	r2, [r0]
LPC142_0:
	add	r1, pc, r1
	ldr	r1, [r1, #712]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB142_6
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	mov	r5, r0
LBB142_6:
	cmp	r5, #0
	beq	LBB142_8
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_2+8))
	mov	r0, r4
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC142_2+8))
LPC142_2:
	add	r6, pc, r6
	ldr	r1, [r6, #728]
	bl	_p_67_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r10, r0
	ldr	r0, [r6, #708]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r10
	mov	r6, r0
	bl	_p_94_plt_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs__ctor_object_intptr_llvm
	mov	r0, r5
	mov	r1, r6
	bl	_p_95_plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm
	str	r0, [r4, #44]
LBB142_8:
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
LBB142_9:
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end142:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin143:
	push	{r4, r5, r7, lr}
Ltmp353:
	add	r7, sp, #8
Ltmp354:
Ltmp355:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5]
	ldr	r1, [r0, #208]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r5]
	ldr	r1, [r0, #208]
	mov	r0, r5
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r5
	bl	_p_98_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end143:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_UseAnimations
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_UseAnimations:
Leh_func_begin144:
	ldrb	r0, [r0, #48]
	bx	lr
Leh_func_end144:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_UseAnimations_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_UseAnimations_bool:
Leh_func_begin145:
	strb	r1, [r0, #48]
	bx	lr
Leh_func_end145:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_AddAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_AddAnimation:
Leh_func_begin146:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end146:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_AddAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_AddAnimation_MonoTouch_UIKit_UITableViewRowAnimation:
Leh_func_begin147:
	str	r1, [r0, #52]
	bx	lr
Leh_func_end147:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_RemoveAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_RemoveAnimation:
Leh_func_begin148:
	ldr	r0, [r0, #56]
	bx	lr
Leh_func_end148:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_RemoveAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_RemoveAnimation_MonoTouch_UIKit_UITableViewRowAnimation:
Leh_func_begin149:
	str	r1, [r0, #56]
	bx	lr
Leh_func_end149:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ReplaceAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ReplaceAnimation:
Leh_func_begin150:
	ldr	r0, [r0, #60]
	bx	lr
Leh_func_end150:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ReplaceAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ReplaceAnimation_MonoTouch_UIKit_UITableViewRowAnimation:
Leh_func_begin151:
	str	r1, [r0, #60]
	bx	lr
Leh_func_end151:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin152:
	push	{r4, r7, lr}
Ltmp356:
	add	r7, sp, #4
Ltmp357:
Ltmp358:
	mov	r4, r0
	ldrb	r0, [r4, #48]
	cmp	r0, #0
	beq	LBB152_2
	mov	r0, r4
	mov	r1, r2
	bl	_p_102_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm
	tst	r0, #255
	popne	{r4, r7, pc}
LBB152_2:
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end152:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin153:
	push	{r4, r5, r6, r7, lr}
Ltmp359:
	add	r7, sp, #12
Ltmp360:
	push	{r8, r10, r11}
Ltmp361:
	mov	r6, r1
	mov	r4, r0
	mov	r10, #0
	ldr	r0, [r6]
	ldr	r0, [r6, #16]
	cmp	r0, #3
	bhi	LBB153_12
	cmp	r0, #1
	beq	LBB153_5
	cmp	r0, #2
	bne	LBB153_6
	ldr	r0, [r6, #12]
	ldr	r1, [r0]
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_2+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_2+8))
LPC153_2:
	add	r2, pc, r2
	ldr	r11, [r2, #732]
	sub	r1, r1, #28
	ldr	r1, [r1]
	mov	r8, r11
	blx	r1
	mov	r5, r0
	ldr	r0, [r6, #8]
	mov	r8, r11
	ldr	r1, [r0]
	sub	r1, r1, #28
	ldr	r1, [r1]
	blx	r1
	cmp	r5, r0
	bne	LBB153_12
	ldr	r0, [r6, #24]
	mov	r1, #0
	bl	_p_104_plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int_llvm
	mov	r6, r0
	ldr	r4, [r4, #20]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_3+8))
	mov	r1, #1
	mov	r10, #1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_3+8))
LPC153_3:
	add	r0, pc, r0
	ldr	r0, [r0, #736]
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	mov	r1, #0
	mov	r2, r6
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r4]
	mov	r1, r5
	mov	r2, #0
	ldr	r3, [r0, #312]
	mov	r0, r4
	blx	r3
	b	LBB153_12
LBB153_5:
	ldr	r5, [r6, #20]
	ldr	r0, [r6, #8]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_1+8))
LPC153_1:
	add	r1, pc, r1
	ldr	r1, [r1, #732]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r1, r0
	mov	r0, r5
	bl	_p_103_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int_llvm
	mov	r1, r0
	ldr	r0, [r4, #20]
	ldr	r2, [r4, #56]
	ldr	r3, [r0]
	ldr	r3, [r3, #316]
	b	LBB153_11
LBB153_6:
	cmp	r0, #3
	bne	LBB153_10
	ldr	r0, [r6, #12]
	ldr	r1, [r0]
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_4+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_4+8))
LPC153_4:
	add	r2, pc, r2
	ldr	r5, [r2, #732]
	sub	r1, r1, #28
	ldr	r1, [r1]
	mov	r8, r5
	blx	r1
	cmp	r0, #1
	beq	LBB153_9
	ldr	r0, [r6, #8]
	mov	r8, r5
	ldr	r1, [r0]
	sub	r1, r1, #28
	ldr	r1, [r1]
	blx	r1
	cmp	r0, #1
	bne	LBB153_12
LBB153_9:
	ldr	r0, [r6, #20]
	mov	r1, #0
	bl	_p_104_plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int_llvm
	mov	r5, r0
	ldr	r0, [r6, #24]
	mov	r1, #0
	bl	_p_104_plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int_llvm
	mov	r2, r0
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
	ldr	r3, [r1, #300]
	mov	r1, r5
	b	LBB153_11
LBB153_10:
	ldr	r5, [r6, #24]
	ldr	r0, [r6, #12]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC153_0+8))
LPC153_0:
	add	r1, pc, r1
	ldr	r1, [r1, #732]
	sub	r2, r2, #28
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r1, r0
	mov	r0, r5
	bl	_p_103_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int_llvm
	mov	r1, r0
	ldr	r0, [r4, #20]
	ldr	r2, [r4, #52]
	ldr	r3, [r0]
	ldr	r3, [r3, #320]
LBB153_11:
	blx	r3
	mov	r10, #1
LBB153_12:
	mov	r0, r10
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end153:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int:
Leh_func_begin154:
	push	{r4, r5, r6, r7, lr}
Ltmp362:
	add	r7, sp, #12
Ltmp363:
	push	{r10}
Ltmp364:
	mov	r10, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC154_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC154_0+8))
LPC154_0:
	add	r0, pc, r0
	ldr	r0, [r0, #736]
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	cmp	r4, #1
	blt	LBB154_3
	mov	r5, #0
LBB154_2:
	add	r0, r10, r5
	mov	r1, #0
	bl	_p_104_plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int_llvm
	mov	r2, r0
	ldr	r0, [r6]
	mov	r1, r5
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	add	r5, r5, #1
	cmp	r4, r5
	bne	LBB154_2
LBB154_3:
	mov	r0, r6
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end154:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_RowsInSection_MonoTouch_UIKit_UITableView_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_RowsInSection_MonoTouch_UIKit_UITableView_int:
Leh_func_begin155:
	push	{r4, r7, lr}
Ltmp365:
	add	r7, sp, #4
Ltmp366:
Ltmp367:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #208]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4]
	ldr	r1, [r0, #208]
	mov	r0, r4
	blx	r1
	bl	_p_97_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable_llvm
	pop	{r4, r7, pc}
Leh_func_end155:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_CellIdentifier
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_CellIdentifier:
Leh_func_begin156:
	ldr	r0, [r0, #68]
	bx	lr
Leh_func_end156:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView:
Leh_func_begin157:
	push	{r5, r7, lr}
Ltmp368:
	add	r7, sp, #4
Ltmp369:
Ltmp370:
	sub	sp, sp, #8
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC157_0+8))
	mov	r5, #0
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC157_0+8))
LPC157_0:
	add	r2, pc, r2
	ldr	r3, [r2, #740]
	ldr	r2, [r2, #744]
	ldr	r2, [r2]
	ldr	r3, [r3]
	stm	sp, {r2, r5}
	mov	r2, #0
	bl	_p_105_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	sub	sp, r7, #4
	pop	{r5, r7, pc}
Leh_func_end157:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSString
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSString:
Leh_func_begin158:
	push	{r5, r7, lr}
Ltmp371:
	add	r7, sp, #4
Ltmp372:
Ltmp373:
	sub	sp, sp, #8
	mov	r3, r2
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC158_0+8))
	mov	r5, #0
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC158_0+8))
LPC158_0:
	add	r2, pc, r2
	ldr	r2, [r2, #744]
	ldr	r2, [r2]
	stm	sp, {r2, r5}
	mov	r2, #0
	bl	_p_105_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	sub	sp, r7, #4
	pop	{r5, r7, pc}
Leh_func_end158:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_string:
Leh_func_begin159:
	push	{r5, r7, lr}
Ltmp374:
	add	r7, sp, #4
Ltmp375:
Ltmp376:
	sub	sp, sp, #8
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC159_0+8))
	mov	r5, #0
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC159_0+8))
LPC159_0:
	add	r3, pc, r3
	ldr	r3, [r3, #740]
	ldr	r3, [r3]
	stm	sp, {r2, r5}
	mov	r2, #0
	bl	_p_106_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	sub	sp, r7, #4
	pop	{r5, r7, pc}
Leh_func_end159:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr:
Leh_func_begin160:
	push	{r4, r7, lr}
Ltmp377:
	add	r7, sp, #4
Ltmp378:
Ltmp379:
	mov	r2, #0
	str	r2, [r0, #76]
	bl	_p_107_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr_llvm
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC160_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC160_0+8))
LPC160_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #748]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end160:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin161:
	push	{r4, r5, r6, r7, lr}
Ltmp380:
	add	r7, sp, #12
Ltmp381:
	push	{r10}
Ltmp382:
	sub	sp, sp, #8
	mov	r4, r0
	ldr	r0, [r7, #8]
	mov	r10, r3
	mov	r5, r2
	mov	r6, r1
	bl	_p_108_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string_llvm
	ldr	r1, [r7, #12]
	mov	r2, r5
	mov	r3, r10
	strd	r0, r1, [sp]
	mov	r0, r4
	mov	r1, r6
	bl	_p_105_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	sub	sp, r7, #16
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end161:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin162:
	push	{r4, r5, r6, r7, lr}
Ltmp383:
	add	r7, sp, #12
Ltmp384:
Ltmp385:
	mov	r4, r0
	mov	r0, #0
	mov	r5, r3
	mov	r6, r2
	str	r0, [r4, #76]
	mov	r0, r4
	bl	_p_109_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm
	ldr	r0, [r7, #8]
	ldr	r1, [r7, #12]
	str	r6, [r4, #72]
	str	r5, [r4, #68]
	str	r0, [r4, #64]
	str	r1, [r4, #76]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end162:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_BindingDescriptions
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_BindingDescriptions:
Leh_func_begin163:
	ldr	r0, [r0, #64]
	bx	lr
Leh_func_end163:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string:
Leh_func_begin164:
	push	{r4, r5, r7, lr}
Ltmp386:
	add	r7, sp, #8
Ltmp387:
	push	{r8}
Ltmp388:
	mov	r4, r0
	cmp	r4, #0
	ldrne	r0, [r4, #8]
	cmpne	r0, #0
	beq	LBB164_2
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC164_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC164_0+8))
LPC164_0:
	add	r5, pc, r5
	ldr	r0, [r5, #752]
	mov	r8, r0
	bl	_p_110_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Binding_Binders_IMvxBindingDescriptionParser_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #756]
	sub	r2, r2, #28
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
LBB164_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC164_1+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC164_1+8))
LPC164_1:
	add	r0, pc, r0
	ldr	r0, [r0, #744]
	ldr	r0, [r0]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end164:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin165:
	push	{r4, r5, r6, r7, lr}
Ltmp389:
	add	r7, sp, #12
Ltmp390:
	push	{r10, r11}
Ltmp391:
	mov	r4, r0
	mov	r6, r1
	mov	r10, r3
	mov	r11, r2
	ldr	r0, [r4]
	ldr	r1, [r0, #216]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_111_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm
	cmp	r0, #0
	bne	LBB165_2
	ldr	r0, [r4]
	mov	r1, r6
	mov	r2, r11
	mov	r3, r10
	ldr	r5, [r0, #212]
	mov	r0, r4
	blx	r5
LBB165_2:
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end165:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_CreateDefaultBindableCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_CreateDefaultBindableCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin166:
	push	{r4, r5, r6, r7, lr}
Ltmp392:
	add	r7, sp, #12
Ltmp393:
	push	{r10, r11}
Ltmp394:
	sub	sp, sp, #4
	mov	r4, r0
	ldr	r10, [r4, #64]
	ldr	r11, [r4, #72]
	ldr	r0, [r4]
	ldr	r1, [r0, #216]
	mov	r0, r4
	blx	r1
	mov	r6, r0
	ldr	r5, [r4, #76]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC166_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC166_0+8))
LPC166_0:
	add	r0, pc, r0
	ldr	r0, [r0, #760]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r10
	mov	r2, r11
	mov	r3, r6
	str	r5, [sp]
	mov	r4, r0
	bl	_p_112_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	mov	r0, r4
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end166:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__cctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__cctor:
Leh_func_begin167:
	push	{r4, r5, r6, r7, lr}
Ltmp395:
	add	r7, sp, #12
Ltmp396:
Ltmp397:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC167_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC167_0+8))
LPC167_0:
	add	r6, pc, r6
	ldr	r0, [r6, #768]
	ldr	r4, [r6, #764]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_113_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	ldr	r0, [r6, #740]
	mov	r1, #1
	str	r5, [r0]
	ldr	r0, [r6, #772]
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r0, [r6, #776]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r6, #780]
	str	r0, [r5, #8]
	ldr	r0, [r6, #784]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #788]
	mov	r2, r5
	ldr	r1, [r1]
	str	r1, [r0, #20]
	str	r0, [r5, #12]
	mov	r1, #0
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r6, #744]
	str	r4, [r0]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end167:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView:
Leh_func_begin168:
	push	{r4, r7, lr}
Ltmp398:
	add	r7, sp, #4
Ltmp399:
Ltmp400:
	mov	r4, r0
	bl	_p_114_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm
	mov	r0, r4
	bl	_p_115_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize_llvm
	pop	{r4, r7, pc}
Leh_func_end168:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_intptr:
Leh_func_begin169:
	push	{r4, r5, r7, lr}
Ltmp401:
	add	r7, sp, #8
Ltmp402:
Ltmp403:
	mov	r4, r0
	bl	_p_116_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr_llvm
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC169_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC169_0+8))
LPC169_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r5, [r1, #792]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	mov	r0, r4
	bl	_p_115_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end169:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin170:
	push	{r4, r7, lr}
Ltmp404:
	add	r7, sp, #4
Ltmp405:
Ltmp406:
	sub	sp, sp, #8
	mov	r4, r0
	ldr	r0, [r7, #12]
	str	r0, [sp, #4]
	ldr	r0, [r7, #8]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_106_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	mov	r0, r4
	bl	_p_115_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end170:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin171:
	push	{r4, r7, lr}
Ltmp407:
	add	r7, sp, #4
Ltmp408:
Ltmp409:
	sub	sp, sp, #8
	mov	r4, r0
	ldr	r0, [r7, #12]
	str	r0, [sp, #4]
	ldr	r0, [r7, #8]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_105_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	mov	r0, r4
	bl	_p_115_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end171:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize:
Leh_func_begin172:
	push	{r4, r5, r6, r7, lr}
Ltmp410:
	add	r7, sp, #12
Ltmp411:
	push	{r10}
Ltmp412:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC172_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC172_0+8))
LPC172_0:
	add	r5, pc, r5
	ldr	r1, [r5, #796]
	bl	_p_67_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r10, r0
	ldr	r0, [r5, #800]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r10
	mov	r6, r0
	bl	_p_117_plt_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_object_intptr_llvm
	str	r6, [r4, #80]
	ldr	r6, [r5, #804]
	ldr	r0, [r6]
	cmp	r0, #0
	bne	LBB172_2
	ldr	r0, [r5, #808]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #812]
	str	r1, [r0, #20]
	ldr	r1, [r5, #816]
	str	r1, [r0, #28]
	ldr	r1, [r5, #820]
	str	r1, [r0, #12]
	str	r0, [r6]
	ldr	r0, [r5, #804]
	ldr	r0, [r0]
LBB172_2:
	str	r0, [r4, #84]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end172:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellCreator
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellCreator:
Leh_func_begin173:
	ldr	r0, [r0, #80]
	bx	lr
Leh_func_end173:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellCreator_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellCreator_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell:
Leh_func_begin174:
	str	r1, [r0, #80]
	bx	lr
Leh_func_end174:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellModifier
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellModifier:
Leh_func_begin175:
	ldr	r0, [r0, #84]
	bx	lr
Leh_func_end175:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellModifier_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellModifier_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell:
Leh_func_begin176:
	str	r1, [r0, #84]
	bx	lr
Leh_func_end176:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifierOverride
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifierOverride:
Leh_func_begin177:
	ldr	r0, [r0, #88]
	bx	lr
Leh_func_end177:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellIdentifierOverride_System_Func_1_MonoTouch_Foundation_NSString
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellIdentifierOverride_System_Func_1_MonoTouch_Foundation_NSString:
Leh_func_begin178:
	str	r1, [r0, #88]
	bx	lr
Leh_func_end178:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifier
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifier:
Leh_func_begin179:
	push	{r7, lr}
Ltmp413:
	mov	r7, sp
Ltmp414:
Ltmp415:
	ldr	r1, [r0, #88]
	cmp	r1, #0
	ldreq	r0, [r0, #68]
	popeq	{r7, pc}
	ldr	r0, [r0, #88]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r7, pc}
Leh_func_end179:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin180:
	push	{r4, r5, r6, r7, lr}
Ltmp416:
	add	r7, sp, #12
Ltmp417:
	push	{r10, r11}
Ltmp418:
	mov	r4, r0
	mov	r5, r1
	mov	r10, r3
	mov	r11, r2
	ldr	r0, [r4]
	ldr	r1, [r0, #216]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_111_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm
	mov	r6, r0
	cmp	r6, #0
	bne	LBB180_3
	ldr	r0, [r4, #80]
	mov	r1, r5
	mov	r2, r11
	mov	r3, r10
	ldr	r6, [r0, #12]
	blx	r6
	ldr	r1, [r4, #84]
	mov	r6, r0
	cmp	r1, #0
	beq	LBB180_3
	ldr	r0, [r4, #84]
	mov	r1, r6
	ldr	r2, [r0, #12]
	blx	r2
LBB180_3:
	mov	r0, r6
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end180:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__Initializeb__2_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__Initializeb__2_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell:
Leh_func_begin181:
	bx	lr
Leh_func_end181:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View:
Leh_func_begin182:
	push	{r7, lr}
Ltmp419:
	mov	r7, sp
Ltmp420:
Ltmp421:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC182_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC182_0+8))
LPC182_0:
	add	r1, pc, r1
	ldr	r2, [r1, #512]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #20]
	mov	r1, #0
	cmp	r3, r2
	movne	r0, r1
	pop	{r7, pc}
Leh_func_end182:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField:
Leh_func_begin183:
	push	{r7, lr}
Ltmp422:
	mov	r7, sp
Ltmp423:
Ltmp424:
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	pop	{r7, pc}
Leh_func_end183:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_HandleEditTextValueChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_HandleEditTextValueChanged_object_System_EventArgs:
Leh_func_begin184:
	push	{r4, r7, lr}
Ltmp425:
	add	r7, sp, #4
Ltmp426:
Ltmp427:
	mov	r4, r0
	bl	_p_118_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View_llvm
	cmp	r0, #0
	popeq	{r4, r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1, #344]
	blx	r1
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end184:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_DefaultMode:
Leh_func_begin185:
	mov	r0, #1
	bx	lr
Leh_func_end185:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SubscribeToEvents
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SubscribeToEvents:
Leh_func_begin186:
	push	{r4, r5, r6, r7, lr}
Ltmp428:
	add	r7, sp, #12
Ltmp429:
Ltmp430:
	mov	r4, r0
	bl	_p_118_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	beq	LBB186_3
	cmp	r4, #0
	beq	LBB186_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC186_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC186_1+8))
LPC186_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #824]
	str	r0, [r1, #20]
	ldr	r0, [r6, #828]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_119_plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler_llvm
	mov	r0, #1
	strb	r0, [r4, #24]
	pop	{r4, r5, r6, r7, pc}
LBB186_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC186_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC186_2+8))
LPC186_2:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #832]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp431:
LBB186_4:
	ldr	r0, LCPI186_0
LPC186_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI186_0:
	.long	Ltmp431-(LPC186_0+8)
	.end_data_region
Leh_func_end186:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_TargetType:
Leh_func_begin187:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC187_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC187_0+8))
LPC187_0:
	add	r0, pc, r0
	ldr	r0, [r0, #484]
	bx	lr
Leh_func_end187:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_ShouldSkipSetValueForViewSpecificReasons_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_ShouldSkipSetValueForViewSpecificReasons_object_object:
Leh_func_begin188:
	push	{r7, lr}
Ltmp432:
	mov	r7, sp
Ltmp433:
Ltmp434:
	bl	_p_120_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ShouldSkipSetValueAsHaveNearlyIdenticalNumericText_Cirrious_MvvmCross_Binding_ExtensionMethods_IMvxEditableTextView_object_object_llvm
	pop	{r7, pc}
Leh_func_end188:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SetValueImpl_object_object:
Leh_func_begin189:
	push	{r7, lr}
Ltmp435:
	mov	r7, sp
Ltmp436:
Ltmp437:
	cmp	r1, #0
	beq	LBB189_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC189_3+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC189_3+8))
	ldr	r3, [r1]
LPC189_3:
	add	r0, pc, r0
	ldr	r0, [r0, #512]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #20]
	cmp	r3, r0
	bne	LBB189_5
LBB189_2:
	cmp	r1, #0
	popeq	{r7, pc}
	cmp	r2, #0
	beq	LBB189_4
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC189_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC189_2+8))
	ldr	r3, [r2]
LPC189_2:
	add	r0, pc, r0
	ldr	r0, [r0, #488]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #4]
	cmp	r3, r0
	bne	LBB189_6
LBB189_4:
	ldr	r0, [r1]
	ldr	r3, [r0, #340]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	pop	{r7, pc}
Ltmp438:
LBB189_5:
	ldr	r0, LCPI189_0
LPC189_0:
	add	r1, pc, r0
	b	LBB189_7
Ltmp439:
LBB189_6:
	ldr	r0, LCPI189_1
LPC189_1:
	add	r1, pc, r0
LBB189_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI189_0:
	.long	Ltmp438-(LPC189_0+8)
LCPI189_1:
	.long	Ltmp439-(LPC189_1+8)
	.end_data_region
Leh_func_end189:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_Dispose_bool:
Leh_func_begin190:
	push	{r4, r5, r6, r7, lr}
Ltmp440:
	add	r7, sp, #12
Ltmp441:
Ltmp442:
	mov	r4, r0
	cmp	r1, #0
	popeq	{r4, r5, r6, r7, pc}
	mov	r0, r4
	bl	_p_118_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	ldrbne	r0, [r4, #24]
	cmpne	r0, #0
	beq	LBB190_3
	cmp	r4, #0
	beq	LBB190_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC190_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC190_1+8))
LPC190_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #824]
	str	r0, [r1, #20]
	ldr	r0, [r6, #828]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_121_plt_MonoTouch_UIKit_UIControl_remove_EditingChanged_System_EventHandler_llvm
	mov	r0, #0
	strb	r0, [r4, #24]
LBB190_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp443:
LBB190_4:
	ldr	r0, LCPI190_0
LPC190_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI190_0:
	.long	Ltmp443-(LPC190_0+8)
	.end_data_region
Leh_func_end190:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_CurrentText
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_CurrentText:
Leh_func_begin191:
	push	{r7, lr}
Ltmp444:
	mov	r7, sp
Ltmp445:
Ltmp446:
	bl	_p_118_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View_llvm
	mov	r1, #0
	cmp	r0, #0
	beq	LBB191_2
	ldr	r1, [r0]
	ldr	r1, [r1, #344]
	blx	r1
	mov	r1, r0
LBB191_2:
	mov	r0, r1
	pop	{r7, pc}
Leh_func_end191:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding__ctor_object_System_Reflection_PropertyInfo:
Leh_func_begin192:
	push	{r7, lr}
Ltmp447:
	mov	r7, sp
Ltmp448:
Ltmp449:
	bl	_p_122_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider__ctor_object_System_Reflection_PropertyInfo_llvm
	pop	{r7, pc}
Leh_func_end192:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SetValueImpl_object_object:
Leh_func_begin193:
	push	{r7, lr}
Ltmp450:
	mov	r7, sp
Ltmp451:
Ltmp452:
	cmp	r1, #0
	beq	LBB193_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC193_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC193_2+8))
	ldr	r3, [r1]
LPC193_2:
	add	r0, pc, r0
	ldr	r0, [r0, #840]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #20]
	cmp	r3, r0
	movne	r1, #0
LBB193_2:
	cmp	r1, #0
	popeq	{r7, pc}
	ldr	r0, [r2]
	ldrb	r3, [r0, #22]
	cmp	r3, #0
	bne	LBB193_5
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC193_3+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC193_3+8))
	ldr	r0, [r0]
LPC193_3:
	add	r3, pc, r3
	ldr	r3, [r3, #836]
	ldr	r0, [r0]
	cmp	r0, r3
	bne	LBB193_6
	ldr	r2, [r2, #8]
	ldr	r0, [r1]
	ldr	r3, [r0, #304]
	mov	r0, r1
	mov	r1, r2
	blx	r3
	pop	{r7, pc}
Ltmp453:
LBB193_5:
	ldr	r0, LCPI193_0
LPC193_0:
	add	r1, pc, r0
	b	LBB193_7
Ltmp454:
LBB193_6:
	ldr	r0, LCPI193_1
LPC193_1:
	add	r1, pc, r0
LBB193_7:
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI193_0:
	.long	Ltmp453-(LPC193_0+8)
LCPI193_1:
	.long	Ltmp454-(LPC193_1+8)
	.end_data_region
Leh_func_end193:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_HandleSliderValueChanged_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_HandleSliderValueChanged_object_System_EventArgs:
Leh_func_begin194:
	push	{r4, r5, r7, lr}
Ltmp455:
	add	r7, sp, #8
Ltmp456:
Ltmp457:
	mov	r4, r0
	bl	_p_123_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View_llvm
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r1, [r0]
	ldr	r1, [r1, #308]
	blx	r1
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC194_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC194_0+8))
LPC194_0:
	add	r0, pc, r0
	ldr	r0, [r0, #844]
	bl	_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	str	r5, [r1, #8]
	ldr	r0, [r4]
	ldr	r2, [r0, #92]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end194:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_get_DefaultMode:
Leh_func_begin195:
	mov	r0, #1
	bx	lr
Leh_func_end195:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SubscribeToEvents
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SubscribeToEvents:
Leh_func_begin196:
	push	{r4, r5, r6, r7, lr}
Ltmp458:
	add	r7, sp, #12
Ltmp459:
Ltmp460:
	mov	r5, r0
	bl	_p_123_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View_llvm
	mov	r4, r0
	cmp	r4, #0
	beq	LBB196_3
	mov	r0, #1
	cmp	r5, #0
	strb	r0, [r5, #28]
	beq	LBB196_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC196_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC196_1+8))
LPC196_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #848]
	str	r0, [r1, #20]
	ldr	r0, [r6, #852]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_25_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm
	pop	{r4, r5, r6, r7, pc}
LBB196_3:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC196_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC196_2+8))
LPC196_2:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #856]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp461:
LBB196_4:
	ldr	r0, LCPI196_0
LPC196_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI196_0:
	.long	Ltmp461-(LPC196_0+8)
	.end_data_region
Leh_func_end196:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_Dispose_bool:
Leh_func_begin197:
	push	{r4, r5, r6, r7, lr}
Ltmp462:
	add	r7, sp, #12
Ltmp463:
Ltmp464:
	mov	r5, r1
	mov	r4, r0
	bl	_p_28_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	mov	r0, r4
	bl	_p_123_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View_llvm
	mov	r5, r0
	cmp	r5, #0
	ldrbne	r0, [r4, #28]
	cmpne	r0, #0
	beq	LBB197_3
	cmp	r4, #0
	beq	LBB197_4
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC197_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC197_1+8))
LPC197_1:
	add	r6, pc, r6
	ldr	r0, [r6, #424]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #848]
	str	r0, [r1, #20]
	ldr	r0, [r6, #852]
	str	r0, [r1, #28]
	ldr	r0, [r6, #436]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_29_plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler_llvm
	mov	r0, #0
	strb	r0, [r4, #28]
LBB197_3:
	pop	{r4, r5, r6, r7, pc}
Ltmp465:
LBB197_4:
	ldr	r0, LCPI197_0
LPC197_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI197_0:
	.long	Ltmp465-(LPC197_0+8)
	.end_data_region
Leh_func_end197:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_Button
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_Button:
Leh_func_begin198:
	push	{r7, lr}
Ltmp466:
	mov	r7, sp
Ltmp467:
Ltmp468:
	bl	_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm
	cmp	r0, #0
	popeq	{r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC198_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC198_0+8))
LPC198_0:
	add	r1, pc, r1
	ldr	r2, [r1, #860]
	ldr	r1, [r0]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r3, [r1, #20]
	mov	r1, #0
	cmp	r3, r2
	movne	r0, r1
	pop	{r7, pc}
Leh_func_end198:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton:
Leh_func_begin199:
	push	{r4, r7, lr}
Ltmp469:
	add	r7, sp, #4
Ltmp470:
Ltmp471:
	mov	r4, r1
	bl	_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm
	cmp	r4, #0
	popne	{r4, r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC199_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC199_0+8))
LPC199_0:
	add	r1, pc, r1
	ldr	r0, [r1, #444]
	ldr	r4, [r1, #864]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, #2
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end199:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_DefaultMode
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_DefaultMode:
Leh_func_begin200:
	mov	r0, #2
	bx	lr
Leh_func_end200:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_TargetType
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_TargetType:
Leh_func_begin201:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC201_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC201_0+8))
LPC201_0:
	add	r0, pc, r0
	ldr	r0, [r0, #484]
	bx	lr
Leh_func_end201:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_SetValueImpl_object_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_SetValueImpl_object_object:
Leh_func_begin202:
	push	{r7, lr}
Ltmp472:
	mov	r7, sp
Ltmp473:
Ltmp474:
	cmp	r1, #0
	beq	LBB202_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC202_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC202_2+8))
	ldr	r3, [r1]
LPC202_2:
	add	r0, pc, r0
	ldr	r0, [r0, #860]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #20]
	cmp	r3, r0
	bne	LBB202_6
LBB202_2:
	cmp	r2, #0
	beq	LBB202_4
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC202_1+8))
	mov	r3, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC202_1+8))
LPC202_1:
	add	r0, pc, r0
	ldr	r9, [r0, #488]
	ldr	r0, [r2]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #4]
	cmp	r0, r9
	moveq	r3, r2
	b	LBB202_5
LBB202_4:
	mov	r3, r2
LBB202_5:
	ldr	r0, [r1]
	mov	r2, #0
	ldr	r9, [r0, #308]
	mov	r0, r1
	mov	r1, r3
	blx	r9
	pop	{r7, pc}
Ltmp475:
LBB202_6:
	ldr	r0, LCPI202_0
LPC202_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI202_0:
	.long	Ltmp475-(LPC202_0+8)
	.end_data_region
Leh_func_end202:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_DefaultCellIdentifier
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_DefaultCellIdentifier:
Leh_func_begin203:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end203:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView:
Leh_func_begin204:
	push	{r7, lr}
Ltmp476:
	mov	r7, sp
Ltmp477:
Ltmp478:
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC204_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC204_0+8))
LPC204_0:
	add	r2, pc, r2
	ldr	r2, [r2, #868]
	ldr	r2, [r2]
	bl	_p_124_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString_llvm
	pop	{r7, pc}
Leh_func_end204:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString:
Leh_func_begin205:
	push	{r4, r7, lr}
Ltmp479:
	add	r7, sp, #4
Ltmp480:
	push	{r10, r11}
Ltmp481:
	mov	r10, r2
	mov	r11, r1
	mov	r4, r0
	bl	_p_125_plt_MonoTouch_UIKit_UICollectionViewSource__ctor_llvm
	strd	r10, r11, [r4, #20]
	pop	{r10, r11}
	pop	{r4, r7, pc}
Leh_func_end205:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_CollectionView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_CollectionView:
Leh_func_begin206:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end206:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectionChangedCommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectionChangedCommand:
Leh_func_begin207:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end207:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand:
Leh_func_begin208:
	str	r1, [r0, #36]
	bx	lr
Leh_func_end208:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin209:
	push	{r4, r5, r7, lr}
Ltmp482:
	add	r7, sp, #8
Ltmp483:
Ltmp484:
	mov	r5, r1
	ldr	r1, [r0]
	mov	r4, r2
	ldr	r1, [r1, #128]
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	mov	r2, r4
	ldr	r3, [r0, #296]
	mov	r0, r5
	blx	r3
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC209_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC209_1+8))
	ldr	r2, [r0]
LPC209_1:
	add	r1, pc, r1
	ldr	r1, [r1, #872]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #20]
	cmp	r2, r1
	bne	LBB209_2
	pop	{r4, r5, r7, pc}
Ltmp485:
LBB209_2:
	ldr	r0, LCPI209_0
LPC209_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI209_0:
	.long	Ltmp485-(LPC209_0+8)
	.end_data_region
Leh_func_end209:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ItemSelected_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ItemSelected_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin210:
	push	{r4, r5, r7, lr}
Ltmp486:
	add	r7, sp, #8
Ltmp487:
	push	{r8}
Ltmp488:
	mov	r4, r0
	mov	r1, r2
	ldr	r0, [r4]
	ldr	r3, [r0, #116]
	mov	r0, r4
	blx	r3
	mov	r5, r0
	ldr	r0, [r4, #36]
	cmp	r0, #0
	beq	LBB210_2
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC210_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC210_0+8))
LPC210_0:
	add	r1, pc, r1
	ldr	r1, [r1, #536]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r5
	blx	r2
LBB210_2:
	mov	r0, r4
	mov	r1, r5
	bl	_p_126_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end210:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectedItem
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectedItem:
Leh_func_begin211:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end211:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object:
Leh_func_begin212:
	push	{r7, lr}
Ltmp489:
	mov	r7, sp
Ltmp490:
Ltmp491:
	mov	r3, r0
	str	r1, [r3, #28]
	ldr	r0, [r3, #32]
	cmp	r0, #0
	popeq	{r7, pc}
	ldr	r9, [r0, #12]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC212_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC212_0+8))
LPC212_0:
	add	r1, pc, r1
	ldr	r1, [r1, #684]
	ldr	r2, [r1]
	mov	r1, r3
	blx	r9
	pop	{r7, pc}
Leh_func_end212:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_add_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_add_SelectedItemChanged_System_EventHandler:
Leh_func_begin213:
	push	{r4, r5, r6, r7, lr}
Ltmp492:
	add	r7, sp, #12
Ltmp493:
	push	{r8, r10, r11}
Ltmp494:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #32]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC213_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC213_2+8))
LPC213_2:
	add	r0, pc, r0
	ldr	r10, [r0, #692]
	beq	LBB213_5
	ldr	r4, [r0, #688]
	b	LBB213_3
LBB213_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB213_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB213_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB213_8
	b	LBB213_2
LBB213_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB213_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB213_8
Ltmp495:
LBB213_7:
	ldr	r0, LCPI213_1
LPC213_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp496:
LBB213_8:
	ldr	r0, LCPI213_0
LPC213_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI213_0:
	.long	Ltmp496-(LPC213_0+8)
LCPI213_1:
	.long	Ltmp495-(LPC213_1+8)
	.end_data_region
Leh_func_end213:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_remove_SelectedItemChanged_System_EventHandler
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_remove_SelectedItemChanged_System_EventHandler:
Leh_func_begin214:
	push	{r4, r5, r6, r7, lr}
Ltmp497:
	add	r7, sp, #12
Ltmp498:
	push	{r8, r10, r11}
Ltmp499:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #32]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC214_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC214_2+8))
LPC214_2:
	add	r0, pc, r0
	ldr	r11, [r0, #688]
	ldr	r10, [r0, #692]
LBB214_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB214_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB214_6
LBB214_3:
	cmp	r5, #0
	beq	LBB214_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB214_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp500:
LBB214_6:
	ldr	r0, LCPI214_0
LPC214_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp501:
LBB214_7:
	ldr	r0, LCPI214_1
LPC214_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI214_0:
	.long	Ltmp500-(LPC214_0+8)
LCPI214_1:
	.long	Ltmp501-(LPC214_1+8)
	.end_data_region
Leh_func_end214:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin215:
	push	{r4, r5, r6, r7, lr}
Ltmp502:
	add	r7, sp, #12
Ltmp503:
	push	{r8, r10, r11}
Ltmp504:
	mov	r4, r0
	mov	r5, r2
	mov	r11, r1
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #116]
	mov	r0, r4
	blx	r2
	mov	r10, r0
	ldr	r0, [r4]
	mov	r1, r11
	mov	r2, r5
	mov	r3, r10
	ldr	r6, [r0, #120]
	mov	r0, r4
	blx	r6
	mov	r5, r0
	mov	r0, #0
	cmp	r5, #0
	beq	LBB215_3
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC215_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC215_0+8))
	ldr	r2, [r5]
LPC215_0:
	add	r1, pc, r1
	ldr	r1, [r1, #700]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB215_3
	ldr	r0, [r2, #16]
	mov	r2, #1
	ldrb	r0, [r0, r1, asr #3]
	and	r1, r1, #7
	and	r0, r0, r2, lsl r1
	cmp	r0, #0
	movne	r0, r5
LBB215_3:
	cmp	r0, #0
	beq	LBB215_5
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC215_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC215_1+8))
LPC215_1:
	add	r1, pc, r1
	ldr	r1, [r1, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, r10
	blx	r2
LBB215_5:
	mov	r0, r5
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end215:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_CellDisplayingEnded_MonoTouch_UIKit_UICollectionView_MonoTouch_UIKit_UICollectionViewCell_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_CellDisplayingEnded_MonoTouch_UIKit_UICollectionView_MonoTouch_UIKit_UICollectionViewCell_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin216:
	push	{r7, lr}
Ltmp505:
	mov	r7, sp
Ltmp506:
	push	{r8}
Ltmp507:
	mov	r0, #0
	cmp	r2, #0
	beq	LBB216_3
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC216_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC216_0+8))
	ldr	r9, [r2]
LPC216_0:
	add	r1, pc, r1
	ldr	r1, [r1, #700]
	ldrh	r3, [r9, #20]
	cmp	r3, r1
	blo	LBB216_3
	ldr	r0, [r9, #16]
	mov	r3, #1
	ldrb	r0, [r0, r1, asr #3]
	and	r1, r1, #7
	and	r0, r0, r3, lsl r1
	cmp	r0, #0
	moveq	r2, r0
	mov	r0, r2
LBB216_3:
	cmp	r0, #0
	beq	LBB216_5
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC216_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC216_1+8))
LPC216_1:
	add	r1, pc, r1
	ldr	r1, [r1, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB216_5:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end216:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_NumberOfSections_MonoTouch_UIKit_UICollectionView:
Leh_func_begin217:
	mov	r0, #1
	bx	lr
Leh_func_end217:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__cctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__cctor:
Leh_func_begin218:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC218_0+8))
	mov	r1, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC218_0+8))
LPC218_0:
	add	r0, pc, r0
	ldr	r0, [r0, #868]
	str	r1, [r0]
	bx	lr
Leh_func_end218:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView:
Leh_func_begin219:
	push	{r7, lr}
Ltmp508:
	mov	r7, sp
Ltmp509:
Ltmp510:
	bl	_p_127_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_llvm
	pop	{r7, pc}
Leh_func_end219:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_Dispose_bool:
Leh_func_begin220:
	push	{r4, r5, r7, lr}
Ltmp511:
	add	r7, sp, #8
Ltmp512:
	push	{r8}
Ltmp513:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	ldrne	r0, [r5, #44]
	cmpne	r0, #0
	beq	LBB220_2
	ldr	r0, [r5, #44]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC220_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC220_0+8))
LPC220_0:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r5, #44]
LBB220_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_93_plt_MonoTouch_Foundation_NSObject_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end220:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString:
Leh_func_begin221:
	push	{r7, lr}
Ltmp514:
	mov	r7, sp
Ltmp515:
Ltmp516:
	bl	_p_124_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString_llvm
	pop	{r7, pc}
Leh_func_end221:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_get_ItemsSource
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_get_ItemsSource:
Leh_func_begin222:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end222:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_set_ItemsSource_System_Collections_IEnumerable
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_set_ItemsSource_System_Collections_IEnumerable:
Leh_func_begin223:
	push	{r4, r5, r6, r7, lr}
Ltmp517:
	add	r7, sp, #12
Ltmp518:
	push	{r8}
Ltmp519:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #40]
	cmp	r0, r5
	beq	LBB223_10
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB223_3
	ldr	r0, [r4, #44]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_3+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_3+8))
LPC223_3:
	add	r1, pc, r1
	ldr	r1, [r1, #676]
	sub	r2, r2, #20
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, #0
	str	r0, [r4, #44]
LBB223_3:
	str	r5, [r4, #40]
	mov	r5, #0
	ldr	r0, [r4, #40]
	cmp	r0, #0
	beq	LBB223_6
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_1+8))
	ldr	r2, [r0]
LPC223_1:
	add	r1, pc, r1
	ldr	r1, [r1, #712]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB223_6
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	mov	r5, r0
LBB223_6:
	cmp	r5, #0
	beq	LBB223_9
	cmp	r4, #0
	beq	LBB223_11
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_2+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC223_2+8))
LPC223_2:
	add	r6, pc, r6
	ldr	r0, [r6, #708]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r6, #876]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r6, #880]
	str	r0, [r1, #28]
	ldr	r0, [r6, #884]
	str	r0, [r1, #12]
	mov	r0, r5
	bl	_p_95_plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm
	str	r0, [r4, #44]
LBB223_9:
	ldr	r0, [r4]
	ldr	r1, [r0, #124]
	mov	r0, r4
	blx	r1
LBB223_10:
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp520:
LBB223_11:
	ldr	r0, LCPI223_0
LPC223_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI223_0:
	.long	Ltmp520-(LPC223_0+8)
	.end_data_region
Leh_func_end223:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath:
Leh_func_begin224:
	push	{r4, r5, r7, lr}
Ltmp521:
	add	r7, sp, #8
Ltmp522:
Ltmp523:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5]
	ldr	r1, [r0, #136]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r5]
	ldr	r1, [r0, #136]
	mov	r0, r5
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r5
	bl	_p_98_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end224:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin225:
	push	{r7, lr}
Ltmp524:
	mov	r7, sp
Ltmp525:
Ltmp526:
	ldr	r1, [r0]
	ldr	r1, [r1, #124]
	blx	r1
	pop	{r7, pc}
Leh_func_end225:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int:
Leh_func_begin226:
	push	{r4, r7, lr}
Ltmp527:
	add	r7, sp, #4
Ltmp528:
Ltmp529:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #136]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, #0
	popeq	{r4, r7, pc}
	ldr	r0, [r4]
	ldr	r1, [r0, #136]
	mov	r0, r4
	blx	r1
	bl	_p_97_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable_llvm
	pop	{r4, r7, pc}
Leh_func_end226:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_BindingContext:
Leh_func_begin227:
	ldr	r0, [r0, #72]
	bx	lr
Leh_func_end227:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin228:
	str	r1, [r0, #72]
	bx	lr
Leh_func_end228:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor:
Leh_func_begin229:
	push	{r7, lr}
Ltmp530:
	mov	r7, sp
Ltmp531:
Ltmp532:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC229_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC229_0+8))
LPC229_0:
	add	r1, pc, r1
	ldr	r1, [r1, #788]
	ldr	r1, [r1]
	bl	_p_128_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_llvm
	pop	{r7, pc}
Leh_func_end229:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string:
Leh_func_begin230:
	push	{r4, r5, r7, lr}
Ltmp533:
	add	r7, sp, #8
Ltmp534:
Ltmp535:
	mov	r4, r1
	mov	r5, r0
	bl	_p_129_plt_MonoTouch_UIKit_UITableViewCell__ctor_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end230:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:
Leh_func_begin231:
	push	{r4, r5, r7, lr}
Ltmp536:
	add	r7, sp, #8
Ltmp537:
Ltmp538:
	mov	r4, r1
	mov	r5, r0
	bl	_p_129_plt_MonoTouch_UIKit_UITableViewCell__ctor_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_131_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end231:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_intptr:
Leh_func_begin232:
	push	{r7, lr}
Ltmp539:
	mov	r7, sp
Ltmp540:
Ltmp541:
	mov	r2, r1
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC232_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC232_0+8))
LPC232_0:
	add	r1, pc, r1
	ldr	r1, [r1, #788]
	ldr	r1, [r1]
	bl	_p_132_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr_llvm
	pop	{r7, pc}
Leh_func_end232:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr:
Leh_func_begin233:
	push	{r4, r5, r7, lr}
Ltmp542:
	add	r7, sp, #8
Ltmp543:
Ltmp544:
	mov	r4, r1
	mov	r1, r2
	mov	r5, r0
	bl	_p_133_plt_MonoTouch_UIKit_UITableViewCell__ctor_intptr_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end233:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr:
Leh_func_begin234:
	push	{r4, r5, r7, lr}
Ltmp545:
	add	r7, sp, #8
Ltmp546:
Ltmp547:
	mov	r4, r1
	mov	r1, r2
	mov	r5, r0
	bl	_p_133_plt_MonoTouch_UIKit_UITableViewCell__ctor_intptr_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_131_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end234:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin235:
	push	{r4, r5, r7, lr}
Ltmp548:
	add	r7, sp, #8
Ltmp549:
Ltmp550:
	mov	r4, r1
	mov	r1, r2
	mov	r2, r3
	mov	r5, r0
	bl	_p_134_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_llvm
	ldr	r0, [r5]
	ldr	r1, [r7, #8]
	ldr	r2, [r0, #272]
	mov	r0, r5
	blx	r2
	mov	r0, r5
	mov	r1, r4
	bl	_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end235:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin236:
	push	{r4, r5, r7, lr}
Ltmp551:
	add	r7, sp, #8
Ltmp552:
Ltmp553:
	mov	r4, r1
	mov	r1, r2
	mov	r2, r3
	mov	r5, r0
	bl	_p_134_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_llvm
	ldr	r0, [r5]
	ldr	r1, [r7, #8]
	ldr	r2, [r0, #272]
	mov	r0, r5
	blx	r2
	mov	r0, r5
	mov	r1, r4
	bl	_p_131_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end236:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_Accessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_Accessory:
Leh_func_begin237:
	push	{r7, lr}
Ltmp554:
	mov	r7, sp
Ltmp555:
Ltmp556:
	bl	_p_135_plt_MonoTouch_UIKit_UITableViewCell_get_Accessory_llvm
	pop	{r7, pc}
Leh_func_end237:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin238:
	push	{r7, lr}
Ltmp557:
	mov	r7, sp
Ltmp558:
Ltmp559:
	bl	_p_136_plt_MonoTouch_UIKit_UITableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	pop	{r7, pc}
Leh_func_end238:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool:
Leh_func_begin239:
	push	{r4, r5, r7, lr}
Ltmp560:
	add	r7, sp, #8
Ltmp561:
	push	{r8}
Ltmp562:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB239_2
	ldr	r0, [r5, #72]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC239_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC239_0+8))
LPC239_0:
	add	r1, pc, r1
	ldr	r1, [r1, #888]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB239_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_137_plt_MonoTouch_UIKit_UITableViewCell_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end239:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_DataContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_DataContext:
Leh_func_begin240:
	push	{r7, lr}
Ltmp563:
	mov	r7, sp
Ltmp564:
	push	{r8}
Ltmp565:
	ldr	r0, [r0, #72]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC240_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC240_0+8))
LPC240_0:
	add	r1, pc, r1
	ldr	r1, [r1, #892]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end240:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_DataContext_object:
Leh_func_begin241:
	push	{r7, lr}
Ltmp566:
	mov	r7, sp
Ltmp567:
	push	{r8}
Ltmp568:
	ldr	r0, [r0, #72]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC241_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC241_0+8))
LPC241_0:
	add	r3, pc, r3
	ldr	r3, [r3, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end241:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_intptr:
Leh_func_begin242:
	push	{r4, r7, lr}
Ltmp569:
	add	r7, sp, #4
Ltmp570:
Ltmp571:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC242_0+8))
	mov	r2, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC242_0+8))
LPC242_0:
	add	r0, pc, r0
	ldr	r1, [r0, #780]
	mov	r0, r4
	bl	_p_138_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr_llvm
	mov	r0, r4
	bl	_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm
	pop	{r4, r7, pc}
Leh_func_end242:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr:
Leh_func_begin243:
	push	{r4, r7, lr}
Ltmp572:
	add	r7, sp, #4
Ltmp573:
Ltmp574:
	mov	r4, r0
	bl	_p_132_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr_llvm
	mov	r0, r4
	bl	_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm
	pop	{r4, r7, pc}
Leh_func_end243:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr:
Leh_func_begin244:
	push	{r4, r7, lr}
Ltmp575:
	add	r7, sp, #4
Ltmp576:
Ltmp577:
	mov	r4, r0
	bl	_p_140_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr_llvm
	mov	r0, r4
	bl	_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm
	pop	{r4, r7, pc}
Leh_func_end244:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin245:
	push	{r4, r7, lr}
Ltmp578:
	add	r7, sp, #4
Ltmp579:
Ltmp580:
	sub	sp, sp, #4
	mov	r4, r0
	ldr	r0, [r7, #8]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_141_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	mov	r0, r4
	bl	_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end245:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory:
Leh_func_begin246:
	push	{r4, r7, lr}
Ltmp581:
	add	r7, sp, #4
Ltmp582:
Ltmp583:
	sub	sp, sp, #4
	mov	r4, r0
	ldr	r0, [r7, #8]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_142_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm
	mov	r0, r4
	bl	_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end246:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader:
Leh_func_begin247:
	push	{r4, r5, r6, r7, lr}
Ltmp584:
	add	r7, sp, #12
Ltmp585:
	push	{r10, r11}
Ltmp586:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB247_2
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC247_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC247_1+8))
LPC247_1:
	add	r6, pc, r6
	ldr	r0, [r6, #608]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #16]
	ldr	r0, [r6, #896]
	str	r0, [r5, #20]
	ldr	r0, [r6, #900]
	str	r0, [r5, #28]
	ldr	r0, [r6, #620]
	str	r0, [r5, #12]
	mov	r0, r4
	ldr	r1, [r6, #904]
	bl	_p_67_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r10, r0
	ldr	r0, [r6, #908]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r10
	mov	r11, r0
	bl	_p_143_plt_System_Action__ctor_object_intptr_llvm
	ldr	r0, [r6, #624]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	mov	r2, r11
	mov	r6, r0
	bl	_p_80_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action_llvm
	str	r6, [r4, #76]
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp587:
LBB247_2:
	ldr	r0, LCPI247_0
LPC247_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI247_0:
	.long	Ltmp587-(LPC247_0+8)
	.end_data_region
Leh_func_end247:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageLoader
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageLoader:
Leh_func_begin248:
	ldr	r0, [r0, #76]
	bx	lr
Leh_func_end248:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_TitleText
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_TitleText:
Leh_func_begin249:
	push	{r7, lr}
Ltmp588:
	mov	r7, sp
Ltmp589:
Ltmp590:
	ldr	r1, [r0]
	ldr	r1, [r1, #296]
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #312]
	blx	r1
	pop	{r7, pc}
Leh_func_end249:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_TitleText_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_TitleText_string:
Leh_func_begin250:
	push	{r4, r7, lr}
Ltmp591:
	add	r7, sp, #4
Ltmp592:
Ltmp593:
	mov	r4, r1
	ldr	r1, [r0]
	ldr	r1, [r1, #296]
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #308]
	mov	r1, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end250:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_DetailText
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_DetailText:
Leh_func_begin251:
	push	{r7, lr}
Ltmp594:
	mov	r7, sp
Ltmp595:
Ltmp596:
	ldr	r1, [r0]
	ldr	r1, [r1, #292]
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #312]
	blx	r1
	pop	{r7, pc}
Leh_func_end251:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_DetailText_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_DetailText_string:
Leh_func_begin252:
	push	{r4, r7, lr}
Ltmp597:
	add	r7, sp, #4
Ltmp598:
Ltmp599:
	mov	r4, r1
	ldr	r1, [r0]
	ldr	r1, [r1, #292]
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #308]
	mov	r1, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end252:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageUrl
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageUrl:
Leh_func_begin253:
	push	{r7, lr}
Ltmp600:
	mov	r7, sp
Ltmp601:
Ltmp602:
	ldr	r0, [r0, #76]
	ldr	r1, [r0]
	bl	_p_71_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ImageUrl_llvm
	pop	{r7, pc}
Leh_func_end253:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_ImageUrl_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_ImageUrl_string:
Leh_func_begin254:
	push	{r7, lr}
Ltmp603:
	mov	r7, sp
Ltmp604:
Ltmp605:
	ldr	r0, [r0, #76]
	ldr	r2, [r0]
	bl	_p_72_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ImageUrl_string_llvm
	pop	{r7, pc}
Leh_func_end254:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_SelectedCommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_SelectedCommand:
Leh_func_begin255:
	ldr	r0, [r0, #80]
	bx	lr
Leh_func_end255:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_SelectedCommand_System_Windows_Input_ICommand
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_SelectedCommand_System_Windows_Input_ICommand:
Leh_func_begin256:
	str	r1, [r0, #80]
	bx	lr
Leh_func_end256:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_SetSelected_bool_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_SetSelected_bool_bool:
Leh_func_begin257:
	push	{r4, r5, r7, lr}
Ltmp606:
	add	r7, sp, #8
Ltmp607:
	push	{r8}
Ltmp608:
	mov	r5, r1
	mov	r4, r0
	bl	_p_144_plt_MonoTouch_UIKit_UITableViewCell_SetSelected_bool_bool_llvm
	ldrb	r0, [r4, #84]
	cmp	r0, r5
	beq	LBB257_3
	strb	r5, [r4, #84]
	ldrb	r0, [r4, #84]
	cmp	r0, #0
	ldrne	r0, [r4, #80]
	cmpne	r0, #0
	beq	LBB257_3
	ldr	r0, [r4, #80]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC257_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC257_0+8))
LPC257_0:
	add	r1, pc, r1
	ldr	r1, [r1, #536]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
LBB257_3:
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end257:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_Dispose_bool:
Leh_func_begin258:
	push	{r4, r5, r7, lr}
Ltmp609:
	add	r7, sp, #8
Ltmp610:
Ltmp611:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB258_2
	ldr	r0, [r5, #76]
	ldr	r1, [r0]
	bl	_p_82_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_Dispose_llvm
LBB258_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_145_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end258:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__InitializeImageLoaderb__0
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__InitializeImageLoaderb__0:
Leh_func_begin259:
	push	{r7, lr}
Ltmp612:
	mov	r7, sp
Ltmp613:
Ltmp614:
	ldr	r1, [r0]
	ldr	r1, [r1, #300]
	blx	r1
	pop	{r7, pc}
Leh_func_end259:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_BindingContext:
Leh_func_begin260:
	ldr	r0, [r0, #52]
	bx	lr
Leh_func_end260:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin261:
	str	r1, [r0, #52]
	bx	lr
Leh_func_end261:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string:
Leh_func_begin262:
	push	{r4, r5, r7, lr}
Ltmp615:
	add	r7, sp, #8
Ltmp616:
Ltmp617:
	mov	r4, r1
	mov	r5, r0
	bl	_p_146_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end262:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr:
Leh_func_begin263:
	push	{r4, r7, lr}
Ltmp618:
	add	r7, sp, #4
Ltmp619:
Ltmp620:
	mov	r4, r0
	bl	_p_147_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm
	pop	{r4, r7, pc}
Leh_func_end263:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr:
Leh_func_begin264:
	push	{r4, r5, r7, lr}
Ltmp621:
	add	r7, sp, #8
Ltmp622:
Ltmp623:
	mov	r4, r1
	mov	r1, r2
	mov	r5, r0
	bl	_p_147_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_intptr_llvm
	mov	r0, r5
	mov	r1, r4
	bl	_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end264:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr_string:
Leh_func_begin265:
	push	{r7, lr}
Ltmp624:
	mov	r7, sp
Ltmp625:
Ltmp626:
	mov	r3, r1
	mov	r1, r2
	mov	r2, r3
	bl	_p_149_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr_llvm
	pop	{r7, pc}
Leh_func_end265:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_Dispose_bool:
Leh_func_begin266:
	push	{r4, r5, r7, lr}
Ltmp627:
	add	r7, sp, #8
Ltmp628:
	push	{r8}
Ltmp629:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB266_2
	ldr	r0, [r5, #52]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC266_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC266_0+8))
LPC266_0:
	add	r1, pc, r1
	ldr	r1, [r1, #888]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB266_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_150_plt_MonoTouch_UIKit_UICollectionViewCell_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end266:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_DataContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_DataContext:
Leh_func_begin267:
	push	{r7, lr}
Ltmp630:
	mov	r7, sp
Ltmp631:
	push	{r8}
Ltmp632:
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC267_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC267_0+8))
LPC267_0:
	add	r1, pc, r1
	ldr	r1, [r1, #892]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end267:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_DataContext_object:
Leh_func_begin268:
	push	{r7, lr}
Ltmp633:
	mov	r7, sp
Ltmp634:
	push	{r8}
Ltmp635:
	ldr	r0, [r0, #52]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC268_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC268_0+8))
LPC268_0:
	add	r3, pc, r3
	ldr	r3, [r3, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end268:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_get_CellIdentifier
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_get_CellIdentifier:
Leh_func_begin269:
	ldr	r0, [r0, #64]
	bx	lr
Leh_func_end269:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_intptr:
Leh_func_begin270:
	push	{r4, r5, r6, r7, lr}
Ltmp636:
	add	r7, sp, #12
Ltmp637:
	push	{r10}
Ltmp638:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC270_0+8))
	mov	r5, r0
	mov	r4, r1
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC270_0+8))
LPC270_0:
	add	r10, pc, r10
	ldr	r0, [r10, #912]
	bl	_p_151_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #6
	mov	r2, #1
	mov	r6, r0
	bl	_p_152_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool_llvm
	mov	r0, r5
	mov	r1, r4
	str	r6, [r5, #68]
	bl	_p_107_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr_llvm
	ldr	r0, [r10, #444]
	ldr	r4, [r10, #916]
	mov	r1, #0
	bl	_p_26_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end270:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_string_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_string_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin271:
	push	{r4, r5, r6, r7, lr}
Ltmp639:
	add	r7, sp, #12
Ltmp640:
	push	{r10, r11}
Ltmp641:
	sub	sp, sp, #4
	str	r2, [sp]
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC271_0+8))
	mov	r6, r0
	mov	r4, r3
	mov	r11, r1
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC271_0+8))
LPC271_0:
	add	r10, pc, r10
	ldr	r0, [r10, #912]
	bl	_p_151_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #6
	mov	r2, #1
	mov	r5, r0
	bl	_p_152_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool_llvm
	mov	r0, r6
	mov	r1, r11
	str	r5, [r6, #68]
	bl	_p_109_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm
	ldr	r0, [r10, #768]
	ldr	r10, [sp]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	cmp	r4, #0
	mov	r5, r0
	moveq	r4, r10
	mov	r1, r4
	bl	_p_113_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	ldr	r1, [r7, #8]
	str	r5, [r6, #64]
	cmp	r1, #0
	bne	LBB271_2
	bl	_p_154_plt_MonoTouch_Foundation_NSBundle_get_MainBundle_llvm
	mov	r1, r0
LBB271_2:
	mov	r0, r10
	bl	_p_153_plt_MonoTouch_UIKit_UINib_FromName_string_MonoTouch_Foundation_NSBundle_llvm
	mov	r1, r0
	ldr	r0, [r11]
	mov	r2, r4
	ldr	r3, [r0, #296]
	mov	r0, r11
	blx	r3
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end271:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_System_Type_string
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_System_Type_string:
Leh_func_begin272:
	push	{r4, r5, r6, r7, lr}
Ltmp642:
	add	r7, sp, #12
Ltmp643:
	push	{r10, r11}
Ltmp644:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC272_0+8))
	mov	r4, r3
	mov	r10, r2
	mov	r11, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC272_0+8))
LPC272_0:
	add	r0, pc, r0
	ldr	r0, [r0, #912]
	bl	_p_151_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #6
	mov	r2, #1
	mov	r5, r0
	bl	_p_152_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool_llvm
	mov	r0, r6
	mov	r1, r11
	str	r5, [r6, #68]
	bl	_p_109_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm
	cmp	r4, #0
	bne	LBB272_2
	ldr	r0, [r10]
	ldr	r1, [r0, #72]
	mov	r0, r10
	blx	r1
	mov	r4, r0
LBB272_2:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC272_1+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC272_1+8))
LPC272_1:
	add	r0, pc, r0
	ldr	r0, [r0, #768]
	bl	_p_10_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_113_plt_MonoTouch_Foundation_NSString__ctor_string_llvm
	str	r5, [r6, #64]
	mov	r1, r10
	ldr	r2, [r6, #64]
	ldr	r0, [r11]
	mov	r0, r11
	bl	_p_155_plt_MonoTouch_UIKit_UITableView_RegisterClassForCellReuse_System_Type_MonoTouch_Foundation_NSString_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end272:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin273:
	push	{r4, r5, r6, r7, lr}
Ltmp645:
	add	r7, sp, #12
Ltmp646:
Ltmp647:
	mov	r5, r1
	ldr	r1, [r0, #68]
	mov	r4, r2
	ldr	r2, [r1]
	ldrb	r6, [r1, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #212]
	blx	r1
	mov	r1, r0
	ldr	r0, [r5]
	cmp	r6, #0
	beq	LBB273_2
	ldr	r3, [r0, #292]
	mov	r0, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB273_2:
	mov	r0, r5
	bl	_p_111_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end273:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action:
Leh_func_begin274:
	push	{r4, r5, r6, r7, lr}
Ltmp648:
	add	r7, sp, #12
Ltmp649:
	push	{r10, r11}
Ltmp650:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC274_1+8))
	mov	r4, r0
	mov	r11, r2
	mov	r10, r1
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC274_1+8))
LPC274_1:
	add	r6, pc, r6
	ldr	r0, [r6, #920]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	strd	r10, r11, [r5, #8]
	cmp	r5, #0
	beq	LBB274_2
	ldr	r0, [r6, #924]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #928]
	str	r0, [r1, #20]
	ldr	r0, [r6, #932]
	str	r0, [r1, #28]
	ldr	r0, [r6, #936]
	str	r0, [r1, #12]
	mov	r0, r4
	bl	_p_156_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage__ctor_System_Action_1_MonoTouch_UIKit_UIImage_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp651:
LBB274_2:
	ldr	r0, LCPI274_0
LPC274_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI274_0:
	.long	Ltmp651-(LPC274_0+8)
	.end_data_region
Leh_func_end274:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage:
Leh_func_begin275:
	push	{r7, lr}
Ltmp652:
	mov	r7, sp
Ltmp653:
Ltmp654:
	cmp	r0, #0
	cmpne	r1, #0
	beq	LBB275_2
	ldr	r2, [r0]
	ldr	r2, [r2, #264]
	blx	r2
LBB275_2:
	pop	{r7, pc}
Leh_func_end275:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_BindingContext:
Leh_func_begin276:
	ldr	r0, [r0, #48]
	bx	lr
Leh_func_end276:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin277:
	str	r1, [r0, #48]
	bx	lr
Leh_func_end277:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor:
Leh_func_begin278:
	push	{r4, r7, lr}
Ltmp655:
	add	r7, sp, #4
Ltmp656:
Ltmp657:
	mov	r4, r0
	bl	_p_157_plt_MonoTouch_UIKit_UIView__ctor_llvm
	mov	r0, r4
	bl	_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm
	pop	{r4, r7, pc}
Leh_func_end278:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_intptr:
Leh_func_begin279:
	push	{r4, r7, lr}
Ltmp658:
	add	r7, sp, #4
Ltmp659:
Ltmp660:
	mov	r4, r0
	bl	_p_158_plt_MonoTouch_UIKit_UIView__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm
	pop	{r4, r7, pc}
Leh_func_end279:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_Dispose_bool
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_Dispose_bool:
Leh_func_begin280:
	push	{r4, r5, r7, lr}
Ltmp661:
	add	r7, sp, #8
Ltmp662:
	push	{r8}
Ltmp663:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB280_2
	ldr	r0, [r5, #48]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC280_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC280_0+8))
LPC280_0:
	add	r1, pc, r1
	ldr	r1, [r1, #888]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB280_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_159_plt_MonoTouch_UIKit_UIView_Dispose_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end280:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_DataContext
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_DataContext:
Leh_func_begin281:
	push	{r7, lr}
Ltmp664:
	mov	r7, sp
Ltmp665:
	push	{r8}
Ltmp666:
	ldr	r0, [r0, #48]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC281_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC281_0+8))
LPC281_0:
	add	r1, pc, r1
	ldr	r1, [r1, #892]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end281:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_DataContext_object:
Leh_func_begin282:
	push	{r7, lr}
Ltmp667:
	mov	r7, sp
Ltmp668:
	push	{r8}
Ltmp669:
	ldr	r0, [r0, #48]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC282_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC282_0+8))
LPC282_0:
	add	r3, pc, r3
	ldr	r3, [r3, #696]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end282:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2__ctor
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2__ctor:
Leh_func_begin283:
	bx	lr
Leh_func_end283:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2___ctorb__0_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2___ctorb__0_MonoTouch_UIKit_UIImage:
Leh_func_begin284:
	push	{r4, r5, r7, lr}
Ltmp670:
	add	r7, sp, #8
Ltmp671:
Ltmp672:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #8]
	ldr	r1, [r0, #12]
	blx	r1
	mov	r1, r5
	bl	_p_160_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage_llvm
	ldr	r0, [r4, #12]
	cmp	r0, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r4, #12]
	ldr	r1, [r0, #12]
	blx	r1
	pop	{r4, r5, r7, pc}
Leh_func_end284:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UIImage_invoke_void__this___T_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UIImage_invoke_void__this___T_MonoTouch_UIKit_UIImage:
Leh_func_begin285:
	push	{r4, r5, r7, lr}
Ltmp673:
	add	r7, sp, #8
Ltmp674:
Ltmp675:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC285_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC285_0+8))
LPC285_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB285_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB285_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB285_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB285_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB285_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB285_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end285:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIView:
Leh_func_begin286:
	push	{r4, r5, r7, lr}
Ltmp676:
	add	r7, sp, #8
Ltmp677:
Ltmp678:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC286_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC286_0+8))
LPC286_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB286_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB286_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB286_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB286_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB286_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB286_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end286:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextField
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextField:
Leh_func_begin287:
	push	{r4, r5, r7, lr}
Ltmp679:
	add	r7, sp, #8
Ltmp680:
Ltmp681:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC287_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC287_0+8))
LPC287_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB287_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB287_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB287_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB287_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB287_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB287_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end287:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIDatePicker
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIDatePicker:
Leh_func_begin288:
	push	{r4, r5, r7, lr}
Ltmp682:
	add	r7, sp, #8
Ltmp683:
Ltmp684:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC288_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC288_0+8))
LPC288_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB288_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB288_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB288_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB288_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB288_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB288_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end288:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UILabel
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UILabel:
Leh_func_begin289:
	push	{r4, r5, r7, lr}
Ltmp685:
	add	r7, sp, #8
Ltmp686:
Ltmp687:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC289_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC289_0+8))
LPC289_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB289_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB289_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB289_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB289_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB289_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB289_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end289:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextView
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextView:
Leh_func_begin290:
	push	{r4, r5, r7, lr}
Ltmp688:
	add	r7, sp, #8
Ltmp689:
Ltmp690:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC290_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC290_0+8))
LPC290_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB290_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB290_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB290_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB290_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB290_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB290_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end290:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIButton
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIButton:
Leh_func_begin291:
	push	{r4, r5, r7, lr}
Ltmp691:
	add	r7, sp, #8
Ltmp692:
Ltmp693:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC291_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC291_0+8))
LPC291_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB291_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB291_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB291_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB291_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB291_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB291_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end291:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
Leh_func_begin292:
	push	{r4, r5, r7, lr}
Ltmp694:
	add	r7, sp, #8
Ltmp695:
Ltmp696:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC292_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC292_0+8))
LPC292_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB292_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB292_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB292_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB292_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB292_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB292_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end292:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
Leh_func_begin293:
	push	{r4, r5, r7, lr}
Ltmp697:
	add	r7, sp, #8
Ltmp698:
Ltmp699:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC293_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC293_0+8))
LPC293_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB293_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB293_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB293_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB293_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB293_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB293_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end293:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin294:
	push	{r4, r5, r7, lr}
Ltmp700:
	add	r7, sp, #8
Ltmp701:
Ltmp702:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC294_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC294_0+8))
LPC294_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB294_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB294_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB294_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB294_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB294_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB294_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end294:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs:
Leh_func_begin295:
	push	{r4, r5, r6, r7, lr}
Ltmp703:
	add	r7, sp, #12
Ltmp704:
Ltmp705:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC295_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC295_0+8))
LPC295_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB295_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB295_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB295_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB295_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB295_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB295_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end295:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_invoke_void__this___T_MonoTouch_UIKit_UITapGestureRecognizer
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_invoke_void__this___T_MonoTouch_UIKit_UITapGestureRecognizer:
Leh_func_begin296:
	push	{r4, r5, r7, lr}
Ltmp706:
	add	r7, sp, #8
Ltmp707:
Ltmp708:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC296_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC296_0+8))
LPC296_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB296_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB296_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB296_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB296_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB296_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB296_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end296:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_UIKit_UIImageView_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_UIKit_UIImageView_invoke_TResult__this__:
Leh_func_begin297:
	push	{r4, r7, lr}
Ltmp709:
	add	r7, sp, #4
Ltmp710:
Ltmp711:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC297_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC297_0+8))
LPC297_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB297_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB297_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB297_4
	ldr	r1, [r0, #12]
	blx	r1
LBB297_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB297_6
	blx	r1
	pop	{r4, r7, pc}
LBB297_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end297:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage:
Leh_func_begin298:
	push	{r4, r5, r6, r7, lr}
Ltmp712:
	add	r7, sp, #12
Ltmp713:
Ltmp714:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC298_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC298_0+8))
LPC298_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB298_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB298_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB298_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB298_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB298_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB298_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end298:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_invoke_void__this___object_TEventArgs_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_invoke_void__this___object_TEventArgs_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
Leh_func_begin299:
	push	{r4, r5, r6, r7, lr}
Ltmp715:
	add	r7, sp, #12
Ltmp716:
Ltmp717:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC299_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC299_0+8))
LPC299_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB299_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB299_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB299_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB299_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB299_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB299_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end299:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_TResult__this___T1_T2_T3_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_TResult__this___T1_T2_T3_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object:
Leh_func_begin300:
	push	{r4, r5, r6, r7, lr}
Ltmp718:
	add	r7, sp, #12
Ltmp719:
	push	{r10, r11}
Ltmp720:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC300_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r6, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC300_0+8))
LPC300_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB300_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB300_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB300_4
	ldr	r5, [r0, #12]
	mov	r1, r6
	mov	r2, r11
	mov	r3, r10
	blx	r5
LBB300_4:
	ldr	r0, [r4, #16]
	ldr	r5, [r4, #8]
	cmp	r0, #0
	beq	LBB300_6
	mov	r1, r6
	mov	r2, r11
	mov	r3, r10
	blx	r5
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB300_6:
	mov	r0, r6
	mov	r1, r11
	mov	r2, r10
	blx	r5
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end300:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_void__this___T_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_void__this___T_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell:
Leh_func_begin301:
	push	{r4, r5, r7, lr}
Ltmp721:
	add	r7, sp, #8
Ltmp722:
Ltmp723:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC301_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC301_0+8))
LPC301_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB301_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB301_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB301_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB301_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB301_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB301_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end301:

	.private_extern	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_Foundation_NSString_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_Foundation_NSString_invoke_TResult__this__:
Leh_func_begin302:
	push	{r4, r7, lr}
Ltmp724:
	add	r7, sp, #4
Ltmp725:
Ltmp726:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC302_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Binding_Touch_got-(LPC302_0+8))
LPC302_0:
	add	r0, pc, r0
	ldr	r0, [r0, #940]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB302_2
	bl	_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB302_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB302_4
	ldr	r1, [r0, #12]
	blx	r1
LBB302_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB302_6
	blx	r1
	pop	{r4, r7, pc}
LBB302_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end302:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_type_info_2,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Binding_Touch_got,1656,4
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillDefaultBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__0_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__1_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__2_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__3_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__4_MonoTouch_UIKit_UIDatePicker
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__5_MonoTouch_UIKit_UILabel
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__6_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__7_MonoTouch_UIKit_UITextView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__8_MonoTouch_UIKit_UIButton
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__9_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__a_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__b_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_DatePickerOnValueChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_MakeSafeValue_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_MakeSafeValue_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_HandleSearchBarValueChanged_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_HandleShouldReturn_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_SetValue_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_EditTextOnChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SubscribeToEvents
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_HandleValueChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxBehaviourExtensions_Tap_MonoTouch_UIKit_UIView_uint_uint
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_get_Command
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_set_Command_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1_HandleGesture_T
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour_HandleGesture_MonoTouch_UIKit_UITapGestureRecognizer
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ImageUrl
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ImageUrl_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_DefaultImagePath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_DefaultImagePath_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ErrorImagePath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ErrorImagePath_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Action
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__InitializeImageHelperb__0
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ImageUrl
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ImageUrl_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_DefaultImagePath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_DefaultImagePath_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ErrorImagePath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ErrorImagePath_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper__ctor_System_Func_1_MonoTouch_UIKit_UIImageView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_ImageHelperOnImageChanged_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_TableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectionChangedCommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_AccessoryTappedCommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_AccessoryTappedCommand_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_AccessoryButtonTapped_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_RowSelected_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectedItem
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_add_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_remove_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_GetCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_CellDisplayingEnded_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCell_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_NumberOfSections_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel__ctor_MonoTouch_UIKit_UIPickerView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_ItemsSource
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_ItemsSource_System_Collections_IEnumerable
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Reload
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_RowTitle_int_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedItem
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedItem_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_add_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_remove_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedChangedCommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedChangedCommand_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_ShowSelectedItem
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ItemsSource
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ItemsSource_System_Collections_IEnumerable
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_UseAnimations
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_UseAnimations_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_AddAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_AddAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_RemoveAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_RemoveAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ReplaceAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ReplaceAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_RowsInSection_MonoTouch_UIKit_UITableView_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_CellIdentifier
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSString
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_BindingDescriptions
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_CreateDefaultBindableCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellCreator
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellCreator_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellModifier
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellModifier_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifierOverride
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellIdentifierOverride_System_Func_1_MonoTouch_Foundation_NSString
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifier
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__Initializeb__2_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_HandleEditTextValueChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SubscribeToEvents
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_ShouldSkipSetValueForViewSpecificReasons_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_CurrentText
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding__ctor_object_System_Reflection_PropertyInfo
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_HandleSliderValueChanged_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SubscribeToEvents
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_Button
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_DefaultMode
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_TargetType
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_SetValueImpl_object_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_DefaultCellIdentifier
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_CollectionView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectionChangedCommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ItemSelected_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectedItem
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_add_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_remove_SelectedItemChanged_System_EventHandler
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_CellDisplayingEnded_MonoTouch_UIKit_UICollectionView_MonoTouch_UIKit_UICollectionViewCell_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_get_ItemsSource
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_set_ItemsSource_System_Collections_IEnumerable
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_Accessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageLoader
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_TitleText
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_TitleText_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_DetailText
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_DetailText_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageUrl
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_ImageUrl_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_SelectedCommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_SelectedCommand_System_Windows_Input_ICommand
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_SetSelected_bool_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__InitializeImageLoaderb__0
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_get_CellIdentifier
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_string_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_System_Type_string
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_Dispose_bool
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2___ctorb__0_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UIImage_invoke_void__this___T_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextField
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIDatePicker
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UILabel
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextView
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIButton
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_invoke_void__this___T_MonoTouch_UIKit_UITapGestureRecognizer
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_UIKit_UIImageView_invoke_TResult__this__
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_invoke_void__this___object_TEventArgs_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_TResult__this___T1_T2_T3_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_void__this___T_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	.no_dead_strip	_Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_Foundation_NSString_invoke_TResult__this__
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Binding_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	303
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	19
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	20
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	21
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	22
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	23
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	24
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	25
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	26
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	27
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	28
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	29
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	30
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	31
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	32
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	33
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	34
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	35
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	36
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	37
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	38
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	39
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	40
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	41
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	42
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	43
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	44
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	45
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	46
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	47
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	48
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	49
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	50
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	51
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	52
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	53
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	54
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	55
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	56
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	57
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	58
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	59
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	60
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	61
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	62
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	63
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	64
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	65
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	66
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	67
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	68
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	69
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	70
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	71
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	72
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	73
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	74
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	75
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	76
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	77
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	78
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	79
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	80
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	81
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	82
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	83
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	84
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	85
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	86
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	87
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	88
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	89
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	90
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	92
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	93
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	94
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	95
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	96
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	97
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	98
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	99
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	100
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	101
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	103
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	104
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	105
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	106
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	107
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	108
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	109
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	110
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	111
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	112
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	113
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	117
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	118
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	119
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	120
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	121
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	122
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	123
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	124
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	125
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	126
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	127
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	128
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	129
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	130
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	131
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	132
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	133
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	134
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	135
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	136
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	137
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	138
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	139
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	140
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	141
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
	.long	142
Lset137 = Lmono_eh_func_begin137-mono_eh_frame
	.long	Lset137
	.long	143
Lset138 = Lmono_eh_func_begin138-mono_eh_frame
	.long	Lset138
	.long	144
Lset139 = Lmono_eh_func_begin139-mono_eh_frame
	.long	Lset139
	.long	145
Lset140 = Lmono_eh_func_begin140-mono_eh_frame
	.long	Lset140
	.long	146
Lset141 = Lmono_eh_func_begin141-mono_eh_frame
	.long	Lset141
	.long	147
Lset142 = Lmono_eh_func_begin142-mono_eh_frame
	.long	Lset142
	.long	148
Lset143 = Lmono_eh_func_begin143-mono_eh_frame
	.long	Lset143
	.long	149
Lset144 = Lmono_eh_func_begin144-mono_eh_frame
	.long	Lset144
	.long	150
Lset145 = Lmono_eh_func_begin145-mono_eh_frame
	.long	Lset145
	.long	151
Lset146 = Lmono_eh_func_begin146-mono_eh_frame
	.long	Lset146
	.long	152
Lset147 = Lmono_eh_func_begin147-mono_eh_frame
	.long	Lset147
	.long	153
Lset148 = Lmono_eh_func_begin148-mono_eh_frame
	.long	Lset148
	.long	154
Lset149 = Lmono_eh_func_begin149-mono_eh_frame
	.long	Lset149
	.long	155
Lset150 = Lmono_eh_func_begin150-mono_eh_frame
	.long	Lset150
	.long	156
Lset151 = Lmono_eh_func_begin151-mono_eh_frame
	.long	Lset151
	.long	157
Lset152 = Lmono_eh_func_begin152-mono_eh_frame
	.long	Lset152
	.long	158
Lset153 = Lmono_eh_func_begin153-mono_eh_frame
	.long	Lset153
	.long	159
Lset154 = Lmono_eh_func_begin154-mono_eh_frame
	.long	Lset154
	.long	160
Lset155 = Lmono_eh_func_begin155-mono_eh_frame
	.long	Lset155
	.long	161
Lset156 = Lmono_eh_func_begin156-mono_eh_frame
	.long	Lset156
	.long	162
Lset157 = Lmono_eh_func_begin157-mono_eh_frame
	.long	Lset157
	.long	163
Lset158 = Lmono_eh_func_begin158-mono_eh_frame
	.long	Lset158
	.long	164
Lset159 = Lmono_eh_func_begin159-mono_eh_frame
	.long	Lset159
	.long	165
Lset160 = Lmono_eh_func_begin160-mono_eh_frame
	.long	Lset160
	.long	166
Lset161 = Lmono_eh_func_begin161-mono_eh_frame
	.long	Lset161
	.long	167
Lset162 = Lmono_eh_func_begin162-mono_eh_frame
	.long	Lset162
	.long	168
Lset163 = Lmono_eh_func_begin163-mono_eh_frame
	.long	Lset163
	.long	169
Lset164 = Lmono_eh_func_begin164-mono_eh_frame
	.long	Lset164
	.long	170
Lset165 = Lmono_eh_func_begin165-mono_eh_frame
	.long	Lset165
	.long	171
Lset166 = Lmono_eh_func_begin166-mono_eh_frame
	.long	Lset166
	.long	172
Lset167 = Lmono_eh_func_begin167-mono_eh_frame
	.long	Lset167
	.long	173
Lset168 = Lmono_eh_func_begin168-mono_eh_frame
	.long	Lset168
	.long	174
Lset169 = Lmono_eh_func_begin169-mono_eh_frame
	.long	Lset169
	.long	175
Lset170 = Lmono_eh_func_begin170-mono_eh_frame
	.long	Lset170
	.long	176
Lset171 = Lmono_eh_func_begin171-mono_eh_frame
	.long	Lset171
	.long	177
Lset172 = Lmono_eh_func_begin172-mono_eh_frame
	.long	Lset172
	.long	178
Lset173 = Lmono_eh_func_begin173-mono_eh_frame
	.long	Lset173
	.long	179
Lset174 = Lmono_eh_func_begin174-mono_eh_frame
	.long	Lset174
	.long	180
Lset175 = Lmono_eh_func_begin175-mono_eh_frame
	.long	Lset175
	.long	181
Lset176 = Lmono_eh_func_begin176-mono_eh_frame
	.long	Lset176
	.long	182
Lset177 = Lmono_eh_func_begin177-mono_eh_frame
	.long	Lset177
	.long	183
Lset178 = Lmono_eh_func_begin178-mono_eh_frame
	.long	Lset178
	.long	184
Lset179 = Lmono_eh_func_begin179-mono_eh_frame
	.long	Lset179
	.long	185
Lset180 = Lmono_eh_func_begin180-mono_eh_frame
	.long	Lset180
	.long	186
Lset181 = Lmono_eh_func_begin181-mono_eh_frame
	.long	Lset181
	.long	187
Lset182 = Lmono_eh_func_begin182-mono_eh_frame
	.long	Lset182
	.long	188
Lset183 = Lmono_eh_func_begin183-mono_eh_frame
	.long	Lset183
	.long	189
Lset184 = Lmono_eh_func_begin184-mono_eh_frame
	.long	Lset184
	.long	190
Lset185 = Lmono_eh_func_begin185-mono_eh_frame
	.long	Lset185
	.long	191
Lset186 = Lmono_eh_func_begin186-mono_eh_frame
	.long	Lset186
	.long	192
Lset187 = Lmono_eh_func_begin187-mono_eh_frame
	.long	Lset187
	.long	193
Lset188 = Lmono_eh_func_begin188-mono_eh_frame
	.long	Lset188
	.long	194
Lset189 = Lmono_eh_func_begin189-mono_eh_frame
	.long	Lset189
	.long	195
Lset190 = Lmono_eh_func_begin190-mono_eh_frame
	.long	Lset190
	.long	196
Lset191 = Lmono_eh_func_begin191-mono_eh_frame
	.long	Lset191
	.long	197
Lset192 = Lmono_eh_func_begin192-mono_eh_frame
	.long	Lset192
	.long	198
Lset193 = Lmono_eh_func_begin193-mono_eh_frame
	.long	Lset193
	.long	199
Lset194 = Lmono_eh_func_begin194-mono_eh_frame
	.long	Lset194
	.long	200
Lset195 = Lmono_eh_func_begin195-mono_eh_frame
	.long	Lset195
	.long	201
Lset196 = Lmono_eh_func_begin196-mono_eh_frame
	.long	Lset196
	.long	202
Lset197 = Lmono_eh_func_begin197-mono_eh_frame
	.long	Lset197
	.long	203
Lset198 = Lmono_eh_func_begin198-mono_eh_frame
	.long	Lset198
	.long	204
Lset199 = Lmono_eh_func_begin199-mono_eh_frame
	.long	Lset199
	.long	205
Lset200 = Lmono_eh_func_begin200-mono_eh_frame
	.long	Lset200
	.long	206
Lset201 = Lmono_eh_func_begin201-mono_eh_frame
	.long	Lset201
	.long	207
Lset202 = Lmono_eh_func_begin202-mono_eh_frame
	.long	Lset202
	.long	208
Lset203 = Lmono_eh_func_begin203-mono_eh_frame
	.long	Lset203
	.long	209
Lset204 = Lmono_eh_func_begin204-mono_eh_frame
	.long	Lset204
	.long	210
Lset205 = Lmono_eh_func_begin205-mono_eh_frame
	.long	Lset205
	.long	211
Lset206 = Lmono_eh_func_begin206-mono_eh_frame
	.long	Lset206
	.long	212
Lset207 = Lmono_eh_func_begin207-mono_eh_frame
	.long	Lset207
	.long	213
Lset208 = Lmono_eh_func_begin208-mono_eh_frame
	.long	Lset208
	.long	215
Lset209 = Lmono_eh_func_begin209-mono_eh_frame
	.long	Lset209
	.long	217
Lset210 = Lmono_eh_func_begin210-mono_eh_frame
	.long	Lset210
	.long	218
Lset211 = Lmono_eh_func_begin211-mono_eh_frame
	.long	Lset211
	.long	219
Lset212 = Lmono_eh_func_begin212-mono_eh_frame
	.long	Lset212
	.long	220
Lset213 = Lmono_eh_func_begin213-mono_eh_frame
	.long	Lset213
	.long	221
Lset214 = Lmono_eh_func_begin214-mono_eh_frame
	.long	Lset214
	.long	222
Lset215 = Lmono_eh_func_begin215-mono_eh_frame
	.long	Lset215
	.long	223
Lset216 = Lmono_eh_func_begin216-mono_eh_frame
	.long	Lset216
	.long	224
Lset217 = Lmono_eh_func_begin217-mono_eh_frame
	.long	Lset217
	.long	225
Lset218 = Lmono_eh_func_begin218-mono_eh_frame
	.long	Lset218
	.long	226
Lset219 = Lmono_eh_func_begin219-mono_eh_frame
	.long	Lset219
	.long	227
Lset220 = Lmono_eh_func_begin220-mono_eh_frame
	.long	Lset220
	.long	228
Lset221 = Lmono_eh_func_begin221-mono_eh_frame
	.long	Lset221
	.long	229
Lset222 = Lmono_eh_func_begin222-mono_eh_frame
	.long	Lset222
	.long	230
Lset223 = Lmono_eh_func_begin223-mono_eh_frame
	.long	Lset223
	.long	231
Lset224 = Lmono_eh_func_begin224-mono_eh_frame
	.long	Lset224
	.long	232
Lset225 = Lmono_eh_func_begin225-mono_eh_frame
	.long	Lset225
	.long	233
Lset226 = Lmono_eh_func_begin226-mono_eh_frame
	.long	Lset226
	.long	234
Lset227 = Lmono_eh_func_begin227-mono_eh_frame
	.long	Lset227
	.long	235
Lset228 = Lmono_eh_func_begin228-mono_eh_frame
	.long	Lset228
	.long	236
Lset229 = Lmono_eh_func_begin229-mono_eh_frame
	.long	Lset229
	.long	237
Lset230 = Lmono_eh_func_begin230-mono_eh_frame
	.long	Lset230
	.long	238
Lset231 = Lmono_eh_func_begin231-mono_eh_frame
	.long	Lset231
	.long	241
Lset232 = Lmono_eh_func_begin232-mono_eh_frame
	.long	Lset232
	.long	242
Lset233 = Lmono_eh_func_begin233-mono_eh_frame
	.long	Lset233
	.long	243
Lset234 = Lmono_eh_func_begin234-mono_eh_frame
	.long	Lset234
	.long	244
Lset235 = Lmono_eh_func_begin235-mono_eh_frame
	.long	Lset235
	.long	245
Lset236 = Lmono_eh_func_begin236-mono_eh_frame
	.long	Lset236
	.long	246
Lset237 = Lmono_eh_func_begin237-mono_eh_frame
	.long	Lset237
	.long	247
Lset238 = Lmono_eh_func_begin238-mono_eh_frame
	.long	Lset238
	.long	248
Lset239 = Lmono_eh_func_begin239-mono_eh_frame
	.long	Lset239
	.long	249
Lset240 = Lmono_eh_func_begin240-mono_eh_frame
	.long	Lset240
	.long	250
Lset241 = Lmono_eh_func_begin241-mono_eh_frame
	.long	Lset241
	.long	251
Lset242 = Lmono_eh_func_begin242-mono_eh_frame
	.long	Lset242
	.long	252
Lset243 = Lmono_eh_func_begin243-mono_eh_frame
	.long	Lset243
	.long	253
Lset244 = Lmono_eh_func_begin244-mono_eh_frame
	.long	Lset244
	.long	254
Lset245 = Lmono_eh_func_begin245-mono_eh_frame
	.long	Lset245
	.long	255
Lset246 = Lmono_eh_func_begin246-mono_eh_frame
	.long	Lset246
	.long	256
Lset247 = Lmono_eh_func_begin247-mono_eh_frame
	.long	Lset247
	.long	257
Lset248 = Lmono_eh_func_begin248-mono_eh_frame
	.long	Lset248
	.long	258
Lset249 = Lmono_eh_func_begin249-mono_eh_frame
	.long	Lset249
	.long	259
Lset250 = Lmono_eh_func_begin250-mono_eh_frame
	.long	Lset250
	.long	260
Lset251 = Lmono_eh_func_begin251-mono_eh_frame
	.long	Lset251
	.long	261
Lset252 = Lmono_eh_func_begin252-mono_eh_frame
	.long	Lset252
	.long	262
Lset253 = Lmono_eh_func_begin253-mono_eh_frame
	.long	Lset253
	.long	263
Lset254 = Lmono_eh_func_begin254-mono_eh_frame
	.long	Lset254
	.long	264
Lset255 = Lmono_eh_func_begin255-mono_eh_frame
	.long	Lset255
	.long	265
Lset256 = Lmono_eh_func_begin256-mono_eh_frame
	.long	Lset256
	.long	266
Lset257 = Lmono_eh_func_begin257-mono_eh_frame
	.long	Lset257
	.long	267
Lset258 = Lmono_eh_func_begin258-mono_eh_frame
	.long	Lset258
	.long	268
Lset259 = Lmono_eh_func_begin259-mono_eh_frame
	.long	Lset259
	.long	269
Lset260 = Lmono_eh_func_begin260-mono_eh_frame
	.long	Lset260
	.long	270
Lset261 = Lmono_eh_func_begin261-mono_eh_frame
	.long	Lset261
	.long	271
Lset262 = Lmono_eh_func_begin262-mono_eh_frame
	.long	Lset262
	.long	272
Lset263 = Lmono_eh_func_begin263-mono_eh_frame
	.long	Lset263
	.long	273
Lset264 = Lmono_eh_func_begin264-mono_eh_frame
	.long	Lset264
	.long	277
Lset265 = Lmono_eh_func_begin265-mono_eh_frame
	.long	Lset265
	.long	280
Lset266 = Lmono_eh_func_begin266-mono_eh_frame
	.long	Lset266
	.long	281
Lset267 = Lmono_eh_func_begin267-mono_eh_frame
	.long	Lset267
	.long	282
Lset268 = Lmono_eh_func_begin268-mono_eh_frame
	.long	Lset268
	.long	283
Lset269 = Lmono_eh_func_begin269-mono_eh_frame
	.long	Lset269
	.long	284
Lset270 = Lmono_eh_func_begin270-mono_eh_frame
	.long	Lset270
	.long	285
Lset271 = Lmono_eh_func_begin271-mono_eh_frame
	.long	Lset271
	.long	286
Lset272 = Lmono_eh_func_begin272-mono_eh_frame
	.long	Lset272
	.long	287
Lset273 = Lmono_eh_func_begin273-mono_eh_frame
	.long	Lset273
	.long	288
Lset274 = Lmono_eh_func_begin274-mono_eh_frame
	.long	Lset274
	.long	289
Lset275 = Lmono_eh_func_begin275-mono_eh_frame
	.long	Lset275
	.long	290
Lset276 = Lmono_eh_func_begin276-mono_eh_frame
	.long	Lset276
	.long	291
Lset277 = Lmono_eh_func_begin277-mono_eh_frame
	.long	Lset277
	.long	292
Lset278 = Lmono_eh_func_begin278-mono_eh_frame
	.long	Lset278
	.long	293
Lset279 = Lmono_eh_func_begin279-mono_eh_frame
	.long	Lset279
	.long	295
Lset280 = Lmono_eh_func_begin280-mono_eh_frame
	.long	Lset280
	.long	296
Lset281 = Lmono_eh_func_begin281-mono_eh_frame
	.long	Lset281
	.long	297
Lset282 = Lmono_eh_func_begin282-mono_eh_frame
	.long	Lset282
	.long	298
Lset283 = Lmono_eh_func_begin283-mono_eh_frame
	.long	Lset283
	.long	299
Lset284 = Lmono_eh_func_begin284-mono_eh_frame
	.long	Lset284
	.long	303
Lset285 = Lmono_eh_func_begin285-mono_eh_frame
	.long	Lset285
	.long	304
Lset286 = Lmono_eh_func_begin286-mono_eh_frame
	.long	Lset286
	.long	305
Lset287 = Lmono_eh_func_begin287-mono_eh_frame
	.long	Lset287
	.long	306
Lset288 = Lmono_eh_func_begin288-mono_eh_frame
	.long	Lset288
	.long	307
Lset289 = Lmono_eh_func_begin289-mono_eh_frame
	.long	Lset289
	.long	308
Lset290 = Lmono_eh_func_begin290-mono_eh_frame
	.long	Lset290
	.long	309
Lset291 = Lmono_eh_func_begin291-mono_eh_frame
	.long	Lset291
	.long	310
Lset292 = Lmono_eh_func_begin292-mono_eh_frame
	.long	Lset292
	.long	311
Lset293 = Lmono_eh_func_begin293-mono_eh_frame
	.long	Lset293
	.long	312
Lset294 = Lmono_eh_func_begin294-mono_eh_frame
	.long	Lset294
	.long	313
Lset295 = Lmono_eh_func_begin295-mono_eh_frame
	.long	Lset295
	.long	314
Lset296 = Lmono_eh_func_begin296-mono_eh_frame
	.long	Lset296
	.long	315
Lset297 = Lmono_eh_func_begin297-mono_eh_frame
	.long	Lset297
	.long	316
Lset298 = Lmono_eh_func_begin298-mono_eh_frame
	.long	Lset298
	.long	317
Lset299 = Lmono_eh_func_begin299-mono_eh_frame
	.long	Lset299
	.long	318
Lset300 = Lmono_eh_func_begin300-mono_eh_frame
	.long	Lset300
	.long	319
Lset301 = Lmono_eh_func_begin301-mono_eh_frame
	.long	Lset301
	.long	320
Lset302 = Lmono_eh_func_begin302-mono_eh_frame
	.long	Lset302
Lset303 = Leh_func_end302-Leh_func_begin302
	.long	Lset303
Lset304 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset304
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin32:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin33:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin39:
	.byte	0

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin44:
	.byte	0

Lmono_eh_func_begin45:
	.byte	0

Lmono_eh_func_begin46:
	.byte	0

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin51:
	.byte	0

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin58:
	.byte	0

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin60:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin63:
	.byte	0

Lmono_eh_func_begin64:
	.byte	0

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin67:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin68:
	.byte	0

Lmono_eh_func_begin69:
	.byte	0

Lmono_eh_func_begin70:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin71:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin72:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin73:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin74:
	.byte	0

Lmono_eh_func_begin75:
	.byte	0

Lmono_eh_func_begin76:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin77:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin78:
	.byte	0

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	4

Lmono_eh_func_begin80:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin81:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin82:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin83:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin84:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin85:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin86:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin88:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin89:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin92:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin93:
	.byte	0

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin95:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin96:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin97:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin98:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin101:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin102:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin103:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin105:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin106:
	.byte	0

Lmono_eh_func_begin107:
	.byte	0

Lmono_eh_func_begin108:
	.byte	0

Lmono_eh_func_begin109:
	.byte	0

Lmono_eh_func_begin110:
	.byte	0

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin113:
	.byte	0

Lmono_eh_func_begin114:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin115:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin116:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin117:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin118:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin119:
	.byte	0

Lmono_eh_func_begin120:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin121:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin122:
	.byte	0

Lmono_eh_func_begin123:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin124:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin125:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin126:
	.byte	0

Lmono_eh_func_begin127:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin128:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin129:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin131:
	.byte	0

Lmono_eh_func_begin132:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin135:
	.byte	0

Lmono_eh_func_begin136:
	.byte	0

Lmono_eh_func_begin137:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin138:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin139:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin140:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin141:
	.byte	0

Lmono_eh_func_begin142:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin143:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin144:
	.byte	0

Lmono_eh_func_begin145:
	.byte	0

Lmono_eh_func_begin146:
	.byte	0

Lmono_eh_func_begin147:
	.byte	0

Lmono_eh_func_begin148:
	.byte	0

Lmono_eh_func_begin149:
	.byte	0

Lmono_eh_func_begin150:
	.byte	0

Lmono_eh_func_begin151:
	.byte	0

Lmono_eh_func_begin152:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin153:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin154:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin155:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin156:
	.byte	0

Lmono_eh_func_begin157:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin158:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin159:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin160:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin161:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin162:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin163:
	.byte	0

Lmono_eh_func_begin164:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin165:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin166:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin167:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin168:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin169:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin170:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin171:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin172:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin173:
	.byte	0

Lmono_eh_func_begin174:
	.byte	0

Lmono_eh_func_begin175:
	.byte	0

Lmono_eh_func_begin176:
	.byte	0

Lmono_eh_func_begin177:
	.byte	0

Lmono_eh_func_begin178:
	.byte	0

Lmono_eh_func_begin179:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin180:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin181:
	.byte	0

Lmono_eh_func_begin182:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin183:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin184:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin185:
	.byte	0

Lmono_eh_func_begin186:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin187:
	.byte	0

Lmono_eh_func_begin188:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin189:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin190:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin191:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin192:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin193:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin194:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin195:
	.byte	0

Lmono_eh_func_begin196:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin197:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin198:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin199:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin200:
	.byte	0

Lmono_eh_func_begin201:
	.byte	0

Lmono_eh_func_begin202:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin203:
	.byte	0

Lmono_eh_func_begin204:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin205:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	4
	.byte	138
	.byte	5

Lmono_eh_func_begin206:
	.byte	0

Lmono_eh_func_begin207:
	.byte	0

Lmono_eh_func_begin208:
	.byte	0

Lmono_eh_func_begin209:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin210:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin211:
	.byte	0

Lmono_eh_func_begin212:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin213:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin214:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin215:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin216:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin217:
	.byte	0

Lmono_eh_func_begin218:
	.byte	0

Lmono_eh_func_begin219:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin220:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin221:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin222:
	.byte	0

Lmono_eh_func_begin223:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin224:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin225:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin226:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin227:
	.byte	0

Lmono_eh_func_begin228:
	.byte	0

Lmono_eh_func_begin229:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin230:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin231:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin232:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin233:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin234:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin235:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin236:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin237:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin238:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin239:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin240:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin241:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin242:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin243:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin244:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin245:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin246:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin247:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin248:
	.byte	0

Lmono_eh_func_begin249:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin250:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin251:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin252:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin253:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin254:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin255:
	.byte	0

Lmono_eh_func_begin256:
	.byte	0

Lmono_eh_func_begin257:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin258:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin259:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin260:
	.byte	0

Lmono_eh_func_begin261:
	.byte	0

Lmono_eh_func_begin262:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin263:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin264:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin265:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin266:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin267:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin268:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin269:
	.byte	0

Lmono_eh_func_begin270:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin271:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin272:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin273:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin274:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin275:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin276:
	.byte	0

Lmono_eh_func_begin277:
	.byte	0

Lmono_eh_func_begin278:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin279:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin280:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin281:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin282:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin283:
	.byte	0

Lmono_eh_func_begin284:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin285:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin286:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin287:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin288:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin289:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin290:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin291:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin292:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin293:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin294:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin295:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin296:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin297:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin298:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin299:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin300:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin301:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin302:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Binding.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action
_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,12,16,155,229,16,32,155,229
	.byte 20,48,155,229,24,192,155,229,0,192,141,229
bl _p_162

	.byte 8,0,155,229,28,16,155,229
bl _p_78

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_5b:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0
_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,16,0,139,229,16,32,155,229,2,0,160,225
	.byte 0,16,160,227,0,32,146,229,15,224,160,225,52,240,146,229,0,0,0,235,4,0,0,234,8,224,139,229,16,0,155,229
	.byte 0,0,139,229,8,192,155,229,12,240,160,225,24,208,139,226,0,9,189,232,128,128,189,232

Lme_66:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0
_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,28,0,139,229,0,0,160,227,0,0,139,229
	.byte 0,0,160,227,4,0,139,229,28,0,155,229,20,16,144,229,1,0,160,225,0,16,145,229,15,224,160,225,100,241,145,229
	.byte 34,0,0,234,8,0,155,229,8,0,155,229,0,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 932
	.byte 0,0,159,231,32,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 432
	.byte 0,0,159,231,1,16,160,227
bl _p_26

	.byte 4,0,139,229,36,0,139,229,0,0,155,229
bl _p_165

	.byte 0,32,160,225,36,48,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229,32,0,155,229
	.byte 4,16,155,229
bl _p_87
bl _p_164

	.byte 24,0,139,229,0,0,80,227,1,0,0,10,24,0,155,229
bl _p_163

	.byte 255,255,255,234,40,208,139,226,0,9,189,232,128,128,189,232

Lme_72:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0
_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,28,0,139,229,0,0,160,227,0,0,139,229
	.byte 0,0,160,227,4,0,139,229,28,0,155,229,24,16,144,229,1,0,160,225,0,16,145,229,15,224,160,225,36,241,145,229
	.byte 34,0,0,234,8,0,155,229,8,0,155,229,0,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 936
	.byte 0,0,159,231,32,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 432
	.byte 0,0,159,231,1,16,160,227
bl _p_26

	.byte 4,0,139,229,36,0,139,229,0,0,155,229
bl _p_165

	.byte 0,32,160,225,36,48,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229,32,0,155,229
	.byte 4,16,155,229
bl _p_87
bl _p_164

	.byte 24,0,139,229,0,0,80,227,1,0,0,10,24,0,155,229
bl _p_163

	.byte 255,255,255,234,40,208,139,226,0,9,189,232,128,128,189,232

Lme_d6:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,16,16,155,229,20,32,155,229
	.byte 24,48,155,229,28,192,155,229,0,192,141,229
bl _p_166

	.byte 8,0,155,229,12,16,155,229
bl _p_130

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_ef:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,16,16,155,229,20,32,155,229
	.byte 24,48,155,229,28,192,155,229,0,192,141,229
bl _p_166

	.byte 8,0,155,229,12,16,155,229
bl _p_131

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_f0:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,8,0,155,229,12,16,155,229,16,32,155,229,20,48,155,229,24,192,155,229
	.byte 0,192,141,229
bl _p_167

	.byte 8,0,155,229
bl _p_148

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_112:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,16,16,155,229,20,32,155,229
	.byte 24,48,155,229,28,192,155,229,0,192,141,229
bl _p_167

	.byte 8,0,155,229,12,16,155,229
bl _p_130

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_113:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,16,16,155,229,20,32,155,229
	.byte 24,48,155,229,28,192,155,229,0,192,141,229
bl _p_167

	.byte 8,0,155,229,12,16,155,229
bl _p_131

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_114:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string
_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,28,16,155,229,12,32,155,229
	.byte 16,48,155,229,20,192,155,229,0,192,141,229,24,192,155,229,4,192,141,229
bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_116:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,52,224,157,229,28,224,139,229,8,0,155,229,28,16,155,229,12,32,155,229
	.byte 16,48,155,229,20,192,155,229,0,192,141,229,24,192,155,229,4,192,141,229
bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_117:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF
_Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,16,32,139,229
	.byte 20,48,139,229,48,224,157,229,24,224,139,229,8,0,155,229,12,16,155,229,16,32,155,229,20,48,155,229,24,192,155,229
	.byte 0,192,141,229
bl _p_170

	.byte 8,0,155,229
bl _p_148

	.byte 32,208,139,226,0,9,189,232,128,128,189,232

Lme_126:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0
_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,16,208,77,226,13,176,160,225,8,0,139,229,12,16,139,229,8,0,155,229
	.byte 0,0,144,229
bl _p_171

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,4,0,139,229,16,208,139,226,0,9,189,232,128,128,189,232

Lme_12d:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor
_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,16,208,77,226,13,176,160,225,8,0,139,229,8,0,155,229,0,0,144,229
bl _p_172

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,4,0,139,229,8,0,155,229
bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor

	.byte 16,208,139,226,0,9,189,232,128,128,189,232

Lme_12e:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_175

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_173

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_174

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_173
bl _p_10

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_141:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillDefaultBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__0_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__1_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__2_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__3_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__4_MonoTouch_UIKit_UIDatePicker
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__5_MonoTouch_UIKit_UILabel
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__6_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__7_MonoTouch_UIKit_UITextView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__8_MonoTouch_UIKit_UIButton
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__9_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__a_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__b_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_DatePickerOnValueChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_MakeSafeValue_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_MakeSafeValue_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_HandleSearchBarValueChanged_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_HandleShouldReturn_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_SetValue_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_EditTextOnChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SubscribeToEvents
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_HandleValueChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxBehaviourExtensions_Tap_MonoTouch_UIKit_UIView_uint_uint
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_get_Command
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_set_Command_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1_HandleGesture_T
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__ctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour_HandleGesture_MonoTouch_UIKit_UITapGestureRecognizer
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ImageUrl
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ImageUrl_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_DefaultImagePath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_DefaultImagePath_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ErrorImagePath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ErrorImagePath_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Action
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__InitializeImageHelperb__0
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ImageUrl
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ImageUrl_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_DefaultImagePath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_DefaultImagePath_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ErrorImagePath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ErrorImagePath_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper__ctor_System_Func_1_MonoTouch_UIKit_UIImageView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_ImageHelperOnImageChanged_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_TableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectionChangedCommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_AccessoryTappedCommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_AccessoryTappedCommand_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_AccessoryButtonTapped_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_RowSelected_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectedItem
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_add_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_remove_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_GetCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_CellDisplayingEnded_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCell_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_NumberOfSections_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel__ctor_MonoTouch_UIKit_UIPickerView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_ItemsSource
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_ItemsSource_System_Collections_IEnumerable
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Reload
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_RowTitle_int_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedItem
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedItem_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_add_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_remove_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedChangedCommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedChangedCommand_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_ShowSelectedItem
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ItemsSource
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ItemsSource_System_Collections_IEnumerable
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_UseAnimations
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_UseAnimations_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_AddAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_AddAnimation_MonoTouch_UIKit_UITableViewRowAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_RemoveAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_RemoveAnimation_MonoTouch_UIKit_UITableViewRowAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ReplaceAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ReplaceAnimation_MonoTouch_UIKit_UITableViewRowAnimation
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_RowsInSection_MonoTouch_UIKit_UITableView_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_CellIdentifier
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSString
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_BindingDescriptions
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_CreateDefaultBindableCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__cctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellCreator
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellCreator_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellModifier
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellModifier_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifierOverride
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellIdentifierOverride_System_Func_1_MonoTouch_Foundation_NSString
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifier
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__Initializeb__2_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_HandleEditTextValueChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SubscribeToEvents
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_ShouldSkipSetValueForViewSpecificReasons_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_CurrentText
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding__ctor_object_System_Reflection_PropertyInfo
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_HandleSliderValueChanged_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SubscribeToEvents
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_Button
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_DefaultMode
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_TargetType
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_SetValueImpl_object_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_DefaultCellIdentifier
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_CollectionView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectionChangedCommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ItemSelected_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectedItem
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_add_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_remove_SelectedItemChanged_System_EventHandler
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_CellDisplayingEnded_MonoTouch_UIKit_UICollectionView_MonoTouch_UIKit_UICollectionViewCell_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__cctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_get_ItemsSource
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_set_ItemsSource_System_Collections_IEnumerable
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_Accessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageLoader
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_TitleText
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_TitleText_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_DetailText
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_DetailText_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageUrl
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_ImageUrl_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_SelectedCommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_SelectedCommand_System_Windows_Input_ICommand
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_SetSelected_bool_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__InitializeImageLoaderb__0
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_get_CellIdentifier
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_string_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_System_Type_string
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_Dispose_bool
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2__ctor
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2___ctorb__0_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UIImage_invoke_void__this___T_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextField
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIDatePicker
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UILabel
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextView
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIButton
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_invoke_void__this___T_MonoTouch_UIKit_UITapGestureRecognizer
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_UIKit_UIImageView_invoke_TResult__this__
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_invoke_void__this___object_TEventArgs_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_TResult__this___T1_T2_T3_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_void__this___T_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
.no_dead_strip _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_Foundation_NSString_invoke_TResult__this__

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder_FillDefaultBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__0_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__1_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__2_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__3_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__4_MonoTouch_UIKit_UIDatePicker
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__5_MonoTouch_UIKit_UILabel
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__6_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__7_MonoTouch_UIKit_UITextView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__8_MonoTouch_UIKit_UIButton
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__9_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__a_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__FillTargetFactoriesb__b_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_DatePickerOnValueChanged_object_System_EventArgs
	bl method_addresses
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerDateTargetBinding_MakeSafeValue_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_GetValueFrom_MonoTouch_UIKit_UIDatePicker
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding_MakeSafeValue_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_HandleSearchBarValueChanged_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISearchBarTextTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_HandleShouldReturn_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_SetValue_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_EditTextOnChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SubscribeToEvents
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_HandleValueChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISwitchOnTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxBehaviourExtensions_Tap_MonoTouch_UIKit_UIView_uint_uint
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_get_Command
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_set_Command_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1_HandleGesture_T
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour_HandleGesture_MonoTouch_UIKit_UITapGestureRecognizer
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ImageUrl
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ImageUrl_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_DefaultImagePath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_DefaultImagePath_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_get_ErrorImagePath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_set_ErrorImagePath_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Action
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__InitializeImageHelperb__0
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ImageUrl
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ImageUrl_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_DefaultImagePath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_DefaultImagePath_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_get_ErrorImagePath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_set_ErrorImagePath_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper__ctor_System_Func_1_MonoTouch_UIKit_UIImageView
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_ImageHelperOnImageChanged_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_TableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectionChangedCommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_AccessoryTappedCommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_AccessoryTappedCommand_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_AccessoryButtonTapped_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_RowSelected_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_get_SelectedItem
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_add_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_remove_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_GetCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_CellDisplayingEnded_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCell_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_NumberOfSections_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel__ctor_MonoTouch_UIKit_UIPickerView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_ItemsSource
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_ItemsSource_System_Collections_IEnumerable
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Reload
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetComponentCount_MonoTouch_UIKit_UIPickerView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetRowsInComponent_MonoTouch_UIKit_UIPickerView_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_GetTitle_MonoTouch_UIKit_UIPickerView_int_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_RowTitle_int_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_Selected_MonoTouch_UIKit_UIPickerView_int_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedItem
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedItem_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_add_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_remove_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_get_SelectedChangedCommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_set_SelectedChangedCommand_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxPickerViewModel_ShowSelectedItem
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ItemsSource
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ItemsSource_System_Collections_IEnumerable
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_UseAnimations
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_UseAnimations_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_AddAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_AddAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_RemoveAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_RemoveAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_get_ReplaceAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_set_ReplaceAnimation_MonoTouch_UIKit_UITableViewRowAnimation
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_RowsInSection_MonoTouch_UIKit_UITableView_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_CellIdentifier
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSString
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_get_BindingDescriptions
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_CreateDefaultBindableCell_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__cctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellCreator
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellCreator_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellModifier
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellModifier_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifierOverride
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_set_CellIdentifierOverride_System_Func_1_MonoTouch_Foundation_NSString
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_get_CellIdentifier
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource__Initializeb__2_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_HandleEditTextValueChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SubscribeToEvents
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_ShouldSkipSetValueForViewSpecificReasons_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_CurrentText
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding__ctor_object_System_Reflection_PropertyInfo
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_HandleSliderValueChanged_object_System_EventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_SubscribeToEvents
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUISliderValueTargetBinding_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_Button
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_DefaultMode
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_get_TargetType
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding_SetValueImpl_object_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_DefaultCellIdentifier
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_CollectionView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectionChangedCommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectionChangedCommand_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath_object
	bl method_addresses
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ItemSelected_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_get_SelectedItem
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_add_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_remove_SelectedItemChanged_System_EventHandler
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_GetCell_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_CellDisplayingEnded_MonoTouch_UIKit_UICollectionView_MonoTouch_UIKit_UICollectionViewCell_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_NumberOfSections_MonoTouch_UIKit_UICollectionView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__cctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_get_ItemsSource
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_set_ItemsSource_System_Collections_IEnumerable
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemAt_MonoTouch_Foundation_NSIndexPath
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_CollectionChangedOnCollectionChanged_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewSource_GetItemsCount_MonoTouch_UIKit_UICollectionView_int
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_BindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_Accessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_get_DataContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_set_DataContext_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageLoader
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_TitleText
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_TitleText_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_DetailText
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_DetailText_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_ImageUrl
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_ImageUrl_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_get_SelectedCommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_set_SelectedCommand_System_Windows_Input_ICommand
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_SetSelected_bool_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__InitializeImageLoaderb__0
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_BindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_intptr_string
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_get_DataContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell_set_DataContext_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_get_CellIdentifier
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_string_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource__ctor_MonoTouch_UIKit_UITableView_System_Type_string
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxSimpleTableViewSource_GetOrCreateCellFor_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_BindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_intptr
	bl _Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_Dispose_bool
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_get_DataContext
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxView_set_DataContext_object
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__c__DisplayClass2___ctorb__0_MonoTouch_UIKit_UIImage
	bl method_addresses
	bl _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0
	bl _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UIImage_invoke_void__this___T_MonoTouch_UIKit_UIImage
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIView
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextField
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIDatePicker
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UILabel
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UITextView
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_invoke_TResult__this___T_MonoTouch_UIKit_UIButton
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_invoke_void__this___T_MonoTouch_UIKit_UITapGestureRecognizer
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_UIKit_UIImageView_invoke_TResult__this__
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_MonoTouch_UIKit_UIImage
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_invoke_void__this___object_TEventArgs_object_System_Collections_Specialized_NotifyCollectionChangedEventArgs
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_TResult__this___T1_T2_T3_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_invoke_void__this___T_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell
	bl _Cirrious_MvvmCross_Binding_Touch__wrapper_delegate_invoke_System_Func_1_MonoTouch_Foundation_NSString_invoke_TResult__this__
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 322,10,33,2
	.short 0, 10, 26, 37, 48, 59, 70, 81
	.short 92, 103, 114, 125, 141, 152, 163, 174
	.short 185, 196, 207, 218, 229, 240, 256, 267
	.short 278, 289, 300, 311, 322, 333, 344, 355
	.short 366
	.byte 1,2,122,2,53,3,3,3,3,5,128,200,3,3,3,3,3,3,8,255,255,255,255,30,128,228,128,230,6,3,2,2
	.byte 3,2,3,4,2,129,4,3,5,3,4,2,3,4,8,2,129,40,6,4,10,6,2,6,4,2,4,129,86,2,2,11
	.byte 3,5,8,11,4,2,129,142,2,2,4,2,4,6,3,2,2,129,173,8,2,2,4,2,2,4,2,2,129,203,2,2
	.byte 8,2,2,2,2,2,2,129,229,2,2,12,2,2,4,4,4,4,130,13,4,14,2,2,2,4,2,5,2,130,52,2
	.byte 2,2,6,255,255,255,253,192,0,130,69,4,2,130,79,6,6,8,8,2,2,4,2,12,130,134,2,2,2,4,2,8
	.byte 2,2,6,130,170,2,2,2,2,5,4,2,12,2,130,205,2,2,2,2,2,2,2,2,16,130,241,2,3,7,5,5
	.byte 6,3,3,3,131,31,3,5,21,2,5,2,2,20,2,131,95,2,2,2,2,2,2,2,4,2,131,117,2,11,3,2
	.byte 5,8,2,2,6,131,162,2,11,8,4,5,2,3,5,3,131,210,3,3,3,3,6,255,255,255,252,28,131,233,5,3
	.byte 131,246,7,7,9,9,3,5,2,4,2,132,40,16,2,2,2,2,2,4,2,2,132,76,2,4,2,2,2,2,2,2
	.byte 4,132,102,4,4,2,2,2,2,16,2,2,132,140,2,2,2,2,2,2,4,2,2,132,162,2,2,2,2,2,2,2
	.byte 2,2,132,182,4,4,4,2,7,6,6,2,12,132,231,2,2,2,2,2,4,4,4,2,0,133,1,2,2,4,4,4
	.byte 4,4,4,133,33,4,4,4,4,4,4,4,4,4,133,73,4
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 37,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 1407,303,38,0,0,0,1443,306
	.long 0,1527,313,41,1636,321,0,0
	.long 0,0,1563,316,0,1503,311,0
	.long 0,0,0,1389,302,0,0,0
	.long 0,1467,308,37,1419,304,42,0
	.long 0,0,1455,307,0,0,0,0
	.long 1371,301,0,0,0,0,0,0
	.long 0,0,0,0,1587,318,0,0
	.long 0,0,0,0,0,0,0,0
	.long 1575,317,0,1431,305,0,0,0
	.long 0,1539,314,40,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 1479,309,0,1491,310,39,1515,312
	.long 0,1551,315,0,1599,319,0,1611
	.long 320,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 21,301,1371,302,1389,303,1407,304
	.long 1419,305,1431,306,1443,307,1455,308
	.long 1467,309,1479,310,1491,311,1503,312
	.long 1515,313,1527,314,1539,315,1551,316
	.long 1563,317,1575,318,1587,319,1599,320
	.long 1611,321,1636
.section __TEXT, __const
	.align 3
class_name_table:

	.short 73, 0, 0, 0, 0, 0, 0, 2
	.short 73, 15, 0, 0, 0, 0, 0, 1
	.short 0, 0, 0, 0, 0, 0, 0, 26
	.short 0, 23, 0, 0, 0, 0, 0, 0
	.short 0, 11, 75, 0, 0, 13, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 33, 0, 0, 0, 0, 0, 39
	.short 0, 6, 80, 20, 0, 18, 77, 0
	.short 0, 4, 0, 36, 0, 0, 0, 27
	.short 0, 21, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 7, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 17, 0, 22
	.short 74, 19, 0, 5, 78, 3, 0, 0
	.short 0, 38, 0, 29, 0, 0, 0, 9
	.short 76, 24, 0, 8, 0, 0, 0, 25
	.short 0, 16, 0, 14, 0, 0, 0, 12
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 10, 0, 28, 79, 30
	.short 0, 31, 0, 32, 0, 34, 0, 35
	.short 0, 37, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 238,10,24,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 165
	.short 176, 187, 198, 209, 220, 231, 242, 253
	.byte 134,119,2,1,1,1,3,4,12,3,4,134,153,4,7,7,3,7,7,3,3,4,134,210,3,4,12,3,4,12,4,4
	.byte 12,135,19,7,3,7,7,3,4,12,4,4,135,74,4,4,4,6,2,2,6,2,2,135,108,2,6,2,2,6,6,2
	.byte 2,6,135,148,2,2,6,6,2,2,6,6,2,135,184,6,2,2,2,2,2,2,2,2,135,213,4,5,7,4,7,7
	.byte 7,7,4,136,16,7,4,7,4,7,7,3,3,3,136,64,3,3,3,3,3,3,5,2,2,136,96,4,7,5,7,5
	.byte 5,5,7,5,136,151,4,7,5,6,2,2,6,4,5,136,197,2,2,5,5,5,5,7,5,2,136,237,4,2,2,4
	.byte 5,3,7,5,4,137,39,6,5,6,2,2,6,3,11,11,137,102,11,11,11,12,6,2,2,6,11,137,179,4,7,12
	.byte 5,5,4,3,6,4,137,233,4,4,3,5,6,4,4,4,12,138,28,3,4,4,7,5,4,4,7,4,138,73,6,4
	.byte 6,3,3,6,3,3,4,138,116,5,5,3,3,4,5,4,4,5,138,157,3,6,5,5,3,3,5,5,4,138,200,3
	.byte 6,3,3,6,1,4
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 322,10,33,2
	.short 0, 11, 27, 38, 49, 60, 71, 82
	.short 93, 104, 115, 126, 142, 153, 164, 175
	.short 186, 197, 208, 219, 230, 241, 257, 268
	.short 279, 290, 301, 312, 323, 334, 345, 356
	.short 367
	.byte 144,16,3,3,3,3,3,3,3,3,3,144,46,3,3,3,3,3,3,3,255,255,255,239,189,144,70,144,73,3,3,3
	.byte 3,3,3,3,3,3,144,103,3,3,3,3,3,3,3,3,3,144,133,3,3,3,3,3,3,3,3,3,144,163,3,3
	.byte 3,3,3,3,3,3,3,144,193,3,3,3,3,3,3,3,3,3,144,223,3,3,3,3,3,3,3,3,10,145,24,23
	.byte 3,3,3,3,3,3,3,3,145,74,3,3,3,3,3,3,3,3,3,145,104,3,3,10,3,3,3,3,3,3,145,141
	.byte 3,3,3,3,255,255,255,238,103,0,145,167,3,3,145,176,3,3,3,3,3,3,3,3,3,145,206,3,3,3,3,3
	.byte 3,3,3,3,145,236,3,3,3,3,3,3,3,3,3,146,10,3,3,3,3,3,3,3,3,3,146,40,3,3,3,3
	.byte 3,3,3,3,3,146,70,3,3,3,3,3,3,3,3,3,146,100,3,3,3,3,3,3,3,3,3,146,130,3,3,3
	.byte 3,3,3,3,3,3,146,160,3,3,3,3,3,3,3,3,3,146,190,3,3,3,3,14,255,255,255,237,40,146,219,3
	.byte 3,146,228,3,3,3,3,3,3,3,3,3,147,2,3,3,3,3,3,3,3,3,3,147,32,3,3,3,3,3,3,3
	.byte 3,3,147,62,3,3,3,3,3,3,3,3,3,147,92,3,3,3,3,3,3,3,3,3,147,122,3,3,3,3,3,3
	.byte 3,3,3,147,152,3,3,3,3,3,3,3,3,3,147,182,3,3,3,3,3,3,3,3,3,0,147,212,30,30,3,3
	.byte 3,3,3,3,148,37,3,3,3,3,3,3,3,3,3,148,67,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11,23,12,13,0,72,14,8,135
	.byte 2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3
	.byte 142,1,68,14,56,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,32,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 39,10,4,2
	.short 0, 12, 23, 38
	.byte 148,101,7,128,166,83,76,80,74,76,82,58,151,102,82,76,76,76,76,23,23,5,24,154,43,23,128,152,80,128,158,128
	.byte 165,128,163,75,79,81,158,89,91,7,129,29,129,27,128,253,128,161,5,128,249

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Binding_Touch_plt:
_p_1_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 952,2790
_p_2_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterPropertyInfoBindingFactory_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Type_System_Type_string:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 956,2802
_p_3_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextField_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 960,2807
_p_4_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIDatePicker_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 964,2819
_p_5_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UILabel_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 968,2831
_p_6_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UITextView_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 972,2843
_p_7_plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding
plt_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_MvxTargetBindingFactoryRegistryExtensions_RegisterCustomBindingFactory_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_string_System_Func_2_MonoTouch_UIKit_UIButton_Cirrious_MvvmCross_Binding_Bindings_Target_IMvxTargetBinding:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 976,2855
_p_8_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 980,2867
_p_9_plt_Cirrious_MvvmCross_Binding_MvxCoreBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_MvxCoreBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
plt_Cirrious_MvvmCross_Binding_MvxCoreBindingBuilder_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 984,2890
_p_10_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 988,2895

.set _p_11_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibilityTargetBinding__ctor_MonoTouch_UIKit_UIView

.set _p_12_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView

.set _p_13_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewHiddenTargetBinding__ctor_MonoTouch_UIKit_UIView
_p_14_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding__ctor_MonoTouch_UIKit_UITextField:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1004,2928
_p_15_plt_System_Type_GetProperty_string_llvm:
	.no_dead_strip plt_System_Type_GetProperty_string
plt_System_Type_GetProperty_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1008,2930

.set _p_16_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUIDatePickerTimeTargetBinding__ctor_object_System_Reflection_PropertyInfo
_p_17_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUILabelTextTargetBinding__ctor_MonoTouch_UIKit_UILabel:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1016,2937

.set _p_18_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding__ctor_MonoTouch_UIKit_UITextField

.set _p_19_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding__ctor_MonoTouch_UIKit_UITextView
_p_20_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIButtonTitleTargetBinding__ctor_MonoTouch_UIKit_UIButton:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1028,2944
_p_21_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUIViewTapTargetBinding__ctor_MonoTouch_UIKit_UIView_uint_uint:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1032,2947
_p_22_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker__ctor_object_System_Reflection_PropertyInfo:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1036,2949
_p_23_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UIDatePicker_get_View:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1040,2960
_p_24_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1044,2971
_p_25_plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_ValueChanged_System_EventHandler:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1048,3016
_p_26_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1052,3021
_p_27_plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__
plt_Cirrious_MvvmCross_Binding_MvxBindingTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1056,3047
_p_28_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_Dispose_bool:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1060,3052
_p_29_plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_remove_ValueChanged_System_EventHandler:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1064,3057
_p_30_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding_get_Target:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1068,3062
_p_31_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxConvertingTargetBinding__ctor_object:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1072,3067
_p_32_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIDatePickerTargetBinding__ctor_object_System_Reflection_PropertyInfo:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1076,3072
_p_33_plt_MonoTouch_Foundation_NSDate_op_Implicit_MonoTouch_Foundation_NSDate_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSDate_op_Implicit_MonoTouch_Foundation_NSDate
plt_MonoTouch_Foundation_NSDate_op_Implicit_MonoTouch_Foundation_NSDate:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1080,3074
_p_34_plt_System_DateTime_get_Date_llvm:
	.no_dead_strip plt_System_DateTime_get_Date
plt_System_DateTime_get_Date:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1084,3079
_p_35_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1088,3084
_p_36_plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime
plt_MonoTouch_Foundation_NSDate_op_Implicit_System_DateTime:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1092,3114
_p_37_plt_System_DateTime_get_UtcNow_llvm:
	.no_dead_strip plt_System_DateTime_get_UtcNow
plt_System_DateTime_get_UtcNow:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1096,3119
_p_38_plt_MonoTouch_Foundation_NSCalendar_get_CurrentCalendar_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSCalendar_get_CurrentCalendar
plt_MonoTouch_Foundation_NSCalendar_get_CurrentCalendar:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1100,3124
_p_39_plt_System_TimeSpan__ctor_int_int_int_llvm:
	.no_dead_strip plt_System_TimeSpan__ctor_int_int_int
plt_System_TimeSpan__ctor_int_int_int:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1104,3129
_p_40_plt_System_DateTime_get_Now_llvm:
	.no_dead_strip plt_System_DateTime_get_Now
plt_System_DateTime_get_Now:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1108,3134
_p_41_plt_System_DateTime_get_Year_llvm:
	.no_dead_strip plt_System_DateTime_get_Year
plt_System_DateTime_get_Year:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1112,3139
_p_42_plt_System_DateTime_get_Month_llvm:
	.no_dead_strip plt_System_DateTime_get_Month
plt_System_DateTime_get_Month:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1116,3144
_p_43_plt_System_DateTime_get_Day_llvm:
	.no_dead_strip plt_System_DateTime_get_Day
plt_System_DateTime_get_Day:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1120,3149
_p_44_plt_System_TimeSpan_get_Hours_llvm:
	.no_dead_strip plt_System_TimeSpan_get_Hours
plt_System_TimeSpan_get_Hours:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1124,3154
_p_45_plt_System_TimeSpan_get_Minutes_llvm:
	.no_dead_strip plt_System_TimeSpan_get_Minutes
plt_System_TimeSpan_get_Minutes:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1128,3159
_p_46_plt_System_TimeSpan_get_Seconds_llvm:
	.no_dead_strip plt_System_TimeSpan_get_Seconds
plt_System_TimeSpan_get_Seconds:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1132,3164
_p_47_plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind_llvm:
	.no_dead_strip plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind
plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1136,3169
_p_48_plt_System_TimeSpan_FromSeconds_double_llvm:
	.no_dead_strip plt_System_TimeSpan_FromSeconds_double
plt_System_TimeSpan_FromSeconds_double:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1140,3174
_p_49_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar__ctor_object_System_Reflection_PropertyInfo:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1144,3179
_p_50_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISearchBar_get_View:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1148,3190
_p_51_plt_MonoTouch_UIKit_UISearchBar_add_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UISearchBar_add_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
plt_MonoTouch_UIKit_UISearchBar_add_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1152,3201
_p_52_plt_MonoTouch_UIKit_UISearchBar_remove_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UISearchBar_remove_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs
plt_MonoTouch_UIKit_UISearchBar_remove_TextChanged_System_EventHandler_1_MonoTouch_UIKit_UISearchBarTextChangedEventArgs:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1156,3206
_p_53_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding__ctor_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding__ctor_object
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxTargetBinding__ctor_object:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1160,3211
_p_54_plt_MonoTouch_UIKit_UITextField_set_ShouldReturn_MonoTouch_UIKit_UITextFieldCondition_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextField_set_ShouldReturn_MonoTouch_UIKit_UITextFieldCondition
plt_MonoTouch_UIKit_UITextField_set_ShouldReturn_MonoTouch_UIKit_UITextFieldCondition:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1164,3216
_p_55_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldShouldReturnTargetBinding_get_View:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1168,3221
_p_56_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextViewTextTargetBinding_get_View:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1172,3223
_p_57_plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler
plt_MonoTouch_UIKit_UITextView_add_Changed_System_EventHandler:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1176,3225
_p_58_plt_MonoTouch_UIKit_UITextView_remove_Changed_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITextView_remove_Changed_System_EventHandler
plt_MonoTouch_UIKit_UITextView_remove_Changed_System_EventHandler:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1180,3230
_p_59_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch__ctor_object_System_Reflection_PropertyInfo:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1184,3235
_p_60_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISwitch_get_View:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1188,3246

.set _p_61_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding__ctor_MonoTouch_UIKit_UIView
_p_62_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxBaseUIViewVisibleTargetBinding_get_View:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1196,3259
_p_63_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ConvertToBoolean_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ConvertToBoolean_object
plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ConvertToBoolean_object:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1200,3261
_p_64_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint
plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxTapGestureRecognizerBehaviour__ctor_MonoTouch_UIKit_UIView_uint_uint:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1204,3266

.set _p_65_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior__ctor
_p_66_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object
plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_FireCommand_object:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1212,3270
_p_67_plt__jit_icall_mono_ldvirtfn_llvm:
	.no_dead_strip plt__jit_icall_mono_ldvirtfn
plt__jit_icall_mono_ldvirtfn:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1216,3272
_p_68_plt_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer__ctor_object_intptr
plt_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer__ctor_object_intptr:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1220,3288
_p_69_plt_MonoTouch_UIKit_UITapGestureRecognizer__ctor_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITapGestureRecognizer__ctor_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer
plt_MonoTouch_UIKit_UITapGestureRecognizer__ctor_System_Action_1_MonoTouch_UIKit_UITapGestureRecognizer:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1224,3299

.set _p_70_plt_Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_AddGestureRecognizer_MonoTouch_UIKit_UIView_MonoTouch_UIKit_UIGestureRecognizer
_p_71_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ImageUrl_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ImageUrl
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ImageUrl:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1232,3306
_p_72_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ImageUrl_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ImageUrl_string
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ImageUrl_string:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1236,3317
_p_73_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_DefaultImagePath_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_DefaultImagePath
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_DefaultImagePath:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1240,3328
_p_74_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_DefaultImagePath_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_DefaultImagePath_string
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_DefaultImagePath_string:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1244,3339
_p_75_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ErrorImagePath_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ErrorImagePath
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_get_ErrorImagePath:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1248,3350
_p_76_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ErrorImagePath_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ErrorImagePath_string
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_set_ErrorImagePath_string:
_p_76:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1252,3361
_p_77_plt_MonoTouch_UIKit_UIImageView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor
plt_MonoTouch_UIKit_UIImageView__ctor:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1256,3372
_p_78_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView_InitializeImageHelper_System_Action:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1260,3377
_p_79_plt_MonoTouch_UIKit_UIImageView__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor_intptr
plt_MonoTouch_UIKit_UIImageView__ctor_intptr:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1264,3379
_p_80_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader__ctor_System_Func_1_MonoTouch_UIKit_UIImageView_System_Action:
_p_80:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1268,3384
_p_81_plt_MonoTouch_UIKit_UIImageView_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView_Dispose_bool
plt_MonoTouch_UIKit_UIImageView_Dispose_bool:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1272,3387
_p_82_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_Dispose_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_Dispose
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage_Dispose:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1276,3392
_p_83_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage:
_p_83:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1280,3403
_p_84_plt_System_GC_SuppressFinalize_object_llvm:
	.no_dead_strip plt_System_GC_SuppressFinalize_object
plt_System_GC_SuppressFinalize_object:
_p_84:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1284,3415
_p_85_plt_MonoTouch_UIKit_UITableViewSource__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewSource__ctor
plt_MonoTouch_UIKit_UITableViewSource__ctor:
_p_85:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1288,3420
_p_86_plt_MonoTouch_UIKit_UITableViewSource__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewSource__ctor_intptr
plt_MonoTouch_UIKit_UITableViewSource__ctor_intptr:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1292,3425
_p_87_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Warning_string_object__
plt_Cirrious_CrossCore_Mvx_Warning_string_object__:
_p_87:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1296,3430
_p_88_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_set_SelectedItem_object:
_p_88:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1300,3435
_p_89_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_89:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1304,3437
_p_90_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler:
_p_90:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1308,3442
_p_91_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Remove_System_Delegate_System_Delegate
plt_System_Delegate_Remove_System_Delegate_System_Delegate:
_p_91:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1312,3454
_p_92_plt_MonoTouch_UIKit_UIPickerViewModel__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIPickerViewModel__ctor
plt_MonoTouch_UIKit_UIPickerViewModel__ctor:
_p_92:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1316,3459
_p_93_plt_MonoTouch_Foundation_NSObject_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSObject_Dispose_bool
plt_MonoTouch_Foundation_NSObject_Dispose_bool:
_p_93:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1320,3464
_p_94_plt_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs__ctor_object_intptr
plt_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs__ctor_object_intptr:
_p_94:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1324,3469
_p_95_plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs
plt_Cirrious_CrossCore_WeakSubscription_MvxWeakSubscriptionExtensionMethods_WeakSubscribe_System_Collections_Specialized_INotifyCollectionChanged_System_EventHandler_1_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
_p_95:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1328,3480
_p_96_plt_Cirrious_CrossCore_Mvx_Trace_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Trace_string_object__
plt_Cirrious_CrossCore_Mvx_Trace_string_object__:
_p_96:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1332,3485
_p_97_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable
plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_Count_System_Collections_IEnumerable:
_p_97:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1336,3490
_p_98_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int
plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_ElementAt_System_Collections_IEnumerable_int:
_p_98:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1340,3495
_p_99_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_GetPosition_System_Collections_IEnumerable_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_GetPosition_System_Collections_IEnumerable_object
plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxEnumerableExtensions_GetPosition_System_Collections_IEnumerable_object:
_p_99:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1344,3500

.set _p_100_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_MonoTouch_UIKit_UITableView
_p_101_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource__ctor_intptr:
_p_101:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1352,3507
_p_102_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_TryDoAnimatedChange_System_Collections_Specialized_NotifyCollectionChangedEventArgs:
_p_102:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1356,3509
_p_103_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource_CreateNSIndexPathArray_int_int:
_p_103:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1360,3512
_p_104_plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int
plt_MonoTouch_Foundation_NSIndexPath_FromRowSection_int_int:
_p_104:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1364,3515

.set _p_105_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellAccessory

.set _p_106_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_string_MonoTouch_UIKit_UITableViewCellAccessory
_p_107_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_intptr:
_p_107:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1376,3526
_p_108_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource_ParseBindingText_string:
_p_108:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1380,3529

.set _p_109_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewSource__ctor_MonoTouch_UIKit_UITableView
_p_110_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Binding_Binders_IMvxBindingDescriptionParser_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Binding_Binders_IMvxBindingDescriptionParser
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Binding_Binders_IMvxBindingDescriptionParser:
_p_110:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1388,3535
_p_111_plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UITableView_DequeueReusableCell_MonoTouch_Foundation_NSString:
_p_111:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1392,3547

.set _p_112_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
_p_113_plt_MonoTouch_Foundation_NSString__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSString__ctor_string
plt_MonoTouch_Foundation_NSString__ctor_string:
_p_113:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1400,3555
_p_114_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_MonoTouch_UIKit_UITableView:
_p_114:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1404,3560
_p_115_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxActionBasedTableViewSource_Initialize:
_p_115:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1408,3563
_p_116_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewSource__ctor_intptr:
_p_116:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1412,3566
_p_117_plt_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_object_intptr
plt_System_Func_4_MonoTouch_UIKit_UITableView_MonoTouch_Foundation_NSIndexPath_object_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_object_intptr:
_p_117:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1416,3569
_p_118_plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View
plt_Cirrious_MvvmCross_Binding_Touch_Target_MvxUITextFieldTextTargetBinding_get_View:
_p_118:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1420,3580
_p_119_plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_add_EditingChanged_System_EventHandler:
_p_119:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1424,3583
_p_120_plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ShouldSkipSetValueAsHaveNearlyIdenticalNumericText_Cirrious_MvvmCross_Binding_ExtensionMethods_IMvxEditableTextView_object_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ShouldSkipSetValueAsHaveNearlyIdenticalNumericText_Cirrious_MvvmCross_Binding_ExtensionMethods_IMvxEditableTextView_object_object
plt_Cirrious_MvvmCross_Binding_ExtensionMethods_MvxBindingExtensions_ShouldSkipSetValueAsHaveNearlyIdenticalNumericText_Cirrious_MvvmCross_Binding_ExtensionMethods_IMvxEditableTextView_object_object:
_p_120:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1428,3588
_p_121_plt_MonoTouch_UIKit_UIControl_remove_EditingChanged_System_EventHandler_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIControl_remove_EditingChanged_System_EventHandler
plt_MonoTouch_UIKit_UIControl_remove_EditingChanged_System_EventHandler:
_p_121:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1432,3593
_p_122_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider__ctor_object_System_Reflection_PropertyInfo_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider__ctor_object_System_Reflection_PropertyInfo
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider__ctor_object_System_Reflection_PropertyInfo:
_p_122:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1436,3598
_p_123_plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View
plt_Cirrious_MvvmCross_Binding_Bindings_Target_MvxPropertyInfoTargetBinding_1_MonoTouch_UIKit_UISlider_get_View:
_p_123:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1440,3609

.set _p_124_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_MonoTouch_Foundation_NSString
_p_125_plt_MonoTouch_UIKit_UICollectionViewSource__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewSource__ctor
plt_MonoTouch_UIKit_UICollectionViewSource__ctor:
_p_125:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1448,3623
_p_126_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_set_SelectedItem_object:
_p_126:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1452,3628
_p_127_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource__ctor_MonoTouch_UIKit_UICollectionView:
_p_127:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1456,3631

.set _p_128_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string
_p_129_plt_MonoTouch_UIKit_UITableViewCell__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor
plt_MonoTouch_UIKit_UITableViewCell__ctor:
_p_129:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1464,3637
_p_130_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_string:
_p_130:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1468,3642
_p_131_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription:
_p_131:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1472,3647

.set _p_132_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_intptr
_p_133_plt_MonoTouch_UIKit_UITableViewCell__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor_intptr
plt_MonoTouch_UIKit_UITableViewCell__ctor_intptr:
_p_133:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1480,3655
_p_134_plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UITableViewCell__ctor_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString:
_p_134:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1484,3660
_p_135_plt_MonoTouch_UIKit_UITableViewCell_get_Accessory_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell_get_Accessory
plt_MonoTouch_UIKit_UITableViewCell_get_Accessory:
_p_135:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1488,3665
_p_136_plt_MonoTouch_UIKit_UITableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory
plt_MonoTouch_UIKit_UITableViewCell_set_Accessory_MonoTouch_UIKit_UITableViewCellAccessory:
_p_136:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1492,3670
_p_137_plt_MonoTouch_UIKit_UITableViewCell_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell_Dispose_bool
plt_MonoTouch_UIKit_UITableViewCell_Dispose_bool:
_p_137:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1496,3675

.set _p_138_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell__ctor_string_intptr
_p_139_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxStandardTableViewCell_InitializeImageLoader:
_p_139:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1504,3683

.set _p_140_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_intptr

.set _p_141_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory

.set _p_142_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_MonoTouch_UIKit_UITableViewCellStyle_MonoTouch_Foundation_NSString_MonoTouch_UIKit_UITableViewCellAccessory
_p_143_plt_System_Action__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Action__ctor_object_intptr
plt_System_Action__ctor_object_intptr:
_p_143:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1520,3695
_p_144_plt_MonoTouch_UIKit_UITableViewCell_SetSelected_bool_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell_SetSelected_bool_bool
plt_MonoTouch_UIKit_UITableViewCell_SetSelected_bool_bool:
_p_144:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1524,3700
_p_145_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool
plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell_Dispose_bool:
_p_145:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1528,3705
_p_146_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewCell__ctor
plt_MonoTouch_UIKit_UICollectionViewCell__ctor:
_p_146:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1532,3708
_p_147_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewCell__ctor_intptr
plt_MonoTouch_UIKit_UICollectionViewCell__ctor_intptr:
_p_147:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1536,3713
_p_148_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_CreateBindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner:
_p_148:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1540,3718

.set _p_149_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_intptr
_p_150_plt_MonoTouch_UIKit_UICollectionViewCell_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewCell_Dispose_bool
plt_MonoTouch_UIKit_UICollectionViewCell_Dispose_bool:
_p_150:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1548,3726
_p_151_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_151:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1552,3731
_p_152_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool
plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool:
_p_152:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1556,3757
_p_153_plt_MonoTouch_UIKit_UINib_FromName_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINib_FromName_string_MonoTouch_Foundation_NSBundle
plt_MonoTouch_UIKit_UINib_FromName_string_MonoTouch_Foundation_NSBundle:
_p_153:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1560,3762
_p_154_plt_MonoTouch_Foundation_NSBundle_get_MainBundle_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSBundle_get_MainBundle
plt_MonoTouch_Foundation_NSBundle_get_MainBundle:
_p_154:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1564,3767
_p_155_plt_MonoTouch_UIKit_UITableView_RegisterClassForCellReuse_System_Type_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableView_RegisterClassForCellReuse_System_Type_MonoTouch_Foundation_NSString
plt_MonoTouch_UIKit_UITableView_RegisterClassForCellReuse_System_Type_MonoTouch_Foundation_NSString:
_p_155:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1568,3772
_p_156_plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage__ctor_System_Action_1_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage__ctor_System_Action_1_MonoTouch_UIKit_UIImage
plt_Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader_1_MonoTouch_UIKit_UIImage__ctor_System_Action_1_MonoTouch_UIKit_UIImage:
_p_156:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1572,3777
_p_157_plt_MonoTouch_UIKit_UIView__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView__ctor
plt_MonoTouch_UIKit_UIView__ctor:
_p_157:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1576,3788
_p_158_plt_MonoTouch_UIKit_UIView__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView__ctor_intptr
plt_MonoTouch_UIKit_UIView__ctor_intptr:
_p_158:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1580,3793
_p_159_plt_MonoTouch_UIKit_UIView_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView_Dispose_bool
plt_MonoTouch_UIKit_UIView_Dispose_bool:
_p_159:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1584,3798

.set _p_160_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage_llvm, _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader_OnImage_MonoTouch_UIKit_UIImageView_MonoTouch_UIKit_UIImage
_p_161_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_161:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1592,3806
_p_162_plt_MonoTouch_UIKit_UIImageView__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImageView__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIImageView__ctor_System_Drawing_RectangleF:
_p_162:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1596,3844
_p_163_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_163:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1600,3849
_p_164_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_164:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1604,3877
_p_165_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_ToLongString_System_Exception:
_p_165:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1608,3916
_p_166_plt_MonoTouch_UIKit_UITableViewCell__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewCell__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UITableViewCell__ctor_System_Drawing_RectangleF:
_p_166:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1612,3921
_p_167_plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UICollectionViewCell__ctor_System_Drawing_RectangleF:
_p_167:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1616,3926

.set _p_168_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF_llvm, _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF

.set _p_169_plt_Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF_llvm, _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
_p_170_plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF
plt_MonoTouch_UIKit_UIView__ctor_System_Drawing_RectangleF:
_p_170:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1628,3937
_p_171_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_171:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1632,3960
_p_172_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_172:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1636,4004
_p_173_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_173:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1640,4058
_p_174_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_174:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1644,4066
_p_175_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_175:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got - . + 1648,4085
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 7
	.asciz "Cirrious.MvvmCross.Binding.Touch"
	.asciz "AF50D302-0E63-49BF-AF60-DEF8AC108924"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Binding"
	.asciz "0CA1A6F2-0F7E-44D7-B6B8-AE8E0BEDDDE8"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.CrossCore.Touch"
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "AF50D302-0E63-49BF-AF60-DEF8AC108924"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Binding.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Binding_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch__Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 238,1656,176,322,11,387000831,0,9126
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Binding_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Binding_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,120,4,46,78,77,43,8,46,76,75,43,10,46,74,73,43,18,62,72,71,59,21,70,69,68,67,24,66
	.byte 65,64,63,26,62,61,60,59,27,58,57,56,55,35,54,53,52,51,38,46,50,49,43,40,46,48,47,43,42,46,45,44
	.byte 43,6,42,42,41,6,40,40,39,6,38,38,37,36,35,35,34,23,33,32,31,30,29,28,27,27,23,19,26,26,23,25
	.byte 24,24,23,22,21,21,20,19,18,18,17,16,15,14,13,12,11,6,10,10,9,6,8,8,7,6,5,5,5,0,0,0
	.byte 51,79,91,95,81,91,94,81,16,15,81,31,30,81,13,12,81,16,15,81,93,92,81,91,90,81,88,89,81,88,87,81
	.byte 23,84,81,23,86,81,23,85,81,23,84,81,23,33,81,83,82,81,80,81,0,1,96,0,1,97,0,1,98,0,1,99
	.byte 0,3,100,16,15,0,1,101,0,1,102,0,1,103,0,1,104,0,1,105,0,1,105,0,1,105,0,6,111,110,109,108
	.byte 107,106,0,0,0,0,0,4,109,108,107,106,0,1,112,0,0,0,0,0,1,113,0,0,0,1,114,0,2,114,115,0
	.byte 0,0,1,116,0,1,117,0,3,118,116,114,0,1,119,0,2,111,120,0,0,0,1,121,0,2,119,122,0,6,111,127
	.byte 126,125,124,123,0,0,0,0,0,4,126,125,124,123,0,1,128,128,0,4,128,130,128,132,128,131,128,129,0,2,128,134
	.byte 128,133,0,0,0,2,128,135,128,135,0,1,128,136,0,0,0,1,128,137,0,0,0,0,0,0,0,6,111,128,140,109
	.byte 128,139,128,138,106,0,1,121,0,2,128,137,122,0,4,128,138,109,128,139,106,0,6,111,128,143,109,128,142,128,141,106
	.byte 0,1,128,144,0,0,0,4,109,128,142,128,141,106,0,0,0,0,0,1,128,145,0,0,0,1,128,136,0,2,128,135
	.byte 128,135,0,1,112,0,0,0,0,0,1,128,146,0,4,112,111,128,148,128,147,0,0,0,0,0,1,128,145,0,0,0
	.byte 0,0,1,128,134,0,0,0,0,0,0,0,0,0,0,0,3,128,149,128,151,128,150,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,5,128,152,128,156,128,155,128,154,128,153,0,0,0,0,0,1,128,157,0,1,128
	.byte 158,0,1,128,159,0,1,128,160,0,1,128,161,0,1,128,162,0,6,128,163,128,167,128,166,128,165,128,164,128,168,0
	.byte 0,0,0,0,0,0,1,128,169,0,0,0,2,111,128,170,0,0,0,0,0,0,0,0,0,0,0,2,128,134,128,133
	.byte 0,2,128,236,111,0,1,128,134,0,0,0,1,128,171,0,2,128,173,128,172,0,2,128,173,128,172,0,3,128,175,128
	.byte 175,128,174,0,3,128,175,128,175,128,174,0,0,0,0,0,1,128,169,0,0,0,5,128,176,128,178,128,178,128,177,128
	.byte 169,0,2,111,128,179,0,0,0,0,0,0,0,1,128,180,0,0,0,3,128,171,128,134,128,133,0,0,0,0,0,2
	.byte 128,173,128,172,0,2,128,173,128,172,0,0,0,0,0,0,0,0,0,2,111,128,181,0,1,128,169,0,0,0,5,128
	.byte 182,128,178,128,178,128,177,128,169,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,128
	.byte 183,128,183,128,184,128,183,128,183,128,183,128,183,0,1,128,184,0,0,1,25,0,1,25,2,128,186,128,185,1,25,1
	.byte 128,186,1,25,1,128,185,1,25,2,111,128,187,1,25,0,1,25,0,1,25,0,1,25,3,128,186,128,189,128,188,1
	.byte 25,0,1,25,1,128,190,1,25,9,128,191,128,197,128,196,128,195,128,194,128,193,128,185,128,192,128,186,0,0,0,2
	.byte 111,128,198,0,0,0,0,0,9,128,199,128,205,128,204,128,203,128,202,128,201,128,201,128,200,128,201,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,128,0,0,0,0,0,0,0,6,111,128,208,109,128,207,128
	.byte 206,106,0,1,121,0,0,0,2,128,128,122,0,4,128,206,109,128,207,106,0,0,0,0,0,2,128,210,128,209,0,1
	.byte 128,211,0,0,0,6,111,128,214,109,128,213,128,212,106,0,4,109,128,213,128,212,106,0,1,128,215,0,2,111,128,216
	.byte 0,0,0,1,121,0,2,128,215,122,1,30,0,1,30,1,128,217,1,30,0,1,30,0,1,30,0,1,30,0,1,30
	.byte 2,128,237,111,1,30,1,128,218,1,30,1,128,134,1,30,0,1,30,1,128,171,1,30,2,128,173,128,172,1,30,2
	.byte 128,173,128,172,1,30,3,128,175,128,175,128,174,1,30,3,128,175,128,175,128,174,1,30,0,1,30,1,128,217,0,0
	.byte 0,1,128,169,0,0,0,0,0,7,128,177,128,178,128,178,128,221,128,220,128,219,128,169,0,0,0,0,0,0,0,0
	.byte 0,0,0,1,128,197,0,0,0,0,0,0,0,0,0,1,128,197,0,0,0,0,0,0,0,0,0,0,0,0,0,1
	.byte 128,222,0,1,128,223,0,1,128,174,0,1,128,195,0,0,0,0,0,0,0,0,0,7,128,152,128,156,128,227,128,226
	.byte 128,155,128,225,128,224,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,134,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,222,0,1,128,223,0,1
	.byte 128,174,0,0,0,3,128,228,111,128,229,0,2,128,228,128,192,0,2,128,228,128,192,0,0,0,5,128,230,128,233,128
	.byte 232,128,231,128,234,0,0,0,0,0,0,0,0,0,0,0,0,0,1,128,222,0,1,128,223,0,1,128,174,0,0,0
	.byte 0,0,0,0,0,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128
	.byte 235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128,235,0,1,128
	.byte 235,0,1,128,235,0,1,128,235,0,1,128,235,0,0,5,19,0,0,1,4,1,18,1,7,133,79,255,253,0,0,0
	.byte 7,133,84,0,198,0,0,80,1,7,133,79,0,255,253,0,0,0,7,133,84,0,198,0,0,81,1,7,133,79,0,255
	.byte 252,0,0,0,1,1,3,219,0,0,26,255,252,0,0,0,1,1,3,219,0,0,7,255,252,0,0,0,1,1,3,219
	.byte 0,0,8,255,252,0,0,0,1,1,3,219,0,0,9,255,252,0,0,0,1,1,3,219,0,0,10,255,252,0,0,0
	.byte 1,1,3,219,0,0,11,255,252,0,0,0,1,1,3,219,0,0,12,255,252,0,0,0,1,1,3,219,0,0,13,255
	.byte 252,0,0,0,1,1,3,219,0,0,14,255,252,0,0,0,1,1,3,219,0,0,16,255,252,0,0,0,1,1,3,219
	.byte 0,0,17,255,252,0,0,0,1,1,3,219,0,0,18,255,252,0,0,0,1,1,3,219,0,0,19,255,252,0,0,0
	.byte 1,1,3,219,0,0,20,255,252,0,0,0,1,1,3,219,0,0,22,255,252,0,0,0,1,1,3,219,0,0,23,255
	.byte 252,0,0,0,1,1,3,219,0,0,24,255,252,0,0,0,1,1,3,219,0,0,25,5,30,0,1,255,255,255,255,255
	.byte 193,0,13,43,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,134,87,12,0,39,42,47,17,0,11,16
	.byte 1,2,4,34,255,254,0,0,0,0,255,43,0,0,1,17,0,33,16,1,2,5,17,0,49,16,1,2,6,19,0,194
	.byte 0,0,28,0,19,0,193,0,0,15,0,17,0,63,19,0,194,0,0,5,0,19,0,193,0,0,3,0,17,0,1,17
	.byte 0,75,16,1,2,7,34,255,254,0,0,0,0,255,43,0,0,2,17,0,101,16,1,2,8,34,255,254,0,0,0,0
	.byte 255,43,0,0,3,17,0,111,16,1,2,9,34,255,254,0,0,0,0,255,43,0,0,4,16,1,2,10,16,1,2,11
	.byte 34,255,254,0,0,0,0,255,43,0,0,5,19,0,194,0,0,11,0,19,0,193,0,0,7,0,17,0,121,19,0,194
	.byte 0,0,8,0,19,0,193,0,0,5,0,17,0,127,16,1,2,12,34,255,254,0,0,0,0,255,43,0,0,6,17,0
	.byte 128,139,16,1,2,13,17,0,128,147,16,1,2,14,17,0,128,167,16,1,2,15,14,3,219,0,0,7,6,16,50,16
	.byte 30,3,219,0,0,7,6,15,50,15,6,14,50,14,14,3,219,0,0,12,6,13,50,13,30,3,219,0,0,12,14,3
	.byte 219,0,0,11,6,12,50,12,30,3,219,0,0,11,14,3,219,0,0,8,6,11,50,11,30,3,219,0,0,8,14,3
	.byte 219,0,0,10,6,10,50,10,30,3,219,0,0,10,14,3,219,0,0,9,6,9,50,9,30,3,219,0,0,9,6,8
	.byte 50,8,6,7,50,7,6,6,50,6,6,5,50,5,19,0,193,0,0,56,0,17,0,128,193,6,194,0,1,170,19,0
	.byte 193,0,0,83,0,17,0,128,221,19,0,193,0,0,35,0,19,0,193,0,0,36,0,19,0,193,0,0,32,0,19,0
	.byte 194,0,0,31,0,17,0,128,237,19,0,194,0,0,24,0,19,0,194,0,0,20,0,17,0,129,5,19,0,193,0,0
	.byte 10,0,17,0,129,23,19,0,219,0,0,15,0,19,0,194,0,0,37,0,14,1,14,14,1,15,14,1,12,14,1,9
	.byte 14,1,6,14,1,7,14,1,27,14,1,10,14,1,29,14,1,13,14,2,130,63,1,6,18,50,18,30,2,130,63,1
	.byte 17,0,129,35,14,6,1,2,130,123,1,11,2,128,217,3,19,0,193,0,0,88,0,14,2,130,38,1,11,2,130,38
	.byte 1,14,2,130,155,1,19,0,193,0,0,94,0,11,2,130,155,1,11,2,129,12,3,17,0,129,166,19,0,193,0,0
	.byte 96,0,11,2,130,146,1,14,3,219,0,0,17,6,39,50,39,30,3,219,0,0,17,17,0,130,20,11,2,128,211,3
	.byte 14,2,129,91,3,6,44,50,44,30,2,129,91,3,6,196,0,9,125,6,196,0,9,126,23,2,129,65,4,19,0,193
	.byte 0,0,34,0,11,2,128,214,3,6,51,50,51,17,0,130,146,6,58,50,58,17,0,131,12,14,2,130,25,1,14,1
	.byte 19,19,0,193,0,0,102,0,11,2,130,27,1,17,0,131,118,6,255,253,0,0,0,3,219,0,0,4,0,198,0,0
	.byte 80,1,2,128,191,3,0,14,3,219,0,0,18,14,2,128,191,3,14,3,219,0,0,19,6,95,50,95,30,3,219,0
	.byte 0,19,14,1,37,6,255,254,0,0,0,0,202,0,0,113,6,255,254,0,0,0,0,202,0,0,114,6,255,254,0,0
	.byte 0,0,202,0,0,115,6,255,254,0,0,0,0,202,0,0,116,6,255,254,0,0,0,0,202,0,0,117,6,255,254,0
	.byte 0,0,0,202,0,0,118,34,255,254,0,0,0,0,255,43,0,0,7,14,3,219,0,0,20,6,105,50,105,30,3,219
	.byte 0,0,20,6,255,254,0,0,0,0,202,0,0,121,6,193,0,17,58,17,0,131,174,16,2,130,61,1,136,41,34,255
	.byte 254,0,0,0,0,255,43,0,0,8,11,2,130,63,1,6,197,0,1,139,23,2,96,5,6,128,130,14,3,219,0,0
	.byte 22,23,2,79,4,17,0,133,91,17,0,134,152,17,0,134,156,6,128,158,6,193,0,4,243,14,6,1,2,23,3,16
	.byte 1,25,40,16,1,25,41,17,0,135,217,34,255,254,0,0,0,0,255,43,0,0,9,6,194,0,3,203,14,1,34,17
	.byte 0,137,44,14,2,29,3,14,6,1,2,128,173,2,14,2,128,173,2,17,0,137,100,14,2,30,2,16,2,130,146,1
	.byte 136,199,17,0,137,120,6,128,172,14,3,219,0,0,23,16,1,26,49,14,3,219,0,0,24,6,128,187,50,128,187,30
	.byte 3,219,0,0,24,6,128,190,50,128,190,17,0,138,209,11,2,130,144,1,11,2,129,34,3,14,2,130,144,1,6,128
	.byte 200,50,128,200,17,0,139,79,11,2,128,177,3,17,0,139,195,16,1,30,52,11,2,129,2,3,6,128,233,50,128,233
	.byte 30,3,219,0,0,22,6,194,0,2,137,6,197,0,1,138,6,129,13,50,129,13,6,195,0,5,249,14,2,129,251,1
	.byte 14,2,3,6,17,0,140,163,14,1,39,14,3,219,0,0,26,6,129,44,50,129,44,30,3,219,0,0,26,33,17,0
	.byte 132,249,17,0,140,55,3,255,254,0,0,0,0,255,43,0,0,1,3,194,0,1,213,3,255,254,0,0,0,0,255,43
	.byte 0,0,2,3,255,254,0,0,0,0,255,43,0,0,3,3,255,254,0,0,0,0,255,43,0,0,4,3,255,254,0,0
	.byte 0,0,255,43,0,0,5,3,255,254,0,0,0,0,255,43,0,0,6,7,20,109,111,110,111,95,111,98,106,101,99,116
	.byte 95,110,101,119,95,102,97,115,116,0,3,194,0,2,237,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119
	.byte 95,115,112,101,99,105,102,105,99,0,3,68,3,72,3,61,3,43,3,193,0,20,143,3,29,3,34,3,128,189,3,50
	.byte 3,128,205,3,63,3,255,254,0,0,0,0,202,0,0,36,3,255,254,0,0,0,0,202,0,0,37,7,42,108,108,118
	.byte 109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97
	.byte 109,112,111,108,105,110,101,0,3,195,0,4,127,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112
	.byte 101,99,105,102,105,99,0,3,194,0,3,249,3,194,0,2,202,3,195,0,4,128,3,194,0,0,193,3,194,0,0,201
	.byte 3,17,3,195,0,0,142,3,193,0,15,64,7,27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116
	.byte 114,102,114,101,101,95,98,111,120,0,3,195,0,0,143,3,193,0,15,77,3,195,0,0,97,3,193,0,19,223,3,193
	.byte 0,15,74,3,193,0,15,78,3,193,0,15,65,3,193,0,15,66,3,193,0,19,227,3,193,0,19,229,3,193,0,19
	.byte 230,3,193,0,15,63,3,193,0,19,248,3,255,254,0,0,0,0,202,0,0,67,3,255,254,0,0,0,0,202,0,0
	.byte 68,3,195,0,5,49,3,195,0,5,50,3,194,0,0,192,3,195,0,5,143,3,42,3,49,3,195,0,5,168,3,195
	.byte 0,5,169,3,255,254,0,0,0,0,202,0,0,85,3,255,254,0,0,0,0,202,0,0,86,3,23,3,22,3,194,0
	.byte 3,200,3,83,3,79,3,77,7,13,109,111,110,111,95,108,100,118,105,114,116,102,110,0,3,255,254,0,0,0,0,202
	.byte 0,0,96,3,195,0,4,185,3,78,3,255,254,0,0,0,0,202,0,0,101,3,255,254,0,0,0,0,202,0,0,102
	.byte 3,255,254,0,0,0,0,202,0,0,103,3,255,254,0,0,0,0,202,0,0,104,3,255,254,0,0,0,0,202,0,0
	.byte 105,3,255,254,0,0,0,0,202,0,0,106,3,195,0,7,29,3,93,3,195,0,7,30,3,129,33,3,195,0,7,35
	.byte 3,255,254,0,0,0,0,202,0,0,111,3,255,254,0,0,0,0,255,43,0,0,7,3,193,0,16,248,3,195,0,7
	.byte 160,3,195,0,7,161,3,197,0,1,13,3,120,3,193,0,16,63,3,255,254,0,0,0,0,255,43,0,0,8,3,193
	.byte 0,16,65,3,195,0,5,9,3,195,0,2,20,3,255,254,0,0,0,0,202,0,0,141,3,197,0,1,222,3,197,0
	.byte 1,12,3,194,0,3,196,3,194,0,3,198,3,194,0,3,197,3,107,3,108,3,128,159,3,128,160,3,195,0,0,219
	.byte 3,128,168,3,128,167,3,128,145,3,128,170,3,128,144,3,255,254,0,0,0,0,255,43,0,0,9,3,195,0,5,96
	.byte 3,129,0,3,195,0,1,12,3,128,163,3,128,178,3,128,166,3,255,254,0,0,0,0,202,0,0,174,3,128,188,3
	.byte 195,0,4,129,3,194,0,3,199,3,195,0,4,130,3,255,254,0,0,0,0,202,0,0,183,3,255,254,0,0,0,0
	.byte 202,0,0,185,3,128,211,3,195,0,6,185,3,128,220,3,128,210,3,128,238,3,195,0,5,105,3,194,0,4,30,3
	.byte 194,0,4,31,3,128,243,3,195,0,5,106,3,195,0,5,108,3,195,0,5,115,3,195,0,5,116,3,195,0,5,119
	.byte 3,128,253,3,129,1,3,128,244,3,128,245,3,128,246,3,193,0,12,215,3,195,0,5,118,3,128,249,3,195,0,6
	.byte 212,3,195,0,6,213,3,194,0,4,29,3,129,18,3,195,0,6,216,7,23,109,111,110,111,95,111,98,106,101,99,116
	.byte 95,110,101,119,95,112,116,114,102,114,101,101,0,3,198,0,0,4,3,195,0,4,243,3,195,0,0,91,3,195,0,5
	.byte 81,3,255,254,0,0,0,0,202,0,0,225,3,195,0,5,202,3,195,0,5,204,3,195,0,6,0,3,129,34,7,35
	.byte 109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111
	.byte 105,110,116,0,3,195,0,7,31,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112
	.byte 116,105,111,110,0,7,36,109,111,110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108
	.byte 101,95,101,120,99,101,112,116,105,111,110,0,3,197,0,0,80,3,195,0,5,107,3,195,0,6,214,3,129,20,3,129
	.byte 21,3,195,0,5,205,255,253,0,0,0,7,133,84,0,198,0,0,80,1,7,133,79,0,35,143,102,192,0,92,40,255
	.byte 253,0,0,0,7,133,84,0,198,0,0,80,1,7,133,79,0,0,255,253,0,0,0,7,133,84,0,198,0,0,81,1
	.byte 7,133,79,0,35,143,146,192,0,92,40,255,253,0,0,0,7,133,84,0,198,0,0,81,1,7,133,79,0,0,255,253
	.byte 0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,134,87,4,2,130,11,1,1,7,134,87,35,143,190,150,5,7
	.byte 143,209,3,255,253,0,0,0,7,143,209,1,198,0,13,121,1,7,134,87,0,35,143,190,192,0,92,41,255,253,0,0
	.byte 0,2,130,10,1,1,198,0,13,43,0,1,7,134,87,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,5,19,0,1,0,1
	.byte 18,17,0,0,17,255,253,0,0,0,1,18,0,198,0,0,80,1,7,144,250,0,0,0,17,0,0,17,255,253,0,0
	.byte 0,1,18,0,198,0,0,81,1,7,144,250,0,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,24,1,2,0,76,24,52,56,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,48,1,0,8,4,2
	.byte 130,64,1,40,68,68,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,6,48,1,0,8,4,2,130,64,1,40,68,68,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,0,0
	.byte 2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,2,0,0,2,0,0,2,0,0,16,0,0,2,0,0,2,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,3,72,0,1,11,8,18,255,253,0,0,0
	.byte 7,133,84,0,198,0,0,80,1,7,133,79,0,1,1,1,0,0,3,72,0,1,11,8,18,255,253,0,0,0,7,133
	.byte 84,0,198,0,0,81,1,7,133,79,0,1,1,1,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,3,0,0,1,11,4,19,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,134,87
	.byte 1,0,1,0,0,0,128,144,8,0,0,1,42,128,224,20,48,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193
	.byte 0,18,172,194,0,2,251,4,194,0,2,249,194,0,2,248,194,0,2,247,194,0,2,245,194,0,2,244,194,0,2,243
	.byte 194,0,2,242,194,0,2,241,194,0,2,240,194,0,2,239,194,0,2,238,3,194,0,2,236,194,0,2,235,194,0,2
	.byte 234,194,0,2,233,194,0,2,232,194,0,2,231,194,0,2,230,194,0,2,229,194,0,2,228,194,0,2,227,194,0,2
	.byte 226,194,0,2,225,194,0,3,236,2,194,0,3,246,194,0,3,245,194,0,3,244,194,0,3,243,194,0,3,242,194,0
	.byte 3,241,194,0,3,240,194,0,3,239,194,0,3,238,194,0,3,237,21,128,162,194,0,0,158,28,0,0,4,193,0,18
	.byte 178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,21,194,0,2,203,20,194,0,0,204,194,0,0,198,194
	.byte 0,0,199,194,0,0,194,20,194,0,0,204,194,0,2,203,194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205
	.byte 194,0,2,205,0,20,128,162,194,0,0,158,24,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172
	.byte 194,0,0,159,194,0,0,160,25,24,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194,24,194,0,0,204,25
	.byte 194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,0,21,128,162,194,0,0,158,28,0,0,4,193,0,18,178
	.byte 193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,21,194,0,2,203,20,194,0,0,204,194,0,0,198,194,0
	.byte 0,199,194,0,0,194,20,194,0,0,204,194,0,2,203,194,0,0,207,194,0,0,194,28,194,0,0,205,194,0,2,205
	.byte 27,21,128,162,194,0,0,158,28,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159
	.byte 21,31,20,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194,20,194,0,0,204,31,194,0,0,207,194,0,0
	.byte 194,32,194,0,0,205,194,0,2,205,30,20,128,162,194,0,0,158,24,0,0,4,193,0,18,178,193,0,18,175,194,0
	.byte 0,158,193,0,18,172,194,0,0,159,194,0,0,160,36,35,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194
	.byte 35,194,0,0,204,36,194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,37,20,128,162,194,0,0,158,28,0
	.byte 0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,41,194,0,2,203,40,194,0,0,204
	.byte 194,0,0,198,194,0,0,199,194,0,0,194,40,194,0,0,204,194,0,2,203,194,0,0,207,194,0,0,194,194,0,0
	.byte 206,194,0,0,205,194,0,2,205,17,128,162,194,0,0,158,20,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158
	.byte 193,0,18,172,194,0,0,159,48,47,45,46,194,0,0,198,194,0,0,199,194,0,0,194,45,46,47,194,0,0,195,194
	.byte 0,0,194,20,128,162,194,0,0,158,28,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0
	.byte 0,159,56,54,52,194,0,0,204,194,0,0,198,194,0,0,199,53,52,194,0,0,204,54,194,0,0,207,53,194,0,0
	.byte 206,194,0,0,205,55,20,128,162,194,0,0,158,28,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18
	.byte 172,194,0,0,159,60,194,0,2,203,59,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194,59,194,0,0,204
	.byte 194,0,2,203,194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,194,0,2,205,20,128,162,194,0,0,158,24
	.byte 0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,194,0,0,160,25,24,194,0,0
	.byte 204,194,0,0,198,194,0,0,199,194,0,0,194,24,194,0,0,204,25,194,0,0,207,194,0,0,194,194,0,0,206,194
	.byte 0,0,205,62,20,128,162,194,0,0,158,28,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194
	.byte 0,0,159,194,0,0,160,65,64,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194,64,194,0,0,204,65,194
	.byte 0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,66,20,128,162,194,0,0,158,24,0,0,4,193,0,18,178,193
	.byte 0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,194,0,0,160,70,69,194,0,0,204,194,0,0,198,194,0,0
	.byte 199,194,0,0,194,69,194,0,0,204,70,194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,71,20,128,162,194
	.byte 0,0,158,24,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,194,0,0,160,25
	.byte 24,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0,194,24,194,0,0,204,25,194,0,0,207,194,0,0,194,194
	.byte 0,0,206,194,0,0,205,73,4,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4
	.byte 128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,255,255,255,255,255,5,128,160,12,0
	.byte 0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,82,60,128,162,195,0,2,8,56,0,0,4,195,0
	.byte 2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,94,195,0,2,13,195,0,2,12,195
	.byte 0,2,7,195,0,7,28,195,0,5,28,195,0,5,27,195,0,5,26,195,0,5,235,195,0,5,217,195,0,5,218,195
	.byte 0,5,208,195,0,5,219,195,0,5,220,195,0,5,255,195,0,5,251,195,0,5,250,195,0,5,249,195,0,5,248,195
	.byte 0,5,247,195,0,5,246,195,0,5,245,195,0,5,238,195,0,5,237,195,0,5,233,195,0,5,232,195,0,5,231,195
	.byte 0,5,230,195,0,5,229,195,0,5,228,195,0,5,227,195,0,5,226,195,0,5,225,195,0,5,224,195,0,5,223,195
	.byte 0,5,222,195,0,5,221,195,0,5,220,195,0,5,219,195,0,5,218,195,0,5,217,195,0,5,216,195,0,5,215,195
	.byte 0,5,214,195,0,5,213,195,0,5,212,195,0,5,211,195,0,5,210,195,0,5,209,195,0,5,208,195,0,5,207,195
	.byte 0,5,206,195,0,7,34,195,0,7,33,6,128,162,103,16,0,0,4,193,0,18,178,193,0,18,175,103,193,0,18,172
	.byte 104,106,42,128,162,195,0,2,8,40,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2
	.byte 4,195,0,2,9,195,0,2,20,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,195,0,7,189,195,0,7
	.byte 188,195,0,7,187,195,0,7,186,195,0,7,185,195,0,7,184,195,0,7,183,195,0,7,182,195,0,7,181,195,0,7
	.byte 180,195,0,7,179,195,0,7,178,195,0,7,177,124,195,0,7,175,118,114,195,0,7,172,195,0,7,171,195,0,7,170
	.byte 195,0,7,169,195,0,7,168,195,0,7,167,195,0,7,166,195,0,7,165,125,123,0,0,0,115,23,128,162,195,0,2
	.byte 8,44,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,127,195,0
	.byte 2,13,195,0,2,12,195,0,2,7,195,0,2,6,128,134,128,136,195,0,5,13,195,0,5,12,128,133,128,132,128,143
	.byte 128,135,128,131,128,130,128,129,128,128,45,128,162,195,0,2,8,64,0,0,4,195,0,2,18,193,0,18,175,195,0,2
	.byte 8,193,0,18,172,195,0,2,4,195,0,2,9,128,146,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,195
	.byte 0,7,189,195,0,7,188,195,0,7,187,195,0,7,186,195,0,7,185,195,0,7,184,195,0,7,183,195,0,7,182,195
	.byte 0,7,181,195,0,7,180,195,0,7,179,195,0,7,178,195,0,7,177,124,195,0,7,175,118,114,195,0,7,172,195,0
	.byte 7,171,195,0,7,170,195,0,7,169,195,0,7,168,195,0,7,167,195,0,7,166,195,0,7,165,125,123,128,161,128,149
	.byte 0,115,128,158,128,148,128,147,47,128,230,128,173,195,0,2,8,80,8,0,4,195,0,2,18,193,0,18,175,195,0,2
	.byte 8,193,0,18,172,195,0,2,4,195,0,2,9,128,146,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,195
	.byte 0,7,189,195,0,7,188,195,0,7,187,195,0,7,186,195,0,7,185,195,0,7,184,195,0,7,183,195,0,7,182,195
	.byte 0,7,181,195,0,7,180,195,0,7,179,195,0,7,178,195,0,7,177,124,195,0,7,175,118,114,195,0,7,172,195,0
	.byte 7,171,195,0,7,170,195,0,7,169,195,0,7,168,195,0,7,167,195,0,7,166,195,0,7,165,125,123,128,161,128,149
	.byte 128,171,115,128,158,128,148,128,147,128,172,128,162,47,128,226,195,0,2,8,92,4,0,4,195,0,2,18,193,0,18,175
	.byte 195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,128,146,195,0,2,13,195,0,2,12,195,0,2,7,195,0
	.byte 2,6,195,0,7,189,195,0,7,188,195,0,7,187,195,0,7,186,195,0,7,185,195,0,7,184,195,0,7,183,195,0
	.byte 7,182,195,0,7,181,195,0,7,180,195,0,7,179,195,0,7,178,195,0,7,177,124,195,0,7,175,118,114,195,0,7
	.byte 172,195,0,7,171,195,0,7,170,195,0,7,169,195,0,7,168,195,0,7,167,195,0,7,166,195,0,7,165,125,123,128
	.byte 161,128,149,128,186,115,128,158,128,148,128,147,128,172,128,185,21,128,162,194,0,0,158,28,0,0,4,193,0,18,178,193
	.byte 0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,128,196,128,193,128,191,194,0,0,204,194,0,0,198,194,0,0
	.byte 199,128,192,128,191,194,0,0,204,128,193,194,0,0,207,128,192,194,0,0,206,128,194,128,195,128,197,20,128,162,194,0
	.byte 0,158,32,0,0,4,193,0,18,178,193,0,18,175,194,0,0,158,193,0,18,172,194,0,0,159,128,203,194,0,2,203
	.byte 128,201,194,0,0,204,194,0,0,198,194,0,0,199,128,202,128,201,194,0,0,204,194,0,2,203,194,0,0,207,128,202
	.byte 194,0,0,206,194,0,0,205,128,199,20,128,162,194,0,0,158,24,0,0,4,193,0,18,178,193,0,18,175,194,0,0
	.byte 158,193,0,18,172,194,0,0,159,194,0,0,160,128,207,128,206,194,0,0,204,194,0,0,198,194,0,0,199,194,0,0
	.byte 194,128,206,194,0,0,204,128,207,194,0,0,207,194,0,0,194,194,0,0,206,194,0,0,205,128,208,25,128,230,128,226
	.byte 195,0,2,8,40,4,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9
	.byte 195,0,2,20,195,0,2,13,195,0,2,12,195,0,2,7,195,0,2,6,195,0,6,187,128,223,128,224,128,218,195,0
	.byte 6,192,195,0,6,191,195,0,6,190,128,225,128,223,195,0,6,187,0,128,216,128,215,128,209,27,128,162,195,0,2,8
	.byte 48,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,128,228,195,0
	.byte 2,13,195,0,2,12,195,0,2,7,195,0,2,6,128,234,128,223,128,224,128,218,195,0,6,192,195,0,6,191,195,0
	.byte 6,190,128,225,128,223,128,234,128,232,128,216,128,215,128,209,128,231,128,230,0,128,144,8,0,0,1,72,128,162,195,0
	.byte 2,8,76,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,128,249
	.byte 195,0,2,13,195,0,2,12,195,0,2,7,195,0,5,104,195,0,5,28,195,0,5,27,195,0,5,26,195,0,5,235
	.byte 195,0,5,217,195,0,5,218,195,0,5,208,195,0,5,219,195,0,5,220,195,0,5,255,195,0,5,251,195,0,5,250
	.byte 195,0,5,249,195,0,5,248,195,0,5,247,195,0,5,246,195,0,5,245,195,0,5,238,195,0,5,237,195,0,5,233
	.byte 195,0,5,232,195,0,5,231,195,0,5,230,195,0,5,229,195,0,5,228,195,0,5,227,195,0,5,226,195,0,5,225
	.byte 195,0,5,224,195,0,5,223,195,0,5,222,195,0,5,221,195,0,5,220,195,0,5,219,195,0,5,218,195,0,5,217
	.byte 195,0,5,216,195,0,5,215,195,0,5,214,195,0,5,213,195,0,5,212,195,0,5,211,195,0,5,210,195,0,5,209
	.byte 195,0,5,208,195,0,5,207,195,0,5,206,195,0,5,118,195,0,5,117,128,248,128,247,195,0,5,114,195,0,5,113
	.byte 195,0,5,112,195,0,5,111,195,0,5,110,195,0,5,109,128,235,128,236,128,250,128,251,72,128,162,195,0,2,8,88
	.byte 0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,129,12,195,0,2
	.byte 13,195,0,2,12,195,0,2,7,195,0,5,104,195,0,5,28,195,0,5,27,195,0,5,26,195,0,5,235,195,0,5
	.byte 217,195,0,5,218,195,0,5,208,195,0,5,219,195,0,5,220,195,0,5,255,195,0,5,251,195,0,5,250,195,0,5
	.byte 249,195,0,5,248,195,0,5,247,195,0,5,246,195,0,5,245,195,0,5,238,195,0,5,237,195,0,5,233,195,0,5
	.byte 232,195,0,5,231,195,0,5,230,195,0,5,229,195,0,5,228,195,0,5,227,195,0,5,226,195,0,5,225,195,0,5
	.byte 224,195,0,5,223,195,0,5,222,195,0,5,221,195,0,5,220,195,0,5,219,195,0,5,218,195,0,5,217,195,0,5
	.byte 216,195,0,5,215,195,0,5,214,195,0,5,213,195,0,5,212,195,0,5,211,195,0,5,210,195,0,5,209,195,0,5
	.byte 208,195,0,5,207,195,0,5,206,129,11,195,0,5,117,128,248,128,247,195,0,5,114,195,0,5,113,195,0,5,112,195
	.byte 0,5,111,195,0,5,110,195,0,5,109,128,235,128,236,128,250,128,251,63,128,162,195,0,2,8,56,0,0,4,195,0
	.byte 2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,129,25,195,0,2,13,195,0,2,12
	.byte 195,0,2,7,195,0,6,211,195,0,5,28,195,0,5,27,195,0,5,26,195,0,5,235,195,0,5,217,195,0,5,218
	.byte 195,0,5,208,195,0,5,219,195,0,5,220,195,0,5,255,195,0,5,251,195,0,5,250,195,0,5,249,195,0,5,248
	.byte 195,0,5,247,195,0,5,246,195,0,5,245,195,0,5,238,195,0,5,237,195,0,5,233,195,0,5,232,195,0,5,231
	.byte 195,0,5,230,195,0,5,229,195,0,5,228,195,0,5,227,195,0,5,226,195,0,5,225,195,0,5,224,195,0,5,223
	.byte 195,0,5,222,195,0,5,221,195,0,5,220,195,0,5,219,195,0,5,218,195,0,5,217,195,0,5,216,195,0,5,215
	.byte 195,0,5,214,195,0,5,213,195,0,5,212,195,0,5,211,195,0,5,210,195,0,5,209,195,0,5,208,195,0,5,207
	.byte 195,0,5,206,195,0,6,215,129,14,129,15,129,26,129,27,46,128,162,195,0,2,8,72,0,0,4,195,0,2,18,193
	.byte 0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,128,146,195,0,2,13,195,0,2,12,195,0,2
	.byte 7,195,0,2,6,195,0,7,189,195,0,7,188,195,0,7,187,195,0,7,186,195,0,7,185,195,0,7,184,195,0,7
	.byte 183,195,0,7,182,195,0,7,181,195,0,7,180,195,0,7,179,195,0,7,178,195,0,7,177,124,195,0,7,175,118,114
	.byte 195,0,7,172,195,0,7,171,195,0,7,170,195,0,7,169,195,0,7,168,195,0,7,167,195,0,7,166,195,0,7,165
	.byte 125,123,128,161,128,149,129,32,115,128,158,128,148,128,147,129,28,255,255,255,255,255,62,128,162,195,0,2,8,52,0,0
	.byte 4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,129,40,195,0,2,13,195
	.byte 0,2,12,195,0,2,7,195,0,5,201,195,0,5,28,195,0,5,27,195,0,5,26,195,0,5,235,195,0,5,217,195
	.byte 0,5,218,195,0,5,208,195,0,5,219,195,0,5,220,195,0,5,255,195,0,5,251,195,0,5,250,195,0,5,249,195
	.byte 0,5,248,195,0,5,247,195,0,5,246,195,0,5,245,195,0,5,238,195,0,5,237,195,0,5,233,195,0,5,232,195
	.byte 0,5,231,195,0,5,230,195,0,5,229,195,0,5,228,195,0,5,227,195,0,5,226,195,0,5,225,195,0,5,224,195
	.byte 0,5,223,195,0,5,222,195,0,5,221,195,0,5,220,195,0,5,219,195,0,5,218,195,0,5,217,195,0,5,216,195
	.byte 0,5,215,195,0,5,214,195,0,5,213,195,0,5,212,195,0,5,211,195,0,5,210,195,0,5,209,195,0,5,208,195
	.byte 0,5,207,195,0,5,206,129,35,129,36,129,41,129,42,4,128,160,16,0,0,4,193,0,18,178,193,0,18,175,193,0
	.byte 18,174,193,0,18,172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_5:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_7:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_6:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM10=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM12=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_4:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM15=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM16=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM17=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM18=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM19=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM20=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM21=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_3:

	.byte 5
	.asciz "MonoTouch_UIKit_UIResponder"

	.byte 24,16
LDIFF_SYM24=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,0,6
	.asciz "__mt_InputAccessoryView_var"

LDIFF_SYM25=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,20,0,7
	.asciz "MonoTouch_UIKit_UIResponder"

LDIFF_SYM26=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM27=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM28=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_2:

	.byte 5
	.asciz "MonoTouch_UIKit_UIView"

	.byte 48,16
LDIFF_SYM29=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,0,6
	.asciz "__mt_BackgroundColor_var"

LDIFF_SYM30=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,24,6
	.asciz "__mt_Layer_var"

LDIFF_SYM31=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,28,6
	.asciz "__mt_Superview_var"

LDIFF_SYM32=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,32,6
	.asciz "__mt_Subviews_var"

LDIFF_SYM33=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,36,6
	.asciz "__mt_GestureRecognizers_var"

LDIFF_SYM34=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,40,6
	.asciz "__mt_TintColor_var"

LDIFF_SYM35=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,44,0,7
	.asciz "MonoTouch_UIKit_UIView"

LDIFF_SYM36=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM37=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM38=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_1:

	.byte 5
	.asciz "MonoTouch_UIKit_UIImageView"

	.byte 52,16
LDIFF_SYM39=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,0,6
	.asciz "__mt_Image_var"

LDIFF_SYM40=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIImageView"

LDIFF_SYM41=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM42=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM43=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_10:

	.byte 17
	.asciz "Cirrious_CrossCore_Platform_IMvxImageHelper`1"

	.byte 8,7
	.asciz "Cirrious_CrossCore_Platform_IMvxImageHelper`1"

LDIFF_SYM44=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM44
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM45=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM45
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM46=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM46
LTDIE_16:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM47=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM48=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM49=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM50=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_15:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM51=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM52=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM53=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM54=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_14:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM55=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM55
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM56=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM57=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM58=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_18:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM59=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM59
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM60=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM61=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM62=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM63=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_17:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM64=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM65=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM66=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM67=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM68=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM69=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_13:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM70=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM71=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM72=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM73=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM74=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM75=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM76=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM77=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM78=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM79=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM80=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM81=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM82=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_12:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM83=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM84=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM85=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM86=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM86
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM87=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM88=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_11:

	.byte 5
	.asciz "System_Action`1"

	.byte 52,16
LDIFF_SYM89=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM89
	.byte 2,35,0,0,7
	.asciz "System_Action`1"

LDIFF_SYM90=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM91=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM91
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM92=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_19:

	.byte 17
	.asciz "System_IDisposable"

	.byte 8,7
	.asciz "System_IDisposable"

LDIFF_SYM93=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM94=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM95=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_9:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader`1"

	.byte 20,16
LDIFF_SYM96=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,0,6
	.asciz "_imageHelper"

LDIFF_SYM97=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,8,6
	.asciz "_imageSetAction"

LDIFF_SYM98=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,12,6
	.asciz "_subscription"

LDIFF_SYM99=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_Binding_Views_MvxBaseImageViewLoader`1"

LDIFF_SYM100=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_8:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader"

	.byte 20,16
LDIFF_SYM103=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewLoader"

LDIFF_SYM104=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM104
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM105=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM105
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM106=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM106
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView"

	.byte 56,16
LDIFF_SYM107=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 2,35,0,6
	.asciz "_imageHelper"

LDIFF_SYM108=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2,35,52,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView"

LDIFF_SYM109=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM110=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM110
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM111=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM111
LTDIE_20:

	.byte 5
	.asciz "System_Action"

	.byte 52,16
LDIFF_SYM112=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,35,0,0,7
	.asciz "System_Action"

LDIFF_SYM113=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM114=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM115=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxImageView:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action
	.long Lme_5b

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM116=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM116
	.byte 2,123,8,3
	.asciz "frame"

LDIFF_SYM117=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 2,123,12,3
	.asciz "afterImageChangeAction"

LDIFF_SYM118=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM119=Lfde0_end - Lfde0_start
	.long LDIFF_SYM119
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action

LDIFF_SYM120=Lme_5b - _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageView__ctor_System_Drawing_RectangleF_System_Action
	.long LDIFF_SYM120
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_22:

	.byte 5
	.asciz "System_Func`1"

	.byte 52,16
LDIFF_SYM121=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 2,35,0,0,7
	.asciz "System_Func`1"

LDIFF_SYM122=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM123=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM123
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM124=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_21:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper"

	.byte 16,16
LDIFF_SYM125=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,35,0,6
	.asciz "_imageView"

LDIFF_SYM126=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM126
	.byte 2,35,8,6
	.asciz "_imageHelper"

LDIFF_SYM127=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM127
	.byte 2,35,12,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper"

LDIFF_SYM128=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM129=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM130=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxImageViewWrapper:Finalize"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0
	.long Lme_66

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM131=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM132=Lfde1_end - Lfde1_start
	.long LDIFF_SYM132
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0

LDIFF_SYM133=Lme_66 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxImageViewWrapper_Finalize_0
	.long LDIFF_SYM133
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_25:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

	.byte 20,16
LDIFF_SYM134=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UIScrollViewDelegate"

LDIFF_SYM135=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM136=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM137=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_24:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewSource"

	.byte 20,16
LDIFF_SYM138=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UITableViewSource"

LDIFF_SYM139=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM140=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM141=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_27:

	.byte 5
	.asciz "MonoTouch_UIKit_UIScrollView"

	.byte 52,16
LDIFF_SYM142=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM142
	.byte 2,35,0,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM143=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM143
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UIScrollView"

LDIFF_SYM144=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM145=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM146=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_26:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableView"

	.byte 68,16
LDIFF_SYM147=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM147
	.byte 2,35,0,6
	.asciz "__mt_WeakDataSource_var"

LDIFF_SYM148=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM148
	.byte 2,35,52,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM149=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,56,6
	.asciz "__mt_TableHeaderView_var"

LDIFF_SYM150=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM151=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UITableView"

LDIFF_SYM152=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM153=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM153
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM154=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_28:

	.byte 5
	.asciz "System_EventHandler"

	.byte 52,16
LDIFF_SYM155=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,0,0,7
	.asciz "System_EventHandler"

LDIFF_SYM156=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM157=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM157
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM158=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_29:

	.byte 17
	.asciz "System_Windows_Input_ICommand"

	.byte 8,7
	.asciz "System_Windows_Input_ICommand"

LDIFF_SYM159=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM160=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM160
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM161=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_23:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource"

	.byte 40,16
LDIFF_SYM162=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 2,35,0,6
	.asciz "_tableView"

LDIFF_SYM163=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 2,35,20,6
	.asciz "_selectedItem"

LDIFF_SYM164=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,24,6
	.asciz "SelectedItemChanged"

LDIFF_SYM165=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 2,35,28,6
	.asciz "<SelectionChangedCommand>k__BackingField"

LDIFF_SYM166=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,32,6
	.asciz "<AccessoryTappedCommand>k__BackingField"

LDIFF_SYM167=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 2,35,36,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource"

LDIFF_SYM168=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM169=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM170=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM170
LTDIE_31:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM171=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM172=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM173=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM174=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM175=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_32:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM176=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM176
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM177=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM178=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM178
LTDIE_30:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM179=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM180=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM181=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM182=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM183=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM184=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM185=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM186=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM187=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM188=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM189=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM190=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM191=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM191
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM192=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM193=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM194=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM195=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM195
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxBaseTableViewSource:ReloadTableData"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0
	.long Lme_72

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM196=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,123,28,11
	.asciz "V_0"

LDIFF_SYM197=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM198=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM198
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM199=Lfde2_end - Lfde2_start
	.long LDIFF_SYM199
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0

LDIFF_SYM200=Lme_72 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseTableViewSource_ReloadTableData_0
	.long LDIFF_SYM200
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_34:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewSource"

	.byte 20,16
LDIFF_SYM201=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewSource"

LDIFF_SYM202=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM203=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM203
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM204=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM204
LTDIE_35:

	.byte 5
	.asciz "MonoTouch_Foundation_NSString"

	.byte 20,16
LDIFF_SYM205=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,0,0,7
	.asciz "MonoTouch_Foundation_NSString"

LDIFF_SYM206=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM206
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM207=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM207
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM208=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_36:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionView"

	.byte 68,16
LDIFF_SYM209=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 2,35,0,6
	.asciz "__mt_CollectionViewLayout_var"

LDIFF_SYM210=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2,35,52,6
	.asciz "__mt_WeakDelegate_var"

LDIFF_SYM211=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM211
	.byte 2,35,56,6
	.asciz "__mt_WeakDataSource_var"

LDIFF_SYM212=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM213=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,35,64,0,7
	.asciz "MonoTouch_UIKit_UICollectionView"

LDIFF_SYM214=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM214
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM215=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM215
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM216=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM216
LTDIE_33:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource"

	.byte 40,16
LDIFF_SYM217=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM217
	.byte 2,35,0,6
	.asciz "_cellIdentifier"

LDIFF_SYM218=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM218
	.byte 2,35,20,6
	.asciz "_collectionView"

LDIFF_SYM219=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM219
	.byte 2,35,24,6
	.asciz "_selectedItem"

LDIFF_SYM220=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,28,6
	.asciz "SelectedItemChanged"

LDIFF_SYM221=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,32,6
	.asciz "<SelectionChangedCommand>k__BackingField"

LDIFF_SYM222=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,36,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource"

LDIFF_SYM223=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM224=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM225=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxBaseCollectionViewSource:ReloadData"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0
	.long Lme_d6

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM226=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,123,28,11
	.asciz "V_0"

LDIFF_SYM227=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM228=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM229=Lfde3_end - Lfde3_start
	.long LDIFF_SYM229
Lfde3_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0

LDIFF_SYM230=Lme_d6 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxBaseCollectionViewSource_ReloadData_0
	.long LDIFF_SYM230
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_38:

	.byte 5
	.asciz "MonoTouch_UIKit_UITableViewCell"

	.byte 72,16
LDIFF_SYM231=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,0,6
	.asciz "__mt_ImageView_var"

LDIFF_SYM232=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,48,6
	.asciz "__mt_TextLabel_var"

LDIFF_SYM233=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,52,6
	.asciz "__mt_DetailTextLabel_var"

LDIFF_SYM234=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,56,6
	.asciz "__mt_ContentView_var"

LDIFF_SYM235=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,60,6
	.asciz "__mt_BackgroundView_var"

LDIFF_SYM236=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,64,6
	.asciz "__mt_AccessoryView_var"

LDIFF_SYM237=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM237
	.byte 2,35,68,0,7
	.asciz "MonoTouch_UIKit_UITableViewCell"

LDIFF_SYM238=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM238
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM239=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM240=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_39:

	.byte 17
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext"

LDIFF_SYM241=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM242=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM243=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM243
LTDIE_37:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell"

	.byte 76,16
LDIFF_SYM244=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,0,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM245=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,72,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell"

LDIFF_SYM246=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM247=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM247
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM248=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxTableViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF
	.long Lme_ef

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM249=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,123,8,3
	.asciz "bindingText"

LDIFF_SYM250=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 2,123,12,3
	.asciz "frame"

LDIFF_SYM251=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM252=Lfde4_end - Lfde4_start
	.long LDIFF_SYM252
Lfde4_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF

LDIFF_SYM253=Lme_ef - _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_string_System_Drawing_RectangleF
	.long LDIFF_SYM253
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_40:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM254=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM255=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM255
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM256=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxTableViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	.long Lme_f0

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM257=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,123,8,3
	.asciz "bindingDescriptions"

LDIFF_SYM258=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,123,12,3
	.asciz "frame"

LDIFF_SYM259=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM260=Lfde5_end - Lfde5_start
	.long LDIFF_SYM260
Lfde5_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF

LDIFF_SYM261=Lme_f0 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxTableViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	.long LDIFF_SYM261
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_43:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionReusableView"

	.byte 48,16
LDIFF_SYM262=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM262
	.byte 2,35,0,0,7
	.asciz "MonoTouch_UIKit_UICollectionReusableView"

LDIFF_SYM263=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM264=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM265=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM265
LTDIE_42:

	.byte 5
	.asciz "MonoTouch_UIKit_UICollectionViewCell"

	.byte 52,16
LDIFF_SYM266=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM266
	.byte 2,35,0,6
	.asciz "__mt_ContentView_var"

LDIFF_SYM267=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM267
	.byte 2,35,48,0,7
	.asciz "MonoTouch_UIKit_UICollectionViewCell"

LDIFF_SYM268=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM269=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM269
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM270=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM270
LTDIE_41:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell"

	.byte 56,16
LDIFF_SYM271=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,0,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM272=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,52,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell"

LDIFF_SYM273=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM273
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM274=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM275=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM275
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxCollectionViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF
	.long Lme_112

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM276=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,123,8,3
	.asciz "frame"

LDIFF_SYM277=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM278=Lfde6_end - Lfde6_start
	.long LDIFF_SYM278
Lfde6_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF

LDIFF_SYM279=Lme_112 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF
	.long LDIFF_SYM279
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde6_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxCollectionViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF
	.long Lme_113

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM280=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM280
	.byte 2,123,8,3
	.asciz "bindingText"

LDIFF_SYM281=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,123,12,3
	.asciz "frame"

LDIFF_SYM282=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM283=Lfde7_end - Lfde7_start
	.long LDIFF_SYM283
Lfde7_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF

LDIFF_SYM284=Lme_113 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_string_System_Drawing_RectangleF
	.long LDIFF_SYM284
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde7_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxCollectionViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	.long Lme_114

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM285=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,123,8,3
	.asciz "bindingDescriptions"

LDIFF_SYM286=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,123,12,3
	.asciz "frame"

LDIFF_SYM287=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM288=Lfde8_end - Lfde8_start
	.long LDIFF_SYM288
Lfde8_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF

LDIFF_SYM289=Lme_114 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription_System_Drawing_RectangleF
	.long LDIFF_SYM289
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde8_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxCollectionViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string
	.long Lme_116

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM290=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,123,8,3
	.asciz "frame"

LDIFF_SYM291=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM291
	.byte 2,123,12,3
	.asciz "bindingText"

LDIFF_SYM292=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM293=Lfde9_end - Lfde9_start
	.long LDIFF_SYM293
Lfde9_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string

LDIFF_SYM294=Lme_116 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_string
	.long LDIFF_SYM294
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde9_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxCollectionViewCell:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.long Lme_117

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM295=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,123,8,3
	.asciz "frame"

LDIFF_SYM296=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,123,12,3
	.asciz "bindingDescriptions"

LDIFF_SYM297=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM298=Lfde10_end - Lfde10_start
	.long LDIFF_SYM298
Lfde10_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription

LDIFF_SYM299=Lme_117 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxCollectionViewCell__ctor_System_Drawing_RectangleF_System_Collections_Generic_IEnumerable_1_Cirrious_MvvmCross_Binding_Bindings_MvxBindingDescription
	.long LDIFF_SYM299
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde10_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_44:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxView"

	.byte 52,16
LDIFF_SYM300=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,0,6
	.asciz "<BindingContext>k__BackingField"

LDIFF_SYM301=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,48,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_MvxView"

LDIFF_SYM302=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM302
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM303=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM303
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM304=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.MvxView:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF
	.long Lme_126

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM305=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,123,8,3
	.asciz "frame"

LDIFF_SYM306=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM307=Lfde11_end - Lfde11_start
	.long LDIFF_SYM307
Lfde11_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF

LDIFF_SYM308=Lme_126 - _Cirrious_MvvmCross_Binding_Touch_Views_MvxView__ctor_System_Drawing_RectangleF
	.long LDIFF_SYM308
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde11_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_46:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior"

	.byte 12,16
LDIFF_SYM309=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 2,35,0,6
	.asciz "<Command>k__BackingField"

LDIFF_SYM310=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior"

LDIFF_SYM311=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM311
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM312=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM312
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM313=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM313
LTDIE_45:

	.byte 5
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior`1"

	.byte 12,16
LDIFF_SYM314=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior`1"

LDIFF_SYM315=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM315
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM316=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM316
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM317=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM317
	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.Gestures.MvxGestureRecognizerBehavior`1<!0>:HandleGesture"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0
	.long Lme_12d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM318=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM318
	.byte 2,123,8,3
	.asciz "gesture"

LDIFF_SYM319=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM319
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM320=Lfde12_end - Lfde12_start
	.long LDIFF_SYM320
Lfde12_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0

LDIFF_SYM321=Lme_12d - _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0_HandleGesture__0
	.long LDIFF_SYM321
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,32,68,13,11
	.align 2
Lfde12_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Binding.Touch.Views.Gestures.MvxGestureRecognizerBehavior`1<!0>:.ctor"
	.long _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor
	.long Lme_12e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM322=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM323=Lfde13_end - Lfde13_start
	.long LDIFF_SYM323
Lfde13_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor

LDIFF_SYM324=Lme_12e - _Cirrious_MvvmCross_Binding_Touch_Views_Gestures_MvxGestureRecognizerBehavior_1__0__ctor
	.long LDIFF_SYM324
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,32,68,13,11
	.align 2
Lfde13_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_47:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM325=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM326=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM327=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM327
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM328=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_141

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM329=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM330=Lfde14_end - Lfde14_start
	.long LDIFF_SYM330
Lfde14_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM331=Lme_141 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM331
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde14_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Array.cs"

	.byte 1,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,2,1,3,207,0,2,32,1,2,252,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
