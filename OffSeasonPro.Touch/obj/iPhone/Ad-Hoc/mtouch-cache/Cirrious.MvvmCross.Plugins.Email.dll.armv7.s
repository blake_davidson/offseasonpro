	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string:
Leh_func_begin1:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r10, r11}
Ltmp2:
	sub	sp, sp, #12
	mov	r4, r0
	mov	r0, #0
	str	r3, [sp, #4]
	mov	r11, r2
	mov	r6, r1
	str	r0, [sp, #8]
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC1_0+8))
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC1_0+8))
LPC1_0:
	add	r10, pc, r10
	ldr	r0, [r10, #16]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_2_plt_System_Text_StringBuilder__ctor_llvm
	ldr	r0, [r10, #20]
	mov	r1, r6
	bl	_p_3_plt_string_Concat_string_string_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_4_plt_System_Text_StringBuilder_Append_string_llvm
	ldr	r0, [r10, #24]
	ldr	r2, [r10, #28]
	add	r6, sp, #8
	mov	r1, r5
	mov	r3, r11
	str	r6, [sp]
	str	r0, [sp, #8]
	mov	r0, r4
	bl	_p_5_plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string__llvm
	ldr	r2, [r10, #32]
	ldr	r3, [sp, #4]
	mov	r0, r4
	mov	r1, r5
	str	r6, [sp]
	bl	_p_5_plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string__llvm
	ldr	r2, [r10, #36]
	ldr	r3, [r7, #8]
	mov	r0, r4
	mov	r1, r5
	str	r6, [sp]
	bl	_p_5_plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string__llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #32]
	mov	r0, r5
	blx	r1
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
	push	{r10, r11}
Ltmp5:
	mov	r5, r3
	mov	r10, r2
	mov	r4, r1
	mov	r0, r5
	bl	_p_6_plt_string_IsNullOrWhiteSpace_string_llvm
	tst	r0, #255
	bne	LBB2_2
	ldr	r11, [r7, #8]
	ldr	r1, [r11]
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_4_plt_System_Text_StringBuilder_Append_string_llvm
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC2_0+8))
	mov	r1, r10
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC2_0+8))
LPC2_0:
	add	r6, pc, r6
	ldr	r0, [r6, #40]
	str	r0, [r11]
	mov	r0, r4
	bl	_p_4_plt_System_Text_StringBuilder_Append_string_llvm
	ldr	r1, [r6, #44]
	mov	r0, r4
	bl	_p_4_plt_System_Text_StringBuilder_Append_string_llvm
	mov	r0, r5
	bl	_p_7_plt_System_Uri_EscapeDataString_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_4_plt_System_Text_StringBuilder_Append_string_llvm
LBB2_2:
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder__ctor:
Leh_func_begin3:
	bx	lr
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader_EnsureLoaded
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader_EnsureLoaded:
Leh_func_begin4:
	push	{r4, r7, lr}
Ltmp6:
	add	r7, sp, #4
Ltmp7:
	push	{r8}
Ltmp8:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC4_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC4_0+8))
LPC4_0:
	add	r4, pc, r4
	ldr	r0, [r4, #48]
	mov	r8, r0
	bl	_p_8_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm
	ldr	r1, [r4, #56]
	ldr	r2, [r0]
	sub	r2, r2, #40
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__ctor:
Leh_func_begin5:
	bx	lr
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__cctor:
Leh_func_begin6:
	push	{r4, r7, lr}
Ltmp9:
	add	r7, sp, #4
Ltmp10:
Ltmp11:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC6_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_got-(LPC6_0+8))
LPC6_0:
	add	r4, pc, r4
	ldr	r0, [r4, #60]
	bl	_p_9_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #64]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end6:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_Email_got,108,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader_EnsureLoaded
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__cctor
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_Email_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	7
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	1
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	2
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	3
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	4
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	5
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	6
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
Lset7 = Leh_func_end6-Leh_func_begin6
	.long	Lset7
Lset8 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset8
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.Email.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader_EnsureLoaded
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__cctor

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder__ctor
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader_EnsureLoaded
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_PluginLoader__cctor
	bl method_addresses
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 0,1,8,4,2,6,3,255,255,255,255,232
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 0
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 2
	.short 11, 0, 0, 3, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 4
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 17,10,2,2
	.short 0, 10
	.byte 29,2,1,1,1,5,3,3,3,3,54,3,3,12,12,12,3
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 0,128,191,3,3,3,3,3,255,255,255,255,50
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 4,10,1,2
	.short 0
	.byte 128,209,7,7,23

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_Email_plt:
_p_1_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 68,103
_p_2_plt_System_Text_StringBuilder__ctor_llvm:
	.no_dead_strip plt_System_Text_StringBuilder__ctor
plt_System_Text_StringBuilder__ctor:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 72,126
_p_3_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 76,131
_p_4_plt_System_Text_StringBuilder_Append_string_llvm:
	.no_dead_strip plt_System_Text_StringBuilder_Append_string
plt_System_Text_StringBuilder_Append_string:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 80,136
_p_5_plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string__llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_
plt_Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_AddParam_System_Text_StringBuilder_string_string_string_:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 84,141
_p_6_plt_string_IsNullOrWhiteSpace_string_llvm:
	.no_dead_strip plt_string_IsNullOrWhiteSpace_string
plt_string_IsNullOrWhiteSpace_string:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 88,143
_p_7_plt_System_Uri_EscapeDataString_string_llvm:
	.no_dead_strip plt_System_Uri_EscapeDataString_string
plt_System_Uri_EscapeDataString_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 92,148
_p_8_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 96,153
_p_9_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got - . + 100,165
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 4
	.asciz "Cirrious.MvvmCross.Plugins.Email"
	.asciz "AD05484A-5DFB-485E-A15B-BFD17FF0C58D"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "AD05484A-5DFB-485E-A15B-BFD17FF0C58D"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.Email"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_Email__Cirrious_MvvmCross_Plugins_Email_MvxMailToUrlBuilder_Build_string_string_string_string
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 17,108,10,8,11,387000831,0,271
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_Email_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_Email_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,6,4,9,8,7,6,5,0,2,11,10,0,0,1,4,3,14,13,12,1,4,0,1,4,2,16,15,12,0,39
	.byte 42,47,14,2,129,210,1,17,0,1,17,0,17,17,0,21,17,0,27,17,0,43,17,0,53,17,0,57,34,255,254,0
	.byte 0,0,0,255,43,0,0,1,34,255,254,0,0,0,0,255,43,0,0,2,6,255,254,0,0,0,0,255,43,0,0,2
	.byte 14,1,4,16,1,4,1,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,193
	.byte 0,11,190,3,193,0,19,133,3,193,0,11,205,3,3,3,193,0,19,167,3,195,0,9,196,3,255,254,0,0,0,0
	.byte 255,43,0,0,1,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,0,128,144,8,0,0,1,4
	.byte 128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,5,128,196,7,8,4,0,1,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,5,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
