	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor:
Leh_func_begin1:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
Ltmp2:
	mov	r4, r0
	bl	_p_1_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance_llvm
	str	r0, [r4, #8]
	pop	{r4, r7, pc}
Leh_func_end1:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Start
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Start:
Leh_func_begin2:
	push	{r7, lr}
Ltmp3:
	mov	r7, sp
Ltmp4:
Ltmp5:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	pop	{r7, pc}
Leh_func_end2:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Stop
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Stop:
Leh_func_begin3:
	push	{r7, lr}
Ltmp6:
	mov	r7, sp
Ltmp7:
Ltmp8:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #76]
	blx	r1
	pop	{r7, pc}
Leh_func_end3:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_NextTrack
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_NextTrack:
Leh_func_begin4:
	push	{r7, lr}
Ltmp9:
	mov	r7, sp
Ltmp10:
Ltmp11:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #84]
	blx	r1
	pop	{r7, pc}
Leh_func_end4:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor:
Leh_func_begin5:
	push	{r4, r7, lr}
Ltmp12:
	add	r7, sp, #4
Ltmp13:
	push	{r10, r11}
Ltmp14:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC5_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC5_0+8))
LPC5_0:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	bl	_p_2_plt__jit_icall_mono_object_new_specific_llvm
	mov	r10, r0
	bl	_p_3_plt_MonoTouch_MediaPlayer_MPMusicPlayerController__ctor_llvm
	ldr	r0, [r10]
	mov	r1, #3
	ldr	r2, [r0, #92]
	mov	r0, r10
	blx	r2
	mov	r11, #0
	mov	r0, r4
	strd	r10, r11, [r4, #8]
	bl	_p_4_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist_llvm
	pop	{r10, r11}
	pop	{r4, r7, pc}
Leh_func_end5:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Player
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Player:
Leh_func_begin6:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end6:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_set_Player_MonoTouch_MediaPlayer_MPMusicPlayerController
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_set_Player_MonoTouch_MediaPlayer_MPMusicPlayerController:
Leh_func_begin7:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end7:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_PlaylistCount
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_PlaylistCount:
Leh_func_begin8:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end8:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance:
Leh_func_begin9:
	push	{r4, r5, r7, lr}
Ltmp15:
	add	r7, sp, #8
Ltmp16:
Ltmp17:
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC9_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC9_0+8))
LPC9_0:
	add	r0, pc, r0
	ldr	r5, [r0, #20]
	ldr	r4, [r5]
	cmp	r4, #0
	bne	LBB9_2
	ldr	r0, [r0, #24]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_6_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor_llvm
	str	r4, [r5]
LBB9_2:
	mov	r0, r4
	pop	{r4, r5, r7, pc}
Leh_func_end9:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream:
Leh_func_begin10:
	push	{r4, r5, r6, r7, lr}
Ltmp18:
	add	r7, sp, #12
Ltmp19:
	push	{r10, r11}
Ltmp20:
	movw	r5, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC10_1+8))
	mov	r11, r0
	mov	r4, r2
	mov	r10, r1
	movt	r5, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC10_1+8))
LPC10_1:
	add	r5, pc, r5
	ldr	r0, [r5, #48]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	str	r4, [r6, #8]
	cmp	r6, #0
	beq	LBB10_2
	ldr	r0, [r5, #52]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r1, r10
	str	r6, [r2, #16]
	ldr	r0, [r5, #56]
	str	r0, [r2, #20]
	ldr	r0, [r5, #60]
	str	r0, [r2, #28]
	ldr	r0, [r5, #64]
	str	r0, [r2, #12]
	mov	r0, r11
	bl	_p_9_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool_llvm
	tst	r0, #255
	popne	{r10, r11}
	popne	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC10_2+8))
	mov	r1, #195
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC10_2+8))
LPC10_2:
	ldr	r0, [pc, r0]
	bl	_p_10_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r11, r0
	ldr	r0, [r5, #68]
	mov	r1, #1
	bl	_p_11_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	mov	r1, #0
	mov	r2, r10
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r5, #72]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r11
	mov	r2, r6
	mov	r4, r0
	bl	_p_12_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm
	mov	r0, r4
	bl	_p_13_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp21:
LBB10_2:
	ldr	r0, LCPI10_0
LPC10_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI10_0:
	.long	Ltmp21-(LPC10_0+8)
	.end_data_region
Leh_func_end10:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool:
Leh_func_begin11:
	push	{r7, lr}
Ltmp22:
	mov	r7, sp
Ltmp23:
Ltmp24:
	bl	_p_14_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm
	pop	{r7, pc}
Leh_func_end11:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp25:
	add	r7, sp, #8
Ltmp26:
Ltmp27:
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC12_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC12_0+8))
LPC12_0:
	add	r0, pc, r0
	ldr	r5, [r0, #76]
	ldr	r0, [r4]
	mov	r0, r4
	mov	r1, r5
	bl	_p_18_plt_string_StartsWith_string_llvm
	tst	r0, #255
	beq	LBB12_2
	ldr	r1, [r5, #8]
	mov	r0, r4
	bl	_p_21_plt_string_Substring_int_llvm
	pop	{r4, r5, r7, pc}
LBB12_2:
	mov	r0, #5
	bl	_p_19_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm
	mov	r1, r4
	bl	_p_20_plt_System_IO_Path_Combine_string_string_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end12:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__SavePlaylistm__2_MonoTouch_MediaPlayer_MPMediaItem
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__SavePlaylistm__2_MonoTouch_MediaPlayer_MPMediaItem:
Leh_func_begin13:
	push	{r7, lr}
Ltmp28:
	mov	r7, sp
Ltmp29:
Ltmp30:
	sub	sp, sp, #8
	bic	sp, sp, #7
	mov	r1, #0
	str	r1, [sp, #4]
	str	r1, [sp]
	ldr	r1, [r0]
	bl	_p_26_plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentID_llvm
	stm	sp, {r0, r1}
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC13_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r0, [r0, #124]
	ldr	r1, [r0]
	mov	r0, sp
	bl	_p_27_plt_ulong_ToString_System_IFormatProvider_llvm
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end13:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp31:
	add	r7, sp, #8
Ltmp32:
	push	{r10}
Ltmp33:
	mov	r5, r2
	mov	r10, r1
	mov	r4, r0
	bl	_p_28_plt_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate__ctor_llvm
	str	r10, [r4, #20]
	str	r5, [r4, #24]
	pop	{r10}
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaItemsPicked_MonoTouch_MediaPlayer_MPMediaPickerController_MonoTouch_MediaPlayer_MPMediaItemCollection
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaItemsPicked_MonoTouch_MediaPlayer_MPMediaPickerController_MonoTouch_MediaPlayer_MPMediaItemCollection:
Leh_func_begin15:
	push	{r4, r5, r6, r7, lr}
Ltmp34:
	add	r7, sp, #12
Ltmp35:
Ltmp36:
	mov	r4, r0
	mov	r5, r2
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r2, [r1, #88]
	mov	r1, r5
	blx	r2
	ldr	r6, [r4, #20]
	ldr	r0, [r5]
	ldr	r1, [r0, #76]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_29_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_llvm
	ldr	r0, [r4, #24]
	mov	r2, #0
	ldr	r1, [r0]
	ldr	r3, [r1, #100]
	mov	r1, #1
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end15:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaPickerDidCancel_MonoTouch_MediaPlayer_MPMediaPickerController
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaPickerDidCancel_MonoTouch_MediaPlayer_MPMediaPickerController:
Leh_func_begin16:
	push	{r7, lr}
Ltmp37:
	mov	r7, sp
Ltmp38:
Ltmp39:
	ldr	r0, [r0, #24]
	mov	r2, #0
	ldr	r1, [r0]
	ldr	r3, [r1, #100]
	mov	r1, #1
	blx	r3
	pop	{r7, pc}
Leh_func_end16:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin__ctor
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin__ctor:
Leh_func_begin17:
	bx	lr
Leh_func_end17:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin_Load
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin_Load:
Leh_func_begin18:
	push	{r4, r5, r7, lr}
Ltmp40:
	add	r7, sp, #8
Ltmp41:
	push	{r8}
Ltmp42:
	movw	r5, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC18_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC18_0+8))
LPC18_0:
	add	r5, pc, r5
	ldr	r0, [r5, #128]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_30_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor_llvm
	ldr	r0, [r5, #132]
	mov	r8, r0
	mov	r0, r4
	bl	_p_31_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end18:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__ctor
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__ctor:
Leh_func_begin19:
	bx	lr
Leh_func_end19:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__ctor
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__ctor:
Leh_func_begin20:
	bx	lr
Leh_func_end20:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__m__1_System_IO_Stream
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__m__1_System_IO_Stream:
Leh_func_begin21:
	push	{r7, lr}
Ltmp43:
	mov	r7, sp
Ltmp44:
Ltmp45:
	ldr	r0, [r0, #8]
	ldr	r2, [r0, #12]
	blx	r2
	mov	r0, #1
	pop	{r7, pc}
Leh_func_end21:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin22:
	push	{r4, r5, r7, lr}
Ltmp46:
	add	r7, sp, #8
Ltmp47:
Ltmp48:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC22_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC22_0+8))
LPC22_0:
	add	r0, pc, r0
	ldr	r0, [r0, #140]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB22_2
	bl	_p_33_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB22_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB22_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB22_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB22_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB22_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end22:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin23:
	push	{r4, r5, r7, lr}
Ltmp49:
	add	r7, sp, #8
Ltmp50:
Ltmp51:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC23_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC23_0+8))
LPC23_0:
	add	r0, pc, r0
	ldr	r0, [r0, #140]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB23_2
	bl	_p_33_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB23_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB23_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB23_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB23_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB23_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end23:

	.private_extern	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_invoke_TResult__this___T_MonoTouch_MediaPlayer_MPMediaItem
	.align	2
_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_invoke_TResult__this___T_MonoTouch_MediaPlayer_MPMediaItem:
Leh_func_begin24:
	push	{r4, r5, r7, lr}
Ltmp52:
	add	r7, sp, #8
Ltmp53:
Ltmp54:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC24_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #140]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB24_2
	bl	_p_33_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB24_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB24_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB24_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB24_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB24_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end24:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_type_info_2,4,2
.zerofill __DATA,__bss,_type_info_3,4,2
.zerofill __DATA,__bss,_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got,428,4
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Start
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Stop
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_NextTrack
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Player
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_set_Player_MonoTouch_MediaPlayer_MPMusicPlayerController
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_PlaylistCount
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__SavePlaylistm__2_MonoTouch_MediaPlayer_MPMediaItem
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaItemsPicked_MonoTouch_MediaPlayer_MPMediaPickerController_MonoTouch_MediaPlayer_MPMediaItemCollection
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaPickerDidCancel_MonoTouch_MediaPlayer_MPMediaPickerController
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin__ctor
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin_Load
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__ctor
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__ctor
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__m__1_System_IO_Stream
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_invoke_TResult__this___T_MonoTouch_MediaPlayer_MPMediaItem
	.no_dead_strip	_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	25
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	11
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	12
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	14
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	16
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	17
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	18
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	19
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	20
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	21
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	22
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	24
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	25
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	27
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	32
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	33
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
Lset25 = Leh_func_end24-Leh_func_begin24
	.long	Lset25
Lset26 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset26
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	4
	.byte	138
	.byte	5

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	5

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "OffSeasonProPluginMusicPlayerPluginTouch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist
_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,112,208,77,226,13,176,160,225,84,0,139,229,0,0,160,227,16,0,139,229
	.byte 0,0,160,227,20,0,139,229,0,0,160,227,24,0,139,229,0,0,160,227,28,0,139,229,0,0,160,227,32,0,139,229
	.byte 5,0,160,227
bl _p_19

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 68
	.byte 1,16,159,231
bl _p_20

	.byte 0,80,160,225,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 72
	.byte 1,16,159,231
bl _p_20

	.byte 0,96,160,225,5,0,160,225
bl _p_53

	.byte 255,0,0,226,0,0,80,227,1,0,0,26,5,0,160,225
bl _p_52

	.byte 6,0,160,225
bl _p_16

	.byte 255,0,0,226,0,0,80,227,213,0,0,10,84,0,155,229,6,16,160,225
bl _p_51

	.byte 0,0,139,229,0,0,80,227,207,0,0,10,6,0,160,225
bl _p_50

	.byte 255,0,0,226,0,0,80,227,202,0,0,26,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 76
	.byte 8,128,159,231
bl _p_22

	.byte 4,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 172
	.byte 0,0,159,231,0,48,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 168
	.byte 3,48,159,231,4,0,155,229,0,16,155,229,4,32,155,229,0,32,146,229,3,128,160,225,4,224,143,226,76,240,18,229
	.byte 0,0,0,0,8,0,139,229,0,0,80,227,149,0,0,10,8,0,155,229,0,224,208,229,12,0,144,229,0,0,80,227
	.byte 144,0,0,218,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 164
	.byte 0,0,159,231
bl _p_5

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 160
	.byte 1,16,159,231,0,16,145,229,8,16,128,229,0,160,160,225,16,16,139,226,8,0,155,229,0,32,160,225,0,224,210,229
bl _p_49

	.byte 47,0,0,234,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 152
	.byte 0,0,159,231,28,0,155,229,12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 156
	.byte 0,0,159,231
bl _p_2

	.byte 96,0,139,229
bl _p_48

	.byte 96,0,155,229,0,64,160,225,88,0,139,229,12,0,155,229
bl _p_47

	.byte 92,0,139,229
bl _p_46
bl _p_45

	.byte 0,16,160,225,92,0,155,229
bl _p_44

	.byte 0,16,160,225,88,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,76,240,146,229,4,0,160,225,0,16,148,229
	.byte 15,224,160,225,80,240,145,229,12,0,144,229,0,0,80,227,10,0,0,218,4,0,160,225,0,16,148,229,15,224,160,225
	.byte 80,240,145,229,12,16,144,229,0,0,81,227,113,0,0,155,16,16,144,229,10,0,160,225,0,224,218,229
bl _p_42

	.byte 16,0,139,226,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 152
	.byte 8,128,159,231
bl _p_41

	.byte 255,0,0,226,0,0,80,227,198,255,255,26,0,0,0,235,9,0,0,234,72,224,139,229,16,0,139,226,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 152
	.byte 1,16,159,231,56,0,139,229,0,224,208,229,72,192,155,229,12,240,160,225,84,0,155,229,8,0,144,229,104,0,139,229
	.byte 10,0,160,225,0,224,218,229
bl _p_40

	.byte 108,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 148
	.byte 0,0,159,231
bl _p_2

	.byte 108,16,155,229,100,0,139,229
bl _p_39

	.byte 100,16,155,229,104,32,155,229,2,0,160,225,0,32,146,229,15,224,160,225,88,240,146,229,0,224,218,229,12,16,154,229
	.byte 84,0,155,229,12,16,128,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 144
	.byte 0,0,159,231,92,0,139,229,0,224,218,229,12,0,154,229,96,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 140
	.byte 0,0,159,231
bl _p_38

	.byte 0,16,160,225,92,0,155,229,96,32,155,229,8,32,129,229,0,32,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 136
	.byte 2,32,159,231
bl _p_37

	.byte 88,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 56
	.byte 0,0,159,231,0,16,160,227
bl _p_11

	.byte 0,32,160,225,88,16,155,229,0,0,160,227
bl _p_35

	.byte 26,0,0,234,36,0,155,229,36,0,155,229,32,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 132
	.byte 0,0,159,231,88,0,139,229,32,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,76,240,145,229,0,16,160,225
	.byte 88,0,155,229
bl _p_36

	.byte 0,16,160,225,2,0,160,227,0,32,160,227
bl _p_35
bl _p_34

	.byte 80,0,139,229,0,0,80,227,1,0,0,10,80,0,155,229
bl _p_13

	.byte 255,255,255,234,112,208,139,226,112,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_43

	.byte 88,2,0,2

Lme_9:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0
_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,56,208,77,226,13,176,160,225,32,0,139,229,36,16,139,229,0,0,160,227
	.byte 8,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 16
	.byte 0,0,159,231
bl _p_5

	.byte 0,0,139,229,0,16,160,227,8,16,128,229,36,16,155,229,44,16,139,229,0,16,160,225,40,16,139,229,0,0,80,227
	.byte 65,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 20
	.byte 0,0,159,231
bl _p_5

	.byte 0,32,160,225,40,0,155,229,44,16,155,229,16,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 24
	.byte 0,0,159,231,20,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 28
	.byte 0,0,159,231,28,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 32
	.byte 0,0,159,231,12,0,130,229,32,0,155,229
bl _p_8

	.byte 0,0,155,229,8,0,144,229,4,0,139,229,30,0,0,234,12,0,155,229,12,0,155,229,8,0,139,229,40,0,139,229
	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . -12
	.byte 0,0,159,231,145,16,160,227
bl _p_10

	.byte 44,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 56
	.byte 0,0,159,231,1,16,160,227
bl _p_11

	.byte 16,0,139,229,48,0,139,229,16,48,155,229,36,32,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225
	.byte 128,240,147,229,40,0,155,229,44,16,155,229,48,32,155,229
bl _p_54
bl _p_13

	.byte 4,0,155,229,255,255,255,234,56,208,139,226,0,9,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_43

	.byte 6,2,0,2

Lme_a:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0:

	.byte 128,64,45,233,13,112,160,225,64,13,45,233,24,208,77,226,13,176,160,225,20,0,139,229,1,96,160,225,2,160,160,225
	.byte 0,0,160,227,0,0,139,229,20,0,155,229,6,16,160,225
bl _p_15

	.byte 0,96,160,225
bl _p_16

	.byte 255,0,0,226,0,0,80,227,1,0,0,26,0,0,160,227,26,0,0,234,6,0,160,225
bl _p_17

	.byte 0,0,139,229,0,16,155,229,10,0,160,225,15,224,160,225,12,240,154,229,4,0,203,229,0,0,0,235,15,0,0,234
	.byte 16,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10,0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 176
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,16,192,155,229,12,240,160,225,4,0,219,229,24,208,139,226
	.byte 64,13,189,232,128,128,189,232

Lme_d:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0
_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0:

	.byte 128,64,45,233,13,112,160,225,16,9,45,233,60,208,77,226,13,176,160,225,40,0,139,229,44,16,139,229,0,0,160,227
	.byte 12,0,139,229,5,0,160,227
bl _p_19

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 68
	.byte 1,16,159,231
bl _p_20

	.byte 0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 72
	.byte 1,16,159,231
bl _p_20

	.byte 0,0,139,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 76
	.byte 8,128,159,231
bl _p_22

	.byte 0,64,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 80
	.byte 0,0,159,231,0,0,144,229,44,16,155,229,20,16,139,229,0,0,80,227,25,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 96
	.byte 0,0,159,231
bl _p_5

	.byte 0,16,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 100
	.byte 0,0,159,231,20,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 104
	.byte 0,0,159,231,28,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 108
	.byte 0,0,159,231,12,0,129,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 80
	.byte 0,0,159,231,0,16,128,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 80
	.byte 0,0,159,231,0,16,144,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 84
	.byte 8,128,159,231,20,0,155,229
bl _p_23

	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 88
	.byte 8,128,159,231
bl _p_24

	.byte 4,0,139,229,4,0,160,225,4,16,155,229,0,32,148,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 92
	.byte 8,128,159,231,4,224,143,226,4,240,18,229,0,0,0,0,8,0,139,229,0,0,155,229,8,16,155,229
bl _p_25

	.byte 26,0,0,234,16,0,155,229,16,0,155,229,12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 180
	.byte 0,0,159,231,48,0,139,229,12,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,76,240,145,229,0,16,160,225
	.byte 48,0,155,229
bl _p_36

	.byte 0,16,160,225,1,0,160,227,0,32,160,227
bl _p_35
bl _p_34

	.byte 36,0,139,229,0,0,80,227,1,0,0,10,36,0,155,229
bl _p_13

	.byte 255,255,255,234,60,208,139,226,16,9,189,232,128,128,189,232

Lme_f:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0
_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,16,0,139,229,20,16,139,229,0,0,160,227
	.byte 0,0,139,229,20,0,155,229,0,0,80,227,35,0,0,10,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 124
	.byte 0,0,159,231
bl _p_5

	.byte 24,0,139,229,20,16,155,229
bl _p_32

	.byte 24,0,155,229,0,0,139,229,0,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,52,240,145,229,0,16,160,225
	.byte 16,0,155,229,8,16,128,229,0,0,0,235,15,0,0,234,12,224,139,229,0,0,155,229,0,0,80,227,9,0,0,10
	.byte 0,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 176
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,32,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_17:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_57

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_55

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_56

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_55
bl _p_2

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_22:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Start
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Stop
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_NextTrack
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Player
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_set_Player_MonoTouch_MediaPlayer_MPMusicPlayerController
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_PlaylistCount
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__SavePlaylistm__2_MonoTouch_MediaPlayer_MPMediaItem
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaItemsPicked_MonoTouch_MediaPlayer_MPMediaPickerController_MonoTouch_MediaPlayer_MPMediaItemCollection
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaPickerDidCancel_MonoTouch_MediaPlayer_MPMediaPickerController
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin__ctor
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin_Load
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__ctor
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__ctor
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__m__1_System_IO_Stream
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
.no_dead_strip _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_invoke_TResult__this___T_MonoTouch_MediaPlayer_MPMediaItem

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Start
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_Stop
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer_NextTrack
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Player
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_set_Player_MonoTouch_MediaPlayer_MPMusicPlayerController
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_PlaylistCount
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
	bl _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist
	bl _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
	bl _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string
	bl _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__SavePlaylistm__2_MonoTouch_MediaPlayer_MPMediaItem
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate__ctor_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MonoTouch_UIKit_UIViewController
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaItemsPicked_MonoTouch_MediaPlayer_MPMediaPickerController_MonoTouch_MediaPlayer_MPMediaItemCollection
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_MediaPickerDelegate_MediaPickerDidCancel_MonoTouch_MediaPlayer_MPMediaPickerController
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin__ctor
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_Plugin_Load
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__ctor
	bl _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__ctor
	bl _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetResourceStreamc__AnonStorey1__m__1_System_IO_Stream
	bl method_addresses
	bl _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	bl _OffSeasonProPluginMusicPlayerPluginTouch__wrapper_delegate_invoke_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_invoke_TResult__this___T_MonoTouch_MediaPlayer_MPMediaItem
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 35,10,4,2
	.short 0, 10, 20, 38
	.byte 1,2,2,2,2,3,2,2,2,5,42,8,9,2,3,4,16,3,2,2,93,2,4,2,4,2,255,255,255,255,149,109
	.byte 255,255,255,255,147,0,0,0,112,3,3
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,167,34,0,0,0,0
	.long 0,0,0,132,32,0,120,27
	.long 11,0,0,0,0,0,0,0
	.long 0,0,144,33,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 8,27,120,28,0,29,0,30
	.long 0,31,0,32,132,33,144,34
	.long 167
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 2, 0, 0
	.short 0, 0, 0, 7, 0, 0, 0, 0
	.short 0, 3, 11, 5, 0, 0, 0, 4
	.short 12, 6, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 49,10,5,2
	.short 0, 11, 22, 33, 44
	.byte 128,185,2,1,1,1,5,5,4,4,6,128,219,5,6,4,6,5,5,6,6,4,129,14,3,3,12,5,12,12,5,6
	.byte 5,129,82,6,6,4,12,4,1,3,3,4,129,128,5,6,14,6,6,12,12,3
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 35,10,4,2
	.short 0, 11, 22, 42
	.byte 132,62,3,3,3,3,3,3,3,3,3,132,115,15,3,3,11,3,15,3,3,3,132,177,3,3,3,12,3,255,255,255
	.byte 251,55,132,204,255,255,255,251,52,0,0,0,132,207,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 32,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,144,1,68,13
	.byte 11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,72,68,13,11,27,12,13,0,72,14,8
	.byte 135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11,25,12,13,0,72,14,8,135,2,68,14
	.byte 20,132,5,136,4,139,3,142,1,68,14,80,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142
	.byte 1,68,14,48,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 7,10,1,2
	.short 0
	.byte 132,243,7,27,15,59,19,15

.text
	.align 4
plt:
_mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_plt:
_p_1_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_get_Instance:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 196,452
_p_2_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 200,457
_p_3_plt_MonoTouch_MediaPlayer_MPMusicPlayerController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMusicPlayerController__ctor
plt_MonoTouch_MediaPlayer_MPMusicPlayerController__ctor:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 204,484
_p_4_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 208,489
_p_5_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 212,494
_p_6_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__ctor:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 216,517
_p_7_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 220,522
_p_8_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetResourceStream_string_System_Action_1_System_IO_Stream:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 224,567

.set _p_9_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool_llvm, _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadBinaryFile_string_System_Func_2_System_IO_Stream_bool
_p_10_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 232,577
_p_11_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 236,597
_p_12_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 240,623
_p_13_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 244,628
_p_14_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 248,656
_p_15_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_FullPath_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 252,661
_p_16_plt_System_IO_File_Exists_string_llvm:
	.no_dead_strip plt_System_IO_File_Exists_string
plt_System_IO_File_Exists_string:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 256,666
_p_17_plt_System_IO_File_OpenRead_string_llvm:
	.no_dead_strip plt_System_IO_File_OpenRead_string
plt_System_IO_File_OpenRead_string:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 260,669
_p_18_plt_string_StartsWith_string_llvm:
	.no_dead_strip plt_string_StartsWith_string
plt_string_StartsWith_string:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 264,672
_p_19_plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder_llvm:
	.no_dead_strip plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder
plt_System_Environment_GetFolderPath_System_Environment_SpecialFolder:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 268,675
_p_20_plt_System_IO_Path_Combine_string_string_llvm:
	.no_dead_strip plt_System_IO_Path_Combine_string_string
plt_System_IO_Path_Combine_string_string:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 272,678
_p_21_plt_string_Substring_int_llvm:
	.no_dead_strip plt_string_Substring_int
plt_string_Substring_int:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 276,681
_p_22_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxJsonConverter:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 280,684
_p_23_plt_System_Linq_Enumerable_Select_MonoTouch_MediaPlayer_MPMediaItem_string_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_Select_MonoTouch_MediaPlayer_MPMediaItem_string_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string
plt_System_Linq_Enumerable_Select_MonoTouch_MediaPlayer_MPMediaItem_string_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_System_Func_2_MonoTouch_MediaPlayer_MPMediaItem_string:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 284,696
_p_24_plt_System_Linq_Enumerable_ToList_string_System_Collections_Generic_IEnumerable_1_string_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_ToList_string_System_Collections_Generic_IEnumerable_1_string
plt_System_Linq_Enumerable_ToList_string_System_Collections_Generic_IEnumerable_1_string:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 288,708
_p_25_plt_System_IO_File_WriteAllText_string_string_llvm:
	.no_dead_strip plt_System_IO_File_WriteAllText_string_string
plt_System_IO_File_WriteAllText_string_string:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 292,720
_p_26_plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentID_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentID
plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentID:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 296,723
_p_27_plt_ulong_ToString_System_IFormatProvider_llvm:
	.no_dead_strip plt_ulong_ToString_System_IFormatProvider
plt_ulong_ToString_System_IFormatProvider:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 300,728
_p_28_plt_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate__ctor_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate__ctor
plt_MonoTouch_MediaPlayer_MPMediaPickerControllerDelegate__ctor:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 304,731
_p_29_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 308,736

.set _p_30_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor_llvm, _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
_p_31_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 316,746
_p_32_plt_System_IO_StreamReader__ctor_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_StreamReader__ctor_System_IO_Stream
plt_System_IO_StreamReader__ctor_System_IO_Stream:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 320,758
_p_33_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 324,761
_p_34_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 328,799
_p_35_plt_Cirrious_CrossCore_Mvx_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__
plt_Cirrious_CrossCore_Mvx_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_object__:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 332,838
_p_36_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 336,843
_p_37_plt_string_Concat_object_object_object_llvm:
	.no_dead_strip plt_string_Concat_object_object_object
plt_string_Concat_object_object_object:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 340,846
_p_38_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 344,849
_p_39_plt_MonoTouch_MediaPlayer_MPMediaItemCollection__ctor_MonoTouch_MediaPlayer_MPMediaItem___llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaItemCollection__ctor_MonoTouch_MediaPlayer_MPMediaItem__
plt_MonoTouch_MediaPlayer_MPMediaItemCollection__ctor_MonoTouch_MediaPlayer_MPMediaItem__:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 348,879
_p_40_plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_ToArray_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_ToArray
plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_ToArray:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 352,884
_p_41_plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext
plt_System_Collections_Generic_List_1_Enumerator_string_MoveNext:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 356,895
_p_42_plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_Add_MonoTouch_MediaPlayer_MPMediaItem_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_Add_MonoTouch_MediaPlayer_MPMediaItem
plt_System_Collections_Generic_List_1_MonoTouch_MediaPlayer_MPMediaItem_Add_MonoTouch_MediaPlayer_MPMediaItem:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 360,906
_p_43_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 364,917
_p_44_plt_MonoTouch_MediaPlayer_MPMediaPropertyPredicate_PredicateWithValue_MonoTouch_Foundation_NSObject_string_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaPropertyPredicate_PredicateWithValue_MonoTouch_Foundation_NSObject_string
plt_MonoTouch_MediaPlayer_MPMediaPropertyPredicate_PredicateWithValue_MonoTouch_Foundation_NSObject_string:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 368,952
_p_45_plt_MonoTouch_Foundation_NSString_op_Implicit_MonoTouch_Foundation_NSString_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSString_op_Implicit_MonoTouch_Foundation_NSString
plt_MonoTouch_Foundation_NSString_op_Implicit_MonoTouch_Foundation_NSString:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 372,957
_p_46_plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentIDProperty_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentIDProperty
plt_MonoTouch_MediaPlayer_MPMediaItem_get_PersistentIDProperty:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 376,962
_p_47_plt_MonoTouch_Foundation_NSObject_FromObject_object_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSObject_FromObject_object
plt_MonoTouch_Foundation_NSObject_FromObject_object:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 380,967
_p_48_plt_MonoTouch_MediaPlayer_MPMediaQuery__ctor_llvm:
	.no_dead_strip plt_MonoTouch_MediaPlayer_MPMediaQuery__ctor
plt_MonoTouch_MediaPlayer_MPMediaQuery__ctor:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 384,972
_p_49_plt_System_Collections_Generic_List_1_string_GetEnumerator_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_string_GetEnumerator
plt_System_Collections_Generic_List_1_string_GetEnumerator:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 388,977
_p_50_plt_string_IsNullOrWhiteSpace_string_llvm:
	.no_dead_strip plt_string_IsNullOrWhiteSpace_string
plt_string_IsNullOrWhiteSpace_string:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 392,988
_p_51_plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string
plt_OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 396,991
_p_52_plt_System_IO_Directory_CreateDirectory_string_llvm:
	.no_dead_strip plt_System_IO_Directory_CreateDirectory_string
plt_System_IO_Directory_CreateDirectory_string:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 400,996
_p_53_plt_System_IO_Directory_Exists_string_llvm:
	.no_dead_strip plt_System_IO_Directory_Exists_string
plt_System_IO_Directory_Exists_string:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 404,999
_p_54_plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxExceptionExtensionMethods_MvxWrap_System_Exception_string_object__:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 408,1002
_p_55_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 412,1033
_p_56_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 416,1041
_p_57_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got - . + 420,1060
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "OffSeasonProPluginMusicPlayerPluginTouch"
	.asciz "B6AB8136-73DF-40BE-9A90-EE49A3A457BF"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "B6AB8136-73DF-40BE-9A90-EE49A3A457BF"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "OffSeasonProPluginMusicPlayerPluginTouch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_OffSeasonProPluginMusicPlayerPluginTouch_got
	.align 2
	.long _OffSeasonProPluginMusicPlayerPluginTouch__OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MvxTouchMusicPlayer__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 49,428,58,35,11,387000831,0,1424
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_OffSeasonProPluginMusicPlayerPluginTouch_info
	.align 2
_mono_aot_module_OffSeasonProPluginMusicPlayerPluginTouch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,1,4,0,0,0,0,0,0,0,3,5,6,5,0,17,20,21,22,46,45,44,43
	.byte 41,42,41,41,40,39,38,37,17,36,0,6,7,8,9,10,11,17,0,7,12,18,17,16,15,14,13,0,0,0,1,47
	.byte 0,2,19,19,0,14,20,21,22,23,27,28,29,30,23,23,24,25,26,48,0,1,31,0,0,0,0,0,0,0,0,0
	.byte 2,33,32,0,0,0,2,34,47,0,0,0,0,0,1,35,0,1,35,0,1,35,0,0,255,252,0,0,0,1,1,3
	.byte 219,0,0,4,255,252,0,0,0,1,1,3,219,0,0,5,255,252,0,0,0,1,1,3,219,0,0,6,5,30,0,1
	.byte 255,255,255,255,255,141,43,255,253,0,0,0,1,130,10,0,198,0,13,43,0,1,7,128,156,12,1,39,42,47,14,2
	.byte 128,231,2,16,2,3,1,3,14,2,3,1,14,2,6,1,14,3,219,0,0,4,6,193,0,0,24,50,193,0,0,24
	.byte 30,3,219,0,0,4,14,2,7,1,14,3,219,0,0,5,6,193,0,0,26,50,193,0,0,26,30,3,219,0,0,5
	.byte 14,6,1,1,130,123,14,2,30,3,17,1,128,243,17,1,1,17,1,11,34,255,254,0,0,0,1,255,43,0,0,1
	.byte 16,2,3,1,6,34,255,254,0,0,0,1,255,43,0,0,3,34,255,254,0,0,0,1,255,43,0,0,4,6,195,0
	.byte 1,55,14,3,219,0,0,6,6,193,0,0,17,50,193,0,0,17,30,3,219,0,0,6,16,1,128,184,130,35,14,2
	.byte 2,1,34,255,254,0,0,0,1,255,43,0,0,5,14,1,128,228,33,17,1,97,17,1,55,14,1,130,90,17,1,39
	.byte 14,2,128,223,2,14,3,219,0,0,3,14,2,128,228,2,4,1,130,50,1,2,128,163,2,16,7,129,144,135,229,14
	.byte 3,219,0,0,2,6,255,254,0,0,0,1,255,43,0,0,2,34,255,254,0,0,0,1,255,43,0,0,2,6,145,58
	.byte 17,1,128,253,3,193,0,0,9,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99,105
	.byte 102,105,99,0,3,194,0,6,98,3,193,0,0,10,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95
	.byte 102,97,115,116,0,3,193,0,0,5,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120
	.byte 99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,3,193,0,0,12,3,193,0,0
	.byte 13,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,23,109,111,110,111,95,97,114,114,97
	.byte 121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,195,0,0,78,7,25,109,111,110,111,95,97,114,99,104,95
	.byte 116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,193,0,0,14,3,193,0,0,15,3,134,165,3,134,168
	.byte 3,147,107,3,144,189,3,135,38,3,147,53,3,255,254,0,0,0,1,255,43,0,0,1,3,255,254,0,0,0,1,255
	.byte 43,0,0,3,3,255,254,0,0,0,1,255,43,0,0,4,3,134,170,3,194,0,3,194,3,148,254,3,194,0,6,78
	.byte 3,193,0,0,16,3,193,0,0,1,3,255,254,0,0,0,1,255,43,0,0,5,3,135,97,7,35,109,111,110,111,95
	.byte 116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,7
	.byte 36,109,111,110,111,95,116,104,114,101,97,100,95,103,101,116,95,117,110,100,101,110,105,97,98,108,101,95,101,120,99,101
	.byte 112,116,105,111,110,0,3,195,0,1,8,3,147,133,3,147,132,7,27,109,111,110,111,95,111,98,106,101,99,116,95,110
	.byte 101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,194,0,6,59,3,255,254,0,0,0,1,202,0,0,30,3
	.byte 255,254,0,0,0,1,202,0,0,28,3,255,254,0,0,0,1,202,0,0,27,7,32,109,111,110,111,95,97,114,99,104
	.byte 95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,0,3,194,0,6,94,3,194,0
	.byte 1,19,3,194,0,3,197,3,194,0,2,17,3,194,0,6,83,3,255,254,0,0,0,1,202,0,0,18,3,147,167,3
	.byte 193,0,0,11,3,134,139,3,134,144,3,195,0,0,83,255,253,0,0,0,1,130,10,0,198,0,13,43,0,1,7,128
	.byte 156,4,1,130,11,1,7,128,156,35,131,239,150,5,7,132,1,3,255,253,0,0,0,7,132,1,0,198,0,13,121,1
	.byte 7,128,156,0,35,131,239,192,0,92,41,255,253,0,0,0,1,130,10,0,198,0,13,43,0,1,7,128,156,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,0,2,2,0,130,168
	.byte 129,144,130,124,130,128,0,36,3,1,130,64,128,236,131,148,131,148,0,6,33,1,0,12,3,1,130,64,36,128,212,128
	.byte 212,0,16,0,0,16,0,0,6,57,1,2,0,128,184,92,116,120,0,16,0,0,6,85,1,0,16,3,1,130,64,112
	.byte 129,124,129,124,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,111,1,2,0,128
	.byte 192,88,124,128,128,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,3,111,0,1,11,4,18,255,253,0,0
	.byte 0,1,130,10,0,198,0,13,43,0,1,7,128,156,1,0,1,0,0,0,128,144,8,0,0,1,7,128,160,12,0,0
	.byte 4,146,178,146,175,146,174,146,172,193,0,0,2,193,0,0,3,193,0,0,4,4,128,232,16,8,0,4,146,178,146,175
	.byte 146,174,146,172,13,128,162,194,0,2,8,28,0,0,4,194,0,2,18,146,175,194,0,2,8,146,172,194,0,2,4,194
	.byte 0,2,9,194,0,2,20,194,0,2,13,194,0,2,12,194,0,2,7,194,0,2,6,193,0,0,20,193,0,0,19,5
	.byte 128,144,8,0,0,1,146,178,146,175,146,174,146,172,193,0,0,22,4,128,160,12,0,0,4,146,178,146,175,146,174,146
	.byte 172,4,128,160,12,0,0,4,146,178,146,175,146,174,146,172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_5:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_4:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM10=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM12=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_3:

	.byte 5
	.asciz "MonoTouch_Foundation_NSObject"

	.byte 20,16
LDIFF_SYM15=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM16=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,6
	.asciz "super"

LDIFF_SYM17=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM18=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,16,6
	.asciz "IsDirectBinding"

LDIFF_SYM19=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,17,6
	.asciz "registered_toggleref"

LDIFF_SYM20=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,18,0,7
	.asciz "MonoTouch_Foundation_NSObject"

LDIFF_SYM21=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM22=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM23=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_2:

	.byte 5
	.asciz "MonoTouch_MediaPlayer_MPMusicPlayerController"

	.byte 20,16
LDIFF_SYM24=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,0,0,7
	.asciz "MonoTouch_MediaPlayer_MPMusicPlayerController"

LDIFF_SYM25=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM26=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM27=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_6:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM28=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM29=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM30=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM30
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM31=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM32=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM32
LTDIE_0:

	.byte 5
	.asciz "OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer"

	.byte 16,16
LDIFF_SYM33=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,0,6
	.asciz "<Player>k__BackingField"

LDIFF_SYM34=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,8,6
	.asciz "_playlistCount"

LDIFF_SYM35=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,12,0,7
	.asciz "OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer"

LDIFF_SYM36=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM37=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM38=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_7:

	.byte 17
	.asciz "Cirrious_CrossCore_Platform_IMvxJsonConverter"

	.byte 8,7
	.asciz "Cirrious_CrossCore_Platform_IMvxJsonConverter"

LDIFF_SYM39=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM39
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM40=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM40
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM41=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_8:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM42=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM43=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM44=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM45=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM46=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM46
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM47=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM47
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM48=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_9:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM49=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM50=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM51=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM52=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM53=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM54=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM55=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_10:

	.byte 5
	.asciz "MonoTouch_MediaPlayer_MPMediaQuery"

	.byte 24,16
LDIFF_SYM56=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM56
	.byte 2,35,0,6
	.asciz "__mt_Items_var"

LDIFF_SYM57=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,20,0,7
	.asciz "MonoTouch_MediaPlayer_MPMediaQuery"

LDIFF_SYM58=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM59=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM60=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_12:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM61=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM62=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM63=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_11:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM64=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM65=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM66=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM67=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM68=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM69=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM69
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM70=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM71=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM72=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM73=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM74=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM75=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM76=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM77=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM78=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM79=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM80=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin.Touch.MyMusicPlayer:GetPlaylist"
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist
	.long Lme_9

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM81=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 3,123,212,0,11
	.asciz "V_0"

LDIFF_SYM82=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM83=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM84=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 1,86,11
	.asciz "V_3"

LDIFF_SYM85=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,123,0,11
	.asciz "V_4"

LDIFF_SYM86=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,123,4,11
	.asciz "V_5"

LDIFF_SYM87=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,123,8,11
	.asciz "V_6"

LDIFF_SYM88=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM88
	.byte 1,90,11
	.asciz "V_7"

LDIFF_SYM89=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM89
	.byte 2,123,12,11
	.asciz "V_8"

LDIFF_SYM90=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,123,16,11
	.asciz "V_9"

LDIFF_SYM91=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 1,84,11
	.asciz "V_10"

LDIFF_SYM92=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,123,32,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM93=Lfde0_end - Lfde0_start
	.long LDIFF_SYM93
Lfde0_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist

LDIFF_SYM94=Lme_9 - _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetPlaylist
	.long LDIFF_SYM94
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,144,1,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_13:

	.byte 5
	.asciz "_<GetTextResource>c__AnonStorey0"

	.byte 12,16
LDIFF_SYM95=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,0,6
	.asciz "text"

LDIFF_SYM96=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,8,0,7
	.asciz "_<GetTextResource>c__AnonStorey0"

LDIFF_SYM97=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM98=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM99=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin.Touch.MyMusicPlayer:GetTextResource"
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0
	.long Lme_a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM100=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM100
	.byte 2,123,32,3
	.asciz "resourcePath"

LDIFF_SYM101=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM101
	.byte 2,123,36,11
	.asciz "V_0"

LDIFF_SYM102=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM103=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,123,4,11
	.asciz "V_2"

LDIFF_SYM104=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM105=Lfde1_end - Lfde1_start
	.long LDIFF_SYM105
Lfde1_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0

LDIFF_SYM106=Lme_a - _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_GetTextResource_string_0
	.long LDIFF_SYM106
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_19:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM107=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM108=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM109=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM110=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM110
LTDIE_18:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM111=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM112=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM113=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM114=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_17:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM115=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM116=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM117=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM118=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_21:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM119=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM120=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM121=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM122=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM122
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM123=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM123
LTDIE_20:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM124=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM125=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM126=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM126
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM127=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM128=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM129=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_16:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM130=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM131=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM131
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM132=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM132
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM133=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM133
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM134=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM135=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM136=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM136
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM137=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM137
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM138=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM139=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM139
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM140=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM141=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM142=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_15:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM143=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM143
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM144=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM144
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM145=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM145
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM146=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM147=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM148=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_14:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM149=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM150=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM150
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM151=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM151
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM152=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_24:

	.byte 5
	.asciz "System_Func`4"

	.byte 52,16
LDIFF_SYM153=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,0,0,7
	.asciz "System_Func`4"

LDIFF_SYM154=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM155=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM155
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM156=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_25:

	.byte 5
	.asciz "System_Action`3"

	.byte 52,16
LDIFF_SYM157=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,0,0,7
	.asciz "System_Action`3"

LDIFF_SYM158=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM159=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM160=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM160
LTDIE_29:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM161=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM161
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM162=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM163=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM164=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM164
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM165=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM165
LTDIE_33:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM166=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM167=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM168=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM169=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_32:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM170=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM171=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM172=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM173=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM174=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM175=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM175
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM176=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM176
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM177=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_31:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM178=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM179=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM179
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM180=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM180
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM181=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM181
LTDIE_30:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM182=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM183=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM184=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM184
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM185=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM185
LTDIE_28:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM186=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM187=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM188=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM189=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM189
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM190=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM190
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM191=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_27:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM192=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM193=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM194=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM195=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_26:

	.byte 5
	.asciz "System_Threading_AutoResetEvent"

	.byte 20,16
LDIFF_SYM196=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,35,0,0,7
	.asciz "System_Threading_AutoResetEvent"

LDIFF_SYM197=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM198=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM199=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_23:

	.byte 5
	.asciz "System_IO_Stream"

	.byte 20,16
LDIFF_SYM200=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 2,35,0,6
	.asciz "async_read"

LDIFF_SYM201=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,35,8,6
	.asciz "async_write"

LDIFF_SYM202=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 2,35,12,6
	.asciz "async_event"

LDIFF_SYM203=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,16,0,7
	.asciz "System_IO_Stream"

LDIFF_SYM204=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM204
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM205=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM205
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM206=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM206
LTDIE_34:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeFileHandle"

	.byte 24,16
LDIFF_SYM207=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeFileHandle"

LDIFF_SYM208=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM209=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM210=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM210
LTDIE_35:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM211=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM211
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM212=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM213=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM214=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM214
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM215=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM215
LTDIE_36:

	.byte 8
	.asciz "System_IO_FileAccess"

	.byte 4
LDIFF_SYM216=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 9
	.asciz "Read"

	.byte 1,9
	.asciz "Write"

	.byte 2,9
	.asciz "ReadWrite"

	.byte 3,0,7
	.asciz "System_IO_FileAccess"

LDIFF_SYM217=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM218=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM218
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM219=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_22:

	.byte 5
	.asciz "System_IO_FileStream"

	.byte 76,16
LDIFF_SYM220=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,0,6
	.asciz "buf"

LDIFF_SYM221=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,20,6
	.asciz "name"

LDIFF_SYM222=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,24,6
	.asciz "safeHandle"

LDIFF_SYM223=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM223
	.byte 2,35,28,6
	.asciz "append_startpos"

LDIFF_SYM224=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM224
	.byte 2,35,32,6
	.asciz "handle"

LDIFF_SYM225=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 2,35,40,6
	.asciz "access"

LDIFF_SYM226=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,44,6
	.asciz "owner"

LDIFF_SYM227=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,48,6
	.asciz "async"

LDIFF_SYM228=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,49,6
	.asciz "canseek"

LDIFF_SYM229=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,50,6
	.asciz "anonymous"

LDIFF_SYM230=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,51,6
	.asciz "buf_dirty"

LDIFF_SYM231=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,52,6
	.asciz "buf_size"

LDIFF_SYM232=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,56,6
	.asciz "buf_length"

LDIFF_SYM233=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,60,6
	.asciz "buf_offset"

LDIFF_SYM234=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,64,6
	.asciz "buf_start"

LDIFF_SYM235=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,68,0,7
	.asciz "System_IO_FileStream"

LDIFF_SYM236=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM236
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM237=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM237
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM238=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM238
	.byte 2
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin.Touch.MyMusicPlayer:TryReadFileCommon"
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	.long Lme_d

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM239=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM239
	.byte 2,123,20,3
	.asciz "path"

LDIFF_SYM240=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM240
	.byte 1,86,3
	.asciz "streamAction"

LDIFF_SYM241=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM241
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM242=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM242
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM243=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,123,0,11
	.asciz "V_2"

LDIFF_SYM244=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM245=Lfde2_end - Lfde2_start
	.long LDIFF_SYM245
Lfde2_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0

LDIFF_SYM246=Lme_d - _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_TryReadFileCommon_string_System_Func_2_System_IO_Stream_bool_0
	.long LDIFF_SYM246
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_37:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM247=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM247
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM248=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM248
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM249=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin.Touch.MyMusicPlayer:SavePlaylist"
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0
	.long Lme_f

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM250=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 0,3
	.asciz "items"

LDIFF_SYM251=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,123,44,11
	.asciz "V_0"

LDIFF_SYM252=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM253=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM253
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM254=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM254
	.byte 2,123,0,11
	.asciz "V_3"

LDIFF_SYM255=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 1,84,11
	.asciz "V_4"

LDIFF_SYM256=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,123,4,11
	.asciz "V_5"

LDIFF_SYM257=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,123,8,11
	.asciz "V_6"

LDIFF_SYM258=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM259=Lfde3_end - Lfde3_start
	.long LDIFF_SYM259
Lfde3_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0

LDIFF_SYM260=Lme_f - _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer_SavePlaylist_System_Collections_Generic_IEnumerable_1_MonoTouch_MediaPlayer_MPMediaItem_0
	.long LDIFF_SYM260
	.byte 12,13,0,72,14,8,135,2,68,14,20,132,5,136,4,139,3,142,1,68,14,80,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_39:

	.byte 5
	.asciz "System_IO_TextReader"

	.byte 8,16
LDIFF_SYM261=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM261
	.byte 2,35,0,0,7
	.asciz "System_IO_TextReader"

LDIFF_SYM262=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM263=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM264=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_41:

	.byte 5
	.asciz "System_Text_DecoderFallback"

	.byte 8,16
LDIFF_SYM265=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallback"

LDIFF_SYM266=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM266
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM267=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM267
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM268=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_42:

	.byte 5
	.asciz "System_Text_EncoderFallback"

	.byte 8,16
LDIFF_SYM269=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,0,0,7
	.asciz "System_Text_EncoderFallback"

LDIFF_SYM270=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM270
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM271=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM271
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM272=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM272
LTDIE_40:

	.byte 5
	.asciz "System_Text_Encoding"

	.byte 48,16
LDIFF_SYM273=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,0,6
	.asciz "codePage"

LDIFF_SYM274=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM274
	.byte 2,35,32,6
	.asciz "windows_code_page"

LDIFF_SYM275=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM275
	.byte 2,35,36,6
	.asciz "is_readonly"

LDIFF_SYM276=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM276
	.byte 2,35,40,6
	.asciz "decoder_fallback"

LDIFF_SYM277=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,8,6
	.asciz "encoder_fallback"

LDIFF_SYM278=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,12,6
	.asciz "is_mail_news_display"

LDIFF_SYM279=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM279
	.byte 2,35,41,6
	.asciz "is_mail_news_save"

LDIFF_SYM280=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM280
	.byte 2,35,42,6
	.asciz "is_browser_save"

LDIFF_SYM281=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,35,43,6
	.asciz "is_browser_display"

LDIFF_SYM282=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,35,44,6
	.asciz "body_name"

LDIFF_SYM283=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,35,16,6
	.asciz "encoding_name"

LDIFF_SYM284=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM284
	.byte 2,35,20,6
	.asciz "header_name"

LDIFF_SYM285=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM285
	.byte 2,35,24,6
	.asciz "web_name"

LDIFF_SYM286=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,35,28,0,7
	.asciz "System_Text_Encoding"

LDIFF_SYM287=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM287
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM288=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM288
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM289=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM289
LTDIE_44:

	.byte 5
	.asciz "System_Text_DecoderFallbackBuffer"

	.byte 8,16
LDIFF_SYM290=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,0,0,7
	.asciz "System_Text_DecoderFallbackBuffer"

LDIFF_SYM291=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM291
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM292=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM292
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM293=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM293
LTDIE_43:

	.byte 5
	.asciz "System_Text_Decoder"

	.byte 16,16
LDIFF_SYM294=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,0,6
	.asciz "fallback"

LDIFF_SYM295=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,8,6
	.asciz "fallback_buffer"

LDIFF_SYM296=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,12,0,7
	.asciz "System_Text_Decoder"

LDIFF_SYM297=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM297
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM298=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM298
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM299=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM299
LTDIE_45:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM300=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM300
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM301=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM302=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM303=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM303
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM304=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM305=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM305
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM306=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM306
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM307=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM307
LTDIE_46:

	.byte 17
	.asciz "System_Threading_Tasks_IDecoupledTask"

	.byte 8,7
	.asciz "System_Threading_Tasks_IDecoupledTask"

LDIFF_SYM308=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM308
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM309=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM309
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM310=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM310
LTDIE_38:

	.byte 5
	.asciz "System_IO_StreamReader"

	.byte 56,16
LDIFF_SYM311=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 2,35,0,6
	.asciz "input_buffer"

LDIFF_SYM312=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM312
	.byte 2,35,8,6
	.asciz "decoded_buffer"

LDIFF_SYM313=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 2,35,12,6
	.asciz "encoding"

LDIFF_SYM314=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 2,35,16,6
	.asciz "decoder"

LDIFF_SYM315=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM315
	.byte 2,35,20,6
	.asciz "line_builder"

LDIFF_SYM316=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,35,24,6
	.asciz "base_stream"

LDIFF_SYM317=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM317
	.byte 2,35,28,6
	.asciz "decoded_count"

LDIFF_SYM318=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM318
	.byte 2,35,36,6
	.asciz "pos"

LDIFF_SYM319=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM319
	.byte 2,35,40,6
	.asciz "buffer_size"

LDIFF_SYM320=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM320
	.byte 2,35,44,6
	.asciz "do_checks"

LDIFF_SYM321=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM321
	.byte 2,35,48,6
	.asciz "mayBlock"

LDIFF_SYM322=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2,35,52,6
	.asciz "async_task"

LDIFF_SYM323=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,35,32,6
	.asciz "leave_open"

LDIFF_SYM324=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 2,35,53,6
	.asciz "foundCR"

LDIFF_SYM325=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 2,35,54,0,7
	.asciz "System_IO_StreamReader"

LDIFF_SYM326=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM327=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM327
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM328=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin.Touch.MyMusicPlayer/<GetTextResource>c__AnonStorey0:<>m__0"
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0
	.long Lme_17

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM329=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,123,16,3
	.asciz "stream"

LDIFF_SYM330=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM330
	.byte 2,123,20,11
	.asciz "V_0"

LDIFF_SYM331=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM332=Lfde4_end - Lfde4_start
	.long LDIFF_SYM332
Lfde4_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0

LDIFF_SYM333=Lme_17 - _OffSeasonPro_Plugin_MusicPlayerPlugin_Touch_MyMusicPlayer__GetTextResourcec__AnonStorey0__m__0_System_IO_Stream_0
	.long LDIFF_SYM333
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_47:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM334=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM334
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM335=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM335
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM336=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM336
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM337=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_22

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM338=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM338
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM339=Lfde5_end - Lfde5_start
	.long LDIFF_SYM339
Lfde5_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM340=Lme_22 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM340
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Array.cs"

	.byte 1,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,2,1,3,207,0,2,32,1,2,252,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
