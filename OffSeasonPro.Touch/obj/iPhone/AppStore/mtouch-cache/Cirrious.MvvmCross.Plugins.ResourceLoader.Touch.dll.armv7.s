	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream:
Leh_func_begin1:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r8, r10}
Ltmp2:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC1_1+8))
	mov	r6, r2
	mov	r4, r1
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC1_1+8))
LPC1_1:
	add	r10, pc, r10
	ldr	r0, [r10, #16]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r10, #20]
	mov	r1, r4
	str	r6, [r5, #8]
	bl	_p_2_plt_string_Concat_string_string_llvm
	mov	r4, r0
	ldr	r0, [r10, #24]
	mov	r8, r0
	bl	_p_3_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm
	mov	r6, r0
	cmp	r5, #0
	beq	LBB1_2
	ldr	r0, [r10, #28]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	str	r5, [r2, #16]
	ldr	r0, [r10, #32]
	str	r0, [r2, #20]
	ldr	r0, [r10, #36]
	str	r0, [r2, #28]
	ldr	r0, [r10, #40]
	str	r0, [r2, #12]
	ldr	r0, [r10, #44]
	ldr	r1, [r6]
	mov	r8, r0
	mov	r0, r6
	sub	r1, r1, #16
	ldr	r3, [r1]
	mov	r1, r4
	blx	r3
	tst	r0, #255
	popne	{r8, r10}
	popne	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC1_2+8))
	mov	r1, #11
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC1_2+8))
LPC1_2:
	ldr	r0, [pc, r0]
	bl	_p_5_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r5, r0
	ldr	r0, [r10, #48]
	mov	r1, #1
	bl	_p_6_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	mov	r1, #0
	mov	r2, r4
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r10, #52]
	bl	_p_1_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r6
	mov	r4, r0
	bl	_p_7_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm
	mov	r0, r4
	bl	_p_8_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp3:
LBB1_2:
	ldr	r0, LCPI1_0
LPC1_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI1_0:
	.long	Ltmp3-(LPC1_0+8)
	.end_data_region
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__ctor:
Leh_func_begin2:
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_Load
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_Load:
Leh_func_begin3:
	push	{r7, lr}
Ltmp4:
	mov	r7, sp
Ltmp5:
	push	{r8}
Ltmp6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC3_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC3_0+8))
LPC3_0:
	add	r0, pc, r0
	ldr	r0, [r0, #56]
	mov	r8, r0
	bl	_p_9_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxResourceLoader_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin__ctor:
Leh_func_begin4:
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__ctor:
Leh_func_begin5:
	bx	lr
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__GetResourceStreamb__0_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__GetResourceStreamb__0_System_IO_Stream:
Leh_func_begin6:
	push	{r7, lr}
Ltmp7:
	mov	r7, sp
Ltmp8:
Ltmp9:
	ldr	r0, [r0, #8]
	ldr	r2, [r0, #12]
	blx	r2
	mov	r0, #1
	pop	{r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin7:
	push	{r4, r5, r7, lr}
Ltmp10:
	add	r7, sp, #8
Ltmp11:
Ltmp12:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC7_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB7_2
	bl	_p_10_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB7_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB7_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB7_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB7_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB7_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin8:
	push	{r4, r5, r7, lr}
Ltmp13:
	add	r7, sp, #8
Ltmp14:
Ltmp15:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC8_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got-(LPC8_0+8))
LPC8_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB8_2
	bl	_p_10_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB8_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB8_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB8_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB8_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB8_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end8:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got,108,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_Load
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__GetResourceStreamb__0_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	9
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	12
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
Lset9 = Leh_func_end8-Leh_func_begin8
	.long	Lset9
Lset10 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset10
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_Load
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__GetResourceStreamb__0_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin_Load
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_Plugin__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader__c__DisplayClass1__GetResourceStreamb__0_System_IO_Stream
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 13,10,2,2
	.short 0, 18
	.byte 1,12,2,3,2,2,255,255,255,255,234,24,255,255,255,255,232,0,0,0,27
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,42,12,0,30,7
	.long 0,0,0,0,0,0,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 6,7,30,8,0,9,0,10
	.long 0,11,0,12,42
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 11, 4, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 3
	.short 0, 0, 0, 0, 0, 0, 0, 2
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 16,10,2,2
	.short 0, 10
	.byte 54,2,1,1,1,4,3,12,6,5,94,6,5,6,4,12
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 13,10,2,2
	.short 0, 20
	.byte 129,84,3,3,3,3,3,255,255,255,254,157,129,102,255,255,255,254,154,0,0,0,129,105
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 4,10,1,2
	.short 0
	.byte 129,108,7,35,19

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_plt:
_p_1_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 64,128
_p_2_plt_string_Concat_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string
plt_string_Concat_string_string:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 68,151
_p_3_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 72,154
_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 76,166
_p_5_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 80,211
_p_6_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 84,231
_p_7_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 88,257
_p_8_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 92,262
_p_9_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxResourceLoader_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxResourceLoader_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader
plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxResourceLoader_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 96,290
_p_10_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got - . + 100,302
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.Touch"
	.asciz "C09C5BA7-06D7-425A-A9FB-FDFC91512D74"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.File"
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader"
	.asciz "D6FEFB8F-01C7-4230-ACE4-ABE563457664"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "C09C5BA7-06D7-425A-A9FB-FDFC91512D74"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_ResourceLoader_Touch__Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_MvxTouchResourceLoader_GetResourceStream_string_System_Action_1_System_IO_Stream
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 16,108,11,13,11,387000831,0,440
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_ResourceLoader_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,10,4,13,12,11,10,9,8,7,6,5,0,0,0,1,14,0,0,0,0,0,0,0,1,15,0,1,15,255,252
	.byte 0,0,0,1,1,3,219,0,0,1,255,252,0,0,0,1,1,3,219,0,0,2,12,1,39,42,47,14,2,4,1,17
	.byte 1,1,34,255,254,0,0,0,1,255,43,0,0,1,14,3,219,0,0,2,6,193,0,0,6,50,193,0,0,6,30,3
	.byte 219,0,0,2,6,195,0,0,3,14,6,1,1,130,123,14,2,30,2,34,255,254,0,0,0,1,255,43,0,0,2,33
	.byte 7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,147,133,3,255,254,0,0,0
	.byte 1,255,43,0,0,1,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116
	.byte 105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,7,17,109,111,110,111,95,104,101,108,112,101,114
	.byte 95,108,100,115,116,114,0,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99
	.byte 0,3,194,0,0,78,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111
	.byte 110,0,3,255,254,0,0,0,1,255,43,0,0,2,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101
	.byte 114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,9,128,152,8,0,0,1,146,178,146,175,146,174
	.byte 146,172,196,0,0,6,196,0,0,4,193,0,0,1,196,0,0,6,193,0,0,1,5,128,144,8,0,0,1,146,178,146
	.byte 175,146,174,146,172,193,0,0,3,4,128,160,12,0,0,4,146,178,146,175,146,174,146,172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
