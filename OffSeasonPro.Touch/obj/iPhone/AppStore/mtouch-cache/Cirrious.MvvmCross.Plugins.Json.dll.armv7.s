	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type:
Leh_func_begin1:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC1_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC1_0+8))
LPC1_0:
	add	r0, pc, r0
	ldr	r2, [r0, #16]
	mov	r0, #0
	cmp	r1, r2
	moveq	r0, #1
	bx	lr
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer:
Leh_func_begin2:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
	push	{r10, r11}
Ltmp2:
	sub	sp, sp, #12
	bic	sp, sp, #7
	mov	r0, #0
	mov	r4, r1
	str	r0, [sp, #4]
	str	r0, [sp]
	ldr	r1, [r2]
	ldrb	r0, [r1, #22]
	cmp	r0, #0
	bne	LBB2_3
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC2_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC2_2+8))
	ldr	r1, [r1]
LPC2_2:
	add	r0, pc, r0
	ldr	r3, [r0, #20]
	ldr	r1, [r1]
	cmp	r1, r3
	bne	LBB2_4
	ldr	r1, [r2, #8]
	ldr	r2, [r2, #12]
	stm	sp, {r1, r2}
	ldrd	r10, r11, [r0, #24]
	mov	r0, sp
	ldr	r2, [r11]
	mov	r1, r10
	bl	_p_2_plt_System_DateTime_ToString_string_System_IFormatProvider_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #208]
	mov	r0, r4
	blx	r2
	sub	sp, r7, #12
	pop	{r10, r11}
	pop	{r4, r7, pc}
Ltmp3:
LBB2_3:
	ldr	r0, LCPI2_0
LPC2_0:
	add	r1, pc, r0
	b	LBB2_5
Ltmp4:
LBB2_4:
	ldr	r0, LCPI2_1
LPC2_1:
	add	r1, pc, r0
LBB2_5:
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI2_0:
	.long	Ltmp3-(LPC2_0+8)
LCPI2_1:
	.long	Ltmp4-(LPC2_1+8)
	.end_data_region
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer:
Leh_func_begin3:
	push	{r4, r7, lr}
Ltmp5:
	add	r7, sp, #4
Ltmp6:
Ltmp7:
	sub	sp, sp, #12
	bic	sp, sp, #7
	ldr	r0, [r1]
	ldr	r2, [r0, #104]
	mov	r0, r1
	blx	r2
	ldr	r1, [r0]
	ldr	r1, [r1, #32]
	blx	r1
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC3_0+8))
	mov	r1, sp
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC3_0+8))
LPC3_0:
	add	r4, pc, r4
	ldrd	r2, r3, [r4, #24]
	ldr	r3, [r3]
	bl	_p_3_plt_System_DateTime_ParseExact_string_string_System_IFormatProvider_llvm
	ldr	r0, [r4, #32]
	bl	_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	ldr	r1, [sp]
	str	r1, [r0, #8]
	ldr	r1, [sp, #4]
	str	r1, [r0, #12]
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter__ctor:
Leh_func_begin4:
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_CanConvert_System_Type
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_CanConvert_System_Type:
Leh_func_begin5:
	push	{r7, lr}
Ltmp8:
	mov	r7, sp
Ltmp9:
Ltmp10:
	ldr	r0, [r1]
	ldr	r2, [r0, #272]
	mov	r0, r1
	blx	r2
	pop	{r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer:
Leh_func_begin6:
	push	{r4, r7, lr}
Ltmp11:
	add	r7, sp, #4
Ltmp12:
Ltmp13:
	ldr	r0, [r2]
	mov	r4, r1
	ldr	r1, [r0, #32]
	mov	r0, r2
	blx	r1
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #208]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer:
Leh_func_begin7:
	push	{r4, r7, lr}
Ltmp14:
	add	r7, sp, #4
Ltmp15:
Ltmp16:
	ldr	r0, [r1]
	mov	r4, r2
	ldr	r2, [r0, #104]
	mov	r0, r1
	blx	r2
	ldr	r1, [r0]
	ldr	r1, [r1, #32]
	blx	r1
	mov	r1, r0
	mov	r0, r4
	mov	r2, #0
	bl	_p_5_plt_System_Enum_Parse_System_Type_string_bool_llvm
	pop	{r4, r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter__ctor:
Leh_func_begin8:
	bx	lr
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__ctor:
Leh_func_begin9:
	mov	r1, #1
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_get_RegisterAsTextSerializer
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_get_RegisterAsTextSerializer:
Leh_func_begin10:
	ldrb	r0, [r0, #8]
	bx	lr
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_set_RegisterAsTextSerializer_bool
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_set_RegisterAsTextSerializer_bool:
Leh_func_begin11:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__cctor:
Leh_func_begin12:
	push	{r4, r7, lr}
Ltmp17:
	add	r7, sp, #4
Ltmp18:
Ltmp19:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC12_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC12_0+8))
LPC12_0:
	add	r4, pc, r4
	ldr	r0, [r4, #36]
	bl	_p_6_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, #1
	strb	r1, [r0, #8]
	ldr	r1, [r4, #40]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__cctor:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp20:
	add	r7, sp, #12
Ltmp21:
Ltmp22:
	bl	_p_7_plt__class_init_Newtonsoft_Json_JsonSerializerSettings_llvm
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC13_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC13_0+8))
LPC13_0:
	add	r6, pc, r6
	ldr	r0, [r6, #44]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_9_plt_Newtonsoft_Json_JsonSerializerSettings__ctor_llvm
	ldr	r0, [r6, #48]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	ldr	r0, [r6, #52]
	ldr	r0, [r0]
	str	r0, [r5, #8]
	ldr	r0, [r6, #56]
	bl	_p_6_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_10_plt_System_Collections_Generic_List_1_Newtonsoft_Json_JsonConverter_Add_Newtonsoft_Json_JsonConverter_llvm
	mov	r0, r4
	mov	r1, r5
	bl	_p_11_plt_Newtonsoft_Json_JsonSerializerSettings_set_Converters_System_Collections_Generic_IList_1_Newtonsoft_Json_JsonConverter_llvm
	mov	r0, r4
	mov	r1, #0
	bl	_p_12_plt_Newtonsoft_Json_JsonSerializerSettings_set_DateFormatHandling_Newtonsoft_Json_DateFormatHandling_llvm
	ldr	r0, [r6, #60]
	str	r4, [r0]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_T_string
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_T_string:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp23:
	add	r7, sp, #8
Ltmp24:
	push	{r8}
Ltmp25:
	sub	sp, sp, #4
	str	r8, [sp]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC14_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC14_0+8))
LPC14_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r5, [r0]
	mov	r0, r8
	bl	_p_13_plt__rgctx_fetch_0_llvm
	mov	r8, r0
	mov	r0, r4
	mov	r1, r5
	bl	_p_14_plt_Newtonsoft_Json_JsonConvert_DeserializeObject_T_string_Newtonsoft_Json_JsonSerializerSettings_llvm
	sub	sp, r7, #12
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_SerializeObject_object
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_SerializeObject_object:
Leh_func_begin15:
	push	{r7, lr}
Ltmp26:
	mov	r7, sp
Ltmp27:
Ltmp28:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC15_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC15_0+8))
LPC15_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r2, [r0]
	mov	r0, r1
	mov	r1, #0
	bl	_p_15_plt_Newtonsoft_Json_JsonConvert_SerializeObject_object_Newtonsoft_Json_Formatting_Newtonsoft_Json_JsonSerializerSettings_llvm
	pop	{r7, pc}
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_System_Type_string
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_System_Type_string:
Leh_func_begin16:
	push	{r7, lr}
Ltmp29:
	mov	r7, sp
Ltmp30:
Ltmp31:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC16_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC16_0+8))
LPC16_0:
	add	r0, pc, r0
	ldr	r0, [r0, #60]
	ldr	r3, [r0]
	mov	r0, r2
	mov	r2, r3
	bl	_p_16_plt_Newtonsoft_Json_JsonConvert_DeserializeObject_string_System_Type_Newtonsoft_Json_JsonSerializerSettings_llvm
	pop	{r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__ctor:
Leh_func_begin17:
	bx	lr
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_EnsureLoaded
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_EnsureLoaded:
Leh_func_begin18:
	push	{r4, r5, r7, lr}
Ltmp32:
	add	r7, sp, #8
Ltmp33:
	push	{r8}
Ltmp34:
	mov	r4, r0
	ldrb	r0, [r4, #12]
	cmp	r0, #0
	bne	LBB18_3
	mov	r0, #1
	strb	r0, [r4, #12]
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC18_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC18_0+8))
LPC18_0:
	add	r5, pc, r5
	ldr	r0, [r5, #64]
	mov	r8, r0
	bl	_p_17_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxJsonConverter_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_llvm
	ldr	r0, [r4, #8]
	cmp	r0, #0
	ldreq	r0, [r5, #40]
	ldreq	r0, [r0]
	ldr	r1, [r0]
	ldrb	r0, [r0, #8]
	cmp	r0, #0
	beq	LBB18_3
	ldr	r0, [r5, #68]
	mov	r8, r0
	bl	_p_18_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxTextSerializer_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_llvm
LBB18_3:
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration:
Leh_func_begin19:
	push	{r4, r5, r6, r7, lr}
Ltmp35:
	add	r7, sp, #12
Ltmp36:
	push	{r10}
Ltmp37:
	mov	r4, r1
	ldrb	r1, [r0, #12]
	cmp	r1, #0
	beq	LBB19_2
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_1+8))
LPC19_1:
	add	r1, pc, r1
	ldr	r0, [r1, #76]
	ldr	r4, [r1, #84]
	mov	r1, #0
	bl	_p_20_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_23_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB19_2:
	cmp	r4, #0
	beq	LBB19_5
	beq	LBB19_5
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_3+8))
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_3+8))
	ldr	r2, [r4]
LPC19_3:
	add	r10, pc, r10
	ldr	r1, [r10, #72]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	bne	LBB19_8
LBB19_5:
	cmp	r4, #0
	beq	LBB19_7
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_2+8))
	ldr	r2, [r4]
LPC19_2:
	add	r1, pc, r1
	ldr	r1, [r1, #72]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	bne	LBB19_9
LBB19_7:
	str	r4, [r0, #8]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB19_8:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_4+8))
	mov	r1, #196
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC19_4+8))
LPC19_4:
	ldr	r0, [pc, r0]
	bl	_p_19_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r5, r0
	ldr	r0, [r10, #76]
	mov	r1, #1
	bl	_p_20_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r0, [r4]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r6]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r10, #80]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r6
	mov	r4, r0
	bl	_p_21_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm
	mov	r0, r4
	bl	_p_22_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp38:
LBB19_9:
	ldr	r0, LCPI19_0
LPC19_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI19_0:
	.long	Ltmp38-(LPC19_0+8)
	.end_data_region
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__ctor:
Leh_func_begin20:
	bx	lr
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__cctor:
Leh_func_begin21:
	push	{r4, r7, lr}
Ltmp39:
	add	r7, sp, #4
Ltmp40:
Ltmp41:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC21_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Json_got-(LPC21_0+8))
LPC21_0:
	add	r4, pc, r4
	ldr	r0, [r4, #88]
	bl	_p_8_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #92]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end21:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_Json_got,204,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_CanConvert_System_Type
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_get_RegisterAsTextSerializer
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_set_RegisterAsTextSerializer_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_T_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_SerializeObject_object
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_System_Type_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_EnsureLoaded
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__cctor
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_Json_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	22
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
Lset22 = Leh_func_end21-Leh_func_begin21
	.long	Lset22
Lset23 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset23
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	4
	.byte	138
	.byte	5

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.Json.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string
_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string:

	.byte 128,64,45,233,13,112,160,225,48,9,45,233,24,208,77,226,13,176,160,225,4,128,139,229,0,16,139,229,8,0,139,229
	.byte 12,32,139,229,4,0,155,229
bl _p_26

	.byte 0,80,160,225,0,0,149,229,7,64,128,226,7,64,196,227,4,208,77,224,0,64,141,226,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 48
	.byte 0,0,159,231,0,0,144,229,20,0,139,229,4,0,155,229
bl _p_25

	.byte 16,0,139,229,4,0,155,229
bl _p_24

	.byte 0,48,160,225,16,0,155,229,20,32,155,229,0,128,160,225,12,16,149,229,4,0,160,225,1,16,128,224,12,0,155,229
	.byte 51,255,47,225,12,16,149,229,4,0,160,225,1,16,128,224,0,0,155,229,4,32,149,229,8,48,149,229,51,255,47,225
	.byte 24,208,139,226,48,9,189,232,128,128,189,232

Lme_16:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_CanConvert_System_Type
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_get_RegisterAsTextSerializer
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_set_RegisterAsTextSerializer_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_T_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_SerializeObject_object
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_System_Type_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_EnsureLoaded
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__cctor

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter__ctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_CanConvert_System_Type
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_WriteJson_Newtonsoft_Json_JsonWriter_object_Newtonsoft_Json_JsonSerializer
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter_ReadJson_Newtonsoft_Json_JsonReader_System_Type_object_Newtonsoft_Json_JsonSerializer
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxEnumJsonConverter__ctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__ctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_get_RegisterAsTextSerializer
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration_set_RegisterAsTextSerializer_bool
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConfiguration__cctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__cctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_T_string
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_SerializeObject_object
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject_System_Type_string
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter__ctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_EnsureLoaded
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_PluginLoader__cctor
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 23,10,3,2
	.short 0, 10, 20
	.byte 1,3,5,5,2,2,2,2,2,3,30,3,5,8,4,4,4,3,6,9,79,255,255,255,255,177,84
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,93,22
	.long 0,0,0,0,0,0,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 1,22,93
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 0
	.short 0, 4, 0, 0, 0, 5, 0, 2
	.short 11, 0, 0, 0, 0, 6, 0, 3
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 24,10,3,2
	.short 0, 10, 21
	.byte 109,2,1,1,1,7,5,3,7,5,128,144,4,4,15,6,3,4,12,12,3,128,214,4,3,3
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 23,10,3,2
	.short 0, 11, 22
	.byte 130,183,3,3,3,3,3,3,3,3,3,130,213,3,3,3,23,3,3,3,3,3,131,7,255,255,255,252,249,131,10
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 27,12,13,0,72,14,8,135,2,68,14,24,132,6,133,5,136,4,139,3,142,1,68,14,48,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 6,10,1,2
	.short 0
	.byte 131,38,7,38,38,24,32

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_Json_plt:
_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 96,228
_p_2_plt_System_DateTime_ToString_string_System_IFormatProvider_llvm:
	.no_dead_strip plt_System_DateTime_ToString_string_System_IFormatProvider
plt_System_DateTime_ToString_string_System_IFormatProvider:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 100,273
_p_3_plt_System_DateTime_ParseExact_string_string_System_IFormatProvider_llvm:
	.no_dead_strip plt_System_DateTime_ParseExact_string_string_System_IFormatProvider
plt_System_DateTime_ParseExact_string_string_System_IFormatProvider:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 104,278
_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 108,283
_p_5_plt_System_Enum_Parse_System_Type_string_bool_llvm:
	.no_dead_strip plt_System_Enum_Parse_System_Type_string_bool
plt_System_Enum_Parse_System_Type_string_bool:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 112,313
_p_6_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 116,318
_p_7_plt__class_init_Newtonsoft_Json_JsonSerializerSettings_llvm:
	.no_dead_strip plt__class_init_Newtonsoft_Json_JsonSerializerSettings
plt__class_init_Newtonsoft_Json_JsonSerializerSettings:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 120,344
_p_8_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 124,348
_p_9_plt_Newtonsoft_Json_JsonSerializerSettings__ctor_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonSerializerSettings__ctor
plt_Newtonsoft_Json_JsonSerializerSettings__ctor:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 128,371
_p_10_plt_System_Collections_Generic_List_1_Newtonsoft_Json_JsonConverter_Add_Newtonsoft_Json_JsonConverter_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_Newtonsoft_Json_JsonConverter_Add_Newtonsoft_Json_JsonConverter
plt_System_Collections_Generic_List_1_Newtonsoft_Json_JsonConverter_Add_Newtonsoft_Json_JsonConverter:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 132,376
_p_11_plt_Newtonsoft_Json_JsonSerializerSettings_set_Converters_System_Collections_Generic_IList_1_Newtonsoft_Json_JsonConverter_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonSerializerSettings_set_Converters_System_Collections_Generic_IList_1_Newtonsoft_Json_JsonConverter
plt_Newtonsoft_Json_JsonSerializerSettings_set_Converters_System_Collections_Generic_IList_1_Newtonsoft_Json_JsonConverter:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 136,387
_p_12_plt_Newtonsoft_Json_JsonSerializerSettings_set_DateFormatHandling_Newtonsoft_Json_DateFormatHandling_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonSerializerSettings_set_DateFormatHandling_Newtonsoft_Json_DateFormatHandling
plt_Newtonsoft_Json_JsonSerializerSettings_set_DateFormatHandling_Newtonsoft_Json_DateFormatHandling:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 140,392
_p_13_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 144,429
_p_14_plt_Newtonsoft_Json_JsonConvert_DeserializeObject_T_string_Newtonsoft_Json_JsonSerializerSettings_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonConvert_DeserializeObject_T_string_Newtonsoft_Json_JsonSerializerSettings
plt_Newtonsoft_Json_JsonConvert_DeserializeObject_T_string_Newtonsoft_Json_JsonSerializerSettings:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 148,452
_p_15_plt_Newtonsoft_Json_JsonConvert_SerializeObject_object_Newtonsoft_Json_Formatting_Newtonsoft_Json_JsonSerializerSettings_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonConvert_SerializeObject_object_Newtonsoft_Json_Formatting_Newtonsoft_Json_JsonSerializerSettings
plt_Newtonsoft_Json_JsonConvert_SerializeObject_object_Newtonsoft_Json_Formatting_Newtonsoft_Json_JsonSerializerSettings:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 152,471
_p_16_plt_Newtonsoft_Json_JsonConvert_DeserializeObject_string_System_Type_Newtonsoft_Json_JsonSerializerSettings_llvm:
	.no_dead_strip plt_Newtonsoft_Json_JsonConvert_DeserializeObject_string_System_Type_Newtonsoft_Json_JsonSerializerSettings
plt_Newtonsoft_Json_JsonConvert_DeserializeObject_string_System_Type_Newtonsoft_Json_JsonSerializerSettings:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 156,476
_p_17_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxJsonConverter_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxJsonConverter_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter
plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxJsonConverter_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 160,481
_p_18_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxTextSerializer_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxTextSerializer_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter
plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxTextSerializer_Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 164,493
_p_19_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 168,505
_p_20_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 172,525
_p_21_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 176,551
_p_22_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 180,556
_p_23_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 184,584
_p_24_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 188,605
_p_25_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 192,640
_p_26_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got - . + 196,662
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 4
	.asciz "Cirrious.MvvmCross.Plugins.Json"
	.asciz "628627EE-844B-4D38-8CC2-C669C83B29D0"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Newtonsoft.Json"
	.asciz "6CC85419-D05F-426F-8892-531CB54DBB17"
	.asciz ""
	.asciz "30ad4fe6b2a6aeed"
	.align 3

	.long 1,4,5,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "628627EE-844B-4D38-8CC2-C669C83B29D0"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.Json"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Json_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_Json__Cirrious_MvvmCross_Plugins_Json_MvxDateTimeJsonConverter_CanConvert_System_Type
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 24,204,27,23,11,387000831,0,971
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_Json_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_Json_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,1,4,0,3,7,6,5,0,3,8,7,6,0,0,0,0,0,0,0,0,0,0,1,4,0,1,4,0,1,4
	.byte 0,1,4,2,10,9,1,5,5,14,13,12,11,15,1,5,1,15,1,5,1,15,1,5,1,15,1,5,0,1,6,3
	.byte 10,17,16,1,6,6,18,19,21,18,20,19,1,6,0,1,6,2,23,22,1,5,1,15,5,30,0,0,1,255,253,0
	.byte 0,0,1,5,0,198,0,0,14,0,1,7,88,12,0,39,42,47,19,0,193,0,0,28,0,11,2,130,38,1,17,0
	.byte 1,16,2,128,184,1,130,35,14,2,130,38,1,14,1,4,16,1,4,2,14,2,55,2,14,3,219,0,0,1,4,2
	.byte 130,50,1,1,2,22,2,16,7,128,158,135,229,14,1,3,16,1,5,4,34,255,254,0,0,0,0,255,43,0,0,2
	.byte 34,255,254,0,0,0,0,255,43,0,0,3,11,1,4,14,6,1,2,130,123,1,14,2,30,3,17,0,43,14,1,6
	.byte 16,1,6,5,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111
	.byte 110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,3,193,0,15,138,3,193,0,15,109,7,27,109,111,110
	.byte 111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,193,0,16,153,7,23
	.byte 109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0,15,2,55,2,7,20,109,111
	.byte 110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,194,0,2,64,3,255,254,0,0,0,0,202
	.byte 0,0,29,3,194,0,2,32,3,194,0,2,56,5,30,0,1,255,255,255,255,255,255,251,0,0,0,14,255,253,0,0
	.byte 0,1,5,0,198,0,0,14,0,1,7,129,141,35,129,156,140,17,255,253,0,0,0,2,42,2,2,198,0,1,156,0
	.byte 1,7,129,141,3,255,253,0,0,0,2,42,2,2,198,0,1,156,0,1,7,129,141,3,194,0,1,149,3,194,0,1
	.byte 158,3,255,254,0,0,0,0,255,43,0,0,2,3,255,254,0,0,0,0,255,43,0,0,3,7,17,109,111,110,111,95
	.byte 104,101,108,112,101,114,95,108,100,115,116,114,0,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112
	.byte 101,99,105,102,105,99,0,3,195,0,0,78,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120
	.byte 99,101,112,116,105,111,110,0,3,195,0,1,125,255,253,0,0,0,1,5,0,198,0,0,14,0,1,7,88,35,130,77
	.byte 192,0,90,33,16,1,2,30,7,88,14,18,2,55,2,255,253,0,0,0,2,42,2,2,198,0,1,156,0,1,7,88
	.byte 35,130,77,140,17,255,253,0,0,0,2,42,2,2,198,0,1,156,0,1,7,88,35,130,77,192,0,92,41,255,253,0
	.byte 0,0,1,5,0,198,0,0,14,0,1,7,88,3,14,7,88,22,7,88,21,7,88,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,17,0
	.byte 0,17,255,253,0,0,0,1,5,0,198,0,0,14,0,1,7,129,141,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,3,0,0,1,11,4,16,255,253,0,0,0,1,5,0,198,0,0,14,0,1,7
	.byte 88,1,0,1,1,0,0,128,144,8,0,0,1,10,128,192,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174
	.byte 193,0,18,172,194,0,1,24,194,0,1,23,194,0,1,22,1,3,2,10,128,144,8,0,0,1,193,0,18,178,193,0
	.byte 18,175,193,0,18,174,193,0,18,172,194,0,1,24,194,0,1,23,194,0,1,22,5,7,6,4,128,196,12,9,4,0
	.byte 1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,7,128,196,13,8,4,0,1,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,255,251,0,0,0,14,15,16,6,128,228,21,16,4,0,4,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,19,18,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter"

LDIFF_SYM7=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM9
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.Json.MvxJsonConverter:DeserializeObject<!!0>"
	.long _Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string
	.long Lme_16

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM10=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,123,8,3
	.asciz "inputText"

LDIFF_SYM11=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM12=Lfde0_end - Lfde0_start
	.long LDIFF_SYM12
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string

LDIFF_SYM13=Lme_16 - _Cirrious_MvvmCross_Plugins_Json_MvxJsonConverter_DeserializeObject___0_string
	.long LDIFF_SYM13
	.byte 12,13,0,72,14,8,135,2,68,14,24,132,6,133,5,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
