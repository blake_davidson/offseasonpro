	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object:
Leh_func_begin1:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r10}
Ltmp2:
	mov	r5, r1
	mov	r4, r0
	cmp	r5, #0
	beq	LBB1_2
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC1_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC1_0+8))
	ldr	r1, [r5]
LPC1_0:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #4]
	cmp	r1, r0
	movne	r5, #0
LBB1_2:
	mov	r10, #0
	cmp	r5, #0
	beq	LBB1_5
	ldr	r0, [r5]
	ldr	r1, [r0, #52]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #52]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r6
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB1_5
	ldr	r0, [r5]
	ldr	r1, [r0, #48]
	mov	r0, r5
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #48]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, r5
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	uxtb	r10, r0
LBB1_5:
	mov	r0, r10
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end1:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_GetHashCode
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_GetHashCode:
Leh_func_begin2:
	push	{r4, r5, r7, lr}
Ltmp3:
	add	r7, sp, #8
Ltmp4:
Ltmp5:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #52]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #48]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	add	r0, r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end2:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader:
Leh_func_begin3:
	push	{r4, r7, lr}
Ltmp6:
	add	r7, sp, #4
Ltmp7:
Ltmp8:
	mov	r4, r0
	mov	r0, r1
	bl	_p_2_plt_System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_3_plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IList_1_System_ServiceModel_Channels_AddressHeader_llvm
	pop	{r4, r7, pc}
Leh_func_end3:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader:
Leh_func_begin4:
	push	{r4, r5, r7, lr}
Ltmp9:
	add	r7, sp, #8
Ltmp10:
Ltmp11:
	mov	r4, r0
	mov	r5, #0
	cmp	r4, #0
	beq	LBB4_3
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC4_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC4_0+8))
	ldr	r1, [r4]
LPC4_0:
	add	r0, pc, r0
	ldr	r0, [r0, #24]
	ldrh	r2, [r1, #20]
	cmp	r2, r0
	blo	LBB4_3
	ldr	r1, [r1, #16]
	mov	r2, #1
	ldrb	r1, [r1, r0, asr #3]
	and	r0, r0, #7
	and	r5, r1, r2, lsl r0
	cmp	r5, #0
	movne	r5, r4
LBB4_3:
	cmp	r5, #0
	bne	LBB4_5
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC4_1+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC4_1+8))
LPC4_1:
	add	r0, pc, r0
	ldr	r0, [r0, #20]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_5_plt_System_Collections_Generic_List_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm
LBB4_5:
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end4:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__cctor
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__cctor:
Leh_func_begin5:
	push	{r4, r7, lr}
Ltmp12:
	add	r7, sp, #4
Ltmp13:
Ltmp14:
	movw	r4, :lower16:(_mono_aot_System_ServiceModel_got-(LPC5_0+8))
	mov	r1, #0
	movt	r4, :upper16:(_mono_aot_System_ServiceModel_got-(LPC5_0+8))
LPC5_0:
	add	r4, pc, r4
	ldr	r0, [r4, #28]
	bl	_p_6_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r4, #32]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end5:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string:
Leh_func_begin6:
	add	r9, r0, #8
	stm	r9, {r1, r2, r3}
	bx	lr
Leh_func_end6:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressing10
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressing10:
Leh_func_begin7:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC7_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	ldr	r0, [r0]
	bx	lr
Leh_func_end7:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressingAugust2004
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressingAugust2004:
Leh_func_begin8:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC8_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC8_0+8))
LPC8_0:
	add	r0, pc, r0
	ldr	r0, [r0, #40]
	ldr	r0, [r0]
	bx	lr
Leh_func_end8:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_None
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_None:
Leh_func_begin9:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC9_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC9_0+8))
LPC9_0:
	add	r0, pc, r0
	ldr	r0, [r0, #44]
	ldr	r0, [r0]
	bx	lr
Leh_func_end9:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_Namespace
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_Namespace:
Leh_func_begin10:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end10:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_ToString
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_ToString:
Leh_func_begin11:
	push	{r4, r7, lr}
Ltmp15:
	add	r7, sp, #4
Ltmp16:
Ltmp17:
	mov	r1, r0
	ldr	r0, [r1, #8]
	movw	r2, :lower16:(_mono_aot_System_ServiceModel_got-(LPC11_0+8))
	movt	r2, :upper16:(_mono_aot_System_ServiceModel_got-(LPC11_0+8))
LPC11_0:
	add	r2, pc, r2
	ldr	r4, [r2, #48]
	ldr	r3, [r2, #52]
	ldr	r2, [r1, #12]
	mov	r1, r4
	bl	_p_7_plt_string_Concat_string_string_string_string_llvm
	pop	{r4, r7, pc}
Leh_func_end11:

	.private_extern	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__cctor
	.align	2
_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__cctor:
Leh_func_begin12:
	push	{r4, r5, r6, r7, lr}
Ltmp18:
	add	r7, sp, #12
Ltmp19:
	push	{r10, r11}
Ltmp20:
	sub	sp, sp, #4
	movw	r6, :lower16:(_mono_aot_System_ServiceModel_got-(LPC12_0+8))
	movt	r6, :upper16:(_mono_aot_System_ServiceModel_got-(LPC12_0+8))
LPC12_0:
	add	r6, pc, r6
	ldr	r0, [r6, #60]
	ldr	r4, [r6, #68]
	ldr	r11, [r6, #56]
	ldr	r10, [r6, #64]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r2, [sp]
	mov	r1, r11
	mov	r3, r10
	mov	r5, r0
	bl	_p_8_plt_System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string_llvm
	ldr	r0, [r6, #40]
	str	r5, [r0]
	ldr	r0, [r6, #76]
	ldr	r11, [r6, #72]
	ldr	r10, [r6, #80]
	str	r0, [sp]
	mov	r0, r4
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r2, [sp]
	mov	r1, r11
	mov	r3, r10
	mov	r5, r0
	bl	_p_8_plt_System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string_llvm
	ldr	r0, [r6, #36]
	str	r5, [r0]
	mov	r0, r4
	ldr	r10, [r6, #84]
	ldr	r5, [r6, #88]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r2, r5
	mov	r3, #0
	mov	r4, r0
	bl	_p_8_plt_System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string_llvm
	ldr	r0, [r6, #44]
	str	r4, [r0]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end12:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp21:
	add	r7, sp, #12
Ltmp22:
	push	{r10, r11}
Ltmp23:
	sub	sp, sp, #8
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC13_0+8))
	mov	r5, r3
	mov	r10, r2
	mov	r11, r1
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r0, [r0, #92]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r4, r0
	bl	_p_9_plt_System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm
	mov	r0, #0
	mov	r1, r11
	mov	r2, r10
	mov	r3, r4
	str	r0, [sp]
	str	r0, [sp, #4]
	mov	r0, r6
	bl	_p_10_plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader_llvm
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end13:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader:
Leh_func_begin14:
	push	{r4, r5, r6, r7, lr}
Ltmp24:
	add	r7, sp, #12
Ltmp25:
	push	{r10}
Ltmp26:
	mov	r5, r1
	mov	r4, r0
	mov	r1, #0
	mov	r6, r3
	mov	r10, r2
	mov	r0, r5
	bl	_p_11_plt_System_Uri_op_Equality_System_Uri_System_Uri_llvm
	tst	r0, #255
	bne	LBB14_3
	ldr	r0, [r5]
	ldrb	r0, [r5, #62]
	cmp	r0, #0
	ldrne	r0, [r7, #12]
	ldrne	r1, [r7, #8]
	addne	r2, r4, #8
	stmne	r2, {r5, r6, r10}
	strne	r1, [r4, #20]
	strne	r0, [r4, #24]
	popne	{r10}
	popne	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC14_0+8))
	movw	r1, #589
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC14_0+8))
LPC14_0:
	ldr	r0, [pc, r0]
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
LBB14_2:
	movt	r0, #512
	bl	_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
LBB14_3:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC14_1+8))
	movw	r1, #581
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC14_1+8))
LPC14_1:
	ldr	r0, [pc, r0]
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	b	LBB14_2
Leh_func_end14:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Headers
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Headers:
Leh_func_begin15:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end15:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Uri
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Uri:
Leh_func_begin16:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end16:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_GetHashCode
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_GetHashCode:
Leh_func_begin17:
	push	{r7, lr}
Ltmp27:
	mov	r7, sp
Ltmp28:
Ltmp29:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #36]
	blx	r1
	pop	{r7, pc}
Leh_func_end17:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress:
Leh_func_begin18:
	push	{r7, lr}
Ltmp30:
	mov	r7, sp
Ltmp31:
Ltmp32:
	mov	r2, r0
	mov	r0, #0
	cmp	r2, #0
	beq	LBB18_2
	cmp	r1, #0
	popeq	{r7, pc}
	ldr	r0, [r2]
	ldr	r3, [r0, #44]
	mov	r0, r2
	blx	r3
	uxtb	r0, r0
	pop	{r7, pc}
LBB18_2:
	cmp	r1, #0
	moveq	r0, #1
	pop	{r7, pc}
Leh_func_end18:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader:
Leh_func_begin19:
	push	{r7, lr}
Ltmp33:
	mov	r7, sp
Ltmp34:
Ltmp35:
	sub	sp, sp, #8
	cmp	r0, #0
	beq	LBB19_3
	cmp	r1, #0
	beq	LBB19_4
	mov	r2, #0
	mov	r3, #0
	str	r2, [sp]
	str	r2, [sp, #4]
	mov	r2, #0
	bl	_p_15_plt_System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString_llvm
	mov	sp, r7
	pop	{r7, pc}
LBB19_3:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC19_0+8))
	movw	r1, #657
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC19_0+8))
LPC19_0:
	ldr	r0, [pc, r0]
	b	LBB19_5
LBB19_4:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC19_1+8))
	movw	r1, #693
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC19_1+8))
LPC19_1:
	ldr	r0, [pc, r0]
LBB19_5:
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end19:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString:
Leh_func_begin20:
	push	{r4, r5, r6, r7, lr}
Ltmp36:
	add	r7, sp, #12
Ltmp37:
	push	{r10}
Ltmp38:
	mov	r4, r1
	mov	r5, r0
	mov	r10, r3
	mov	r6, r2
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	cmp	r0, #1
	bne	LBB20_14
	ldr	r0, [r4]
	ldr	r1, [r0, #212]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	bne	LBB20_14
	cmp	r6, #0
	beq	LBB20_4
	ldr	r0, [r4]
	mov	r1, r6
	mov	r2, r10
	ldr	r3, [r0, #72]
	mov	r0, r4
	blx	r3
	b	LBB20_8
LBB20_4:
	ldr	r1, [r7, #8]
	cmp	r1, #0
	beq	LBB20_7
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC20_4+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC20_4+8))
	ldr	r2, [r4]
LPC20_4:
	add	r0, pc, r0
	ldr	r0, [r0, #96]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	bne	LBB20_15
	ldr	r0, [r4]
	ldr	r2, [r7, #12]
	ldr	r3, [r0, #240]
	mov	r0, r4
	blx	r3
	b	LBB20_8
LBB20_7:
	ldr	r0, [r4]
	ldr	r1, [r0, #76]
	mov	r0, r4
	blx	r1
LBB20_8:
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	cmp	r5, #0
	bne	LBB20_13
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	movw	r5, :lower16:(_mono_aot_System_ServiceModel_got-(LPC20_2+8))
	movt	r5, :upper16:(_mono_aot_System_ServiceModel_got-(LPC20_2+8))
LPC20_2:
	add	r5, pc, r5
	ldr	r6, [r5, #36]
	ldr	r1, [r6]
	ldr	r2, [r1]
	ldr	r1, [r1, #12]
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB20_11
	ldr	r5, [r6]
	b	LBB20_13
LBB20_11:
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	ldr	r1, [r5, #40]
	ldr	r1, [r1]
	ldr	r2, [r1]
	ldr	r1, [r1, #12]
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB20_16
	ldr	r0, [r5, #40]
	ldr	r5, [r0]
LBB20_13:
	mov	r0, r5
	mov	r1, r4
	bl	_p_16_plt_System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_llvm
	mov	r5, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #84]
	mov	r0, r4
	blx	r1
	mov	r0, r5
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB20_14:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC20_1+8))
	movw	r1, #707
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC20_1+8))
LPC20_1:
	ldr	r0, [pc, r0]
	b	LBB20_17
Ltmp39:
LBB20_15:
	ldr	r0, LCPI20_0
LPC20_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB20_16:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC20_3+8))
	movw	r1, #821
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC20_3+8))
LPC20_3:
	ldr	r0, [pc, r0]
LBB20_17:
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
	.align	2
	.data_region
LCPI20_0:
	.long	Ltmp39-(LPC20_0+8)
	.end_data_region
Leh_func_end20:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader:
Leh_func_begin21:
	push	{r4, r5, r6, r7, lr}
Ltmp40:
	add	r7, sp, #12
Ltmp41:
	push	{r10, r11}
Ltmp42:
	mov	r4, r1
	mov	r11, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	movw	r1, :lower16:(_mono_aot_System_ServiceModel_got-(LPC21_0+8))
	movt	r1, :upper16:(_mono_aot_System_ServiceModel_got-(LPC21_0+8))
LPC21_0:
	add	r1, pc, r1
	ldr	r1, [r1, #100]
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB21_7
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	ldr	r1, [r11]
	mov	r5, r11
	ldr	r1, [r5, #12]!
	bl	_p_1_plt_string_op_Equality_string_string_llvm
	tst	r0, #255
	beq	LBB21_8
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	cmp	r0, #1
	bne	LBB21_8
	ldr	r0, [r4]
	ldr	r1, [r0, #212]
	mov	r0, r4
	blx	r1
	tst	r0, #255
	bne	LBB21_8
	ldr	r0, [r4]
	ldr	r1, [r0, #64]
	mov	r0, r4
	blx	r1
	movw	r5, :lower16:(_mono_aot_System_ServiceModel_got-(LPC21_3+8))
	mov	r10, r0
	movt	r5, :upper16:(_mono_aot_System_ServiceModel_got-(LPC21_3+8))
LPC21_3:
	add	r5, pc, r5
	ldr	r0, [r5, #108]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r6, r0
	bl	_p_20_plt_System_Uri__ctor_string_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #112]
	mov	r0, r4
	blx	r1
	ldr	r0, [r5, #36]
	ldr	r0, [r0]
	cmp	r11, r0
	bne	LBB21_6
	ldr	r0, [r5, #116]
	ldr	r1, [r0]
	mov	r0, r6
	bl	_p_11_plt_System_Uri_op_Equality_System_Uri_System_Uri_llvm
	tst	r0, #255
	ldrne	r0, [r5, #120]
	ldrne	r6, [r0]
LBB21_6:
	ldr	r0, [r5, #28]
	mov	r1, #0
	bl	_p_6_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	ldr	r0, [r5, #112]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, #0
	mov	r3, r4
	mov	r5, r0
	bl	_p_21_plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader___llvm
	mov	r0, r5
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB21_7:
	add	r5, r11, #12
LBB21_8:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC21_1+8))
	movw	r1, #935
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC21_1+8))
LPC21_1:
	ldr	r0, [pc, r0]
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r10, r0
	ldr	r0, [r11]
	ldr	r5, [r5]
	ldr	r0, [r4]
	ldr	r1, [r0, #196]
	mov	r0, r4
	blx	r1
	mov	r6, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #188]
	mov	r0, r4
	blx	r1
	mov	r3, r0
	mov	r0, r10
	mov	r1, r5
	mov	r2, r6
	bl	_p_18_plt_string_Format_string_object_object_object_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC21_2+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC21_2+8))
LPC21_2:
	add	r0, pc, r0
	ldr	r0, [r0, #104]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_19_plt_System_Xml_XmlException__ctor_string_llvm
	mov	r0, r5
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end21:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_ToString
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_ToString:
Leh_func_begin22:
	push	{r7, lr}
Ltmp43:
	mov	r7, sp
Ltmp44:
Ltmp45:
	ldr	r0, [r0, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #32]
	blx	r1
	pop	{r7, pc}
Leh_func_end22:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter:
Leh_func_begin23:
	push	{r4, r5, r7, lr}
Ltmp46:
	add	r7, sp, #8
Ltmp47:
Ltmp48:
	mov	r4, r2
	mov	r5, r0
	cmp	r4, #0
	beq	LBB23_4
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC23_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC23_0+8))
LPC23_0:
	add	r0, pc, r0
	ldr	r2, [r0, #44]
	ldr	r2, [r2]
	cmp	r1, r2
	bne	LBB23_3
	ldr	r0, [r5, #8]
	ldr	r1, [r0]
	bl	_p_23_plt_System_Uri_get_AbsoluteUri_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #56]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB23_3:
	ldr	r3, [r0, #100]
	ldr	r0, [r1]
	ldr	r2, [r1, #12]
	ldr	r0, [r4]
	mov	r0, r4
	mov	r1, r3
	bl	_p_22_plt_System_Xml_XmlWriter_WriteStartElement_string_string_llvm
	ldr	r0, [r5, #8]
	ldr	r1, [r0]
	bl	_p_23_plt_System_Uri_get_AbsoluteUri_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #56]
	mov	r0, r4
	blx	r2
	ldr	r0, [r4]
	ldr	r1, [r0, #100]
	mov	r0, r4
	blx	r1
	pop	{r4, r5, r7, pc}
LBB23_4:
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC23_1+8))
	movw	r1, #1094
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC23_1+8))
LPC23_1:
	ldr	r0, [pc, r0]
	bl	_p_12_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end23:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter:
Leh_func_begin24:
	push	{r4, r5, r6, r7, lr}
Ltmp49:
	add	r7, sp, #12
Ltmp50:
Ltmp51:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC24_0+8))
	mov	r6, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #124]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r0, r5
	mov	r1, r4
	str	r6, [r2, #24]
	bl	_p_24_plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end24:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress__cctor
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress__cctor:
Leh_func_begin25:
	push	{r4, r5, r6, r7, lr}
Ltmp52:
	add	r7, sp, #12
Ltmp53:
	push	{r10}
Ltmp54:
	movw	r5, :lower16:(_mono_aot_System_ServiceModel_got-(LPC25_0+8))
	movt	r5, :upper16:(_mono_aot_System_ServiceModel_got-(LPC25_0+8))
LPC25_0:
	add	r5, pc, r5
	ldr	r4, [r5, #108]
	ldr	r10, [r5, #128]
	mov	r0, r4
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r6, r0
	bl	_p_20_plt_System_Uri__ctor_string_llvm
	ldr	r0, [r5, #116]
	ldr	r10, [r5, #132]
	str	r6, [r0]
	mov	r0, r4
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r6, r0
	bl	_p_20_plt_System_Uri__ctor_string_llvm
	ldr	r0, [r5, #120]
	str	r6, [r0]
	mov	r0, r4
	ldr	r6, [r5, #136]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r4, r0
	bl	_p_20_plt_System_Uri__ctor_string_llvm
	ldr	r0, [r5, #140]
	str	r4, [r0]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end25:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_GetSchema
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_GetSchema:
Leh_func_begin26:
	mov	r0, #0
	bx	lr
Leh_func_end26:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader:
Leh_func_begin27:
	push	{r4, r7, lr}
Ltmp55:
	add	r7, sp, #4
Ltmp56:
Ltmp57:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_System_ServiceModel_got-(LPC27_0+8))
	movt	r0, :upper16:(_mono_aot_System_ServiceModel_got-(LPC27_0+8))
LPC27_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	ldr	r0, [r0]
	bl	_p_25_plt_System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_llvm
	str	r0, [r4, #8]
	pop	{r4, r7, pc}
Leh_func_end27:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter:
Leh_func_begin28:
	push	{r7, lr}
Ltmp58:
	mov	r7, sp
Ltmp59:
Ltmp60:
	mov	r2, r1
	ldr	r0, [r0, #8]
	movw	r1, :lower16:(_mono_aot_System_ServiceModel_got-(LPC28_0+8))
	movt	r1, :upper16:(_mono_aot_System_ServiceModel_got-(LPC28_0+8))
LPC28_0:
	add	r1, pc, r1
	ldr	r1, [r1, #36]
	ldr	r1, [r1]
	ldr	r3, [r0]
	bl	_p_26_plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter_llvm
	pop	{r7, pc}
Leh_func_end28:

	.private_extern	_System_ServiceModel__System_ServiceModel_EndpointAddress10__cctor
	.align	2
_System_ServiceModel__System_ServiceModel_EndpointAddress10__cctor:
Leh_func_begin29:
	push	{r4, r5, r6, r7, lr}
Ltmp61:
	add	r7, sp, #12
Ltmp62:
Ltmp63:
	movw	r6, :lower16:(_mono_aot_System_ServiceModel_got-(LPC29_0+8))
	movt	r6, :upper16:(_mono_aot_System_ServiceModel_got-(LPC29_0+8))
LPC29_0:
	add	r6, pc, r6
	ldr	r0, [r6, #108]
	ldr	r4, [r6, #128]
	bl	_p_4_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_20_plt_System_Uri__ctor_string_llvm
	ldr	r0, [r6, #144]
	str	r5, [r0]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end29:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_HelpLink
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_HelpLink:
Leh_func_begin30:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end30:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_HelpLink_string
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_HelpLink_string:
Leh_func_begin31:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end31:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_InnerException
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_InnerException:
Leh_func_begin32:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end32:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_InnerException_System_ServiceModel_ExceptionDetail
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_InnerException_System_ServiceModel_ExceptionDetail:
Leh_func_begin33:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end33:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Message
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Message:
Leh_func_begin34:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end34:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Message_string
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Message_string:
Leh_func_begin35:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end35:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_StackTrace
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_StackTrace:
Leh_func_begin36:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end36:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_StackTrace_string
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_StackTrace_string:
Leh_func_begin37:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end37:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Type
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Type:
Leh_func_begin38:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end38:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Type_string
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Type_string:
Leh_func_begin39:
	str	r1, [r0, #24]
	bx	lr
Leh_func_end39:

	.private_extern	_System_ServiceModel__System_ServiceModel_ExceptionDetail_ToString
	.align	2
_System_ServiceModel__System_ServiceModel_ExceptionDetail_ToString:
Leh_func_begin40:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end40:

	.private_extern	_System_ServiceModel__System_Array_InternalArray__RemoveAt_int
	.align	2
_System_ServiceModel__System_Array_InternalArray__RemoveAt_int:
Leh_func_begin41:
	push	{r7, lr}
Ltmp64:
	mov	r7, sp
Ltmp65:
Ltmp66:
	movw	r0, #45403
	bl	_p_27_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_14_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end41:

.zerofill __DATA,__bss,_mono_aot_System_ServiceModel_got,356,4
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressHeader_GetHashCode
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__cctor
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressing10
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressingAugust2004
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_None
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_Namespace
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_ToString
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__cctor
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Headers
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_get_Uri
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_GetHashCode
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_ToString
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress__cctor
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_GetSchema
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_EndpointAddress10__cctor
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_HelpLink
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_HelpLink_string
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_InnerException
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_InnerException_System_ServiceModel_ExceptionDetail
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Message
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Message_string
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_StackTrace
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_StackTrace_string
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Type
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Type_string
	.no_dead_strip	_System_ServiceModel__System_ServiceModel_ExceptionDetail_ToString
	.no_dead_strip	_System_ServiceModel__System_Array_InternalArray__RemoveAt_int
	.no_dead_strip	_mono_aot_System_ServiceModel_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	42
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	2
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	3
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	4
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	5
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	6
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	7
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	8
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	9
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	10
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	11
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	12
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	13
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	14
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	15
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	16
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	17
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	19
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	20
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	21
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	22
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	23
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	24
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	25
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	26
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	27
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	28
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	29
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	30
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	31
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	32
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	33
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	34
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	35
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	36
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	37
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	38
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	39
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	40
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	41
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	42
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	45
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
Lset42 = Leh_func_end41-Leh_func_begin41
	.long	Lset42
Lset43 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset43
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin26:
	.byte	0

Lmono_eh_func_begin27:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0

Lmono_eh_func_begin36:
	.byte	0

Lmono_eh_func_begin37:
	.byte	0

Lmono_eh_func_begin38:
	.byte	0

Lmono_eh_func_begin39:
	.byte	0

Lmono_eh_func_begin40:
	.byte	0

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "System.ServiceModel.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _System_ServiceModel_EndpointAddress_Equals_object
_System_ServiceModel_EndpointAddress_Equals_object:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,56,208,77,226,13,176,160,225,0,96,160,225,1,160,160,225,0,0,160,227
	.byte 4,0,139,229,0,0,160,227,12,0,139,229,40,160,139,229,44,160,139,229,0,0,90,227,12,0,0,10,40,0,155,229
	.byte 0,0,144,229,0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 148
	.byte 1,16,159,231,1,0,80,225,1,0,0,10,0,0,160,227,44,0,139,229,44,0,155,229,0,0,139,229,44,0,155,229
	.byte 0,16,160,227
bl _System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress

	.byte 255,0,0,226,0,0,80,227,33,0,0,26,0,0,155,229,0,224,208,229,8,0,144,229,0,16,160,227
bl _p_11

	.byte 255,0,0,226,0,0,80,227,25,0,0,26,0,0,155,229,0,224,208,229,8,32,144,229,8,16,150,229,2,0,160,225
	.byte 0,32,146,229,15,224,160,225,44,240,146,229,255,0,0,226,0,0,80,227,14,0,0,10,0,0,155,229,0,224,208,229
	.byte 12,16,144,229,1,0,160,225,0,224,209,229
bl _p_29

	.byte 48,0,139,229,12,16,150,229,1,0,160,225,0,224,209,229
bl _p_29

	.byte 0,16,160,225,48,0,155,229,1,0,80,225,1,0,0,10,0,0,160,227,120,0,0,234,12,16,150,229,1,0,160,225
	.byte 0,224,209,229
bl _p_28

	.byte 4,0,139,229,80,0,0,234,4,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 144
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,64,160,225,0,0,160,227,8,0,203,229,0,0,155,229
	.byte 0,224,208,229,12,16,144,229,1,0,160,225,0,224,209,229
bl _p_28

	.byte 12,0,139,229,21,0,0,234,12,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 144
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,80,160,225,4,0,160,225,5,16,160,225,0,32,148,229
	.byte 15,224,160,225,44,240,146,229,255,0,0,226,0,0,80,227,2,0,0,10,1,0,160,227,8,0,203,229,12,0,0,234
	.byte 12,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 140
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,220,255,255,26,0,0,0,235
	.byte 15,0,0,234,32,224,139,229,12,0,155,229,0,0,80,227,9,0,0,10,12,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 136
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,32,192,155,229,12,240,160,225,8,0,219,229,0,0,80,227
	.byte 3,0,0,26,0,0,160,227,16,0,203,229,15,0,0,235,32,0,0,234,4,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 140
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,161,255,255,26,0,0,0,235
	.byte 15,0,0,234,36,224,139,229,4,0,155,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 136
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,36,192,155,229,12,240,160,225,1,0,160,227,0,0,0,234
	.byte 16,0,219,229,56,208,139,226,112,13,189,232,128,128,189,232

Lme_12:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__Insert_T_int_T
_System_Array_InternalArray__Insert_T_int_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,4,128,139,229,12,0,139,229,16,16,139,229
	.byte 20,32,139,229,4,0,155,229
bl _p_31

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,91,1,11,227
bl _p_27

	.byte 0,16,160,225,117,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_14

	.byte 24,208,139,226,0,9,189,232,128,128,189,232

Lme_2c:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IndexOf_T_T
_System_Array_InternalArray__IndexOf_T_T:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,24,208,77,226,13,176,160,225,4,128,139,229,0,96,160,225,1,160,160,225
	.byte 4,0,155,229
bl _p_35

	.byte 0,80,160,225,0,0,149,229,0,0,160,227,8,0,139,229,0,0,160,227,0,0,139,229,0,0,150,229,22,0,208,229
	.byte 1,0,80,227,59,0,0,202,12,80,150,229,0,64,160,227,44,0,0,234,4,0,155,229
bl _p_34

	.byte 0,128,160,225,6,0,160,225,4,16,160,225,11,32,160,225
bl _p_33

	.byte 0,0,90,227,10,0,0,26,0,0,155,229,0,0,80,227,31,0,0,26,8,160,150,229,0,0,90,227,1,0,0,10
	.byte 4,80,154,229,0,0,0,234,0,80,160,227,5,0,132,224,33,0,0,234,0,32,155,229,2,0,160,225,10,16,160,225
	.byte 0,32,146,229,15,224,160,225,44,240,146,229,255,0,0,226,0,0,80,227,14,0,0,10,20,64,139,229,8,0,150,229
	.byte 12,0,139,229,0,0,80,227,3,0,0,10,12,0,155,229,4,0,144,229,16,0,139,229,1,0,0,234,0,0,160,227
	.byte 16,0,139,229,20,0,155,229,16,16,155,229,1,0,128,224,9,0,0,234,1,64,132,226,5,0,84,225,208,255,255,186
	.byte 8,96,150,229,0,0,86,227,1,0,0,10,4,64,150,229,0,0,0,234,0,64,160,227,1,0,68,226,24,208,139,226
	.byte 112,13,189,232,128,128,189,232,211,0,11,227
bl _p_27
bl _p_32

	.byte 0,16,160,225,134,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_14

Lme_2e:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__get_Item_T_int
_System_Array_InternalArray__get_Item_T_int:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,8,128,139,229,16,0,139,229,20,16,139,229
	.byte 8,0,155,229
bl _p_38

	.byte 4,0,139,229,0,0,144,229,0,0,160,227,12,0,139,229,0,0,160,227,0,0,139,229,16,0,155,229,12,16,144,229
	.byte 20,0,155,229,1,0,80,225,10,0,0,42,8,0,155,229
bl _p_37

	.byte 0,128,160,225,16,0,155,229,20,16,155,229,11,32,160,225
bl _p_36

	.byte 0,0,155,229,24,208,139,226,0,9,189,232,128,128,189,232,183,13,2,227
bl _p_27

	.byte 0,16,160,225,8,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_14

Lme_2f:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__set_Item_T_int_T
_System_Array_InternalArray__set_Item_T_int_T:

	.byte 128,64,45,233,13,112,160,225,96,13,45,233,44,208,77,226,13,176,160,225,0,128,139,229,0,96,160,225,20,16,139,229
	.byte 24,32,139,229,0,0,155,229
bl _p_41

	.byte 0,80,160,225,0,0,149,229,0,0,160,227,4,0,139,229,12,16,150,229,20,0,155,229,1,0,80,225,50,0,0,42
	.byte 8,96,139,229,6,160,160,225,0,0,86,227,24,0,0,10,8,0,155,229,0,0,144,229,12,0,139,229,22,0,208,229
	.byte 1,0,80,227,17,0,0,26,12,0,155,229,0,0,144,229,4,0,144,229,16,0,139,229,28,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 156
	.byte 1,16,159,231,1,0,80,225,7,0,0,26,0,16,159,229,0,0,0,234
	.long _mono_aot_System_ServiceModel_got - . + 152
	.byte 1,16,159,231,16,0,155,229,1,0,80,225,0,0,0,10,0,160,160,227,10,80,160,225,0,0,90,227,6,0,0,10
	.byte 24,32,155,229,5,0,160,225,20,16,155,229,0,48,149,229,15,224,160,225,128,240,147,229,8,0,0,234,24,0,139,226
	.byte 32,0,139,229,0,0,155,229
bl _p_40

	.byte 0,128,160,225,32,32,155,229,6,0,160,225,20,16,155,229
bl _p_39

	.byte 44,208,139,226,96,13,189,232,128,128,189,232,183,13,2,227
bl _p_27

	.byte 0,16,160,225,8,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_14

Lme_30:
.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_45

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_43

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_44

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_43
bl _p_42

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_31:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressHeader_GetHashCode
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__cctor
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressing10
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressingAugust2004
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_None
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_Namespace
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_ToString
.no_dead_strip _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__cctor
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_get_Headers
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_get_Uri
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_GetHashCode
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_ToString
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress__cctor
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_GetSchema
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
.no_dead_strip _System_ServiceModel__System_ServiceModel_EndpointAddress10__cctor
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_HelpLink
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_HelpLink_string
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_InnerException
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_InnerException_System_ServiceModel_ExceptionDetail
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Message
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Message_string
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_StackTrace
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_StackTrace_string
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Type
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Type_string
.no_dead_strip _System_ServiceModel__System_ServiceModel_ExceptionDetail_ToString
.no_dead_strip _System_ServiceModel__System_Array_InternalArray__RemoveAt_int

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressHeader_GetHashCode
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__cctor
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressing10
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_WSAddressingAugust2004
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_None
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_get_Namespace
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion_ToString
	bl _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__cctor
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_get_Headers
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_get_Uri
	bl _System_ServiceModel_EndpointAddress_Equals_object
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_GetHashCode
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_ToString
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress__cctor
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_GetSchema
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_ReadXml_System_Xml_XmlReader
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress10_System_Xml_Serialization_IXmlSerializable_WriteXml_System_Xml_XmlWriter
	bl _System_ServiceModel__System_ServiceModel_EndpointAddress10__cctor
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_HelpLink
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_HelpLink_string
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_InnerException
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_InnerException_System_ServiceModel_ExceptionDetail
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Message
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Message_string
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_StackTrace
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_StackTrace_string
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_get_Type
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_set_Type_string
	bl _System_ServiceModel__System_ServiceModel_ExceptionDetail_ToString
	bl method_addresses
	bl _System_Array_InternalArray__Insert_T_int_T
	bl _System_ServiceModel__System_Array_InternalArray__RemoveAt_int
	bl _System_Array_InternalArray__IndexOf_T_T
	bl _System_Array_InternalArray__get_Item_T_int
	bl _System_Array_InternalArray__set_Item_T_int_T
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 50,10,5,2
	.short 0, 10, 20, 30, 41
	.byte 0,0,1,3,2,3,6,5,3,4,31,4,3,5,17,4,3,3,3,10,86,3,3,8,11,3,5,4,12,3,128,142
	.byte 4,6,2,2,2,2,2,2,2,128,168,2,2,255,255,255,255,84,128,174,2,2,2,2,4
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,220,45,0,301
	.long 48,0,237,46,11,0,0,0
	.long 0,0,0,201,44,0,269,47
	.long 0,0,0,0,0,0,0,0
	.long 0,0,333,49,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 6,44,201,45,220,46,237,47
	.long 269,48,301,49,333
.section __TEXT, __const
	.align 3
class_name_table:

	.short 19, 0, 0, 0, 0, 0, 0, 0
	.short 0, 4, 0, 0, 0, 1, 0, 0
	.short 0, 0, 0, 6, 0, 0, 0, 0
	.short 0, 5, 0, 3, 19, 2, 0, 7
	.short 0, 0, 0, 0, 0, 0, 0, 8
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 43,10,5,2
	.short 0, 11, 22, 33, 44
	.byte 129,96,2,1,1,1,3,6,6,5,4,129,129,4,4,3,3,3,3,4,3,4,129,164,4,4,4,3,4,4,5,5
	.byte 3,129,204,4,4,4,4,4,4,4,5,5,129,253,3,5
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 50,10,5,2
	.short 0, 11, 22, 33, 44
	.byte 0,0,132,238,3,3,3,3,3,3,3,133,6,3,3,3,3,3,3,3,3,29,133,62,3,3,3,3,3,3,3,3
	.byte 3,133,92,3,3,3,3,3,3,3,3,3,133,122,3,3,255,255,255,250,128,133,131,31,3,31,31,31
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11,31,12,13,0,72,14,8,135
	.byte 2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11,29,12,13,0,72,14,8,135
	.byte 2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,72,68,13,11,23,12,13,0,72,14,8,135,2,68
	.byte 14,16,136,4,139,3,142,1,68,14,48,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 134,33,7,23,19,5,21,15,27

.text
	.align 4
plt:
_mono_aot_System_ServiceModel_plt:
_p_1_plt_string_op_Equality_string_string_llvm:
	.no_dead_strip plt_string_op_Equality_string_string
plt_string_op_Equality_string_string:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 172,522
_p_2_plt_System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm:
	.no_dead_strip plt_System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
plt_System_ServiceModel_Channels_AddressHeaderCollection_GetList_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 176,527
_p_3_plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IList_1_System_ServiceModel_Channels_AddressHeader_llvm:
	.no_dead_strip plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IList_1_System_ServiceModel_Channels_AddressHeader
plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IList_1_System_ServiceModel_Channels_AddressHeader:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 180,529
_p_4_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 184,540
_p_5_plt_System_Collections_Generic_List_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader
plt_System_Collections_Generic_List_1_System_ServiceModel_Channels_AddressHeader__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 188,563
_p_6_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 192,574
_p_7_plt_string_Concat_string_string_string_string_llvm:
	.no_dead_strip plt_string_Concat_string_string_string_string
plt_string_Concat_string_string_string_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 196,600

.set _p_8_plt_System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string_llvm, _System_ServiceModel__System_ServiceModel_Channels_AddressingVersion__ctor_string_string_string

.set _p_9_plt_System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader_llvm, _System_ServiceModel__System_ServiceModel_Channels_AddressHeaderCollection__ctor_System_Collections_Generic_IEnumerable_1_System_ServiceModel_Channels_AddressHeader

.set _p_10_plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader_llvm, _System_ServiceModel__System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeaderCollection_System_Xml_XmlDictionaryReader_System_Xml_XmlDictionaryReader
_p_11_plt_System_Uri_op_Equality_System_Uri_System_Uri_llvm:
	.no_dead_strip plt_System_Uri_op_Equality_System_Uri_System_Uri
plt_System_Uri_op_Equality_System_Uri_System_Uri:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 212,611
_p_12_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 216,616
_p_13_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 220,636
_p_14_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 224,669
_p_15_plt_System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString_llvm:
	.no_dead_strip plt_System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString
plt_System_ServiceModel_EndpointAddress_ReadFromInternal_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_string_string_System_Xml_XmlDictionaryString_System_Xml_XmlDictionaryString:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 228,697
_p_16_plt_System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_llvm:
	.no_dead_strip plt_System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
plt_System_ServiceModel_EndpointAddress_ReadContents_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 232,699
_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 236,701
_p_18_plt_string_Format_string_object_object_object_llvm:
	.no_dead_strip plt_string_Format_string_object_object_object
plt_string_Format_string_object_object_object:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 240,746
_p_19_plt_System_Xml_XmlException__ctor_string_llvm:
	.no_dead_strip plt_System_Xml_XmlException__ctor_string
plt_System_Xml_XmlException__ctor_string:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 244,751
_p_20_plt_System_Uri__ctor_string_llvm:
	.no_dead_strip plt_System_Uri__ctor_string
plt_System_Uri__ctor_string:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 248,756
_p_21_plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader___llvm:
	.no_dead_strip plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__
plt_System_ServiceModel_EndpointAddress__ctor_System_Uri_System_ServiceModel_EndpointIdentity_System_ServiceModel_Channels_AddressHeader__:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 252,761
_p_22_plt_System_Xml_XmlWriter_WriteStartElement_string_string_llvm:
	.no_dead_strip plt_System_Xml_XmlWriter_WriteStartElement_string_string
plt_System_Xml_XmlWriter_WriteStartElement_string_string:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 256,763
_p_23_plt_System_Uri_get_AbsoluteUri_llvm:
	.no_dead_strip plt_System_Uri_get_AbsoluteUri
plt_System_Uri_get_AbsoluteUri:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 260,768
_p_24_plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter_llvm:
	.no_dead_strip plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter
plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlDictionaryWriter:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 264,773

.set _p_25_plt_System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader_llvm, _System_ServiceModel__System_ServiceModel_EndpointAddress_ReadFrom_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlReader
_p_26_plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter_llvm:
	.no_dead_strip plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter
plt_System_ServiceModel_EndpointAddress_WriteContentsTo_System_ServiceModel_Channels_AddressingVersion_System_Xml_XmlWriter:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 272,777
_p_27_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 276,779
_p_28_plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_GetEnumerator_llvm:
	.no_dead_strip plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_GetEnumerator
plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_GetEnumerator:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 280,808
_p_29_plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_get_Count_llvm:
	.no_dead_strip plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_get_Count
plt_System_Collections_ObjectModel_ReadOnlyCollection_1_System_ServiceModel_Channels_AddressHeader_get_Count:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 284,819

.set _p_30_plt_System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress_llvm, _System_ServiceModel__System_ServiceModel_EndpointAddress_op_Equality_System_ServiceModel_EndpointAddress_System_ServiceModel_EndpointAddress
_p_31_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 292,851
_p_32_plt_Locale_GetText_string_llvm:
	.no_dead_strip plt_Locale_GetText_string
plt_Locale_GetText_string:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 296,878
_p_33_plt_System_Array_GetGenericValueImpl_T_int_T__llvm:
	.no_dead_strip plt_System_Array_GetGenericValueImpl_T_int_T_
plt_System_Array_GetGenericValueImpl_T_int_T_:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 300,883
_p_34_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 304,922
_p_35_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 308,946
_p_36_plt_System_Array_GetGenericValueImpl_T_int_T__0_llvm:
	.no_dead_strip plt_System_Array_GetGenericValueImpl_T_int_T__0
plt_System_Array_GetGenericValueImpl_T_int_T__0:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 312,973
_p_37_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 316,1012
_p_38_plt__rgctx_fetch_4_llvm:
	.no_dead_strip plt__rgctx_fetch_4
plt__rgctx_fetch_4:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 320,1036
_p_39_plt_System_Array_SetGenericValueImpl_T_int_T__llvm:
	.no_dead_strip plt_System_Array_SetGenericValueImpl_T_int_T_
plt_System_Array_SetGenericValueImpl_T_int_T_:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 324,1063
_p_40_plt__rgctx_fetch_5_llvm:
	.no_dead_strip plt__rgctx_fetch_5
plt__rgctx_fetch_5:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 328,1102
_p_41_plt__rgctx_fetch_6_llvm:
	.no_dead_strip plt__rgctx_fetch_6
plt__rgctx_fetch_6:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 332,1126
_p_42_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 336,1153
_p_43_plt__rgctx_fetch_7_llvm:
	.no_dead_strip plt__rgctx_fetch_7
plt__rgctx_fetch_7:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 340,1208
_p_44_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 344,1216
_p_45_plt__rgctx_fetch_8_llvm:
	.no_dead_strip plt__rgctx_fetch_8
plt__rgctx_fetch_8:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_System_ServiceModel_got - . + 348,1235
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "System.ServiceModel"
	.asciz "EBC4352B-2DE3-4ED8-A815-11AB02D0D708"
	.asciz ""
	.asciz "31bf3856ad364e35"
	.align 3

	.long 1,2,0,5,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System.Runtime.Serialization"
	.asciz "5B04B6ED-328E-42F2-9966-E17AC74BE6D6"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System.Xml"
	.asciz "4D3147BC-C2E3-4CDA-88A7-E0386EE9E899"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "EBC4352B-2DE3-4ED8-A815-11AB02D0D708"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "System.ServiceModel"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_System_ServiceModel_got
	.align 2
	.long _System_ServiceModel__System_ServiceModel_Channels_AddressHeader_Equals_object
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 43,356,46,50,11,387000831,0,1706
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_System_ServiceModel_info
	.align 2
_mono_aot_module_System_ServiceModel_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,1,4,0,0,1,4,0,1,4,3,6,6,5,1,4,2,8,7,1,5,0,1,5,1,9,1,5,1,10,1
	.byte 5,1,11,1,5,0,1,5,2,13,12,1,5,14,14,17,22,21,9,17,20,19,18,10,17,16,15,11,1,6,1,23
	.byte 1,6,0,1,6,0,1,6,0,1,6,7,40,39,39,38,37,38,37,1,6,0,1,6,0,1,6,0,1,6,5,24
	.byte 9,10,10,9,1,6,8,25,29,28,7,9,27,26,30,1,6,0,1,6,2,11,25,1,6,1,31,1,6,9,32,27
	.byte 34,30,27,33,29,27,35,1,7,0,1,7,1,9,1,7,1,9,1,7,3,36,27,32,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,42,41,0,0,5,30,0,1
	.byte 255,255,255,255,255,193,0,13,51,255,253,0,0,0,2,130,10,1,1,198,0,13,51,0,1,7,128,188,193,0,13,52
	.byte 5,30,0,1,255,255,255,255,255,193,0,13,53,255,253,0,0,0,2,130,10,1,1,198,0,13,53,0,1,7,128,224
	.byte 5,30,0,1,255,255,255,255,255,193,0,13,54,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,7,129,0
	.byte 5,30,0,1,255,255,255,255,255,193,0,13,55,255,253,0,0,0,2,130,10,1,1,198,0,13,55,0,1,7,129,32
	.byte 5,30,0,1,255,255,255,255,255,193,0,13,43,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,129,64
	.byte 12,0,39,42,47,11,1,3,14,3,219,0,0,3,23,3,219,0,0,2,14,6,1,1,3,16,1,4,1,16,1,5
	.byte 6,16,1,5,5,16,1,5,7,17,0,1,17,0,7,17,0,11,17,0,45,17,0,128,143,14,1,5,17,0,128,253
	.byte 17,0,129,23,17,0,129,97,17,0,129,183,17,0,129,213,14,1,4,11,2,8,2,17,0,131,151,14,2,129,82,3
	.byte 14,2,129,68,4,14,1,6,16,1,6,8,16,1,6,9,14,2,12,2,17,0,132,84,17,0,132,178,17,0,133,65
	.byte 16,1,6,10,16,1,7,16,6,193,0,17,58,6,193,0,5,8,6,255,254,0,0,0,0,202,0,0,15,11,1,6
	.byte 11,2,130,57,1,11,2,130,177,1,3,193,0,19,41,3,6,3,255,254,0,0,0,0,202,0,0,3,7,20,109,111
	.byte 110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,255,254,0,0,0,0,202,0,0,4,7,23
	.byte 109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,193,0,19,135,3,8,3
	.byte 5,3,16,3,196,0,9,162,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,30,109,111
	.byte 110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,49,0,7,25,109
	.byte 111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,23,3,24,7,42,108
	.byte 108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116
	.byte 114,97,109,112,111,108,105,110,101,0,3,193,0,19,127,3,195,0,11,236,3,196,0,9,129,3,15,3,195,0,14,175
	.byte 3,196,0,9,135,3,26,3,22,3,27,7,26,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,95,109
	.byte 115,99,111,114,108,105,98,0,3,255,254,0,0,0,0,202,0,0,14,3,255,254,0,0,0,0,202,0,0,13,3,21
	.byte 255,253,0,0,0,2,130,10,1,1,198,0,13,51,0,1,7,128,188,35,131,64,192,0,92,41,255,253,0,0,0,2
	.byte 130,10,1,1,198,0,13,51,0,1,7,128,188,0,3,193,0,0,129,3,255,253,0,0,0,2,130,10,1,1,198,0
	.byte 13,56,0,1,7,128,224,255,253,0,0,0,2,130,10,1,1,198,0,13,53,0,1,7,128,224,35,131,135,140,17,255
	.byte 253,0,0,0,2,130,10,1,1,198,0,13,56,0,1,7,128,224,35,131,135,192,0,92,41,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,53,0,1,7,128,224,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,56,0,1,7,129
	.byte 0,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,7,129,0,35,131,225,140,17,255,253,0,0,0,2,130
	.byte 10,1,1,198,0,13,56,0,1,7,129,0,35,131,225,192,0,92,41,255,253,0,0,0,2,130,10,1,1,198,0,13
	.byte 54,0,1,7,129,0,0,3,255,253,0,0,0,2,130,10,1,1,198,0,13,57,0,1,7,129,32,255,253,0,0,0
	.byte 2,130,10,1,1,198,0,13,55,0,1,7,129,32,35,132,59,140,17,255,253,0,0,0,2,130,10,1,1,198,0,13
	.byte 57,0,1,7,129,32,35,132,59,192,0,92,41,255,253,0,0,0,2,130,10,1,1,198,0,13,55,0,1,7,129,32
	.byte 0,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99,0,255,253,0,0
	.byte 0,2,130,10,1,1,198,0,13,43,0,1,7,129,64,4,2,130,11,1,1,7,129,64,35,132,156,150,5,7,132,175
	.byte 3,255,253,0,0,0,7,132,175,1,198,0,13,121,1,7,129,64,0,35,132,156,192,0,92,41,255,253,0,0,0,2
	.byte 130,10,1,1,198,0,13,43,0,1,7,129,64,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,38,0
	.byte 1,2,2,0,130,96,129,136,130,28,130,32,2,0,130,248,129,52,130,180,130,184,1,4,130,120,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,3,32,0,1,11,4,19,255,253,0,0,0,2,130,10,1,1,198,0,13,51,0,1,7,128,188,1,0,1
	.byte 0,0,16,0,0,3,56,0,1,11,4,19,255,253,0,0,0,2,130,10,1,1,198,0,13,53,0,1,7,128,224,1
	.byte 0,1,0,0,3,32,0,1,11,8,19,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,7,129,0,1,0
	.byte 1,0,0,3,88,0,1,11,0,19,255,253,0,0,0,2,130,10,1,1,198,0,13,55,0,1,7,129,32,1,0,1
	.byte 0,0,3,118,0,1,11,4,19,255,253,0,0,0,2,130,10,1,1,198,0,13,43,0,1,7,129,64,1,0,1,0
	.byte 0,0,128,144,8,0,0,1,4,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,6
	.byte 128,144,8,0,0,1,193,0,18,178,4,193,0,18,174,3,0,0,255,255,255,255,255,4,128,228,14,20,12,0,4,13
	.byte 193,0,18,175,193,0,18,174,193,0,18,172,4,128,228,28,28,12,0,4,25,20,193,0,18,174,19,7,128,228,32,12
	.byte 4,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,29,30,31,4,128,160,28,0,0,4,43,193,0
	.byte 18,175,193,0,18,174,193,0,18,172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_4:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_3:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM10=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM12=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_5:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM15=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM16=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM17=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM18=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM19=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_6:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM20=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM21=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM22=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM23=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM24=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_7:

	.byte 5
	.asciz "System_UriParser"

	.byte 16,16
LDIFF_SYM25=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,0,6
	.asciz "scheme_name"

LDIFF_SYM26=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM26
	.byte 2,35,8,6
	.asciz "default_port"

LDIFF_SYM27=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,12,0,7
	.asciz "System_UriParser"

LDIFF_SYM28=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM29=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM29
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM30=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM30
LTDIE_2:

	.byte 5
	.asciz "System_Uri"

	.byte 80,16
LDIFF_SYM31=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,0,6
	.asciz "isUnixFilePath"

LDIFF_SYM32=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,52,6
	.asciz "source"

LDIFF_SYM33=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,8,6
	.asciz "scheme"

LDIFF_SYM34=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,12,6
	.asciz "host"

LDIFF_SYM35=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,16,6
	.asciz "port"

LDIFF_SYM36=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,56,6
	.asciz "path"

LDIFF_SYM37=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,20,6
	.asciz "query"

LDIFF_SYM38=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,24,6
	.asciz "fragment"

LDIFF_SYM39=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,28,6
	.asciz "userinfo"

LDIFF_SYM40=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,32,6
	.asciz "isUnc"

LDIFF_SYM41=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM41
	.byte 2,35,60,6
	.asciz "isOpaquePart"

LDIFF_SYM42=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,61,6
	.asciz "isAbsoluteUri"

LDIFF_SYM43=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2,35,62,6
	.asciz "scope_id"

LDIFF_SYM44=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,64,6
	.asciz "userEscaped"

LDIFF_SYM45=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,72,6
	.asciz "cachedAbsoluteUri"

LDIFF_SYM46=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,36,6
	.asciz "cachedToString"

LDIFF_SYM47=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,40,6
	.asciz "cachedLocalPath"

LDIFF_SYM48=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,35,44,6
	.asciz "cachedHashCode"

LDIFF_SYM49=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,76,6
	.asciz "parser"

LDIFF_SYM50=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,35,48,0,7
	.asciz "System_Uri"

LDIFF_SYM51=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM52=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM53=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM53
LTDIE_10:

	.byte 17
	.asciz "System_Collections_Generic_IList`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IList`1"

LDIFF_SYM54=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM55=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM56=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_9:

	.byte 5
	.asciz "System_Collections_ObjectModel_ReadOnlyCollection`1"

	.byte 12,16
LDIFF_SYM57=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,0,6
	.asciz "list"

LDIFF_SYM58=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM58
	.byte 2,35,8,0,7
	.asciz "System_Collections_ObjectModel_ReadOnlyCollection`1"

LDIFF_SYM59=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM60=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM61=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_8:

	.byte 5
	.asciz "System_ServiceModel_Channels_AddressHeaderCollection"

	.byte 12,16
LDIFF_SYM62=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,0,0,7
	.asciz "System_ServiceModel_Channels_AddressHeaderCollection"

LDIFF_SYM63=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM64=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM65=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_11:

	.byte 5
	.asciz "System_ServiceModel_EndpointIdentity"

	.byte 8,16
LDIFF_SYM66=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,0,0,7
	.asciz "System_ServiceModel_EndpointIdentity"

LDIFF_SYM67=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM67
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM68=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM69=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_14:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM70=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM71=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM72=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM73=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM74=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM75=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM76=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM77=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_16:

	.byte 8
	.asciz "_CommandState"

	.byte 4
LDIFF_SYM78=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ReadElementContentAsBase64"

	.byte 1,9
	.asciz "ReadContentAsBase64"

	.byte 2,9
	.asciz "ReadElementContentAsBinHex"

	.byte 3,9
	.asciz "ReadContentAsBinHex"

	.byte 4,0,7
	.asciz "_CommandState"

LDIFF_SYM79=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM80=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM81=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_15:

	.byte 5
	.asciz "System_Xml_XmlReaderBinarySupport"

	.byte 24,16
LDIFF_SYM82=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,0,6
	.asciz "reader"

LDIFF_SYM83=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,8,6
	.asciz "base64CacheStartsAt"

LDIFF_SYM84=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM85=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,16,6
	.asciz "hasCache"

LDIFF_SYM86=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,20,6
	.asciz "dontReset"

LDIFF_SYM87=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,21,0,7
	.asciz "System_Xml_XmlReaderBinarySupport"

LDIFF_SYM88=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM89=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM90=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_18:

	.byte 8
	.asciz "System_Xml_ConformanceLevel"

	.byte 4
LDIFF_SYM91=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 9
	.asciz "Auto"

	.byte 0,9
	.asciz "Fragment"

	.byte 1,9
	.asciz "Document"

	.byte 2,0,7
	.asciz "System_Xml_ConformanceLevel"

LDIFF_SYM92=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM93=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM94=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_19:

	.byte 5
	.asciz "System_Xml_XmlNameTable"

	.byte 8,16
LDIFF_SYM95=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlNameTable"

LDIFF_SYM96=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM96
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM97=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM98=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_21:

	.byte 5
	.asciz "System_Xml_XmlResolver"

	.byte 8,16
LDIFF_SYM99=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlResolver"

LDIFF_SYM100=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_22:

	.byte 5
	.asciz "System_Collections_ArrayList"

	.byte 20,16
LDIFF_SYM103=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM104=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM105=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM106=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,16,0,7
	.asciz "System_Collections_ArrayList"

LDIFF_SYM107=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM108=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM109=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_25:

	.byte 5
	.asciz "_DictionaryNode"

	.byte 20,16
LDIFF_SYM110=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,0,6
	.asciz "key"

LDIFF_SYM111=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,8,6
	.asciz "value"

LDIFF_SYM112=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,35,12,6
	.asciz "next"

LDIFF_SYM113=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM113
	.byte 2,35,16,0,7
	.asciz "_DictionaryNode"

LDIFF_SYM114=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM115=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM115
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM116=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_26:

	.byte 17
	.asciz "System_Collections_IComparer"

	.byte 8,7
	.asciz "System_Collections_IComparer"

LDIFF_SYM117=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM118=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM118
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM119=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM119
LTDIE_24:

	.byte 5
	.asciz "System_Collections_Specialized_ListDictionary"

	.byte 24,16
LDIFF_SYM120=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 2,35,0,6
	.asciz "count"

LDIFF_SYM121=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 2,35,16,6
	.asciz "version"

LDIFF_SYM122=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 2,35,20,6
	.asciz "head"

LDIFF_SYM123=LTDIE_25_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,8,6
	.asciz "comparer"

LDIFF_SYM124=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,12,0,7
	.asciz "System_Collections_Specialized_ListDictionary"

LDIFF_SYM125=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM126=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM127=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_23:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

	.byte 12,16
LDIFF_SYM128=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM128
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM129=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM129
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

LDIFF_SYM130=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM130
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM131=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM131
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM132=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_27:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

	.byte 9,16
LDIFF_SYM133=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM133
	.byte 2,35,0,6
	.asciz "enable_upa_check"

LDIFF_SYM134=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

LDIFF_SYM135=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM136=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM137=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_33:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM138=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM139=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM140=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM140
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM141=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_32:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM142=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM142
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM143=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM144=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM145=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_31:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM146=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM146
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM147=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM148=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM148
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM149=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM149
LTDIE_35:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM150=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM151=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM152=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM153=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM153
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM154=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_34:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM155=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM156=LTDIE_35_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM157=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM158=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM159=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM160=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM160
LTDIE_30:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM161=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM161
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM162=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM163=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM164=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM165=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM166=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM167=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM168=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM168
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM169=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM170=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM171=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM171
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM172=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM172
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM173=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_29:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM174=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM175=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM175
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM176=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM177=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM177
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM178=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM178
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM179=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM179
LTDIE_28:

	.byte 5
	.asciz "System_Xml_Schema_ValidationEventHandler"

	.byte 52,16
LDIFF_SYM180=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,0,0,7
	.asciz "System_Xml_Schema_ValidationEventHandler"

LDIFF_SYM181=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM181
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM182=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM182
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM183=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_37:

	.byte 5
	.asciz "_HashKeys"

	.byte 12,16
LDIFF_SYM184=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM185=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,8,0,7
	.asciz "_HashKeys"

LDIFF_SYM186=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM186
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM187=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM187
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM188=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM188
LTDIE_38:

	.byte 5
	.asciz "_HashValues"

	.byte 12,16
LDIFF_SYM189=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM190=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 2,35,8,0,7
	.asciz "_HashValues"

LDIFF_SYM191=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM192=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM192
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM193=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM193
LTDIE_39:

	.byte 17
	.asciz "System_Collections_IHashCodeProvider"

	.byte 8,7
	.asciz "System_Collections_IHashCodeProvider"

LDIFF_SYM194=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM194
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM195=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM195
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM196=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM196
LTDIE_40:

	.byte 17
	.asciz "System_Collections_IEqualityComparer"

	.byte 8,7
	.asciz "System_Collections_IEqualityComparer"

LDIFF_SYM197=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM198=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM199=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_41:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM200=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM200
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM201=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM202=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM203=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM203
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM204=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM204
LTDIE_36:

	.byte 5
	.asciz "System_Collections_Hashtable"

	.byte 52,16
LDIFF_SYM205=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM206=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,8,6
	.asciz "hashes"

LDIFF_SYM207=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,12,6
	.asciz "hashKeys"

LDIFF_SYM208=LTDIE_37_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM208
	.byte 2,35,16,6
	.asciz "hashValues"

LDIFF_SYM209=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 2,35,20,6
	.asciz "hcpRef"

LDIFF_SYM210=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2,35,24,6
	.asciz "comparerRef"

LDIFF_SYM211=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM211
	.byte 2,35,28,6
	.asciz "equalityComparer"

LDIFF_SYM212=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 2,35,32,6
	.asciz "inUse"

LDIFF_SYM213=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,35,36,6
	.asciz "modificationCount"

LDIFF_SYM214=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,35,40,6
	.asciz "loadFactor"

LDIFF_SYM215=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,35,44,6
	.asciz "threshold"

LDIFF_SYM216=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM216
	.byte 2,35,48,0,7
	.asciz "System_Collections_Hashtable"

LDIFF_SYM217=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM218=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM218
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM219=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_20:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaSet"

	.byte 80,16
LDIFF_SYM220=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM220
	.byte 2,35,0,6
	.asciz "nameTable"

LDIFF_SYM221=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM221
	.byte 2,35,8,6
	.asciz "xmlResolver"

LDIFF_SYM222=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,12,6
	.asciz "schemas"

LDIFF_SYM223=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM223
	.byte 2,35,16,6
	.asciz "attributes"

LDIFF_SYM224=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM224
	.byte 2,35,20,6
	.asciz "elements"

LDIFF_SYM225=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM225
	.byte 2,35,24,6
	.asciz "types"

LDIFF_SYM226=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,28,6
	.asciz "settings"

LDIFF_SYM227=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,32,6
	.asciz "isCompiled"

LDIFF_SYM228=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,60,6
	.asciz "<CompilationId>k__BackingField"

LDIFF_SYM229=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,64,6
	.asciz "ValidationEventHandler"

LDIFF_SYM230=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM230
	.byte 2,35,36,6
	.asciz "global_attribute_groups"

LDIFF_SYM231=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM231
	.byte 2,35,40,6
	.asciz "global_groups"

LDIFF_SYM232=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM232
	.byte 2,35,44,6
	.asciz "global_notations"

LDIFF_SYM233=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,48,6
	.asciz "global_identity_constraints"

LDIFF_SYM234=LTDIE_23_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,52,6
	.asciz "global_ids"

LDIFF_SYM235=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,56,0,7
	.asciz "System_Xml_Schema_XmlSchemaSet"

LDIFF_SYM236=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM236
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM237=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM237
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM238=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM238
LTDIE_42:

	.byte 8
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

	.byte 4
LDIFF_SYM239=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM239
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ProcessInlineSchema"

	.byte 1,9
	.asciz "ProcessSchemaLocation"

	.byte 2,9
	.asciz "ReportValidationWarnings"

	.byte 4,9
	.asciz "ProcessIdentityConstraints"

	.byte 8,9
	.asciz "AllowXmlAttributes"

	.byte 16,0,7
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

LDIFF_SYM240=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM241=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM242=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_43:

	.byte 8
	.asciz "System_Xml_ValidationType"

	.byte 4
LDIFF_SYM243=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "Auto"

	.byte 1,9
	.asciz "DTD"

	.byte 2,9
	.asciz "XDR"

	.byte 3,9
	.asciz "Schema"

	.byte 4,0,7
	.asciz "System_Xml_ValidationType"

LDIFF_SYM244=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM244
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM245=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM245
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM246=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM246
LTDIE_17:

	.byte 5
	.asciz "System_Xml_XmlReaderSettings"

	.byte 60,16
LDIFF_SYM247=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,0,6
	.asciz "checkCharacters"

LDIFF_SYM248=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM248
	.byte 2,35,24,6
	.asciz "closeInput"

LDIFF_SYM249=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM249
	.byte 2,35,25,6
	.asciz "conformance"

LDIFF_SYM250=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM250
	.byte 2,35,28,6
	.asciz "ignoreComments"

LDIFF_SYM251=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,32,6
	.asciz "ignoreProcessingInstructions"

LDIFF_SYM252=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 2,35,33,6
	.asciz "ignoreWhitespace"

LDIFF_SYM253=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM253
	.byte 2,35,34,6
	.asciz "lineNumberOffset"

LDIFF_SYM254=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM254
	.byte 2,35,36,6
	.asciz "linePositionOffset"

LDIFF_SYM255=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM255
	.byte 2,35,40,6
	.asciz "prohibitDtd"

LDIFF_SYM256=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,44,6
	.asciz "nameTable"

LDIFF_SYM257=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,8,6
	.asciz "schemas"

LDIFF_SYM258=LTDIE_20_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM258
	.byte 2,35,12,6
	.asciz "schemasNeedsInitialization"

LDIFF_SYM259=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM259
	.byte 2,35,45,6
	.asciz "validationFlags"

LDIFF_SYM260=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM260
	.byte 2,35,48,6
	.asciz "validationType"

LDIFF_SYM261=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM261
	.byte 2,35,52,6
	.asciz "xmlResolver"

LDIFF_SYM262=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM262
	.byte 2,35,16,6
	.asciz "isReadOnly"

LDIFF_SYM263=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM263
	.byte 2,35,56,6
	.asciz "isAsync"

LDIFF_SYM264=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM264
	.byte 2,35,57,6
	.asciz "ValidationEventHandler"

LDIFF_SYM265=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReaderSettings"

LDIFF_SYM266=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM266
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM267=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM267
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM268=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_13:

	.byte 5
	.asciz "System_Xml_XmlReader"

	.byte 24,16
LDIFF_SYM269=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM269
	.byte 2,35,0,6
	.asciz "readStringBuffer"

LDIFF_SYM270=LTDIE_14_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,8,6
	.asciz "binary"

LDIFF_SYM271=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,12,6
	.asciz "settings"

LDIFF_SYM272=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM272
	.byte 2,35,16,6
	.asciz "asyncRunning"

LDIFF_SYM273=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM273
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReader"

LDIFF_SYM274=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM275=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM276=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM276
LTDIE_44:

	.byte 5
	.asciz "System_Xml_XmlDictionaryReaderQuotas"

	.byte 32,16
LDIFF_SYM277=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM277
	.byte 2,35,0,6
	.asciz "is_readonly"

LDIFF_SYM278=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM278
	.byte 2,35,8,6
	.asciz "array_len"

LDIFF_SYM279=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM279
	.byte 2,35,12,6
	.asciz "bytes"

LDIFF_SYM280=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM280
	.byte 2,35,16,6
	.asciz "depth"

LDIFF_SYM281=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,35,20,6
	.asciz "nt_chars"

LDIFF_SYM282=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,35,24,6
	.asciz "text_len"

LDIFF_SYM283=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM283
	.byte 2,35,28,0,7
	.asciz "System_Xml_XmlDictionaryReaderQuotas"

LDIFF_SYM284=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM284
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM285=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM285
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM286=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM286
LTDIE_12:

	.byte 5
	.asciz "System_Xml_XmlDictionaryReader"

	.byte 28,16
LDIFF_SYM287=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 2,35,0,6
	.asciz "quotas"

LDIFF_SYM288=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM288
	.byte 2,35,24,0,7
	.asciz "System_Xml_XmlDictionaryReader"

LDIFF_SYM289=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM289
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM290=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM290
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM291=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM291
LTDIE_0:

	.byte 5
	.asciz "System_ServiceModel_EndpointAddress"

	.byte 28,16
LDIFF_SYM292=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,35,0,6
	.asciz "address"

LDIFF_SYM293=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,35,8,6
	.asciz "headers"

LDIFF_SYM294=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,12,6
	.asciz "identity"

LDIFF_SYM295=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,16,6
	.asciz "metadata_reader"

LDIFF_SYM296=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,20,6
	.asciz "extension_reader"

LDIFF_SYM297=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,35,24,0,7
	.asciz "System_ServiceModel_EndpointAddress"

LDIFF_SYM298=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM298
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM299=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM299
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM300=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM300
LTDIE_45:

	.byte 5
	.asciz "System_ServiceModel_Channels_AddressHeader"

	.byte 8,16
LDIFF_SYM301=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,0,0,7
	.asciz "System_ServiceModel_Channels_AddressHeader"

LDIFF_SYM302=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM302
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM303=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM303
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM304=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM304
LTDIE_46:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM305=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM305
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM306=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM306
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM307=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 2
	.asciz "System.ServiceModel.EndpointAddress:Equals"
	.long _System_ServiceModel_EndpointAddress_Equals_object
	.long Lme_12

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM308=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM308
	.byte 1,86,3
	.asciz "obj"

LDIFF_SYM309=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 1,90,11
	.asciz "other"

LDIFF_SYM310=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 2,123,0,11
	.asciz "h"

LDIFF_SYM311=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 1,84,11
	.asciz ""

LDIFF_SYM312=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM312
	.byte 2,123,4,11
	.asciz "match"

LDIFF_SYM313=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 2,123,8,11
	.asciz "o"

LDIFF_SYM314=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 1,85,11
	.asciz ""

LDIFF_SYM315=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM315
	.byte 2,123,12,11
	.asciz ""

LDIFF_SYM316=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM317=Lfde0_end - Lfde0_start
	.long LDIFF_SYM317
Lfde0_start:

	.long 0
	.align 2
	.long _System_ServiceModel_EndpointAddress_Equals_object

LDIFF_SYM318=Lme_12 - _System_ServiceModel_EndpointAddress_Equals_object
	.long LDIFF_SYM318
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_47:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM319=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM319
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM320=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM320
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM321=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM321
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM322=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM322
	.byte 2
	.asciz "System.Array:InternalArray__Insert<T>"
	.long _System_Array_InternalArray__Insert_T_int_T
	.long Lme_2c

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM323=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM323
	.byte 2,123,12,3
	.asciz "index"

LDIFF_SYM324=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 0,3
	.asciz "item"

LDIFF_SYM325=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM325
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM326=Lfde1_end - Lfde1_start
	.long LDIFF_SYM326
Lfde1_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__Insert_T_int_T

LDIFF_SYM327=Lme_2c - _System_Array_InternalArray__Insert_T_int_T
	.long LDIFF_SYM327
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Array:InternalArray__IndexOf<T>"
	.long _System_Array_InternalArray__IndexOf_T_T
	.long Lme_2e

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM328=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 1,86,3
	.asciz "item"

LDIFF_SYM329=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 1,90,11
	.asciz "length"

LDIFF_SYM330=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM330
	.byte 1,85,11
	.asciz "i"

LDIFF_SYM331=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 1,84,11
	.asciz "value"

LDIFF_SYM332=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM332
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM333=Lfde2_end - Lfde2_start
	.long LDIFF_SYM333
Lfde2_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IndexOf_T_T

LDIFF_SYM334=Lme_2e - _System_Array_InternalArray__IndexOf_T_T
	.long LDIFF_SYM334
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Array:InternalArray__get_Item<T>"
	.long _System_Array_InternalArray__get_Item_T_int
	.long Lme_2f

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM335=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM335
	.byte 2,123,16,3
	.asciz "index"

LDIFF_SYM336=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM336
	.byte 2,123,20,11
	.asciz "value"

LDIFF_SYM337=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM338=Lfde3_end - Lfde3_start
	.long LDIFF_SYM338
Lfde3_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__get_Item_T_int

LDIFF_SYM339=Lme_2f - _System_Array_InternalArray__get_Item_T_int
	.long LDIFF_SYM339
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Array:InternalArray__set_Item<T>"
	.long _System_Array_InternalArray__set_Item_T_int_T
	.long Lme_30

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM340=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM340
	.byte 1,86,3
	.asciz "index"

LDIFF_SYM341=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM341
	.byte 2,123,20,3
	.asciz "item"

LDIFF_SYM342=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM342
	.byte 2,123,24,11
	.asciz "oarray"

LDIFF_SYM343=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM343
	.byte 1,85,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM344=Lfde4_end - Lfde4_start
	.long LDIFF_SYM344
Lfde4_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__set_Item_T_int_T

LDIFF_SYM345=Lme_30 - _System_Array_InternalArray__set_Item_T_int_T
	.long LDIFF_SYM345
	.byte 12,13,0,72,14,8,135,2,68,14,28,133,7,134,6,136,5,138,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_31

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM346=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM346
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM347=Lfde5_end - Lfde5_start
	.long LDIFF_SYM347
Lfde5_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM348=Lme_31 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM348
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde5_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/System.ServiceModel/System.ServiceModel"
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "EndpointAddress.cs"

	.byte 1,0,0
	.asciz "Array.cs"

	.byte 2,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_ServiceModel_EndpointAddress_Equals_object

	.byte 3,140,1,4,2,1,3,140,1,2,44,1,3,1,2,208,0,1,3,3,2,156,1,1,8,174,3,1,2,52,1,8
	.byte 229,3,1,2,48,1,3,1,2,36,1,75,3,2,2,132,1,1,187,3,3,2,136,1,1,2,24,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__Insert_T_int_T

	.byte 3,163,1,4,3,1,3,163,1,2,40,1,2,60,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IndexOf_T_T

	.byte 3,173,1,4,3,1,3,173,1,2,36,1,3,3,2,44,1,75,188,8,117,187,131,3,4,2,36,1,8,231,3,116
	.byte 2,60,1,3,17,2,12,1,3,108,2,40,1,2,32,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__get_Item_T_int

	.byte 3,200,1,4,3,1,3,200,1,2,36,1,3,4,2,52,1,8,173,182,2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__set_Item_T_int_T

	.byte 3,210,1,4,3,1,3,210,1,2,40,1,3,3,2,36,1,3,1,2,248,0,1,187,8,61,76,3,121,2,48,1
	.byte 2,28,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,3,1,3,207,0,2,32,1,2,252,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
