	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName:
Leh_func_begin1:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheName_string
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheName_string:
Leh_func_begin2:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheFolderPath
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheFolderPath:
Leh_func_begin3:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheFolderPath_string
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheFolderPath_string:
Leh_func_begin4:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFiles
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFiles:
Leh_func_begin5:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFiles_int
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFiles_int:
Leh_func_begin6:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFileAge
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFileAge:
Leh_func_begin7:
	push	{r7}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	sub	sp, sp, #12
	bic	sp, sp, #7
	ldr	r2, [r0, #20]
	ldr	r0, [r0, #24]
	str	r0, [sp, #4]
	str	r2, [sp]
	ldr	r0, [sp]
	str	r0, [r1]
	ldr	r0, [sp, #4]
	str	r0, [r1, #4]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFileAge_System_TimeSpan
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFileAge_System_TimeSpan:
Leh_func_begin8:
	push	{r7}
Ltmp3:
	mov	r7, sp
Ltmp4:
Ltmp5:
	sub	sp, sp, #12
	bic	sp, sp, #7
	stm	sp, {r1, r2}
	ldr	r1, [sp]
	str	r1, [r0, #20]
	ldr	r1, [sp, #4]
	str	r1, [r0, #24]
	mov	sp, r7
	pop	{r7}
	bx	lr
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryFiles
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryFiles:
Leh_func_begin9:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryFiles_int
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryFiles_int:
Leh_func_begin10:
	str	r1, [r0, #28]
	bx	lr
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryBytes
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryBytes:
Leh_func_begin11:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryBytes_int
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryBytes_int:
Leh_func_begin12:
	str	r1, [r0, #32]
	bx	lr
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor:
Leh_func_begin13:
	push	{r4, r7, lr}
Ltmp6:
	add	r7, sp, #4
Ltmp7:
Ltmp8:
	sub	sp, sp, #12
	bic	sp, sp, #7
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC13_0+8))
	vmov.f64	d0, #7.000000e+00
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC13_0+8))
LPC13_0:
	add	r0, pc, r0
	ldr	r1, [r0, #16]
	ldr	r0, [r0, #20]
	str	r1, [r4, #8]
	str	r0, [r4, #12]
	mov	r0, #500
	vmov	r1, r2, d0
	str	r0, [r4, #16]
	mov	r0, sp
	bl	_p_1_plt_System_TimeSpan_FromDays_double_llvm
	ldr	r0, [sp]
	mov	r1, #30
	str	r0, [r4, #20]
	ldr	r0, [sp, #4]
	str	r0, [r4, #24]
	movw	r0, #2304
	movt	r0, #61
	str	r0, [r4, #32]
	str	r1, [r4, #28]
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__cctor:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp9:
	add	r7, sp, #8
Ltmp10:
Ltmp11:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC14_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC14_0+8))
LPC14_0:
	add	r5, pc, r5
	ldr	r0, [r5, #24]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_3_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor_llvm
	ldr	r0, [r5, #28]
	str	r4, [r0]
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage__ctor_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage__ctor_MonoTouch_UIKit_UIImage:
Leh_func_begin15:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage_GetSizeInBytes
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage_GetSizeInBytes:
Leh_func_begin16:
	push	{r4, r5, r7, lr}
Ltmp12:
	add	r7, sp, #8
Ltmp13:
Ltmp14:
	mov	r1, r0
	mov	r0, #0
	ldr	r2, [r1, #8]
	cmp	r2, #0
	popeq	{r4, r5, r7, pc}
	ldr	r0, [r1, #8]
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	mov	r4, r0
	ldr	r0, [r4]
	mov	r0, r4
	bl	_p_4_plt_MonoTouch_CoreGraphics_CGImage_get_BytesPerRow_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_5_plt_MonoTouch_CoreGraphics_CGImage_get_Height_llvm
	mul	r0, r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_Load_string_bool
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_Load_string_bool:
Leh_func_begin17:
	push	{r4, r5, r6, r7, lr}
Ltmp15:
	add	r7, sp, #12
Ltmp16:
	push	{r10}
Ltmp17:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC17_0+8))
	mov	r6, r1
	mov	r5, r0
	mov	r4, r2
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC17_0+8))
LPC17_0:
	add	r10, pc, r10
	ldr	r1, [r10, #32]
	ldr	r0, [r6]
	mov	r0, r6
	bl	_p_6_plt_string_StartsWith_string_llvm
	tst	r0, #255
	beq	LBB17_2
	ldr	r0, [r10, #32]
	ldr	r1, [r0, #8]
	mov	r0, r6
	bl	_p_8_plt_string_Substring_int_llvm
	mov	r1, r0
	mov	r0, r5
	mov	r2, r4
	bl	_p_9_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool_llvm
	b	LBB17_3
LBB17_2:
	mov	r0, r5
	mov	r1, r6
	bl	_p_7_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string_llvm
LBB17_3:
	mov	r4, r0
	ldr	r0, [r10, #36]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	str	r4, [r0, #8]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp18:
	add	r7, sp, #12
Ltmp19:
	push	{r8, r10, r11}
Ltmp20:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC18_1+8))
	mov	r10, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC18_1+8))
LPC18_1:
	add	r5, pc, r5
	ldr	r0, [r5, #40]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r5, #44]
	mov	r8, r0
	bl	_p_10_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm
	mov	r6, #0
	mov	r11, r0
	cmp	r4, #0
	str	r6, [r4, #8]
	beq	LBB18_4
	ldr	r0, [r5, #48]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	ldr	r0, [r5, #52]
	str	r4, [r2, #16]
	str	r0, [r2, #20]
	ldr	r0, [r5, #56]
	str	r0, [r2, #28]
	ldr	r0, [r5, #60]
	str	r0, [r2, #12]
	ldr	r1, [r11]
	ldr	r0, [r5, #64]
	sub	r1, r1, #16
	mov	r8, r0
	mov	r0, r11
	ldr	r3, [r1]
	mov	r1, r10
	blx	r3
	tst	r0, #255
	beq	LBB18_3
	ldr	r0, [r4, #8]
	bl	_p_12_plt_MonoTouch_Foundation_NSData_FromArray_byte___llvm
	bl	_p_13_plt_MonoTouch_UIKit_UIImage_LoadFromData_MonoTouch_Foundation_NSData_llvm
	mov	r6, r0
LBB18_3:
	mov	r0, r6
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp21:
LBB18_4:
	ldr	r0, LCPI18_0
LPC18_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_11_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI18_0:
	.long	Ltmp21-(LPC18_0+8)
	.end_data_region
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool:
Leh_func_begin19:
	push	{r7, lr}
Ltmp22:
	mov	r7, sp
Ltmp23:
Ltmp24:
	mov	r0, r1
	cmp	r2, #0
	beq	LBB19_2
	bl	_p_15_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm
	pop	{r7, pc}
LBB19_2:
	bl	_p_14_plt_MonoTouch_UIKit_UIImage_FromFileUncached_string_llvm
	pop	{r7, pc}
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__ctor:
Leh_func_begin20:
	bx	lr
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration:
Leh_func_begin21:
	push	{r4, r5, r6, r7, lr}
Ltmp25:
	add	r7, sp, #12
Ltmp26:
	push	{r10}
Ltmp27:
	mov	r4, r1
	cmp	r4, #0
	beq	LBB21_3
	beq	LBB21_3
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_2+8))
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_2+8))
	ldr	r2, [r4]
LPC21_2:
	add	r10, pc, r10
	ldr	r1, [r10, #68]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	bne	LBB21_6
LBB21_3:
	cmp	r4, #0
	beq	LBB21_5
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_1+8))
	ldr	r2, [r4]
LPC21_1:
	add	r1, pc, r1
	ldr	r1, [r1, #68]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #4]
	cmp	r2, r1
	bne	LBB21_7
LBB21_5:
	str	r4, [r0, #8]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB21_6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_3+8))
	mov	r1, #125
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC21_3+8))
LPC21_3:
	ldr	r0, [pc, r0]
	bl	_p_16_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r5, r0
	ldr	r0, [r10, #72]
	mov	r1, #1
	bl	_p_17_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r0, [r4]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r6]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r10, #76]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r2, r6
	mov	r4, r0
	bl	_p_18_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm
	mov	r0, r4
	bl	_p_19_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp28:
LBB21_7:
	ldr	r0, LCPI21_0
LPC21_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_11_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI21_0:
	.long	Ltmp28-(LPC21_0+8)
	.end_data_region
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Load
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Load:
Leh_func_begin22:
	push	{r4, r5, r7, lr}
Ltmp29:
	add	r7, sp, #8
Ltmp30:
	push	{r8}
Ltmp31:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC22_1+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC22_1+8))
LPC22_1:
	add	r5, pc, r5
	ldr	r0, [r5, #80]
	ldr	r0, [r0]
	cmp	r0, #0
	bne	LBB22_2
	ldr	r0, [r5, #136]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #140]
	str	r1, [r0, #20]
	ldr	r1, [r5, #144]
	str	r1, [r0, #28]
	ldr	r1, [r5, #148]
	str	r1, [r0, #12]
	ldr	r1, [r5, #80]
	str	r0, [r1]
	ldr	r0, [r5, #80]
	ldr	r0, [r0]
LBB22_2:
	ldr	r1, [r5, #84]
	mov	r8, r1
	bl	_p_20_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_llvm
	cmp	r4, #0
	beq	LBB22_6
	ldr	r0, [r5, #88]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #92]
	str	r4, [r0, #16]
	str	r1, [r0, #20]
	ldr	r1, [r5, #96]
	str	r1, [r0, #28]
	ldr	r1, [r5, #100]
	str	r1, [r0, #12]
	ldr	r1, [r5, #104]
	mov	r8, r1
	bl	_p_21_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_llvm
	ldr	r0, [r5, #108]
	mov	r8, r0
	bl	_p_22_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_Cirrious_MvvmCross_Plugins_DownloadCache_MvxDynamicImageHelper_1_MonoTouch_UIKit_UIImage_llvm
	ldr	r0, [r5, #112]
	ldr	r0, [r0]
	cmp	r0, #0
	bne	LBB22_5
	ldr	r0, [r5, #120]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r5, #124]
	str	r1, [r0, #20]
	ldr	r1, [r5, #128]
	str	r1, [r0, #28]
	ldr	r1, [r5, #132]
	str	r1, [r0, #12]
	ldr	r1, [r5, #112]
	str	r0, [r1]
	ldr	r0, [r5, #112]
	ldr	r0, [r0]
LBB22_5:
	ldr	r1, [r5, #116]
	mov	r8, r1
	bl	_p_23_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Ltmp32:
LBB22_6:
	ldr	r0, LCPI22_0
LPC22_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_11_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI22_0:
	.long	Ltmp32-(LPC22_0+8)
	.end_data_region
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__ctor:
Leh_func_begin23:
	bx	lr
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__0
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__0:
Leh_func_begin24:
	push	{r4, r7, lr}
Ltmp33:
	add	r7, sp, #4
Ltmp34:
Ltmp35:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC24_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC24_0+8))
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #152]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #20
	mov	r4, r0
	bl	_p_24_plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxHttpFileDownloader__ctor_int_llvm
	mov	r0, r4
	pop	{r4, r7, pc}
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__1
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__1:
Leh_func_begin25:
	push	{r7, lr}
Ltmp36:
	mov	r7, sp
Ltmp37:
Ltmp38:
	bl	_p_25_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache_llvm
	pop	{r7, pc}
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__2
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__2:
Leh_func_begin26:
	push	{r7, lr}
Ltmp39:
	mov	r7, sp
Ltmp40:
Ltmp41:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC26_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC26_0+8))
LPC26_0:
	add	r0, pc, r0
	ldr	r0, [r0, #156]
	bl	_p_26_plt__jit_icall_mono_object_new_ptrfree_llvm
	pop	{r7, pc}
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__ctor:
Leh_func_begin27:
	bx	lr
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__LoadUIImageb__0_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__LoadUIImageb__0_System_IO_Stream:
Leh_func_begin28:
	push	{r4, r5, r6, r7, lr}
Ltmp42:
	add	r7, sp, #12
Ltmp43:
Ltmp44:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC28_0+8))
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC28_0+8))
LPC28_0:
	add	r0, pc, r0
	ldr	r0, [r0, #160]
	bl	_p_2_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	bl	_p_27_plt_System_IO_MemoryStream__ctor_llvm
	ldr	r0, [r5]
	mov	r1, r6
	mov	r0, r5
	bl	_p_28_plt_System_IO_Stream_CopyTo_System_IO_Stream_llvm
	ldr	r0, [r6]
	ldr	r1, [r0, #140]
	mov	r0, r6
	blx	r1
	str	r0, [r4, #8]
	mov	r0, #1
	pop	{r4, r5, r6, r7, pc}
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin29:
	push	{r4, r5, r7, lr}
Ltmp45:
	add	r7, sp, #8
Ltmp46:
Ltmp47:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC29_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC29_0+8))
LPC29_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB29_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB29_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB29_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB29_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB29_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB29_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end29:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_invoke_TResult__this__:
Leh_func_begin30:
	push	{r4, r7, lr}
Ltmp48:
	add	r7, sp, #4
Ltmp49:
Ltmp50:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC30_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC30_0+8))
LPC30_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB30_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB30_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB30_4
	ldr	r1, [r0, #12]
	blx	r1
LBB30_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB30_6
	blx	r1
	pop	{r4, r7, pc}
LBB30_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end30:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__:
Leh_func_begin31:
	push	{r4, r7, lr}
Ltmp51:
	add	r7, sp, #4
Ltmp52:
Ltmp53:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC31_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC31_0+8))
LPC31_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB31_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB31_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB31_4
	ldr	r1, [r0, #12]
	blx	r1
LBB31_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB31_6
	blx	r1
	pop	{r4, r7, pc}
LBB31_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end31:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__:
Leh_func_begin32:
	push	{r4, r7, lr}
Ltmp54:
	add	r7, sp, #4
Ltmp55:
Ltmp56:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC32_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC32_0+8))
LPC32_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB32_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB32_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB32_4
	ldr	r1, [r0, #12]
	blx	r1
LBB32_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB32_6
	blx	r1
	pop	{r4, r7, pc}
LBB32_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end32:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage:
Leh_func_begin33:
	push	{r4, r5, r6, r7, lr}
Ltmp57:
	add	r7, sp, #12
Ltmp58:
	push	{r10, r11}
Ltmp59:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC33_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC33_0+8))
LPC33_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB33_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB33_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB33_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB33_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB33_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB33_7
LBB33_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB33_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end33:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage:
Leh_func_begin34:
	push	{r4, r5, r6, r7, lr}
Ltmp60:
	add	r7, sp, #12
Ltmp61:
	push	{r10, r11}
Ltmp62:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC34_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC34_0+8))
LPC34_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB34_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB34_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB34_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB34_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB34_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB34_7
LBB34_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB34_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end34:

	.private_extern	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_int_invoke_TResult__this___T_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
	.align	2
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_int_invoke_TResult__this___T_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage:
Leh_func_begin35:
	push	{r4, r5, r7, lr}
Ltmp63:
	add	r7, sp, #8
Ltmp64:
Ltmp65:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC35_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got-(LPC35_0+8))
LPC35_0:
	add	r0, pc, r0
	ldr	r0, [r0, #164]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB35_2
	bl	_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB35_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB35_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB35_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB35_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB35_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end35:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got,304,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheName_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheFolderPath
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheFolderPath_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFiles
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFiles_int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFileAge
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFileAge_System_TimeSpan
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryFiles
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryFiles_int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryBytes
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryBytes_int
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage__ctor_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage_GetSizeInBytes
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_Load_string_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Load
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__0
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__1
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__2
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__LoadUIImageb__0_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_invoke_TResult__this__
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_int_invoke_TResult__this___T_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	36
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	6
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	7
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	8
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	9
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	10
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	11
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	12
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	13
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	14
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	15
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	16
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	17
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	18
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	19
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	20
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	21
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	23
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	24
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	25
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	26
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	27
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	28
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	34
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	35
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	36
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	37
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	42
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	47
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	52
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
Lset36 = Leh_func_end35-Leh_func_begin35
	.long	Lset36
Lset37 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset37
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	4
	.byte	135
	.byte	1
	.byte	12
	.byte	7
	.byte	4

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin31:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin32:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin33:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin34:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.DownloadCache.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache
_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache:

	.byte 128,64,45,233,13,112,160,225,64,13,45,233,64,208,77,226,13,176,160,225,0,160,160,225,8,0,154,229,0,160,160,225
	.byte 0,0,80,227,4,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 16
	.byte 0,0,159,231,0,160,144,229,10,96,160,225,0,224,218,229,8,0,154,229,52,0,139,229,0,224,218,229,12,0,154,229
	.byte 56,0,139,229,0,224,218,229,16,0,154,229,60,0,139,229,0,224,218,229,20,0,138,226,0,16,144,229,20,16,139,229
	.byte 4,0,144,229,24,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 160
	.byte 0,0,159,231
bl _p_2

	.byte 52,16,155,229,56,32,155,229,60,48,155,229,48,0,139,229,20,192,155,229,0,192,141,229,24,192,155,229,4,192,141,229
bl _p_31

	.byte 48,0,155,229,8,0,139,229,36,0,139,229,0,224,218,229,28,0,154,229,40,0,139,229,0,224,218,229,32,0,154,229
	.byte 44,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 156
	.byte 0,0,159,231
bl _p_2

	.byte 36,16,155,229,40,32,155,229,44,48,155,229,32,0,139,229
bl _p_30

	.byte 32,0,155,229,12,0,139,229,16,0,139,229,64,208,139,226,64,13,189,232,128,128,189,232

Lme_16:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheName_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheFolderPath
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheFolderPath_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFiles
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFiles_int
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFileAge
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFileAge_System_TimeSpan
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryFiles
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryFiles_int
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryBytes
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryBytes_int
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage__ctor_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage_GetSizeInBytes
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_Load_string_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Load
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__0
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__1
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__2
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__LoadUIImageb__0_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_invoke_TResult__this__
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
.no_dead_strip _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_int_invoke_TResult__this___T_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheName_string
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheFolderPath
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_CacheFolderPath_string
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFiles
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFiles_int
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxFileAge
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxFileAge_System_TimeSpan
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryFiles
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryFiles_int
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_MaxInMemoryBytes
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_set_MaxInMemoryBytes_int
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__cctor
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage__ctor_MonoTouch_UIKit_UIImage
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchImage_GetSizeInBytes
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_Load_string_bool
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Configure_Cirrious_CrossCore_Plugins_IMvxPluginConfiguration
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_Load
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__ctor
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__0
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__1
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin__Loadb__2
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader__c__DisplayClass1__LoadUIImageb__0_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_invoke_TResult__this__
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_invoke_TResult__this__
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_System_Collections_Generic_List_1_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_CallbackPair_MonoTouch_UIKit_UIImage
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_System_Collections_Generic_KeyValuePair_2_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_invoke_TRet__this___TKey_TValue_string_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__wrapper_delegate_invoke_System_Func_2_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage_int_invoke_TResult__this___T_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_Entry_MonoTouch_UIKit_UIImage
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 53,10,6,2
	.short 0, 10, 20, 34, 48, 68
	.byte 1,3,3,3,3,3,3,3,3,3,31,3,3,5,5,2,2,5,9,2,69,6,24,5,2,3,2,3,2,255,255,255
	.byte 255,140,0,0,0,0,119,3,3,3,255,255,255,255,128,0,0,0,128,131,255,255,255,255,125,0,0,0,128,134,255,255
	.byte 255,255,122,0,0,0,128,137
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,232,42,12,0
	.long 0,0,0,0,0,0,0,0
	.long 152,35,13,140,34,0,0,0
	.long 0,0,0,0,164,36,11,0
	.long 0,0,176,37,0,278,47,0
	.long 301,52,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 23,30,0,31,0,32,0,33
	.long 0,34,140,35,152,36,164,37
	.long 176,38,0,39,0,40,0,41
	.long 0,42,232,43,0,44,0,45
	.long 0,46,0,47,278,48,0,49
	.long 0,50,0,51,0,52,301
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 11, 0, 0, 0, 0, 4
	.short 0, 0, 0, 0, 0, 5, 0, 3
	.short 0, 0, 0, 2, 0, 0, 0, 6
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 44,10,5,2
	.short 0, 11, 22, 33, 44
	.byte 129,55,2,1,1,1,3,3,3,4,3,129,79,3,12,6,2,2,6,5,3,7,129,129,4,12,6,2,2,6,12,12
	.byte 4,129,201,6,2,2,6,6,2,2,6,4,129,240,5,1,6
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 53,10,6,2
	.short 0, 11, 22, 37, 52, 72
	.byte 131,99,3,3,3,3,3,3,3,3,3,131,129,3,3,3,3,3,3,3,3,3,131,159,3,3,3,3,3,3,3,3
	.byte 255,255,255,252,73,0,0,0,0,131,186,3,3,3,255,255,255,252,61,0,0,0,131,198,255,255,255,252,58,0,0,0
	.byte 131,201,255,255,255,252,55,0,0,0,131,204
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 27,12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 6,10,1,2
	.short 0
	.byte 131,207,7,24,24,24,25

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_plt:
_p_1_plt_System_TimeSpan_FromDays_double_llvm:
	.no_dead_strip plt_System_TimeSpan_FromDays_double
plt_System_TimeSpan_FromDays_double:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 176,512
_p_2_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 180,517
_p_3_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor
plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration__ctor:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 184,540
_p_4_plt_MonoTouch_CoreGraphics_CGImage_get_BytesPerRow_llvm:
	.no_dead_strip plt_MonoTouch_CoreGraphics_CGImage_get_BytesPerRow
plt_MonoTouch_CoreGraphics_CGImage_get_BytesPerRow:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 188,542
_p_5_plt_MonoTouch_CoreGraphics_CGImage_get_Height_llvm:
	.no_dead_strip plt_MonoTouch_CoreGraphics_CGImage_get_Height
plt_MonoTouch_CoreGraphics_CGImage_get_Height:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 192,547
_p_6_plt_string_StartsWith_string_llvm:
	.no_dead_strip plt_string_StartsWith_string
plt_string_StartsWith_string:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 196,552
_p_7_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string
plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadUIImage_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 200,557
_p_8_plt_string_Substring_int_llvm:
	.no_dead_strip plt_string_Substring_int
plt_string_Substring_int:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 204,559

.set _p_9_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool_llvm, _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxTouchLocalFileImageLoader_LoadResourceImage_string_bool
_p_10_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 212,566
_p_11_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 216,578
_p_12_plt_MonoTouch_Foundation_NSData_FromArray_byte___llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSData_FromArray_byte__
plt_MonoTouch_Foundation_NSData_FromArray_byte__:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 220,623
_p_13_plt_MonoTouch_UIKit_UIImage_LoadFromData_MonoTouch_Foundation_NSData_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_LoadFromData_MonoTouch_Foundation_NSData
plt_MonoTouch_UIKit_UIImage_LoadFromData_MonoTouch_Foundation_NSData:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 224,628
_p_14_plt_MonoTouch_UIKit_UIImage_FromFileUncached_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_FromFileUncached_string
plt_MonoTouch_UIKit_UIImage_FromFileUncached_string:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 228,633
_p_15_plt_MonoTouch_UIKit_UIImage_FromFile_string_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIImage_FromFile_string
plt_MonoTouch_UIKit_UIImage_FromFile_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 232,638
_p_16_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 236,643
_p_17_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 240,663
_p_18_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_object__:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 244,689
_p_19_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 248,694
_p_20_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxHttpFileDownloader:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 252,722
_p_21_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxImageCache_1_MonoTouch_UIKit_UIImage:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 256,734
_p_22_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_Cirrious_MvvmCross_Plugins_DownloadCache_MvxDynamicImageHelper_1_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_Cirrious_MvvmCross_Plugins_DownloadCache_MvxDynamicImageHelper_1_MonoTouch_UIKit_UIImage
plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_CrossCore_Platform_IMvxImageHelper_1_MonoTouch_UIKit_UIImage_Cirrious_MvvmCross_Plugins_DownloadCache_MvxDynamicImageHelper_1_MonoTouch_UIKit_UIImage:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 260,746
_p_23_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage_System_Func_1_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxLocalFileImageLoader_1_MonoTouch_UIKit_UIImage:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 264,758
_p_24_plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxHttpFileDownloader__ctor_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxHttpFileDownloader__ctor_int
plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxHttpFileDownloader__ctor_int:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 268,770
_p_25_plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache
plt_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 272,775
_p_26_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 276,777
_p_27_plt_System_IO_MemoryStream__ctor_llvm:
	.no_dead_strip plt_System_IO_MemoryStream__ctor
plt_System_IO_MemoryStream__ctor:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 280,803
_p_28_plt_System_IO_Stream_CopyTo_System_IO_Stream_llvm:
	.no_dead_strip plt_System_IO_Stream_CopyTo_System_IO_Stream
plt_System_IO_Stream_CopyTo_System_IO_Stream:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 284,808
_p_29_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 288,813
_p_30_plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_MonoTouch_UIKit_UIImage__ctor_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxFileDownloadCache_int_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_MonoTouch_UIKit_UIImage__ctor_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxFileDownloadCache_int_int
plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache_1_MonoTouch_UIKit_UIImage__ctor_Cirrious_MvvmCross_Plugins_DownloadCache_IMvxFileDownloadCache_int_int:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 292,851
_p_31_plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxFileDownloadCache__ctor_string_string_int_System_TimeSpan_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxFileDownloadCache__ctor_string_string_int_System_TimeSpan
plt_Cirrious_MvvmCross_Plugins_DownloadCache_MvxFileDownloadCache__ctor_string_string_int_System_TimeSpan:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got - . + 296,862
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 6
	.asciz "Cirrious.MvvmCross.Plugins.DownloadCache.Touch"
	.asciz "5FD8F0B6-0979-4C9A-8092-EE22179C11CA"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Plugins.DownloadCache"
	.asciz "7053059C-761F-4C09-B0A7-0B182E0B451D"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.File"
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "5FD8F0B6-0979-4C9A-8092-EE22179C11CA"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.DownloadCache.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_DownloadCache_Touch__Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration_get_CacheName
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 44,304,32,53,11,387000831,0,1102
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_DownloadCache_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1,2,0,1
	.byte 2,0,1,2,0,1,2,2,5,4,1,2,2,7,6,0,0,0,0,0,3,8,8,9,0,7,10,16,15,14,13,12
	.byte 11,0,0,0,0,0,4,17,19,18,17,0,22,20,37,36,35,34,28,33,32,31,30,29,28,28,27,26,25,24,23,22
	.byte 21,20,20,0,3,7,43,42,0,0,0,1,38,0,0,0,1,39,0,0,0,1,40,0,1,41,0,1,41,0,1,41
	.byte 0,1,41,0,1,41,0,1,41,0,1,41,255,252,0,0,0,1,1,3,219,0,0,3,255,252,0,0,0,1,1,3
	.byte 219,0,0,4,255,252,0,0,0,1,1,3,219,0,0,5,255,252,0,0,0,1,1,3,219,0,0,6,4,2,31,2
	.byte 1,2,128,194,3,4,2,120,1,1,7,128,188,4,2,119,1,2,2,130,146,1,7,128,197,4,2,97,1,3,2,130
	.byte 146,1,7,128,197,7,128,205,255,252,0,0,0,1,1,7,128,217,4,2,32,2,1,2,128,194,3,4,2,119,1,2
	.byte 2,130,146,1,7,128,242,4,2,97,1,3,2,130,146,1,7,128,242,7,128,251,255,252,0,0,0,1,1,7,129,7
	.byte 4,2,130,70,1,2,7,128,242,2,130,90,1,255,252,0,0,0,1,1,7,129,32,12,0,39,42,47,17,0,1,17
	.byte 0,39,14,1,2,16,1,2,1,17,0,115,14,1,3,14,1,6,34,255,254,0,0,0,0,255,43,0,0,1,14,3
	.byte 219,0,0,3,6,29,50,29,30,3,219,0,0,3,6,197,0,0,3,11,1,2,14,6,1,2,130,123,1,14,2,30
	.byte 4,16,1,5,10,34,255,254,0,0,0,0,255,43,0,0,2,14,3,219,0,0,5,6,26,50,26,30,3,219,0,0
	.byte 5,34,255,254,0,0,0,0,255,43,0,0,3,34,255,254,0,0,0,0,255,43,0,0,4,16,1,5,11,34,255,254
	.byte 0,0,0,0,255,43,0,0,5,14,3,219,0,0,6,6,27,50,27,30,3,219,0,0,6,14,3,219,0,0,4,6
	.byte 25,50,25,30,3,219,0,0,4,14,2,25,2,14,1,4,14,2,128,217,1,33,14,3,219,0,0,7,14,2,8,2
	.byte 3,193,0,19,245,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,13,3,195
	.byte 0,1,190,3,195,0,1,189,3,193,0,19,107,3,18,3,193,0,19,53,3,19,3,255,254,0,0,0,0,255,43,0
	.byte 0,1,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95
	.byte 97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,3,195,0,0,121,3,195,0,4,208,3,195,0,4,205,3,195
	.byte 0,4,207,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,23,109,111,110,111,95,97,114
	.byte 114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,196,0,0,78,7,25,109,111,110,111,95,97,114,99
	.byte 104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,255,254,0,0,0,0,255,43,0,0,2,3,255
	.byte 254,0,0,0,0,255,43,0,0,3,3,255,254,0,0,0,0,255,43,0,0,4,3,255,254,0,0,0,0,255,43,0
	.byte 0,5,3,194,0,0,110,3,23,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114
	.byte 101,101,0,3,193,0,6,241,3,193,0,7,79,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114
	.byte 114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,255,254,0,0,0,0,202,0,0,44,3,194
	.byte 0,0,33,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,4,128,228,14,36,4,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,5,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,16,5,128,200,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,17,6,128
	.byte 224,12,8,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,21,22,4,128,160,12,0,0,4,193,0
	.byte 18,178,193,0,18,175,193,0,18,174,193,0,18,172,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_4:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_3:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM10=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM12=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_2:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration"

	.byte 36,16
LDIFF_SYM15=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,35,0,6
	.asciz "<CacheName>k__BackingField"

LDIFF_SYM16=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,8,6
	.asciz "<CacheFolderPath>k__BackingField"

LDIFF_SYM17=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM17
	.byte 2,35,12,6
	.asciz "<MaxFiles>k__BackingField"

LDIFF_SYM18=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,16,6
	.asciz "<MaxFileAge>k__BackingField"

LDIFF_SYM19=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,20,6
	.asciz "<MaxInMemoryFiles>k__BackingField"

LDIFF_SYM20=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,28,6
	.asciz "<MaxInMemoryBytes>k__BackingField"

LDIFF_SYM21=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,32,0,7
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_Touch_MvxDownloadCacheConfiguration"

LDIFF_SYM22=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM23=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM24=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin"

	.byte 12,16
LDIFF_SYM25=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,0,6
	.asciz "_configuration"

LDIFF_SYM26=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM26
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin"

LDIFF_SYM27=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM27
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM28=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM28
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM29=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM29
LTDIE_6:

	.byte 5
	.asciz "Cirrious_CrossCore_Core_MvxLockableObject"

	.byte 12,16
LDIFF_SYM30=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,0,6
	.asciz "_lockObject"

LDIFF_SYM31=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,8,0,7
	.asciz "Cirrious_CrossCore_Core_MvxLockableObject"

LDIFF_SYM32=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM32
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM33=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM33
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM34=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM34
LTDIE_7:

	.byte 17
	.asciz "Cirrious_CrossCore_Platform_IMvxTextSerializer"

	.byte 8,7
	.asciz "Cirrious_CrossCore_Platform_IMvxTextSerializer"

LDIFF_SYM35=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM35
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM36=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM37=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_8:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM38=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM39=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM40=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM40
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM41=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM42=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM42
LTDIE_10:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM43=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM44=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM44
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM45=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM45
LTDIE_9:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM46=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM47=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM48=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM49=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM50=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM50
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM51=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM51
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM52=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM53=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM54=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM55=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM55
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM56=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM56
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM57=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM58=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM59=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_11:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM60=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM60
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM61=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM62=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM63=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM63
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM64=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM64
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM65=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM66=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM67=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM68=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM69=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM69
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM70=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM70
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM71=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM72=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM72
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM73=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_12:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM74=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM75=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM76=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM77=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM78=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM79=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM80=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_14:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM81=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM82=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM83=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM83
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM84=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM84
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM85=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM85
LTDIE_20:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM86=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM87=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM87
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM88=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM89=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_19:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM90=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM91=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM91
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM92=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM93=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_18:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM94=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM95=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM95
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM96=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM96
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM97=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_22:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM98=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM98
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM99=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM99
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM100=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_21:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM103=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM104=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM105=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM106=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM106
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM107=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM108=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_17:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM109=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM110=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM111=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM112=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM113=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM113
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM114=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM115=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM116=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM116
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM117=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM118=LTDIE_21_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM119=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM119
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM120=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM120
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM121=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM121
LTDIE_16:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM122=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM123=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM123
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM124=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM125=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM126=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM126
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM127=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_15:

	.byte 5
	.asciz "System_Threading_TimerCallback"

	.byte 52,16
LDIFF_SYM128=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM128
	.byte 2,35,0,0,7
	.asciz "System_Threading_TimerCallback"

LDIFF_SYM129=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM130=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM130
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM131=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM131
LTDIE_23:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM132=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM132
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM133=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM133
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM134=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM134
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM135=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM135
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM136=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM136
LTDIE_13:

	.byte 5
	.asciz "System_Threading_Timer"

	.byte 48,16
LDIFF_SYM137=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM137
	.byte 2,35,0,6
	.asciz "callback"

LDIFF_SYM138=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM138
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM139=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM139
	.byte 2,35,16,6
	.asciz "due_time_ms"

LDIFF_SYM140=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM140
	.byte 2,35,20,6
	.asciz "period_ms"

LDIFF_SYM141=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM141
	.byte 2,35,28,6
	.asciz "next_run"

LDIFF_SYM142=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM142
	.byte 2,35,36,6
	.asciz "disposed"

LDIFF_SYM143=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM143
	.byte 2,35,44,0,7
	.asciz "System_Threading_Timer"

LDIFF_SYM144=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM144
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM145=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM146=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_5:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_MvxFileDownloadCache"

	.byte 60,16
LDIFF_SYM147=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM147
	.byte 2,35,0,6
	.asciz "_textConvert"

LDIFF_SYM148=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM148
	.byte 2,35,12,6
	.asciz "_textConvertTried"

LDIFF_SYM149=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,40,6
	.asciz "_cacheFolder"

LDIFF_SYM150=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,16,6
	.asciz "_cacheName"

LDIFF_SYM151=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,20,6
	.asciz "_maxFileCount"

LDIFF_SYM152=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM152
	.byte 2,35,44,6
	.asciz "_maxFileAge"

LDIFF_SYM153=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM153
	.byte 2,35,48,6
	.asciz "_entriesByHttpUrl"

LDIFF_SYM154=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM154
	.byte 2,35,24,6
	.asciz "_currentlyRequested"

LDIFF_SYM155=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,28,6
	.asciz "_toDeleteFiles"

LDIFF_SYM156=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM156
	.byte 2,35,32,6
	.asciz "_periodicTaskTimer"

LDIFF_SYM157=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM157
	.byte 2,35,36,6
	.asciz "_indexNeedsSaving"

LDIFF_SYM158=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM158
	.byte 2,35,56,0,7
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_MvxFileDownloadCache"

LDIFF_SYM159=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM159
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM160=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM160
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM161=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM161
LTDIE_26:

	.byte 5
	.asciz "Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject"

	.byte 8,16
LDIFF_SYM162=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 2,35,0,0,7
	.asciz "Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject"

LDIFF_SYM163=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM163
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM164=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM164
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM165=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM165
LTDIE_25:

	.byte 5
	.asciz "Cirrious_CrossCore_Core_MvxAllThreadDispatchingObject"

	.byte 12,16
LDIFF_SYM166=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM166
	.byte 2,35,0,6
	.asciz "_lockObject"

LDIFF_SYM167=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM167
	.byte 2,35,8,0,7
	.asciz "Cirrious_CrossCore_Core_MvxAllThreadDispatchingObject"

LDIFF_SYM168=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM169=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM169
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM170=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM170
LTDIE_27:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM171=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM172=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM172
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM173=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM173
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM174=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM174
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM175=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM175
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM176=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM177=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM178=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM178
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM179=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM179
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM180=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM180
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM181=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM182=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM182
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM183=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM183
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM184=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM184
LTDIE_28:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM185=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM185
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM186=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM186
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM187=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM187
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM188=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM189=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM190=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM190
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM191=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM191
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM192=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM192
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM193=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM194=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM194
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM195=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM195
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM196=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM196
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM197=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM197
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM198=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_29:

	.byte 17
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_IMvxFileDownloadCache"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_IMvxFileDownloadCache"

LDIFF_SYM199=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM200=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM200
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM201=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM201
LTDIE_24:

	.byte 5
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache`1"

	.byte 32,16
LDIFF_SYM202=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM202
	.byte 2,35,0,6
	.asciz "_currentlyRequested"

LDIFF_SYM203=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM203
	.byte 2,35,12,6
	.asciz "_entriesByHttpUrl"

LDIFF_SYM204=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM204
	.byte 2,35,16,6
	.asciz "_fileDownloadCache"

LDIFF_SYM205=LTDIE_29_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,20,6
	.asciz "_maxInMemoryBytes"

LDIFF_SYM206=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,24,6
	.asciz "_maxInMemoryFiles"

LDIFF_SYM207=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,28,0,7
	.asciz "Cirrious_MvvmCross_Plugins_DownloadCache_MvxImageCache`1"

LDIFF_SYM208=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM208
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM209=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM209
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM210=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2
	.asciz "Cirrious.MvvmCross.Plugins.DownloadCache.Touch.Plugin:CreateCache"
	.long _Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache
	.long Lme_16

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM211=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM211
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM212=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM212
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM213=LTDIE_5_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM213
	.byte 2,123,8,11
	.asciz "V_2"

LDIFF_SYM214=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 2,123,12,11
	.asciz "V_3"

LDIFF_SYM215=LTDIE_24_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM215
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM216=Lfde0_end - Lfde0_start
	.long LDIFF_SYM216
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache

LDIFF_SYM217=Lme_16 - _Cirrious_MvvmCross_Plugins_DownloadCache_Touch_Plugin_CreateCache
	.long LDIFF_SYM217
	.byte 12,13,0,72,14,8,135,2,68,14,24,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
