	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor:
Leh_func_begin1:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
	push	{r8}
Ltmp2:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC1_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC1_0+8))
LPC1_0:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	mov	r8, r0
	bl	_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_llvm
	str	r0, [r4, #8]
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_ComposeEmail_string_string_string_string_bool
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_ComposeEmail_string_string_string_string_bool:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
	push	{r8, r10, r11}
Ltmp5:
	sub	sp, sp, #8
	mov	r5, r3
	str	r2, [sp]
	str	r1, [sp, #4]
	mov	r4, r0
	bl	_p_2_plt_MonoTouch_MessageUI_MFMailComposeViewController_get_CanSendMail_llvm
	tst	r0, #255
	beq	LBB2_5
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC2_1+8))
	ldr	r11, [r7, #8]
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC2_1+8))
LPC2_1:
	add	r10, pc, r10
	ldr	r0, [r10, #20]
	bl	_p_3_plt__jit_icall_mono_object_new_specific_llvm
	mov	r6, r0
	bl	_p_4_plt_MonoTouch_MessageUI_MFMailComposeViewController__ctor_llvm
	str	r6, [r4, #12]
	cmp	r11, #0
	ldr	r2, [r7, #12]
	ldr	r0, [r4, #12]
	ldreq	r1, [r10, #48]
	ldreq	r11, [r1]
	ldr	r1, [r0]
	ldr	r3, [r1, #220]
	mov	r1, r11
	blx	r3
	cmp	r5, #0
	ldr	r0, [r4, #12]
	ldreq	r1, [r10, #48]
	ldreq	r5, [r1]
	ldr	r1, [r0]
	ldr	r2, [r1, #232]
	mov	r1, r5
	blx	r2
	ldr	r5, [r4, #12]
	ldr	r0, [r10, #24]
	mov	r1, #1
	bl	_p_5_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r2, [sp]
	mov	r6, r0
	cmp	r2, #0
	bne	LBB2_3
	ldr	r0, [r10, #48]
	ldr	r2, [r0]
LBB2_3:
	ldr	r0, [r6]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r5]
	mov	r1, r6
	ldr	r2, [r0, #224]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #12]
	ldr	r0, [r10, #24]
	mov	r1, #1
	bl	_p_5_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r2, [sp, #4]
	mov	r6, r0
	mov	r1, #0
	cmp	r2, #0
	ldreq	r0, [r10, #48]
	ldreq	r2, [r0]
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	ldr	r0, [r5]
	mov	r1, r6
	ldr	r2, [r0, #228]
	mov	r0, r5
	blx	r2
	ldr	r5, [r4, #12]
	cmp	r4, #0
	beq	LBB2_6
	ldr	r0, [r10, #28]
	bl	_p_7_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r10, #32]
	str	r0, [r1, #20]
	ldr	r0, [r10, #36]
	str	r0, [r1, #28]
	ldr	r0, [r10, #40]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_8_plt_MonoTouch_MessageUI_MFMailComposeViewController_add_Finished_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_llvm
	ldr	r0, [r4, #8]
	ldr	r1, [r4, #12]
	ldr	r2, [r10, #44]
	ldr	r3, [r0]
	mov	r8, r2
	mov	r2, #1
	sub	r3, r3, #68
	ldr	r3, [r3]
	blx	r3
LBB2_5:
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp6:
LBB2_6:
	ldr	r0, LCPI2_0
LPC2_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_6_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI2_0:
	.long	Ltmp6-(LPC2_0+8)
	.end_data_region
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_HandleMailFinished_object_MonoTouch_MessageUI_MFComposeResultEventArgs
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_HandleMailFinished_object_MonoTouch_MessageUI_MFComposeResultEventArgs:
Leh_func_begin3:
	push	{r4, r5, r6, r7, lr}
Ltmp7:
	add	r7, sp, #12
Ltmp8:
	push	{r8, r10}
Ltmp9:
	mov	r5, r1
	mov	r10, r0
	cmp	r5, #0
	beq	LBB3_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_0+8))
	ldr	r1, [r5]
LPC3_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	movne	r5, #0
LBB3_2:
	cmp	r5, #0
	beq	LBB3_6
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_1+8))
LPC3_1:
	add	r6, pc, r6
	ldr	r4, [r6, #52]
	ldr	r2, [r4]
	cmp	r2, #0
	bne	LBB3_5
	ldr	r0, [r6, #60]
	bl	_p_7_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #64]
	str	r1, [r0, #20]
	ldr	r1, [r6, #68]
	str	r1, [r0, #28]
	ldr	r1, [r6, #72]
	str	r1, [r0, #12]
	str	r0, [r4]
	ldr	r0, [r6, #52]
	ldr	r2, [r0]
LBB3_5:
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r3, [r0, #100]
	mov	r0, r5
	blx	r3
	ldr	r0, [r10, #8]
	ldr	r2, [r0]
	ldr	r1, [r6, #56]
	sub	r2, r2, #24
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
LBB3_6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_2+8))
	mov	r1, #1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC3_2+8))
LPC3_2:
	ldr	r0, [pc, r0]
	bl	_p_9_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_10_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__HandleMailFinishedb__0
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__HandleMailFinishedb__0:
Leh_func_begin4:
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin_Load
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin_Load:
Leh_func_begin5:
	push	{r7, lr}
Ltmp10:
	mov	r7, sp
Ltmp11:
	push	{r8}
Ltmp12:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC5_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC5_0+8))
LPC5_0:
	add	r0, pc, r0
	ldr	r0, [r0, #80]
	mov	r8, r0
	bl	_p_12_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin__ctor:
Leh_func_begin6:
	bx	lr
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_Email_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_MessageUI_MFComposeResultEventArgs
	.align	2
_Cirrious_MvvmCross_Plugins_Email_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_MessageUI_MFComposeResultEventArgs:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp13:
	add	r7, sp, #12
Ltmp14:
Ltmp15:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC7_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got-(LPC7_0+8))
LPC7_0:
	add	r0, pc, r0
	ldr	r0, [r0, #84]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB7_2
	bl	_p_13_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB7_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB7_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB7_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB7_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB7_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end7:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got,144,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_ComposeEmail_string_string_string_string_bool
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_HandleMailFinished_object_MonoTouch_MessageUI_MFComposeResultEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__HandleMailFinishedb__0
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin_Load
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_Email_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_MessageUI_MFComposeResultEventArgs
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	8
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
Lset8 = Leh_func_end7-Leh_func_begin7
	.long	Lset8
Lset9 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset9
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.Email.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_ComposeEmail_string_string_string_string_bool
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_HandleMailFinished_object_MonoTouch_MessageUI_MFComposeResultEventArgs
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__HandleMailFinishedb__0
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin_Load
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_Email_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_MessageUI_MFComposeResultEventArgs

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_ComposeEmail_string_string_string_string_bool
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_HandleMailFinished_object_MonoTouch_MessageUI_MFComposeResultEventArgs
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__HandleMailFinishedb__0
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin_Load
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_Plugin__ctor
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_Email_Touch__wrapper_delegate_invoke_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_invoke_void__this___object_TEventArgs_object_MonoTouch_MessageUI_MFComposeResultEventArgs
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 1,3,14,11,2,3,255,255,255,255,222,36
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,39
	.long 7,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 1,7,39
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 0
	.short 0, 2, 0, 3, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 22,10,3,2
	.short 0, 10, 20
	.byte 51,2,1,1,1,12,5,6,6,5,95,6,5,6,5,5,4,5,5,4,128,145,12
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 8,10,1,2
	.short 0
	.byte 129,181,3,3,3,3,3,255,255,255,254,60,129,199
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 3,10,1,2
	.short 0
	.byte 129,202,7,19

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_plt:
_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 88,158
_p_2_plt_MonoTouch_MessageUI_MFMailComposeViewController_get_CanSendMail_llvm:
	.no_dead_strip plt_MonoTouch_MessageUI_MFMailComposeViewController_get_CanSendMail
plt_MonoTouch_MessageUI_MFMailComposeViewController_get_CanSendMail:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 92,170
_p_3_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 96,175
_p_4_plt_MonoTouch_MessageUI_MFMailComposeViewController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_MessageUI_MFMailComposeViewController__ctor
plt_MonoTouch_MessageUI_MFMailComposeViewController__ctor:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 100,202
_p_5_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 104,207
_p_6_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 108,233
_p_7_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 112,278
_p_8_plt_MonoTouch_MessageUI_MFMailComposeViewController_add_Finished_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs_llvm:
	.no_dead_strip plt_MonoTouch_MessageUI_MFMailComposeViewController_add_Finished_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs
plt_MonoTouch_MessageUI_MFMailComposeViewController_add_Finished_System_EventHandler_1_MonoTouch_MessageUI_MFComposeResultEventArgs:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 116,301
_p_9_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 120,306
_p_10_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 124,326
_p_11_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 128,359
_p_12_plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask
plt_Cirrious_CrossCore_Mvx_RegisterType_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 132,387
_p_13_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got - . + 136,399
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Plugins.Email.Touch"
	.asciz "A4EDEB49-8F3D-4E66-8A0B-A5A8FCB32458"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "Cirrious.CrossCore.Touch"
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "A4EDEB49-8F3D-4E66-8A0B-A5A8FCB32458"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.Email.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_Email_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_Email_Touch__Cirrious_MvvmCross_Plugins_Email_Touch_MvxComposeEmailTask__ctor
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 22,144,14,8,11,387000831,0,503
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_Email_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_Email_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,1,4,0,12,12,12,12,11,10,9,8,7,6,6,5,12,0,9,13,13,18,17,16,15,14,13,19,0,0,0
	.byte 1,20,0,0,0,1,21,255,252,0,0,0,1,1,3,219,0,0,1,12,1,39,42,47,34,255,254,0,0,0,1,255
	.byte 43,0,0,1,14,2,128,165,3,14,6,1,1,130,146,14,3,219,0,0,1,6,193,0,0,3,50,193,0,0,3,30
	.byte 3,219,0,0,1,6,196,0,0,18,16,1,130,146,136,199,16,2,2,1,3,6,196,0,0,19,14,2,59,3,6,193
	.byte 0,0,4,50,193,0,0,4,30,2,59,3,11,2,128,220,3,34,255,254,0,0,0,1,255,43,0,0,2,33,3,255
	.byte 254,0,0,0,1,255,43,0,0,1,3,195,0,3,209,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119
	.byte 95,115,112,101,99,105,102,105,99,0,3,195,0,3,207,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95
	.byte 115,112,101,99,105,102,105,99,0,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99
	.byte 101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,7,20,109,111,110,111,95,111,98,106
	.byte 101,99,116,95,110,101,119,95,102,97,115,116,0,3,195,0,3,204,7,17,109,111,110,111,95,104,101,108,112,101,114,95
	.byte 108,100,115,116,114,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112
	.byte 116,105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105
	.byte 111,110,0,3,255,254,0,0,0,1,255,43,0,0,2,7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116
	.byte 101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,5,128,224,16,4,0,4,146,178,146,175,146,174,146,172
	.byte 193,0,0,2,5,128,144,8,0,0,1,146,178,146,175,146,174,146,172,193,0,0,5,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
