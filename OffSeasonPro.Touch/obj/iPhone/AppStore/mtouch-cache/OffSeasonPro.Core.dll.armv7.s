	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs:
Leh_func_begin1:
	push	{r4, r5, r6, r7, lr}
Ltmp0:
	add	r7, sp, #12
Ltmp1:
	push	{r8, r10, r11}
Ltmp2:
	mov	r5, r0
	mov	r4, r1
	cmp	r5, #0
	ldr	r6, [r5, #8]!
	beq	LBB1_6
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC1_1+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC1_1+8))
LPC1_1:
	add	r0, pc, r0
	ldrd	r10, r11, [r0, #16]
	b	LBB1_3
LBB1_2:
	mov	r8, r10
	mov	r0, r5
	mov	r2, r6
	bl	_p_3_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs__System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB1_3:
	mov	r0, r6
	mov	r1, r4
	bl	_p_2_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB1_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r11
	beq	LBB1_2
Ltmp3:
	ldr	r0, LCPI1_0
LPC1_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB1_6:
	movw	r0, #630
	mov	r1, #1
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI1_0:
	.long	Ltmp3-(LPC1_0+8)
	.end_data_region
Leh_func_end1:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_remove_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_remove_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs:
Leh_func_begin2:
	push	{r4, r5, r6, r7, lr}
Ltmp4:
	add	r7, sp, #12
Ltmp5:
	push	{r8, r10, r11}
Ltmp6:
	mov	r5, r0
	mov	r4, r1
	cmp	r5, #0
	ldr	r6, [r5, #8]!
	beq	LBB2_6
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC2_1+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC2_1+8))
LPC2_1:
	add	r0, pc, r0
	ldrd	r10, r11, [r0, #16]
	b	LBB2_3
LBB2_2:
	mov	r8, r10
	mov	r0, r5
	mov	r2, r6
	bl	_p_3_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs__System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB2_3:
	mov	r0, r6
	mov	r1, r4
	bl	_p_4_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB2_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r11
	beq	LBB2_2
Ltmp7:
	ldr	r0, LCPI2_0
LPC2_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
LBB2_6:
	movw	r0, #630
	mov	r1, #1
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI2_0:
	.long	Ltmp7-(LPC2_0+8)
	.end_data_region
Leh_func_end2:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ctor:
Leh_func_begin3:
	bx	lr
Leh_func_end3:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_ReportError_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_ReportError_string:
Leh_func_begin4:
	push	{r4, r5, r6, r7, lr}
Ltmp8:
	add	r7, sp, #12
Ltmp9:
	push	{r10}
Ltmp10:
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC4_1+8))
	mov	r5, r0
	mov	r10, r1
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC4_1+8))
LPC4_1:
	add	r6, pc, r6
	ldr	r0, [r6, #24]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r10, [r4, #8]
	str	r5, [r4, #12]
	ldr	r0, [r5, #8]
	cmp	r0, #0
	beq	LBB4_3
	cmp	r4, #0
	beq	LBB4_4
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r6, #32]
	str	r0, [r1, #20]
	ldr	r0, [r6, #36]
	str	r0, [r1, #28]
	ldr	r0, [r6, #40]
	str	r0, [r1, #12]
	mov	r0, r5
	bl	_p_6_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject_InvokeOnMainThread_System_Action_llvm
LBB4_3:
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Ltmp11:
LBB4_4:
	ldr	r0, LCPI4_0
LPC4_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI4_0:
	.long	Ltmp11-(LPC4_0+8)
	.end_data_region
Leh_func_end4:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__ctor:
Leh_func_begin5:
	bx	lr
Leh_func_end5:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__m__0
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__m__0:
Leh_func_begin6:
	push	{r4, r5, r6, r7, lr}
Ltmp12:
	add	r7, sp, #12
Ltmp13:
Ltmp14:
	ldr	r1, [r0, #12]
	ldr	r4, [r1, #8]
	cmp	r4, #0
	popeq	{r4, r5, r6, r7, pc}
	ldr	r5, [r0, #12]
	ldr	r6, [r0, #8]
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC6_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC6_0+8))
LPC6_0:
	add	r0, pc, r0
	ldr	r0, [r0, #44]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	mov	r0, r4
	mov	r1, r5
	str	r6, [r2, #8]
	ldr	r3, [r4, #12]
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end6:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp15:
	add	r7, sp, #12
Ltmp16:
Ltmp17:
	mov	r4, r0
	mov	r5, #1
	mov	r6, #0
	ldr	r0, [r4]
	ldr	r0, [r4, #36]
	str	r5, [r0, #8]
	ldr	r0, [r4, #36]
	str	r5, [r0, #12]
	ldr	r0, [r4, #36]
	str	r5, [r0, #16]
	ldr	r0, [r4, #36]
	str	r6, [r0, #20]
	ldr	r0, [r4, #28]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	cmp	r0, #1
	ldrge	r0, [r4, #36]
	strbge	r5, [r0, #25]
	ldrge	r0, [r4, #36]
	strge	r6, [r0, #8]
	pop	{r4, r5, r6, r7, pc}
Leh_func_end7:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout:
Leh_func_begin8:
	mov	r1, r0
	mov	r0, #1
	ldr	r2, [r1, #8]
	cmp	r2, #1
	ldrle	r2, [r1, #12]
	cmple	r2, #2
	bge	LBB8_2
	ldr	r2, [r1, #16]
	cmp	r2, #1
	bxgt	lr
	ldr	r1, [r1, #20]
	mov	r0, #0
	cmp	r1, #0
	movgt	r0, #1
LBB8_2:
	bx	lr
Leh_func_end8:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings:
Leh_func_begin9:
	push	{r4, r5, r7, lr}
Ltmp18:
	add	r7, sp, #8
Ltmp19:
Ltmp20:
	mov	r5, r0
	ldr	r0, [r5]
	ldr	r0, [r5, #12]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r4, #0
	cmp	r0, #0
	beq	LBB9_5
	ldr	r0, [r5, #16]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	cmp	r0, #0
	beq	LBB9_5
	ldr	r0, [r5, #20]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	cmp	r0, #0
	beq	LBB9_5
	ldr	r0, [r5, #8]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	cmp	r0, #0
	beq	LBB9_5
	ldr	r0, [r5, #24]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r4, r0
	cmp	r4, #0
	movne	r4, #1
LBB9_5:
	mov	r0, r4
	pop	{r4, r5, r7, pc}
Leh_func_end9:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp21:
	add	r7, sp, #8
Ltmp22:
Ltmp23:
	mov	r4, r2
	mov	r5, r0
	bl	_p_9_plt_Cirrious_MvvmCross_Plugins_Messenger_MvxMessage__ctor_object_llvm
	str	r4, [r5, #12]
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_get_Message
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_get_Message:
Leh_func_begin11:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end11:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_set_Message_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_set_Message_string:
Leh_func_begin12:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end12:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs__ctor_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs__ctor_string:
Leh_func_begin13:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end13:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_get_Message
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_get_Message:
Leh_func_begin14:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end14:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_set_Message_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_set_Message_string:
Leh_func_begin15:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end15:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StationCount
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StationCount:
Leh_func_begin16:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end16:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StationCount_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StationCount_string:
Leh_func_begin17:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end17:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_PartnerCount
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_PartnerCount:
Leh_func_begin18:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end18:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_PartnerCount_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_PartnerCount_string:
Leh_func_begin19:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end19:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetCount
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetCount:
Leh_func_begin20:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end20:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetCount_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetCount_string:
Leh_func_begin21:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end21:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetLengthSeconds
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetLengthSeconds:
Leh_func_begin22:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end22:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetLengthSeconds_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetLengthSeconds_string:
Leh_func_begin23:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end23:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_TransitionLengthSeconds
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_TransitionLengthSeconds:
Leh_func_begin24:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end24:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_TransitionLengthSeconds_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_TransitionLengthSeconds_string:
Leh_func_begin25:
	str	r1, [r0, #24]
	bx	lr
Leh_func_end25:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SkipAfterLift
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SkipAfterLift:
Leh_func_begin26:
	ldrb	r0, [r0, #40]
	bx	lr
Leh_func_end26:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SkipAfterLift_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SkipAfterLift_bool:
Leh_func_begin27:
	strb	r1, [r0, #40]
	bx	lr
Leh_func_end27:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StartupLength
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StartupLength:
Leh_func_begin28:
	ldr	r0, [r0, #28]
	bx	lr
Leh_func_end28:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StartupLength_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StartupLength_string:
Leh_func_begin29:
	str	r1, [r0, #28]
	bx	lr
Leh_func_end29:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_WorkoutBackgroundImage
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_WorkoutBackgroundImage:
Leh_func_begin30:
	ldr	r0, [r0, #32]
	bx	lr
Leh_func_end30:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_WorkoutBackgroundImage_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_WorkoutBackgroundImage_string:
Leh_func_begin31:
	str	r1, [r0, #32]
	bx	lr
Leh_func_end31:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_UsePhotoAlbumForBgImage
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_UsePhotoAlbumForBgImage:
Leh_func_begin32:
	ldrb	r0, [r0, #41]
	bx	lr
Leh_func_end32:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_UsePhotoAlbumForBgImage_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_UsePhotoAlbumForBgImage_bool:
Leh_func_begin33:
	strb	r1, [r0, #41]
	bx	lr
Leh_func_end33:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_IsPaid
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_IsPaid:
Leh_func_begin34:
	ldrb	r0, [r0, #42]
	bx	lr
Leh_func_end34:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_IsPaid_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_IsPaid_bool:
Leh_func_begin35:
	strb	r1, [r0, #42]
	bx	lr
Leh_func_end35:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_CurrentWorkout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_CurrentWorkout:
Leh_func_begin36:
	ldr	r0, [r0, #36]
	bx	lr
Leh_func_end36:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_CurrentWorkout_OffSeasonPro_Core_Models_Workout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_CurrentWorkout_OffSeasonPro_Core_Models_Workout:
Leh_func_begin37:
	str	r1, [r0, #36]
	bx	lr
Leh_func_end37:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings__ctor:
Leh_func_begin38:
	bx	lr
Leh_func_end38:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__ctor:
Leh_func_begin39:
	push	{r4, r7, lr}
Ltmp24:
	add	r7, sp, #4
Ltmp25:
	push	{r8}
Ltmp26:
	mov	r4, r0
	bl	_p_10_plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings_llvm
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC39_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC39_0+8))
LPC39_0:
	add	r0, pc, r0
	ldr	r0, [r0, #48]
	mov	r8, r0
	bl	_p_11_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_llvm
	mov	r1, r0
	mov	r0, #0
	strd	r0, r1, [r4, #8]
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end39:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Playlist
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Playlist:
Leh_func_begin40:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end40:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Settings:
Leh_func_begin41:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end41:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_SaveSettings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_SaveSettings:
Leh_func_begin42:
	push	{r4, r5, r6, r7, lr}
Ltmp27:
	add	r7, sp, #12
Ltmp28:
	push	{r8, r10, r11}
Ltmp29:
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC42_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC42_1+8))
LPC42_1:
	add	r6, pc, r6
	ldr	r0, [r6, #52]
	mov	r8, r0
	bl	_p_12_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm
	mov	r4, r0
	cmp	r5, #0
	beq	LBB42_2
	ldrd	r10, r11, [r6, #56]
	mov	r0, r11
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	str	r5, [r2, #16]
	ldr	r0, [r6, #64]
	str	r0, [r2, #20]
	ldr	r0, [r6, #68]
	str	r0, [r2, #28]
	ldr	r0, [r6, #72]
	str	r0, [r2, #12]
	ldr	r0, [r6, #76]
	ldr	r1, [r4]
	mov	r8, r0
	mov	r0, r4
	sub	r1, r1, #48
	ldr	r3, [r1]
	mov	r1, r10
	blx	r3
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp30:
LBB42_2:
	ldr	r0, LCPI42_0
LPC42_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI42_0:
	.long	Ltmp30-(LPC42_0+8)
	.end_data_region
Leh_func_end42:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_CreateTts_System_Action_System_Action
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_CreateTts_System_Action_System_Action:
Leh_func_begin43:
	push	{r4, r5, r6, r7, lr}
Ltmp31:
	add	r7, sp, #12
Ltmp32:
	push	{r10, r11}
Ltmp33:
	sub	sp, sp, #40
	mov	r4, r0
	mov	r10, r2
	mov	r5, r1
	str	r4, [sp, #24]
	movw	r11, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_1+8))
	movt	r11, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_1+8))
LPC43_1:
	add	r11, pc, r11
	ldr	r0, [r11, #80]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	ldr	r0, [r11, #84]
	str	r5, [r6, #12]
	mov	r5, r4
	str	r6, [sp, #28]
	str	r10, [r6, #16]
	str	r5, [r6, #20]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_13_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm
	str	r4, [r6, #8]
	ldr	r0, [r5, #16]
	ldr	r1, [r0]
	ldr	r0, [r0, #16]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	add	r0, r0, #1
	cmp	r0, #2
	blt	LBB43_5
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_2+8))
	mov	r6, #1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_2+8))
LPC43_2:
	add	r0, pc, r0
	ldr	r1, [r0, #92]
	ldr	r11, [r0, #88]
	str	r1, [sp, #20]
	ldr	r1, [r0, #96]
	str	r1, [sp]
	ldr	r1, [r0, #100]
	str	r1, [sp, #16]
	ldr	r1, [r0, #104]
	str	r1, [sp, #12]
	ldr	r1, [r0, #108]
	ldr	r0, [r0, #112]
	str	r1, [sp, #8]
	str	r0, [sp, #4]
LBB43_2:
	ldr	r0, [r5, #16]
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	ldr	r10, [sp]
	add	r0, r0, #1
	mov	r4, r6
	mov	r6, #1
	cmp	r0, #1
	ble	LBB43_4
LBB43_3:
	ldr	r0, [sp, #28]
	mov	r1, #4
	ldr	r0, [r0, #8]
	str	r0, [r7, #-24]
	mov	r0, r11
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r2, [sp, #20]
	mov	r1, #0
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	bl	_p_15_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #1
	str	r4, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [sp, #16]
	mov	r1, #2
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	bl	_p_15_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #3
	str	r6, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r5
	bl	_p_16_plt_string_Concat_object___llvm
	str	r0, [r7, #-28]
	mov	r0, r11
	mov	r1, #5
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r2, [sp, #12]
	mov	r1, #0
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	bl	_p_15_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #1
	str	r4, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [sp, #8]
	mov	r1, #2
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r10
	bl	_p_15_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #3
	str	r6, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [sp, #4]
	mov	r1, #4
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r5
	bl	_p_16_plt_string_Concat_object___llvm
	mov	r2, r0
	ldr	r0, [r7, #-24]
	ldr	r1, [r0]
	ldr	r5, [sp, #24]
	ldr	r1, [r7, #-28]
	bl	_p_17_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm
	ldr	r0, [r5, #16]
	add	r6, r6, #1
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	add	r0, r0, #1
	cmp	r6, r0
	blt	LBB43_3
LBB43_4:
	ldr	r0, [r5, #16]
	add	r4, r4, #1
	ldr	r1, [r0]
	ldr	r0, [r0, #16]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	add	r0, r0, #1
	mov	r6, r4
	cmp	r4, r0
	blt	LBB43_2
LBB43_5:
	ldr	r5, [sp, #28]
	ldr	r0, [r5, #8]
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_3+8))
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC43_3+8))
LPC43_3:
	add	r4, pc, r4
	ldrd	r2, r3, [r4, #116]
	ldr	r1, [r0]
	mov	r1, r2
	mov	r2, r3
	bl	_p_17_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm
	ldr	r0, [r5, #8]
	ldrd	r2, r3, [r4, #124]
	ldr	r1, [r0]
	mov	r1, r2
	mov	r2, r3
	bl	_p_17_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm
	ldr	r0, [r5, #8]
	ldrd	r2, r3, [r4, #132]
	ldr	r1, [r0]
	mov	r1, r2
	mov	r2, r3
	bl	_p_17_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm
	cmp	r5, #0
	beq	LBB43_7
	ldr	r0, [r4, #140]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	str	r5, [r0, #16]
	ldr	r1, [r4, #144]
	str	r1, [r0, #20]
	ldr	r1, [r4, #148]
	str	r1, [r0, #28]
	ldr	r1, [r4, #152]
	str	r1, [r0, #12]
	bl	_p_18_plt_System_Threading_ThreadPool_QueueUserWorkItem_System_Threading_WaitCallback_llvm
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp34:
LBB43_7:
	ldr	r0, LCPI43_0
LPC43_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI43_0:
	.long	Ltmp34-(LPC43_0+8)
	.end_data_region
Leh_func_end43:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_ReloadSettings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_ReloadSettings:
Leh_func_begin44:
	push	{r7, lr}
Ltmp35:
	mov	r7, sp
Ltmp36:
Ltmp37:
	bl	_p_10_plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings_llvm
	pop	{r7, pc}
Leh_func_end44:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings:
Leh_func_begin45:
	push	{r4, r5, r6, r7, lr}
Ltmp38:
	add	r7, sp, #12
Ltmp39:
	push	{r8, r10}
Ltmp40:
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC45_1+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC45_1+8))
LPC45_1:
	add	r6, pc, r6
	ldr	r0, [r6, #52]
	mov	r8, r0
	bl	_p_12_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm
	mov	r5, r0
	cmp	r4, #0
	beq	LBB45_4
	ldr	r0, [r6, #156]
	ldr	r10, [r6, #56]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	str	r4, [r2, #16]
	ldr	r0, [r6, #160]
	str	r0, [r2, #20]
	ldr	r0, [r6, #164]
	str	r0, [r2, #28]
	ldr	r0, [r6, #168]
	str	r0, [r2, #12]
	ldr	r0, [r6, #172]
	ldr	r1, [r5]
	mov	r8, r0
	mov	r0, r5
	sub	r1, r1, #16
	ldr	r3, [r1]
	mov	r1, r10
	blx	r3
	tst	r0, #255
	bne	LBB45_3
	ldr	r0, [r6, #176]
	mov	r8, r0
	bl	_p_19_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm
	mov	r5, r0
	ldr	r0, [r6, #60]
	ldr	r10, [r6, #180]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	str	r4, [r2, #16]
	ldr	r0, [r6, #184]
	str	r0, [r2, #20]
	ldr	r0, [r6, #188]
	str	r0, [r2, #28]
	ldr	r0, [r6, #72]
	str	r0, [r2, #12]
	ldr	r1, [r5]
	ldr	r0, [r6, #192]
	sub	r1, r1, #36
	mov	r8, r0
	mov	r0, r5
	ldr	r3, [r1]
	mov	r1, r10
	blx	r3
LBB45_3:
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Ltmp41:
LBB45_4:
	ldr	r0, LCPI45_0
LPC45_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI45_0:
	.long	Ltmp41-(LPC45_0+8)
	.end_data_region
Leh_func_end45:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__SaveSettingsm__1_System_IO_Stream
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__SaveSettingsm__1_System_IO_Stream:
Leh_func_begin46:
	push	{r4, r5, r6, r7, lr}
Ltmp42:
	add	r7, sp, #12
Ltmp43:
	push	{r10}
Ltmp44:
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC46_0+8))
	mov	r10, r1
	mov	r5, r0
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC46_0+8))
LPC46_0:
	add	r4, pc, r4
	ldr	r6, [r4, #196]
	bl	_p_20_plt__class_init_System_Xml_Serialization_XmlSerializer_llvm
	ldr	r0, [r4, #200]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r4, r0
	bl	_p_21_plt_System_Xml_Serialization_XmlSerializer__ctor_System_Type_llvm
	ldr	r2, [r5, #16]
	mov	r0, r4
	mov	r1, r10
	bl	_p_22_plt_System_Xml_Serialization_XmlSerializer_Serialize_System_IO_Stream_object_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end46:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__LoadSettingsm__3_System_IO_Stream
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__LoadSettingsm__3_System_IO_Stream:
Leh_func_begin47:
	push	{r7, lr}
Ltmp45:
	mov	r7, sp
Ltmp46:
Ltmp47:
	bl	_p_23_plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream_llvm
	pop	{r7, pc}
Leh_func_end47:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__ctor:
Leh_func_begin48:
	bx	lr
Leh_func_end48:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__m__2_object
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__m__2_object:
Leh_func_begin49:
	push	{r7, lr}
Ltmp48:
	mov	r7, sp
Ltmp49:
	push	{r8}
Ltmp50:
	mov	r3, r0
	ldr	r0, [r3, #20]
	ldr	r0, [r0, #12]
	ldr	r1, [r3, #8]
	ldr	r2, [r3, #12]
	ldr	r3, [r3, #16]
	ldr	r9, [r0]
	movw	r12, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC49_0+8))
	movt	r12, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC49_0+8))
LPC49_0:
	add	r12, pc, r12
	ldr	r12, [r12, #204]
	sub	r9, r9, #36
	ldr	r9, [r9]
	mov	r8, r12
	blx	r9
	pop	{r8}
	pop	{r7, pc}
Leh_func_end49:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_App__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_App__ctor:
Leh_func_begin50:
	push	{r4, r5, r6, r7, lr}
Ltmp51:
	add	r7, sp, #12
Ltmp52:
	push	{r8}
Ltmp53:
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC50_0+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC50_0+8))
LPC50_0:
	add	r6, pc, r6
	ldr	r0, [r6, #208]
	ldr	r0, [r0]
	ldr	r1, [r0]
	bl	_p_24_plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded_llvm
	ldr	r0, [r6, #212]
	ldr	r0, [r0]
	ldr	r1, [r0]
	bl	_p_25_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded_llvm
	ldr	r0, [r6, #216]
	ldr	r0, [r0]
	ldr	r1, [r0]
	bl	_p_26_plt_OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded_llvm
	ldr	r0, [r6, #220]
	ldr	r0, [r0]
	ldr	r1, [r0]
	bl	_p_27_plt_Cirrious_MvvmCross_Plugins_Messenger_PluginLoader_EnsureLoaded_llvm
	ldr	r0, [r6, #224]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_28_plt_OffSeasonPro_Core_Models_SimpleDataStore__ctor_llvm
	ldr	r0, [r6, #228]
	mov	r8, r0
	mov	r0, r5
	bl	_p_29_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IDataStore_OffSeasonPro_Core_Interfaces_IDataStore_llvm
	mov	r0, r4
	bl	_p_30_plt_OffSeasonPro_Core_App_InitalizeErrorSystem_llvm
	ldr	r0, [r6, #232]
	mov	r8, r0
	mov	r0, r4
	bl	_p_31_plt_Cirrious_MvvmCross_ViewModels_MvxApplication_RegisterAppStart_OffSeasonPro_Core_ViewModels_HomeViewModel_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end50:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_App_InitalizeErrorSystem
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_App_InitalizeErrorSystem:
Leh_func_begin51:
	push	{r4, r5, r7, lr}
Ltmp54:
	add	r7, sp, #8
Ltmp55:
	push	{r8}
Ltmp56:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC51_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC51_0+8))
LPC51_0:
	add	r5, pc, r5
	ldr	r0, [r5, #236]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r5, #240]
	mov	r8, r0
	mov	r0, r4
	bl	_p_32_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorReporter_OffSeasonPro_Core_Interfaces_IErrorReporter_llvm
	ldr	r0, [r5, #244]
	mov	r8, r0
	mov	r0, r4
	bl	_p_33_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorSource_OffSeasonPro_Core_Interfaces_IErrorSource_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end51:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Workout__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Models_Workout__ctor:
Leh_func_begin52:
	bx	lr
Leh_func_end52:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__ctor:
Leh_func_begin53:
	push	{r7, lr}
Ltmp57:
	mov	r7, sp
Ltmp58:
Ltmp59:
	bl	_p_34_plt_OffSeasonPro_Core_ViewModels_BaseViewModel__ctor_llvm
	pop	{r7, pc}
Leh_func_end53:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings:
Leh_func_begin54:
	push	{r7, lr}
Ltmp60:
	mov	r7, sp
Ltmp61:
	push	{r8}
Ltmp62:
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC54_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC54_0+8))
LPC54_0:
	add	r1, pc, r1
	ldr	r1, [r1, #248]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end54:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_set_Settings_OffSeasonPro_Core_Models_Settings:
Leh_func_begin55:
	push	{r4, r5, r6, r7, lr}
Ltmp63:
	add	r7, sp, #12
Ltmp64:
	push	{r8}
Ltmp65:
	mov	r4, r0
	str	r1, [r4, #20]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC55_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC55_1+8))
LPC55_1:
	add	r6, pc, r6
	ldr	r0, [r6, #252]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB55_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB55_3
LBB55_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #260]
	mov	r8, r0
	mov	r0, r5
	bl	_p_39_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #264]
	mov	r8, r0
	mov	r0, r4
	bl	_p_40_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_1_System_Func_1_OffSeasonPro_Core_Models_Settings_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp66:
LBB55_3:
	ldr	r0, LCPI55_0
LPC55_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI55_0:
	.long	Ltmp66-(LPC55_0+8)
	.end_data_region
Leh_func_end55:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail:
Leh_func_begin56:
	push	{r4, r5, r6, r7, lr}
Ltmp67:
	add	r7, sp, #12
Ltmp68:
Ltmp69:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB56_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC56_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC56_1+8))
LPC56_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #272]
	str	r0, [r4, #20]
	ldr	r0, [r6, #276]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp70:
LBB56_2:
	ldr	r0, LCPI56_0
LPC56_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI56_0:
	.long	Ltmp70-(LPC56_0+8)
	.end_data_region
Leh_func_end56:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SaveSettings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SaveSettings:
Leh_func_begin57:
	push	{r4, r5, r6, r7, lr}
Ltmp71:
	add	r7, sp, #12
Ltmp72:
Ltmp73:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB57_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC57_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC57_1+8))
LPC57_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r6, #284]
	str	r5, [r4, #16]
	str	r0, [r4, #20]
	ldr	r0, [r6, #288]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp74:
LBB57_2:
	ldr	r0, LCPI57_0
LPC57_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI57_0:
	.long	Ltmp74-(LPC57_0+8)
	.end_data_region
Leh_func_end57:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Save
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Save:
Leh_func_begin58:
	push	{r4, r5, r7, lr}
Ltmp75:
	add	r7, sp, #8
Ltmp76:
	push	{r8}
Ltmp77:
	mov	r4, r0
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC58_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC58_0+8))
LPC58_0:
	add	r5, pc, r5
	ldr	r1, [r5, #292]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, r4
	bl	_p_42_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #296]
	sub	r2, r2, #36
	mov	r8, r1
	mov	r1, #0
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end58:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody:
Leh_func_begin59:
	push	{r4, r5, r7, lr}
Ltmp78:
	add	r7, sp, #8
Ltmp79:
Ltmp80:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC59_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC59_0+8))
LPC59_0:
	add	r5, pc, r5
	ldr	r0, [r5, #300]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_43_plt_System_Text_StringBuilder__ctor_llvm
	ldr	r1, [r5, #304]
	mov	r0, r4
	bl	_p_44_plt_System_Text_StringBuilder_AppendLine_string_llvm
	ldr	r1, [r5, #308]
	mov	r0, r4
	bl	_p_44_plt_System_Text_StringBuilder_AppendLine_string_llvm
	ldr	r1, [r5, #312]
	mov	r0, r4
	bl	_p_44_plt_System_Text_StringBuilder_AppendLine_string_llvm
	ldr	r0, [r4]
	ldr	r1, [r0, #32]
	mov	r0, r4
	blx	r1
	pop	{r4, r5, r7, pc}
Leh_func_end59:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__get_SendBackgroundRequestEmailm__4
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__get_SendBackgroundRequestEmailm__4:
Leh_func_begin60:
	push	{r4, r5, r6, r7, lr}
Ltmp81:
	add	r7, sp, #12
Ltmp82:
Ltmp83:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC60_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC60_0+8))
LPC60_0:
	add	r0, pc, r0
	ldr	r5, [r0, #316]
	ldr	r6, [r0, #320]
	mov	r0, r4
	bl	_p_45_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody_llvm
	mov	r3, r0
	mov	r0, r4
	mov	r1, r5
	mov	r2, r6
	bl	_p_46_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end60:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand:
Leh_func_begin61:
	push	{r4, r5, r6, r7, lr}
Ltmp84:
	add	r7, sp, #12
Ltmp85:
Ltmp86:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB61_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC61_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC61_1+8))
LPC61_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r6, #324]
	str	r5, [r4, #16]
	str	r0, [r4, #20]
	ldr	r0, [r6, #328]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp87:
LBB61_2:
	ldr	r0, LCPI61_0
LPC61_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI61_0:
	.long	Ltmp87-(LPC61_0+8)
	.end_data_region
Leh_func_end61:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore:
Leh_func_begin62:
	push	{r7, lr}
Ltmp88:
	mov	r7, sp
Ltmp89:
	push	{r8}
Ltmp90:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC62_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC62_0+8))
LPC62_0:
	add	r0, pc, r0
	ldr	r0, [r0, #332]
	mov	r8, r0
	bl	_p_47_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IDataStore_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end62:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor:
Leh_func_begin63:
	push	{r7, lr}
Ltmp91:
	mov	r7, sp
Ltmp92:
Ltmp93:
	bl	_p_48_plt_Cirrious_MvvmCross_ViewModels_MvxViewModel__ctor_llvm
	pop	{r7, pc}
Leh_func_end63:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string:
Leh_func_begin64:
	push	{r4, r5, r7, lr}
Ltmp94:
	add	r7, sp, #8
Ltmp95:
	push	{r8}
Ltmp96:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC64_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC64_0+8))
LPC64_0:
	add	r5, pc, r5
	ldr	r0, [r5, #336]
	mov	r8, r0
	bl	_p_49_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorReporter_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #340]
	sub	r2, r2, #20
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end64:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string:
Leh_func_begin65:
	push	{r4, r5, r6, r7, lr}
Ltmp97:
	add	r7, sp, #12
Ltmp98:
	push	{r8, r10, r11}
Ltmp99:
	sub	sp, sp, #8
	movw	r4, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC65_0+8))
	mov	r10, r3
	mov	r6, r2
	mov	r5, r1
	movt	r4, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC65_0+8))
LPC65_0:
	add	r4, pc, r4
	ldr	r0, [r4, #344]
	mov	r8, r0
	bl	_p_50_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_llvm
	ldr	r2, [r0]
	ldr	r1, [r4, #348]
	mov	r11, #0
	mov	r3, r6
	sub	r2, r2, #72
	mov	r8, r1
	mov	r1, r5
	ldr	r4, [r2]
	mov	r2, #0
	strd	r10, r11, [sp]
	blx	r4
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end65:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_RequestClose
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_RequestClose:
Leh_func_begin66:
	push	{r4, r5, r7, lr}
Ltmp100:
	add	r7, sp, #8
Ltmp101:
	push	{r8}
Ltmp102:
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC66_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC66_0+8))
LPC66_0:
	add	r5, pc, r5
	ldr	r0, [r5, #352]
	mov	r8, r0
	bl	_p_51_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IViewModelCloser_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #356]
	sub	r2, r2, #60
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end66:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__ctor:
Leh_func_begin67:
	push	{r7, lr}
Ltmp103:
	mov	r7, sp
Ltmp104:
Ltmp105:
	bl	_p_34_plt_OffSeasonPro_Core_ViewModels_BaseViewModel__ctor_llvm
	pop	{r7, pc}
Leh_func_end67:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings:
Leh_func_begin68:
	push	{r7, lr}
Ltmp106:
	mov	r7, sp
Ltmp107:
	push	{r8}
Ltmp108:
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC68_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC68_0+8))
LPC68_0:
	add	r1, pc, r1
	ldr	r1, [r1, #248]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end68:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ShowSettingsCommand
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ShowSettingsCommand:
Leh_func_begin69:
	push	{r4, r5, r6, r7, lr}
Ltmp109:
	add	r7, sp, #12
Ltmp110:
Ltmp111:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB69_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC69_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC69_1+8))
LPC69_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #360]
	str	r0, [r4, #20]
	ldr	r0, [r6, #364]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp112:
LBB69_2:
	ldr	r0, LCPI69_0
LPC69_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI69_0:
	.long	Ltmp112-(LPC69_0+8)
	.end_data_region
Leh_func_end69:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand:
Leh_func_begin70:
	push	{r4, r5, r6, r7, lr}
Ltmp113:
	add	r7, sp, #12
Ltmp114:
Ltmp115:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB70_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC70_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC70_1+8))
LPC70_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #368]
	str	r0, [r4, #20]
	ldr	r0, [r6, #372]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp116:
LBB70_2:
	ldr	r0, LCPI70_0
LPC70_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI70_0:
	.long	Ltmp116-(LPC70_0+8)
	.end_data_region
Leh_func_end70:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_ToggleAction
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_ToggleAction:
Leh_func_begin71:
	push	{r4, r7, lr}
Ltmp117:
	add	r7, sp, #4
Ltmp118:
	push	{r8}
Ltmp119:
	mov	r4, r0
	bl	_p_52_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings_llvm
	bl	_p_53_plt_OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings_llvm
	tst	r0, #255
	beq	LBB71_2
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC71_0+8))
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC71_0+8))
LPC71_0:
	add	r0, pc, r0
	ldr	r0, [r0, #376]
	mov	r8, r0
	mov	r0, r4
	bl	_p_54_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm
	pop	{r8}
	pop	{r4, r7, pc}
LBB71_2:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC71_1+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC71_1+8))
LPC71_1:
	add	r0, pc, r0
	ldr	r1, [r0, #380]
	mov	r0, r4
	bl	_p_55_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end71:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__get_ShowSettingsCommandm__5
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__get_ShowSettingsCommandm__5:
Leh_func_begin72:
	push	{r7, lr}
Ltmp120:
	mov	r7, sp
Ltmp121:
	push	{r8}
Ltmp122:
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC72_0+8))
	mov	r2, #0
	mov	r3, #0
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC72_0+8))
LPC72_0:
	add	r1, pc, r1
	ldr	r1, [r1, #384]
	mov	r8, r1
	mov	r1, #0
	bl	_p_56_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end72:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Trace__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Trace__ctor:
Leh_func_begin73:
	bx	lr
Leh_func_end73:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Info_string_object__
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Info_string_object__:
Leh_func_begin74:
	push	{r7, lr}
Ltmp123:
	mov	r7, sp
Ltmp124:
Ltmp125:
	mov	r2, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC74_0+8))
	mov	r3, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC74_0+8))
LPC74_0:
	add	r0, pc, r0
	ldr	r1, [r0, #388]
	mov	r0, #0
	bl	_p_57_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___llvm
	pop	{r7, pc}
Leh_func_end74:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Warn_string_object__
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Warn_string_object__:
Leh_func_begin75:
	push	{r7, lr}
Ltmp126:
	mov	r7, sp
Ltmp127:
Ltmp128:
	mov	r2, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC75_0+8))
	mov	r3, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC75_0+8))
LPC75_0:
	add	r0, pc, r0
	ldr	r1, [r0, #388]
	mov	r0, #1
	bl	_p_57_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___llvm
	pop	{r7, pc}
Leh_func_end75:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Error_string_object__
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Error_string_object__:
Leh_func_begin76:
	push	{r7, lr}
Ltmp129:
	mov	r7, sp
Ltmp130:
Ltmp131:
	mov	r2, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC76_0+8))
	mov	r3, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC76_0+8))
LPC76_0:
	add	r0, pc, r0
	ldr	r1, [r0, #388]
	mov	r0, #2
	bl	_p_57_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___llvm
	pop	{r7, pc}
Leh_func_end76:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings:
Leh_func_begin77:
	push	{r7, lr}
Ltmp132:
	mov	r7, sp
Ltmp133:
	push	{r8}
Ltmp134:
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC77_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC77_0+8))
LPC77_0:
	add	r1, pc, r1
	ldr	r1, [r1, #248]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end77:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_Settings_OffSeasonPro_Core_Models_Settings:
Leh_func_begin78:
	push	{r4, r5, r6, r7, lr}
Ltmp135:
	add	r7, sp, #12
Ltmp136:
	push	{r8}
Ltmp137:
	mov	r4, r0
	str	r1, [r4, #20]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC78_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC78_1+8))
LPC78_1:
	add	r6, pc, r6
	ldr	r0, [r6, #392]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB78_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB78_3
LBB78_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #260]
	mov	r8, r0
	mov	r0, r5
	bl	_p_39_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #264]
	mov	r8, r0
	mov	r0, r4
	bl	_p_40_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_1_System_Func_1_OffSeasonPro_Core_Models_Settings_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp138:
LBB78_3:
	ldr	r0, LCPI78_0
LPC78_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI78_0:
	.long	Ltmp138-(LPC78_0+8)
	.end_data_region
Leh_func_end78:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave:
Leh_func_begin79:
	push	{r4, r5, r6, r7, lr}
Ltmp139:
	add	r7, sp, #12
Ltmp140:
Ltmp141:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB79_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC79_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC79_1+8))
LPC79_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r6, #396]
	str	r5, [r4, #16]
	str	r0, [r4, #20]
	ldr	r0, [r6, #400]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp142:
LBB79_2:
	ldr	r0, LCPI79_0
LPC79_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI79_0:
	.long	Ltmp142-(LPC79_0+8)
	.end_data_region
Leh_func_end79:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings:
Leh_func_begin80:
	push	{r4, r5, r6, r7, lr}
Ltmp143:
	add	r7, sp, #12
Ltmp144:
Ltmp145:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB80_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC80_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC80_1+8))
LPC80_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r6, #404]
	str	r5, [r4, #16]
	str	r0, [r4, #20]
	ldr	r0, [r6, #408]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp146:
LBB80_2:
	ldr	r0, LCPI80_0
LPC80_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI80_0:
	.long	Ltmp146-(LPC80_0+8)
	.end_data_region
Leh_func_end80:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_IsBusy
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_IsBusy:
Leh_func_begin81:
	ldrb	r0, [r0, #24]
	bx	lr
Leh_func_end81:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool:
Leh_func_begin82:
	push	{r4, r5, r6, r7, lr}
Ltmp147:
	add	r7, sp, #12
Ltmp148:
	push	{r8}
Ltmp149:
	mov	r4, r0
	strb	r1, [r4, #24]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC82_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC82_1+8))
LPC82_1:
	add	r6, pc, r6
	ldr	r0, [r6, #412]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB82_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB82_3
LBB82_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #416]
	mov	r8, r0
	mov	r0, r5
	bl	_p_58_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #420]
	mov	r8, r0
	mov	r0, r4
	bl	_p_59_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp150:
LBB82_3:
	ldr	r0, LCPI82_0
LPC82_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI82_0:
	.long	Ltmp150-(LPC82_0+8)
	.end_data_region
Leh_func_end82:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser:
Leh_func_begin83:
	push	{r4, r5, r6, r7, lr}
Ltmp151:
	add	r7, sp, #12
Ltmp152:
Ltmp153:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB83_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC83_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC83_1+8))
LPC83_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #424]
	str	r0, [r4, #20]
	ldr	r0, [r6, #428]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp154:
LBB83_2:
	ldr	r0, LCPI83_0
LPC83_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI83_0:
	.long	Ltmp154-(LPC83_0+8)
	.end_data_region
Leh_func_end83:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel__ctor
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel__ctor:
Leh_func_begin84:
	push	{r7, lr}
Ltmp155:
	mov	r7, sp
Ltmp156:
Ltmp157:
	bl	_p_34_plt_OffSeasonPro_Core_ViewModels_BaseViewModel__ctor_llvm
	pop	{r7, pc}
Leh_func_end84:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Cancel
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Cancel:
Leh_func_begin85:
	push	{r4, r5, r7, lr}
Ltmp158:
	add	r7, sp, #8
Ltmp159:
	push	{r8}
Ltmp160:
	mov	r4, r0
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC85_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC85_0+8))
LPC85_0:
	add	r5, pc, r5
	ldr	r1, [r5, #432]
	sub	r2, r2, #4
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, r4
	bl	_p_42_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #296]
	sub	r2, r2, #36
	mov	r8, r1
	mov	r1, #0
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end85:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Save
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Save:
Leh_func_begin86:
	push	{r4, r5, r6, r7, lr}
Ltmp161:
	add	r7, sp, #12
Ltmp162:
	push	{r8, r10, r11}
Ltmp163:
	mov	r4, r0
	bl	_p_60_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	bl	_p_53_plt_OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings_llvm
	tst	r0, #255
	beq	LBB86_3
	mov	r0, r4
	mov	r1, #1
	bl	_p_61_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool_llvm
	mov	r0, r4
	bl	_p_60_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	bl	_p_62_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm
	mov	r0, r4
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r11, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC86_1+8))
	movt	r11, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC86_1+8))
LPC86_1:
	add	r11, pc, r11
	ldr	r1, [r11, #292]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	mov	r0, r4
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	mov	r10, r0
	cmp	r4, #0
	beq	LBB86_4
	ldr	r5, [r11, #28]
	mov	r0, r5
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	ldr	r0, [r11, #436]
	str	r4, [r6, #16]
	str	r0, [r6, #20]
	ldr	r0, [r11, #440]
	str	r0, [r6, #28]
	ldr	r0, [r11, #40]
	str	r0, [r6, #12]
	mov	r0, r5
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r2, r0
	ldr	r0, [r11, #444]
	str	r4, [r2, #16]
	str	r0, [r2, #20]
	ldr	r0, [r11, #448]
	str	r0, [r2, #28]
	ldr	r0, [r11, #40]
	str	r0, [r2, #12]
	ldr	r0, [r11, #452]
	ldr	r1, [r10]
	mov	r8, r0
	mov	r0, r10
	sub	r1, r1, #76
	ldr	r3, [r1]
	mov	r1, r6
	blx	r3
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB86_3:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC86_2+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC86_2+8))
LPC86_2:
	add	r0, pc, r0
	ldr	r1, [r0, #380]
	mov	r0, r4
	bl	_p_55_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string_llvm
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp164:
LBB86_4:
	ldr	r0, LCPI86_0
LPC86_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI86_0:
	.long	Ltmp164-(LPC86_0+8)
	.end_data_region
Leh_func_end86:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Finished
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Finished:
Leh_func_begin87:
	push	{r7, lr}
Ltmp165:
	mov	r7, sp
Ltmp166:
Ltmp167:
	mov	r1, #0
	bl	_p_61_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool_llvm
	pop	{r7, pc}
Leh_func_end87:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ErrorOccured
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ErrorOccured:
Leh_func_begin88:
	push	{r7, lr}
Ltmp168:
	mov	r7, sp
Ltmp169:
Ltmp170:
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC88_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC88_0+8))
LPC88_0:
	add	r1, pc, r1
	ldr	r1, [r1, #456]
	bl	_p_55_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string_llvm
	pop	{r7, pc}
Leh_func_end88:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ShouldShowBackgroundChooser
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ShouldShowBackgroundChooser:
Leh_func_begin89:
	push	{r4, r7, lr}
Ltmp171:
	add	r7, sp, #4
Ltmp172:
	push	{r8}
Ltmp173:
	mov	r4, r0
	bl	_p_60_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r0, [r0, #42]
	cmp	r0, #0
	beq	LBB89_2
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC89_1+8))
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC89_1+8))
LPC89_1:
	add	r0, pc, r0
	ldr	r0, [r0, #464]
	mov	r8, r0
	mov	r0, r4
	bl	_p_63_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm
	pop	{r8}
	pop	{r4, r7, pc}
LBB89_2:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC89_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC89_0+8))
LPC89_0:
	add	r0, pc, r0
	ldr	r1, [r0, #460]
	mov	r0, r4
	bl	_p_55_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end89:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__ctor_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__ctor_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer:
Leh_func_begin90:
	push	{r4, r5, r6, r7, lr}
Ltmp174:
	add	r7, sp, #12
Ltmp175:
	push	{r10, r11}
Ltmp176:
	sub	sp, sp, #16
	str	r2, [sp, #12]
	stmib	sp, {r1, r3}
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC90_1+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC90_1+8))
LPC90_1:
	add	r5, pc, r5
	ldr	r0, [r5, #468]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #0
	mov	r6, r0
	bl	_p_64_plt_System_Threading_AutoResetEvent__ctor_bool_llvm
	mov	r0, r4
	str	r6, [r4, #24]
	bl	_p_34_plt_OffSeasonPro_Core_ViewModels_BaseViewModel__ctor_llvm
	cmp	r4, #0
	beq	LBB90_2
	ldr	r0, [r5, #472]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r6, r0
	ldr	r0, [r5, #476]
	str	r4, [r6, #16]
	str	r0, [r6, #20]
	ldr	r0, [r5, #480]
	str	r0, [r6, #28]
	ldr	r0, [r5, #484]
	str	r0, [r6, #12]
	ldr	r10, [r4, #24]
	ldr	r0, [r5, #488]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r11, r0
	mvn	r0, #0
	mov	r1, r6
	mov	r2, r10
	mvn	r3, #0
	str	r0, [sp]
	mov	r0, r11
	bl	_p_65_plt_System_Threading_Timer__ctor_System_Threading_TimerCallback_object_int_int_llvm
	ldr	r0, [sp, #8]
	str	r11, [r4, #36]
	str	r0, [r4, #20]
	ldr	r0, [sp, #4]
	str	r0, [r4, #28]
	ldr	r0, [sp, #12]
	str	r0, [r4, #32]
	mov	r0, #1
	strh	r0, [r4, #49]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_67_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int_llvm
	ldr	r1, [r5, #492]
	mov	r0, r4
	bl	_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp177:
LBB90_2:
	ldr	r0, LCPI90_0
LPC90_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI90_0:
	.long	Ltmp177-(LPC90_0+8)
	.end_data_region
Leh_func_end90:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings:
Leh_func_begin91:
	push	{r7, lr}
Ltmp178:
	mov	r7, sp
Ltmp179:
	push	{r8}
Ltmp180:
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC91_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC91_0+8))
LPC91_0:
	add	r1, pc, r1
	ldr	r1, [r1, #248]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end91:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand:
Leh_func_begin92:
	push	{r4, r5, r6, r7, lr}
Ltmp181:
	add	r7, sp, #12
Ltmp182:
Ltmp183:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB92_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC92_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC92_1+8))
LPC92_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #496]
	str	r0, [r4, #20]
	ldr	r0, [r6, #500]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp184:
LBB92_2:
	ldr	r0, LCPI92_0
LPC92_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI92_0:
	.long	Ltmp184-(LPC92_0+8)
	.end_data_region
Leh_func_end92:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout:
Leh_func_begin93:
	push	{r4, r5, r6, r7, lr}
Ltmp185:
	add	r7, sp, #12
Ltmp186:
Ltmp187:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB93_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC93_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC93_1+8))
LPC93_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #504]
	str	r0, [r4, #20]
	ldr	r0, [r6, #508]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp188:
LBB93_2:
	ldr	r0, LCPI93_0
LPC93_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI93_0:
	.long	Ltmp188-(LPC93_0+8)
	.end_data_region
Leh_func_end93:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StartMusic
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StartMusic:
Leh_func_begin94:
	push	{r4, r5, r6, r7, lr}
Ltmp189:
	add	r7, sp, #12
Ltmp190:
Ltmp191:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB94_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC94_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC94_1+8))
LPC94_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #512]
	str	r0, [r4, #20]
	ldr	r0, [r6, #516]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp192:
LBB94_2:
	ldr	r0, LCPI94_0
LPC94_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI94_0:
	.long	Ltmp192-(LPC94_0+8)
	.end_data_region
Leh_func_end94:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Reset
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Reset:
Leh_func_begin95:
	push	{r4, r5, r6, r7, lr}
Ltmp193:
	add	r7, sp, #12
Ltmp194:
Ltmp195:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB95_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC95_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC95_1+8))
LPC95_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #520]
	str	r0, [r4, #20]
	ldr	r0, [r6, #524]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp196:
LBB95_2:
	ldr	r0, LCPI95_0
LPC95_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI95_0:
	.long	Ltmp196-(LPC95_0+8)
	.end_data_region
Leh_func_end95:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_GoHome
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_GoHome:
Leh_func_begin96:
	push	{r4, r5, r6, r7, lr}
Ltmp197:
	add	r7, sp, #12
Ltmp198:
Ltmp199:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB96_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC96_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC96_1+8))
LPC96_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #528]
	str	r0, [r4, #20]
	ldr	r0, [r6, #532]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp200:
LBB96_2:
	ldr	r0, LCPI96_0
LPC96_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI96_0:
	.long	Ltmp200-(LPC96_0+8)
	.end_data_region
Leh_func_end96:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Playing
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Playing:
Leh_func_begin97:
	ldrb	r0, [r0, #60]
	bx	lr
Leh_func_end97:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ResizeLabel
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ResizeLabel:
Leh_func_begin98:
	ldrb	r0, [r0, #61]
	bx	lr
Leh_func_end98:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool:
Leh_func_begin99:
	push	{r4, r5, r6, r7, lr}
Ltmp201:
	add	r7, sp, #12
Ltmp202:
	push	{r8}
Ltmp203:
	mov	r4, r0
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC99_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC99_1+8))
LPC99_1:
	add	r6, pc, r6
	ldr	r0, [r6, #536]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB99_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB99_3
LBB99_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #416]
	mov	r8, r0
	mov	r0, r5
	bl	_p_58_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #420]
	mov	r8, r0
	mov	r0, r4
	bl	_p_59_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp204:
LBB99_3:
	ldr	r0, LCPI99_0
LPC99_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI99_0:
	.long	Ltmp204-(LPC99_0+8)
	.end_data_region
Leh_func_end99:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount:
Leh_func_begin100:
	push	{r4, r5, r6, r7, lr}
Ltmp205:
	add	r7, sp, #12
Ltmp206:
	push	{r10, r11}
Ltmp207:
	vpush	{d8, d9}
	mov	r4, r0
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	vmov	s0, r0
	vcvt.f32.s32	s0, s0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_69_plt_System_Math_Log10_double_llvm
	vmov.f64	d8, #1.000000e+00
	vmov	d0, r0, r1
	vadd.f64	d0, d0, d8
	vmov	r0, r1, d0
	bl	_p_70_plt_System_Math_Floor_double_llvm
	mov	r10, r0
	mov	r0, r4
	mov	r11, r1
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #24]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	vmov	s0, r0
	vcvt.f32.s32	s0, s0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_69_plt_System_Math_Log10_double_llvm
	vmov	d0, r0, r1
	vadd.f64	d0, d0, d8
	vmov	r0, r1, d0
	bl	_p_70_plt_System_Math_Floor_double_llvm
	mov	r5, r0
	mov	r0, r4
	mov	r6, r1
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #28]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	vmov	s0, r0
	vcvt.f32.s32	s0, s0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_69_plt_System_Math_Log10_double_llvm
	vmov	d0, r0, r1
	vmov	d9, r5, r6
	vadd.f64	d0, d0, d8
	vmov	d8, r10, r11
	vmov	r0, r1, d0
	bl	_p_70_plt_System_Math_Floor_double_llvm
	vcvt.s32.f64	s0, d8
	vcvt.s32.f64	s2, d9
	vmov	d2, r0, r1
	vmov	r0, s0
	vmov	r1, s2
	vcvt.s32.f64	s0, d2
	cmp	r1, r0
	movgt	r0, r1
	vmov	r1, s0
	cmp	r1, r0
	movgt	r0, r1
	vpop	{d8, d9}
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end100:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ClockValue
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ClockValue:
Leh_func_begin101:
	vldr	s0, [r0, #52]
	vmov	r0, s0
	bx	lr
Leh_func_end101:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single:
Leh_func_begin102:
	push	{r4, r5, r6, r7, lr}
Ltmp208:
	add	r7, sp, #12
Ltmp209:
	push	{r8}
Ltmp210:
	mov	r4, r0
	str	r1, [r4, #52]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC102_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC102_1+8))
LPC102_1:
	add	r6, pc, r6
	ldr	r0, [r6, #540]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB102_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB102_3
LBB102_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #544]
	mov	r8, r0
	mov	r0, r5
	bl	_p_71_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_single_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #548]
	mov	r8, r0
	mov	r0, r4
	bl	_p_72_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp211:
LBB102_3:
	ldr	r0, LCPI102_0
LPC102_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI102_0:
	.long	Ltmp211-(LPC102_0+8)
	.end_data_region
Leh_func_end102:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BtnPausePlayText
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BtnPausePlayText:
Leh_func_begin103:
	ldr	r0, [r0, #40]
	bx	lr
Leh_func_end103:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string:
Leh_func_begin104:
	push	{r4, r5, r6, r7, lr}
Ltmp212:
	add	r7, sp, #12
Ltmp213:
	push	{r8}
Ltmp214:
	mov	r4, r0
	str	r1, [r4, #40]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC104_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC104_1+8))
LPC104_1:
	add	r6, pc, r6
	ldr	r0, [r6, #552]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB104_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB104_3
LBB104_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #556]
	mov	r8, r0
	mov	r0, r5
	bl	_p_73_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_string_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #560]
	mov	r8, r0
	mov	r0, r4
	bl	_p_74_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_System_Linq_Expressions_Expression_1_System_Func_1_string_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp215:
LBB104_3:
	ldr	r0, LCPI104_0
LPC104_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI104_0:
	.long	Ltmp215-(LPC104_0+8)
	.end_data_region
Leh_func_end104:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_MaxProgressValue
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_MaxProgressValue:
Leh_func_begin105:
	ldr	r0, [r0, #56]
	bx	lr
Leh_func_end105:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int:
Leh_func_begin106:
	push	{r4, r5, r6, r7, lr}
Ltmp216:
	add	r7, sp, #12
Ltmp217:
	push	{r8}
Ltmp218:
	mov	r4, r0
	str	r1, [r4, #56]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC106_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC106_1+8))
LPC106_1:
	add	r6, pc, r6
	ldr	r0, [r6, #564]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB106_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB106_3
LBB106_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #568]
	mov	r8, r0
	mov	r0, r5
	bl	_p_75_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_int_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #572]
	mov	r8, r0
	mov	r0, r4
	bl	_p_76_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp219:
LBB106_3:
	ldr	r0, LCPI106_0
LPC106_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI106_0:
	.long	Ltmp219-(LPC106_0+8)
	.end_data_region
Leh_func_end106:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_CurrentPosition
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_CurrentPosition:
Leh_func_begin107:
	ldr	r0, [r0, #44]
	bx	lr
Leh_func_end107:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string:
Leh_func_begin108:
	push	{r4, r5, r6, r7, lr}
Ltmp220:
	add	r7, sp, #12
Ltmp221:
	push	{r8}
Ltmp222:
	mov	r4, r0
	str	r1, [r4, #44]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC108_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC108_1+8))
LPC108_1:
	add	r6, pc, r6
	ldr	r0, [r6, #576]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB108_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB108_3
LBB108_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #556]
	mov	r8, r0
	mov	r0, r5
	bl	_p_73_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_string_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #560]
	mov	r8, r0
	mov	r0, r4
	bl	_p_74_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_System_Linq_Expressions_Expression_1_System_Func_1_string_llvm
	mov	r0, r4
	mov	r1, #1
	bl	_p_77_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp223:
LBB108_3:
	ldr	r0, LCPI108_0
LPC108_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI108_0:
	.long	Ltmp223-(LPC108_0+8)
	.end_data_region
Leh_func_end108:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ButtonsEnabled
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ButtonsEnabled:
Leh_func_begin109:
	ldrb	r0, [r0, #48]
	bx	lr
Leh_func_end109:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool:
Leh_func_begin110:
	push	{r4, r5, r6, r7, lr}
Ltmp224:
	add	r7, sp, #12
Ltmp225:
	push	{r8}
Ltmp226:
	mov	r4, r0
	strb	r1, [r4, #48]
	bl	_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC110_1+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC110_1+8))
LPC110_1:
	add	r6, pc, r6
	ldr	r0, [r6, #580]
	bl	_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB110_2
	ldr	r2, [r1]
	ldr	r0, [r6, #268]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB110_3
LBB110_2:
	mov	r0, r5
	bl	_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm
	mov	r5, r0
	ldr	r0, [r6, #256]
	mov	r1, #0
	bl	_p_14_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	ldr	r0, [r6, #416]
	mov	r8, r0
	mov	r0, r5
	bl	_p_58_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm
	mov	r1, r0
	ldr	r0, [r6, #420]
	mov	r8, r0
	mov	r0, r4
	bl	_p_59_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Ltmp227:
LBB110_3:
	ldr	r0, LCPI110_0
LPC110_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI110_0:
	.long	Ltmp227-(LPC110_0+8)
	.end_data_region
Leh_func_end110:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave:
Leh_func_begin111:
	push	{r4, r5, r6, r7, lr}
Ltmp228:
	add	r7, sp, #12
Ltmp229:
Ltmp230:
	mov	r5, r0
	cmp	r5, #0
	beq	LBB111_2
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC111_1+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC111_1+8))
LPC111_1:
	add	r6, pc, r6
	ldr	r0, [r6, #28]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	str	r5, [r4, #16]
	ldr	r0, [r6, #584]
	str	r0, [r4, #20]
	ldr	r0, [r6, #588]
	str	r0, [r4, #28]
	ldr	r0, [r6, #40]
	str	r0, [r4, #12]
	ldr	r0, [r6, #280]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm
	mov	r0, r5
	pop	{r4, r5, r6, r7, pc}
Ltmp231:
LBB111_2:
	ldr	r0, LCPI111_0
LPC111_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI111_0:
	.long	Ltmp231-(LPC111_0+8)
	.end_data_region
Leh_func_end111:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ReturnToHome
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ReturnToHome:
Leh_func_begin112:
	push	{r4, r7, lr}
Ltmp232:
	add	r7, sp, #4
Ltmp233:
	push	{r8}
Ltmp234:
	mov	r4, r0
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r0, [r0, #42]
	cmp	r0, #0
	bne	LBB112_3
	ldrb	r0, [r4, #51]
	cmp	r0, #0
	bne	LBB112_3
	mov	r0, r4
	bl	_p_78_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd_llvm
LBB112_3:
	mov	r0, r4
	bl	_p_42_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC112_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC112_0+8))
LPC112_0:
	add	r1, pc, r1
	ldr	r1, [r1, #296]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	mov	r1, #0
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end112:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_FinishedTts
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_FinishedTts:
Leh_func_begin113:
	push	{r4, r7, lr}
Ltmp235:
	add	r7, sp, #4
Ltmp236:
Ltmp237:
	mov	r1, #1
	mov	r4, r0
	bl	_p_79_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool_llvm
	ldrb	r0, [r4, #51]
	cmp	r0, #0
	beq	LBB113_2
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r0, [r0, #42]
	cmp	r0, #0
	popne	{r4, r7, pc}
	mov	r0, r4
	bl	_p_78_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd_llvm
	pop	{r4, r7, pc}
LBB113_2:
	mov	r0, r4
	bl	_p_80_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_llvm
	pop	{r4, r7, pc}
Leh_func_end113:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseOrResume
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseOrResume:
Leh_func_begin114:
	push	{r4, r5, r7, lr}
Ltmp238:
	add	r7, sp, #8
Ltmp239:
	push	{r8}
Ltmp240:
	mov	r4, r0
	ldrb	r0, [r4, #60]
	cmp	r0, #0
	beq	LBB114_2
	mov	r0, r4
	bl	_p_82_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout_llvm
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_2+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_2+8))
LPC114_2:
	add	r0, pc, r0
	ldr	r1, [r0, #592]
	mov	r0, r4
	bl	_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm
	b	LBB114_7
LBB114_2:
	ldrb	r0, [r4, #51]
	cmp	r0, #0
	beq	LBB114_4
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	bl	_p_62_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm
	mov	r0, #0
	strb	r0, [r4, #51]
	mov	r0, r4
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_3+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_3+8))
LPC114_3:
	add	r5, pc, r5
	ldr	r1, [r5, #292]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	ldr	r1, [r5, #492]
	b	LBB114_6
LBB114_4:
	ldrb	r0, [r4, #50]
	cmp	r0, #0
	beq	LBB114_10
	mov	r0, #0
	strb	r0, [r4, #50]
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_1+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_1+8))
LPC114_1:
	add	r0, pc, r0
	ldr	r1, [r0, #492]
LBB114_6:
	mov	r0, r4
	bl	_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm
	mov	r0, r4
	bl	_p_81_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout_llvm
LBB114_7:
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldrb	r0, [r0, #24]
	cmp	r0, #0
	bne	LBB114_9
	ldrb	r1, [r4, #49]
	mov	r0, #0
	cmp	r1, #0
	moveq	r0, #1
	strb	r0, [r4, #49]
LBB114_9:
	pop	{r8}
	pop	{r4, r5, r7, pc}
LBB114_10:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC114_0+8))
LPC114_0:
	add	r0, pc, r0
	ldr	r1, [r0, #492]
	mov	r0, r4
	bl	_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm
	mov	r0, r4
	bl	_p_80_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_llvm
	b	LBB114_7
Leh_func_end114:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResetWorkout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResetWorkout:
Leh_func_begin115:
	push	{r4, r5, r6, r7, lr}
Ltmp241:
	add	r7, sp, #12
Ltmp242:
	push	{r8}
Ltmp243:
	mov	r4, r0
	bl	_p_83_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout_llvm
	mov	r0, r4
	mov	r1, #0
	mov	r6, #0
	bl	_p_84_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single_llvm
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	bl	_p_62_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC115_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC115_0+8))
LPC115_0:
	add	r5, pc, r5
	ldr	r0, [r5, #596]
	ldr	r1, [r0]
	mov	r0, r4
	bl	_p_85_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string_llvm
	mov	r0, r4
	strb	r6, [r4, #49]
	strb	r6, [r4, #51]
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	strb	r6, [r0, #24]
	mov	r0, #1
	strb	r0, [r4, #50]
	mov	r0, r4
	ldr	r1, [r5, #600]
	bl	_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm
	mov	r0, r4
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #292]
	sub	r2, r2, #48
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end115:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_Workout_Timer_Tick_object
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_Workout_Timer_Tick_object:
Leh_func_begin116:
	push	{r4, r7, lr}
Ltmp244:
	add	r7, sp, #4
Ltmp245:
Ltmp246:
	mov	r4, r0
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r0, [r0, #20]
	ldr	r1, [r4, #56]
	cmp	r0, r1
	bge	LBB116_2
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r1, [r0, #20]
	add	r1, r1, #1
	str	r1, [r0, #20]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r0, [r0, #20]
	vmov	s0, r0
	mov	r0, r4
	vcvt.f32.s32	s0, s0
	vmov	r1, s0
	bl	_p_84_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single_llvm
	pop	{r4, r7, pc}
LBB116_2:
	mov	r0, r4
	bl	_p_86_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction_llvm
	pop	{r4, r7, pc}
Leh_func_end116:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction:
Leh_func_begin117:
	push	{r4, r5, r6, r7, lr}
Ltmp247:
	add	r7, sp, #12
Ltmp248:
	push	{r8}
Ltmp249:
	mov	r1, #0
	mov	r4, r0
	mov	r5, #0
	bl	_p_84_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single_llvm
	mov	r0, r4
	bl	_p_83_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout_llvm
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldrb	r0, [r0, #25]
	cmp	r0, #0
	beq	LBB117_2
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	strb	r5, [r0, #25]
	mov	r0, #1
	strb	r0, [r4, #49]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_67_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int_llvm
LBB117_2:
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldrb	r0, [r0, #24]
	cmp	r0, #0
	beq	LBB117_4
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	mov	r1, #0
	strb	r1, [r0, #24]
	mov	r0, #1
	strb	r0, [r4, #49]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_67_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int_llvm
LBB117_4:
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r6, [r0, #8]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	cmp	r6, r5
	bge	LBB117_6
	ldr	r1, [r0, #8]
	add	r1, r1, #1
	str	r1, [r0, #8]
	b	LBB117_7
LBB117_6:
	mov	r1, #1
	str	r1, [r0, #8]
	mov	r0, r4
	bl	_p_87_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet_llvm
LBB117_7:
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldrb	r0, [r0, #40]
	cmp	r0, #0
	beq	LBB117_9
	ldr	r0, [r4, #20]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC117_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC117_0+8))
LPC117_0:
	add	r1, pc, r1
	ldr	r1, [r1, #604]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB117_9:
	mov	r0, r4
	bl	_p_81_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout_llvm
	mov	r0, r4
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC117_1+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC117_1+8))
LPC117_1:
	add	r1, pc, r1
	ldr	r1, [r1, #292]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end117:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet:
Leh_func_begin118:
	push	{r4, r5, r6, r7, lr}
Ltmp250:
	add	r7, sp, #12
Ltmp251:
Ltmp252:
	mov	r4, r0
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r6, [r0, #12]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #16]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	cmp	r6, r5
	ldrlt	r1, [r0, #12]
	addlt	r1, r1, #1
	strlt	r1, [r0, #12]
	poplt	{r4, r5, r6, r7, pc}
	mov	r1, #1
	str	r1, [r0, #12]
	mov	r0, r4
	bl	_p_88_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end118:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation:
Leh_func_begin119:
	push	{r4, r5, r6, r7, lr}
Ltmp253:
	add	r7, sp, #12
Ltmp254:
Ltmp255:
	mov	r4, r0
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r6, [r0, #16]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #8]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	cmp	r6, r5
	mov	r1, #1
	strge	r1, [r0, #16]
	strbge	r1, [r4, #51]
	popge	{r4, r5, r6, r7, pc}
	strb	r1, [r0, #24]
	mov	r5, #0
	mov	r0, r4
	strb	r5, [r4, #49]
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	ldr	r1, [r0, #16]
	add	r1, r1, #1
	str	r1, [r0, #16]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	str	r5, [r0, #20]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	str	r5, [r0, #8]
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #24]
	bl	_p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_67_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end119:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout:
Leh_func_begin120:
	push	{r4, r7, lr}
Ltmp256:
	add	r7, sp, #4
Ltmp257:
Ltmp258:
	mov	r4, r0
	bl	_p_82_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout_llvm
	mov	r0, r4
	bl	_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm
	ldr	r1, [r0]
	ldr	r0, [r0, #36]
	mov	r1, #0
	str	r1, [r0, #20]
	pop	{r4, r7, pc}
Leh_func_end120:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout:
Leh_func_begin121:
	push	{r4, r5, r7, lr}
Ltmp259:
	add	r7, sp, #8
Ltmp260:
	push	{r8}
Ltmp261:
	mov	r4, r0
	bl	_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm
	ldr	r2, [r0]
	movw	r5, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC121_0+8))
	movt	r5, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC121_0+8))
LPC121_0:
	add	r5, pc, r5
	ldr	r1, [r5, #292]
	sub	r2, r2, #48
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	ldr	r0, [r4, #36]
	mvn	r2, #0
	ldr	r1, [r0]
	mvn	r1, #0
	bl	_p_89_plt_System_Threading_Timer_Change_int_int_llvm
	ldr	r0, [r4, #20]
	ldr	r2, [r0]
	ldr	r1, [r5, #608]
	sub	r2, r2, #44
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	mov	r0, #0
	strb	r0, [r4, #60]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end121:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd:
Leh_func_begin122:
	push	{r4, r5, r6, r7, lr}
Ltmp262:
	add	r7, sp, #12
Ltmp263:
	push	{r8, r10, r11}
Ltmp264:
	movw	r11, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC122_0+8))
	mov	r6, r0
	movt	r11, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC122_0+8))
LPC122_0:
	add	r11, pc, r11
	ldr	r0, [r11, #620]
	ldr	r10, [r11, #616]
	ldr	r4, [r6, #32]
	bl	_p_5_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r2, r10
	mov	r5, r0
	bl	_p_90_plt_OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string_llvm
	ldr	r1, [r4]
	ldr	r0, [r11, #628]
	sub	r1, r1, #76
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end122:

	.private_extern	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__get_StartMusicm__6
	.align	2
_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__get_StartMusicm__6:
Leh_func_begin123:
	push	{r7, lr}
Ltmp265:
	mov	r7, sp
Ltmp266:
	push	{r8}
Ltmp267:
	ldr	r0, [r0, #20]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC123_0+8))
	movt	r1, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC123_0+8))
LPC123_0:
	add	r1, pc, r1
	ldr	r1, [r1, #612]
	sub	r2, r2, #56
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end123:

	.private_extern	_OffSeasonPro_Core__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.align	2
_OffSeasonPro_Core__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs:
Leh_func_begin124:
	push	{r4, r5, r6, r7, lr}
Ltmp268:
	add	r7, sp, #12
Ltmp269:
Ltmp270:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC124_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC124_0+8))
LPC124_0:
	add	r0, pc, r0
	ldr	r0, [r0, #632]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB124_2
	bl	_p_91_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB124_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB124_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB124_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB124_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB124_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end124:

	.private_extern	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_OffSeasonPro_Core__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin125:
	push	{r4, r5, r7, lr}
Ltmp271:
	add	r7, sp, #8
Ltmp272:
Ltmp273:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC125_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC125_0+8))
LPC125_0:
	add	r0, pc, r0
	ldr	r0, [r0, #632]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB125_2
	bl	_p_91_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB125_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB125_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB125_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB125_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB125_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end125:

	.private_extern	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.align	2
_OffSeasonPro_Core__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string:
Leh_func_begin126:
	push	{r4, r5, r6, r7, lr}
Ltmp274:
	add	r7, sp, #12
Ltmp275:
	push	{r10, r11}
Ltmp276:
	sub	sp, sp, #28
	bic	sp, sp, #7
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC126_0+8))
	mov	r10, r3
	mov	r11, r2
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC126_0+8))
LPC126_0:
	add	r0, pc, r0
	ldr	r0, [r0, #632]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB126_2
	bl	_p_91_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB126_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB126_4
	ldr	r6, [r0, #12]
	mov	r1, sp
	mov	r2, r11
	mov	r3, r10
	blx	r6
LBB126_4:
	ldr	r0, [r5, #16]
	ldr	r6, [r5, #8]
	cmp	r0, #0
	beq	LBB126_6
	add	r1, sp, #8
	mov	r2, r11
	mov	r3, r10
	blx	r6
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	b	LBB126_7
LBB126_6:
	add	r1, sp, #16
	mov	r0, r11
	mov	r2, r10
	blx	r6
	ldr	r0, [sp, #16]
	str	r0, [r4]
	ldr	r0, [sp, #20]
LBB126_7:
	str	r0, [r4, #4]
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end126:

	.private_extern	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_OffSeasonPro_Core__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin127:
	push	{r4, r5, r7, lr}
Ltmp277:
	add	r7, sp, #8
Ltmp278:
Ltmp279:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC127_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC127_0+8))
LPC127_0:
	add	r0, pc, r0
	ldr	r0, [r0, #632]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB127_2
	bl	_p_91_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB127_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB127_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB127_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB127_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB127_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end127:

	.private_extern	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.align	2
_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool:
Leh_func_begin128:
	push	{r4, r7, lr}
Ltmp280:
	add	r7, sp, #4
Ltmp281:
	push	{r8}
Ltmp282:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC128_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC128_0+8))
LPC128_0:
	add	r0, pc, r0
	ldr	r0, [r0, #636]
	mov	r8, r0
	mov	r0, r4
	bl	_p_92_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_93_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end128:

	.private_extern	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	.align	2
_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single:
Leh_func_begin129:
	push	{r4, r7, lr}
Ltmp283:
	add	r7, sp, #4
Ltmp284:
	push	{r8}
Ltmp285:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC129_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC129_0+8))
LPC129_0:
	add	r0, pc, r0
	ldr	r0, [r0, #640]
	mov	r8, r0
	mov	r0, r4
	bl	_p_94_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_93_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end129:

	.private_extern	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int
	.align	2
_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int:
Leh_func_begin130:
	push	{r4, r7, lr}
Ltmp286:
	add	r7, sp, #4
Ltmp287:
	push	{r8}
Ltmp288:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC130_0+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC130_0+8))
LPC130_0:
	add	r0, pc, r0
	ldr	r0, [r0, #644]
	mov	r8, r0
	mov	r0, r4
	bl	_p_95_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_93_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end130:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool:
Leh_func_begin131:
	push	{r4, r5, r6, r7, lr}
Ltmp289:
	add	r7, sp, #12
Ltmp290:
	push	{r8}
Ltmp291:
	mov	r5, r0
	cmp	r1, #0
	beq	LBB131_10
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC131_0+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC131_0+8))
LPC131_0:
	add	r6, pc, r6
	ldr	r0, [r6, #648]
	mov	r8, r0
	mov	r0, r1
	bl	_p_96_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm
	cmp	r0, #0
	beq	LBB131_11
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	mov	r4, #0
	cmp	r0, #0
	beq	LBB131_4
	ldr	r2, [r0]
	ldr	r1, [r6, #656]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r1
	movne	r0, #0
	mov	r4, r0
LBB131_4:
	cmp	r4, #0
	beq	LBB131_11
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB131_11
	cmp	r5, #0
	beq	LBB131_8
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	ldr	r1, [r5]
	ldr	r1, [r1, #12]
	ldr	r2, [r0]
	ldr	r2, [r2, #216]
	blx	r2
	tst	r0, #255
	beq	LBB131_11
LBB131_8:
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r2, [r0, #108]
	mov	r0, r4
	blx	r2
	ldr	r1, [r0]
	bl	_p_97_plt_System_Reflection_MethodBase_get_IsStatic_llvm
	tst	r0, #255
	bne	LBB131_11
	ldr	r0, [r4]
	ldr	r1, [r0, #72]
	mov	r0, r4
	blx	r1
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB131_10:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC131_1+8))
	mov	r1, #408
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC131_1+8))
LPC131_1:
	add	r0, pc, r0
	ldr	r0, [r0, #652]
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_101_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
LBB131_11:
	ldr	r5, [r6, #652]
	movw	r1, #430
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end131:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single:
Leh_func_begin132:
	push	{r4, r5, r6, r7, lr}
Ltmp292:
	add	r7, sp, #12
Ltmp293:
	push	{r8}
Ltmp294:
	mov	r5, r0
	cmp	r1, #0
	beq	LBB132_10
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC132_0+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC132_0+8))
LPC132_0:
	add	r6, pc, r6
	ldr	r0, [r6, #660]
	mov	r8, r0
	mov	r0, r1
	bl	_p_102_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm
	cmp	r0, #0
	beq	LBB132_11
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	mov	r4, #0
	cmp	r0, #0
	beq	LBB132_4
	ldr	r2, [r0]
	ldr	r1, [r6, #656]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r1
	movne	r0, #0
	mov	r4, r0
LBB132_4:
	cmp	r4, #0
	beq	LBB132_11
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB132_11
	cmp	r5, #0
	beq	LBB132_8
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	ldr	r1, [r5]
	ldr	r1, [r1, #12]
	ldr	r2, [r0]
	ldr	r2, [r2, #216]
	blx	r2
	tst	r0, #255
	beq	LBB132_11
LBB132_8:
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r2, [r0, #108]
	mov	r0, r4
	blx	r2
	ldr	r1, [r0]
	bl	_p_97_plt_System_Reflection_MethodBase_get_IsStatic_llvm
	tst	r0, #255
	bne	LBB132_11
	ldr	r0, [r4]
	ldr	r1, [r0, #72]
	mov	r0, r4
	blx	r1
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB132_10:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC132_1+8))
	mov	r1, #408
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC132_1+8))
LPC132_1:
	add	r0, pc, r0
	ldr	r0, [r0, #652]
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_101_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
LBB132_11:
	ldr	r5, [r6, #652]
	movw	r1, #430
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end132:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int:
Leh_func_begin133:
	push	{r4, r5, r6, r7, lr}
Ltmp295:
	add	r7, sp, #12
Ltmp296:
	push	{r8}
Ltmp297:
	mov	r5, r0
	cmp	r1, #0
	beq	LBB133_10
	movw	r6, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC133_0+8))
	movt	r6, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC133_0+8))
LPC133_0:
	add	r6, pc, r6
	ldr	r0, [r6, #664]
	mov	r8, r0
	mov	r0, r1
	bl	_p_103_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm
	cmp	r0, #0
	beq	LBB133_11
	ldr	r1, [r0]
	ldr	r0, [r0, #20]
	mov	r4, #0
	cmp	r0, #0
	beq	LBB133_4
	ldr	r2, [r0]
	ldr	r1, [r6, #656]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r1
	movne	r0, #0
	mov	r4, r0
LBB133_4:
	cmp	r4, #0
	beq	LBB133_11
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB133_11
	cmp	r5, #0
	beq	LBB133_8
	ldr	r0, [r4]
	ldr	r1, [r0, #80]
	mov	r0, r4
	blx	r1
	ldr	r1, [r5]
	ldr	r1, [r1, #12]
	ldr	r2, [r0]
	ldr	r2, [r2, #216]
	blx	r2
	tst	r0, #255
	beq	LBB133_11
LBB133_8:
	ldr	r0, [r4]
	mov	r1, #1
	ldr	r2, [r0, #108]
	mov	r0, r4
	blx	r2
	ldr	r1, [r0]
	bl	_p_97_plt_System_Reflection_MethodBase_get_IsStatic_llvm
	tst	r0, #255
	bne	LBB133_11
	ldr	r0, [r4]
	ldr	r1, [r0, #72]
	mov	r0, r4
	blx	r1
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB133_10:
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC133_1+8))
	mov	r1, #408
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC133_1+8))
LPC133_1:
	add	r0, pc, r0
	ldr	r0, [r0, #652]
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #519
	movt	r0, #512
	bl	_p_101_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
LBB133_11:
	ldr	r5, [r6, #652]
	movw	r1, #430
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end133:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool:
Leh_func_begin134:
	push	{r4, r5, r7, lr}
Ltmp298:
	add	r7, sp, #8
Ltmp299:
Ltmp300:
	ldr	r1, [r0]
	ldr	r2, [r0, #16]
	mov	r1, #0
	cmp	r2, #0
	moveq	r1, #1
	beq	LBB134_2
	movw	r3, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_1+8))
	movt	r3, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_1+8))
	ldr	r2, [r2]
LPC134_1:
	add	r3, pc, r3
	ldr	r3, [r3, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r5, [r2, #8]
	mov	r2, #1
	cmp	r5, r3
	moveq	r2, r1
	b	LBB134_3
LBB134_2:
	mov	r2, r1
LBB134_3:
	ldr	r1, [r0, #16]
	cmp	r2, #1
	bne	LBB134_7
	mov	r0, #0
	cmp	r1, #0
	beq	LBB134_6
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_2+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_2+8))
	ldr	r2, [r1]
LPC134_2:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB134_6:
	pop	{r4, r5, r7, pc}
LBB134_7:
	cmp	r1, #0
	beq	LBB134_9
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_5+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_5+8))
	ldr	r2, [r1]
LPC134_5:
	add	r0, pc, r0
	ldr	r0, [r0, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	bne	LBB134_12
LBB134_9:
	ldr	r0, [r1]
	ldr	r1, [r1, #16]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB134_11
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_3+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_3+8))
	ldr	r2, [r1]
LPC134_3:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB134_11:
	cmp	r0, #0
	popne	{r4, r5, r7, pc}
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_4+8))
	movw	r1, #579
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC134_4+8))
LPC134_4:
	add	r0, pc, r0
	ldr	r5, [r0, #652]
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp301:
LBB134_12:
	ldr	r0, LCPI134_0
LPC134_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI134_0:
	.long	Ltmp301-(LPC134_0+8)
	.end_data_region
Leh_func_end134:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single:
Leh_func_begin135:
	push	{r4, r5, r7, lr}
Ltmp302:
	add	r7, sp, #8
Ltmp303:
Ltmp304:
	ldr	r1, [r0]
	ldr	r2, [r0, #16]
	mov	r1, #0
	cmp	r2, #0
	moveq	r1, #1
	beq	LBB135_2
	movw	r3, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_1+8))
	movt	r3, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_1+8))
	ldr	r2, [r2]
LPC135_1:
	add	r3, pc, r3
	ldr	r3, [r3, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r5, [r2, #8]
	mov	r2, #1
	cmp	r5, r3
	moveq	r2, r1
	b	LBB135_3
LBB135_2:
	mov	r2, r1
LBB135_3:
	ldr	r1, [r0, #16]
	cmp	r2, #1
	bne	LBB135_7
	mov	r0, #0
	cmp	r1, #0
	beq	LBB135_6
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_2+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_2+8))
	ldr	r2, [r1]
LPC135_2:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB135_6:
	pop	{r4, r5, r7, pc}
LBB135_7:
	cmp	r1, #0
	beq	LBB135_9
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_5+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_5+8))
	ldr	r2, [r1]
LPC135_5:
	add	r0, pc, r0
	ldr	r0, [r0, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	bne	LBB135_12
LBB135_9:
	ldr	r0, [r1]
	ldr	r1, [r1, #16]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB135_11
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_3+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_3+8))
	ldr	r2, [r1]
LPC135_3:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB135_11:
	cmp	r0, #0
	popne	{r4, r5, r7, pc}
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_4+8))
	movw	r1, #579
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC135_4+8))
LPC135_4:
	add	r0, pc, r0
	ldr	r5, [r0, #652]
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp305:
LBB135_12:
	ldr	r0, LCPI135_0
LPC135_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI135_0:
	.long	Ltmp305-(LPC135_0+8)
	.end_data_region
Leh_func_end135:

	.private_extern	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int
	.align	2
_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int:
Leh_func_begin136:
	push	{r4, r5, r7, lr}
Ltmp306:
	add	r7, sp, #8
Ltmp307:
Ltmp308:
	ldr	r1, [r0]
	ldr	r2, [r0, #16]
	mov	r1, #0
	cmp	r2, #0
	moveq	r1, #1
	beq	LBB136_2
	movw	r3, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_1+8))
	movt	r3, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_1+8))
	ldr	r2, [r2]
LPC136_1:
	add	r3, pc, r3
	ldr	r3, [r3, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r5, [r2, #8]
	mov	r2, #1
	cmp	r5, r3
	moveq	r2, r1
	b	LBB136_3
LBB136_2:
	mov	r2, r1
LBB136_3:
	ldr	r1, [r0, #16]
	cmp	r2, #1
	bne	LBB136_7
	mov	r0, #0
	cmp	r1, #0
	beq	LBB136_6
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_2+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_2+8))
	ldr	r2, [r1]
LPC136_2:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB136_6:
	pop	{r4, r5, r7, pc}
LBB136_7:
	cmp	r1, #0
	beq	LBB136_9
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_5+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_5+8))
	ldr	r2, [r1]
LPC136_5:
	add	r0, pc, r0
	ldr	r0, [r0, #672]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	bne	LBB136_12
LBB136_9:
	ldr	r0, [r1]
	ldr	r1, [r1, #16]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB136_11
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_3+8))
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_3+8))
	ldr	r2, [r1]
LPC136_3:
	add	r0, pc, r0
	ldr	r0, [r0, #668]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB136_11:
	cmp	r0, #0
	popne	{r4, r5, r7, pc}
	movw	r0, :lower16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_4+8))
	movw	r1, #579
	movt	r0, :upper16:(_mono_aot_OffSeasonPro_Core_got-(LPC136_4+8))
LPC136_4:
	add	r0, pc, r0
	ldr	r5, [r0, #652]
	mov	r0, r5
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	mov	r0, r5
	mov	r1, #408
	bl	_p_98_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_100_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp309:
LBB136_12:
	ldr	r0, LCPI136_0
LPC136_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI136_0:
	.long	Ltmp309-(LPC136_0+8)
	.end_data_region
Leh_func_end136:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_mono_aot_OffSeasonPro_Core_got,1172,4
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_remove_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_ReportError_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__m__0
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_get_Message
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_set_Message_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs__ctor_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_get_Message
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_set_Message_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StationCount
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StationCount_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_PartnerCount
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_PartnerCount_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetCount
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetCount_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetLengthSeconds
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetLengthSeconds_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_TransitionLengthSeconds
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_TransitionLengthSeconds_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SkipAfterLift
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SkipAfterLift_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StartupLength
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StartupLength_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_WorkoutBackgroundImage
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_WorkoutBackgroundImage_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_UsePhotoAlbumForBgImage
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_UsePhotoAlbumForBgImage_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_IsPaid
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_IsPaid_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_CurrentWorkout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_CurrentWorkout_OffSeasonPro_Core_Models_Workout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Playlist
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_SaveSettings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_CreateTts_System_Action_System_Action
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_ReloadSettings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__SaveSettingsm__1_System_IO_Stream
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__LoadSettingsm__3_System_IO_Stream
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__m__2_object
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_App__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_App_InitalizeErrorSystem
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Models_Workout__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SaveSettings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Save
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__get_SendBackgroundRequestEmailm__4
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_RequestClose
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ShowSettingsCommand
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_ToggleAction
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__get_ShowSettingsCommandm__5
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Trace__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Info_string_object__
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Warn_string_object__
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_Trace_Error_string_object__
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_IsBusy
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel__ctor
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Cancel
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Save
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Finished
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ErrorOccured
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ShouldShowBackgroundChooser
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__ctor_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StartMusic
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Reset
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_GoHome
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Playing
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ResizeLabel
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ClockValue
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BtnPausePlayText
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_MaxProgressValue
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_CurrentPosition
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ButtonsEnabled
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ReturnToHome
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_FinishedTts
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseOrResume
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResetWorkout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_Workout_Timer_Tick_object
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd
	.no_dead_strip	_OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__get_StartMusicm__6
	.no_dead_strip	_OffSeasonPro_Core__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	.no_dead_strip	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	.no_dead_strip	_OffSeasonPro_Core__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	.no_dead_strip	_OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int
	.no_dead_strip	_mono_aot_OffSeasonPro_Core_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	137
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	2
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	3
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	4
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	5
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	8
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	9
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	18
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	19
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	20
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	21
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	22
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	23
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	24
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	25
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	26
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	27
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	28
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	29
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	30
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	31
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	32
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	33
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	34
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	35
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	36
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	37
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	38
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	39
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	40
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	41
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	42
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	43
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	44
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	45
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	46
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	47
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	48
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	49
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	50
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	51
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	52
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	53
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	55
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	56
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	57
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	58
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	59
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	60
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	61
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	62
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	63
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	64
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	65
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	66
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	67
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	68
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	69
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	70
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	71
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	72
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	73
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	74
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	75
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	76
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	77
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	78
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	79
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	80
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	81
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	82
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	83
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	84
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	85
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	86
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	87
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	88
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	89
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	90
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	91
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	92
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	93
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	94
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	95
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	96
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	97
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	98
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	99
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	100
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	101
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	102
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	103
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	104
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	105
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	106
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	107
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	108
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	109
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	110
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	111
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	112
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	113
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	114
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	115
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	116
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	117
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	118
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	119
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	120
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	121
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	123
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	124
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	125
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	126
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	127
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	128
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	129
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	130
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	131
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	133
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	134
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	136
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	137
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	142
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	147
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	148
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	149
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	150
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	151
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	152
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	153
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	154
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	155
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	156
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
Lset137 = Leh_func_end136-Leh_func_begin136
	.long	Lset137
Lset138 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset138
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin3:
	.byte	0

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0

Lmono_eh_func_begin21:
	.byte	0

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0

Lmono_eh_func_begin29:
	.byte	0

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0

Lmono_eh_func_begin36:
	.byte	0

Lmono_eh_func_begin37:
	.byte	0

Lmono_eh_func_begin38:
	.byte	0

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin40:
	.byte	0

Lmono_eh_func_begin41:
	.byte	0

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin45:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin46:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin48:
	.byte	0

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin51:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin52:
	.byte	0

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin58:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin60:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin67:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin68:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin70:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin71:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin72:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin73:
	.byte	0

Lmono_eh_func_begin74:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin75:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin76:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin77:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin78:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin80:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin81:
	.byte	0

Lmono_eh_func_begin82:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin83:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin84:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin85:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin86:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin88:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin89:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin92:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin93:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin95:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin96:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin97:
	.byte	0

Lmono_eh_func_begin98:
	.byte	0

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin101:
	.byte	0

Lmono_eh_func_begin102:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin103:
	.byte	0

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin105:
	.byte	0

Lmono_eh_func_begin106:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin107:
	.byte	0

Lmono_eh_func_begin108:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin109:
	.byte	0

Lmono_eh_func_begin110:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin113:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin114:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin115:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin116:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin117:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin118:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin119:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin120:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin121:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin122:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin123:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin124:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin125:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin126:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin127:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin128:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin129:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin131:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin132:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin135:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin136:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "OffSeasonPro.Core.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Core_Extensions_ToInt32_string_0
_OffSeasonPro_Core_Extensions_ToInt32_string_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,24,0,139,229,0,0,160,227,0,0,139,229
	.byte 24,0,155,229
bl _p_7

	.byte 0,0,139,229,9,0,0,234,4,0,155,229,0,0,160,227,0,0,139,229
bl _p_104

	.byte 20,0,139,229,0,0,80,227,1,0,0,10,20,0,155,229
bl _p_100

	.byte 255,255,255,234,0,0,155,229,255,255,255,234,32,208,139,226,0,9,189,232,128,128,189,232

Lme_6:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream
_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream:

	.byte 128,64,45,233,13,112,160,225,32,9,45,233,76,208,77,226,13,176,160,225,48,0,139,229,52,16,139,229,0,0,160,227
	.byte 0,0,203,229,0,0,160,227,4,0,139,229,0,0,160,227,12,0,139,229,52,0,155,229
bl _p_110

	.byte 0,80,160,225,5,16,160,225,1,0,160,225,0,224,209,229
bl _p_109

	.byte 0,0,80,227,2,0,0,26,0,0,160,227,0,0,203,229,101,0,0,234,5,0,160,225,0,224,213,229
bl _p_109

	.byte 0,16,160,225,0,224,209,229
bl _p_108

	.byte 4,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 184
	.byte 0,0,159,231,60,0,139,229
bl _p_20

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 188
	.byte 0,0,159,231
bl _p_5

	.byte 60,16,155,229,56,0,139,229
bl _p_21

	.byte 56,0,155,229,4,16,155,229
bl _p_107

	.byte 0,80,160,225,0,0,85,227,9,0,0,10,0,0,149,229,0,0,144,229,8,0,144,229,4,0,144,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 672
	.byte 1,16,159,231,1,0,80,225,69,0,0,27,8,80,139,229,48,0,155,229,16,80,128,229,1,0,160,227,0,0,203,229
	.byte 0,0,0,235,57,0,0,234,32,224,139,229,4,0,155,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 668
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,32,192,155,229,12,240,160,225,16,0,155,229,16,0,155,229
	.byte 12,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 664
	.byte 0,0,159,231,56,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 76
	.byte 0,0,159,231,1,16,160,227
bl _p_14

	.byte 20,0,139,229,60,0,139,229,20,0,155,229,64,0,139,229,12,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225
	.byte 32,240,145,229,0,32,160,225,64,48,155,229,3,0,160,225,0,16,160,227,0,48,147,229,15,224,160,225,128,240,147,229
	.byte 56,0,155,229,60,16,155,229
bl _p_105

	.byte 0,0,160,227,0,0,203,229
bl _p_104

	.byte 44,0,139,229,0,0,80,227,1,0,0,10,44,0,155,229
bl _p_100

	.byte 255,255,255,234,0,0,219,229,255,255,255,234,76,208,139,226,32,9,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_106

	.byte 93,2,0,2

Lme_36:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout
_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,96,208,77,226,13,176,160,225,76,0,139,229,0,0,160,227,8,0,139,229
	.byte 0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,0,0,160,227,20,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 288
	.byte 0,0,159,231
bl _p_5

	.byte 80,0,139,229
bl _p_43

	.byte 80,0,155,229,8,0,139,229,76,0,155,229,51,0,208,229,0,0,80,227,117,0,0,26,76,0,155,229
bl _p_66

	.byte 0,16,160,225,0,224,209,229,36,0,144,229,25,0,208,229,0,0,80,227,19,0,0,10,8,32,155,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 692
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_114

	.byte 76,0,155,229,80,0,139,229,76,0,155,229
bl _p_66

	.byte 0,16,160,225,0,224,209,229,28,0,144,229
bl _OffSeasonPro_Core_Extensions_ToInt32_string_0

	.byte 0,16,160,225,80,0,155,229
bl _p_67

	.byte 64,0,0,234,76,0,155,229
bl _p_66

	.byte 0,16,160,225,0,224,209,229,36,0,144,229,24,0,208,229,0,0,80,227,8,0,0,10,8,32,155,229,0,16,159,229
	.byte 0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 120
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_114

	.byte 47,0,0,234,8,32,155,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 80
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_114

	.byte 8,0,155,229,84,0,139,229,76,0,155,229
bl _p_66

	.byte 0,16,160,225,0,224,209,229,36,0,144,229,0,0,80,227,215,0,0,11,12,0,128,226
bl _p_115

	.byte 0,16,160,225,84,32,155,229,2,0,160,225,0,224,210,229
bl _p_114

	.byte 8,32,155,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 88
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_114

	.byte 8,0,155,229,80,0,139,229,76,0,155,229
bl _p_66

	.byte 0,16,160,225,0,224,209,229,36,0,144,229,0,0,80,227,191,0,0,11,8,0,128,226
bl _p_115

	.byte 0,16,160,225,80,32,155,229,2,0,160,225,0,224,210,229
bl _p_114

	.byte 43,0,0,234,24,0,155,229,24,0,155,229,12,0,139,229,0,16,160,225,0,16,145,229,15,224,160,225,76,240,145,229
	.byte 80,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 76
	.byte 0,0,159,231,0,16,160,227
bl _p_14

	.byte 0,16,160,225,80,0,155,229
bl _p_111
bl _p_104

	.byte 68,0,139,229,0,0,80,227,1,0,0,10,68,0,155,229
bl _p_100

	.byte 19,0,0,234,8,32,155,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 112
	.byte 1,16,159,231,2,0,160,225,0,224,210,229
bl _p_114

	.byte 76,0,155,229,0,16,160,227,49,16,192,229,76,0,155,229,0,16,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 588
	.byte 1,16,159,231
bl _p_68

	.byte 76,0,155,229
bl _p_66
bl _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings

	.byte 0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 688
	.byte 0,0,159,231
bl _p_5

	.byte 80,0,139,229,0,16,160,227
bl _p_113

	.byte 80,0,155,229,16,0,139,229,16,0,155,229,80,0,139,229,0,11,159,237,1,0,0,234,0,0,0,0,0,0,240,63
	.byte 32,0,139,226,2,11,13,237,8,16,29,229,4,32,29,229
bl _p_112

	.byte 80,48,155,229,3,0,160,225,32,16,155,229,36,32,155,229,0,48,147,229,15,224,160,225,56,240,147,229,0,0,0,235
	.byte 15,0,0,234,56,224,139,229,16,0,155,229,0,0,80,227,9,0,0,10,16,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 668
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,56,192,155,229,12,240,160,225,76,0,155,229,0,16,160,227
bl _p_79

	.byte 76,0,155,229,28,0,144,229,88,0,139,229,8,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,32,240,145,229
	.byte 84,0,139,229,76,0,155,229,80,0,139,229,0,0,80,227,73,0,0,11,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 16
	.byte 0,0,159,231
bl _p_5

	.byte 0,32,160,225,80,0,155,229,84,16,155,229,88,48,155,229,16,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 684
	.byte 0,0,159,231,20,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 680
	.byte 0,0,159,231,28,0,130,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 28
	.byte 0,0,159,231,12,0,130,229,3,0,160,225,0,48,147,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 676
	.byte 8,128,159,231,4,224,143,226,68,240,19,229,0,0,0,0,25,0,0,234,28,0,155,229,28,0,155,229,20,0,139,229
	.byte 0,16,160,225,0,16,145,229,15,224,160,225,76,240,145,229,80,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 76
	.byte 0,0,159,231,0,16,160,227
bl _p_14

	.byte 0,16,160,225,80,0,155,229
bl _p_111

	.byte 76,0,155,229
bl _p_80
bl _p_104

	.byte 72,0,139,229,0,0,80,227,1,0,0,10,72,0,155,229
bl _p_100

	.byte 255,255,255,234,76,0,155,229,80,0,139,229,8,16,155,229,1,0,160,225,0,16,145,229,15,224,160,225,32,240,145,229
	.byte 0,16,160,225,80,0,155,229
bl _p_85

	.byte 96,208,139,226,0,9,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_106

	.byte 6,2,0,2,14,16,160,225,0,0,159,229
bl _p_106

	.byte 118,2,0,2

Lme_7a:
.text
	.align 2
	.no_dead_strip _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0
_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,24,0,139,229,0,0,160,227,0,0,139,229
	.byte 24,0,155,229,36,48,144,229,3,0,160,225,250,31,160,227,250,47,160,227,0,224,211,229
bl _p_89

	.byte 24,0,155,229,20,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_OffSeasonPro_Core_got - . + 600
	.byte 8,128,159,231,4,224,143,226,56,240,17,229,0,0,0,0,24,0,155,229,1,16,160,227,60,16,192,229,19,0,0,234
	.byte 4,0,155,229,4,0,155,229,0,0,139,229,24,0,155,229,32,0,139,229,0,16,155,229,1,0,160,225,0,16,145,229
	.byte 15,224,160,225,76,240,145,229,0,16,160,225,32,0,155,229
bl _p_55
bl _p_104

	.byte 20,0,139,229,0,0,80,227,1,0,0,10,20,0,155,229
bl _p_100

	.byte 255,255,255,234,40,208,139,226,0,9,189,232,128,128,189,232

Lme_84:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_remove_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_ReportError_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__m__0
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_get_Message
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_set_Message_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs__ctor_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_get_Message
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_set_Message_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StationCount
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StationCount_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_PartnerCount
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_PartnerCount_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetCount
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetCount_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetLengthSeconds
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetLengthSeconds_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_TransitionLengthSeconds
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_TransitionLengthSeconds_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SkipAfterLift
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SkipAfterLift_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StartupLength
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StartupLength_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_WorkoutBackgroundImage
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_WorkoutBackgroundImage_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_UsePhotoAlbumForBgImage
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_UsePhotoAlbumForBgImage_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_IsPaid
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_IsPaid_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_CurrentWorkout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_CurrentWorkout_OffSeasonPro_Core_Models_Workout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Playlist
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_SaveSettings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_CreateTts_System_Action_System_Action
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_ReloadSettings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__SaveSettingsm__1_System_IO_Stream
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__LoadSettingsm__3_System_IO_Stream
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__m__2_object
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_App__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_App_InitalizeErrorSystem
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Models_Workout__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SaveSettings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Save
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__get_SendBackgroundRequestEmailm__4
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_RequestClose
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ShowSettingsCommand
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_ToggleAction
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__get_ShowSettingsCommandm__5
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Trace__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Info_string_object__
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Warn_string_object__
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Error_string_object__
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_IsBusy
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel__ctor
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Cancel
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Save
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Finished
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ErrorOccured
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ShouldShowBackgroundChooser
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__ctor_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StartMusic
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Reset
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_GoHome
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Playing
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ResizeLabel
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ClockValue
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BtnPausePlayText
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_MaxProgressValue
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_CurrentPosition
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ButtonsEnabled
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ReturnToHome
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_FinishedTts
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseOrResume
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResetWorkout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_Workout_Timer_Tick_object
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd
.no_dead_strip _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__get_StartMusicm__6
.no_dead_strip _OffSeasonPro_Core__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
.no_dead_strip _OffSeasonPro_Core__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _OffSeasonPro_Core__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
.no_dead_strip _OffSeasonPro_Core__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
.no_dead_strip _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
.no_dead_strip _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single
.no_dead_strip _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single
.no_dead_strip _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_remove_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_ReportError_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject__ReportErrorc__AnonStorey0__m__0
	bl _OffSeasonPro_Core_Extensions_ToInt32_string_0
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_InWorkout_OffSeasonPro_Core_Models_Workout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_get_Message
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage_set_Message_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs__ctor_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_get_Message
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_ErrorEventArgs_set_Message_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StationCount
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StationCount_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_PartnerCount
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_PartnerCount_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetCount
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetCount_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SetLengthSeconds
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SetLengthSeconds_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_TransitionLengthSeconds
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_TransitionLengthSeconds_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_SkipAfterLift
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_SkipAfterLift_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_StartupLength
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_StartupLength_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_WorkoutBackgroundImage
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_WorkoutBackgroundImage_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_UsePhotoAlbumForBgImage
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_UsePhotoAlbumForBgImage_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_IsPaid
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_IsPaid_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_get_CurrentWorkout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings_set_CurrentWorkout_OffSeasonPro_Core_Models_Workout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Settings__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Playlist
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_get_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_SaveSettings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_CreateTts_System_Action_System_Action
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_ReloadSettings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings
	bl _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__SaveSettingsm__1_System_IO_Stream
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__LoadSettingsm__3_System_IO_Stream
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_SimpleDataStore__CreateTtsc__AnonStorey1__m__2_object
	bl _OffSeasonPro_Core__OffSeasonPro_Core_App__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_App_InitalizeErrorSystem
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Models_Workout__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SendBackgroundRequestEmail
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_get_SaveSettings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Save
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel__get_SendBackgroundRequestEmailm__4
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel_RequestClose
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ShowSettingsCommand
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_get_ToggleActionCommand
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel_ToggleAction
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_HomeViewModel__get_ShowSettingsCommandm__5
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Trace__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Info_string_object__
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Warn_string_object__
	bl _OffSeasonPro_Core__OffSeasonPro_Core_Trace_Error_string_object__
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_Settings_OffSeasonPro_Core_Models_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_CancelSave
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_SaveSettings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_IsBusy
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_get_ShowBackgroundChooser
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel__ctor
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Cancel
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Save
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_Finished
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ErrorOccured
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_SettingsViewModel_ShouldShowBackgroundChooser
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__ctor_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger_OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BeginWorkoutCommand
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_PauseResumeWorkout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StartMusic
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Reset
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_GoHome
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Playing
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ResizeLabel
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_DigitCount
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ClockValue
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_BtnPausePlayText
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_MaxProgressValue
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_CurrentPosition
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_ButtonsEnabled
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_StopAndSave
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ReturnToHome
	bl _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_FinishedTts
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseOrResume
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResetWorkout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_Workout_Timer_Tick_object
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout
	bl _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd
	bl _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel__get_StartMusicm__6
	bl method_addresses
	bl _OffSeasonPro_Core__wrapper_delegate_invoke_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_invoke_void__this___object_TEventArgs_object_OffSeasonPro_Core_Models_ErrorEventArgs
	bl _OffSeasonPro_Core__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Core__wrapper_delegate_invoke_System_Collections_Generic_Dictionary_2_Transform_1_string_string_System_Collections_Generic_KeyValuePair_2_string_string_invoke_TRet__this___TKey_TValue_string_string
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _OffSeasonPro_Core__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	bl _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	bl _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	bl _OffSeasonPro_Core__Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single
	bl _OffSeasonPro_Core__Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 157,10,16,2
	.short 0, 10, 20, 30, 40, 50, 60, 71
	.short 82, 93, 104, 115, 126, 137, 157, 173
	.byte 1,4,4,2,7,2,3,2,2,2,0,0,0,0,0,0,0,0,31,2,35,2,2,2,2,2,2,2,2,2,55,2
	.byte 2,2,2,2,2,2,2,2,75,2,2,2,2,2,2,2,3,2,96,9,25,2,16,11,4,2,2,3,128,179,5,2
	.byte 2,3,7,7,7,4,6,128,226,7,3,2,4,4,4,2,3,7,129,13,4,3,2,3,3,3,3,7,7,129,55,2
	.byte 7,7,2,4,13,2,3,4,129,108,3,7,7,9,9,9,2,2,8,129,166,2,10,2,10,2,10,2,10,2,129,224
	.byte 9,3,25,2,8,7,2,5,2,130,33,2,5,4,10,255,255,255,253,202,130,58,4,255,255,255,253,194,0,0,0,130
	.byte 66,255,255,255,253,190,0,0,0,130,70,4,4,130,82,4,28,28,28,14,14
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,0,0,0,0,0,0,0
	.long 0,0,793,148,0,804,149,0
	.long 826,151,22,0,0,0,724,136
	.long 0,0,0,0,0,0,0,0
	.long 0,0,781,147,0,0,0,0
	.long 0,0,0,736,137,19,862,153
	.long 0,0,0,0,844,152,21,815
	.long 150,0,771,142,20,880,154,0
	.long 898,155,0,916,156,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 21,136,724,137,736,138,0,139
	.long 0,140,0,141,0,142,771,143
	.long 0,144,0,145,0,146,0,147
	.long 781,148,793,149,804,150,815,151
	.long 826,152,844,153,862,154,880,155
	.long 898,156,916
.section __TEXT, __const
	.align 3
class_name_table:

	.short 37, 0, 0, 9, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 4, 0, 16
	.short 0, 7, 0, 14, 0, 0, 0, 0
	.short 0, 20, 0, 12, 39, 2, 38, 0
	.short 0, 0, 0, 5, 0, 0, 0, 0
	.short 0, 18, 40, 0, 0, 0, 0, 6
	.short 37, 11, 0, 8, 0, 0, 0, 22
	.short 0, 17, 0, 0, 0, 0, 0, 1
	.short 0, 3, 0, 0, 0, 0, 0, 21
	.short 0, 0, 0, 10, 0, 13, 0, 15
	.short 0, 19, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 177,10,18,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 154, 165
	.short 176, 187
	.byte 131,166,2,1,1,1,12,6,4,4,5,131,207,4,4,12,12,3,6,5,5,6,132,13,4,6,6,3,4,3,3,3
	.byte 3,132,51,3,3,4,4,4,4,5,5,4,132,93,5,5,6,5,12,4,5,5,5,132,152,5,5,5,5,5,5,4
	.byte 12,12,132,214,12,12,5,7,6,12,12,4,5,133,38,4,5,5,5,5,4,4,4,4,133,82,4,5,5,12,12,5
	.byte 12,5,12,133,159,5,5,5,5,12,4,12,4,7,133,223,5,5,5,7,12,12,5,5,5,134,33,5,5,5,5,4
	.byte 4,12,4,4,134,86,5,4,4,4,5,5,5,5,5,134,133,5,5,5,5,7,7,12,12,7,134,210,12,7,12,12
	.byte 7,7,5,5,4,135,31,4,5,5,5,4,4,12,12,1,135,102,19,19,19,2,4,19,19,4,4,135,215,3,4,5
	.byte 5,5,4
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 157,10,16,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143, 163, 179
	.byte 140,70,3,3,3,3,3,3,13,3,3,0,0,0,0,0,0,0,0,140,110,3,140,116,3,3,3,3,3,3,3,3
	.byte 3,140,146,3,3,3,3,3,3,3,3,3,140,176,3,3,3,3,3,3,3,3,3,140,206,3,3,3,3,25,3,3
	.byte 3,3,141,2,3,3,3,3,3,3,3,3,3,141,32,3,3,3,3,3,3,3,3,3,141,62,3,3,3,3,3,3
	.byte 3,3,3,141,92,3,3,3,3,3,3,3,3,3,141,122,3,3,3,3,3,3,3,3,3,141,152,3,3,3,3,3
	.byte 3,3,3,3,141,182,3,3,37,3,3,3,3,3,3,141,246,3,3,13,3,255,255,255,241,244,142,15,3,255,255,255
	.byte 241,238,0,0,0,142,21,255,255,255,241,235,0,0,0,142,24,3,3,142,33,3,3,3,3,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11,25,12,13,0,72,14,8,135
	.byte 2,68,14,20,133,5,136,4,139,3,142,1,68,14,96,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4
	.byte 139,3,142,1,68,14,112,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68
	.byte 13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 22,10,3,2
	.short 0, 11, 22
	.byte 142,54,7,27,15,15,15,7,7,7,7,142,176,15,15,31,15,39,15,100,100,100,144,109,100

.text
	.align 4
plt:
_mono_aot_OffSeasonPro_Core_plt:
_p_1_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 708,2037
_p_2_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 712,2082
_p_3_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs__System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs__System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs__System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 716,2085
_p_4_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Remove_System_Delegate_System_Delegate
plt_System_Delegate_Remove_System_Delegate_System_Delegate:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 720,2097
_p_5_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 724,2100
_p_6_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject_InvokeOnMainThread_System_Action_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject_InvokeOnMainThread_System_Action
plt_Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject_InvokeOnMainThread_System_Action:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 728,2123
_p_7_plt_System_Convert_ToInt32_string_llvm:
	.no_dead_strip plt_System_Convert_ToInt32_string
plt_System_Convert_ToInt32_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 732,2128

.set _p_8_plt_OffSeasonPro_Core_Extensions_ToInt32_string_llvm, _OffSeasonPro_Core_Extensions_ToInt32_string_0
_p_9_plt_Cirrious_MvvmCross_Plugins_Messenger_MvxMessage__ctor_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_Messenger_MvxMessage__ctor_object
plt_Cirrious_MvvmCross_Plugins_Messenger_MvxMessage__ctor_object:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 740,2136
_p_10_plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings
plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettings:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 744,2141
_p_11_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech
plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 748,2146
_p_12_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_File_IMvxFileStore:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 752,2158
_p_13_plt_System_Collections_Generic_Dictionary_2_string_string__ctor_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string__ctor
plt_System_Collections_Generic_Dictionary_2_string_string__ctor:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 756,2170
_p_14_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 760,2181
_p_15_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 764,2207
_p_16_plt_string_Concat_object___llvm:
	.no_dead_strip plt_string_Concat_object__
plt_string_Concat_object__:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 768,2237
_p_17_plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string_llvm:
	.no_dead_strip plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string
plt_System_Collections_Generic_Dictionary_2_string_string_Add_string_string:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 772,2240
_p_18_plt_System_Threading_ThreadPool_QueueUserWorkItem_System_Threading_WaitCallback_llvm:
	.no_dead_strip plt_System_Threading_ThreadPool_QueueUserWorkItem_System_Threading_WaitCallback
plt_System_Threading_ThreadPool_QueueUserWorkItem_System_Threading_WaitCallback:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 776,2251
_p_19_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Platform_IMvxResourceLoader:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 780,2254
_p_20_plt__class_init_System_Xml_Serialization_XmlSerializer_llvm:
	.no_dead_strip plt__class_init_System_Xml_Serialization_XmlSerializer
plt__class_init_System_Xml_Serialization_XmlSerializer:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 784,2266
_p_21_plt_System_Xml_Serialization_XmlSerializer__ctor_System_Type_llvm:
	.no_dead_strip plt_System_Xml_Serialization_XmlSerializer__ctor_System_Type
plt_System_Xml_Serialization_XmlSerializer__ctor_System_Type:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 788,2271
_p_22_plt_System_Xml_Serialization_XmlSerializer_Serialize_System_IO_Stream_object_llvm:
	.no_dead_strip plt_System_Xml_Serialization_XmlSerializer_Serialize_System_IO_Stream_object
plt_System_Xml_Serialization_XmlSerializer_Serialize_System_IO_Stream_object:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 792,2276
_p_23_plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream
plt_OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 796,2281
_p_24_plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
plt_Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 800,2286
_p_25_plt_Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded
plt_Cirrious_MvvmCross_Plugins_ResourceLoader_PluginLoader_EnsureLoaded:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 804,2291
_p_26_plt_OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded_llvm:
	.no_dead_strip plt_OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded
plt_OffSeasonPro_Plugin_TextToSpeech_PluginLoader_EnsureLoaded:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 808,2296
_p_27_plt_Cirrious_MvvmCross_Plugins_Messenger_PluginLoader_EnsureLoaded_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Plugins_Messenger_PluginLoader_EnsureLoaded
plt_Cirrious_MvvmCross_Plugins_Messenger_PluginLoader_EnsureLoaded:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 812,2301
_p_28_plt_OffSeasonPro_Core_Models_SimpleDataStore__ctor_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Models_SimpleDataStore__ctor
plt_OffSeasonPro_Core_Models_SimpleDataStore__ctor:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 816,2306
_p_29_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IDataStore_OffSeasonPro_Core_Interfaces_IDataStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IDataStore_OffSeasonPro_Core_Interfaces_IDataStore
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IDataStore_OffSeasonPro_Core_Interfaces_IDataStore:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 820,2311
_p_30_plt_OffSeasonPro_Core_App_InitalizeErrorSystem_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_App_InitalizeErrorSystem
plt_OffSeasonPro_Core_App_InitalizeErrorSystem:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 824,2323
_p_31_plt_Cirrious_MvvmCross_ViewModels_MvxApplication_RegisterAppStart_OffSeasonPro_Core_ViewModels_HomeViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxApplication_RegisterAppStart_OffSeasonPro_Core_ViewModels_HomeViewModel
plt_Cirrious_MvvmCross_ViewModels_MvxApplication_RegisterAppStart_OffSeasonPro_Core_ViewModels_HomeViewModel:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 828,2328
_p_32_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorReporter_OffSeasonPro_Core_Interfaces_IErrorReporter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorReporter_OffSeasonPro_Core_Interfaces_IErrorReporter
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorReporter_OffSeasonPro_Core_Interfaces_IErrorReporter:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 832,2340
_p_33_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorSource_OffSeasonPro_Core_Interfaces_IErrorSource_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorSource_OffSeasonPro_Core_Interfaces_IErrorSource
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_OffSeasonPro_Core_Interfaces_IErrorSource_OffSeasonPro_Core_Interfaces_IErrorSource:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 836,2352

.set _p_34_plt_OffSeasonPro_Core_ViewModels_BaseViewModel__ctor_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_BaseViewModel__ctor
_p_35_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore
plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_DataStore:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 844,2369
_p_36_plt_System_Linq_Expressions_Expression_Constant_object_llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Constant_object
plt_System_Linq_Expressions_Expression_Constant_object:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 848,2374
_p_37_plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle_llvm:
	.no_dead_strip plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle
plt_System_Reflection_MethodBase_GetMethodFromHandle_System_RuntimeMethodHandle:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 852,2379
_p_38_plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo_llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo
plt_System_Linq_Expressions_Expression_Property_System_Linq_Expressions_Expression_System_Reflection_MethodInfo:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 856,2382
_p_39_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 860,2387
_p_40_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_1_System_Func_1_OffSeasonPro_Core_Models_Settings_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_1_System_Func_1_OffSeasonPro_Core_Models_Settings
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_OffSeasonPro_Core_Models_Settings_System_Linq_Expressions_Expression_1_System_Func_1_OffSeasonPro_Core_Models_Settings:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 864,2399
_p_41_plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action
plt_Cirrious_MvvmCross_ViewModels_MvxCommand__ctor_System_Action:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 868,2411
_p_42_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand
plt_OffSeasonPro_Core_ViewModels_BaseViewModel_get_CloseCommand:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 872,2416
_p_43_plt_System_Text_StringBuilder__ctor_llvm:
	.no_dead_strip plt_System_Text_StringBuilder__ctor
plt_System_Text_StringBuilder__ctor:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 876,2421
_p_44_plt_System_Text_StringBuilder_AppendLine_string_llvm:
	.no_dead_strip plt_System_Text_StringBuilder_AppendLine_string
plt_System_Text_StringBuilder_AppendLine_string:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 880,2424
_p_45_plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody
plt_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_GetEmailBody:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 884,2427
_p_46_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string
plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ComposeEmail_string_string_string:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 888,2432
_p_47_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IDataStore_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IDataStore
plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IDataStore:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 892,2437
_p_48_plt_Cirrious_MvvmCross_ViewModels_MvxViewModel__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxViewModel__ctor
plt_Cirrious_MvvmCross_ViewModels_MvxViewModel__ctor:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 896,2449
_p_49_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorReporter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorReporter
plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IErrorReporter:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 900,2454
_p_50_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Plugins_Email_IMvxComposeEmailTask:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 904,2466
_p_51_plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IViewModelCloser_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IViewModelCloser
plt_Cirrious_CrossCore_Mvx_Resolve_OffSeasonPro_Core_Interfaces_IViewModelCloser:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 908,2478
_p_52_plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_HomeViewModel_get_Settings:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 912,2490

.set _p_53_plt_OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_SettingsValid_OffSeasonPro_Core_Models_Settings
_p_54_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_WorkoutViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 920,2500
_p_55_plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string
plt_OffSeasonPro_Core_ViewModels_BaseViewModel_ReportError_string:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 924,2512
_p_56_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_SettingsViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 928,2517
_p_57_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 932,2529
_p_58_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_bool_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 936,2534
_p_59_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 940,2546
_p_60_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_get_Settings:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 944,2558
_p_61_plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool
plt_OffSeasonPro_Core_ViewModels_SettingsViewModel_set_IsBusy_bool:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 948,2563

.set _p_62_plt_OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_Extensions_ClearWorkout_OffSeasonPro_Core_Models_Settings
_p_63_plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxNavigatingObject_ShowViewModel_OffSeasonPro_Core_ViewModels_BackgroundChooserViewModel_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 956,2573
_p_64_plt_System_Threading_AutoResetEvent__ctor_bool_llvm:
	.no_dead_strip plt_System_Threading_AutoResetEvent__ctor_bool
plt_System_Threading_AutoResetEvent__ctor_bool:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 960,2585
_p_65_plt_System_Threading_Timer__ctor_System_Threading_TimerCallback_object_int_int_llvm:
	.no_dead_strip plt_System_Threading_Timer__ctor_System_Threading_TimerCallback_object_int_int
plt_System_Threading_Timer__ctor_System_Threading_TimerCallback_object_int_int:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 964,2588
_p_66_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_get_Settings:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 968,2591
_p_67_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_MaxProgressValue_int:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 972,2596
_p_68_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_BtnPausePlayText_string:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 976,2601
_p_69_plt_System_Math_Log10_double_llvm:
	.no_dead_strip plt_System_Math_Log10_double
plt_System_Math_Log10_double:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 980,2606
_p_70_plt_System_Math_Floor_double_llvm:
	.no_dead_strip plt_System_Math_Floor_double
plt_System_Math_Floor_double:
_p_70:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 984,2609
_p_71_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_single_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_single_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_single_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 988,2612
_p_72_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_single_System_Linq_Expressions_Expression_1_System_Func_1_single:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 992,2624
_p_73_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_string_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_string_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_string_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 996,2636
_p_74_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_System_Linq_Expressions_Expression_1_System_Func_1_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_System_Linq_Expressions_Expression_1_System_Func_1_string
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_System_Linq_Expressions_Expression_1_System_Func_1_string:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1000,2648
_p_75_plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_int_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression___llvm:
	.no_dead_strip plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_int_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__
plt_System_Linq_Expressions_Expression_Lambda_System_Func_1_int_System_Linq_Expressions_Expression_System_Linq_Expressions_ParameterExpression__:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1004,2660
_p_76_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_int_System_Linq_Expressions_Expression_1_System_Func_1_int:
_p_76:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1008,2672
_p_77_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ResizeLabel_bool:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1012,2684
_p_78_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_DisplayAd:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1016,2689
_p_79_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ButtonsEnabled_bool:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1020,2694
_p_80_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout:
_p_80:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1024,2699
_p_81_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1028,2704
_p_82_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_PauseWorkout:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1032,2709

.set _p_83_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_StopWorkout
_p_84_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_ClockValue_single:
_p_84:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1040,2719
_p_85_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_set_CurrentPosition_string:
_p_85:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1044,2724
_p_86_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction_llvm:
	.no_dead_strip plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction
plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextAction:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1048,2729

.set _p_87_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextSet

.set _p_88_plt_OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_ViewModels_WorkoutViewModel_NextStation
_p_89_plt_System_Threading_Timer_Change_int_int_llvm:
	.no_dead_strip plt_System_Threading_Timer_Change_int_int
plt_System_Threading_Timer_Change_int_int:
_p_89:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1060,2744

.set _p_90_plt_OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string_llvm, _OffSeasonPro_Core__OffSeasonPro_Core_Messages_DisplayMessage__ctor_object_string
_p_91_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_91:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1068,2752
_p_92_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_bool_object_System_Linq_Expressions_Expression_1_System_Func_1_bool:
_p_92:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1072,2790
_p_93_plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string
plt_Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged_RaisePropertyChanged_string:
_p_93:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1076,2809
_p_94_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_single_object_System_Linq_Expressions_Expression_1_System_Func_1_single:
_p_94:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1080,2814
_p_95_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_GetPropertyNameFromExpression_int_object_System_Linq_Expressions_Expression_1_System_Func_1_int:
_p_95:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1084,2833
_p_96_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_bool_System_Linq_Expressions_Expression_1_System_Func_1_bool:
_p_96:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1088,2852
_p_97_plt_System_Reflection_MethodBase_get_IsStatic_llvm:
	.no_dead_strip plt_System_Reflection_MethodBase_get_IsStatic
plt_System_Reflection_MethodBase_get_IsStatic:
_p_97:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1092,2871
_p_98_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_98:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1096,2874
_p_99_plt__jit_icall_mono_create_corlib_exception_2_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_2
plt__jit_icall_mono_create_corlib_exception_2:
_p_99:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1100,2894
_p_100_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_100:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1104,2927
_p_101_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_101:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1108,2955
_p_102_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_single_System_Linq_Expressions_Expression_1_System_Func_1_single:
_p_102:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1112,2988
_p_103_plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int
plt_Cirrious_CrossCore_Core_MvxPropertyNameExtensionMethods_FindMemberExpression_int_System_Linq_Expressions_Expression_1_System_Func_1_int:
_p_103:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1116,3007
_p_104_plt__jit_icall_mono_thread_get_undeniable_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_get_undeniable_exception
plt__jit_icall_mono_thread_get_undeniable_exception:
_p_104:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1120,3026
_p_105_plt_OffSeasonPro_Core_Trace_Error_string_object___llvm:
	.no_dead_strip plt_OffSeasonPro_Core_Trace_Error_string_object__
plt_OffSeasonPro_Core_Trace_Error_string_object__:
_p_105:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1124,3065
_p_106_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_106:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1128,3070
_p_107_plt_System_Xml_Serialization_XmlSerializer_Deserialize_System_Xml_XmlReader_llvm:
	.no_dead_strip plt_System_Xml_Serialization_XmlSerializer_Deserialize_System_Xml_XmlReader
plt_System_Xml_Serialization_XmlSerializer_Deserialize_System_Xml_XmlReader:
_p_107:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1132,3105
_p_108_plt_System_Xml_Linq_XNode_CreateReader_llvm:
	.no_dead_strip plt_System_Xml_Linq_XNode_CreateReader
plt_System_Xml_Linq_XNode_CreateReader:
_p_108:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1136,3110
_p_109_plt_System_Xml_Linq_XDocument_get_Root_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_get_Root
plt_System_Xml_Linq_XDocument_get_Root:
_p_109:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1140,3115
_p_110_plt_System_Xml_Linq_XDocument_Load_System_IO_Stream_llvm:
	.no_dead_strip plt_System_Xml_Linq_XDocument_Load_System_IO_Stream
plt_System_Xml_Linq_XDocument_Load_System_IO_Stream:
_p_110:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1144,3120
_p_111_plt_Cirrious_CrossCore_Mvx_Trace_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Trace_string_object__
plt_Cirrious_CrossCore_Mvx_Trace_string_object__:
_p_111:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1148,3125
_p_112_plt_System_TimeSpan_FromSeconds_double_llvm:
	.no_dead_strip plt_System_TimeSpan_FromSeconds_double
plt_System_TimeSpan_FromSeconds_double:
_p_112:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1152,3130
_p_113_plt_System_Threading_ManualResetEvent__ctor_bool_llvm:
	.no_dead_strip plt_System_Threading_ManualResetEvent__ctor_bool
plt_System_Threading_ManualResetEvent__ctor_bool:
_p_113:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1156,3133
_p_114_plt_System_Text_StringBuilder_Append_string_llvm:
	.no_dead_strip plt_System_Text_StringBuilder_Append_string
plt_System_Text_StringBuilder_Append_string:
_p_114:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1160,3136
_p_115_plt_int_ToString_llvm:
	.no_dead_strip plt_int_ToString
plt_int_ToString:
_p_115:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_OffSeasonPro_Core_got - . + 1164,3139
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 14
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross"
	.asciz "066A9949-60EF-4499-87FB-90DB7F8EAEA9"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "OffSeasonPro.Core"
	.asciz "7624867D-A45E-4BAD-8689-3188DA78D112"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.File"
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System.Xml"
	.asciz "4D3147BC-C2E3-4CDA-88A7-E0386EE9E899"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "OffSeasonPro.Plugin.TextToSpeech"
	.asciz "CA4BC06B-00DF-4373-B0BE-5C676315A0B6"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.ResourceLoader"
	.asciz "D6FEFB8F-01C7-4230-ACE4-ABE563457664"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.MvvmCross.Plugins.Messenger"
	.asciz "64D7401A-6B92-495B-BA98-86C03A7321B4"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "System"
	.asciz "45EFC0D9-F89B-4E86-BBCA-D23AB67F019D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross.Plugins.Email"
	.asciz "AD05484A-5DFB-485E-A15B-BFD17FF0C58D"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "OffSeasonPro.Plugin.MusicPlayerPlugin"
	.asciz "FF2C7751-BC3D-4DF9-B846-CCDAFB0456ED"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "System.Xml.Linq"
	.asciz "143F1E95-86A8-453C-A5B0-C7F92B936B94"
	.asciz ""
	.asciz "31bf3856ad364e35"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "7624867D-A45E-4BAD-8689-3188DA78D112"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "OffSeasonPro.Core"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_OffSeasonPro_Core_got
	.align 2
	.long _OffSeasonPro_Core__OffSeasonPro_Core_ApplicationObjects_ErrorApplicationObject_add_ErrorReported_System_EventHandler_1_OffSeasonPro_Core_Models_ErrorEventArgs
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 177,1172,116,157,11,387000831,0,4405
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_OffSeasonPro_Core_info
	.align 2
_mono_aot_module_OffSeasonPro_Core_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,2,5,4,0,2,5,4,0,0,0,5,6,10,9,8,7,0,0,0,1,11,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,12,0,0,0,0
	.byte 0,7,13,18,17,16,15,14,19,0,23,20,38,37,36,35,34,33,32,31,30,29,28,24,27,24,26,22,24,25,24,23
	.byte 22,21,0,0,0,14,13,18,47,46,15,45,44,43,42,41,40,39,14,48,0,6,49,50,128,171,128,170,128,169,22,0
	.byte 2,50,49,0,0,0,0,0,1,51,0,7,52,58,57,56,55,54,53,0,3,61,60,59,0,0,0,0,0,1,62,0
	.byte 5,66,65,64,63,67,0,5,7,70,10,69,68,0,5,7,70,10,72,71,0,2,74,73,0,4,75,78,77,76,0,2
	.byte 80,79,0,5,7,70,10,82,81,0,1,83,0,0,0,2,85,84,0,2,87,86,0,2,89,88,0,0,0,1,62,0
	.byte 5,7,70,10,91,90,0,5,7,70,10,93,92,0,2,95,94,0,1,96,0,0,0,1,97,0,1,97,0,1,97,0
	.byte 1,62,0,5,66,65,64,98,67,0,5,7,70,10,100,99,0,5,7,70,10,102,101,0,0,0,5,105,104,64,103,67
	.byte 0,5,7,70,10,107,106,0,0,0,2,74,108,0,11,95,113,10,112,111,7,10,110,109,7,73,0,0,0,1,114,0
	.byte 2,116,115,0,7,117,123,122,121,120,119,118,0,1,62,0,5,7,70,10,125,124,0,5,7,70,10,127,126,0,5,7
	.byte 70,10,128,129,128,128,0,5,7,70,10,128,131,128,130,0,5,7,70,10,128,133,128,132,0,0,0,0,0,5,105,104
	.byte 64,128,134,67,0,0,0,0,0,5,128,137,128,136,64,128,135,67,0,0,0,5,128,140,128,139,64,128,138,67,0,0
	.byte 0,5,128,143,128,142,64,128,141,67,0,0,0,5,128,140,128,139,64,128,144,67,0,0,0,5,105,104,64,128,145,67
	.byte 0,5,7,70,10,128,147,128,146,0,1,74,0,16,75,128,176,33,23,25,22,31,128,150,128,175,128,170,7,128,174,128
	.byte 173,10,128,172,22,0,0,0,5,123,128,148,123,73,123,0,3,128,150,128,149,73,0,0,0,2,128,151,73,0,0,0
	.byte 0,0,0,0,2,128,152,73,0,1,128,153,0,4,128,157,128,156,128,155,128,154,0,1,128,153,0,1,128,158,0,1
	.byte 128,158,0,1,128,158,0,1,128,158,0,1,128,159,0,1,128,160,0,1,128,161,0,13,128,162,128,163,128,163,128,163
	.byte 128,164,128,163,128,163,128,163,128,163,128,163,128,163,128,163,128,163,0,13,128,165,128,163,128,163,128,163,128,164,128,163
	.byte 128,163,128,163,128,163,128,163,128,163,128,163,128,163,0,13,128,166,128,163,128,163,128,163,128,164,128,163,128,163,128,163
	.byte 128,163,128,163,128,163,128,163,128,163,0,6,128,167,128,168,128,167,128,163,128,163,128,168,0,6,128,167,128,168,128,167
	.byte 128,163,128,163,128,168,0,6,128,167,128,168,128,167,128,163,128,163,128,168,255,252,0,0,0,1,1,3,219,0,0,1
	.byte 255,252,0,0,0,1,1,3,219,0,0,2,4,1,119,2,1,130,146,1,130,146,4,1,97,3,1,130,146,1,130,146
	.byte 7,130,236,255,252,0,0,0,1,1,7,130,246,255,252,0,0,0,1,1,3,219,0,0,4,255,254,0,0,0,2,255
	.byte 43,0,0,18,255,254,0,0,0,2,255,43,0,0,21,255,254,0,0,0,2,255,43,0,0,25,255,253,0,0,0,2
	.byte 28,3,3,198,0,0,71,0,1,1,130,25,255,253,0,0,0,2,28,3,3,198,0,0,71,0,1,1,130,144,255,253
	.byte 0,0,0,2,28,3,3,198,0,0,71,0,1,1,130,90,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1
	.byte 130,25,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,144,255,253,0,0,0,2,28,3,3,198,0,0
	.byte 72,0,1,1,130,90,12,2,39,42,47,34,255,254,0,0,0,2,255,43,0,0,1,11,3,219,0,0,1,14,2,3
	.byte 2,14,1,129,251,6,194,0,0,6,50,194,0,0,6,30,1,129,251,14,2,11,2,34,255,254,0,0,0,2,255,43
	.byte 0,0,2,34,255,254,0,0,0,2,255,43,0,0,3,17,2,1,14,3,219,0,0,2,6,194,0,0,56,50,194,0
	.byte 0,56,30,3,219,0,0,2,6,196,0,0,6,14,2,14,2,14,3,219,0,0,3,14,6,1,1,130,123,17,2,27
	.byte 14,1,130,90,17,2,37,17,2,51,17,2,59,17,2,69,17,2,79,17,2,97,17,2,119,17,2,128,153,17,2,128
	.byte 193,17,2,128,215,14,1,129,249,6,194,0,0,59,50,194,0,0,59,30,1,129,249,14,3,219,0,0,4,6,194,0
	.byte 0,55,50,194,0,0,55,30,3,219,0,0,4,6,196,0,0,3,34,255,254,0,0,0,2,255,43,0,0,4,17,2
	.byte 128,245,6,194,0,0,57,50,194,0,0,57,6,195,0,1,60,19,2,194,0,0,12,0,14,2,128,247,5,6,198,0
	.byte 0,4,16,2,3,4,1,16,2,8,7,3,16,2,3,6,1,16,2,11,8,10,14,2,13,2,34,255,254,0,0,0
	.byte 2,255,43,0,0,5,34,255,254,0,0,0,2,255,43,0,0,6,14,2,2,2,34,255,254,0,0,0,2,255,43,0
	.byte 0,7,34,255,254,0,0,0,2,255,43,0,0,8,6,194,0,0,11,18,2,198,0,0,64,0,14,6,1,2,29,9
	.byte 34,255,254,0,0,0,2,255,43,0,0,9,34,255,254,0,0,0,2,255,43,0,0,10,11,1,129,31,6,194,0,0
	.byte 70,50,194,0,0,70,14,2,109,1,6,194,0,0,68,50,194,0,0,68,6,194,0,0,12,6,202,0,9,126,14,1
	.byte 129,210,17,2,129,81,17,2,129,105,17,2,129,109,17,2,130,68,17,2,130,114,6,194,0,0,76,50,194,0,0,76
	.byte 34,255,254,0,0,0,2,255,43,0,0,11,34,255,254,0,0,0,2,255,43,0,0,12,6,194,0,0,15,34,255,254
	.byte 0,0,0,2,255,43,0,0,13,6,203,0,0,1,34,255,254,0,0,0,2,255,43,0,0,14,6,194,0,0,18,6
	.byte 194,0,0,82,50,194,0,0,82,6,194,0,0,81,50,194,0,0,81,34,255,254,0,0,0,2,255,43,0,0,15,17
	.byte 2,130,150,34,255,254,0,0,0,2,255,43,0,0,16,17,2,130,246,18,2,198,0,0,87,0,6,194,0,0,95,50
	.byte 194,0,0,95,6,194,0,0,96,50,194,0,0,96,18,2,198,0,0,91,0,34,255,254,0,0,0,2,255,43,0,0
	.byte 17,34,255,254,0,0,0,2,255,43,0,0,18,6,194,0,0,99,50,194,0,0,99,6,194,0,0,13,6,194,0,0
	.byte 97,50,194,0,0,97,6,194,0,0,98,50,194,0,0,98,6,194,0,0,14,17,2,131,16,17,2,131,96,34,255,254
	.byte 0,0,0,2,255,43,0,0,19,14,1,129,222,14,1,129,248,6,194,0,0,127,50,194,0,0,127,30,1,129,248,14
	.byte 1,129,245,17,2,131,216,6,194,0,0,123,50,194,0,0,123,6,194,0,0,125,50,194,0,0,125,6,194,0,0,135
	.byte 50,194,0,0,135,6,194,0,0,126,50,194,0,0,126,6,194,0,0,122,50,194,0,0,122,18,2,198,0,0,108,0
	.byte 18,2,198,0,0,111,0,34,255,254,0,0,0,2,255,43,0,0,20,34,255,254,0,0,0,2,255,43,0,0,21,18
	.byte 2,198,0,0,113,0,34,255,254,0,0,0,2,255,43,0,0,22,34,255,254,0,0,0,2,255,43,0,0,23,18,2
	.byte 198,0,0,115,0,34,255,254,0,0,0,2,255,43,0,0,24,34,255,254,0,0,0,2,255,43,0,0,25,18,2,198
	.byte 0,0,117,0,18,2,198,0,0,119,0,6,194,0,0,132,50,194,0,0,132,17,2,132,0,16,1,130,146,136,199,17
	.byte 2,131,244,6,204,0,0,3,6,204,0,0,2,6,204,0,0,1,17,2,132,14,14,2,10,2,34,255,254,0,0,0
	.byte 2,255,43,0,0,26,6,255,254,0,0,0,2,255,43,0,0,26,33,34,255,253,0,0,0,2,28,3,3,198,0,0
	.byte 71,0,1,1,130,25,34,255,253,0,0,0,2,28,3,3,198,0,0,71,0,1,1,130,144,34,255,253,0,0,0,2
	.byte 28,3,3,198,0,0,71,0,1,1,130,90,34,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,25,12
	.byte 3,11,1,129,57,34,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,144,34,255,253,0,0,0,2,28
	.byte 3,3,198,0,0,72,0,1,1,130,90,11,2,22,9,11,2,31,9,17,2,129,23,6,145,58,11,2,12,2,6,198
	.byte 0,0,3,50,194,0,0,124,6,194,0,0,124,14,1,129,229,17,2,131,228,7,42,108,108,118,109,95,116,104,114,111
	.byte 119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110
	.byte 101,0,3,144,63,3,255,254,0,0,0,2,255,43,0,0,1,3,144,65,7,20,109,111,110,111,95,111,98,106,101,99
	.byte 116,95,110,101,119,95,102,97,115,116,0,3,195,0,0,27,3,142,191,3,194,0,0,7,3,200,0,0,3,3,194,0
	.byte 0,54,3,255,254,0,0,0,2,255,43,0,0,2,3,255,254,0,0,0,2,255,43,0,0,3,3,255,254,0,0,0
	.byte 2,202,0,0,17,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,7
	.byte 27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,147,136
	.byte 3,255,254,0,0,0,2,202,0,0,19,3,140,162,3,255,254,0,0,0,2,255,43,0,0,4,15,2,128,247,5,3
	.byte 197,0,8,80,3,197,0,8,91,3,194,0,0,55,3,196,0,0,16,3,199,0,0,17,3,198,0,0,6,3,200,0
	.byte 0,55,3,194,0,0,48,3,255,254,0,0,0,2,255,43,0,0,5,3,194,0,0,61,3,255,254,0,0,0,2,255
	.byte 43,0,0,6,3,255,254,0,0,0,2,255,43,0,0,7,3,255,254,0,0,0,2,255,43,0,0,8,3,194,0,0
	.byte 73,3,194,0,0,72,3,201,0,0,51,3,136,193,3,201,0,0,69,3,255,254,0,0,0,2,255,43,0,0,9,3
	.byte 255,254,0,0,0,2,255,43,0,0,10,3,193,0,1,169,3,194,0,0,71,3,139,190,3,139,213,3,194,0,0,69
	.byte 3,194,0,0,75,3,255,254,0,0,0,2,255,43,0,0,11,3,193,0,1,158,3,255,254,0,0,0,2,255,43,0
	.byte 0,12,3,255,254,0,0,0,2,255,43,0,0,13,3,255,254,0,0,0,2,255,43,0,0,14,3,194,0,0,78,3
	.byte 194,0,0,10,3,255,254,0,0,0,2,255,43,0,0,15,3,194,0,0,74,3,255,254,0,0,0,2,255,43,0,0
	.byte 16,3,195,0,1,113,3,255,254,0,0,0,2,255,43,0,0,17,3,255,254,0,0,0,2,255,43,0,0,18,3,194
	.byte 0,0,87,3,194,0,0,92,3,194,0,0,8,3,255,254,0,0,0,2,255,43,0,0,19,3,140,70,3,140,173,3
	.byte 194,0,0,101,3,194,0,0,116,3,194,0,0,114,3,145,210,3,145,199,3,255,254,0,0,0,2,255,43,0,0,20
	.byte 3,255,254,0,0,0,2,255,43,0,0,21,3,255,254,0,0,0,2,255,43,0,0,22,3,255,254,0,0,0,2,255
	.byte 43,0,0,23,3,255,254,0,0,0,2,255,43,0,0,24,3,255,254,0,0,0,2,255,43,0,0,25,3,194,0,0
	.byte 109,3,194,0,0,134,3,194,0,0,120,3,194,0,0,133,3,194,0,0,123,3,194,0,0,132,3,194,0,0,131,3
	.byte 194,0,0,112,3,194,0,0,118,3,194,0,0,128,3,194,0,0,129,3,194,0,0,130,3,140,176,3,194,0,0,19
	.byte 7,35,109,111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107
	.byte 112,111,105,110,116,0,3,255,253,0,0,0,2,28,3,3,198,0,0,71,0,1,1,130,25,3,193,0,1,141,3,255
	.byte 253,0,0,0,2,28,3,3,198,0,0,71,0,1,1,130,144,3,255,253,0,0,0,2,28,3,3,198,0,0,71,0
	.byte 1,1,130,90,3,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,25,3,136,184,7,17,109,111,110,111
	.byte 95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108
	.byte 105,98,95,101,120,99,101,112,116,105,111,110,95,50,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119
	.byte 95,101,120,99,101,112,116,105,111,110,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95
	.byte 101,120,99,101,112,116,105,111,110,95,49,0,3,255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,144,3
	.byte 255,253,0,0,0,2,28,3,3,198,0,0,72,0,1,1,130,90,7,36,109,111,110,111,95,116,104,114,101,97,100,95
	.byte 103,101,116,95,117,110,100,101,110,105,97,98,108,101,95,101,120,99,101,112,116,105,111,110,0,3,194,0,0,86,7,32
	.byte 109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110
	.byte 0,3,197,0,8,88,3,205,0,0,146,3,205,0,0,51,3,205,0,0,52,3,195,0,1,12,3,147,248,3,140,109
	.byte 3,139,205,3,145,114,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,0,1,0,4,3,1,130
	.byte 64,32,48,48,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,24,2,2,0,129
	.byte 84,128,128,129,16,129,20,0,16,3,1,130,64,52,129,84,129,84,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,50,3,0
	.byte 24,3,1,130,64,108,129,228,129,228,2,0,131,76,130,188,131,8,131,12,0,28,3,1,130,64,131,76,132,24,132,24
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,74,1,0
	.byte 4,3,1,130,64,32,120,120,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,7,128,168
	.byte 12,0,0,4,146,178,146,175,146,174,146,172,194,0,0,4,194,0,0,1,194,0,0,2,4,128,160,16,0,0,4,146
	.byte 178,146,175,146,174,146,172,4,128,192,8,0,0,1,146,178,146,175,146,174,146,172,4,128,144,8,0,0,1,146,178,146
	.byte 175,146,174,146,172,0,128,144,8,0,0,1,0,128,144,8,0,0,1,0,128,144,8,0,0,1,0,128,144,8,0,0
	.byte 1,4,128,160,16,0,0,4,146,178,146,175,146,174,146,172,4,128,160,12,0,0,4,146,178,146,175,146,174,146,172,4
	.byte 128,160,44,0,0,4,146,178,146,175,146,174,146,172,8,128,232,20,0,0,4,146,178,146,175,146,174,146,172,194,0,0
	.byte 50,194,0,0,51,194,0,0,53,194,0,0,52,4,128,160,24,0,0,4,146,178,146,175,146,174,146,172,10,128,160,12
	.byte 0,0,4,146,178,146,175,146,174,146,172,193,0,0,185,193,0,0,186,193,0,0,187,193,0,0,186,193,0,0,185,193
	.byte 0,0,184,4,128,128,28,0,0,4,146,178,146,175,146,174,146,172,24,128,160,24,0,0,4,146,178,146,175,146,174,146
	.byte 172,193,0,1,137,193,0,1,138,255,251,0,0,0,193,0,1,140,193,0,1,141,193,0,1,143,193,0,1,135,193,0
	.byte 1,136,193,0,1,144,193,0,1,143,193,0,1,142,193,0,1,159,193,0,1,160,193,0,1,161,193,0,1,162,193,0
	.byte 1,163,193,0,1,164,193,0,1,167,193,0,1,166,193,0,1,165,193,0,1,163,24,128,160,20,0,0,4,146,178,146
	.byte 175,146,174,146,172,193,0,1,137,193,0,1,138,255,251,0,0,0,193,0,1,140,193,0,1,141,193,0,1,143,193,0
	.byte 1,135,193,0,1,136,193,0,1,144,193,0,1,143,193,0,1,142,193,0,1,159,193,0,1,160,193,0,1,161,193,0
	.byte 1,162,193,0,1,163,193,0,1,164,193,0,1,167,193,0,1,166,193,0,1,165,193,0,1,163,24,128,160,20,0,0
	.byte 4,146,178,146,175,146,174,146,172,193,0,1,137,193,0,1,138,255,251,0,0,0,193,0,1,140,193,0,1,141,193,0
	.byte 1,143,193,0,1,135,193,0,1,136,193,0,1,144,193,0,1,143,193,0,1,142,193,0,1,159,193,0,1,160,193,0
	.byte 1,161,193,0,1,162,193,0,1,163,193,0,1,164,193,0,1,167,193,0,1,166,193,0,1,165,193,0,1,163,4,128
	.byte 192,8,0,0,1,146,178,146,175,146,174,146,172,24,128,160,28,0,0,4,146,178,146,175,146,174,146,172,193,0,1,137
	.byte 193,0,1,138,255,251,0,0,0,193,0,1,140,193,0,1,141,193,0,1,143,193,0,1,135,193,0,1,136,193,0,1
	.byte 144,193,0,1,143,193,0,1,142,193,0,1,159,193,0,1,160,193,0,1,161,193,0,1,162,193,0,1,163,193,0,1
	.byte 164,193,0,1,167,193,0,1,166,193,0,1,165,193,0,1,163,24,128,160,64,0,0,4,146,178,146,175,146,174,146,172
	.byte 193,0,1,137,193,0,1,138,255,251,0,0,0,193,0,1,140,193,0,1,141,193,0,1,143,193,0,1,135,193,0,1
	.byte 136,193,0,1,144,193,0,1,143,193,0,1,142,193,0,1,159,193,0,1,160,193,0,1,161,193,0,1,162,193,0,1
	.byte 163,193,0,1,164,193,0,1,167,193,0,1,166,193,0,1,165,193,0,1,163,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_2:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_1:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM6=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM6
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM7=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_0:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM10=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM11=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM12=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM13=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM14=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM14
	.byte 2
	.asciz "OffSeasonPro.Core.Extensions:ToInt32"
	.long _OffSeasonPro_Core_Extensions_ToInt32_string_0
	.long Lme_6

	.byte 2,118,16,3
	.asciz "str"

LDIFF_SYM15=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM15
	.byte 2,123,24,11
	.asciz ""

LDIFF_SYM16=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM17=Lfde0_end - Lfde0_start
	.long LDIFF_SYM17
Lfde0_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Core_Extensions_ToInt32_string_0

LDIFF_SYM18=Lme_6 - _OffSeasonPro_Core_Extensions_ToInt32_string_0
	.long LDIFF_SYM18
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_4:

	.byte 17
	.asciz "OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech"

	.byte 8,7
	.asciz "OffSeasonPro_Plugin_TextToSpeech_ITextToSpeech"

LDIFF_SYM19=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM20=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM20
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM21=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM21
LTDIE_6:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM22=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM22
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM23=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM23
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM24=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM25=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM26=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_7:

	.byte 5
	.asciz "OffSeasonPro_Core_Models_Workout"

	.byte 28,16
LDIFF_SYM27=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,0,6
	.asciz "CurrentPartner"

LDIFF_SYM28=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,8,6
	.asciz "CurrentSet"

LDIFF_SYM29=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,35,12,6
	.asciz "CurrentStation"

LDIFF_SYM30=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 2,35,16,6
	.asciz "SecondsElapsed"

LDIFF_SYM31=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 2,35,20,6
	.asciz "Transitioning"

LDIFF_SYM32=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,24,6
	.asciz "WarmingUp"

LDIFF_SYM33=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,25,0,7
	.asciz "OffSeasonPro_Core_Models_Workout"

LDIFF_SYM34=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM34
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM35=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM35
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM36=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_5:

	.byte 5
	.asciz "OffSeasonPro_Core_Models_Settings"

	.byte 44,16
LDIFF_SYM37=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM37
	.byte 2,35,0,6
	.asciz "<StationCount>k__BackingField"

LDIFF_SYM38=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM38
	.byte 2,35,8,6
	.asciz "<PartnerCount>k__BackingField"

LDIFF_SYM39=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2,35,12,6
	.asciz "<SetCount>k__BackingField"

LDIFF_SYM40=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,35,16,6
	.asciz "<SetLengthSeconds>k__BackingField"

LDIFF_SYM41=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM41
	.byte 2,35,20,6
	.asciz "<TransitionLengthSeconds>k__BackingField"

LDIFF_SYM42=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,24,6
	.asciz "<SkipAfterLift>k__BackingField"

LDIFF_SYM43=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2,35,40,6
	.asciz "<StartupLength>k__BackingField"

LDIFF_SYM44=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,28,6
	.asciz "<WorkoutBackgroundImage>k__BackingField"

LDIFF_SYM45=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,32,6
	.asciz "<UsePhotoAlbumForBgImage>k__BackingField"

LDIFF_SYM46=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM46
	.byte 2,35,41,6
	.asciz "<IsPaid>k__BackingField"

LDIFF_SYM47=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM47
	.byte 2,35,42,6
	.asciz "<CurrentWorkout>k__BackingField"

LDIFF_SYM48=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM48
	.byte 2,35,36,0,7
	.asciz "OffSeasonPro_Core_Models_Settings"

LDIFF_SYM49=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM49
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM50=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM51=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_3:

	.byte 5
	.asciz "OffSeasonPro_Core_Models_SimpleDataStore"

	.byte 20,16
LDIFF_SYM52=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM52
	.byte 2,35,0,6
	.asciz "_playlist"

LDIFF_SYM53=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,8,6
	.asciz "_tts"

LDIFF_SYM54=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2,35,12,6
	.asciz "_settings"

LDIFF_SYM55=LTDIE_5_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM55
	.byte 2,35,16,0,7
	.asciz "OffSeasonPro_Core_Models_SimpleDataStore"

LDIFF_SYM56=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM57=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM58=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_14:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM59=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM59
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM60=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM61=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM61
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM62=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_13:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM63=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM63
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM64=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM65=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM66=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM66
LTDIE_12:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM67=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM68=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM69=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM70=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_16:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM71=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM72=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM73=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM73
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM74=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM74
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM75=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM75
LTDIE_15:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM76=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM77=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM78=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM79=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM79
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM80=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM80
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM81=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_11:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM82=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM82
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM83=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM83
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM84=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM84
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM85=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM86=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM87=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM88=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM88
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM89=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM89
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM90=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM90
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM91=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM92=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM93=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM94=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_10:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM95=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM96=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM97=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM98=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM99=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM100=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_9:

	.byte 5
	.asciz "System_Func`4"

	.byte 52,16
LDIFF_SYM101=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM101
	.byte 2,35,0,0,7
	.asciz "System_Func`4"

LDIFF_SYM102=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM102
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM103=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM103
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM104=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM104
LTDIE_17:

	.byte 5
	.asciz "System_Action`3"

	.byte 52,16
LDIFF_SYM105=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,0,0,7
	.asciz "System_Action`3"

LDIFF_SYM106=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM106
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM107=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM107
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM108=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_21:

	.byte 5
	.asciz "System_MarshalByRefObject"

	.byte 12,16
LDIFF_SYM109=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 2,35,0,6
	.asciz "_identity"

LDIFF_SYM110=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM110
	.byte 2,35,8,0,7
	.asciz "System_MarshalByRefObject"

LDIFF_SYM111=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM111
LTDIE_21_POINTER:

	.byte 13
LDIFF_SYM112=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_21_REFERENCE:

	.byte 14
LDIFF_SYM113=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_25:

	.byte 5
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

	.byte 8,16
LDIFF_SYM114=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM114
	.byte 2,35,0,0,7
	.asciz "System_Runtime_ConstrainedExecution_CriticalFinalizerObject"

LDIFF_SYM115=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM115
LTDIE_25_POINTER:

	.byte 13
LDIFF_SYM116=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM116
LTDIE_25_REFERENCE:

	.byte 14
LDIFF_SYM117=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM117
LTDIE_24:

	.byte 5
	.asciz "System_Runtime_InteropServices_SafeHandle"

	.byte 24,16
LDIFF_SYM118=LTDIE_25 - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,35,0,6
	.asciz "handle"

LDIFF_SYM119=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 2,35,8,6
	.asciz "invalid_handle_value"

LDIFF_SYM120=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 2,35,12,6
	.asciz "refcount"

LDIFF_SYM121=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 2,35,16,6
	.asciz "owns_handle"

LDIFF_SYM122=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 2,35,20,0,7
	.asciz "System_Runtime_InteropServices_SafeHandle"

LDIFF_SYM123=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM123
LTDIE_24_POINTER:

	.byte 13
LDIFF_SYM124=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM124
LTDIE_24_REFERENCE:

	.byte 14
LDIFF_SYM125=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM125
LTDIE_23:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

	.byte 24,16
LDIFF_SYM126=LTDIE_24 - Ldebug_info_start
	.long LDIFF_SYM126
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeHandleZeroOrMinusOneIsInvalid"

LDIFF_SYM127=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM127
LTDIE_23_POINTER:

	.byte 13
LDIFF_SYM128=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM128
LTDIE_23_REFERENCE:

	.byte 14
LDIFF_SYM129=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM129
LTDIE_22:

	.byte 5
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

	.byte 24,16
LDIFF_SYM130=LTDIE_23 - Ldebug_info_start
	.long LDIFF_SYM130
	.byte 2,35,0,0,7
	.asciz "Microsoft_Win32_SafeHandles_SafeWaitHandle"

LDIFF_SYM131=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM131
LTDIE_22_POINTER:

	.byte 13
LDIFF_SYM132=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM132
LTDIE_22_REFERENCE:

	.byte 14
LDIFF_SYM133=LTDIE_22 - Ldebug_info_start
	.long LDIFF_SYM133
LTDIE_20:

	.byte 5
	.asciz "System_Threading_WaitHandle"

	.byte 20,16
LDIFF_SYM134=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM134
	.byte 2,35,0,6
	.asciz "safe_wait_handle"

LDIFF_SYM135=LTDIE_22_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM135
	.byte 2,35,12,6
	.asciz "disposed"

LDIFF_SYM136=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM136
	.byte 2,35,16,0,7
	.asciz "System_Threading_WaitHandle"

LDIFF_SYM137=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM137
LTDIE_20_POINTER:

	.byte 13
LDIFF_SYM138=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM138
LTDIE_20_REFERENCE:

	.byte 14
LDIFF_SYM139=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM139
LTDIE_19:

	.byte 5
	.asciz "System_Threading_EventWaitHandle"

	.byte 20,16
LDIFF_SYM140=LTDIE_20 - Ldebug_info_start
	.long LDIFF_SYM140
	.byte 2,35,0,0,7
	.asciz "System_Threading_EventWaitHandle"

LDIFF_SYM141=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM141
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM142=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM142
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM143=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM143
LTDIE_18:

	.byte 5
	.asciz "System_Threading_AutoResetEvent"

	.byte 20,16
LDIFF_SYM144=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM144
	.byte 2,35,0,0,7
	.asciz "System_Threading_AutoResetEvent"

LDIFF_SYM145=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM145
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM146=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM146
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM147=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM147
LTDIE_8:

	.byte 5
	.asciz "System_IO_Stream"

	.byte 20,16
LDIFF_SYM148=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM148
	.byte 2,35,0,6
	.asciz "async_read"

LDIFF_SYM149=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM149
	.byte 2,35,8,6
	.asciz "async_write"

LDIFF_SYM150=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM150
	.byte 2,35,12,6
	.asciz "async_event"

LDIFF_SYM151=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM151
	.byte 2,35,16,0,7
	.asciz "System_IO_Stream"

LDIFF_SYM152=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM152
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM153=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM153
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM154=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM154
LTDIE_30:

	.byte 5
	.asciz "System_EventHandler`1"

	.byte 52,16
LDIFF_SYM155=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM155
	.byte 2,35,0,0,7
	.asciz "System_EventHandler`1"

LDIFF_SYM156=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM156
LTDIE_30_POINTER:

	.byte 13
LDIFF_SYM157=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM157
LTDIE_30_REFERENCE:

	.byte 14
LDIFF_SYM158=LTDIE_30 - Ldebug_info_start
	.long LDIFF_SYM158
LTDIE_29:

	.byte 5
	.asciz "System_Xml_Linq_XObject"

	.byte 32,16
LDIFF_SYM159=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM159
	.byte 2,35,0,6
	.asciz "owner"

LDIFF_SYM160=LTDIE_27_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM160
	.byte 2,35,8,6
	.asciz "baseuri"

LDIFF_SYM161=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM161
	.byte 2,35,12,6
	.asciz "line"

LDIFF_SYM162=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM162
	.byte 2,35,24,6
	.asciz "column"

LDIFF_SYM163=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM163
	.byte 2,35,28,6
	.asciz "Changing"

LDIFF_SYM164=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM164
	.byte 2,35,16,6
	.asciz "Changed"

LDIFF_SYM165=LTDIE_30_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM165
	.byte 2,35,20,0,7
	.asciz "System_Xml_Linq_XObject"

LDIFF_SYM166=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM166
LTDIE_29_POINTER:

	.byte 13
LDIFF_SYM167=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM167
LTDIE_29_REFERENCE:

	.byte 14
LDIFF_SYM168=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM168
LTDIE_28:

	.byte 5
	.asciz "System_Xml_Linq_XNode"

	.byte 40,16
LDIFF_SYM169=LTDIE_29 - Ldebug_info_start
	.long LDIFF_SYM169
	.byte 2,35,0,6
	.asciz "previous"

LDIFF_SYM170=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM170
	.byte 2,35,32,6
	.asciz "next"

LDIFF_SYM171=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM171
	.byte 2,35,36,0,7
	.asciz "System_Xml_Linq_XNode"

LDIFF_SYM172=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM172
LTDIE_28_POINTER:

	.byte 13
LDIFF_SYM173=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM173
LTDIE_28_REFERENCE:

	.byte 14
LDIFF_SYM174=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM174
LTDIE_27:

	.byte 5
	.asciz "System_Xml_Linq_XContainer"

	.byte 48,16
LDIFF_SYM175=LTDIE_28 - Ldebug_info_start
	.long LDIFF_SYM175
	.byte 2,35,0,6
	.asciz "first"

LDIFF_SYM176=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM176
	.byte 2,35,40,6
	.asciz "last"

LDIFF_SYM177=LTDIE_28_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM177
	.byte 2,35,44,0,7
	.asciz "System_Xml_Linq_XContainer"

LDIFF_SYM178=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM178
LTDIE_27_POINTER:

	.byte 13
LDIFF_SYM179=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM179
LTDIE_27_REFERENCE:

	.byte 14
LDIFF_SYM180=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM180
LTDIE_31:

	.byte 5
	.asciz "System_Xml_Linq_XDeclaration"

	.byte 20,16
LDIFF_SYM181=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM181
	.byte 2,35,0,6
	.asciz "encoding"

LDIFF_SYM182=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM182
	.byte 2,35,8,6
	.asciz "standalone"

LDIFF_SYM183=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM183
	.byte 2,35,12,6
	.asciz "version"

LDIFF_SYM184=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM184
	.byte 2,35,16,0,7
	.asciz "System_Xml_Linq_XDeclaration"

LDIFF_SYM185=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM185
LTDIE_31_POINTER:

	.byte 13
LDIFF_SYM186=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM186
LTDIE_31_REFERENCE:

	.byte 14
LDIFF_SYM187=LTDIE_31 - Ldebug_info_start
	.long LDIFF_SYM187
LTDIE_26:

	.byte 5
	.asciz "System_Xml_Linq_XDocument"

	.byte 52,16
LDIFF_SYM188=LTDIE_27 - Ldebug_info_start
	.long LDIFF_SYM188
	.byte 2,35,0,6
	.asciz "xmldecl"

LDIFF_SYM189=LTDIE_31_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM189
	.byte 2,35,48,0,7
	.asciz "System_Xml_Linq_XDocument"

LDIFF_SYM190=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM190
LTDIE_26_POINTER:

	.byte 13
LDIFF_SYM191=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM191
LTDIE_26_REFERENCE:

	.byte 14
LDIFF_SYM192=LTDIE_26 - Ldebug_info_start
	.long LDIFF_SYM192
LTDIE_33:

	.byte 5
	.asciz "System_Text_StringBuilder"

	.byte 24,16
LDIFF_SYM193=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM193
	.byte 2,35,0,6
	.asciz "_length"

LDIFF_SYM194=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM194
	.byte 2,35,8,6
	.asciz "_str"

LDIFF_SYM195=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM195
	.byte 2,35,12,6
	.asciz "_cached_str"

LDIFF_SYM196=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM196
	.byte 2,35,16,6
	.asciz "_maxCapacity"

LDIFF_SYM197=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM197
	.byte 2,35,20,0,7
	.asciz "System_Text_StringBuilder"

LDIFF_SYM198=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM198
LTDIE_33_POINTER:

	.byte 13
LDIFF_SYM199=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM199
LTDIE_33_REFERENCE:

	.byte 14
LDIFF_SYM200=LTDIE_33 - Ldebug_info_start
	.long LDIFF_SYM200
LTDIE_35:

	.byte 8
	.asciz "_CommandState"

	.byte 4
LDIFF_SYM201=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM201
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ReadElementContentAsBase64"

	.byte 1,9
	.asciz "ReadContentAsBase64"

	.byte 2,9
	.asciz "ReadElementContentAsBinHex"

	.byte 3,9
	.asciz "ReadContentAsBinHex"

	.byte 4,0,7
	.asciz "_CommandState"

LDIFF_SYM202=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM202
LTDIE_35_POINTER:

	.byte 13
LDIFF_SYM203=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM203
LTDIE_35_REFERENCE:

	.byte 14
LDIFF_SYM204=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM204
LTDIE_34:

	.byte 5
	.asciz "System_Xml_XmlReaderBinarySupport"

	.byte 24,16
LDIFF_SYM205=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM205
	.byte 2,35,0,6
	.asciz "reader"

LDIFF_SYM206=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM206
	.byte 2,35,8,6
	.asciz "base64CacheStartsAt"

LDIFF_SYM207=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM207
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM208=LTDIE_35 - Ldebug_info_start
	.long LDIFF_SYM208
	.byte 2,35,16,6
	.asciz "hasCache"

LDIFF_SYM209=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM209
	.byte 2,35,20,6
	.asciz "dontReset"

LDIFF_SYM210=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM210
	.byte 2,35,21,0,7
	.asciz "System_Xml_XmlReaderBinarySupport"

LDIFF_SYM211=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM211
LTDIE_34_POINTER:

	.byte 13
LDIFF_SYM212=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM212
LTDIE_34_REFERENCE:

	.byte 14
LDIFF_SYM213=LTDIE_34 - Ldebug_info_start
	.long LDIFF_SYM213
LTDIE_37:

	.byte 8
	.asciz "System_Xml_ConformanceLevel"

	.byte 4
LDIFF_SYM214=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM214
	.byte 9
	.asciz "Auto"

	.byte 0,9
	.asciz "Fragment"

	.byte 1,9
	.asciz "Document"

	.byte 2,0,7
	.asciz "System_Xml_ConformanceLevel"

LDIFF_SYM215=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM215
LTDIE_37_POINTER:

	.byte 13
LDIFF_SYM216=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM216
LTDIE_37_REFERENCE:

	.byte 14
LDIFF_SYM217=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM217
LTDIE_38:

	.byte 5
	.asciz "System_Xml_XmlNameTable"

	.byte 8,16
LDIFF_SYM218=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM218
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlNameTable"

LDIFF_SYM219=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM219
LTDIE_38_POINTER:

	.byte 13
LDIFF_SYM220=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM220
LTDIE_38_REFERENCE:

	.byte 14
LDIFF_SYM221=LTDIE_38 - Ldebug_info_start
	.long LDIFF_SYM221
LTDIE_40:

	.byte 5
	.asciz "System_Xml_XmlResolver"

	.byte 8,16
LDIFF_SYM222=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM222
	.byte 2,35,0,0,7
	.asciz "System_Xml_XmlResolver"

LDIFF_SYM223=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM223
LTDIE_40_POINTER:

	.byte 13
LDIFF_SYM224=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM224
LTDIE_40_REFERENCE:

	.byte 14
LDIFF_SYM225=LTDIE_40 - Ldebug_info_start
	.long LDIFF_SYM225
LTDIE_41:

	.byte 5
	.asciz "System_Collections_ArrayList"

	.byte 20,16
LDIFF_SYM226=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM226
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM227=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM227
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM228=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM228
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM229=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM229
	.byte 2,35,16,0,7
	.asciz "System_Collections_ArrayList"

LDIFF_SYM230=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM230
LTDIE_41_POINTER:

	.byte 13
LDIFF_SYM231=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM231
LTDIE_41_REFERENCE:

	.byte 14
LDIFF_SYM232=LTDIE_41 - Ldebug_info_start
	.long LDIFF_SYM232
LTDIE_44:

	.byte 5
	.asciz "_DictionaryNode"

	.byte 20,16
LDIFF_SYM233=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM233
	.byte 2,35,0,6
	.asciz "key"

LDIFF_SYM234=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM234
	.byte 2,35,8,6
	.asciz "value"

LDIFF_SYM235=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM235
	.byte 2,35,12,6
	.asciz "next"

LDIFF_SYM236=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM236
	.byte 2,35,16,0,7
	.asciz "_DictionaryNode"

LDIFF_SYM237=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM237
LTDIE_44_POINTER:

	.byte 13
LDIFF_SYM238=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM238
LTDIE_44_REFERENCE:

	.byte 14
LDIFF_SYM239=LTDIE_44 - Ldebug_info_start
	.long LDIFF_SYM239
LTDIE_45:

	.byte 17
	.asciz "System_Collections_IComparer"

	.byte 8,7
	.asciz "System_Collections_IComparer"

LDIFF_SYM240=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM240
LTDIE_45_POINTER:

	.byte 13
LDIFF_SYM241=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM241
LTDIE_45_REFERENCE:

	.byte 14
LDIFF_SYM242=LTDIE_45 - Ldebug_info_start
	.long LDIFF_SYM242
LTDIE_43:

	.byte 5
	.asciz "System_Collections_Specialized_ListDictionary"

	.byte 24,16
LDIFF_SYM243=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM243
	.byte 2,35,0,6
	.asciz "count"

LDIFF_SYM244=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM244
	.byte 2,35,16,6
	.asciz "version"

LDIFF_SYM245=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM245
	.byte 2,35,20,6
	.asciz "head"

LDIFF_SYM246=LTDIE_44_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM246
	.byte 2,35,8,6
	.asciz "comparer"

LDIFF_SYM247=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM247
	.byte 2,35,12,0,7
	.asciz "System_Collections_Specialized_ListDictionary"

LDIFF_SYM248=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM248
LTDIE_43_POINTER:

	.byte 13
LDIFF_SYM249=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM249
LTDIE_43_REFERENCE:

	.byte 14
LDIFF_SYM250=LTDIE_43 - Ldebug_info_start
	.long LDIFF_SYM250
LTDIE_42:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

	.byte 12,16
LDIFF_SYM251=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM251
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM252=LTDIE_43_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM252
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaObjectTable"

LDIFF_SYM253=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM253
LTDIE_42_POINTER:

	.byte 13
LDIFF_SYM254=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM254
LTDIE_42_REFERENCE:

	.byte 14
LDIFF_SYM255=LTDIE_42 - Ldebug_info_start
	.long LDIFF_SYM255
LTDIE_46:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

	.byte 9,16
LDIFF_SYM256=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM256
	.byte 2,35,0,6
	.asciz "enable_upa_check"

LDIFF_SYM257=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM257
	.byte 2,35,8,0,7
	.asciz "System_Xml_Schema_XmlSchemaCompilationSettings"

LDIFF_SYM258=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM258
LTDIE_46_POINTER:

	.byte 13
LDIFF_SYM259=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM259
LTDIE_46_REFERENCE:

	.byte 14
LDIFF_SYM260=LTDIE_46 - Ldebug_info_start
	.long LDIFF_SYM260
LTDIE_47:

	.byte 5
	.asciz "System_Xml_Schema_ValidationEventHandler"

	.byte 52,16
LDIFF_SYM261=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM261
	.byte 2,35,0,0,7
	.asciz "System_Xml_Schema_ValidationEventHandler"

LDIFF_SYM262=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM262
LTDIE_47_POINTER:

	.byte 13
LDIFF_SYM263=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM263
LTDIE_47_REFERENCE:

	.byte 14
LDIFF_SYM264=LTDIE_47 - Ldebug_info_start
	.long LDIFF_SYM264
LTDIE_49:

	.byte 5
	.asciz "_HashKeys"

	.byte 12,16
LDIFF_SYM265=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM265
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM266=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM266
	.byte 2,35,8,0,7
	.asciz "_HashKeys"

LDIFF_SYM267=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM267
LTDIE_49_POINTER:

	.byte 13
LDIFF_SYM268=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM268
LTDIE_49_REFERENCE:

	.byte 14
LDIFF_SYM269=LTDIE_49 - Ldebug_info_start
	.long LDIFF_SYM269
LTDIE_50:

	.byte 5
	.asciz "_HashValues"

	.byte 12,16
LDIFF_SYM270=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM270
	.byte 2,35,0,6
	.asciz "host"

LDIFF_SYM271=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM271
	.byte 2,35,8,0,7
	.asciz "_HashValues"

LDIFF_SYM272=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM272
LTDIE_50_POINTER:

	.byte 13
LDIFF_SYM273=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM273
LTDIE_50_REFERENCE:

	.byte 14
LDIFF_SYM274=LTDIE_50 - Ldebug_info_start
	.long LDIFF_SYM274
LTDIE_51:

	.byte 17
	.asciz "System_Collections_IHashCodeProvider"

	.byte 8,7
	.asciz "System_Collections_IHashCodeProvider"

LDIFF_SYM275=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM275
LTDIE_51_POINTER:

	.byte 13
LDIFF_SYM276=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM276
LTDIE_51_REFERENCE:

	.byte 14
LDIFF_SYM277=LTDIE_51 - Ldebug_info_start
	.long LDIFF_SYM277
LTDIE_52:

	.byte 17
	.asciz "System_Collections_IEqualityComparer"

	.byte 8,7
	.asciz "System_Collections_IEqualityComparer"

LDIFF_SYM278=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM278
LTDIE_52_POINTER:

	.byte 13
LDIFF_SYM279=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM279
LTDIE_52_REFERENCE:

	.byte 14
LDIFF_SYM280=LTDIE_52 - Ldebug_info_start
	.long LDIFF_SYM280
LTDIE_53:

	.byte 5
	.asciz "System_Single"

	.byte 12,16
LDIFF_SYM281=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM281
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM282=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM282
	.byte 2,35,8,0,7
	.asciz "System_Single"

LDIFF_SYM283=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM283
LTDIE_53_POINTER:

	.byte 13
LDIFF_SYM284=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM284
LTDIE_53_REFERENCE:

	.byte 14
LDIFF_SYM285=LTDIE_53 - Ldebug_info_start
	.long LDIFF_SYM285
LTDIE_48:

	.byte 5
	.asciz "System_Collections_Hashtable"

	.byte 52,16
LDIFF_SYM286=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM286
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM287=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM287
	.byte 2,35,8,6
	.asciz "hashes"

LDIFF_SYM288=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM288
	.byte 2,35,12,6
	.asciz "hashKeys"

LDIFF_SYM289=LTDIE_49_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM289
	.byte 2,35,16,6
	.asciz "hashValues"

LDIFF_SYM290=LTDIE_50_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM290
	.byte 2,35,20,6
	.asciz "hcpRef"

LDIFF_SYM291=LTDIE_51_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM291
	.byte 2,35,24,6
	.asciz "comparerRef"

LDIFF_SYM292=LTDIE_45_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM292
	.byte 2,35,28,6
	.asciz "equalityComparer"

LDIFF_SYM293=LTDIE_52_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM293
	.byte 2,35,32,6
	.asciz "inUse"

LDIFF_SYM294=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM294
	.byte 2,35,36,6
	.asciz "modificationCount"

LDIFF_SYM295=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM295
	.byte 2,35,40,6
	.asciz "loadFactor"

LDIFF_SYM296=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM296
	.byte 2,35,44,6
	.asciz "threshold"

LDIFF_SYM297=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM297
	.byte 2,35,48,0,7
	.asciz "System_Collections_Hashtable"

LDIFF_SYM298=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM298
LTDIE_48_POINTER:

	.byte 13
LDIFF_SYM299=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM299
LTDIE_48_REFERENCE:

	.byte 14
LDIFF_SYM300=LTDIE_48 - Ldebug_info_start
	.long LDIFF_SYM300
LTDIE_39:

	.byte 5
	.asciz "System_Xml_Schema_XmlSchemaSet"

	.byte 80,16
LDIFF_SYM301=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM301
	.byte 2,35,0,6
	.asciz "nameTable"

LDIFF_SYM302=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM302
	.byte 2,35,8,6
	.asciz "xmlResolver"

LDIFF_SYM303=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM303
	.byte 2,35,12,6
	.asciz "schemas"

LDIFF_SYM304=LTDIE_41_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM304
	.byte 2,35,16,6
	.asciz "attributes"

LDIFF_SYM305=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM305
	.byte 2,35,20,6
	.asciz "elements"

LDIFF_SYM306=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM306
	.byte 2,35,24,6
	.asciz "types"

LDIFF_SYM307=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM307
	.byte 2,35,28,6
	.asciz "settings"

LDIFF_SYM308=LTDIE_46_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM308
	.byte 2,35,32,6
	.asciz "isCompiled"

LDIFF_SYM309=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM309
	.byte 2,35,60,6
	.asciz "<CompilationId>k__BackingField"

LDIFF_SYM310=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM310
	.byte 2,35,64,6
	.asciz "ValidationEventHandler"

LDIFF_SYM311=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM311
	.byte 2,35,36,6
	.asciz "global_attribute_groups"

LDIFF_SYM312=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM312
	.byte 2,35,40,6
	.asciz "global_groups"

LDIFF_SYM313=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM313
	.byte 2,35,44,6
	.asciz "global_notations"

LDIFF_SYM314=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM314
	.byte 2,35,48,6
	.asciz "global_identity_constraints"

LDIFF_SYM315=LTDIE_42_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM315
	.byte 2,35,52,6
	.asciz "global_ids"

LDIFF_SYM316=LTDIE_48_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM316
	.byte 2,35,56,0,7
	.asciz "System_Xml_Schema_XmlSchemaSet"

LDIFF_SYM317=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM317
LTDIE_39_POINTER:

	.byte 13
LDIFF_SYM318=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM318
LTDIE_39_REFERENCE:

	.byte 14
LDIFF_SYM319=LTDIE_39 - Ldebug_info_start
	.long LDIFF_SYM319
LTDIE_54:

	.byte 8
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

	.byte 4
LDIFF_SYM320=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM320
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "ProcessInlineSchema"

	.byte 1,9
	.asciz "ProcessSchemaLocation"

	.byte 2,9
	.asciz "ReportValidationWarnings"

	.byte 4,9
	.asciz "ProcessIdentityConstraints"

	.byte 8,9
	.asciz "AllowXmlAttributes"

	.byte 16,0,7
	.asciz "System_Xml_Schema_XmlSchemaValidationFlags"

LDIFF_SYM321=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM321
LTDIE_54_POINTER:

	.byte 13
LDIFF_SYM322=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM322
LTDIE_54_REFERENCE:

	.byte 14
LDIFF_SYM323=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM323
LTDIE_55:

	.byte 8
	.asciz "System_Xml_ValidationType"

	.byte 4
LDIFF_SYM324=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM324
	.byte 9
	.asciz "None"

	.byte 0,9
	.asciz "Auto"

	.byte 1,9
	.asciz "DTD"

	.byte 2,9
	.asciz "XDR"

	.byte 3,9
	.asciz "Schema"

	.byte 4,0,7
	.asciz "System_Xml_ValidationType"

LDIFF_SYM325=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM325
LTDIE_55_POINTER:

	.byte 13
LDIFF_SYM326=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM326
LTDIE_55_REFERENCE:

	.byte 14
LDIFF_SYM327=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM327
LTDIE_36:

	.byte 5
	.asciz "System_Xml_XmlReaderSettings"

	.byte 60,16
LDIFF_SYM328=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM328
	.byte 2,35,0,6
	.asciz "checkCharacters"

LDIFF_SYM329=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM329
	.byte 2,35,24,6
	.asciz "closeInput"

LDIFF_SYM330=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM330
	.byte 2,35,25,6
	.asciz "conformance"

LDIFF_SYM331=LTDIE_37 - Ldebug_info_start
	.long LDIFF_SYM331
	.byte 2,35,28,6
	.asciz "ignoreComments"

LDIFF_SYM332=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM332
	.byte 2,35,32,6
	.asciz "ignoreProcessingInstructions"

LDIFF_SYM333=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM333
	.byte 2,35,33,6
	.asciz "ignoreWhitespace"

LDIFF_SYM334=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM334
	.byte 2,35,34,6
	.asciz "lineNumberOffset"

LDIFF_SYM335=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM335
	.byte 2,35,36,6
	.asciz "linePositionOffset"

LDIFF_SYM336=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM336
	.byte 2,35,40,6
	.asciz "prohibitDtd"

LDIFF_SYM337=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM337
	.byte 2,35,44,6
	.asciz "nameTable"

LDIFF_SYM338=LTDIE_38_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM338
	.byte 2,35,8,6
	.asciz "schemas"

LDIFF_SYM339=LTDIE_39_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM339
	.byte 2,35,12,6
	.asciz "schemasNeedsInitialization"

LDIFF_SYM340=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM340
	.byte 2,35,45,6
	.asciz "validationFlags"

LDIFF_SYM341=LTDIE_54 - Ldebug_info_start
	.long LDIFF_SYM341
	.byte 2,35,48,6
	.asciz "validationType"

LDIFF_SYM342=LTDIE_55 - Ldebug_info_start
	.long LDIFF_SYM342
	.byte 2,35,52,6
	.asciz "xmlResolver"

LDIFF_SYM343=LTDIE_40_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM343
	.byte 2,35,16,6
	.asciz "isReadOnly"

LDIFF_SYM344=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM344
	.byte 2,35,56,6
	.asciz "isAsync"

LDIFF_SYM345=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM345
	.byte 2,35,57,6
	.asciz "ValidationEventHandler"

LDIFF_SYM346=LTDIE_47_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM346
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReaderSettings"

LDIFF_SYM347=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM347
LTDIE_36_POINTER:

	.byte 13
LDIFF_SYM348=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM348
LTDIE_36_REFERENCE:

	.byte 14
LDIFF_SYM349=LTDIE_36 - Ldebug_info_start
	.long LDIFF_SYM349
LTDIE_32:

	.byte 5
	.asciz "System_Xml_XmlReader"

	.byte 24,16
LDIFF_SYM350=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM350
	.byte 2,35,0,6
	.asciz "readStringBuffer"

LDIFF_SYM351=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM351
	.byte 2,35,8,6
	.asciz "binary"

LDIFF_SYM352=LTDIE_34_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM352
	.byte 2,35,12,6
	.asciz "settings"

LDIFF_SYM353=LTDIE_36_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM353
	.byte 2,35,16,6
	.asciz "asyncRunning"

LDIFF_SYM354=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM354
	.byte 2,35,20,0,7
	.asciz "System_Xml_XmlReader"

LDIFF_SYM355=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM355
LTDIE_32_POINTER:

	.byte 13
LDIFF_SYM356=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM356
LTDIE_32_REFERENCE:

	.byte 14
LDIFF_SYM357=LTDIE_32 - Ldebug_info_start
	.long LDIFF_SYM357
LTDIE_57:

	.byte 17
	.asciz "System_Collections_IDictionary"

	.byte 8,7
	.asciz "System_Collections_IDictionary"

LDIFF_SYM358=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM358
LTDIE_57_POINTER:

	.byte 13
LDIFF_SYM359=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM359
LTDIE_57_REFERENCE:

	.byte 14
LDIFF_SYM360=LTDIE_57 - Ldebug_info_start
	.long LDIFF_SYM360
LTDIE_56:

	.byte 5
	.asciz "System_Exception"

	.byte 60,16
LDIFF_SYM361=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM361
	.byte 2,35,0,6
	.asciz "trace_ips"

LDIFF_SYM362=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM362
	.byte 2,35,8,6
	.asciz "inner_exception"

LDIFF_SYM363=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM363
	.byte 2,35,12,6
	.asciz "message"

LDIFF_SYM364=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM364
	.byte 2,35,16,6
	.asciz "help_link"

LDIFF_SYM365=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM365
	.byte 2,35,20,6
	.asciz "class_name"

LDIFF_SYM366=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM366
	.byte 2,35,24,6
	.asciz "stack_trace"

LDIFF_SYM367=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM367
	.byte 2,35,28,6
	.asciz "_remoteStackTraceString"

LDIFF_SYM368=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM368
	.byte 2,35,32,6
	.asciz "remote_stack_index"

LDIFF_SYM369=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM369
	.byte 2,35,36,6
	.asciz "hresult"

LDIFF_SYM370=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM370
	.byte 2,35,40,6
	.asciz "source"

LDIFF_SYM371=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM371
	.byte 2,35,44,6
	.asciz "_data"

LDIFF_SYM372=LTDIE_57_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM372
	.byte 2,35,48,6
	.asciz "captured_traces"

LDIFF_SYM373=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM373
	.byte 2,35,52,6
	.asciz "native_trace_ips"

LDIFF_SYM374=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM374
	.byte 2,35,56,0,7
	.asciz "System_Exception"

LDIFF_SYM375=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM375
LTDIE_56_POINTER:

	.byte 13
LDIFF_SYM376=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM376
LTDIE_56_REFERENCE:

	.byte 14
LDIFF_SYM377=LTDIE_56 - Ldebug_info_start
	.long LDIFF_SYM377
	.byte 2
	.asciz "OffSeasonPro.Core.Models.SimpleDataStore:LoadSettingsFrom"
	.long _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream
	.long Lme_36

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM378=LTDIE_3_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM378
	.byte 2,123,48,3
	.asciz "inputStream"

LDIFF_SYM379=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM379
	.byte 2,123,52,11
	.asciz "loadedData"

LDIFF_SYM380=LTDIE_26_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM380
	.byte 1,85,11
	.asciz ""

LDIFF_SYM381=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM381
	.byte 2,123,0,11
	.asciz "reader"

LDIFF_SYM382=LTDIE_32_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM382
	.byte 2,123,4,11
	.asciz "settings"

LDIFF_SYM383=LTDIE_5_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM383
	.byte 2,123,8,11
	.asciz "exception"

LDIFF_SYM384=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM384
	.byte 2,123,12,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM385=Lfde1_end - Lfde1_start
	.long LDIFF_SYM385
Lfde1_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream

LDIFF_SYM386=Lme_36 - _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream
	.long LDIFF_SYM386
	.byte 12,13,0,72,14,8,135,2,68,14,20,133,5,136,4,139,3,142,1,68,14,96,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_63:

	.byte 5
	.asciz "Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject"

	.byte 8,16
LDIFF_SYM387=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM387
	.byte 2,35,0,0,7
	.asciz "Cirrious_CrossCore_Core_MvxMainThreadDispatchingObject"

LDIFF_SYM388=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM388
LTDIE_63_POINTER:

	.byte 13
LDIFF_SYM389=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM389
LTDIE_63_REFERENCE:

	.byte 14
LDIFF_SYM390=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM390
LTDIE_64:

	.byte 5
	.asciz "System_ComponentModel_PropertyChangedEventHandler"

	.byte 52,16
LDIFF_SYM391=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM391
	.byte 2,35,0,0,7
	.asciz "System_ComponentModel_PropertyChangedEventHandler"

LDIFF_SYM392=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM392
LTDIE_64_POINTER:

	.byte 13
LDIFF_SYM393=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM393
LTDIE_64_REFERENCE:

	.byte 14
LDIFF_SYM394=LTDIE_64 - Ldebug_info_start
	.long LDIFF_SYM394
LTDIE_62:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged"

	.byte 16,16
LDIFF_SYM395=LTDIE_63 - Ldebug_info_start
	.long LDIFF_SYM395
	.byte 2,35,0,6
	.asciz "PropertyChanged"

LDIFF_SYM396=LTDIE_64_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM396
	.byte 2,35,8,6
	.asciz "_shouldAlwaysRaiseInpcOnUserInterfaceThread"

LDIFF_SYM397=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM397
	.byte 2,35,12,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxNotifyPropertyChanged"

LDIFF_SYM398=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM398
LTDIE_62_POINTER:

	.byte 13
LDIFF_SYM399=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM399
LTDIE_62_REFERENCE:

	.byte 14
LDIFF_SYM400=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM400
LTDIE_61:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxNavigatingObject"

	.byte 16,16
LDIFF_SYM401=LTDIE_62 - Ldebug_info_start
	.long LDIFF_SYM401
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxNavigatingObject"

LDIFF_SYM402=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM402
LTDIE_61_POINTER:

	.byte 13
LDIFF_SYM403=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM403
LTDIE_61_REFERENCE:

	.byte 14
LDIFF_SYM404=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM404
LTDIE_66:

	.byte 8
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

	.byte 4
LDIFF_SYM405=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM405
	.byte 9
	.asciz "Unknown"

	.byte 0,9
	.asciz "UserAction"

	.byte 1,9
	.asciz "Bookmark"

	.byte 2,9
	.asciz "AutomatedService"

	.byte 3,9
	.asciz "Other"

	.byte 4,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

LDIFF_SYM406=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM406
LTDIE_66_POINTER:

	.byte 13
LDIFF_SYM407=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM407
LTDIE_66_REFERENCE:

	.byte 14
LDIFF_SYM408=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM408
LTDIE_65:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

	.byte 16,16
LDIFF_SYM409=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM409
	.byte 2,35,0,6
	.asciz "<Type>k__BackingField"

LDIFF_SYM410=LTDIE_66 - Ldebug_info_start
	.long LDIFF_SYM410
	.byte 2,35,12,6
	.asciz "<AdditionalInfo>k__BackingField"

LDIFF_SYM411=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM411
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

LDIFF_SYM412=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM412
LTDIE_65_POINTER:

	.byte 13
LDIFF_SYM413=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM413
LTDIE_65_REFERENCE:

	.byte 14
LDIFF_SYM414=LTDIE_65 - Ldebug_info_start
	.long LDIFF_SYM414
LTDIE_60:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModel"

	.byte 20,16
LDIFF_SYM415=LTDIE_61 - Ldebug_info_start
	.long LDIFF_SYM415
	.byte 2,35,0,6
	.asciz "<RequestedBy>k__BackingField"

LDIFF_SYM416=LTDIE_65_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM416
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModel"

LDIFF_SYM417=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM417
LTDIE_60_POINTER:

	.byte 13
LDIFF_SYM418=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM418
LTDIE_60_REFERENCE:

	.byte 14
LDIFF_SYM419=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM419
LTDIE_59:

	.byte 5
	.asciz "OffSeasonPro_Core_ViewModels_BaseViewModel"

	.byte 20,16
LDIFF_SYM420=LTDIE_60 - Ldebug_info_start
	.long LDIFF_SYM420
	.byte 2,35,0,0,7
	.asciz "OffSeasonPro_Core_ViewModels_BaseViewModel"

LDIFF_SYM421=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM421
LTDIE_59_POINTER:

	.byte 13
LDIFF_SYM422=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM422
LTDIE_59_REFERENCE:

	.byte 14
LDIFF_SYM423=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM423
LTDIE_67:

	.byte 17
	.asciz "OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer"

	.byte 8,7
	.asciz "OffSeasonPro_Plugin_MusicPlayerPlugin_IMusicPlayer"

LDIFF_SYM424=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM424
LTDIE_67_POINTER:

	.byte 13
LDIFF_SYM425=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM425
LTDIE_67_REFERENCE:

	.byte 14
LDIFF_SYM426=LTDIE_67 - Ldebug_info_start
	.long LDIFF_SYM426
LTDIE_68:

	.byte 17
	.asciz "Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Plugins_Messenger_IMvxMessenger"

LDIFF_SYM427=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM427
LTDIE_68_POINTER:

	.byte 13
LDIFF_SYM428=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM428
LTDIE_68_REFERENCE:

	.byte 14
LDIFF_SYM429=LTDIE_68 - Ldebug_info_start
	.long LDIFF_SYM429
LTDIE_70:

	.byte 5
	.asciz "System_Threading_TimerCallback"

	.byte 52,16
LDIFF_SYM430=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM430
	.byte 2,35,0,0,7
	.asciz "System_Threading_TimerCallback"

LDIFF_SYM431=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM431
LTDIE_70_POINTER:

	.byte 13
LDIFF_SYM432=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM432
LTDIE_70_REFERENCE:

	.byte 14
LDIFF_SYM433=LTDIE_70 - Ldebug_info_start
	.long LDIFF_SYM433
LTDIE_71:

	.byte 5
	.asciz "System_Int64"

	.byte 16,16
LDIFF_SYM434=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM434
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM435=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM435
	.byte 2,35,8,0,7
	.asciz "System_Int64"

LDIFF_SYM436=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM436
LTDIE_71_POINTER:

	.byte 13
LDIFF_SYM437=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM437
LTDIE_71_REFERENCE:

	.byte 14
LDIFF_SYM438=LTDIE_71 - Ldebug_info_start
	.long LDIFF_SYM438
LTDIE_69:

	.byte 5
	.asciz "System_Threading_Timer"

	.byte 48,16
LDIFF_SYM439=LTDIE_21 - Ldebug_info_start
	.long LDIFF_SYM439
	.byte 2,35,0,6
	.asciz "callback"

LDIFF_SYM440=LTDIE_70_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM440
	.byte 2,35,12,6
	.asciz "state"

LDIFF_SYM441=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM441
	.byte 2,35,16,6
	.asciz "due_time_ms"

LDIFF_SYM442=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM442
	.byte 2,35,20,6
	.asciz "period_ms"

LDIFF_SYM443=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM443
	.byte 2,35,28,6
	.asciz "next_run"

LDIFF_SYM444=LDIE_I8 - Ldebug_info_start
	.long LDIFF_SYM444
	.byte 2,35,36,6
	.asciz "disposed"

LDIFF_SYM445=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM445
	.byte 2,35,44,0,7
	.asciz "System_Threading_Timer"

LDIFF_SYM446=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM446
LTDIE_69_POINTER:

	.byte 13
LDIFF_SYM447=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM447
LTDIE_69_REFERENCE:

	.byte 14
LDIFF_SYM448=LTDIE_69 - Ldebug_info_start
	.long LDIFF_SYM448
LTDIE_58:

	.byte 5
	.asciz "OffSeasonPro_Core_ViewModels_WorkoutViewModel"

	.byte 64,16
LDIFF_SYM449=LTDIE_59 - Ldebug_info_start
	.long LDIFF_SYM449
	.byte 2,35,0,6
	.asciz "_player"

LDIFF_SYM450=LTDIE_67_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM450
	.byte 2,35,20,6
	.asciz "_buttonsEnabled"

LDIFF_SYM451=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM451
	.byte 2,35,48,6
	.asciz "_reset"

LDIFF_SYM452=LTDIE_18_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM452
	.byte 2,35,24,6
	.asciz "_tts"

LDIFF_SYM453=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM453
	.byte 2,35,28,6
	.asciz "_messenger"

LDIFF_SYM454=LTDIE_68_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM454
	.byte 2,35,32,6
	.asciz "_workoutClock"

LDIFF_SYM455=LTDIE_69_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM455
	.byte 2,35,36,6
	.asciz "_workingout"

LDIFF_SYM456=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM456
	.byte 2,35,49,6
	.asciz "_workoutReset"

LDIFF_SYM457=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM457
	.byte 2,35,50,6
	.asciz "_workoutComplete"

LDIFF_SYM458=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM458
	.byte 2,35,51,6
	.asciz "_clockValue"

LDIFF_SYM459=LDIE_R4 - Ldebug_info_start
	.long LDIFF_SYM459
	.byte 2,35,52,6
	.asciz "_btnPausePlayText"

LDIFF_SYM460=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM460
	.byte 2,35,40,6
	.asciz "_maxProgressValue"

LDIFF_SYM461=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM461
	.byte 2,35,56,6
	.asciz "_currentPostion"

LDIFF_SYM462=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM462
	.byte 2,35,44,6
	.asciz "_playing"

LDIFF_SYM463=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM463
	.byte 2,35,60,6
	.asciz "_resizeLabel"

LDIFF_SYM464=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM464
	.byte 2,35,61,0,7
	.asciz "OffSeasonPro_Core_ViewModels_WorkoutViewModel"

LDIFF_SYM465=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM465
LTDIE_58_POINTER:

	.byte 13
LDIFF_SYM466=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM466
LTDIE_58_REFERENCE:

	.byte 14
LDIFF_SYM467=LTDIE_58 - Ldebug_info_start
	.long LDIFF_SYM467
	.byte 2
	.asciz "OffSeasonPro.Core.ViewModels.WorkoutViewModel:UpdateWorkout"
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout
	.long Lme_7a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM468=LTDIE_58_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM468
	.byte 3,123,204,0,11
	.asciz "sb"

LDIFF_SYM469=LTDIE_33_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM469
	.byte 2,123,8,11
	.asciz "ex"

LDIFF_SYM470=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM470
	.byte 2,123,12,11
	.asciz "tmpEvent"

LDIFF_SYM471=LTDIE_19_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM471
	.byte 2,123,16,11
	.asciz "ex"

LDIFF_SYM472=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM472
	.byte 2,123,20,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM473=Lfde2_end - Lfde2_start
	.long LDIFF_SYM473
Lfde2_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout

LDIFF_SYM474=Lme_7a - _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout
	.long LDIFF_SYM474
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,112,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "OffSeasonPro.Core.ViewModels.WorkoutViewModel:ResumeWorkout"
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0
	.long Lme_84

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM475=LTDIE_58_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM475
	.byte 2,123,24,11
	.asciz "ex"

LDIFF_SYM476=LTDIE_56_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM476
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM477=Lfde3_end - Lfde3_start
	.long LDIFF_SYM477
Lfde3_start:

	.long 0
	.align 2
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0

LDIFF_SYM478=Lme_84 - _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0
	.long LDIFF_SYM478
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Users/blakeD/Desktop/OffSeasonPro/OffSeasonPro.Core"
	.asciz "/Users/blakeD/Desktop/OffSeasonPro/OffSeasonPro.Core/Models"
	.asciz "/Users/blakeD/Desktop/OffSeasonPro/OffSeasonPro.Core/ViewModels"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Extensions.cs"

	.byte 1,0,0
	.asciz "SimpleDataStore.cs"

	.byte 2,0,0
	.asciz "WorkoutViewModel.cs"

	.byte 3,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _OffSeasonPro_Core_Extensions_ToInt32_string_0

	.byte 3,11,4,2,1,3,11,2,44,1,190,2,52,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _OffSeasonPro_Core_Models_SimpleDataStore_LoadSettingsFrom_System_IO_Stream

	.byte 3,229,0,4,3,1,3,229,0,2,192,0,1,8,173,132,8,174,3,1,2,128,1,1,131,3,3,2,212,0,1,76
	.byte 3,1,2,252,0,1,2,196,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_UpdateWorkout

	.byte 3,171,1,4,4,1,3,171,1,2,56,1,3,1,2,40,1,8,232,244,3,1,2,56,1,3,2,2,40,1,244,3
	.byte 4,2,36,1,3,1,2,56,1,3,1,2,40,1,3,1,2,56,1,3,3,2,36,1,8,62,3,5,2,212,0,1
	.byte 3,1,2,36,1,131,8,173,77,3,2,2,48,1,3,5,2,140,1,1,187,3,2,2,184,1,1,8,62,3,1,2
	.byte 56,1,3,2,2,44,1,2,200,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _OffSeasonPro_Core_ViewModels_WorkoutViewModel_ResumeWorkout_0

	.byte 3,135,3,4,4,1,3,135,3,2,36,1,8,173,3,1,2,48,1,76,8,174,2,196,0,1,0,1,1,0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
