	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher:
Leh_func_begin1:
	ldrb	r0, [r0, #8]
	bx	lr
Leh_func_end1:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_set_IsVersionOrHigher_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_set_IsVersionOrHigher_bool:
Leh_func_begin2:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool:
Leh_func_begin3:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
Ltmp2:
	mov	r4, r0
	mov	r0, r1
	mov	r1, r2
	bl	_p_1_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool_llvm
	strb	r0, [r4, #8]
	pop	{r4, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool:
Leh_func_begin4:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
	push	{r8, r10, r11}
Ltmp5:
	sub	sp, sp, #4
	mov	r6, #0
	mov	r10, r1
	mov	r5, r0
	mov	r0, sp
	str	r6, [sp]
	movw	r4, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC4_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC4_0+8))
LPC4_0:
	add	r4, pc, r4
	ldr	r1, [r4, #16]
	mov	r8, r1
	bl	_p_2_plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem__llvm
	ldr	r0, [sp]
	cmp	r0, #0
	beq	LBB4_2
	ldr	r2, [r0]
	ldr	r1, [r4, #20]
	sub	r2, r2, #64
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	ldr	r1, [r0]
	ldr	r0, [r0, #12]
	cmp	r0, r5
	movge	r6, #1
	b	LBB4_3
LBB4_2:
	ldr	r11, [r4, #24]
	ldr	r1, [r4, #28]
	mov	r0, r1
	mov	r1, #2
	bl	_p_3_plt__jit_icall_mono_array_new_specific_llvm
	mov	r6, r0
	ldr	r0, [r4, #32]
	bl	_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #0
	str	r5, [r2, #8]
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	add	r0, r4, #36
	cmp	r10, #0
	mov	r1, #1
	addeq	r0, r4, #40
	ldr	r2, [r0]
	ldr	r0, [r6]
	ldr	r3, [r0, #128]
	mov	r0, r6
	blx	r3
	mov	r0, r11
	mov	r1, r6
	bl	_p_5_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm
	mov	r6, r10
LBB4_3:
	mov	r0, r6
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_get_Version
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_get_Version:
Leh_func_begin5:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end5:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_set_Version_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_set_Version_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion:
Leh_func_begin6:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end6:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor:
Leh_func_begin7:
	push	{r7, lr}
Ltmp6:
	mov	r7, sp
Ltmp7:
Ltmp8:
	bl	_p_6_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion_llvm
	pop	{r7, pc}
Leh_func_end7:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion:
Leh_func_begin8:
	push	{r4, r5, r6, r7, lr}
Ltmp9:
	add	r7, sp, #12
Ltmp10:
	push	{r8}
Ltmp11:
	mov	r4, r0
	bl	_p_7_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	movw	r6, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC8_0+8))
	mov	r5, r0
	mov	r1, #1
	movt	r6, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC8_0+8))
LPC8_0:
	add	r6, pc, r6
	ldr	r0, [r6, #44]
	bl	_p_3_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, #46
	strh	r0, [r1, #16]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_8_plt_string_Split_char___llvm
	mov	r5, r0
	ldr	r0, [r6, #48]
	ldr	r1, [r0]
	cmp	r1, #0
	bne	LBB8_2
	ldr	r0, [r6, #64]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #68]
	str	r1, [r0, #20]
	ldr	r1, [r6, #72]
	str	r1, [r0, #28]
	ldr	r1, [r6, #76]
	str	r1, [r0, #12]
	ldr	r1, [r6, #48]
	str	r0, [r1]
	ldr	r0, [r6, #48]
	ldr	r1, [r0]
LBB8_2:
	ldr	r0, [r6, #52]
	mov	r8, r0
	mov	r0, r5
	bl	_p_9_plt_System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int_llvm
	ldr	r1, [r6, #56]
	mov	r8, r1
	bl	_p_10_plt_System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_llvm
	mov	r5, r0
	ldr	r0, [r6, #60]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_12_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int___llvm
	str	r6, [r4, #8]
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__BuildVersionb__0_string
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__BuildVersionb__0_string:
Leh_func_begin9:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
Ltmp14:
	bl	_p_13_plt_int_Parse_string_llvm
	pop	{r7, pc}
Leh_func_end9:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp15:
	add	r7, sp, #8
Ltmp16:
Ltmp17:
	cmp	r1, #0
	ldrne	r2, [r1, #12]
	cmpne	r2, #0
	beq	LBB10_3
	str	r1, [r0, #8]
	ldr	r2, [r1, #12]
	cmp	r2, #0
	beq	LBB10_4
	ldr	r2, [r1, #16]
	str	r2, [r0, #12]
	ldr	r2, [r1, #12]
	cmp	r2, #1
	ldrgt	r1, [r1, #20]
	strgt	r1, [r0, #16]
	pop	{r4, r5, r7, pc}
LBB10_3:
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC10_1+8))
	mov	r1, #160
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC10_1+8))
LPC10_1:
	ldr	r0, [pc, r0]
	bl	_p_14_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC10_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC10_2+8))
LPC10_2:
	add	r0, pc, r0
	ldr	r0, [r0, #80]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_15_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm
	mov	r0, r5
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Ltmp18:
LBB10_4:
	ldr	r0, LCPI10_0
LPC10_0:
	add	r1, pc, r0
	movw	r0, #600
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI10_0:
	.long	Ltmp18-(LPC10_0+8)
	.end_data_region
Leh_func_end10:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Major
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Major:
Leh_func_begin11:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end11:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Major_int
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Major_int:
Leh_func_begin12:
	str	r1, [r0, #12]
	bx	lr
Leh_func_end12:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Minor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Minor:
Leh_func_begin13:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end13:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Minor_int
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Minor_int:
Leh_func_begin14:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end14:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Parts
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Parts:
Leh_func_begin15:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end15:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Parts_int__
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Parts_int__:
Leh_func_begin16:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end16:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorFromInt_uint
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorFromInt_uint:
Leh_func_begin17:
	push	{r4, r5, r6, r7, lr}
Ltmp19:
	add	r7, sp, #12
Ltmp20:
Ltmp21:
	vpush	{d8, d9, d10}
	mov	r4, r0
	ubfx	r0, r4, #16, #8
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	mov	r5, r0
	ubfx	r0, r4, #8, #8
	mov	r6, r1
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	vldr	s16, LCPI17_0
	uxtb	r2, r4
	vmov	d10, r0, r1
	vmov	d9, r5, r6
	mov	r0, r2
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	vmov	d0, r0, r1
	vcvt.f32.f64	s2, d9
	vcvt.f32.f64	s4, d10
	vcvt.f32.f64	s0, d0
	vdiv.f32	s2, s2, s16
	vdiv.f32	s4, s4, s16
	vdiv.f32	s0, s0, s16
	vmov	r0, s2
	vmov	r1, s4
	vmov	r2, s0
	bl	_p_19_plt_MonoTouch_UIKit_UIColor_FromRGB_single_single_single_llvm
	vpop	{d8, d9, d10}
	pop	{r4, r5, r6, r7, pc}
	.align	2
	.data_region
LCPI17_0:
	.long	1132396544
	.end_data_region
Leh_func_end17:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorWithAlphaFromInt_uint
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorWithAlphaFromInt_uint:
Leh_func_begin18:
	push	{r4, r5, r6, r7, lr}
Ltmp22:
	add	r7, sp, #12
Ltmp23:
	push	{r10, r11}
Ltmp24:
	vpush	{d8, d9, d10, d11}
	mov	r4, r0
	ubfx	r0, r4, #16, #8
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	mov	r10, r0
	ubfx	r0, r4, #8, #8
	mov	r11, r1
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	mov	r5, r0
	uxtb	r0, r4
	mov	r6, r1
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	vldr	s16, LCPI18_0
	lsr	r2, r4, #24
	vmov	d11, r0, r1
	vmov	d9, r10, r11
	vmov	d10, r5, r6
	mov	r0, r2
	bl	_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm
	vmov	d0, r0, r1
	vcvt.f32.f64	s2, d9
	vcvt.f32.f64	s4, d10
	vcvt.f32.f64	s6, d11
	vcvt.f32.f64	s0, d0
	vdiv.f32	s2, s2, s16
	vdiv.f32	s4, s4, s16
	vdiv.f32	s6, s6, s16
	vdiv.f32	s0, s0, s16
	vmov	r0, s2
	vmov	r1, s4
	vmov	r2, s6
	vmov	r3, s0
	bl	_p_20_plt_MonoTouch_UIKit_UIColor_FromRGBA_single_single_single_single_llvm
	vpop	{d8, d9, d10, d11}
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
	.align	2
	.data_region
LCPI18_0:
	.long	1132396544
	.end_data_region
Leh_func_end18:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_IntFromColor_MonoTouch_UIKit_UIColor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_IntFromColor_MonoTouch_UIKit_UIColor:
Leh_func_begin19:
	push	{r4, r5, r7, lr}
Ltmp25:
	add	r7, sp, #8
Ltmp26:
Ltmp27:
	sub	sp, sp, #20
	mov	r1, #0
	add	r2, sp, #8
	add	r3, sp, #12
	str	r1, [sp, #4]
	str	r1, [sp, #8]
	str	r1, [sp, #12]
	str	r1, [r7, #-12]
	ldr	r1, [r0]
	sub	r1, r7, #12
	str	r1, [sp]
	add	r1, sp, #4
	bl	_p_21_plt_MonoTouch_UIKit_UIColor_GetRGBA_single__single__single__single__llvm
	vldr	s0, [r7, #-12]
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_22_plt__jit_icall___emul_fconv_to_i8_llvm
	mov	r2, #24
	bl	_p_23_plt__jit_icall___emul_lshl_llvm
	vldr	s0, [sp, #4]
	mov	r4, r0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_22_plt__jit_icall___emul_fconv_to_i8_llvm
	mov	r2, #16
	bl	_p_23_plt__jit_icall___emul_lshl_llvm
	vldr	s0, [sp, #8]
	mov	r5, r0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_22_plt__jit_icall___emul_fconv_to_i8_llvm
	mov	r2, #8
	bl	_p_23_plt__jit_icall___emul_lshl_llvm
	vldr	s0, [sp, #12]
	orr	r1, r5, r4
	orr	r4, r1, r0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_22_plt__jit_icall___emul_fconv_to_i8_llvm
	orr	r0, r4, r0
	sub	sp, r7, #8
	pop	{r4, r5, r7, pc}
Leh_func_end19:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToDateTimeUtc_MonoTouch_Foundation_NSDate
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToDateTimeUtc_MonoTouch_Foundation_NSDate:
Leh_func_begin20:
	push	{r4, r7, lr}
Ltmp28:
	add	r7, sp, #4
Ltmp29:
Ltmp30:
	sub	sp, sp, #20
	bic	sp, sp, #7
	mov	r4, r1
	mov	r1, #0
	str	r1, [sp, #4]
	str	r1, [sp]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC20_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC20_0+8))
LPC20_0:
	add	r1, pc, r1
	ldr	r2, [r1, #84]
	ldr	r1, [r2]
	ldr	r2, [r2, #4]
	stm	sp, {r1, r2}
	ldr	r1, [r0]
	ldr	r1, [r1, #80]
	blx	r1
	mov	r2, r0
	mov	r3, r1
	mov	r0, sp
	add	r1, sp, #8
	bl	_p_24_plt_System_DateTime_AddSeconds_double_llvm
	ldr	r0, [sp, #8]
	str	r0, [r4]
	ldr	r0, [sp, #12]
	str	r0, [r4, #4]
	sub	sp, r7, #4
	pop	{r4, r7, pc}
Leh_func_end20:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods__cctor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods__cctor:
Leh_func_begin21:
	push	{r7, lr}
Ltmp31:
	mov	r7, sp
Ltmp32:
Ltmp33:
	sub	sp, sp, #32
	bic	sp, sp, #7
	mov	r0, #0
	mov	r1, #1
	mov	r2, #1
	mov	r3, #1
	str	r0, [sp, #20]
	str	r0, [sp]
	str	r0, [sp, #4]
	str	r0, [sp, #8]
	str	r0, [sp, #16]
	str	r1, [sp, #12]
	add	r0, sp, #16
	movw	r1, #2001
	bl	_p_25_plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind_llvm
	ldr	r1, [sp, #20]
	ldr	r0, [sp, #16]
	str	r1, [sp, #28]
	str	r0, [sp, #24]
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC21_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC21_0+8))
	ldr	r1, [sp, #24]
LPC21_0:
	add	r0, pc, r0
	ldr	r0, [r0, #84]
	str	r1, [r0]
	ldr	r1, [sp, #28]
	str	r1, [r0, #4]
	mov	sp, r7
	pop	{r7, pc}
Leh_func_end21:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchUIViewControllerExtensions_IsVisible_MonoTouch_UIKit_UIViewController
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchUIViewControllerExtensions_IsVisible_MonoTouch_UIKit_UIViewController:
Leh_func_begin22:
	push	{r4, r7, lr}
Ltmp34:
	add	r7, sp, #4
Ltmp35:
Ltmp36:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #172]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	popeq	{r4, r7, pc}
	ldr	r0, [r4]
	ldr	r1, [r0, #168]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB22_2
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC22_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC22_0+8))
	ldr	r2, [r0]
LPC22_0:
	add	r1, pc, r1
	ldr	r1, [r1, #88]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #16]
	cmp	r2, r1
	movne	r0, #0
LBB22_2:
	cmp	r0, #0
	beq	LBB22_4
	ldr	r1, [r0]
	ldr	r1, [r1, #212]
	blx	r1
	mov	r1, r0
	mov	r0, #0
	cmp	r1, r4
	moveq	r0, #1
	pop	{r4, r7, pc}
LBB22_4:
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #184]
	blx	r1
	cmp	r0, #0
	movne	r0, #1
	pop	{r4, r7, pc}
Leh_func_end22:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask_DoUrlOpen_MonoTouch_Foundation_NSUrl
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask_DoUrlOpen_MonoTouch_Foundation_NSUrl:
Leh_func_begin23:
	push	{r4, r7, lr}
Ltmp37:
	add	r7, sp, #4
Ltmp38:
Ltmp39:
	mov	r4, r1
	bl	_p_26_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #88]
	mov	r1, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end23:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask__ctor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask__ctor:
Leh_func_begin24:
	bx	lr
Leh_func_end24:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_get_ViewController
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_get_ViewController:
Leh_func_begin25:
	ldr	r1, [r0, #8]
	cmp	r1, #0
	beq	LBB25_2
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC25_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC25_0+8))
LPC25_0:
	add	r0, pc, r0
	ldr	r2, [r0, #92]
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r3, [r0, #12]
	mov	r0, #0
	cmp	r3, r2
	bxne	lr
LBB25_2:
	mov	r0, r1
	bx	lr
Leh_func_end25:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
Leh_func_begin26:
	push	{r4, r5, r6, r7, lr}
Ltmp40:
	add	r7, sp, #12
Ltmp41:
	push	{r8, r10, r11}
Ltmp42:
	sub	sp, sp, #8
	mov	r4, r0
	cmp	r1, #0
	beq	LBB26_4
	beq	LBB26_3
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_0+8))
	ldr	r2, [r1]
LPC26_0:
	add	r0, pc, r0
	ldr	r0, [r0, #92]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	bne	LBB26_5
LBB26_3:
	str	r1, [r4, #8]
	mov	r0, r4
	ldr	r6, [r4, #8]
	movw	r10, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_1+8))
	movt	r10, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_1+8))
LPC26_1:
	add	r10, pc, r10
	ldr	r1, [r10, #96]
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r11, r0
	ldr	r0, [r10, #100]
	str	r0, [sp, #4]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r11
	mov	r5, r0
	bl	_p_28_plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr_llvm
	ldr	r1, [r6]
	ldr	r0, [r10, #104]
	sub	r1, r1, #40
	mov	r8, r0
	mov	r0, r6
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
	ldr	r1, [r10, #108]
	ldr	r11, [r4, #8]
	mov	r0, r4
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	ldr	r6, [sp, #4]
	str	r0, [sp]
	mov	r0, r6
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r2, [sp]
	mov	r1, r4
	mov	r5, r0
	bl	_p_28_plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr_llvm
	ldr	r1, [r11]
	ldr	r0, [r10, #112]
	sub	r1, r1, #20
	mov	r8, r0
	mov	r0, r11
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
	ldr	r1, [r10, #116]
	ldr	r11, [r4, #8]
	mov	r0, r4
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	str	r0, [sp]
	mov	r0, r6
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r2, [sp]
	mov	r1, r4
	mov	r5, r0
	bl	_p_28_plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr_llvm
	ldr	r1, [r11]
	ldr	r0, [r10, #120]
	sub	r1, r1, #76
	mov	r8, r0
	mov	r0, r11
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
	ldr	r1, [r10, #124]
	ldr	r6, [r4, #8]
	mov	r0, r4
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r11, r0
	ldr	r0, [sp, #4]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r11
	mov	r5, r0
	bl	_p_28_plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr_llvm
	ldr	r1, [r6]
	ldr	r0, [r10, #128]
	sub	r1, r1, #36
	mov	r8, r0
	mov	r0, r6
	ldr	r2, [r1]
	mov	r1, r5
	blx	r2
	ldr	r1, [r10, #132]
	ldr	r5, [r4, #8]
	mov	r0, r4
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r11, r0
	ldr	r0, [r10, #136]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r11
	mov	r6, r0
	bl	_p_29_plt_System_EventHandler__ctor_object_intptr_llvm
	ldr	r1, [r5]
	ldr	r0, [r10, #140]
	sub	r1, r1, #20
	mov	r8, r0
	mov	r0, r5
	ldr	r2, [r1]
	mov	r1, r6
	blx	r2
	ldr	r5, [r4, #8]
	ldr	r1, [r10, #144]
	mov	r0, r4
	bl	_p_27_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r11, r0
	ldr	r0, [r10, #136]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r11
	mov	r6, r0
	bl	_p_29_plt_System_EventHandler__ctor_object_intptr_llvm
	ldr	r1, [r5]
	ldr	r0, [r10, #148]
	sub	r1, r1, #32
	mov	r8, r0
	mov	r0, r5
	ldr	r2, [r1]
	mov	r1, r6
	blx	r2
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB26_4:
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_2+8))
	movw	r1, #258
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_2+8))
LPC26_2:
	ldr	r0, [pc, r0]
	b	LBB26_6
LBB26_5:
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_3+8))
	mov	r1, #348
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC26_3+8))
LPC26_3:
	ldr	r0, [pc, r0]
LBB26_6:
	bl	_p_14_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r1, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end26:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs:
Leh_func_begin27:
	bx	lr
Leh_func_end27:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs:
Leh_func_begin28:
	bx	lr
Leh_func_end28:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin29:
	bx	lr
Leh_func_end29:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin30:
	bx	lr
Leh_func_end30:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin31:
	bx	lr
Leh_func_end31:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin32:
	bx	lr
Leh_func_end32:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout:
Leh_func_begin33:
	push	{r7, lr}
Ltmp43:
	mov	r7, sp
Ltmp44:
Ltmp45:
	bl	_p_31_plt_MonoTouch_UIKit_UICollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout_llvm
	pop	{r7, pc}
Leh_func_end33:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr:
Leh_func_begin34:
	push	{r7, lr}
Ltmp46:
	mov	r7, sp
Ltmp47:
Ltmp48:
	bl	_p_32_plt_MonoTouch_UIKit_UICollectionViewController__ctor_intptr_llvm
	pop	{r7, pc}
Leh_func_end34:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin35:
	push	{r7, lr}
Ltmp49:
	mov	r7, sp
Ltmp50:
Ltmp51:
	bl	_p_33_plt_MonoTouch_UIKit_UICollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	pop	{r7, pc}
Leh_func_end35:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillDisappear_bool:
Leh_func_begin36:
	push	{r4, r5, r7, lr}
Ltmp52:
	add	r7, sp, #8
Ltmp53:
	push	{r8}
Ltmp54:
	mov	r4, r1
	mov	r5, r0
	bl	_p_34_plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r5, #68]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC36_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC36_0+8))
LPC36_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end36:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidAppear_bool:
Leh_func_begin37:
	push	{r4, r5, r7, lr}
Ltmp55:
	add	r7, sp, #8
Ltmp56:
	push	{r8}
Ltmp57:
	mov	r4, r1
	mov	r5, r0
	bl	_p_36_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r5, #60]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC37_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC37_0+8))
LPC37_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end37:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillAppear_bool:
Leh_func_begin38:
	push	{r4, r5, r7, lr}
Ltmp58:
	add	r7, sp, #8
Ltmp59:
	push	{r8}
Ltmp60:
	mov	r4, r1
	mov	r5, r0
	bl	_p_37_plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r5, #56]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC38_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC38_0+8))
LPC38_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end38:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidDisappear_bool:
Leh_func_begin39:
	push	{r4, r5, r7, lr}
Ltmp61:
	add	r7, sp, #8
Ltmp62:
	push	{r8}
Ltmp63:
	mov	r4, r1
	mov	r5, r0
	bl	_p_38_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm
	ldr	r0, [r5, #64]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC39_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC39_0+8))
LPC39_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end39:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidLoad
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidLoad:
Leh_func_begin40:
	push	{r4, r7, lr}
Ltmp64:
	add	r7, sp, #4
Ltmp65:
Ltmp66:
	mov	r4, r0
	bl	_p_39_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	ldr	r0, [r4, #52]
	mov	r1, r4
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
	pop	{r4, r7, pc}
Leh_func_end40:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_Dispose_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_Dispose_bool:
Leh_func_begin41:
	push	{r4, r5, r7, lr}
Ltmp67:
	add	r7, sp, #8
Ltmp68:
Ltmp69:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB41_2
	ldr	r0, [r5, #72]
	mov	r1, r5
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
LBB41_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_41_plt_MonoTouch_UIKit_UICollectionViewController_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end41:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin42:
	push	{r4, r5, r6, r7, lr}
Ltmp70:
	add	r7, sp, #12
Ltmp71:
	push	{r8, r10, r11}
Ltmp72:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC42_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC42_2+8))
LPC42_2:
	add	r0, pc, r0
	ldr	r10, [r0, #160]
	beq	LBB42_5
	ldr	r4, [r0, #156]
	b	LBB42_3
LBB42_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB42_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB42_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB42_8
	b	LBB42_2
LBB42_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB42_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB42_8
Ltmp73:
LBB42_7:
	ldr	r0, LCPI42_1
LPC42_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp74:
LBB42_8:
	ldr	r0, LCPI42_0
LPC42_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI42_0:
	.long	Ltmp74-(LPC42_0+8)
LCPI42_1:
	.long	Ltmp73-(LPC42_1+8)
	.end_data_region
Leh_func_end42:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin43:
	push	{r4, r5, r6, r7, lr}
Ltmp75:
	add	r7, sp, #12
Ltmp76:
	push	{r8, r10, r11}
Ltmp77:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC43_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC43_2+8))
LPC43_2:
	add	r0, pc, r0
	ldr	r10, [r0, #160]
	beq	LBB43_5
	ldr	r4, [r0, #156]
	b	LBB43_3
LBB43_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB43_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB43_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB43_8
	b	LBB43_2
LBB43_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB43_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB43_8
Ltmp78:
LBB43_7:
	ldr	r0, LCPI43_1
LPC43_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp79:
LBB43_8:
	ldr	r0, LCPI43_0
LPC43_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI43_0:
	.long	Ltmp79-(LPC43_0+8)
LCPI43_1:
	.long	Ltmp78-(LPC43_1+8)
	.end_data_region
Leh_func_end43:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin44:
	push	{r4, r5, r6, r7, lr}
Ltmp80:
	add	r7, sp, #12
Ltmp81:
	push	{r8, r10, r11}
Ltmp82:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC44_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC44_2+8))
LPC44_2:
	add	r0, pc, r0
	ldr	r10, [r0, #168]
	beq	LBB44_5
	ldr	r4, [r0, #164]
	b	LBB44_3
LBB44_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB44_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB44_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB44_8
	b	LBB44_2
LBB44_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB44_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB44_8
Ltmp83:
LBB44_7:
	ldr	r0, LCPI44_1
LPC44_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp84:
LBB44_8:
	ldr	r0, LCPI44_0
LPC44_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI44_0:
	.long	Ltmp84-(LPC44_0+8)
LCPI44_1:
	.long	Ltmp83-(LPC44_1+8)
	.end_data_region
Leh_func_end44:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin45:
	push	{r4, r5, r6, r7, lr}
Ltmp85:
	add	r7, sp, #12
Ltmp86:
	push	{r8, r10, r11}
Ltmp87:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC45_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC45_2+8))
LPC45_2:
	add	r0, pc, r0
	ldr	r10, [r0, #168]
	beq	LBB45_5
	ldr	r4, [r0, #164]
	b	LBB45_3
LBB45_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB45_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB45_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB45_8
	b	LBB45_2
LBB45_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB45_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB45_8
Ltmp88:
LBB45_7:
	ldr	r0, LCPI45_1
LPC45_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp89:
LBB45_8:
	ldr	r0, LCPI45_0
LPC45_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI45_0:
	.long	Ltmp89-(LPC45_0+8)
LCPI45_1:
	.long	Ltmp88-(LPC45_1+8)
	.end_data_region
Leh_func_end45:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin46:
	push	{r4, r5, r6, r7, lr}
Ltmp90:
	add	r7, sp, #12
Ltmp91:
	push	{r8, r10, r11}
Ltmp92:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC46_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC46_2+8))
LPC46_2:
	add	r0, pc, r0
	ldr	r10, [r0, #168]
	beq	LBB46_5
	ldr	r4, [r0, #164]
	b	LBB46_3
LBB46_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB46_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB46_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB46_8
	b	LBB46_2
LBB46_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB46_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB46_8
Ltmp93:
LBB46_7:
	ldr	r0, LCPI46_1
LPC46_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp94:
LBB46_8:
	ldr	r0, LCPI46_0
LPC46_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI46_0:
	.long	Ltmp94-(LPC46_0+8)
LCPI46_1:
	.long	Ltmp93-(LPC46_1+8)
	.end_data_region
Leh_func_end46:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin47:
	push	{r4, r5, r6, r7, lr}
Ltmp95:
	add	r7, sp, #12
Ltmp96:
	push	{r8, r10, r11}
Ltmp97:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC47_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC47_2+8))
LPC47_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB47_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB47_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB47_6
LBB47_3:
	cmp	r5, #0
	beq	LBB47_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB47_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp98:
LBB47_6:
	ldr	r0, LCPI47_0
LPC47_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp99:
LBB47_7:
	ldr	r0, LCPI47_1
LPC47_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI47_0:
	.long	Ltmp98-(LPC47_0+8)
LCPI47_1:
	.long	Ltmp99-(LPC47_1+8)
	.end_data_region
Leh_func_end47:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin48:
	push	{r4, r5, r6, r7, lr}
Ltmp100:
	add	r7, sp, #12
Ltmp101:
	push	{r8, r10, r11}
Ltmp102:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC48_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC48_2+8))
LPC48_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB48_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB48_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB48_6
LBB48_3:
	cmp	r5, #0
	beq	LBB48_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB48_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp103:
LBB48_6:
	ldr	r0, LCPI48_0
LPC48_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp104:
LBB48_7:
	ldr	r0, LCPI48_1
LPC48_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI48_0:
	.long	Ltmp103-(LPC48_0+8)
LCPI48_1:
	.long	Ltmp104-(LPC48_1+8)
	.end_data_region
Leh_func_end48:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin49:
	push	{r4, r5, r6, r7, lr}
Ltmp105:
	add	r7, sp, #12
Ltmp106:
	push	{r8, r10, r11}
Ltmp107:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC49_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC49_2+8))
LPC49_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB49_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB49_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB49_6
LBB49_3:
	cmp	r5, #0
	beq	LBB49_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB49_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp108:
LBB49_6:
	ldr	r0, LCPI49_0
LPC49_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp109:
LBB49_7:
	ldr	r0, LCPI49_1
LPC49_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI49_0:
	.long	Ltmp108-(LPC49_0+8)
LCPI49_1:
	.long	Ltmp109-(LPC49_1+8)
	.end_data_region
Leh_func_end49:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin50:
	push	{r4, r5, r6, r7, lr}
Ltmp110:
	add	r7, sp, #12
Ltmp111:
	push	{r8, r10, r11}
Ltmp112:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC50_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC50_2+8))
LPC50_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB50_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB50_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB50_6
LBB50_3:
	cmp	r5, #0
	beq	LBB50_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB50_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp113:
LBB50_6:
	ldr	r0, LCPI50_0
LPC50_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp114:
LBB50_7:
	ldr	r0, LCPI50_1
LPC50_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI50_0:
	.long	Ltmp113-(LPC50_0+8)
LCPI50_1:
	.long	Ltmp114-(LPC50_1+8)
	.end_data_region
Leh_func_end50:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin51:
	push	{r4, r5, r6, r7, lr}
Ltmp115:
	add	r7, sp, #12
Ltmp116:
	push	{r8, r10, r11}
Ltmp117:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC51_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC51_2+8))
LPC51_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB51_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB51_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB51_6
LBB51_3:
	cmp	r5, #0
	beq	LBB51_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB51_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp118:
LBB51_6:
	ldr	r0, LCPI51_0
LPC51_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp119:
LBB51_7:
	ldr	r0, LCPI51_1
LPC51_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI51_0:
	.long	Ltmp118-(LPC51_0+8)
LCPI51_1:
	.long	Ltmp119-(LPC51_1+8)
	.end_data_region
Leh_func_end51:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_DisposeCalled_System_EventHandler:
Leh_func_begin52:
	push	{r4, r5, r6, r7, lr}
Ltmp120:
	add	r7, sp, #12
Ltmp121:
	push	{r8, r10, r11}
Ltmp122:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #72]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC52_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC52_2+8))
LPC52_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB52_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB52_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB52_6
LBB52_3:
	cmp	r5, #0
	beq	LBB52_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB52_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp123:
LBB52_6:
	ldr	r0, LCPI52_0
LPC52_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp124:
LBB52_7:
	ldr	r0, LCPI52_1
LPC52_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI52_0:
	.long	Ltmp123-(LPC52_0+8)
LCPI52_1:
	.long	Ltmp124-(LPC52_1+8)
	.end_data_region
Leh_func_end52:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_DisposeCalled_System_EventHandler:
Leh_func_begin53:
	push	{r4, r5, r6, r7, lr}
Ltmp125:
	add	r7, sp, #12
Ltmp126:
	push	{r8, r10, r11}
Ltmp127:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #72]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC53_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC53_2+8))
LPC53_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB53_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB53_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB53_6
LBB53_3:
	cmp	r5, #0
	beq	LBB53_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB53_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp128:
LBB53_6:
	ldr	r0, LCPI53_0
LPC53_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp129:
LBB53_7:
	ldr	r0, LCPI53_1
LPC53_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI53_0:
	.long	Ltmp128-(LPC53_0+8)
LCPI53_1:
	.long	Ltmp129-(LPC53_1+8)
	.end_data_region
Leh_func_end53:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor:
Leh_func_begin54:
	push	{r7, lr}
Ltmp130:
	mov	r7, sp
Ltmp131:
Ltmp132:
	bl	_p_46_plt_MonoTouch_UIKit_UITabBarController__ctor_llvm
	pop	{r7, pc}
Leh_func_end54:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr:
Leh_func_begin55:
	push	{r7, lr}
Ltmp133:
	mov	r7, sp
Ltmp134:
Ltmp135:
	bl	_p_47_plt_MonoTouch_UIKit_UITabBarController__ctor_intptr_llvm
	pop	{r7, pc}
Leh_func_end55:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillDisappear_bool:
Leh_func_begin56:
	push	{r4, r5, r7, lr}
Ltmp136:
	add	r7, sp, #8
Ltmp137:
	push	{r8}
Ltmp138:
	mov	r4, r1
	mov	r5, r0
	bl	_p_34_plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r5, #64]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC56_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC56_0+8))
LPC56_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end56:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidAppear_bool:
Leh_func_begin57:
	push	{r4, r5, r7, lr}
Ltmp139:
	add	r7, sp, #8
Ltmp140:
	push	{r8}
Ltmp141:
	mov	r4, r1
	mov	r5, r0
	bl	_p_36_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r5, #56]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC57_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC57_0+8))
LPC57_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end57:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillAppear_bool:
Leh_func_begin58:
	push	{r4, r5, r7, lr}
Ltmp142:
	add	r7, sp, #8
Ltmp143:
	push	{r8}
Ltmp144:
	mov	r4, r1
	mov	r5, r0
	bl	_p_37_plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r5, #52]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC58_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC58_0+8))
LPC58_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end58:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidDisappear_bool:
Leh_func_begin59:
	push	{r4, r5, r7, lr}
Ltmp145:
	add	r7, sp, #8
Ltmp146:
	push	{r8}
Ltmp147:
	mov	r4, r1
	mov	r5, r0
	bl	_p_38_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm
	ldr	r0, [r5, #60]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC59_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC59_0+8))
LPC59_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end59:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidLoad
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidLoad:
Leh_func_begin60:
	push	{r4, r7, lr}
Ltmp148:
	add	r7, sp, #4
Ltmp149:
Ltmp150:
	mov	r4, r0
	bl	_p_39_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	ldr	r0, [r4, #48]
	mov	r1, r4
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
	pop	{r4, r7, pc}
Leh_func_end60:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_Dispose_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_Dispose_bool:
Leh_func_begin61:
	push	{r4, r5, r7, lr}
Ltmp151:
	add	r7, sp, #8
Ltmp152:
Ltmp153:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB61_2
	ldr	r0, [r5, #68]
	mov	r1, r5
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
LBB61_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_48_plt_MonoTouch_UIKit_UITabBarController_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end61:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin62:
	push	{r4, r5, r6, r7, lr}
Ltmp154:
	add	r7, sp, #12
Ltmp155:
	push	{r8, r10, r11}
Ltmp156:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #48]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC62_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC62_2+8))
LPC62_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB62_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB62_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB62_6
LBB62_3:
	cmp	r5, #0
	beq	LBB62_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB62_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp157:
LBB62_6:
	ldr	r0, LCPI62_0
LPC62_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp158:
LBB62_7:
	ldr	r0, LCPI62_1
LPC62_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI62_0:
	.long	Ltmp157-(LPC62_0+8)
LCPI62_1:
	.long	Ltmp158-(LPC62_1+8)
	.end_data_region
Leh_func_end62:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin63:
	push	{r4, r5, r6, r7, lr}
Ltmp159:
	add	r7, sp, #12
Ltmp160:
	push	{r8, r10, r11}
Ltmp161:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #48]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC63_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC63_2+8))
LPC63_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB63_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB63_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB63_6
LBB63_3:
	cmp	r5, #0
	beq	LBB63_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB63_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp162:
LBB63_6:
	ldr	r0, LCPI63_0
LPC63_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp163:
LBB63_7:
	ldr	r0, LCPI63_1
LPC63_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI63_0:
	.long	Ltmp162-(LPC63_0+8)
LCPI63_1:
	.long	Ltmp163-(LPC63_1+8)
	.end_data_region
Leh_func_end63:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin64:
	push	{r4, r5, r6, r7, lr}
Ltmp164:
	add	r7, sp, #12
Ltmp165:
	push	{r8, r10, r11}
Ltmp166:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC64_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC64_2+8))
LPC64_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB64_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB64_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB64_6
LBB64_3:
	cmp	r5, #0
	beq	LBB64_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB64_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp167:
LBB64_6:
	ldr	r0, LCPI64_0
LPC64_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp168:
LBB64_7:
	ldr	r0, LCPI64_1
LPC64_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI64_0:
	.long	Ltmp167-(LPC64_0+8)
LCPI64_1:
	.long	Ltmp168-(LPC64_1+8)
	.end_data_region
Leh_func_end64:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin65:
	push	{r4, r5, r6, r7, lr}
Ltmp169:
	add	r7, sp, #12
Ltmp170:
	push	{r8, r10, r11}
Ltmp171:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC65_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC65_2+8))
LPC65_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB65_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB65_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB65_6
LBB65_3:
	cmp	r5, #0
	beq	LBB65_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB65_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp172:
LBB65_6:
	ldr	r0, LCPI65_0
LPC65_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp173:
LBB65_7:
	ldr	r0, LCPI65_1
LPC65_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI65_0:
	.long	Ltmp172-(LPC65_0+8)
LCPI65_1:
	.long	Ltmp173-(LPC65_1+8)
	.end_data_region
Leh_func_end65:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin66:
	push	{r4, r5, r6, r7, lr}
Ltmp174:
	add	r7, sp, #12
Ltmp175:
	push	{r8, r10, r11}
Ltmp176:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC66_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC66_2+8))
LPC66_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB66_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB66_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB66_6
LBB66_3:
	cmp	r5, #0
	beq	LBB66_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB66_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp177:
LBB66_6:
	ldr	r0, LCPI66_0
LPC66_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp178:
LBB66_7:
	ldr	r0, LCPI66_1
LPC66_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI66_0:
	.long	Ltmp177-(LPC66_0+8)
LCPI66_1:
	.long	Ltmp178-(LPC66_1+8)
	.end_data_region
Leh_func_end66:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin67:
	push	{r4, r5, r6, r7, lr}
Ltmp179:
	add	r7, sp, #12
Ltmp180:
	push	{r8, r10, r11}
Ltmp181:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC67_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC67_2+8))
LPC67_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB67_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB67_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB67_6
LBB67_3:
	cmp	r5, #0
	beq	LBB67_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB67_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp182:
LBB67_6:
	ldr	r0, LCPI67_0
LPC67_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp183:
LBB67_7:
	ldr	r0, LCPI67_1
LPC67_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI67_0:
	.long	Ltmp182-(LPC67_0+8)
LCPI67_1:
	.long	Ltmp183-(LPC67_1+8)
	.end_data_region
Leh_func_end67:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin68:
	push	{r4, r5, r6, r7, lr}
Ltmp184:
	add	r7, sp, #12
Ltmp185:
	push	{r8, r10, r11}
Ltmp186:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC68_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC68_2+8))
LPC68_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB68_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB68_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB68_6
LBB68_3:
	cmp	r5, #0
	beq	LBB68_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB68_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp187:
LBB68_6:
	ldr	r0, LCPI68_0
LPC68_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp188:
LBB68_7:
	ldr	r0, LCPI68_1
LPC68_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI68_0:
	.long	Ltmp187-(LPC68_0+8)
LCPI68_1:
	.long	Ltmp188-(LPC68_1+8)
	.end_data_region
Leh_func_end68:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin69:
	push	{r4, r5, r6, r7, lr}
Ltmp189:
	add	r7, sp, #12
Ltmp190:
	push	{r8, r10, r11}
Ltmp191:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC69_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC69_2+8))
LPC69_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB69_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB69_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB69_6
LBB69_3:
	cmp	r5, #0
	beq	LBB69_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB69_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp192:
LBB69_6:
	ldr	r0, LCPI69_0
LPC69_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp193:
LBB69_7:
	ldr	r0, LCPI69_1
LPC69_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI69_0:
	.long	Ltmp192-(LPC69_0+8)
LCPI69_1:
	.long	Ltmp193-(LPC69_1+8)
	.end_data_region
Leh_func_end69:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin70:
	push	{r4, r5, r6, r7, lr}
Ltmp194:
	add	r7, sp, #12
Ltmp195:
	push	{r8, r10, r11}
Ltmp196:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC70_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC70_2+8))
LPC70_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB70_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB70_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB70_6
LBB70_3:
	cmp	r5, #0
	beq	LBB70_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB70_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp197:
LBB70_6:
	ldr	r0, LCPI70_0
LPC70_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp198:
LBB70_7:
	ldr	r0, LCPI70_1
LPC70_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI70_0:
	.long	Ltmp197-(LPC70_0+8)
LCPI70_1:
	.long	Ltmp198-(LPC70_1+8)
	.end_data_region
Leh_func_end70:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin71:
	push	{r4, r5, r6, r7, lr}
Ltmp199:
	add	r7, sp, #12
Ltmp200:
	push	{r8, r10, r11}
Ltmp201:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC71_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC71_2+8))
LPC71_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB71_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB71_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB71_6
LBB71_3:
	cmp	r5, #0
	beq	LBB71_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB71_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp202:
LBB71_6:
	ldr	r0, LCPI71_0
LPC71_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp203:
LBB71_7:
	ldr	r0, LCPI71_1
LPC71_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI71_0:
	.long	Ltmp202-(LPC71_0+8)
LCPI71_1:
	.long	Ltmp203-(LPC71_1+8)
	.end_data_region
Leh_func_end71:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_DisposeCalled_System_EventHandler:
Leh_func_begin72:
	push	{r4, r5, r6, r7, lr}
Ltmp204:
	add	r7, sp, #12
Ltmp205:
	push	{r8, r10, r11}
Ltmp206:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC72_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC72_2+8))
LPC72_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB72_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB72_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB72_6
LBB72_3:
	cmp	r5, #0
	beq	LBB72_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB72_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp207:
LBB72_6:
	ldr	r0, LCPI72_0
LPC72_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp208:
LBB72_7:
	ldr	r0, LCPI72_1
LPC72_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI72_0:
	.long	Ltmp207-(LPC72_0+8)
LCPI72_1:
	.long	Ltmp208-(LPC72_1+8)
	.end_data_region
Leh_func_end72:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_DisposeCalled_System_EventHandler:
Leh_func_begin73:
	push	{r4, r5, r6, r7, lr}
Ltmp209:
	add	r7, sp, #12
Ltmp210:
	push	{r8, r10, r11}
Ltmp211:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC73_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC73_2+8))
LPC73_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB73_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB73_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB73_6
LBB73_3:
	cmp	r5, #0
	beq	LBB73_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB73_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp212:
LBB73_6:
	ldr	r0, LCPI73_0
LPC73_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp213:
LBB73_7:
	ldr	r0, LCPI73_1
LPC73_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI73_0:
	.long	Ltmp212-(LPC73_0+8)
LCPI73_1:
	.long	Ltmp213-(LPC73_1+8)
	.end_data_region
Leh_func_end73:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle:
Leh_func_begin74:
	push	{r7, lr}
Ltmp214:
	mov	r7, sp
Ltmp215:
Ltmp216:
	bl	_p_49_plt_MonoTouch_UIKit_UITableViewController__ctor_MonoTouch_UIKit_UITableViewStyle_llvm
	pop	{r7, pc}
Leh_func_end74:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr:
Leh_func_begin75:
	push	{r7, lr}
Ltmp217:
	mov	r7, sp
Ltmp218:
Ltmp219:
	bl	_p_50_plt_MonoTouch_UIKit_UITableViewController__ctor_intptr_llvm
	pop	{r7, pc}
Leh_func_end75:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin76:
	push	{r7, lr}
Ltmp220:
	mov	r7, sp
Ltmp221:
Ltmp222:
	bl	_p_51_plt_MonoTouch_UIKit_UITableViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	pop	{r7, pc}
Leh_func_end76:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillDisappear_bool:
Leh_func_begin77:
	push	{r4, r5, r7, lr}
Ltmp223:
	add	r7, sp, #8
Ltmp224:
	push	{r8}
Ltmp225:
	mov	r4, r1
	mov	r5, r0
	bl	_p_34_plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r5, #68]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC77_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC77_0+8))
LPC77_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end77:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidAppear_bool:
Leh_func_begin78:
	push	{r4, r5, r7, lr}
Ltmp226:
	add	r7, sp, #8
Ltmp227:
	push	{r8}
Ltmp228:
	mov	r4, r1
	mov	r5, r0
	bl	_p_36_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r5, #60]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC78_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC78_0+8))
LPC78_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end78:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillAppear_bool:
Leh_func_begin79:
	push	{r4, r5, r7, lr}
Ltmp229:
	add	r7, sp, #8
Ltmp230:
	push	{r8}
Ltmp231:
	mov	r4, r1
	mov	r5, r0
	bl	_p_37_plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r5, #56]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC79_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC79_0+8))
LPC79_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end79:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidDisappear_bool:
Leh_func_begin80:
	push	{r4, r5, r7, lr}
Ltmp232:
	add	r7, sp, #8
Ltmp233:
	push	{r8}
Ltmp234:
	mov	r4, r1
	mov	r5, r0
	bl	_p_38_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm
	ldr	r0, [r5, #64]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC80_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC80_0+8))
LPC80_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end80:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidLoad
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidLoad:
Leh_func_begin81:
	push	{r4, r7, lr}
Ltmp235:
	add	r7, sp, #4
Ltmp236:
Ltmp237:
	mov	r4, r0
	bl	_p_39_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	ldr	r0, [r4, #52]
	mov	r1, r4
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
	pop	{r4, r7, pc}
Leh_func_end81:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_Dispose_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_Dispose_bool:
Leh_func_begin82:
	push	{r4, r5, r7, lr}
Ltmp238:
	add	r7, sp, #8
Ltmp239:
Ltmp240:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB82_2
	ldr	r0, [r5, #72]
	mov	r1, r5
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
LBB82_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_52_plt_MonoTouch_UIKit_UITableViewController_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end82:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin83:
	push	{r4, r5, r6, r7, lr}
Ltmp241:
	add	r7, sp, #12
Ltmp242:
	push	{r8, r10, r11}
Ltmp243:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC83_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC83_2+8))
LPC83_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB83_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB83_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB83_6
LBB83_3:
	cmp	r5, #0
	beq	LBB83_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB83_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp244:
LBB83_6:
	ldr	r0, LCPI83_0
LPC83_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp245:
LBB83_7:
	ldr	r0, LCPI83_1
LPC83_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI83_0:
	.long	Ltmp244-(LPC83_0+8)
LCPI83_1:
	.long	Ltmp245-(LPC83_1+8)
	.end_data_region
Leh_func_end83:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin84:
	push	{r4, r5, r6, r7, lr}
Ltmp246:
	add	r7, sp, #12
Ltmp247:
	push	{r8, r10, r11}
Ltmp248:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC84_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC84_2+8))
LPC84_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB84_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB84_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB84_6
LBB84_3:
	cmp	r5, #0
	beq	LBB84_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB84_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp249:
LBB84_6:
	ldr	r0, LCPI84_0
LPC84_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp250:
LBB84_7:
	ldr	r0, LCPI84_1
LPC84_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI84_0:
	.long	Ltmp249-(LPC84_0+8)
LCPI84_1:
	.long	Ltmp250-(LPC84_1+8)
	.end_data_region
Leh_func_end84:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin85:
	push	{r4, r5, r6, r7, lr}
Ltmp251:
	add	r7, sp, #12
Ltmp252:
	push	{r8, r10, r11}
Ltmp253:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC85_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC85_2+8))
LPC85_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB85_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB85_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB85_6
LBB85_3:
	cmp	r5, #0
	beq	LBB85_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB85_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp254:
LBB85_6:
	ldr	r0, LCPI85_0
LPC85_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp255:
LBB85_7:
	ldr	r0, LCPI85_1
LPC85_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI85_0:
	.long	Ltmp254-(LPC85_0+8)
LCPI85_1:
	.long	Ltmp255-(LPC85_1+8)
	.end_data_region
Leh_func_end85:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin86:
	push	{r4, r5, r6, r7, lr}
Ltmp256:
	add	r7, sp, #12
Ltmp257:
	push	{r8, r10, r11}
Ltmp258:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC86_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC86_2+8))
LPC86_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB86_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB86_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB86_6
LBB86_3:
	cmp	r5, #0
	beq	LBB86_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB86_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp259:
LBB86_6:
	ldr	r0, LCPI86_0
LPC86_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp260:
LBB86_7:
	ldr	r0, LCPI86_1
LPC86_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI86_0:
	.long	Ltmp259-(LPC86_0+8)
LCPI86_1:
	.long	Ltmp260-(LPC86_1+8)
	.end_data_region
Leh_func_end86:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin87:
	push	{r4, r5, r6, r7, lr}
Ltmp261:
	add	r7, sp, #12
Ltmp262:
	push	{r8, r10, r11}
Ltmp263:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC87_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC87_2+8))
LPC87_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB87_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB87_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB87_6
LBB87_3:
	cmp	r5, #0
	beq	LBB87_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB87_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp264:
LBB87_6:
	ldr	r0, LCPI87_0
LPC87_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp265:
LBB87_7:
	ldr	r0, LCPI87_1
LPC87_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI87_0:
	.long	Ltmp264-(LPC87_0+8)
LCPI87_1:
	.long	Ltmp265-(LPC87_1+8)
	.end_data_region
Leh_func_end87:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin88:
	push	{r4, r5, r6, r7, lr}
Ltmp266:
	add	r7, sp, #12
Ltmp267:
	push	{r8, r10, r11}
Ltmp268:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC88_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC88_2+8))
LPC88_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB88_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB88_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB88_6
LBB88_3:
	cmp	r5, #0
	beq	LBB88_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB88_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp269:
LBB88_6:
	ldr	r0, LCPI88_0
LPC88_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp270:
LBB88_7:
	ldr	r0, LCPI88_1
LPC88_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI88_0:
	.long	Ltmp269-(LPC88_0+8)
LCPI88_1:
	.long	Ltmp270-(LPC88_1+8)
	.end_data_region
Leh_func_end88:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin89:
	push	{r4, r5, r6, r7, lr}
Ltmp271:
	add	r7, sp, #12
Ltmp272:
	push	{r8, r10, r11}
Ltmp273:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC89_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC89_2+8))
LPC89_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB89_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB89_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB89_6
LBB89_3:
	cmp	r5, #0
	beq	LBB89_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB89_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp274:
LBB89_6:
	ldr	r0, LCPI89_0
LPC89_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp275:
LBB89_7:
	ldr	r0, LCPI89_1
LPC89_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI89_0:
	.long	Ltmp274-(LPC89_0+8)
LCPI89_1:
	.long	Ltmp275-(LPC89_1+8)
	.end_data_region
Leh_func_end89:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin90:
	push	{r4, r5, r6, r7, lr}
Ltmp276:
	add	r7, sp, #12
Ltmp277:
	push	{r8, r10, r11}
Ltmp278:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC90_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC90_2+8))
LPC90_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB90_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB90_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB90_6
LBB90_3:
	cmp	r5, #0
	beq	LBB90_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB90_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp279:
LBB90_6:
	ldr	r0, LCPI90_0
LPC90_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp280:
LBB90_7:
	ldr	r0, LCPI90_1
LPC90_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI90_0:
	.long	Ltmp279-(LPC90_0+8)
LCPI90_1:
	.long	Ltmp280-(LPC90_1+8)
	.end_data_region
Leh_func_end90:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin91:
	push	{r4, r5, r6, r7, lr}
Ltmp281:
	add	r7, sp, #12
Ltmp282:
	push	{r8, r10, r11}
Ltmp283:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC91_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC91_2+8))
LPC91_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB91_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB91_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB91_6
LBB91_3:
	cmp	r5, #0
	beq	LBB91_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB91_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp284:
LBB91_6:
	ldr	r0, LCPI91_0
LPC91_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp285:
LBB91_7:
	ldr	r0, LCPI91_1
LPC91_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI91_0:
	.long	Ltmp284-(LPC91_0+8)
LCPI91_1:
	.long	Ltmp285-(LPC91_1+8)
	.end_data_region
Leh_func_end91:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin92:
	push	{r4, r5, r6, r7, lr}
Ltmp286:
	add	r7, sp, #12
Ltmp287:
	push	{r8, r10, r11}
Ltmp288:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC92_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC92_2+8))
LPC92_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB92_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB92_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB92_6
LBB92_3:
	cmp	r5, #0
	beq	LBB92_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB92_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp289:
LBB92_6:
	ldr	r0, LCPI92_0
LPC92_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp290:
LBB92_7:
	ldr	r0, LCPI92_1
LPC92_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI92_0:
	.long	Ltmp289-(LPC92_0+8)
LCPI92_1:
	.long	Ltmp290-(LPC92_1+8)
	.end_data_region
Leh_func_end92:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_DisposeCalled_System_EventHandler:
Leh_func_begin93:
	push	{r4, r5, r6, r7, lr}
Ltmp291:
	add	r7, sp, #12
Ltmp292:
	push	{r8, r10, r11}
Ltmp293:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #72]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC93_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC93_2+8))
LPC93_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB93_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB93_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB93_6
LBB93_3:
	cmp	r5, #0
	beq	LBB93_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB93_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp294:
LBB93_6:
	ldr	r0, LCPI93_0
LPC93_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp295:
LBB93_7:
	ldr	r0, LCPI93_1
LPC93_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI93_0:
	.long	Ltmp294-(LPC93_0+8)
LCPI93_1:
	.long	Ltmp295-(LPC93_1+8)
	.end_data_region
Leh_func_end93:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_DisposeCalled_System_EventHandler:
Leh_func_begin94:
	push	{r4, r5, r6, r7, lr}
Ltmp296:
	add	r7, sp, #12
Ltmp297:
	push	{r8, r10, r11}
Ltmp298:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #72]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC94_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC94_2+8))
LPC94_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB94_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB94_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB94_6
LBB94_3:
	cmp	r5, #0
	beq	LBB94_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB94_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp299:
LBB94_6:
	ldr	r0, LCPI94_0
LPC94_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp300:
LBB94_7:
	ldr	r0, LCPI94_1
LPC94_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI94_0:
	.long	Ltmp299-(LPC94_0+8)
LCPI94_1:
	.long	Ltmp300-(LPC94_1+8)
	.end_data_region
Leh_func_end94:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor:
Leh_func_begin95:
	push	{r7, lr}
Ltmp301:
	mov	r7, sp
Ltmp302:
Ltmp303:
	bl	_p_53_plt_MonoTouch_UIKit_UIViewController__ctor_llvm
	pop	{r7, pc}
Leh_func_end95:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr:
Leh_func_begin96:
	push	{r7, lr}
Ltmp304:
	mov	r7, sp
Ltmp305:
Ltmp306:
	bl	_p_54_plt_MonoTouch_UIKit_UIViewController__ctor_intptr_llvm
	pop	{r7, pc}
Leh_func_end96:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin97:
	push	{r7, lr}
Ltmp307:
	mov	r7, sp
Ltmp308:
Ltmp309:
	bl	_p_55_plt_MonoTouch_UIKit_UIViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	pop	{r7, pc}
Leh_func_end97:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool:
Leh_func_begin98:
	push	{r4, r5, r7, lr}
Ltmp310:
	add	r7, sp, #8
Ltmp311:
	push	{r8}
Ltmp312:
	mov	r4, r1
	mov	r5, r0
	bl	_p_34_plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool_llvm
	ldr	r0, [r5, #64]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC98_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC98_0+8))
LPC98_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end98:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool:
Leh_func_begin99:
	push	{r4, r5, r7, lr}
Ltmp313:
	add	r7, sp, #8
Ltmp314:
	push	{r8}
Ltmp315:
	mov	r4, r1
	mov	r5, r0
	bl	_p_36_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm
	ldr	r0, [r5, #56]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC99_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC99_0+8))
LPC99_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end99:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool:
Leh_func_begin100:
	push	{r4, r5, r7, lr}
Ltmp316:
	add	r7, sp, #8
Ltmp317:
	push	{r8}
Ltmp318:
	mov	r4, r1
	mov	r5, r0
	bl	_p_37_plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool_llvm
	ldr	r0, [r5, #52]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC100_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC100_0+8))
LPC100_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end100:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool:
Leh_func_begin101:
	push	{r4, r5, r7, lr}
Ltmp319:
	add	r7, sp, #8
Ltmp320:
	push	{r8}
Ltmp321:
	mov	r4, r1
	mov	r5, r0
	bl	_p_38_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm
	ldr	r0, [r5, #60]
	movw	r1, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC101_0+8))
	mov	r2, r4
	movt	r1, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC101_0+8))
LPC101_0:
	add	r1, pc, r1
	ldr	r1, [r1, #152]
	mov	r8, r1
	mov	r1, r5
	bl	_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end101:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad:
Leh_func_begin102:
	push	{r4, r7, lr}
Ltmp322:
	add	r7, sp, #4
Ltmp323:
Ltmp324:
	mov	r4, r0
	bl	_p_39_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm
	ldr	r0, [r4, #48]
	mov	r1, r4
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
	pop	{r4, r7, pc}
Leh_func_end102:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_Dispose_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_Dispose_bool:
Leh_func_begin103:
	push	{r4, r5, r7, lr}
Ltmp325:
	add	r7, sp, #8
Ltmp326:
Ltmp327:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB103_2
	ldr	r0, [r5, #68]
	mov	r1, r5
	bl	_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm
LBB103_2:
	mov	r0, r5
	mov	r1, r4
	bl	_p_56_plt_MonoTouch_UIKit_UIViewController_Dispose_bool_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end103:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin104:
	push	{r4, r5, r6, r7, lr}
Ltmp328:
	add	r7, sp, #12
Ltmp329:
	push	{r8, r10, r11}
Ltmp330:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #48]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC104_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC104_2+8))
LPC104_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB104_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB104_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB104_6
LBB104_3:
	cmp	r5, #0
	beq	LBB104_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB104_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp331:
LBB104_6:
	ldr	r0, LCPI104_0
LPC104_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp332:
LBB104_7:
	ldr	r0, LCPI104_1
LPC104_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI104_0:
	.long	Ltmp331-(LPC104_0+8)
LCPI104_1:
	.long	Ltmp332-(LPC104_1+8)
	.end_data_region
Leh_func_end104:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidLoadCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidLoadCalled_System_EventHandler:
Leh_func_begin105:
	push	{r4, r5, r6, r7, lr}
Ltmp333:
	add	r7, sp, #12
Ltmp334:
	push	{r8, r10, r11}
Ltmp335:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #48]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC105_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC105_2+8))
LPC105_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB105_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB105_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB105_6
LBB105_3:
	cmp	r5, #0
	beq	LBB105_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB105_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp336:
LBB105_6:
	ldr	r0, LCPI105_0
LPC105_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp337:
LBB105_7:
	ldr	r0, LCPI105_1
LPC105_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI105_0:
	.long	Ltmp336-(LPC105_0+8)
LCPI105_1:
	.long	Ltmp337-(LPC105_1+8)
	.end_data_region
Leh_func_end105:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin106:
	push	{r4, r5, r6, r7, lr}
Ltmp338:
	add	r7, sp, #12
Ltmp339:
	push	{r8, r10, r11}
Ltmp340:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC106_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC106_2+8))
LPC106_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB106_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB106_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB106_6
LBB106_3:
	cmp	r5, #0
	beq	LBB106_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB106_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp341:
LBB106_6:
	ldr	r0, LCPI106_0
LPC106_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp342:
LBB106_7:
	ldr	r0, LCPI106_1
LPC106_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI106_0:
	.long	Ltmp341-(LPC106_0+8)
LCPI106_1:
	.long	Ltmp342-(LPC106_1+8)
	.end_data_region
Leh_func_end106:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin107:
	push	{r4, r5, r6, r7, lr}
Ltmp343:
	add	r7, sp, #12
Ltmp344:
	push	{r8, r10, r11}
Ltmp345:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #52]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC107_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC107_2+8))
LPC107_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB107_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB107_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB107_6
LBB107_3:
	cmp	r5, #0
	beq	LBB107_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB107_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp346:
LBB107_6:
	ldr	r0, LCPI107_0
LPC107_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp347:
LBB107_7:
	ldr	r0, LCPI107_1
LPC107_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI107_0:
	.long	Ltmp346-(LPC107_0+8)
LCPI107_1:
	.long	Ltmp347-(LPC107_1+8)
	.end_data_region
Leh_func_end107:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin108:
	push	{r4, r5, r6, r7, lr}
Ltmp348:
	add	r7, sp, #12
Ltmp349:
	push	{r8, r10, r11}
Ltmp350:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC108_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC108_2+8))
LPC108_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB108_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB108_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB108_6
LBB108_3:
	cmp	r5, #0
	beq	LBB108_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB108_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp351:
LBB108_6:
	ldr	r0, LCPI108_0
LPC108_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp352:
LBB108_7:
	ldr	r0, LCPI108_1
LPC108_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI108_0:
	.long	Ltmp351-(LPC108_0+8)
LCPI108_1:
	.long	Ltmp352-(LPC108_1+8)
	.end_data_region
Leh_func_end108:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin109:
	push	{r4, r5, r6, r7, lr}
Ltmp353:
	add	r7, sp, #12
Ltmp354:
	push	{r8, r10, r11}
Ltmp355:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #56]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC109_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC109_2+8))
LPC109_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB109_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB109_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB109_6
LBB109_3:
	cmp	r5, #0
	beq	LBB109_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB109_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp356:
LBB109_6:
	ldr	r0, LCPI109_0
LPC109_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp357:
LBB109_7:
	ldr	r0, LCPI109_1
LPC109_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI109_0:
	.long	Ltmp356-(LPC109_0+8)
LCPI109_1:
	.long	Ltmp357-(LPC109_1+8)
	.end_data_region
Leh_func_end109:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin110:
	push	{r4, r5, r6, r7, lr}
Ltmp358:
	add	r7, sp, #12
Ltmp359:
	push	{r8, r10, r11}
Ltmp360:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC110_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC110_2+8))
LPC110_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB110_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB110_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB110_6
LBB110_3:
	cmp	r5, #0
	beq	LBB110_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB110_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp361:
LBB110_6:
	ldr	r0, LCPI110_0
LPC110_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp362:
LBB110_7:
	ldr	r0, LCPI110_1
LPC110_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI110_0:
	.long	Ltmp361-(LPC110_0+8)
LCPI110_1:
	.long	Ltmp362-(LPC110_1+8)
	.end_data_region
Leh_func_end110:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin111:
	push	{r4, r5, r6, r7, lr}
Ltmp363:
	add	r7, sp, #12
Ltmp364:
	push	{r8, r10, r11}
Ltmp365:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #60]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC111_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC111_2+8))
LPC111_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB111_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB111_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB111_6
LBB111_3:
	cmp	r5, #0
	beq	LBB111_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB111_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp366:
LBB111_6:
	ldr	r0, LCPI111_0
LPC111_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp367:
LBB111_7:
	ldr	r0, LCPI111_1
LPC111_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI111_0:
	.long	Ltmp366-(LPC111_0+8)
LCPI111_1:
	.long	Ltmp367-(LPC111_1+8)
	.end_data_region
Leh_func_end111:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin112:
	push	{r4, r5, r6, r7, lr}
Ltmp368:
	add	r7, sp, #12
Ltmp369:
	push	{r8, r10, r11}
Ltmp370:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC112_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC112_2+8))
LPC112_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB112_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB112_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB112_6
LBB112_3:
	cmp	r5, #0
	beq	LBB112_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB112_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp371:
LBB112_6:
	ldr	r0, LCPI112_0
LPC112_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp372:
LBB112_7:
	ldr	r0, LCPI112_1
LPC112_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI112_0:
	.long	Ltmp371-(LPC112_0+8)
LCPI112_1:
	.long	Ltmp372-(LPC112_1+8)
	.end_data_region
Leh_func_end112:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin113:
	push	{r4, r5, r6, r7, lr}
Ltmp373:
	add	r7, sp, #12
Ltmp374:
	push	{r8, r10, r11}
Ltmp375:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #64]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC113_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC113_2+8))
LPC113_2:
	add	r0, pc, r0
	ldr	r11, [r0, #164]
	ldr	r10, [r0, #168]
LBB113_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB113_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB113_6
LBB113_3:
	cmp	r5, #0
	beq	LBB113_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB113_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp376:
LBB113_6:
	ldr	r0, LCPI113_0
LPC113_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp377:
LBB113_7:
	ldr	r0, LCPI113_1
LPC113_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI113_0:
	.long	Ltmp376-(LPC113_0+8)
LCPI113_1:
	.long	Ltmp377-(LPC113_1+8)
	.end_data_region
Leh_func_end113:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_DisposeCalled_System_EventHandler:
Leh_func_begin114:
	push	{r4, r5, r6, r7, lr}
Ltmp378:
	add	r7, sp, #12
Ltmp379:
	push	{r8, r10, r11}
Ltmp380:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC114_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC114_2+8))
LPC114_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB114_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB114_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB114_6
LBB114_3:
	cmp	r5, #0
	beq	LBB114_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB114_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp381:
LBB114_6:
	ldr	r0, LCPI114_0
LPC114_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp382:
LBB114_7:
	ldr	r0, LCPI114_1
LPC114_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI114_0:
	.long	Ltmp381-(LPC114_0+8)
LCPI114_1:
	.long	Ltmp382-(LPC114_1+8)
	.end_data_region
Leh_func_end114:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_DisposeCalled_System_EventHandler
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_DisposeCalled_System_EventHandler:
Leh_func_begin115:
	push	{r4, r5, r6, r7, lr}
Ltmp383:
	add	r7, sp, #12
Ltmp384:
	push	{r8, r10, r11}
Ltmp385:
	push	{r1}
	mov	r5, r0
	mov	r6, r5
	ldr	r4, [r6, #68]!
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC115_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC115_2+8))
LPC115_2:
	add	r0, pc, r0
	ldr	r11, [r0, #156]
	ldr	r10, [r0, #160]
LBB115_1:
	ldr	r1, [sp]
	mov	r0, r4
	bl	_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB115_3
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB115_6
LBB115_3:
	cmp	r5, #0
	beq	LBB115_7
	mov	r8, r11
	mov	r0, r6
	mov	r2, r4
	bl	_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm
	cmp	r0, r4
	mov	r4, r0
	bne	LBB115_1
	sub	sp, r7, #24
	pop	{r8, r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp386:
LBB115_6:
	ldr	r0, LCPI115_0
LPC115_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp387:
LBB115_7:
	ldr	r0, LCPI115_1
LPC115_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI115_0:
	.long	Ltmp386-(LPC115_0+8)
LCPI115_1:
	.long	Ltmp387-(LPC115_1+8)
	.end_data_region
Leh_func_end115:

	.private_extern	_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.align	2
_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
Leh_func_begin116:
	push	{r4, r5, r6, r7, lr}
Ltmp388:
	add	r7, sp, #12
Ltmp389:
Ltmp390:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC116_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC116_0+8))
LPC116_0:
	add	r0, pc, r0
	ldr	r0, [r0, #172]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB116_2
	bl	_p_57_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB116_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB116_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB116_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB116_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB116_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end116:

	.private_extern	_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_Func_2_string_int_invoke_TResult__this___T_string
	.align	2
_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_Func_2_string_int_invoke_TResult__this___T_string:
Leh_func_begin117:
	push	{r4, r5, r7, lr}
Ltmp391:
	add	r7, sp, #8
Ltmp392:
Ltmp393:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC117_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC117_0+8))
LPC117_0:
	add	r0, pc, r0
	ldr	r0, [r0, #172]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB117_2
	bl	_p_57_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB117_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB117_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB117_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB117_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB117_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end117:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool:
Leh_func_begin118:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end118:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value:
Leh_func_begin119:
	ldrb	r0, [r0, #8]
	bx	lr
Leh_func_end119:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool:
Leh_func_begin120:
	strb	r1, [r0, #8]
	bx	lr
Leh_func_end120:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current:
Leh_func_begin121:
	push	{r4, r5, r7, lr}
Ltmp394:
	add	r7, sp, #8
Ltmp395:
	push	{r8}
Ltmp396:
	movw	r5, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC121_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC121_0+8))
LPC121_0:
	add	r5, pc, r5
	ldr	r1, [r5, #216]
	mov	r8, r1
	bl	_p_59_plt_System_Array_InternalEnumerator_1_int_get_Current_llvm
	mov	r4, r0
	ldr	r0, [r5, #32]
	bl	_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	str	r4, [r0, #8]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end121:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array:
Leh_func_begin122:
	mov	r2, r1
	mvn	r3, #1
	strd	r2, r3, [r0]
	bx	lr
Leh_func_end122:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current:
Leh_func_begin123:
	push	{r7, lr}
Ltmp397:
	mov	r7, sp
Ltmp398:
	push	{r8}
Ltmp399:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	beq	LBB123_3
	ldr	r1, [r0, #4]
	cmn	r1, #1
	beq	LBB123_4
	ldr	r2, [r0]
	ldr	r1, [r0]
	ldr	r1, [r1, #12]
	ldr	r9, [r0, #4]
	movw	r3, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC123_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC123_0+8))
	ldr	r0, [r2]
LPC123_0:
	add	r3, pc, r3
	mov	r0, r2
	ldr	r3, [r3, #220]
	sub	r1, r1, #1
	sub	r1, r1, r9
	mov	r8, r3
	bl	_p_60_plt_System_Array_InternalArray__get_Item_int_int_llvm
	pop	{r8}
	pop	{r7, pc}
LBB123_3:
	movw	r0, #47632
	b	LBB123_5
LBB123_4:
	movw	r0, #47718
LBB123_5:
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end123:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose:
Leh_func_begin124:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end124:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext:
Leh_func_begin125:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	ldreq	r1, [r0]
	ldreq	r1, [r1, #12]
	streq	r1, [r0, #4]
	mov	r1, #0
	ldr	r2, [r0, #4]
	cmn	r2, #1
	beq	LBB125_2
	ldr	r1, [r0, #4]
	cmp	r1, #0
	sub	r2, r1, #1
	movne	r1, #1
	str	r2, [r0, #4]
LBB125_2:
	mov	r0, r1
	bx	lr
Leh_func_end125:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset:
Leh_func_begin126:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end126:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_int:
Leh_func_begin127:
	push	{r4, r7, lr}
Ltmp400:
	add	r7, sp, #4
Ltmp401:
	push	{r8}
Ltmp402:
	sub	sp, sp, #16
	bic	sp, sp, #7
	movw	r4, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC127_0+8))
	mov	r1, r0
	mov	r0, #0
	movt	r4, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC127_0+8))
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
LPC127_0:
	add	r4, pc, r4
	ldr	r2, [r4, #216]
	mov	r8, r2
	bl	_p_62_plt_System_Array_InternalEnumerator_1_int__ctor_System_Array_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	ldr	r0, [r4, #216]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end127:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_Count
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_Count:
Leh_func_begin128:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end128:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly:
Leh_func_begin129:
	mov	r0, #1
	bx	lr
Leh_func_end129:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Clear
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Clear:
Leh_func_begin130:
	push	{r7, lr}
Ltmp403:
	mov	r7, sp
Ltmp404:
Ltmp405:
	movw	r0, #45355
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end130:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Add_int_int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Add_int_int:
Leh_func_begin131:
	push	{r7, lr}
Ltmp406:
	mov	r7, sp
Ltmp407:
Ltmp408:
	movw	r0, #45403
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end131:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Remove_int_int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Remove_int_int:
Leh_func_begin132:
	push	{r7, lr}
Ltmp409:
	mov	r7, sp
Ltmp410:
Ltmp411:
	movw	r0, #45403
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #629
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end132:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Contains_int_int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Contains_int_int:
Leh_func_begin133:
	push	{r4, r5, r6, r7, lr}
Ltmp412:
	add	r7, sp, #12
Ltmp413:
	push	{r10, r11}
Ltmp414:
	sub	sp, sp, #4
	mov	r2, r0
	str	r1, [sp]
	ldr	r0, [r2]
	ldrb	r0, [r0, #22]
	cmp	r0, #1
	bhi	LBB133_6
	ldr	r11, [r2, #12]
	mov	r0, #0
	cmp	r11, #1
	blt	LBB133_5
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC133_0+8))
	add	r5, r2, #16
	mov	r6, #0
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC133_0+8))
LPC133_0:
	add	r0, pc, r0
	ldr	r4, [r0, #32]
LBB133_3:
	ldr	r10, [r5]
	mov	r0, r4
	bl	_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r1, r0
	mov	r0, sp
	str	r10, [r1, #8]
	bl	_p_63_plt_int_Equals_object_llvm
	mov	r1, r0
	mov	r0, #1
	tst	r1, #255
	bne	LBB133_5
	add	r6, r6, #1
	add	r5, r5, #4
	mov	r0, #0
	cmp	r6, r11
	blt	LBB133_3
LBB133_5:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB133_6:
	movw	r0, #45267
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #646
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end133:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_CopyTo_int_int___int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_CopyTo_int_int___int:
Leh_func_begin134:
	push	{r4, r7, lr}
Ltmp415:
	add	r7, sp, #4
Ltmp416:
Ltmp417:
	sub	sp, sp, #4
	mov	r9, r2
	mov	r2, r1
	cmp	r2, #0
	beq	LBB134_6
	ldr	r1, [r0]
	ldrb	r1, [r1, #22]
	cmp	r1, #2
	bhs	LBB134_8
	ldr	r1, [r0, #8]
	cmp	r1, #0
	addeq	r1, r0, #12
	ldr	r4, [r1]
	ldr	r3, [r2, #8]
	mov	r1, #0
	cmp	r3, #0
	add	r4, r4, r9
	ldrne	r1, [r3, #4]
	ldr	r3, [r2, #8]
	cmp	r3, #0
	addeq	r3, r2, #12
	ldr	r3, [r3]
	add	r1, r3, r1
	cmp	r4, r1
	bgt	LBB134_7
	ldr	r1, [r2]
	ldrb	r1, [r1, #22]
	cmp	r1, #2
	bhs	LBB134_8
	cmp	r9, #0
	blt	LBB134_10
	ldr	r3, [r0, #8]
	mov	r1, #0
	cmp	r3, #0
	ldrne	r1, [r3, #4]
	ldr	r3, [r0, #8]
	cmp	r3, #0
	addeq	r3, r0, #12
	ldr	r3, [r3]
	str	r3, [sp]
	mov	r3, r9
	bl	_p_64_plt_System_Array_Copy_System_Array_int_System_Array_int_int_llvm
	sub	sp, r7, #4
	pop	{r4, r7, pc}
LBB134_6:
	movw	r0, #11691
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #519
	b	LBB134_9
LBB134_7:
	movw	r0, #45463
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #518
	b	LBB134_9
LBB134_8:
	movw	r0, #45267
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #646
LBB134_9:
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
LBB134_10:
	movw	r0, #11703
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r4, r0
	movw	r0, #45658
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r2, r0
	movw	r0, #520
	mov	r1, r4
	movt	r0, #512
	bl	_p_65_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end134:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int:
Leh_func_begin135:
	push	{r4, r5, r7, lr}
Ltmp418:
	add	r7, sp, #8
Ltmp419:
	push	{r8}
Ltmp420:
	mov	r4, r1
	mov	r5, r0
	bl	_p_66_plt_System_Linq_Check_SourceAndSelector_object_object_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC135_0+8))
	mov	r1, r4
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC135_0+8))
LPC135_0:
	add	r0, pc, r0
	ldr	r0, [r0, #228]
	mov	r8, r0
	mov	r0, r5
	bl	_p_67_plt_System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end135:

	.private_extern	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
	.align	2
_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool:
Leh_func_begin136:
	push	{r4, r5, r6, r7, lr}
Ltmp421:
	add	r7, sp, #12
Ltmp422:
Ltmp423:
	mov	r5, r0
	mov	r6, r2
	mov	r4, r1
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC136_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC136_0+8))
LPC136_0:
	add	r0, pc, r0
	ldr	r0, [r0, #232]
	bl	_p_68_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r2, r0
	mov	r0, r5
	mov	r1, r4
	strb	r6, [r2, #8]
	ldr	r3, [r5, #12]
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end136:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_Resize_int_int____int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_Resize_int_int____int:
Leh_func_begin137:
	push	{r4, r5, r6, r7, lr}
Ltmp424:
	add	r7, sp, #12
Ltmp425:
	push	{r10, r11}
Ltmp426:
	sub	sp, sp, #4
	mov	r6, r1
	mov	r10, r0
	cmp	r6, #0
	blt	LBB137_11
	ldr	r4, [r10]
	cmp	r4, #0
	beq	LBB137_5
	ldr	r5, [r4, #12]
	cmp	r5, r6
	beq	LBB137_10
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC137_0+8))
	mov	r1, r6
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC137_0+8))
LPC137_0:
	add	r0, pc, r0
	ldr	r0, [r0, #196]
	bl	_p_3_plt__jit_icall_mono_array_new_specific_llvm
	mov	r11, r0
	cmp	r5, r6
	mov	r0, r5
	movge	r0, r6
	cmp	r0, #8
	ble	LBB137_6
	str	r0, [sp]
	mov	r0, r4
	mov	r1, #0
	mov	r2, r11
	mov	r3, #0
	bl	_p_69_plt_System_Array_FastCopy_System_Array_int_System_Array_int_int_llvm
	b	LBB137_9
LBB137_5:
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC137_1+8))
	mov	r1, r6
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC137_1+8))
LPC137_1:
	add	r0, pc, r0
	ldr	r0, [r0, #196]
	bl	_p_3_plt__jit_icall_mono_array_new_specific_llvm
	str	r0, [r10]
	b	LBB137_10
LBB137_6:
	cmp	r0, #1
	blt	LBB137_9
	mvn	r0, r5
	mvn	r1, r6
	add	r2, r11, #16
	cmp	r0, r1
	movgt	r1, r0
	mvn	r0, r1
	add	r1, r4, #16
LBB137_8:
	ldr	r3, [r1], #4
	subs	r0, r0, #1
	str	r3, [r2], #4
	bne	LBB137_8
LBB137_9:
	str	r11, [r10]
LBB137_10:
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB137_11:
	movw	r0, #47356
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end137:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_EmptyOf_1_int__cctor
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable_EmptyOf_1_int__cctor:
Leh_func_begin138:
	push	{r4, r7, lr}
Ltmp427:
	add	r7, sp, #4
Ltmp428:
Ltmp429:
	movw	r4, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC138_0+8))
	mov	r1, #0
	movt	r4, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC138_0+8))
LPC138_0:
	add	r4, pc, r4
	ldr	r0, [r4, #196]
	bl	_p_3_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r1, [r4, #176]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end138:

	.private_extern	_Cirrious_CrossCore_Touch__System_Array_InternalArray__get_Item_int_int
	.align	2
_Cirrious_CrossCore_Touch__System_Array_InternalArray__get_Item_int_int:
Leh_func_begin139:
	push	{r7, lr}
Ltmp430:
	mov	r7, sp
Ltmp431:
Ltmp432:
	ldr	r2, [r0, #12]
	cmp	r2, r1
	addhi	r0, r0, r1, lsl #2
	ldrhi	r0, [r0, #16]
	pophi	{r7, pc}
	movw	r0, #11703
	bl	_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end139:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int:
Leh_func_begin140:
	push	{r4, r5, r7, lr}
Ltmp433:
	add	r7, sp, #8
Ltmp434:
Ltmp435:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC140_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC140_0+8))
LPC140_0:
	add	r0, pc, r0
	ldr	r0, [r0, #236]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	mvn	r1, #1
	str	r5, [r0, #8]
	str	r4, [r0, #20]
	str	r1, [r0, #32]
	pop	{r4, r5, r7, pc}
Leh_func_end140:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerator_TResult_get_Current
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerator_TResult_get_Current:
Leh_func_begin141:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end141:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerator_get_Current
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerator_get_Current:
Leh_func_begin142:
	push	{r4, r7, lr}
Ltmp436:
	add	r7, sp, #4
Ltmp437:
Ltmp438:
	ldr	r4, [r0, #24]
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC142_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC142_0+8))
LPC142_0:
	add	r0, pc, r0
	ldr	r0, [r0, #32]
	bl	_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	str	r4, [r0, #8]
	pop	{r4, r7, pc}
Leh_func_end142:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int__ctor
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int__ctor:
Leh_func_begin143:
	bx	lr
Leh_func_end143:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Reset
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Reset:
Leh_func_begin144:
	push	{r7, lr}
Ltmp439:
	mov	r7, sp
Ltmp440:
Ltmp441:
	movw	r0, #629
	movt	r0, #512
	bl	_p_70_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_16_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end144:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerable_GetEnumerator
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerable_GetEnumerator:
Leh_func_begin145:
	push	{r7, lr}
Ltmp442:
	mov	r7, sp
Ltmp443:
Ltmp444:
	bl	_p_71_plt_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator_llvm
	pop	{r7, pc}
Leh_func_end145:

	.private_extern	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator
	.align	2
_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator:
Leh_func_begin146:
	push	{r4, r7, lr}
Ltmp445:
	add	r7, sp, #4
Ltmp446:
Ltmp447:
	mov	r4, r0
	cmp	r4, #0
	beq	LBB146_2
	add	r0, r4, #32
	mov	r1, #0
	mvn	r2, #1
	bl	_p_72_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm
	cmn	r0, #2
	moveq	r0, r4
	popeq	{r4, r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC146_1+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_CrossCore_Touch_got-(LPC146_1+8))
LPC146_1:
	add	r0, pc, r0
	ldr	r0, [r0, #236]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #8]
	str	r1, [r0, #8]
	ldr	r1, [r4, #20]
	str	r1, [r0, #20]
	pop	{r4, r7, pc}
Ltmp448:
LBB146_2:
	ldr	r0, LCPI146_0
LPC146_0:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI146_0:
	.long	Ltmp448-(LPC146_0+8)
	.end_data_region
Leh_func_end146:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_type_info_1,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_CrossCore_Touch_got,588,4
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_set_IsVersionOrHigher_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_get_Version
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_set_Version_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__BuildVersionb__0_string
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Major
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Major_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Minor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Minor_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Parts
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Parts_int__
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorFromInt_uint
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorWithAlphaFromInt_uint
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_IntFromColor_MonoTouch_UIKit_UIColor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToDateTimeUtc_MonoTouch_Foundation_NSDate
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods__cctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchUIViewControllerExtensions_IsVisible_MonoTouch_UIKit_UIViewController
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask_DoUrlOpen_MonoTouch_Foundation_NSUrl
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask__ctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_get_ViewController
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidLoad
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_Dispose_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidLoad
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_Dispose_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidLoad
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_Dispose_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_Dispose_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidLoadCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_DisposeCalled_System_EventHandler
	.no_dead_strip	_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_Func_2_string_int_invoke_TResult__this___T_string
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_Count
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Clear
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Add_int_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Remove_int_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Contains_int_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_CopyTo_int_int___int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_Resize_int_int____int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_EmptyOf_1_int__cctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Array_InternalArray__get_Item_int_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerator_TResult_get_Current
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int__ctor
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Reset
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerable_GetEnumerator
	.no_dead_strip	_Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator
	.no_dead_strip	_mono_aot_Cirrious_CrossCore_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	147
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	1
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	2
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	3
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	4
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	5
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	6
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	7
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	8
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	9
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	10
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	11
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	12
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	13
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	14
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	15
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	16
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	19
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	20
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	21
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	22
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	24
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	25
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	26
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	27
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	28
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	29
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	30
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	31
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	32
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	33
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	34
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	35
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	46
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	47
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	48
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	49
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	50
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	51
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	52
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	53
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	54
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	55
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	56
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	57
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	58
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	59
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	60
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	61
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	62
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	63
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	64
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	65
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	66
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	67
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	68
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	69
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	70
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	71
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	72
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	73
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	74
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	75
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	76
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	77
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	78
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	79
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	80
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	81
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	82
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	83
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	84
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	85
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	86
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	87
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	88
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	89
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	90
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	91
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	92
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	93
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	94
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	95
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	96
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	97
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	98
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	99
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	100
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	101
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	102
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	103
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	104
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	105
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	106
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	107
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	108
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	109
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	110
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	111
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	112
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	113
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	114
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	115
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	116
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	117
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	118
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	119
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	120
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	121
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	122
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	123
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	124
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	125
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	126
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	127
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	128
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	130
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	135
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	136
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	137
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	138
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	141
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	142
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	143
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	144
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	145
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	146
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	147
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	155
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	156
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	157
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	158
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	159
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	160
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	161
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	163
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	165
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
	.long	166
Lset137 = Lmono_eh_func_begin137-mono_eh_frame
	.long	Lset137
	.long	167
Lset138 = Lmono_eh_func_begin138-mono_eh_frame
	.long	Lset138
	.long	168
Lset139 = Lmono_eh_func_begin139-mono_eh_frame
	.long	Lset139
	.long	169
Lset140 = Lmono_eh_func_begin140-mono_eh_frame
	.long	Lset140
	.long	170
Lset141 = Lmono_eh_func_begin141-mono_eh_frame
	.long	Lset141
	.long	171
Lset142 = Lmono_eh_func_begin142-mono_eh_frame
	.long	Lset142
	.long	172
Lset143 = Lmono_eh_func_begin143-mono_eh_frame
	.long	Lset143
	.long	175
Lset144 = Lmono_eh_func_begin144-mono_eh_frame
	.long	Lset144
	.long	176
Lset145 = Lmono_eh_func_begin145-mono_eh_frame
	.long	Lset145
	.long	177
Lset146 = Lmono_eh_func_begin146-mono_eh_frame
	.long	Lset146
Lset147 = Leh_func_end146-Leh_func_begin146
	.long	Lset147
Lset148 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset148
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin5:
	.byte	0

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0

Lmono_eh_func_begin13:
	.byte	0

Lmono_eh_func_begin14:
	.byte	0

Lmono_eh_func_begin15:
	.byte	0

Lmono_eh_func_begin16:
	.byte	0

Lmono_eh_func_begin17:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin18:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin19:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin24:
	.byte	0

Lmono_eh_func_begin25:
	.byte	0

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin27:
	.byte	0

Lmono_eh_func_begin28:
	.byte	0

Lmono_eh_func_begin29:
	.byte	0

Lmono_eh_func_begin30:
	.byte	0

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin34:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin35:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin38:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin45:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin46:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin47:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin48:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin49:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin50:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin51:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin58:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin60:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin62:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin67:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin68:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin70:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin71:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin72:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin73:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin74:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin75:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin76:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin77:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin78:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin79:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin80:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin81:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin82:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin83:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin84:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin85:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin86:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin88:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin89:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin92:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin93:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin95:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin96:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin97:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin98:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin101:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin102:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin103:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin105:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin106:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin107:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin108:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin109:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin110:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin113:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin114:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin115:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin116:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin117:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin118:
	.byte	0

Lmono_eh_func_begin119:
	.byte	0

Lmono_eh_func_begin120:
	.byte	0

Lmono_eh_func_begin121:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin122:
	.byte	0

Lmono_eh_func_begin123:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin124:
	.byte	0

Lmono_eh_func_begin125:
	.byte	0

Lmono_eh_func_begin126:
	.byte	0

Lmono_eh_func_begin127:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin128:
	.byte	0

Lmono_eh_func_begin129:
	.byte	0

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin131:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin132:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin135:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin136:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin137:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin138:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin139:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin140:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin141:
	.byte	0

Lmono_eh_func_begin142:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin143:
	.byte	0

Lmono_eh_func_begin144:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin145:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin146:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.CrossCore.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime
_Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,32,0,139,229,36,16,139,229,0,0,160,227
	.byte 16,0,139,229,0,0,160,227,20,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 72
	.byte 0,0,159,231,0,16,144,229,24,16,139,229,4,0,144,229,28,0,139,229,16,0,139,226,32,16,155,229,36,32,155,229
	.byte 24,48,155,229,28,192,155,229,0,192,141,229
bl _p_75

	.byte 16,0,139,226
bl _p_74

	.byte 16,11,65,236,0,11,141,237,0,0,157,229,4,16,157,229
bl _p_73

	.byte 40,208,139,226,0,9,189,232,128,128,189,232

Lme_17:
.text
	.align 2
	.no_dead_strip _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0
_System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,40,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,139,229
	.byte 0,0,160,227,4,0,139,229,0,0,90,227,197,0,0,10,8,160,139,229,10,0,160,225,28,0,139,229,8,0,155,229
	.byte 0,0,80,227,22,0,0,10,28,0,155,229,0,96,144,229,180,1,214,225,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 196
	.byte 1,16,159,231,1,0,80,225,13,0,0,58,16,0,150,229,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 196
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,32,0,139,229,1,0,0,234,0,0,160,227,32,0,139,229,32,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,12,0,139,229,1,0,0,234,8,0,155,229,12,0,139,229,12,96,155,229
	.byte 6,0,160,225,0,0,80,227,45,0,0,10,6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 188
	.byte 8,128,159,231,4,224,143,226,76,240,17,229,0,0,0,0,0,0,80,227,5,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 164
	.byte 0,0,159,231,0,0,144,229,131,0,0,234,6,0,160,225,0,16,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 188
	.byte 8,128,159,231,4,224,143,226,76,240,17,229,0,0,0,0,0,16,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 184
	.byte 0,0,159,231
bl _p_3

	.byte 0,0,139,229,0,16,160,225,6,0,160,225,0,32,160,227,0,48,150,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 192
	.byte 8,128,159,231,4,224,143,226,32,240,19,229,0,0,0,0,0,0,155,229,102,0,0,234,0,80,160,227,0,0,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 164
	.byte 0,0,159,231,0,0,144,229,0,0,139,229,10,0,160,225,0,16,154,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 168
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,4,0,139,229,41,0,0,234,4,16,155,229,1,0,160,225
	.byte 0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 180
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,64,160,225,0,0,155,229,12,0,144,229,0,0,85,225
	.byte 16,0,0,26,0,0,85,227,7,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 184
	.byte 0,0,159,231,4,16,160,227
bl _p_3

	.byte 0,0,139,229,6,0,0,234,133,16,160,225,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 172
	.byte 8,128,159,231,11,0,160,225
bl _p_58

	.byte 0,0,155,229,5,16,160,225,1,80,133,226,12,32,144,229,1,0,82,225,60,0,0,155,1,17,160,225,1,0,128,224
	.byte 16,0,128,226,0,64,128,229,4,16,155,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 176
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,200,255,255,26,0,0,0,235
	.byte 15,0,0,234,24,224,139,229,4,0,155,229,0,0,80,227,9,0,0,10,4,16,155,229,1,0,160,225,0,16,145,229
	.byte 0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 236
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,24,192,155,229,12,240,160,225,0,0,155,229,12,0,144,229
	.byte 0,0,85,225,6,0,0,10,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 172
	.byte 8,128,159,231,11,0,160,225,5,16,160,225
bl _p_58

	.byte 0,0,155,229,40,208,139,226,112,13,189,232,128,128,189,232,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 200
	.byte 0,0,159,231,157,23,0,227
bl _p_14

	.byte 0,16,160,225,7,2,0,227,0,2,64,227
bl _mono_create_corlib_exception_1
bl _p_16

	.byte 14,16,160,225,0,0,159,229
bl _p_76

	.byte 88,2,0,2

Lme_8b:
.text
ut_141:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current

.text
ut_142:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array

.text
ut_143:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current

.text
ut_144:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose

.text
ut_145:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext

.text
ut_146:

	.byte 8,0,128,226
	b _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset

.text
	.align 2
	.no_dead_strip _System_Array_InternalArray__IEnumerable_GetEnumerator_T
_System_Array_InternalArray__IEnumerable_GetEnumerator_T:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,32,208,77,226,13,176,160,225,4,128,139,229,28,0,139,229,4,0,155,229
bl _p_80

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,4,0,155,229
bl _p_78

	.byte 0,16,160,225,0,0,160,227,12,0,139,229,0,0,160,227,16,0,139,229,12,0,139,226,1,128,160,225,28,16,155,229
bl _p_79

	.byte 12,0,155,229,20,0,139,229,16,0,155,229,24,0,139,229,4,0,155,229
bl _p_78
bl _p_77

	.byte 8,16,128,226,20,32,155,229,0,32,129,229,24,32,155,229,4,32,129,229,32,208,139,226,0,9,189,232,128,128,189,232

Lme_a4:
.text
	.align 2
	.no_dead_strip _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0
_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,44,208,77,226,13,176,160,225,20,0,139,229,0,0,160,227,0,0,203,229
	.byte 20,0,155,229,32,160,144,229,20,0,155,229,0,16,224,227,32,16,128,229,0,0,160,227,0,0,203,229,16,160,139,229
	.byte 2,0,90,227,120,0,0,42,16,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 244
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,20,0,155,229,24,0,139,229,20,0,155,229,8,16,144,229
	.byte 1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 228
	.byte 8,128,159,231,4,224,143,226,28,240,17,229,0,0,0,0,0,16,160,225,24,0,155,229,12,16,128,229,2,160,224,227
	.byte 1,160,74,226,1,0,90,227,7,0,0,42,10,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 240
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,40,0,0,234,20,0,155,229,32,0,139,229,20,0,155,229
	.byte 12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 232
	.byte 8,128,159,231,4,224,143,226,12,240,17,229,0,0,0,0,0,16,160,225,32,0,155,229,16,16,128,229,20,0,155,229
	.byte 24,0,139,229,20,0,155,229,20,32,144,229,20,0,155,229,16,16,144,229,2,0,160,225,28,32,139,229,15,224,160,225
	.byte 12,240,146,229,0,16,160,225,24,0,155,229,28,32,155,229,24,16,128,229,20,0,155,229,28,0,208,229,0,0,80,227
	.byte 2,0,0,26,20,0,155,229,1,16,160,227,32,16,128,229,1,0,160,227,0,0,203,229,16,0,0,235,43,0,0,234
	.byte 20,0,155,229,12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 176
	.byte 8,128,159,231,4,224,143,226,60,240,17,229,0,0,0,0,255,0,0,226,0,0,80,227,200,255,255,26,0,0,0,235
	.byte 22,0,0,234,12,224,139,229,0,0,219,229,0,0,80,227,1,0,0,10,12,192,155,229,12,240,160,225,20,0,155,229
	.byte 12,0,144,229,0,0,80,227,10,0,0,10,20,0,155,229,12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 236
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,12,192,155,229,12,240,160,225,20,0,155,229,0,16,224,227
	.byte 32,16,128,229,0,0,160,227,0,0,0,234,1,0,160,227,44,208,139,226,0,13,189,232,128,128,189,232

Lme_ad:
.text
	.align 2
	.no_dead_strip _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0
_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,16,0,139,229,16,0,155,229,32,0,144,229
	.byte 16,16,155,229,1,32,160,227,28,32,193,229,16,16,155,229,0,32,224,227,32,32,129,229,12,0,139,229,2,0,80,227
	.byte 28,0,0,42,12,0,155,229,0,17,160,225,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 248
	.byte 0,0,159,231,1,0,128,224,0,0,144,229,0,240,160,225,0,0,0,235,17,0,0,234,8,224,139,229,16,0,155,229
	.byte 12,0,144,229,0,0,80,227,10,0,0,10,16,0,155,229,12,16,144,229,1,0,160,225,0,16,145,229,0,128,159,229
	.byte 0,0,0,234
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 236
	.byte 8,128,159,231,4,224,143,226,20,240,17,229,0,0,0,0,8,192,155,229,12,240,160,225,24,208,139,226,0,9,189,232
	.byte 128,128,189,232

Lme_ae:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_set_IsVersionOrHigher_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_get_Version
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_set_Version_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__BuildVersionb__0_string
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Major
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Major_int
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Minor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Minor_int
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Parts
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Parts_int__
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorFromInt_uint
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorWithAlphaFromInt_uint
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_IntFromColor_MonoTouch_UIKit_UIColor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToDateTimeUtc_MonoTouch_Foundation_NSDate
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods__cctor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchUIViewControllerExtensions_IsVisible_MonoTouch_UIKit_UIViewController
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask_DoUrlOpen_MonoTouch_Foundation_NSUrl
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask__ctor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_get_ViewController
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidLoad
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_Dispose_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidLoad
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_Dispose_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidLoad
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_Dispose_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_Dispose_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidLoadCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_DisposeCalled_System_EventHandler
.no_dead_strip _Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
.no_dead_strip _Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_Func_2_string_int_invoke_TResult__this___T_string
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_Count
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Clear
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Add_int_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Remove_int_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Contains_int_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_CopyTo_int_int___int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
.no_dead_strip _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_Resize_int_int____int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable_EmptyOf_1_int__cctor
.no_dead_strip _Cirrious_CrossCore_Touch__System_Array_InternalArray__get_Item_int_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerator_TResult_get_Current
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerator_get_Current
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int__ctor
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Reset
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerable_GetEnumerator
.no_dead_strip _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_set_IsVersionOrHigher_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker__ctor_int_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_get_Version
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_set_Version_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__BuildVersionb__0_string
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Major
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Major_int
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Minor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Minor_int
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_get_Parts
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchVersion_set_Parts_int__
	bl method_addresses
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorFromInt_uint
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_ColorWithAlphaFromInt_uint
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchColorExtensionMethods_IntFromColor_MonoTouch_UIKit_UIColor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToDateTimeUtc_MonoTouch_Foundation_NSDate
	bl _Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods__cctor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_MvxTouchUIViewControllerExtensions_IsVisible_MonoTouch_UIKit_UIViewController
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask_DoUrlOpen_MonoTouch_Foundation_NSUrl
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxTouchTask__ctor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_get_ViewController
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewWillAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidDisappearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter_HandleViewDidAppearCalled_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewWillAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_ViewDidLoad
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_Dispose_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_add_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController_remove_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewWillAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_ViewDidLoad
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_Dispose_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_add_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController_remove_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewWillAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_ViewDidLoad
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_Dispose_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_add_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController_remove_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewWillAppear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidDisappear_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_ViewDidLoad
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_Dispose_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidLoadCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidAppearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewDidDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_ViewWillDisappearCalled_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_add_DisposeCalled_System_EventHandler
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController_remove_DisposeCalled_System_EventHandler
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_invoke_void__this___object_TEventArgs_object_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__wrapper_delegate_invoke_System_Func_2_string_int_invoke_TResult__this___T_string
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_bool
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_get_Value
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_set_Value_bool
	bl _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_get_Current
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int__ctor_System_Array
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_get_Current
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_Dispose
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_MoveNext
	bl _Cirrious_CrossCore_Touch__System_Array_InternalEnumerator_1_int_System_Collections_IEnumerator_Reset
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__IEnumerable_GetEnumerator_int
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_Count
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_get_IsReadOnly
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Clear
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Add_int_int
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Remove_int_int
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_Contains_int_int
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__ICollection_CopyTo_int_int___int
	bl method_addresses
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	bl _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	bl _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
	bl _Cirrious_CrossCore_Touch__System_Array_Resize_int_int____int
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable_EmptyOf_1_int__cctor
	bl _Cirrious_CrossCore_Touch__System_Array_InternalArray__get_Item_int_int
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerator_TResult_get_Current
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerator_get_Current
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int__ctor
	bl _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0
	bl _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Reset
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_IEnumerable_GetEnumerator
	bl _Cirrious_CrossCore_Touch__System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:

	.long 141

	bl ut_141

	.long 142

	bl ut_142

	.long 143

	bl ut_143

	.long 144

	bl ut_144

	.long 145

	bl ut_145

	.long 146

	bl ut_146
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 178,10,18,2
	.short 0, 10, 24, 34, 48, 58, 68, 79
	.short 90, 101, 112, 123, 134, 149, 165, 180
	.short 191, 207
	.byte 0,1,2,2,2,9,2,2,2,13,37,3,2,2,2,2,2,255,255,255,255,206,0,52,54,2,2,4,4,4,3,2
	.byte 2,3,101,2,2,2,2,2,255,255,255,255,145,0,0,0,0,0,0,0,0,0,113,2,2,2,122,3,3,3,2,2
	.byte 4,4,4,4,128,155,4,4,4,4,4,4,4,2,2,128,190,3,3,3,2,2,4,4,4,4,128,223,4,4,4,4
	.byte 4,4,4,2,2,129,1,3,3,3,3,2,2,4,4,4,129,33,4,4,4,4,4,4,4,4,2,129,69,2,3,3
	.byte 3,3,2,2,4,4,129,99,4,4,4,4,4,4,4,4,255,255,255,254,125,129,135,255,255,255,254,121,0,0,0,129
	.byte 138,3,2,2,2,0,129,165,4,2,3,2,2,2,255,255,255,254,76,0,0,0,0,0,0,129,184,2,2,2,2,129
	.byte 194,4,255,255,255,254,58,129,200,3,2,3,13,6,3,129,233,2,3,2,8,4,2,2
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 73,760,155,0,906,166,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,764,156,73,812,160,0
	.long 645,142,0,0,0,0,0,0
	.long 0,0,0,0,852,163,75,0
	.long 0,0,0,0,0,0,0,0
	.long 702,145,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,1116,175,0,0,0,0
	.long 0,0,0,1093,174,0,0,0
	.long 0,515,130,0,0,0,0,527
	.long 135,0,0,0,0,0,0,0
	.long 832,161,0,683,144,0,0,0
	.long 0,721,146,76,1070,173,0,0
	.long 0,0,1001,170,0,1162,177,0
	.long 0,0,0,768,157,0,0,0
	.long 0,0,0,0,567,137,0,965
	.long 169,0,0,0,0,0,0,0
	.long 664,143,0,0,0,0,0,0
	.long 0,0,0,0,605,139,0,945
	.long 168,0,0,0,0,0,0,0
	.long 876,164,0,0,0,0,0,0
	.long 0,1024,171,0,548,136,0,926
	.long 167,0,626,141,0,586,138,74
	.long 0,0,0,772,158,0,740,147
	.long 0,0,0,0,792,159,0,895
	.long 165,0,1047,172,0,1139,176,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 48,130,515,131,0,132,0,133
	.long 0,134,0,135,527,136,548,137
	.long 567,138,586,139,605,140,0,141
	.long 626,142,645,143,664,144,683,145
	.long 702,146,721,147,740,148,0,149
	.long 0,150,0,151,0,152,0,153
	.long 0,154,0,155,760,156,764,157
	.long 768,158,772,159,792,160,812,161
	.long 832,162,0,163,852,164,876,165
	.long 895,166,906,167,926,168,945,169
	.long 965,170,1001,171,1024,172,1047,173
	.long 1070,174,1093,175,1116,176,1139,177
	.long 1162
.section __TEXT, __const
	.align 3
class_name_table:

	.short 37, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 4
	.short 0, 0, 0, 0, 0, 5, 37, 0
	.short 0, 0, 0, 7, 0, 15, 0, 12
	.short 0, 10, 0, 3, 39, 0, 0, 0
	.short 0, 0, 0, 13, 0, 0, 0, 0
	.short 0, 8, 38, 0, 0, 0, 0, 0
	.short 0, 0, 0, 2, 40, 0, 0, 1
	.short 0, 0, 0, 0, 0, 0, 0, 14
	.short 0, 0, 0, 6, 0, 9, 0, 11
	.short 0, 16, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 66,10,7,2
	.short 0, 11, 22, 33, 44, 55, 66
	.byte 132,161,2,1,1,1,12,2,3,7,5,132,199,4,7,4,12,12,3,6,2,2,133,1,4,4,5,5,2,6,2,2
	.byte 2,133,35,2,2,2,2,5,5,2,2,12,133,81,5,12,6,1,14,20,21,14,20,133,210,20,20,4,2,4,21,21
	.byte 24,4,134,87,29,20,5,4,5
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 178,10,18,2
	.short 0, 11, 27, 38, 53, 64, 75, 86
	.short 97, 108, 119, 130, 141, 156, 172, 187
	.short 198, 214
	.byte 0,138,147,3,3,3,3,3,3,3,3,138,174,3,3,3,3,3,3,255,255,255,245,64,0,138,195,138,198,3,3,3
	.byte 3,3,3,3,3,3,138,228,3,3,3,3,3,255,255,255,245,13,0,0,0,0,0,0,0,0,0,138,246,3,3,3
	.byte 139,2,3,3,3,3,3,3,3,3,3,139,32,3,3,3,3,3,3,3,3,3,139,62,3,3,3,3,3,3,3,3
	.byte 3,139,92,3,3,3,3,3,3,3,3,3,139,122,3,3,3,3,3,3,3,3,3,139,152,3,3,3,3,3,3,3
	.byte 3,3,139,182,3,3,3,3,3,3,3,3,3,139,212,3,3,3,3,3,3,3,3,255,255,255,244,20,139,239,255,255
	.byte 255,244,17,0,0,0,139,242,3,3,3,3,0,140,12,3,3,3,3,3,3,255,255,255,243,226,0,0,0,0,0,0
	.byte 140,33,3,3,3,3,140,48,3,255,255,255,243,205,140,54,3,31,3,3,3,3,140,103,3,3,3,19,11,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11,31,12,13,0,72,14,8,135
	.byte 2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,72,68,13,11,23,12,13,0,72,14,8,135
	.byte 2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11,25,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4
	.byte 139,3,142,1,68,14,64,68,13,11,23,12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68
	.byte 13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 16,10,2,2
	.short 0, 11
	.byte 140,151,7,7,23,24,23,7,23,24,23,141,79,29,7,128,173,128,157,128,161

.text
	.align 4
plt:
_mono_aot_Cirrious_CrossCore_Touch_plt:
_p_1_plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool
plt_Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_ReadIsIosVersionOrHigher_int_bool:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 264,1691
_p_2_plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem__llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_
plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 268,1693
_p_3_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 272,1705
_p_4_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 276,1731
_p_5_plt_Cirrious_CrossCore_Mvx_Warning_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Warning_string_object__
plt_Cirrious_CrossCore_Mvx_Warning_string_object__:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 280,1761
_p_6_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion
plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem_BuildVersion:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 284,1766
_p_7_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice
plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 288,1768
_p_8_plt_string_Split_char___llvm:
	.no_dead_strip plt_string_Split_char__
plt_string_Split_char__:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 292,1773
_p_9_plt_System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
plt_System_Linq_Enumerable_Select_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 296,1778
_p_10_plt_System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int
plt_System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 300,1790
_p_11_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 304,1802
_p_12_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__
plt_Cirrious_CrossCore_Touch_Platform_MvxTouchVersion__ctor_int__:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 308,1825
_p_13_plt_int_Parse_string_llvm:
	.no_dead_strip plt_int_Parse_string
plt_int_Parse_string:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 312,1827
_p_14_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 316,1832
_p_15_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 320,1852
_p_16_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 324,1857
_p_17_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 328,1885
_p_18_plt__jit_icall___emul_iconv_to_r_un_llvm:
	.no_dead_strip plt__jit_icall___emul_iconv_to_r_un
plt__jit_icall___emul_iconv_to_r_un:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 332,1930
_p_19_plt_MonoTouch_UIKit_UIColor_FromRGB_single_single_single_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_FromRGB_single_single_single
plt_MonoTouch_UIKit_UIColor_FromRGB_single_single_single:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 336,1953
_p_20_plt_MonoTouch_UIKit_UIColor_FromRGBA_single_single_single_single_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_FromRGBA_single_single_single_single
plt_MonoTouch_UIKit_UIColor_FromRGBA_single_single_single_single:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 340,1958
_p_21_plt_MonoTouch_UIKit_UIColor_GetRGBA_single__single__single__single__llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIColor_GetRGBA_single__single__single__single_
plt_MonoTouch_UIKit_UIColor_GetRGBA_single__single__single__single_:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 344,1963
_p_22_plt__jit_icall___emul_fconv_to_i8_llvm:
	.no_dead_strip plt__jit_icall___emul_fconv_to_i8
plt__jit_icall___emul_fconv_to_i8:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 348,1968
_p_23_plt__jit_icall___emul_lshl_llvm:
	.no_dead_strip plt__jit_icall___emul_lshl
plt__jit_icall___emul_lshl:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 352,1989
_p_24_plt_System_DateTime_AddSeconds_double_llvm:
	.no_dead_strip plt_System_DateTime_AddSeconds_double
plt_System_DateTime_AddSeconds_double:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 356,2003
_p_25_plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind_llvm:
	.no_dead_strip plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind
plt_System_DateTime__ctor_int_int_int_int_int_int_System_DateTimeKind:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 360,2008
_p_26_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplication_get_SharedApplication
plt_MonoTouch_UIKit_UIApplication_get_SharedApplication:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 364,2013
_p_27_plt__jit_icall_mono_ldvirtfn_llvm:
	.no_dead_strip plt__jit_icall_mono_ldvirtfn
plt__jit_icall_mono_ldvirtfn:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 368,2018
_p_28_plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr
plt_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__ctor_object_intptr:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 372,2034
_p_29_plt_System_EventHandler__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_EventHandler__ctor_object_intptr
plt_System_EventHandler__ctor_object_intptr:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 376,2045
_p_30_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 380,2050
_p_31_plt_MonoTouch_UIKit_UICollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
plt_MonoTouch_UIKit_UICollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 384,2083
_p_32_plt_MonoTouch_UIKit_UICollectionViewController__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewController__ctor_intptr
plt_MonoTouch_UIKit_UICollectionViewController__ctor_intptr:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 388,2088
_p_33_plt_MonoTouch_UIKit_UICollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_MonoTouch_UIKit_UICollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 392,2093
_p_34_plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool
plt_MonoTouch_UIKit_UIViewController_ViewWillDisappear_bool:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 396,2098
_p_35_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool
plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_object_bool:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 400,2103
_p_36_plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool
plt_MonoTouch_UIKit_UIViewController_ViewDidAppear_bool:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 404,2115
_p_37_plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool
plt_MonoTouch_UIKit_UIViewController_ViewWillAppear_bool:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 408,2120
_p_38_plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool
plt_MonoTouch_UIKit_UIViewController_ViewDidDisappear_bool:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 412,2125
_p_39_plt_MonoTouch_UIKit_UIViewController_ViewDidLoad_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_ViewDidLoad
plt_MonoTouch_UIKit_UIViewController_ViewDidLoad:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 416,2130
_p_40_plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object
plt_Cirrious_CrossCore_Core_MvxDelegateExtensionMethods_Raise_System_EventHandler_object:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 420,2135
_p_41_plt_MonoTouch_UIKit_UICollectionViewController_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UICollectionViewController_Dispose_bool
plt_MonoTouch_UIKit_UICollectionViewController_Dispose_bool:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 424,2140
_p_42_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 428,2145
_p_43_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_System_EventHandler__System_EventHandler_System_EventHandler:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 432,2150
_p_44_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Remove_System_Delegate_System_Delegate
plt_System_Delegate_Remove_System_Delegate_System_Delegate:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 436,2162
_p_45_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool__System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool_System_EventHandler_1_Cirrious_CrossCore_Core_MvxValueEventArgs_1_bool:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 440,2167
_p_46_plt_MonoTouch_UIKit_UITabBarController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITabBarController__ctor
plt_MonoTouch_UIKit_UITabBarController__ctor:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 444,2179
_p_47_plt_MonoTouch_UIKit_UITabBarController__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITabBarController__ctor_intptr
plt_MonoTouch_UIKit_UITabBarController__ctor_intptr:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 448,2184
_p_48_plt_MonoTouch_UIKit_UITabBarController_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITabBarController_Dispose_bool
plt_MonoTouch_UIKit_UITabBarController_Dispose_bool:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 452,2189
_p_49_plt_MonoTouch_UIKit_UITableViewController__ctor_MonoTouch_UIKit_UITableViewStyle_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
plt_MonoTouch_UIKit_UITableViewController__ctor_MonoTouch_UIKit_UITableViewStyle:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 456,2194
_p_50_plt_MonoTouch_UIKit_UITableViewController__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewController__ctor_intptr
plt_MonoTouch_UIKit_UITableViewController__ctor_intptr:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 460,2199
_p_51_plt_MonoTouch_UIKit_UITableViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_MonoTouch_UIKit_UITableViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 464,2204
_p_52_plt_MonoTouch_UIKit_UITableViewController_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UITableViewController_Dispose_bool
plt_MonoTouch_UIKit_UITableViewController_Dispose_bool:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 468,2209
_p_53_plt_MonoTouch_UIKit_UIViewController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController__ctor
plt_MonoTouch_UIKit_UIViewController__ctor:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 472,2214
_p_54_plt_MonoTouch_UIKit_UIViewController__ctor_intptr_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController__ctor_intptr
plt_MonoTouch_UIKit_UIViewController__ctor_intptr:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 476,2219
_p_55_plt_MonoTouch_UIKit_UIViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_MonoTouch_UIKit_UIViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 480,2224
_p_56_plt_MonoTouch_UIKit_UIViewController_Dispose_bool_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIViewController_Dispose_bool
plt_MonoTouch_UIKit_UIViewController_Dispose_bool:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 484,2229
_p_57_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 488,2234
_p_58_plt_System_Array_Resize_int_int____int_llvm:
	.no_dead_strip plt_System_Array_Resize_int_int____int
plt_System_Array_Resize_int_int____int:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 492,2272
_p_59_plt_System_Array_InternalEnumerator_1_int_get_Current_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_int_get_Current
plt_System_Array_InternalEnumerator_1_int_get_Current:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 496,2293
_p_60_plt_System_Array_InternalArray__get_Item_int_int_llvm:
	.no_dead_strip plt_System_Array_InternalArray__get_Item_int_int
plt_System_Array_InternalArray__get_Item_int_int:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 500,2313
_p_61_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 504,2334
_p_62_plt_System_Array_InternalEnumerator_1_int__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_int__ctor_System_Array
plt_System_Array_InternalEnumerator_1_int__ctor_System_Array:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 508,2363
_p_63_plt_int_Equals_object_llvm:
	.no_dead_strip plt_int_Equals_object
plt_int_Equals_object:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 512,2383
_p_64_plt_System_Array_Copy_System_Array_int_System_Array_int_int_llvm:
	.no_dead_strip plt_System_Array_Copy_System_Array_int_System_Array_int_int
plt_System_Array_Copy_System_Array_int_System_Array_int_int:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 516,2388
_p_65_plt__jit_icall_mono_create_corlib_exception_2_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_2
plt__jit_icall_mono_create_corlib_exception_2:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 520,2393
_p_66_plt_System_Linq_Check_SourceAndSelector_object_object_llvm:
	.no_dead_strip plt_System_Linq_Check_SourceAndSelector_object_object
plt_System_Linq_Check_SourceAndSelector_object_object:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 524,2426
_p_67_plt_System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int_llvm:
	.no_dead_strip plt_System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int
plt_System_Linq_Enumerable_CreateSelectIterator_string_int_System_Collections_Generic_IEnumerable_1_string_System_Func_2_string_int:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 528,2431
_p_68_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 532,2455
_p_69_plt_System_Array_FastCopy_System_Array_int_System_Array_int_int_llvm:
	.no_dead_strip plt_System_Array_FastCopy_System_Array_int_System_Array_int_int
plt_System_Array_FastCopy_System_Array_int_System_Array_int_int:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 536,2481
_p_70_plt__jit_icall_mono_create_corlib_exception_0_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_0
plt__jit_icall_mono_create_corlib_exception_0:
_p_70:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 540,2486
_p_71_plt_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator_llvm:
	.no_dead_strip plt_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator
plt_System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_System_Collections_Generic_IEnumerable_TResult_GetEnumerator:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 544,2519
_p_72_plt_System_Threading_Interlocked_CompareExchange_int__int_int_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_int__int_int
plt_System_Threading_Interlocked_CompareExchange_int__int_int:
_p_72:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 548,2543
_p_73_plt_MonoTouch_Foundation_NSDate_FromTimeIntervalSinceReferenceDate_double_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSDate_FromTimeIntervalSinceReferenceDate_double
plt_MonoTouch_Foundation_NSDate_FromTimeIntervalSinceReferenceDate_double:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 552,2548
_p_74_plt_System_TimeSpan_get_TotalSeconds_llvm:
	.no_dead_strip plt_System_TimeSpan_get_TotalSeconds
plt_System_TimeSpan_get_TotalSeconds:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 556,2553
_p_75_plt_System_DateTime_op_Subtraction_System_DateTime_System_DateTime_llvm:
	.no_dead_strip plt_System_DateTime_op_Subtraction_System_DateTime_System_DateTime
plt_System_DateTime_op_Subtraction_System_DateTime_System_DateTime:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 560,2558
_p_76_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_76:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 564,2563
_p_77_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 568,2598
_p_78_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 572,2653
_p_79_plt_System_Array_InternalEnumerator_1_T__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_T__ctor_System_Array
plt_System_Array_InternalEnumerator_1_T__ctor_System_Array:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 576,2661
_p_80_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_80:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_CrossCore_Touch_got - . + 580,2680
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 5
	.asciz "Cirrious.CrossCore.Touch"
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "System.Core"
	.asciz "331CE9C5-37DC-44D4-9141-E5A50D9D74AC"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.CrossCore.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_CrossCore_Touch_got
	.align 2
	.long _Cirrious_CrossCore_Touch__Cirrious_CrossCore_Touch_Platform_MvxIosMajorVersionChecker_get_IsVersionOrHigher
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 66,588,81,178,11,387000831,0,4093
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_CrossCore_Touch_info
	.align 2
_mono_aot_module_Cirrious_CrossCore_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,7,4,9,8,7,6,5,10,0,0,0,0,0,0,0,11,19,18,17,16,15,14,13,12
	.byte 12,11,12,0,0,0,1,20,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,8,1,21,1,8
	.byte 1,21,1,8,1,21,0,1,22,0,0,0,0,0,1,23,0,19,24,23,37,34,36,35,34,33,32,25,31,30,25,29
	.byte 28,25,27,26,25,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,38,0,1,38,0,1,38
	.byte 0,1,38,0,0,0,0,0,2,40,39,0,2,40,39,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0
	.byte 2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,40,39,0,2,40,39,0,0,0,0,0,1,38,0,1
	.byte 38,0,1,38,0,1,38,0,0,0,0,0,2,40,39,0,2,40,39,0,2,42,41,0,2,42,41,0,2,42,41,0
	.byte 2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,40,39,0,2,40,39,0,0,0,0,0
	.byte 0,0,1,38,0,1,38,0,1,38,0,1,38,0,0,0,0,0,2,40,39,0,2,40,39,0,2,42,41,0,2,42
	.byte 41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,40,39,0,2,40
	.byte 39,0,0,0,0,0,0,0,1,38,0,1,38,0,1,38,0,1,38,0,0,0,0,0,2,40,39,0,2,40,39,0
	.byte 2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0,2,42,41,0
	.byte 2,40,39,0,2,40,39,0,1,43,0,1,43,0,0,0,0,0,0,0,16,52,52,50,44,50,49,51,44,45,48,49
	.byte 46,47,62,46,53,0,2,8,54,0,0,0,1,55,0,0,0,0,0,0,0,2,54,54,0,0,0,0,0,0,0,0
	.byte 0,0,0,2,8,56,0,0,0,1,57,0,0,0,1,58,0,2,49,49,4,2,35,1,1,2,130,90,2,7,129,212
	.byte 2,44,49,0,1,56,0,1,59,0,0,0,1,8,0,0,0,6,64,60,63,61,47,62,0,2,65,62,0,0,0,0
	.byte 0,1,59,255,252,0,0,0,1,1,3,219,0,0,1,255,252,0,0,0,1,1,3,219,0,0,2,4,2,29,3,1
	.byte 2,130,25,2,255,253,0,0,0,7,130,27,3,198,0,0,73,1,2,130,25,2,0,255,253,0,0,0,7,130,27,3
	.byte 198,0,0,74,1,2,130,25,2,0,255,253,0,0,0,7,130,27,3,198,0,0,75,1,2,130,25,2,0,255,254,0
	.byte 0,0,0,255,43,0,0,3,4,2,130,11,2,1,2,130,90,2,255,253,0,0,0,7,130,104,2,198,0,13,120,1
	.byte 2,130,90,2,0,255,253,0,0,0,7,130,104,2,198,0,13,121,1,2,130,90,2,0,255,253,0,0,0,7,130,104
	.byte 2,198,0,13,122,1,2,130,90,2,0,255,253,0,0,0,7,130,104,2,198,0,13,123,1,2,130,90,2,0,255,253
	.byte 0,0,0,7,130,104,2,198,0,13,124,1,2,130,90,2,0,255,253,0,0,0,7,130,104,2,198,0,13,125,1,2
	.byte 130,90,2,0,255,253,0,0,0,2,130,10,2,2,198,0,13,43,0,1,2,130,90,2,194,0,13,41,194,0,13,42
	.byte 194,0,13,44,255,253,0,0,0,2,130,10,2,2,198,0,13,45,0,1,2,130,90,2,255,253,0,0,0,2,130,10
	.byte 2,2,198,0,13,46,0,1,2,130,90,2,255,253,0,0,0,2,130,10,2,2,198,0,13,47,0,1,2,130,90,2
	.byte 255,253,0,0,0,2,130,10,2,2,198,0,13,48,0,1,2,130,90,2,255,254,0,0,0,0,255,43,0,0,2,5
	.byte 30,0,1,255,255,255,255,255,194,0,13,43,255,253,0,0,0,2,130,10,2,2,198,0,13,43,0,1,7,131,95,255
	.byte 254,0,0,0,0,255,43,0,0,4,255,253,0,0,0,2,130,10,2,2,198,0,13,112,0,1,2,130,90,2,255,253
	.byte 0,0,0,7,129,212,1,198,0,0,234,1,2,130,90,2,0,255,253,0,0,0,2,130,10,2,2,198,0,13,54,0
	.byte 1,2,130,90,2,255,253,0,0,0,2,33,1,1,198,0,0,206,0,2,2,130,146,2,2,130,90,2,4,2,42,1
	.byte 2,2,130,146,2,2,130,90,2,255,253,0,0,0,7,131,220,1,198,0,1,20,2,2,130,146,2,2,130,90,2,0
	.byte 255,253,0,0,0,7,131,220,1,198,0,1,21,2,2,130,146,2,2,130,90,2,0,255,253,0,0,0,7,131,220,1
	.byte 198,0,1,22,2,2,130,146,2,2,130,90,2,0,255,253,0,0,0,7,131,220,1,198,0,1,23,2,2,130,146,2
	.byte 2,130,90,2,0,255,253,0,0,0,7,131,220,1,198,0,1,24,2,2,130,146,2,2,130,90,2,0,255,253,0,0
	.byte 0,7,131,220,1,198,0,1,25,2,2,130,146,2,2,130,90,2,0,255,253,0,0,0,7,131,220,1,198,0,1,26
	.byte 2,2,130,146,2,2,130,90,2,0,255,253,0,0,0,7,131,220,1,198,0,1,27,2,2,130,146,2,2,130,90,2
	.byte 0,12,0,39,42,47,34,255,254,0,0,0,0,255,43,0,0,1,6,1,17,0,1,14,6,1,2,130,123,2,14,2
	.byte 130,90,2,17,0,128,152,17,0,128,136,14,6,1,2,130,30,2,16,1,4,3,34,255,254,0,0,0,0,255,43,0
	.byte 0,2,34,255,254,0,0,0,0,255,43,0,0,3,14,1,5,14,3,219,0,0,2,6,10,50,10,30,3,219,0,0
	.byte 2,14,2,30,3,16,1,8,7,11,2,128,197,4,11,2,128,220,4,6,36,14,3,219,0,0,1,6,41,6,35,6
	.byte 43,6,34,6,39,6,33,6,45,6,32,14,2,130,63,2,6,195,0,0,84,6,31,6,37,34,255,254,0,0,0,0
	.byte 255,43,0,0,4,34,255,254,0,0,0,0,255,43,0,0,5,11,2,130,63,2,34,255,254,0,0,0,0,255,43,0
	.byte 0,6,11,3,219,0,0,1,33,16,7,129,212,109,4,2,111,2,1,2,130,90,2,6,255,253,0,0,0,7,133,110
	.byte 2,198,0,3,125,1,2,130,90,2,0,34,255,253,0,0,0,2,130,10,2,2,198,0,13,112,0,1,2,130,90,2
	.byte 6,194,0,5,8,4,2,112,2,1,2,130,90,2,6,255,253,0,0,0,7,133,165,2,198,0,3,126,1,2,130,90
	.byte 2,0,14,6,1,2,130,90,2,4,2,108,2,1,2,130,90,2,6,255,253,0,0,0,7,133,201,2,198,0,3,109
	.byte 1,2,130,90,2,0,6,255,253,0,0,0,7,133,201,2,198,0,3,114,1,2,130,90,2,0,23,7,133,201,12,1
	.byte 14,7,130,104,34,255,253,0,0,0,2,130,10,2,2,198,0,13,54,0,1,2,130,90,2,34,255,253,0,0,0,2
	.byte 130,10,2,2,198,0,13,56,0,1,2,130,90,2,34,255,253,0,0,0,2,33,1,1,198,0,0,206,0,2,2,130
	.byte 146,2,2,130,90,2,14,7,130,27,14,7,131,220,4,2,111,2,1,2,130,146,2,6,255,253,0,0,0,7,134,78
	.byte 2,198,0,3,125,1,2,130,146,2,0,4,2,112,2,1,2,130,146,2,6,255,253,0,0,0,7,134,107,2,198,0
	.byte 3,126,1,2,130,146,2,0,6,194,0,17,58,8,1,129,132,8,2,108,128,176,8,2,128,184,104,3,5,3,255,254
	.byte 0,0,0,0,255,43,0,0,1,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102
	.byte 105,99,0,7,27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120
	.byte 0,3,195,0,1,13,3,9,3,196,0,4,148,3,194,0,19,49,3,255,254,0,0,0,0,255,43,0,0,2,3,255
	.byte 254,0,0,0,0,255,43,0,0,3,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116
	.byte 0,3,11,3,194,0,17,110,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,3,195,0,0
	.byte 77,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,7,42,108
	.byte 108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116
	.byte 114,97,109,112,111,108,105,110,101,0,7,20,95,95,101,109,117,108,95,105,99,111,110,118,95,116,111,95,114,95,117,110
	.byte 0,3,196,0,4,108,3,196,0,4,113,3,196,0,4,111,7,18,95,95,101,109,117,108,95,102,99,111,110,118,95,116
	.byte 111,95,105,56,0,7,11,95,95,101,109,117,108,95,108,115,104,108,0,3,194,0,15,89,3,194,0,15,63,3,196,0
	.byte 4,14,7,13,109,111,110,111,95,108,100,118,105,114,116,102,110,0,3,255,254,0,0,0,0,202,0,0,34,3,194,0
	.byte 16,200,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110
	.byte 95,49,0,3,196,0,4,88,3,196,0,4,86,3,196,0,4,87,3,196,0,6,31,3,255,254,0,0,0,0,255,43
	.byte 0,0,4,3,196,0,6,30,3,196,0,6,29,3,196,0,6,32,3,196,0,6,28,3,195,0,0,4,3,196,0,4
	.byte 91,3,194,0,16,63,3,255,254,0,0,0,0,255,43,0,0,5,3,194,0,16,65,3,255,254,0,0,0,0,255,43
	.byte 0,0,6,3,196,0,7,156,3,196,0,7,157,3,196,0,7,158,3,196,0,7,193,3,196,0,7,191,3,196,0,7
	.byte 192,3,196,0,7,195,3,196,0,6,11,3,196,0,6,13,3,196,0,6,14,3,196,0,6,41,7,35,109,111,110,111
	.byte 95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0
	.byte 3,255,253,0,0,0,2,130,10,2,2,198,0,13,112,0,1,2,130,90,2,3,255,253,0,0,0,7,130,104,2,198
	.byte 0,13,122,1,2,130,90,2,0,3,255,253,0,0,0,2,130,10,2,2,198,0,13,54,0,1,2,130,90,2,7,26
	.byte 109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,95,109,115,99,111,114,108,105,98,0,3,255,253,0,0
	.byte 0,7,130,104,2,198,0,13,121,1,2,130,90,2,0,3,194,0,17,93,3,194,0,13,85,7,30,109,111,110,111,95
	.byte 99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,50,0,3,193,0,0,170,3
	.byte 255,253,0,0,0,2,33,1,1,198,0,0,206,0,2,2,130,146,2,2,130,90,2,7,23,109,111,110,111,95,111,98
	.byte 106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0,3,194,0,13,70,7,30,109,111,110,111,95,99,114,101
	.byte 97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,48,0,3,255,253,0,0,0,7,131,220
	.byte 1,198,0,1,27,2,2,130,146,2,2,130,90,2,0,3,194,0,12,100,3,196,0,0,145,3,194,0,19,236,3,194
	.byte 0,15,148,7,32,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101
	.byte 112,116,105,111,110,0,7,24,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,115,112,101,99,105,102,105,99
	.byte 0,255,253,0,0,0,2,130,10,2,2,198,0,13,43,0,1,7,131,95,4,2,130,11,2,1,7,131,95,35,138,65
	.byte 150,5,7,138,84,3,255,253,0,0,0,7,138,84,2,198,0,13,121,1,7,131,95,0,35,138,65,192,0,92,41,255
	.byte 253,0,0,0,2,130,10,2,2,198,0,13,43,0,1,7,131,95,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,2,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,6,24
	.byte 1,2,0,131,12,129,228,130,200,130,204,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,3,56,0,1,11,4,19
	.byte 255,253,0,0,0,2,130,10,2,2,198,0,13,43,0,1,7,131,95,1,0,1,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,38,80,1,1,2,0,130,32,128,176,129,192,129,196,0,4
	.byte 129,128,0,6,106,1,2,0,128,184,104,108,112,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,0,128
	.byte 144,8,0,0,1,4,128,128,9,0,0,1,194,0,18,178,194,0,18,175,194,0,18,174,194,0,18,172,5,128,224,12
	.byte 4,0,4,194,0,18,178,194,0,18,175,194,0,18,174,194,0,18,172,6,4,128,160,20,0,0,4,194,0,18,178,194
	.byte 0,18,175,194,0,18,174,194,0,18,172,0,128,144,8,0,0,1,4,128,144,8,0,0,1,194,0,18,178,194,0,18
	.byte 175,194,0,18,174,194,0,18,172,4,128,132,25,8,8,0,1,194,0,18,178,194,0,18,175,194,0,18,174,194,0,18
	.byte 172,4,128,144,8,0,0,1,194,0,18,178,194,0,18,175,194,0,18,174,194,0,18,172,4,128,144,8,0,0,1,194
	.byte 0,18,178,194,0,18,175,194,0,18,174,194,0,18,172,10,128,160,12,0,0,4,194,0,18,178,194,0,18,175,194,0
	.byte 18,174,194,0,18,172,36,35,34,33,32,31,0,128,144,8,0,0,1,54,128,162,196,0,2,8,76,0,0,4,196,0
	.byte 2,18,194,0,18,175,196,0,2,8,194,0,18,172,196,0,2,4,196,0,2,9,55,196,0,2,13,196,0,2,12,196
	.byte 0,2,7,196,0,4,85,196,0,5,28,196,0,5,27,196,0,5,26,196,0,6,26,196,0,6,40,196,0,6,39,196
	.byte 0,6,38,196,0,6,37,196,0,6,36,196,0,6,35,196,0,6,34,196,0,6,33,53,50,51,52,54,196,0,6,27
	.byte 196,0,6,23,196,0,6,22,196,0,6,21,196,0,6,20,196,0,6,19,196,0,6,18,196,0,6,17,196,0,6,16
	.byte 196,0,6,15,196,0,4,89,196,0,4,90,196,0,4,90,196,0,4,89,56,57,58,59,60,61,62,63,64,65,66,67
	.byte 50,128,162,196,0,2,8,72,0,0,4,196,0,2,18,194,0,18,175,196,0,2,8,194,0,18,172,196,0,2,4,196
	.byte 0,2,9,75,196,0,2,13,196,0,2,12,196,0,2,7,196,0,7,155,196,0,5,28,196,0,5,27,196,0,5,26
	.byte 196,0,6,26,196,0,6,40,196,0,6,39,196,0,6,38,196,0,6,37,196,0,6,36,196,0,6,35,196,0,6,34
	.byte 196,0,6,33,73,70,71,72,74,196,0,6,27,196,0,6,23,196,0,6,22,196,0,6,21,196,0,6,20,196,0,6
	.byte 19,196,0,6,18,196,0,6,17,196,0,6,16,196,0,6,15,76,77,78,79,80,81,82,83,84,85,86,87,51,128,162
	.byte 196,0,2,8,76,0,0,4,196,0,2,18,194,0,18,175,196,0,2,8,194,0,18,172,196,0,2,4,196,0,2,9
	.byte 96,196,0,2,13,196,0,2,12,196,0,2,7,196,0,7,190,196,0,5,28,196,0,5,27,196,0,5,26,196,0,6
	.byte 26,196,0,6,40,196,0,6,39,196,0,6,38,196,0,6,37,196,0,6,36,196,0,6,35,196,0,6,34,196,0,6
	.byte 33,94,91,92,93,95,196,0,6,27,196,0,6,23,196,0,6,22,196,0,6,21,196,0,6,20,196,0,6,19,196,0
	.byte 6,18,196,0,6,17,196,0,6,16,196,0,6,15,196,0,7,194,97,98,99,100,101,102,103,104,105,106,107,108,50,128
	.byte 162,196,0,2,8,72,0,0,4,196,0,2,18,194,0,18,175,196,0,2,8,194,0,18,172,196,0,2,4,196,0,2
	.byte 9,117,196,0,2,13,196,0,2,12,196,0,2,7,196,0,6,10,196,0,5,28,196,0,5,27,196,0,5,26,196,0
	.byte 6,26,196,0,6,40,196,0,6,39,196,0,6,38,196,0,6,37,196,0,6,36,196,0,6,35,196,0,6,34,196,0
	.byte 6,33,115,112,113,114,116,196,0,6,27,196,0,6,23,196,0,6,22,196,0,6,21,196,0,6,20,196,0,6,19,196
	.byte 0,6,18,196,0,6,17,196,0,6,16,196,0,6,15,118,119,120,121,122,123,124,125,126,127,128,128,128,129,98,111,101
	.byte 104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.CrossCore.Touch.MvxTouchDateTimeExtensionMethods:ToNSDate"
	.long _Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime
	.long Lme_17

	.byte 2,118,16,3
	.asciz "date"

LDIFF_SYM3=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM3
	.byte 2,123,32,11
	.asciz "V_0"

LDIFF_SYM4=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM4
	.byte 2,123,16,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM5=Lfde0_end - Lfde0_start
	.long LDIFF_SYM5
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime

LDIFF_SYM6=Lme_17 - _Cirrious_CrossCore_Touch_MvxTouchDateTimeExtensionMethods_ToNSDate_System_DateTime
	.long LDIFF_SYM6
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_0:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM7=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM8=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM9=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM9
LTDIE_1:

	.byte 17
	.asciz "System_Collections_Generic_ICollection`1"

	.byte 8,7
	.asciz "System_Collections_Generic_ICollection`1"

LDIFF_SYM10=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM10
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM11=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM11
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM12=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_4:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM13=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM14=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM15=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM15
LTDIE_3:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM16=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM17=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM18=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM19=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_2:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM20=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM21=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM22=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM23=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM24=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM24
LTDIE_5:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM25=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM25
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM26=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM26
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM27=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2
	.asciz "System.Linq.Enumerable:ToArray<int>"
	.long _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0
	.long Lme_8b

	.byte 2,118,16,3
	.asciz "source"

LDIFF_SYM28=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 1,90,11
	.asciz "array"

LDIFF_SYM29=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM29
	.byte 2,123,0,11
	.asciz "collection"

LDIFF_SYM30=LTDIE_1_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM30
	.byte 1,86,11
	.asciz "pos"

LDIFF_SYM31=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM31
	.byte 1,85,11
	.asciz "element"

LDIFF_SYM32=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 1,84,11
	.asciz ""

LDIFF_SYM33=LTDIE_5_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,123,4,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM34=Lfde1_end - Lfde1_start
	.long LDIFF_SYM34
Lfde1_start:

	.long 0
	.align 2
	.long _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0

LDIFF_SYM35=Lme_8b - _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0
	.long LDIFF_SYM35
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,72,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_6:

	.byte 5
	.asciz "System_Array"

	.byte 8,16
LDIFF_SYM36=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM36
	.byte 2,35,0,0,7
	.asciz "System_Array"

LDIFF_SYM37=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM38=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM39=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM39
	.byte 2
	.asciz "System.Array:InternalArray__IEnumerable_GetEnumerator<T>"
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long Lme_a4

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM40=LTDIE_6_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM40
	.byte 2,123,28,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM41=Lfde2_end - Lfde2_start
	.long LDIFF_SYM41
Lfde2_start:

	.long 0
	.align 2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

LDIFF_SYM42=Lme_a4 - _System_Array_InternalArray__IEnumerable_GetEnumerator_T
	.long LDIFF_SYM42
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_8:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerable`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerable`1"

LDIFF_SYM43=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM43
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM44=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM44
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM45=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM45
LTDIE_9:

	.byte 17
	.asciz "System_Collections_Generic_IEnumerator`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEnumerator`1"

LDIFF_SYM46=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM46
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM47=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM47
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM48=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_15:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM49=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM50=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM51=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM52=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_14:

	.byte 5
	.asciz "System_Reflection_MethodBase"

	.byte 8,16
LDIFF_SYM53=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodBase"

LDIFF_SYM54=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM54
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM55=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM56=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_13:

	.byte 5
	.asciz "System_Reflection_MethodInfo"

	.byte 8,16
LDIFF_SYM57=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM57
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MethodInfo"

LDIFF_SYM58=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM59=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM60=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_17:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM61=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM62=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM62
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM63=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM64=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM65=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM65
LTDIE_16:

	.byte 5
	.asciz "System_DelegateData"

	.byte 16,16
LDIFF_SYM66=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,0,6
	.asciz "target_type"

LDIFF_SYM67=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,8,6
	.asciz "method_name"

LDIFF_SYM68=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM68
	.byte 2,35,12,0,7
	.asciz "System_DelegateData"

LDIFF_SYM69=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM70=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM71=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM71
LTDIE_12:

	.byte 5
	.asciz "System_Delegate"

	.byte 44,16
LDIFF_SYM72=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,0,6
	.asciz "method_ptr"

LDIFF_SYM73=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,8,6
	.asciz "invoke_impl"

LDIFF_SYM74=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,12,6
	.asciz "m_target"

LDIFF_SYM75=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,16,6
	.asciz "method"

LDIFF_SYM76=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM76
	.byte 2,35,20,6
	.asciz "delegate_trampoline"

LDIFF_SYM77=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM77
	.byte 2,35,24,6
	.asciz "method_code"

LDIFF_SYM78=LDIE_I - Ldebug_info_start
	.long LDIFF_SYM78
	.byte 2,35,28,6
	.asciz "method_info"

LDIFF_SYM79=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,32,6
	.asciz "original_method_info"

LDIFF_SYM80=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,36,6
	.asciz "data"

LDIFF_SYM81=LTDIE_16_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM81
	.byte 2,35,40,0,7
	.asciz "System_Delegate"

LDIFF_SYM82=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM83=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM83
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM84=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM84
LTDIE_11:

	.byte 5
	.asciz "System_MulticastDelegate"

	.byte 52,16
LDIFF_SYM85=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM85
	.byte 2,35,0,6
	.asciz "prev"

LDIFF_SYM86=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM86
	.byte 2,35,44,6
	.asciz "kpm_next"

LDIFF_SYM87=LTDIE_11_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,48,0,7
	.asciz "System_MulticastDelegate"

LDIFF_SYM88=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM88
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM89=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM90=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_10:

	.byte 5
	.asciz "System_Func`2"

	.byte 52,16
LDIFF_SYM91=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2,35,0,0,7
	.asciz "System_Func`2"

LDIFF_SYM92=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM92
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM93=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM93
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM94=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM94
LTDIE_18:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM95=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM96=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM97=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM97
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM98=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM98
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM99=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM99
LTDIE_7:

	.byte 5
	.asciz "_<CreateSelectIterator>c__Iterator10`2"

	.byte 36,16
LDIFF_SYM100=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM100
	.byte 2,35,0,6
	.asciz "source"

LDIFF_SYM101=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM101
	.byte 2,35,8,6
	.asciz "$locvar0"

LDIFF_SYM102=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2,35,12,6
	.asciz "<element>__0"

LDIFF_SYM103=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 2,35,16,6
	.asciz "selector"

LDIFF_SYM104=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,35,20,6
	.asciz "$current"

LDIFF_SYM105=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 2,35,24,6
	.asciz "$disposing"

LDIFF_SYM106=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM106
	.byte 2,35,28,6
	.asciz "$PC"

LDIFF_SYM107=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM107
	.byte 2,35,32,0,7
	.asciz "_<CreateSelectIterator>c__Iterator10`2"

LDIFF_SYM108=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM108
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM109=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM109
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM110=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM110
LTDIE_19:

	.byte 5
	.asciz "System_UInt32"

	.byte 12,16
LDIFF_SYM111=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM111
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM112=LDIE_U4 - Ldebug_info_start
	.long LDIFF_SYM112
	.byte 2,35,8,0,7
	.asciz "System_UInt32"

LDIFF_SYM113=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM113
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM114=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM115=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM115
	.byte 2
	.asciz "System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<string, int>:MoveNext"
	.long _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0
	.long Lme_ad

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM116=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM116
	.byte 2,123,20,11
	.asciz ""

LDIFF_SYM117=LDIE_U4 - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 1,90,11
	.asciz ""

LDIFF_SYM118=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM119=Lfde3_end - Lfde3_start
	.long LDIFF_SYM119
Lfde3_start:

	.long 0
	.align 2
	.long _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0

LDIFF_SYM120=Lme_ad - _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0
	.long LDIFF_SYM120
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,64,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "System.Linq.Enumerable/<CreateSelectIterator>c__Iterator10`2<string, int>:Dispose"
	.long _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0
	.long Lme_ae

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM121=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 2,123,16,11
	.asciz "V_0"

LDIFF_SYM122=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM122
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM123=Lfde4_end - Lfde4_start
	.long LDIFF_SYM123
Lfde4_start:

	.long 0
	.align 2
	.long _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0

LDIFF_SYM124=Lme_ae - _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_Dispose_0
	.long LDIFF_SYM124
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde4_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/System.Core/System.Linq"
	.asciz "/Developer/MonoTouch/Source/mono/mcs/class/corlib/System"

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0
	.asciz "Enumerable.cs"

	.byte 1,0,0
	.asciz "Array.cs"

	.byte 2,0,0,0
Ldebug_line_header_end:
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Linq_Enumerable_ToArray_int_System_Collections_Generic_IEnumerable_1_int_0

	.byte 3,202,22,4,2,1,3,202,22,2,48,1,3,1,2,176,1,1,131,3,1,2,44,1,8,118,3,1,2,196,0,1
	.byte 3,1,2,44,1,77,75,8,117,3,1,2,224,0,1,131,3,1,2,36,1,76,8,231,3,3,2,168,1,1,131,8
	.byte 230,2,200,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Array_InternalArray__IEnumerable_GetEnumerator_T

	.byte 3,207,0,4,3,1,3,207,0,2,32,1,2,252,0,1,0,1,1
.section __DWARF, __debug_line,regular,debug

	.byte 0,5,2
	.long _System_Linq_Enumerable__CreateSelectIteratorc__Iterator10_2_string_int_MoveNext_0

	.byte 3,231,17,4,2,1,3,231,17,2,248,0,1,3,1,2,180,1,1,3,1,2,252,1,1,2,28,1,0,1,1,0
	.byte 1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
