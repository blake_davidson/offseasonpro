	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo:
Leh_func_begin1:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
	push	{r8}
Ltmp2:
	mov	r0, #0
	mov	r4, #0
	cmp	r1, #0
	beq	LBB1_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC1_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC1_0+8))
	ldr	r9, [r1]
LPC1_0:
	add	r2, pc, r2
	ldr	r2, [r2, #20]
	ldrh	r4, [r9, #20]
	cmp	r4, r2
	mov	r4, #0
	blo	LBB1_3
	ldr	r4, [r9, #16]
	ldrb	r9, [r4, r2, asr #3]
	and	r2, r2, #7
	mov	r4, #1
	and	r2, r9, r4, lsl r2
	cmp	r2, #0
	moveq	r1, r2
	mov	r4, r1
LBB1_3:
	cmp	r4, #0
	cmpne	r3, #0
	beq	LBB1_5
	ldr	r0, [r3]
	ldr	r1, [r0, #32]
	mov	r0, r3
	blx	r1
	ldr	r2, [r4]
	mov	r1, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC1_1+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC1_1+8))
LPC1_1:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	sub	r2, r2, #36
	ldr	r2, [r2]
	mov	r8, r0
	mov	r0, r4
	blx	r2
LBB1_5:
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter__ctor
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter__ctor:
Leh_func_begin2:
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_System_Type
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_System_Type:
Leh_func_begin3:
	push	{r4, r5, r6, r7, lr}
Ltmp3:
	add	r7, sp, #12
Ltmp4:
Ltmp5:
	mov	r5, r1
	mov	r4, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #268]
	mov	r0, r5
	blx	r1
	mov	r6, r0
	ldr	r0, [r5]
	ldr	r1, [r0, #72]
	mov	r0, r5
	blx	r1
	mov	r2, r0
	mov	r0, r4
	mov	r1, r6
	bl	_p_1_plt_Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string_llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string:
Leh_func_begin4:
	mov	r3, r2
	mov	r2, r1
	strd	r2, r3, [r0, #8]
	bx	lr
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string:
Leh_func_begin5:
	push	{r7, lr}
Ltmp6:
	mov	r7, sp
Ltmp7:
Ltmp8:
	mov	r12, r1
	ldr	r1, [r0, #8]
	ldr	r2, [r0, #12]
	ldr	r3, [r0]
	ldr	r9, [r3, #56]
	mov	r3, r12
	blx	r9
	pop	{r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_object__
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_object__:
Leh_func_begin6:
	push	{r4, r7, lr}
Ltmp9:
	add	r7, sp, #4
Ltmp10:
Ltmp11:
	mov	r4, r2
	ldr	r2, [r0]
	ldr	r2, [r2, #64]
	blx	r2
	mov	r1, r4
	bl	_p_9_plt_string_Format_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_string_string
	.align	2
_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_string_string:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp12:
	add	r7, sp, #12
Ltmp13:
	push	{r8, r10}
Ltmp14:
	mov	r6, r1
	ldr	r1, [r0]
	mov	r10, r3
	mov	r5, r2
	ldr	r1, [r1, #68]
	blx	r1
	ldr	r1, [r0]
	mov	r2, r5
	mov	r3, r10
	sub	r1, r1, #12
	ldr	r4, [r1]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC7_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Localization_got-(LPC7_0+8))
LPC7_0:
	add	r1, pc, r1
	ldr	r1, [r1, #32]
	mov	r8, r1
	mov	r1, r6
	blx	r4
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end7:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Localization_got,84,4
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_System_Type
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_object__
	.no_dead_strip	_Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_string_string
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Localization_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	8
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	0
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	1
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	6
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	7
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	9
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	10
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	11
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
Lset8 = Leh_func_end7-Leh_func_begin7
	.long	Lset8
Lset9 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset9
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Localization.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0
_Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0:

	.byte 128,64,45,233,13,112,160,225,0,13,45,233,36,208,77,226,13,176,160,225,0,160,160,225,0,0,160,227,0,0,203,229
	.byte 0,0,160,227,8,0,139,229,16,0,154,229,0,0,80,227,1,0,0,10,16,0,154,229,46,0,0,234,0,0,160,227
	.byte 0,0,203,229,8,160,139,229,10,0,160,225,11,16,160,225
bl _p_2

	.byte 0,0,90,227,41,0,0,11,16,0,138,226,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 12
	.byte 8,128,159,231
bl _p_4

	.byte 16,0,154,229,0,0,80,227,16,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . -12
	.byte 0,0,159,231,1,16,160,227
bl _p_5

	.byte 28,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 16
	.byte 0,0,159,231
bl _p_6

	.byte 28,16,155,229,24,0,139,229
bl _p_7

	.byte 24,0,155,229
bl _p_8

	.byte 16,0,154,229,4,0,139,229,0,0,0,235,7,0,0,234,20,224,139,229,0,0,219,229,0,0,80,227,1,0,0,10
	.byte 8,0,155,229
bl _p_10

	.byte 20,192,155,229,12,240,160,225,4,0,155,229,36,208,139,226,0,13,189,232,128,128,189,232,14,16,160,225,0,0,159,229
bl _p_11

	.byte 118,2,0,2

Lme_8:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter__ctor
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_System_Type
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_object__
.no_dead_strip _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_string_string

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter__ctor
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_System_Type
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string
	bl _Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_object__
	bl _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder_GetText_string_string_string
	bl method_addresses
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 13,10,2,2
	.short 0, 14
	.byte 1,5,255,255,255,255,250,0,0,0,8,2,2,4,18,2,255,255,255,255,236
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 11,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 0
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 4, 0, 0, 0, 0
	.short 0, 5, 0, 0, 0, 0, 0, 3
	.short 0, 0, 0, 2, 0, 0, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 9,10,1,2
	.short 0
	.byte 23,2,1,1,1,2,3,12,4
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 13,10,2,2
	.short 0, 16
	.byte 128,236,3,255,255,255,255,17,0,0,0,128,242,3,3,13,129,8,3,255,255,255,254,245
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 25,12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,56,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 5,10,1,2
	.short 0
	.byte 129,14,7,33,7,7

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Localization_plt:

.set _p_1_plt_Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string_llvm, _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageBinder__ctor_string_string
_p_2_plt_System_Threading_Monitor_Enter_object_bool__llvm:
	.no_dead_strip plt_System_Threading_Monitor_Enter_object_bool_
plt_System_Threading_Monitor_Enter_object_bool_:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 40,53
_p_3_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 44,58
_p_4_plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_MvvmCross_Localization_IMvxTextProvider_Cirrious_MvvmCross_Localization_IMvxTextProvider__llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_MvvmCross_Localization_IMvxTextProvider_Cirrious_MvvmCross_Localization_IMvxTextProvider_
plt_Cirrious_CrossCore_Mvx_TryResolve_Cirrious_MvvmCross_Localization_IMvxTextProvider_Cirrious_MvvmCross_Localization_IMvxTextProvider_:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 48,103
_p_5_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 52,115
_p_6_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 56,135
_p_7_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 60,158
_p_8_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 64,163
_p_9_plt_string_Format_string_object___llvm:
	.no_dead_strip plt_string_Format_string_object__
plt_string_Format_string_object__:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 68,191
_p_10_plt_System_Threading_Monitor_Exit_object_llvm:
	.no_dead_strip plt_System_Threading_Monitor_Exit_object
plt_System_Threading_Monitor_Exit_object:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 72,196
_p_11_plt__jit_icall_mono_arch_throw_corlib_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_corlib_exception
plt__jit_icall_mono_arch_throw_corlib_exception:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Localization_got - . + 76,201
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "Cirrious.MvvmCross.Localization"
	.asciz "198676EF-1FB1-478C-B008-FF2DED0817FD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "198676EF-1FB1-478C-B008-FF2DED0817FD"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Localization"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Localization_got
	.align 2
	.long _Cirrious_MvvmCross_Localization__Cirrious_MvvmCross_Localization_MvxLanguageConverter_Convert_object_System_Type_object_System_Globalization_CultureInfo
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 9,84,12,13,11,387000831,0,353
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Localization_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Localization_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,3,5,5,4,0,0,0,0,0,0,0,2,6,7,0,0,0,0,0,1,8,12,0,39,42,47,6,3,23,1
	.byte 3,34,255,254,0,0,0,0,255,43,0,0,1,14,2,30,1,6,5,3,8,3,194,0,12,116,7,42,108,108,118,109
	.byte 95,116,104,114,111,119,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109
	.byte 112,111,108,105,110,101,0,3,255,254,0,0,0,0,255,43,0,0,1,7,17,109,111,110,111,95,104,101,108,112,101,114
	.byte 95,108,100,115,116,114,0,7,20,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,193
	.byte 0,0,77,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3
	.byte 194,0,19,128,3,194,0,12,112,7,32,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,99,111,114,108,105
	.byte 98,95,101,120,99,101,112,116,105,111,110,0,16,0,0,16,0,0,16,0,0,16,0,0,6,0,1,2,0,128,244,68
	.byte 128,208,128,212,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,8,128,144,8,0,0,1,194,0,18,178
	.byte 194,0,18,175,194,0,18,174,194,0,18,172,1,193,0,0,11,193,0,0,11,1,0,128,144,8,0,0,1,0,128,144
	.byte 8,0,0,1,10,128,160,20,0,0,4,194,0,18,178,194,0,18,175,194,0,18,174,194,0,18,172,10,11,12,11,10
	.byte 9,98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_1:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_2:

	.byte 17
	.asciz "Cirrious_MvvmCross_Localization_IMvxTextProvider"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Localization_IMvxTextProvider"

LDIFF_SYM6=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM6
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM7=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM8=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Localization_MvxLanguageBinder"

	.byte 20,16
LDIFF_SYM9=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM9
	.byte 2,35,0,6
	.asciz "_namespaceName"

LDIFF_SYM10=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM10
	.byte 2,35,8,6
	.asciz "_typeName"

LDIFF_SYM11=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM11
	.byte 2,35,12,6
	.asciz "_cachedTextProvider"

LDIFF_SYM12=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM12
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_Localization_MvxLanguageBinder"

LDIFF_SYM13=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM13
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM14=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM14
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM15=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM15
LTDIE_4:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM16=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM16
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM17=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM18=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM18
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM19=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM19
LTDIE_3:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM20=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM21=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM22=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM22
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM23=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM23
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM24=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2
	.asciz "Cirrious.MvvmCross.Localization.MvxLanguageBinder:get_TextProvider"
	.long _Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0
	.long Lme_8

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM25=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM26=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM26
	.byte 2,123,0,11
	.asciz "V_1"

LDIFF_SYM27=LTDIE_2_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,123,4,11
	.asciz "V_2"

LDIFF_SYM28=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,123,8,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM29=Lfde0_end - Lfde0_start
	.long LDIFF_SYM29
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0

LDIFF_SYM30=Lme_8 - _Cirrious_MvvmCross_Localization_MvxLanguageBinder_get_TextProvider_0
	.long LDIFF_SYM30
	.byte 12,13,0,72,14,8,135,2,68,14,20,136,5,138,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
