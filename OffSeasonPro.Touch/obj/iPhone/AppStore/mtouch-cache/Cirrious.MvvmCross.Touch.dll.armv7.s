	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication:
Leh_func_begin1:
	push	{r7, lr}
Ltmp0:
	mov	r7, sp
Ltmp1:
Ltmp2:
	mov	r1, #1
	bl	_p_1_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent_llvm
	pop	{r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication:
Leh_func_begin2:
	push	{r7, lr}
Ltmp3:
	mov	r7, sp
Ltmp4:
Ltmp5:
	mov	r1, #3
	bl	_p_1_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent_llvm
	pop	{r7, pc}
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillTerminate_MonoTouch_UIKit_UIApplication
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillTerminate_MonoTouch_UIKit_UIApplication:
Leh_func_begin3:
	push	{r7, lr}
Ltmp6:
	mov	r7, sp
Ltmp7:
Ltmp8:
	mov	r1, #4
	bl	_p_1_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent_llvm
	pop	{r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication:
Leh_func_begin4:
	push	{r7, lr}
Ltmp9:
	mov	r7, sp
Ltmp10:
Ltmp11:
	mov	r1, #0
	bl	_p_1_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent_llvm
	pop	{r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent:
Leh_func_begin5:
	push	{r4, r5, r6, r7, lr}
Ltmp12:
	add	r7, sp, #12
Ltmp13:
Ltmp14:
	mov	r4, r0
	mov	r6, r1
	ldr	r5, [r4, #20]
	cmp	r5, #0
	popeq	{r4, r5, r6, r7, pc}
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC5_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC5_0+8))
LPC5_0:
	add	r0, pc, r0
	ldr	r0, [r0, #16]
	bl	_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm
	mov	r2, r0
	mov	r0, r5
	mov	r1, r4
	str	r6, [r2, #8]
	ldr	r3, [r5, #12]
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_add_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_add_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs:
Leh_func_begin6:
	push	{r4, r5, r6, r7, lr}
Ltmp15:
	add	r7, sp, #12
Ltmp16:
	push	{r8, r10, r11}
Ltmp17:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #20]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC6_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC6_2+8))
LPC6_2:
	add	r0, pc, r0
	ldr	r10, [r0, #24]
	beq	LBB6_5
	ldr	r4, [r0, #20]
	b	LBB6_3
LBB6_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_5_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs__System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB6_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_3_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB6_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB6_8
	b	LBB6_2
LBB6_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_3_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB6_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB6_8
Ltmp18:
LBB6_7:
	ldr	r0, LCPI6_1
LPC6_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp19:
LBB6_8:
	ldr	r0, LCPI6_0
LPC6_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI6_0:
	.long	Ltmp19-(LPC6_0+8)
LCPI6_1:
	.long	Ltmp18-(LPC6_1+8)
	.end_data_region
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_remove_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_remove_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs:
Leh_func_begin7:
	push	{r4, r5, r6, r7, lr}
Ltmp20:
	add	r7, sp, #12
Ltmp21:
	push	{r8, r10, r11}
Ltmp22:
	mov	r5, r0
	mov	r11, r1
	cmp	r5, #0
	ldr	r6, [r5, #20]!
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC7_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC7_2+8))
LPC7_2:
	add	r0, pc, r0
	ldr	r10, [r0, #24]
	beq	LBB7_5
	ldr	r4, [r0, #20]
	b	LBB7_3
LBB7_2:
	mov	r8, r4
	mov	r0, r5
	mov	r2, r6
	bl	_p_5_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs__System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_llvm
	cmp	r0, r6
	mov	r6, r0
	popeq	{r8, r10, r11}
	popeq	{r4, r5, r6, r7, pc}
LBB7_3:
	mov	r0, r6
	mov	r1, r11
	bl	_p_6_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	mov	r1, r0
	cmp	r1, #0
	beq	LBB7_2
	ldr	r0, [r1]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB7_8
	b	LBB7_2
LBB7_5:
	mov	r0, r6
	mov	r1, r11
	bl	_p_6_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm
	cmp	r0, #0
	beq	LBB7_7
	ldr	r0, [r0]
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	bne	LBB7_8
Ltmp23:
LBB7_7:
	ldr	r0, LCPI7_1
LPC7_1:
	add	r1, pc, r0
	movw	r0, #630
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
Ltmp24:
LBB7_8:
	ldr	r0, LCPI7_0
LPC7_0:
	add	r1, pc, r0
	movw	r0, #605
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI7_0:
	.long	Ltmp24-(LPC7_0+8)
LCPI7_1:
	.long	Ltmp23-(LPC7_1+8)
	.end_data_region
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor:
Leh_func_begin8:
	push	{r7, lr}
Ltmp25:
	mov	r7, sp
Ltmp26:
Ltmp27:
	bl	_p_7_plt_MonoTouch_UIKit_UIApplicationDelegate__ctor_llvm
	pop	{r7, pc}
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_FormFactor
	.align	3
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_FormFactor:
Leh_func_begin9:
	push	{r7, lr}
Ltmp28:
	mov	r7, sp
Ltmp29:
Ltmp30:
	vpush	{d8}
	sub	sp, sp, #16
	bic	sp, sp, #7
	mov	r0, #0
	str	r0, [sp, #12]
	str	r0, [sp, #8]
	str	r0, [sp, #4]
	str	r0, [sp]
	bl	_p_8_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm
	ldr	r1, [r0]
	bl	_p_9_plt_MonoTouch_UIKit_UIDevice_get_UserInterfaceIdiom_llvm
	cmp	r0, #1
	bhi	LBB9_4
	mov	r0, #1
	beq	LBB9_3
	bl	_p_12_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	ldr	r1, [r0]
	ldr	r2, [r1, #84]
	mov	r1, sp
	blx	r2
	vldr	s16, [sp, #12]
	bl	_p_12_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #76]
	blx	r1
	vldr	d0, LCPI9_0
	vmov	s2, r0
	vcvt.f64.f32	d2, s16
	mov	r0, #2
	vcvt.f64.f32	d1, s2
	vmul.f64	d1, d2, d1
	vcmpe.f64	d1, d0
	vmrs	APSR_nzcv, fpscr
	movlt	r0, #0
LBB9_3:
	sub	sp, r7, #8
	vpop	{d8}
	pop	{r7, pc}
LBB9_4:
	movw	r0, #520
	movt	r0, #512
	bl	_p_10_plt__jit_icall_mono_create_corlib_exception_0_llvm
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
	.align	3
	.data_region
LCPI9_0:
	.long	0
	.long	1083293696
	.end_data_region
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_DisplayDensity
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_DisplayDensity:
Leh_func_begin10:
	push	{r4, r5, r7, lr}
Ltmp31:
	add	r7, sp, #8
Ltmp32:
	push	{r10, r11}
Ltmp33:
	bl	_p_12_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC10_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC10_0+8))
LPC10_0:
	add	r0, pc, r0
	ldrd	r10, r11, [r0, #28]
	mov	r0, r11
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r10
	mov	r5, r0
	bl	_p_14_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #60]
	mov	r0, r4
	blx	r2
	mov	r1, r0
	mov	r0, #0
	tst	r1, #255
	beq	LBB10_2
	bl	_p_12_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm
	ldr	r1, [r0]
	ldr	r1, [r1, #76]
	blx	r1
	vmov	s0, r0
	vcvt.f64.f32	d0, s0
	vmov	r0, r1, d0
	bl	_p_15_plt_System_Math_Round_double_llvm
	vmov	d0, r0, r1
	mov	r0, #0
	vcvt.s32.f64	s0, d0
	vmov	r1, s0
	cmp	r1, #2
	moveq	r0, #1
LBB10_2:
	pop	{r10, r11}
	pop	{r4, r5, r7, pc}
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties__ctor:
Leh_func_begin11:
	bx	lr
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp34:
	add	r7, sp, #8
Ltmp35:
	push	{r8}
Ltmp36:
	sub	sp, sp, #4
	mov	r4, r0
	mov	r5, #0
	str	r8, [sp]
	cmp	r1, #0
	beq	LBB12_2
	mov	r0, r1
	bl	_p_18_plt_Cirrious_MvvmCross_Platform_MvxSimplePropertyDictionaryExtensionMethods_ToSimplePropertyDictionary_object_llvm
	mov	r5, r0
LBB12_2:
	ldr	r0, [sp]
	bl	_p_16_plt__rgctx_fetch_0_llvm
	mov	r8, r0
	mov	r0, r4
	mov	r1, r5
	bl	_p_17_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string_llvm
	sub	sp, r7, #12
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string:
Leh_func_begin13:
	push	{r4, r5, r6, r7, lr}
Ltmp37:
	add	r7, sp, #12
Ltmp38:
	push	{r8, r10}
Ltmp39:
	sub	sp, sp, #4
	str	r8, [sp]
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC13_0+8))
	mov	r10, r0
	mov	r6, r1
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC13_0+8))
LPC13_0:
	add	r4, pc, r4
	ldr	r0, [r4, #36]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r6
	mov	r5, r0
	bl	_p_19_plt_Cirrious_MvvmCross_ViewModels_MvxBundle__ctor_System_Collections_Generic_IDictionary_2_string_string_llvm
	ldr	r0, [r4, #40]
	ldr	r6, [r0]
	ldr	r0, [sp]
	bl	_p_20_plt__rgctx_fetch_1_llvm
	bl	_p_21_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r5
	mov	r2, #0
	mov	r3, r6
	mov	r4, r0
	bl	_p_22_plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1_TTargetViewModel__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm
	mov	r0, r10
	mov	r1, r4
	bl	_p_23_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_llvm
	sub	sp, r7, #20
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end13:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin14:
	push	{r4, r5, r7, lr}
Ltmp40:
	add	r7, sp, #8
Ltmp41:
	push	{r8}
Ltmp42:
	sub	sp, sp, #4
	str	r8, [sp]
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC14_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC14_0+8))
LPC14_0:
	add	r5, pc, r5
	ldr	r0, [r5, #44]
	mov	r8, r0
	bl	_p_24_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #48]
	sub	r2, r2, #12
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	sub	sp, r7, #12
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end14:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin15:
	push	{r4, r5, r7, lr}
Ltmp43:
	add	r7, sp, #8
Ltmp44:
	push	{r8}
Ltmp45:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC15_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC15_0+8))
LPC15_0:
	add	r5, pc, r5
	ldr	r0, [r5, #44]
	mov	r8, r0
	bl	_p_24_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #48]
	sub	r2, r2, #12
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end15:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin16:
	push	{r4, r5, r7, lr}
Ltmp46:
	add	r7, sp, #8
Ltmp47:
	push	{r8}
Ltmp48:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC16_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC16_0+8))
LPC16_0:
	add	r5, pc, r5
	ldr	r0, [r5, #44]
	mov	r8, r0
	bl	_p_24_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #52]
	sub	r2, r2, #20
	mov	r8, r1
	mov	r1, r4
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end16:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute__ctor_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute__ctor_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor:
Leh_func_begin17:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end17:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_Target
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_Target:
Leh_func_begin18:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end18:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_set_Target_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_set_Target_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor:
Leh_func_begin19:
	str	r1, [r0, #8]
	bx	lr
Leh_func_end19:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_IsConditionSatisfied
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_IsConditionSatisfied:
Leh_func_begin20:
	push	{r4, r5, r7, lr}
Ltmp49:
	add	r7, sp, #8
Ltmp50:
	push	{r8}
Ltmp51:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC20_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC20_0+8))
LPC20_0:
	add	r5, pc, r5
	ldr	r0, [r5, #56]
	mov	r8, r0
	bl	_p_25_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_llvm
	ldr	r2, [r0]
	ldr	r1, [r5, #60]
	sub	r2, r2, #24
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	ldr	r2, [r4, #8]
	mov	r1, r0
	mov	r0, #0
	cmp	r1, r2
	moveq	r0, #1
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end20:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
Leh_func_begin21:
	push	{r4, r5, r7, lr}
Ltmp52:
	add	r7, sp, #8
Ltmp53:
Ltmp54:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC21_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC21_0+8))
LPC21_0:
	add	r5, pc, r5
	ldr	r0, [r5, #64]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	bl	_p_26_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	ldr	r0, [r5, #68]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	bl	_p_27_plt_Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r5, r7, pc}
Leh_func_end21:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_get_TouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_get_TouchView:
Leh_func_begin22:
	ldr	r1, [r0, #8]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB22_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC22_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC22_0+8))
	ldr	r2, [r1]
LPC22_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB22_2:
	cmp	r0, #0
	beq	LBB22_4
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC22_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC22_1+8))
	ldr	r2, [r0]
LPC22_1:
	add	r1, pc, r1
	ldr	r1, [r1, #72]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	bhs	LBB22_5
LBB22_4:
	mov	r0, #0
	bx	lr
LBB22_5:
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	bx	lr
Leh_func_end22:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
Leh_func_begin23:
	push	{r4, r5, r6, r7, lr}
Ltmp55:
	add	r7, sp, #12
Ltmp56:
	push	{r8}
Ltmp57:
	mov	r5, r1
	mov	r4, r0
	bl	_p_28_plt_Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	cmp	r5, #0
	beq	LBB23_9
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_0+8))
	ldr	r1, [r5]
LPC23_0:
	add	r6, pc, r6
	ldr	r0, [r6, #72]
	ldrh	r2, [r1, #20]
	cmp	r2, r0
	blo	LBB23_9
	ldr	r1, [r1, #16]
	mov	r2, #1
	ldrb	r1, [r1, r0, asr #3]
	and	r0, r0, #7
	tst	r1, r2, lsl r0
	cmpne	r5, #0
	beq	LBB23_9
	ldr	r1, [r4, #8]
	mov	r4, #0
	mov	r0, #0
	cmp	r1, #0
	beq	LBB23_5
	ldr	r2, [r1]
	ldr	r0, [r6, #76]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB23_5:
	cmp	r0, #0
	beq	LBB23_8
	ldr	r2, [r0]
	ldr	r1, [r6, #72]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	blo	LBB23_8
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	mov	r4, r0
LBB23_8:
	ldr	r0, [r6, #80]
	bl	_p_21_plt__jit_icall_mono_object_new_specific_llvm
	mov	r5, r0
	bl	_p_29_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContext__ctor_llvm
	ldr	r1, [r4]
	ldr	r0, [r6, #84]
	ldr	r2, [r1, #-4]!
	mov	r8, r0
	mov	r0, r4
	mov	r1, r5
	blx	r2
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
LBB23_9:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_1+8))
	mov	r1, #13
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_1+8))
LPC23_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_2+8))
	mov	r1, #37
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC23_2+8))
LPC23_2:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end23:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs:
Leh_func_begin24:
	push	{r4, r5, r6, r7, lr}
Ltmp58:
	add	r7, sp, #12
Ltmp59:
	push	{r10, r11}
Ltmp60:
	mov	r5, r0
	mov	r0, #0
	ldr	r1, [r5, #8]
	cmp	r1, #0
	beq	LBB24_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_0+8))
	ldr	r2, [r1]
LPC24_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB24_2:
	cmp	r0, #0
	beq	LBB24_5
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_1+8))
	ldr	r3, [r0]
LPC24_1:
	add	r1, pc, r1
	ldr	r2, [r1, #72]
	ldrh	r6, [r3, #20]
	cmp	r6, r2
	blo	LBB24_5
	ldr	r3, [r3, #16]
	mov	r6, #1
	ldrb	r3, [r3, r2, asr #3]
	and	r2, r2, #7
	tst	r3, r6, lsl r2
	cmpne	r0, #0
	bne	LBB24_11
LBB24_5:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_2+8))
	mov	r1, #1
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC24_2+8))
LPC24_2:
	add	r6, pc, r6
	ldrd	r10, r11, [r6, #88]
	mov	r0, r11
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	ldr	r2, [r5, #8]
	mov	r4, r0
	mov	r0, #0
	mov	r1, #0
	cmp	r2, #0
	beq	LBB24_7
	ldr	r3, [r2]
	ldr	r1, [r6, #76]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #12]
	cmp	r3, r1
	movne	r2, #0
	mov	r1, r2
LBB24_7:
	cmp	r1, #0
	beq	LBB24_10
	ldr	r3, [r1]
	ldr	r2, [r6, #72]
	ldrh	r6, [r3, #20]
	cmp	r6, r2
	blo	LBB24_10
	ldr	r0, [r3, #16]
	mov	r3, #1
	ldrb	r0, [r0, r2, asr #3]
	and	r2, r2, #7
	and	r0, r0, r3, lsl r2
	cmp	r0, #0
	moveq	r1, r0
	mov	r0, r1
LBB24_10:
	ldr	r0, [r0]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r4]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	mov	r0, r10
	mov	r1, r4
	bl	_p_34_plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
LBB24_11:
	ldr	r3, [r5, #8]
	mov	r0, #0
	mov	r2, #0
	cmp	r3, #0
	beq	LBB24_13
	ldr	r6, [r3]
	ldr	r2, [r1, #76]
	ldr	r6, [r6]
	ldr	r6, [r6, #8]
	ldr	r6, [r6, #12]
	cmp	r6, r2
	movne	r3, #0
	mov	r2, r3
LBB24_13:
	cmp	r2, #0
	beq	LBB24_16
	ldr	r3, [r2]
	ldr	r1, [r1, #72]
	ldrh	r6, [r3, #20]
	cmp	r6, r1
	blo	LBB24_16
	ldr	r0, [r3, #16]
	mov	r3, #1
	ldrb	r0, [r0, r1, asr #3]
	and	r1, r1, #7
	and	r0, r0, r3, lsl r1
	cmp	r0, #0
	moveq	r2, r0
	mov	r0, r2
LBB24_16:
	bl	_p_32_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_ClearAllBindings_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end24:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor:
Leh_func_begin25:
	push	{r4, r7, lr}
Ltmp61:
	add	r7, sp, #4
Ltmp62:
Ltmp63:
	mov	r4, r0
	bl	_p_35_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end25:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor_intptr:
Leh_func_begin26:
	push	{r4, r7, lr}
Ltmp64:
	add	r7, sp, #4
Ltmp65:
Ltmp66:
	mov	r4, r0
	bl	_p_37_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end26:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext:
Leh_func_begin27:
	push	{r7, lr}
Ltmp67:
	mov	r7, sp
Ltmp68:
	push	{r8}
Ltmp69:
	mov	r1, r0
	mov	r0, #0
	ldr	r2, [r1, #76]
	cmp	r2, #0
	beq	LBB27_2
	ldr	r0, [r1, #76]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC27_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC27_0+8))
LPC27_0:
	add	r1, pc, r1
	ldr	r1, [r1, #96]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
LBB27_2:
	pop	{r8}
	pop	{r7, pc}
Leh_func_end27:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object:
Leh_func_begin28:
	push	{r7, lr}
Ltmp70:
	mov	r7, sp
Ltmp71:
	push	{r8}
Ltmp72:
	ldr	r0, [r0, #76]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC28_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC28_0+8))
LPC28_0:
	add	r3, pc, r3
	ldr	r3, [r3, #100]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end28:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_ViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_ViewModel:
Leh_func_begin29:
	push	{r7, lr}
Ltmp73:
	mov	r7, sp
Ltmp74:
Ltmp75:
	bl	_p_38_plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext_llvm
	mov	r9, #0
	cmp	r0, #0
	beq	LBB29_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC29_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC29_0+8))
	ldr	r3, [r0]
LPC29_0:
	add	r2, pc, r2
	ldr	r2, [r2, #104]
	ldrh	r1, [r3, #20]
	cmp	r1, r2
	blo	LBB29_3
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r9, r0
LBB29_3:
	mov	r0, r9
	pop	{r7, pc}
Leh_func_end29:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin30:
	push	{r7, lr}
Ltmp76:
	mov	r7, sp
Ltmp77:
Ltmp78:
	bl	_p_39_plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object_llvm
	pop	{r7, pc}
Leh_func_end30:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_Request
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_Request:
Leh_func_begin31:
	ldr	r0, [r0, #72]
	bx	lr
Leh_func_end31:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin32:
	str	r1, [r0, #72]
	bx	lr
Leh_func_end32:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_BindingContext:
Leh_func_begin33:
	ldr	r0, [r0, #76]
	bx	lr
Leh_func_end33:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin34:
	str	r1, [r0, #76]
	bx	lr
Leh_func_end34:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_get_TouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_get_TouchView:
Leh_func_begin35:
	ldr	r1, [r0, #8]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB35_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC35_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC35_0+8))
	ldr	r2, [r1]
LPC35_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r0
	movne	r1, #0
	mov	r0, r1
LBB35_2:
	cmp	r0, #0
	beq	LBB35_4
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC35_1+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC35_1+8))
	ldr	r2, [r0]
LPC35_1:
	add	r1, pc, r1
	ldr	r1, [r1, #72]
	ldrh	r3, [r2, #20]
	cmp	r3, r1
	bhs	LBB35_5
LBB35_4:
	mov	r0, #0
	bx	lr
LBB35_5:
	ldr	r2, [r2, #16]
	mov	r3, #1
	ldrb	r2, [r2, r1, asr #3]
	and	r1, r1, #7
	and	r1, r2, r3, lsl r1
	cmp	r1, #0
	moveq	r0, r1
	bx	lr
Leh_func_end35:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
Leh_func_begin36:
	push	{r4, r7, lr}
Ltmp79:
	add	r7, sp, #4
Ltmp80:
Ltmp81:
	mov	r4, r1
	bl	_p_28_plt_Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	cmp	r4, #0
	beq	LBB36_4
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_0+8))
	ldr	r1, [r4]
LPC36_0:
	add	r0, pc, r0
	ldr	r0, [r0, #72]
	ldrh	r2, [r1, #20]
	cmp	r2, r0
	blo	LBB36_4
	ldr	r1, [r1, #16]
	mov	r2, #1
	ldrb	r1, [r1, r0, asr #3]
	and	r0, r0, #7
	tst	r1, r2, lsl r0
	cmpne	r4, #0
	beq	LBB36_4
	pop	{r4, r7, pc}
LBB36_4:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_1+8))
	mov	r1, #13
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_1+8))
LPC36_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_2+8))
	mov	r1, #37
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC36_2+8))
LPC36_2:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r2, r0
	movw	r0, #518
	mov	r1, r4
	movt	r0, #512
	bl	_p_31_plt__jit_icall_mono_create_corlib_exception_2_llvm
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end36:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs:
Leh_func_begin37:
	push	{r7, lr}
Ltmp82:
	mov	r7, sp
Ltmp83:
Ltmp84:
	ldr	r2, [r0, #8]
	mov	r0, #0
	mov	r1, #0
	cmp	r2, #0
	beq	LBB37_2
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC37_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC37_0+8))
	ldr	r3, [r2]
LPC37_0:
	add	r1, pc, r1
	ldr	r1, [r1, #76]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #12]
	cmp	r3, r1
	movne	r2, #0
	mov	r1, r2
LBB37_2:
	cmp	r1, #0
	beq	LBB37_5
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC37_1+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC37_1+8))
	ldr	r9, [r1]
LPC37_1:
	add	r2, pc, r2
	ldr	r2, [r2, #72]
	ldrh	r3, [r9, #20]
	cmp	r3, r2
	blo	LBB37_5
	ldr	r0, [r9, #16]
	mov	r3, #1
	ldrb	r0, [r0, r2, asr #3]
	and	r2, r2, #7
	and	r0, r0, r3, lsl r2
	cmp	r0, #0
	moveq	r1, r0
	mov	r0, r1
LBB37_5:
	bl	_p_40_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm
	pop	{r7, pc}
Leh_func_end37:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs:
Leh_func_begin38:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end38:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle:
Leh_func_begin39:
	push	{r4, r7, lr}
Ltmp85:
	add	r7, sp, #4
Ltmp86:
Ltmp87:
	mov	r4, r0
	bl	_p_41_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end39:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_intptr:
Leh_func_begin40:
	push	{r4, r7, lr}
Ltmp88:
	add	r7, sp, #4
Ltmp89:
Ltmp90:
	mov	r4, r0
	bl	_p_42_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end40:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin41:
	push	{r4, r7, lr}
Ltmp91:
	add	r7, sp, #4
Ltmp92:
Ltmp93:
	mov	r4, r0
	bl	_p_43_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end41:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext:
Leh_func_begin42:
	push	{r7, lr}
Ltmp94:
	mov	r7, sp
Ltmp95:
	push	{r8}
Ltmp96:
	ldr	r0, [r0, #80]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC42_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC42_0+8))
LPC42_0:
	add	r1, pc, r1
	ldr	r1, [r1, #96]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end42:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object:
Leh_func_begin43:
	push	{r7, lr}
Ltmp97:
	mov	r7, sp
Ltmp98:
	push	{r8}
Ltmp99:
	ldr	r0, [r0, #80]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC43_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC43_0+8))
LPC43_0:
	add	r3, pc, r3
	ldr	r3, [r3, #100]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end43:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_ViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_ViewModel:
Leh_func_begin44:
	push	{r7, lr}
Ltmp100:
	mov	r7, sp
Ltmp101:
Ltmp102:
	bl	_p_44_plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext_llvm
	mov	r9, #0
	cmp	r0, #0
	beq	LBB44_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC44_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC44_0+8))
	ldr	r3, [r0]
LPC44_0:
	add	r2, pc, r2
	ldr	r2, [r2, #104]
	ldrh	r1, [r3, #20]
	cmp	r1, r2
	blo	LBB44_3
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r9, r0
LBB44_3:
	mov	r0, r9
	pop	{r7, pc}
Leh_func_end44:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin45:
	push	{r7, lr}
Ltmp103:
	mov	r7, sp
Ltmp104:
Ltmp105:
	bl	_p_45_plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object_llvm
	pop	{r7, pc}
Leh_func_end45:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_Request
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_Request:
Leh_func_begin46:
	ldr	r0, [r0, #76]
	bx	lr
Leh_func_end46:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin47:
	str	r1, [r0, #76]
	bx	lr
Leh_func_end47:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_BindingContext:
Leh_func_begin48:
	ldr	r0, [r0, #80]
	bx	lr
Leh_func_end48:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin49:
	str	r1, [r0, #80]
	bx	lr
Leh_func_end49:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_get_CurrentRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_get_CurrentRequest:
Leh_func_begin50:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end50:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_set_CurrentRequest_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_set_CurrentRequest_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin51:
	str	r1, [r0, #20]
	bx	lr
Leh_func_end51:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin52:
	push	{r4, r5, r6, r7, lr}
Ltmp106:
	add	r7, sp, #12
Ltmp107:
Ltmp108:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC52_0+8))
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC52_0+8))
LPC52_0:
	add	r0, pc, r0
	ldr	r0, [r0, #116]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r5
	mov	r6, r0
	bl	_p_50_plt_Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	ldr	r0, [r4]
	mov	r1, r6
	ldr	r2, [r0, #88]
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r6, r7, pc}
Leh_func_end52:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor:
Leh_func_begin53:
	push	{r7, lr}
Ltmp109:
	mov	r7, sp
Ltmp110:
Ltmp111:
	bl	_p_51_plt_Cirrious_MvvmCross_Views_MvxViewsContainer__ctor_llvm
	pop	{r7, pc}
Leh_func_end53:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor:
Leh_func_begin54:
	push	{r4, r5, r7, lr}
Ltmp112:
	add	r7, sp, #8
Ltmp113:
Ltmp114:
	mov	r4, r0
	bl	_p_52_plt__jit_icall_mono_domain_get_llvm
	mov	r5, r0
	mov	r0, r4
	bl	_p_53_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher__ctor_llvm
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC54_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC54_0+8))
LPC54_0:
	add	r0, pc, r0
	ldr	r1, [r0, #120]
	mov	r0, r5
	bl	_p_54_plt__jit_icall_mono_class_static_field_address_llvm
	ldr	r0, [r0]
	str	r0, [r4, #8]
	pop	{r4, r5, r7, pc}
Leh_func_end54:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action:
Leh_func_begin55:
	push	{r4, r5, r6, r7, lr}
Ltmp115:
	add	r7, sp, #12
Ltmp116:
	push	{r10, r11}
Ltmp117:
	mov	r6, r1
	mov	r10, r0
	bl	_p_52_plt__jit_icall_mono_domain_get_llvm
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC55_1+8))
	mov	r5, r0
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC55_1+8))
LPC55_1:
	add	r11, pc, r11
	ldr	r0, [r11, #124]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	mov	r0, r5
	str	r6, [r4, #8]
	ldr	r6, [r10, #8]
	ldr	r1, [r11, #120]
	bl	_p_54_plt__jit_icall_mono_class_static_field_address_llvm
	ldr	r0, [r0]
	cmp	r6, r0
	bne	LBB55_2
	ldr	r0, [r4, #8]
	ldr	r1, [r0, #12]
	blx	r1
	b	LBB55_4
LBB55_2:
	bl	_p_55_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm
	mov	r5, r0
	cmp	r4, #0
	beq	LBB55_5
	ldr	r0, [r11, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	ldr	r0, [r11, #132]
	str	r4, [r1, #16]
	str	r0, [r1, #20]
	ldr	r0, [r11, #136]
	str	r0, [r1, #28]
	ldr	r0, [r11, #140]
	str	r0, [r1, #12]
	ldr	r0, [r5]
	mov	r0, r5
	bl	_p_56_plt_MonoTouch_Foundation_NSObject_BeginInvokeOnMainThread_MonoTouch_Foundation_NSAction_llvm
LBB55_4:
	mov	r0, #1
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp118:
LBB55_5:
	ldr	r0, LCPI55_0
LPC55_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI55_0:
	.long	Ltmp118-(LPC55_0+8)
	.end_data_region
Leh_func_end55:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
Leh_func_begin56:
	push	{r4, r5, r7, lr}
Ltmp119:
	add	r7, sp, #8
Ltmp120:
Ltmp121:
	mov	r4, r1
	mov	r5, r0
	bl	_p_57_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor_llvm
	str	r4, [r5, #12]
	pop	{r4, r5, r7, pc}
Leh_func_end56:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ShowViewModel_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ShowViewModel_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin57:
	push	{r4, r5, r7, lr}
Ltmp122:
	add	r7, sp, #8
Ltmp123:
	push	{r10, r11}
Ltmp124:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC57_1+8))
	mov	r10, r0
	mov	r11, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC57_1+8))
LPC57_1:
	add	r5, pc, r5
	ldr	r0, [r5, #144]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	strd	r10, r11, [r4, #8]
	cmp	r4, #0
	beq	LBB57_2
	ldr	r0, [r5, #148]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r5, #152]
	str	r0, [r1, #20]
	ldr	r0, [r5, #156]
	str	r0, [r1, #28]
	ldr	r0, [r5, #160]
	str	r0, [r1, #12]
	mov	r0, r10
	bl	_p_58_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action_llvm
	pop	{r10, r11}
	pop	{r4, r5, r7, pc}
Ltmp125:
LBB57_2:
	ldr	r0, LCPI57_0
LPC57_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI57_0:
	.long	Ltmp125-(LPC57_0+8)
	.end_data_region
Leh_func_end57:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint:
Leh_func_begin58:
	push	{r4, r5, r7, lr}
Ltmp126:
	add	r7, sp, #8
Ltmp127:
	push	{r10, r11}
Ltmp128:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC58_1+8))
	mov	r10, r0
	mov	r11, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC58_1+8))
LPC58_1:
	add	r5, pc, r5
	ldr	r0, [r5, #164]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	strd	r10, r11, [r4, #8]
	cmp	r4, #0
	beq	LBB58_2
	ldr	r0, [r5, #148]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r4, [r1, #16]
	ldr	r0, [r5, #168]
	str	r0, [r1, #20]
	ldr	r0, [r5, #172]
	str	r0, [r1, #28]
	ldr	r0, [r5, #160]
	str	r0, [r1, #12]
	mov	r0, r10
	bl	_p_58_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action_llvm
	pop	{r10, r11}
	pop	{r4, r5, r7, pc}
Ltmp129:
LBB58_2:
	ldr	r0, LCPI58_0
LPC58_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI58_0:
	.long	Ltmp129-(LPC58_0+8)
	.end_data_region
Leh_func_end58:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string:
Leh_func_begin59:
	push	{r4, r5, r6, r7, lr}
Ltmp130:
	add	r7, sp, #12
Ltmp131:
	push	{r10, r11}
Ltmp132:
	movw	r11, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC59_0+8))
	mov	r10, r1
	mov	r1, #5
	mov	r5, r3
	mov	r6, r2
	movt	r11, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC59_0+8))
LPC59_0:
	add	r11, pc, r11
	ldr	r0, [r11, #92]
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r4, r0
	mov	r1, #0
	mov	r2, r6
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r2, [r11, #176]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r11, #180]
	bl	_p_59_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #2
	str	r10, [r2, #8]
	ldr	r0, [r4]
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r0, [r4]
	ldr	r2, [r11, #176]
	mov	r1, #3
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	ldr	r1, [r5, #12]
	mov	r0, r5
	blx	r1
	mov	r2, r0
	ldr	r0, [r4]
	mov	r1, #4
	ldr	r3, [r0, #128]
	mov	r0, r4
	blx	r3
	mov	r0, r4
	bl	_p_60_plt_string_Concat_object___llvm
	bl	_p_61_plt_System_Console_WriteLine_string_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end59:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string:
Leh_func_begin60:
	push	{r4, r5, r6, r7, lr}
Ltmp133:
	add	r7, sp, #12
Ltmp134:
	push	{r10, r11}
Ltmp135:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC60_0+8))
	mov	r11, r1
	mov	r1, #5
	mov	r10, r3
	mov	r4, r2
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC60_0+8))
LPC60_0:
	add	r6, pc, r6
	ldr	r0, [r6, #92]
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	mov	r1, #0
	mov	r2, r4
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [r6, #184]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r6, #180]
	bl	_p_59_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #2
	str	r11, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [r6, #184]
	mov	r1, #3
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #4
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r5
	bl	_p_60_plt_string_Concat_object___llvm
	bl	_p_61_plt_System_Console_WriteLine_string_llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end60:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__:
Leh_func_begin61:
	push	{r4, r5, r6, r7, lr}
Ltmp136:
	add	r7, sp, #12
Ltmp137:
	push	{r10, r11}
Ltmp138:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC61_0+8))
	mov	r11, r1
	mov	r1, #5
	mov	r10, r3
	mov	r4, r2
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC61_0+8))
LPC61_0:
	add	r6, pc, r6
	ldr	r0, [r6, #92]
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	mov	r1, #0
	mov	r2, r4
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [r6, #184]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r6, #180]
	bl	_p_59_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	mov	r2, r0
	mov	r1, #2
	str	r11, [r2, #8]
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [r6, #184]
	mov	r1, #3
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	mov	r1, #4
	mov	r2, r10
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r5
	bl	_p_60_plt_string_Concat_object___llvm
	ldr	r1, [r7, #8]
	bl	_p_62_plt_System_Console_WriteLine_string_object___llvm
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end61:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace__ctor:
Leh_func_begin62:
	bx	lr
Leh_func_end62:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor:
Leh_func_begin63:
	push	{r4, r7, lr}
Ltmp139:
	add	r7, sp, #4
Ltmp140:
Ltmp141:
	mov	r4, r0
	bl	_p_63_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end63:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_intptr:
Leh_func_begin64:
	push	{r4, r7, lr}
Ltmp142:
	add	r7, sp, #4
Ltmp143:
Ltmp144:
	mov	r4, r0
	bl	_p_64_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end64:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin65:
	push	{r4, r7, lr}
Ltmp145:
	add	r7, sp, #4
Ltmp146:
Ltmp147:
	mov	r4, r0
	bl	_p_65_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end65:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext:
Leh_func_begin66:
	push	{r7, lr}
Ltmp148:
	mov	r7, sp
Ltmp149:
	push	{r8}
Ltmp150:
	ldr	r0, [r0, #76]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC66_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC66_0+8))
LPC66_0:
	add	r1, pc, r1
	ldr	r1, [r1, #96]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end66:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object:
Leh_func_begin67:
	push	{r7, lr}
Ltmp151:
	mov	r7, sp
Ltmp152:
	push	{r8}
Ltmp153:
	ldr	r0, [r0, #76]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC67_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC67_0+8))
LPC67_0:
	add	r3, pc, r3
	ldr	r3, [r3, #100]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end67:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel:
Leh_func_begin68:
	push	{r7, lr}
Ltmp154:
	mov	r7, sp
Ltmp155:
Ltmp156:
	bl	_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext_llvm
	mov	r9, #0
	cmp	r0, #0
	beq	LBB68_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC68_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC68_0+8))
	ldr	r3, [r0]
LPC68_0:
	add	r2, pc, r2
	ldr	r2, [r2, #104]
	ldrh	r1, [r3, #20]
	cmp	r1, r2
	blo	LBB68_3
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r9, r0
LBB68_3:
	mov	r0, r9
	pop	{r7, pc}
Leh_func_end68:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin69:
	push	{r7, lr}
Ltmp157:
	mov	r7, sp
Ltmp158:
Ltmp159:
	bl	_p_67_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object_llvm
	pop	{r7, pc}
Leh_func_end69:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_Request
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_Request:
Leh_func_begin70:
	ldr	r0, [r0, #72]
	bx	lr
Leh_func_end70:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin71:
	str	r1, [r0, #72]
	bx	lr
Leh_func_end71:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_BindingContext:
Leh_func_begin72:
	ldr	r0, [r0, #76]
	bx	lr
Leh_func_end72:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin73:
	str	r1, [r0, #76]
	bx	lr
Leh_func_end73:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest_get_ViewModelInstance
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest_get_ViewModelInstance:
Leh_func_begin74:
	ldr	r0, [r0, #24]
	bx	lr
Leh_func_end74:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin75:
	push	{r4, r5, r7, lr}
Ltmp160:
	add	r7, sp, #8
Ltmp161:
Ltmp162:
	sub	sp, sp, #4
	mov	r4, r1
	mov	r5, r0
	mov	r2, #0
	mov	r3, #0
	ldr	r0, [r4]
	ldr	r1, [r0, #12]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC75_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC75_0+8))
LPC75_0:
	add	r0, pc, r0
	ldr	r0, [r0, #188]
	ldr	r0, [r0]
	str	r0, [sp]
	mov	r0, r5
	bl	_p_68_plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest__ctor_System_Type_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm
	str	r4, [r5, #24]
	sub	sp, r7, #8
	pop	{r4, r5, r7, pc}
Leh_func_end75:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin76:
	bx	lr
Leh_func_end76:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint:
Leh_func_begin77:
	push	{r4, r5, r6, r7, lr}
Ltmp163:
	add	r7, sp, #12
Ltmp164:
Ltmp165:
	mov	r6, r1
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC77_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC77_0+8))
LPC77_0:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #192]
	mov	r1, #1
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r0, [r6]
	ldr	r0, [r0, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #72]
	blx	r1
	mov	r2, r0
	ldr	r0, [r5]
	mov	r1, #0
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	mov	r0, r4
	mov	r1, r5
	bl	_p_34_plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end77:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool:
Leh_func_begin78:
	mov	r0, #0
	bx	lr
Leh_func_end78:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn:
Leh_func_begin79:
	bx	lr
Leh_func_end79:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter__ctor:
Leh_func_begin80:
	bx	lr
Leh_func_end80:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_MasterNavigationController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_MasterNavigationController:
Leh_func_begin81:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end81:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_set_MasterNavigationController_MonoTouch_UIKit_UINavigationController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_set_MasterNavigationController_MonoTouch_UIKit_UINavigationController:
Leh_func_begin82:
	str	r1, [r0, #16]
	bx	lr
Leh_func_end82:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_ApplicationDelegate
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_ApplicationDelegate:
Leh_func_begin83:
	ldr	r0, [r0, #8]
	bx	lr
Leh_func_end83:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_Window
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_Window:
Leh_func_begin84:
	ldr	r0, [r0, #12]
	bx	lr
Leh_func_end84:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin85:
	mov	r3, r2
	mov	r2, r1
	strd	r2, r3, [r0, #8]
	bx	lr
Leh_func_end85:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin86:
	push	{r4, r7, lr}
Ltmp166:
	add	r7, sp, #4
Ltmp167:
Ltmp168:
	mov	r4, r0
	bl	_p_23_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_llvm
	mov	r1, r0
	ldr	r0, [r4]
	ldr	r2, [r0, #108]
	mov	r0, r4
	blx	r2
	pop	{r4, r7, pc}
Leh_func_end86:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint:
Leh_func_begin87:
	push	{r7, lr}
Ltmp169:
	mov	r7, sp
Ltmp170:
Ltmp171:
	cmp	r1, #0
	beq	LBB87_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC87_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC87_0+8))
	ldr	r3, [r1]
LPC87_0:
	add	r2, pc, r2
	ldr	r2, [r2, #196]
	ldr	r3, [r3]
	ldr	r3, [r3, #8]
	ldr	r3, [r3, #8]
	cmp	r3, r2
	bne	LBB87_6
	cmp	r1, #0
	bne	LBB87_4
	b	LBB87_6
LBB87_3:
	beq	LBB87_6
LBB87_4:
	cmp	r1, #0
	beq	LBB87_7
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC87_1+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC87_1+8))
LPC87_1:
	add	r2, pc, r2
	ldr	r9, [r2, #196]
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r3, [r2, #8]
	mov	r2, #0
	cmp	r3, r9
	moveq	r2, r1
	b	LBB87_8
LBB87_6:
	bl	_p_69_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint_llvm
	pop	{r7, pc}
LBB87_7:
	mov	r2, r1
LBB87_8:
	ldr	r1, [r2]
	ldr	r1, [r2, #12]
	ldr	r2, [r0]
	ldr	r2, [r2, #100]
	blx	r2
	pop	{r7, pc}
Leh_func_end87:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin88:
	push	{r4, r5, r7, lr}
Ltmp172:
	add	r7, sp, #8
Ltmp173:
Ltmp174:
	mov	r4, r1
	mov	r5, r0
	cmp	r4, #0
	beq	LBB88_2
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_0+8))
	ldr	r1, [r4]
LPC88_0:
	add	r0, pc, r0
	ldr	r0, [r0, #76]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #12]
	cmp	r1, r0
	movne	r4, #0
LBB88_2:
	cmp	r4, #0
	beq	LBB88_6
	ldr	r0, [r5]
	ldr	r1, [r0, #124]
	mov	r0, r5
	blx	r1
	ldr	r1, [r5]
	cmp	r0, #0
	beq	LBB88_5
	ldr	r1, [r1, #124]
	mov	r0, r5
	blx	r1
	ldr	r1, [r0]
	mov	r2, #1
	ldr	r3, [r1, #192]
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r7, pc}
LBB88_5:
	ldr	r2, [r1, #96]
	mov	r0, r5
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB88_6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_1+8))
	movw	r1, #427
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_1+8))
LPC88_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_2+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC88_2+8))
LPC88_2:
	add	r0, pc, r0
	ldr	r0, [r0, #112]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_49_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm
	mov	r0, r5
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end88:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController:
Leh_func_begin89:
	push	{r7, lr}
Ltmp175:
	mov	r7, sp
Ltmp176:
Ltmp177:
	ldr	r1, [r0]
	ldr	r1, [r1, #124]
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #188]
	mov	r1, #1
	blx	r2
	pop	{r7, pc}
Leh_func_end89:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin90:
	push	{r4, r5, r6, r7, lr}
Ltmp178:
	add	r7, sp, #12
Ltmp179:
Ltmp180:
	mov	r4, r0
	mov	r6, r1
	ldr	r0, [r4]
	ldr	r1, [r0, #124]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #216]
	blx	r1
	cmp	r0, #0
	beq	LBB90_7
	mov	r1, #0
	cmp	r0, #0
	beq	LBB90_4
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_1+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_1+8))
	ldr	r3, [r0]
LPC90_1:
	add	r2, pc, r2
	ldr	r2, [r2, #72]
	ldrh	r5, [r3, #20]
	cmp	r5, r2
	blo	LBB90_4
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r1, r0
LBB90_4:
	cmp	r1, #0
	beq	LBB90_8
	mov	r0, r1
	bl	_p_70_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView_llvm
	cmp	r0, r6
	bne	LBB90_9
	ldr	r0, [r4]
	ldr	r1, [r0, #124]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r2, [r1, #188]
	mov	r1, #1
	blx	r2
	pop	{r4, r5, r6, r7, pc}
LBB90_7:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_0+8))
LPC90_0:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #208]
	b	LBB90_10
LBB90_8:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_3+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_3+8))
LPC90_3:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #204]
	b	LBB90_10
LBB90_9:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC90_2+8))
LPC90_2:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #200]
LBB90_10:
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_34_plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end90:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool:
Leh_func_begin91:
	push	{r4, r5, r6, r7, lr}
Ltmp181:
	add	r7, sp, #12
Ltmp182:
	push	{r10, r11}
Ltmp183:
	mov	r11, r1
	ldr	r1, [r0]
	mov	r10, r2
	ldr	r1, [r1, #80]
	blx	r1
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC91_0+8))
	mov	r6, r0
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC91_0+8))
LPC91_0:
	add	r4, pc, r4
	ldr	r5, [r4, #212]
	ldr	r3, [r5]
	cmp	r3, #0
	bne	LBB91_2
	ldr	r0, [r4, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #216]
	str	r1, [r0, #20]
	ldr	r1, [r4, #220]
	str	r1, [r0, #28]
	ldr	r1, [r4, #140]
	str	r1, [r0, #12]
	str	r0, [r5]
	ldr	r0, [r4, #212]
	ldr	r3, [r0]
LBB91_2:
	ldr	r0, [r6]
	mov	r1, r11
	mov	r2, r10
	ldr	r5, [r0, #104]
	mov	r0, r6
	blx	r5
	mov	r0, #1
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end91:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn:
Leh_func_begin92:
	bx	lr
Leh_func_end92:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ShowFirstView_MonoTouch_UIKit_UIViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ShowFirstView_MonoTouch_UIKit_UIViewController:
Leh_func_begin93:
	push	{r4, r5, r6, r7, lr}
Ltmp184:
	add	r7, sp, #12
Ltmp185:
	push	{r10, r11}
Ltmp186:
	push	{r1}
	mov	r11, r0
	ldr	r0, [r11, #12]
	ldr	r1, [r0]
	ldr	r1, [r1, #188]
	blx	r1
	mov	r6, r0
	ldr	r5, [r6, #12]
	cmp	r5, #1
	blt	LBB93_4
	add	r4, r6, #16
	mov	r10, #0
LBB93_2:
	ldr	r0, [r6, #12]
	cmp	r0, r10
	bls	LBB93_5
	ldr	r0, [r4], #4
	ldr	r1, [r0]
	ldr	r1, [r1, #132]
	blx	r1
	add	r10, r10, #1
	cmp	r10, r5
	blt	LBB93_2
LBB93_4:
	ldr	r0, [r11]
	ldr	r1, [sp]
	ldr	r2, [r0, #84]
	mov	r0, r11
	blx	r2
	mov	r1, r0
	ldr	r0, [r11]
	ldr	r2, [r0, #120]
	mov	r0, r11
	blx	r2
	ldr	r0, [r11]
	ldr	r1, [r0, #88]
	mov	r0, r11
	blx	r1
	ldr	r0, [r11]
	ldr	r1, [r0, #124]
	mov	r0, r11
	blx	r1
	mov	r1, r0
	ldr	r0, [r11]
	ldr	r2, [r0, #92]
	mov	r0, r11
	blx	r2
	sub	sp, r7, #20
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Ltmp187:
LBB93_5:
	ldr	r0, LCPI93_0
LPC93_0:
	add	r1, pc, r0
	movw	r0, #600
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI93_0:
	.long	Ltmp187-(LPC93_0+8)
	.end_data_region
Leh_func_end93:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_SetWindowRootViewController_MonoTouch_UIKit_UIViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_SetWindowRootViewController_MonoTouch_UIKit_UIViewController:
Leh_func_begin94:
	push	{r4, r5, r6, r7, lr}
Ltmp188:
	add	r7, sp, #12
Ltmp189:
Ltmp190:
	mov	r5, r0
	mov	r4, r1
	ldr	r6, [r5, #12]
	ldr	r0, [r4]
	ldr	r1, [r0, #180]
	mov	r0, r4
	blx	r1
	mov	r1, r0
	ldr	r0, [r6]
	ldr	r2, [r0, #148]
	mov	r0, r6
	blx	r2
	ldr	r0, [r5, #12]
	ldr	r1, [r0]
	ldr	r2, [r1, #268]
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r6, r7, pc}
Leh_func_end94:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_OnMasterNavigationControllerCreated
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_OnMasterNavigationControllerCreated:
Leh_func_begin95:
	bx	lr
Leh_func_end95:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CreateNavigationController_MonoTouch_UIKit_UIViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CreateNavigationController_MonoTouch_UIKit_UIViewController:
Leh_func_begin96:
	push	{r4, r5, r7, lr}
Ltmp191:
	add	r7, sp, #8
Ltmp192:
Ltmp193:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC96_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC96_0+8))
LPC96_0:
	add	r0, pc, r0
	ldr	r0, [r0, #224]
	bl	_p_21_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_71_plt_MonoTouch_UIKit_UINavigationController__ctor_MonoTouch_UIKit_UIViewController_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end96:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_CurrentTopViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_CurrentTopViewController:
Leh_func_begin97:
	push	{r7, lr}
Ltmp194:
	mov	r7, sp
Ltmp195:
Ltmp196:
	ldr	r1, [r0]
	ldr	r1, [r1, #124]
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #216]
	blx	r1
	pop	{r7, pc}
Leh_func_end97:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__PresentModalViewControllerb__0
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__PresentModalViewControllerb__0:
Leh_func_begin98:
	bx	lr
Leh_func_end98:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin99:
	push	{r7, lr}
Ltmp197:
	mov	r7, sp
Ltmp198:
Ltmp199:
	bl	_p_72_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm
	pop	{r7, pc}
Leh_func_end99:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin100:
	push	{r4, r5, r6, r7, lr}
Ltmp200:
	add	r7, sp, #12
Ltmp201:
	push	{r10}
Ltmp202:
	mov	r5, r1
	mov	r4, r0
	cmp	r5, #0
	beq	LBB100_5
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC100_0+8))
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC100_0+8))
	ldr	r1, [r5]
LPC100_0:
	add	r10, pc, r10
	ldr	r0, [r10, #228]
	ldrh	r2, [r1, #20]
	cmp	r2, r0
	blo	LBB100_5
	ldr	r1, [r1, #16]
	mov	r2, #1
	ldrb	r1, [r1, r0, asr #3]
	and	r0, r0, #7
	tst	r1, r2, lsl r0
	cmpne	r5, #0
	beq	LBB100_5
	ldr	r0, [r4, #20]
	cmp	r0, #0
	bne	LBB100_6
	ldr	r0, [r4]
	ldr	r1, [r0, #128]
	mov	r0, r4
	blx	r1
	mov	r6, r0
	ldr	r0, [r5]
	ldr	r10, [r10, #76]
	mov	r1, #0
	mov	r2, #0
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	ldr	r0, [r6]
	moveq	r1, r5
	ldr	r3, [r0, #192]
	mov	r0, r6
	blx	r3
	ldr	r0, [r5]
	mov	r1, r6
	mov	r2, #1
	ldr	r0, [r0]
	ldr	r0, [r0, #8]
	ldr	r0, [r0, #12]
	cmp	r0, r10
	movne	r5, #0
	str	r5, [r4, #20]
	ldr	r0, [r4]
	ldr	r3, [r0, #68]
	mov	r0, r4
	blx	r3
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB100_5:
	mov	r0, r4
	mov	r1, r5
	bl	_p_73_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB100_6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC100_1+8))
	movw	r1, #941
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC100_1+8))
LPC100_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	ldr	r0, [r10, #112]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_49_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm
	mov	r0, r5
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end100:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CreateModalNavigationController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CreateModalNavigationController:
Leh_func_begin101:
	push	{r4, r7, lr}
Ltmp203:
	add	r7, sp, #4
Ltmp204:
Ltmp205:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC101_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC101_0+8))
LPC101_0:
	add	r0, pc, r0
	ldr	r0, [r0, #224]
	bl	_p_21_plt__jit_icall_mono_object_new_specific_llvm
	mov	r4, r0
	bl	_p_74_plt_MonoTouch_UIKit_UINavigationController__ctor_llvm
	mov	r0, r4
	pop	{r4, r7, pc}
Leh_func_end101:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn:
Leh_func_begin102:
	push	{r4, r7, lr}
Ltmp206:
	add	r7, sp, #4
Ltmp207:
Ltmp208:
	ldr	r1, [r0, #20]
	cmp	r1, #0
	moveq	r1, #0
	streq	r1, [r0, #20]
	popeq	{r4, r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC102_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC102_0+8))
LPC102_0:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #232]
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_75_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end102:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CloseModalViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CloseModalViewController:
Leh_func_begin103:
	push	{r4, r5, r6, r7, lr}
Ltmp209:
	add	r7, sp, #12
Ltmp210:
	push	{r10}
Ltmp211:
	mov	r10, r0
	ldr	r0, [r10, #20]
	cmp	r0, #0
	beq	LBB103_6
	ldr	r0, [r10, #20]
	ldr	r1, [r0]
	ldr	r1, [r1, #168]
	blx	r1
	mov	r5, r0
	cmp	r5, #0
	beq	LBB103_3
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_0+8))
	ldr	r1, [r5]
LPC103_0:
	add	r0, pc, r0
	ldr	r0, [r0, #260]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	movne	r5, #0
LBB103_3:
	cmp	r5, #0
	beq	LBB103_7
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_2+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_2+8))
LPC103_2:
	add	r6, pc, r6
	ldr	r0, [r6, #248]
	ldr	r2, [r0]
	cmp	r2, #0
	bne	LBB103_9
	ldr	r0, [r6, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #252]
	str	r1, [r0, #20]
	ldr	r1, [r6, #256]
	str	r1, [r0, #28]
	ldr	r1, [r6, #140]
	str	r1, [r0, #12]
	ldr	r1, [r6, #248]
	str	r0, [r1]
	ldr	r0, [r6, #248]
	ldr	r2, [r0]
	b	LBB103_9
LBB103_6:
	mov	r0, r10
	bl	_p_76_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB103_7:
	ldr	r5, [r10, #20]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC103_1+8))
LPC103_1:
	add	r6, pc, r6
	ldr	r4, [r6, #236]
	ldr	r2, [r4]
	cmp	r2, #0
	bne	LBB103_9
	ldr	r0, [r6, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #240]
	str	r1, [r0, #20]
	ldr	r1, [r6, #244]
	str	r1, [r0, #28]
	ldr	r1, [r6, #140]
	str	r1, [r0, #12]
	str	r0, [r4]
	ldr	r0, [r6, #236]
	ldr	r2, [r0]
LBB103_9:
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r3, [r0, #100]
	mov	r0, r5
	blx	r3
	mov	r0, #0
	str	r0, [r10, #20]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end103:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin104:
	push	{r4, r5, r6, r7, lr}
Ltmp212:
	add	r7, sp, #12
Ltmp213:
	push	{r10}
Ltmp214:
	mov	r10, r0
	mov	r5, r1
	ldr	r0, [r10, #20]
	cmp	r0, #0
	beq	LBB104_11
	ldr	r1, [r10, #20]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB104_4
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_0+8))
	ldr	r3, [r1]
LPC104_0:
	add	r2, pc, r2
	ldr	r2, [r2, #72]
	ldrh	r6, [r3, #20]
	cmp	r6, r2
	blo	LBB104_4
	ldr	r0, [r3, #16]
	mov	r3, #1
	ldrb	r0, [r0, r2, asr #3]
	and	r2, r2, #7
	and	r0, r0, r3, lsl r2
	cmp	r0, #0
	moveq	r1, r0
	mov	r0, r1
LBB104_4:
	cmp	r0, #0
	beq	LBB104_12
	bl	_p_70_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView_llvm
	cmp	r0, r5
	bne	LBB104_13
	ldr	r0, [r10, #20]
	ldr	r1, [r0]
	ldr	r1, [r1, #168]
	blx	r1
	mov	r5, r0
	cmp	r5, #0
	beq	LBB104_8
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_1+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_1+8))
	ldr	r1, [r5]
LPC104_1:
	add	r0, pc, r0
	ldr	r0, [r0, #260]
	ldr	r1, [r1]
	ldr	r1, [r1, #8]
	ldr	r1, [r1, #16]
	cmp	r1, r0
	movne	r5, #0
LBB104_8:
	cmp	r5, #0
	beq	LBB104_15
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_5+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_5+8))
LPC104_5:
	add	r6, pc, r6
	ldr	r4, [r6, #276]
	ldr	r2, [r4]
	cmp	r2, #0
	bne	LBB104_17
	ldr	r0, [r6, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #280]
	str	r1, [r0, #20]
	ldr	r1, [r6, #284]
	str	r1, [r0, #28]
	ldr	r1, [r6, #140]
	str	r1, [r0, #12]
	str	r0, [r4]
	ldr	r0, [r6, #276]
	ldr	r2, [r0]
	b	LBB104_17
LBB104_11:
	mov	r0, r10
	mov	r1, r5
	bl	_p_77_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB104_12:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_3+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_3+8))
LPC104_3:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #292]
	b	LBB104_14
LBB104_13:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_4+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_4+8))
LPC104_4:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #288]
LBB104_14:
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_75_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
LBB104_15:
	ldr	r5, [r10, #20]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_2+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC104_2+8))
LPC104_2:
	add	r6, pc, r6
	ldr	r0, [r6, #264]
	ldr	r2, [r0]
	cmp	r2, #0
	bne	LBB104_17
	ldr	r0, [r6, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #268]
	str	r1, [r0, #20]
	ldr	r1, [r6, #272]
	str	r1, [r0, #28]
	ldr	r1, [r6, #140]
	str	r1, [r0, #12]
	ldr	r1, [r6, #264]
	str	r0, [r1]
	ldr	r0, [r6, #264]
	ldr	r2, [r0]
LBB104_17:
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r3, [r0, #100]
	mov	r0, r5
	blx	r3
	mov	r0, #0
	str	r0, [r10, #20]
	pop	{r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end104:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__2
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__2:
Leh_func_begin105:
	bx	lr
Leh_func_end105:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__3
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__3:
Leh_func_begin106:
	bx	lr
Leh_func_end106:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__6
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__6:
Leh_func_begin107:
	bx	lr
Leh_func_end107:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__7
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__7:
Leh_func_begin108:
	bx	lr
Leh_func_end108:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin109:
	push	{r7, lr}
Ltmp215:
	mov	r7, sp
Ltmp216:
Ltmp217:
	bl	_p_72_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm
	pop	{r7, pc}
Leh_func_end109:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin110:
	push	{r4, r5, r6, r7, lr}
Ltmp218:
	add	r7, sp, #12
Ltmp219:
Ltmp220:
	cmp	r1, #0
	beq	LBB110_3
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC110_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC110_0+8))
	ldr	r2, [r1]
LPC110_0:
	add	r6, pc, r6
	ldr	r3, [r6, #228]
	ldrh	r4, [r2, #20]
	cmp	r4, r3
	blo	LBB110_3
	ldr	r4, [r2, #16]
	mov	r5, #1
	ldrb	r4, [r4, r3, asr #3]
	and	r3, r3, #7
	tst	r4, r5, lsl r3
	cmpne	r1, #0
	bne	LBB110_4
LBB110_3:
	bl	_p_73_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm
	pop	{r4, r5, r6, r7, pc}
LBB110_4:
	ldr	r3, [r0, #20]
	cmp	r3, #0
	bne	LBB110_6
	ldr	r2, [r2]
	ldr	r3, [r6, #76]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #12]
	cmp	r2, r3
	mov	r2, r1
	movne	r2, #0
	str	r2, [r0, #20]
	ldr	r2, [r1]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r5, [r2, #12]
	mov	r2, #0
	cmp	r5, r3
	moveq	r2, r1
	ldr	r1, [r0]
	ldr	r3, [r1, #68]
	mov	r1, r2
	mov	r2, #1
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB110_6:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC110_1+8))
	movw	r1, #941
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC110_1+8))
LPC110_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r4, r0
	ldr	r0, [r6, #112]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_49_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm
	mov	r0, r5
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end110:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn:
Leh_func_begin111:
	push	{r4, r7, lr}
Ltmp221:
	add	r7, sp, #4
Ltmp222:
Ltmp223:
	ldr	r1, [r0, #20]
	cmp	r1, #0
	moveq	r1, #0
	streq	r1, [r0, #20]
	popeq	{r4, r7, pc}
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC111_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC111_0+8))
LPC111_0:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #232]
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_75_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm
	pop	{r4, r7, pc}
Leh_func_end111:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_CloseModalViewController
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_CloseModalViewController:
Leh_func_begin112:
	push	{r4, r7, lr}
Ltmp224:
	add	r7, sp, #4
Ltmp225:
Ltmp226:
	mov	r4, r0
	ldr	r0, [r4, #20]
	cmp	r0, #0
	beq	LBB112_2
	ldr	r0, [r4, #20]
	ldr	r1, [r0]
	ldr	r2, [r1, #120]
	mov	r1, #1
	blx	r2
	mov	r0, #0
	str	r0, [r4, #20]
	pop	{r4, r7, pc}
LBB112_2:
	mov	r0, r4
	bl	_p_76_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end112:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin113:
	push	{r4, r5, r6, r7, lr}
Ltmp227:
	add	r7, sp, #12
Ltmp228:
Ltmp229:
	mov	r4, r0
	mov	r5, r1
	ldr	r0, [r4, #20]
	cmp	r0, #0
	beq	LBB113_9
	ldr	r1, [r4, #20]
	mov	r0, #0
	cmp	r1, #0
	beq	LBB113_4
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_0+8))
	ldr	r3, [r1]
LPC113_0:
	add	r2, pc, r2
	ldr	r2, [r2, #72]
	ldrh	r6, [r3, #20]
	cmp	r6, r2
	blo	LBB113_4
	ldr	r0, [r3, #16]
	mov	r3, #1
	ldrb	r0, [r0, r2, asr #3]
	and	r2, r2, #7
	and	r0, r0, r3, lsl r2
	cmp	r0, #0
	moveq	r1, r0
	mov	r0, r1
LBB113_4:
	cmp	r0, #0
	beq	LBB113_10
	bl	_p_70_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView_llvm
	cmp	r0, r5
	bne	LBB113_11
	ldr	r5, [r4, #20]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_1+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_1+8))
LPC113_1:
	add	r6, pc, r6
	ldr	r0, [r6, #296]
	ldr	r2, [r0]
	cmp	r2, #0
	bne	LBB113_8
	ldr	r0, [r6, #128]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r6, #300]
	str	r1, [r0, #20]
	ldr	r1, [r6, #304]
	str	r1, [r0, #28]
	ldr	r1, [r6, #140]
	str	r1, [r0, #12]
	ldr	r1, [r6, #296]
	str	r0, [r1]
	ldr	r0, [r6, #296]
	ldr	r2, [r0]
LBB113_8:
	ldr	r0, [r5]
	mov	r1, #1
	ldr	r3, [r0, #100]
	mov	r0, r5
	blx	r3
	mov	r0, #0
	str	r0, [r4, #20]
	pop	{r4, r5, r6, r7, pc}
LBB113_9:
	mov	r0, r4
	mov	r1, r5
	bl	_p_77_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r4, r5, r6, r7, pc}
LBB113_10:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_2+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_2+8))
LPC113_2:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #292]
	b	LBB113_12
LBB113_11:
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_3+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC113_3+8))
LPC113_3:
	add	r1, pc, r1
	ldr	r0, [r1, #92]
	ldr	r4, [r1, #288]
LBB113_12:
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r4
	bl	_p_75_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm
	pop	{r4, r5, r6, r7, pc}
Leh_func_end113:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__Closeb__0
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__Closeb__0:
Leh_func_begin114:
	bx	lr
Leh_func_end114:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow:
Leh_func_begin115:
	mov	r3, r2
	mov	r2, r1
	strd	r2, r3, [r0, #16]
	bx	lr
Leh_func_end115:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
Leh_func_begin116:
	str	r2, [r0, #24]
	str	r1, [r0, #16]
	bx	lr
Leh_func_end116:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Window
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Window:
Leh_func_begin117:
	ldr	r0, [r0, #20]
	bx	lr
Leh_func_end117:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ApplicationDelegate
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ApplicationDelegate:
Leh_func_begin118:
	ldr	r0, [r0, #16]
	bx	lr
Leh_func_end118:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateDebugTrace
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateDebugTrace:
Leh_func_begin119:
	push	{r7, lr}
Ltmp230:
	mov	r7, sp
Ltmp231:
Ltmp232:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC119_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC119_0+8))
LPC119_0:
	add	r0, pc, r0
	ldr	r0, [r0, #308]
	bl	_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm
	pop	{r7, pc}
Leh_func_end119:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePluginManager
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePluginManager:
Leh_func_begin120:
	push	{r4, r5, r6, r7, lr}
Ltmp233:
	add	r7, sp, #12
Ltmp234:
	push	{r10, r11}
Ltmp235:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC120_0+8))
	mov	r10, r0
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC120_0+8))
LPC120_0:
	add	r4, pc, r4
	ldr	r0, [r4, #312]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_78_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginManager__ctor_llvm
	ldr	r0, [r4, #320]
	ldr	r11, [r4, #316]
	ldr	r4, [r5, #16]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r11
	mov	r2, r4
	mov	r6, r0
	bl	_p_79_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry__ctor_string_System_Collections_Generic_IDictionary_2_string_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_llvm
	ldr	r0, [r10]
	mov	r1, r6
	ldr	r2, [r0, #268]
	mov	r0, r10
	blx	r2
	mov	r0, r5
	pop	{r10, r11}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end120:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry:
Leh_func_begin121:
	bx	lr
Leh_func_end121:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewsContainer
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewsContainer:
Leh_func_begin122:
	push	{r4, r5, r7, lr}
Ltmp236:
	add	r7, sp, #8
Ltmp237:
Ltmp238:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC122_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC122_0+8))
LPC122_0:
	add	r0, pc, r0
	ldr	r0, [r0, #324]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	bl	_p_80_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor_llvm
	ldr	r0, [r4]
	mov	r1, r5
	ldr	r2, [r0, #264]
	mov	r0, r4
	blx	r2
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end122:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterTouchViewCreator_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterTouchViewCreator_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer:
Leh_func_begin123:
	push	{r4, r5, r7, lr}
Ltmp239:
	add	r7, sp, #8
Ltmp240:
	push	{r8}
Ltmp241:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC123_0+8))
	mov	r4, r1
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC123_0+8))
LPC123_0:
	add	r5, pc, r5
	ldr	r0, [r5, #328]
	mov	r8, r0
	mov	r0, r4
	bl	_p_81_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm
	ldr	r0, [r5, #332]
	mov	r8, r0
	mov	r0, r4
	bl	_p_82_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end123:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewDispatcher
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewDispatcher:
Leh_func_begin124:
	push	{r4, r5, r7, lr}
Ltmp242:
	add	r7, sp, #8
Ltmp243:
Ltmp244:
	bl	_p_83_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter_llvm
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC124_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC124_0+8))
LPC124_0:
	add	r0, pc, r0
	ldr	r0, [r0, #336]
	bl	_p_21_plt__jit_icall_mono_object_new_specific_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_84_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm
	mov	r0, r5
	pop	{r4, r5, r7, pc}
Leh_func_end124:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializePlatformServices
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializePlatformServices:
Leh_func_begin125:
	push	{r4, r7, lr}
Ltmp245:
	add	r7, sp, #4
Ltmp246:
Ltmp247:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #260]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #252]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #240]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #248]
	mov	r0, r4
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end125:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPlatformProperties
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPlatformProperties:
Leh_func_begin126:
	push	{r7, lr}
Ltmp248:
	mov	r7, sp
Ltmp249:
	push	{r8}
Ltmp250:
	ldr	r1, [r0]
	ldr	r1, [r1, #256]
	blx	r1
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC126_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC126_0+8))
LPC126_0:
	add	r1, pc, r1
	ldr	r1, [r1, #340]
	mov	r8, r1
	bl	_p_85_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end126:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateTouchSystemProperties
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateTouchSystemProperties:
Leh_func_begin127:
	push	{r4, r7, lr}
Ltmp251:
	add	r7, sp, #4
Ltmp252:
Ltmp253:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC127_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC127_0+8))
LPC127_0:
	add	r0, pc, r0
	ldr	r0, [r0, #344]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	bl	_p_86_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor_llvm
	mov	r0, r4
	pop	{r4, r7, pc}
Leh_func_end127:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterOldStylePlatformProperties
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterOldStylePlatformProperties:
Leh_func_begin128:
	push	{r4, r7, lr}
Ltmp254:
	add	r7, sp, #4
Ltmp255:
	push	{r8}
Ltmp256:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC128_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC128_0+8))
LPC128_0:
	add	r4, pc, r4
	ldr	r0, [r4, #348]
	bl	_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #352]
	mov	r8, r1
	bl	_p_87_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_llvm
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end128:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterLifetime
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterLifetime:
Leh_func_begin129:
	push	{r7, lr}
Ltmp257:
	mov	r7, sp
Ltmp258:
	push	{r8}
Ltmp259:
	ldr	r0, [r0, #16]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC129_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC129_0+8))
LPC129_0:
	add	r1, pc, r1
	ldr	r1, [r1, #356]
	mov	r8, r1
	bl	_p_88_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Platform_IMvxLifetime_Cirrious_MvvmCross_Platform_IMvxLifetime_llvm
	pop	{r8}
	pop	{r7, pc}
Leh_func_end129:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter:
Leh_func_begin130:
	push	{r4, r7, lr}
Ltmp260:
	add	r7, sp, #4
Ltmp261:
Ltmp262:
	mov	r4, r0
	ldr	r0, [r4, #24]
	cmp	r0, #0
	bne	LBB130_2
	ldr	r0, [r4]
	ldr	r1, [r0, #244]
	mov	r0, r4
	blx	r1
LBB130_2:
	str	r0, [r4, #24]
	ldr	r0, [r4, #24]
	pop	{r4, r7, pc}
Leh_func_end130:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePresenter
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePresenter:
Leh_func_begin131:
	push	{r4, r5, r6, r7, lr}
Ltmp263:
	add	r7, sp, #12
Ltmp264:
Ltmp265:
	ldr	r4, [r0, #16]
	ldr	r5, [r0, #20]
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC131_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC131_0+8))
LPC131_0:
	add	r0, pc, r0
	ldr	r0, [r0, #360]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r5
	mov	r6, r0
	bl	_p_72_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm
	mov	r0, r6
	pop	{r4, r5, r6, r7, pc}
Leh_func_end131:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPresenter
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPresenter:
Leh_func_begin132:
	push	{r4, r5, r7, lr}
Ltmp266:
	add	r7, sp, #8
Ltmp267:
	push	{r8}
Ltmp268:
	bl	_p_83_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter_llvm
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC132_0+8))
	mov	r4, r0
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC132_0+8))
LPC132_0:
	add	r5, pc, r5
	ldr	r0, [r5, #364]
	mov	r8, r0
	mov	r0, r4
	bl	_p_89_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm
	ldr	r0, [r5, #368]
	mov	r8, r0
	mov	r0, r4
	bl	_p_90_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_llvm
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end132:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance:
Leh_func_begin133:
	push	{r7, lr}
Ltmp269:
	mov	r7, sp
Ltmp270:
Ltmp271:
	ldr	r1, [r0]
	ldr	r1, [r1, #236]
	blx	r1
	pop	{r7, pc}
Leh_func_end133:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeBindingBuilder
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeBindingBuilder:
Leh_func_begin134:
	push	{r4, r7, lr}
Ltmp272:
	add	r7, sp, #4
Ltmp273:
Ltmp274:
	mov	r4, r0
	ldr	r0, [r4]
	ldr	r1, [r0, #232]
	mov	r0, r4
	blx	r1
	ldr	r0, [r4]
	ldr	r1, [r0, #228]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r1, #152]
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end134:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterBindingBuilderCallbacks
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterBindingBuilderCallbacks:
Leh_func_begin135:
	push	{r4, r5, r6, r7, lr}
Ltmp275:
	add	r7, sp, #12
Ltmp276:
	push	{r8, r10}
Ltmp277:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC135_0+8))
	mov	r4, r0
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC135_0+8))
LPC135_0:
	add	r10, pc, r10
	ldr	r1, [r10, #372]
	bl	_p_91_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r5, r0
	ldr	r0, [r10, #376]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r5
	mov	r6, r0
	bl	_p_92_plt_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry__ctor_object_intptr_llvm
	ldr	r0, [r10, #380]
	mov	r8, r0
	mov	r0, r6
	bl	_p_93_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_llvm
	ldr	r1, [r10, #384]
	mov	r0, r4
	bl	_p_91_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r5, r0
	ldr	r0, [r10, #388]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r5
	mov	r6, r0
	bl	_p_94_plt_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry__ctor_object_intptr_llvm
	ldr	r0, [r10, #392]
	mov	r8, r0
	mov	r0, r6
	bl	_p_95_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_llvm
	ldr	r1, [r10, #396]
	mov	r0, r4
	bl	_p_91_plt__jit_icall_mono_ldvirtfn_llvm
	mov	r5, r0
	ldr	r0, [r10, #400]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r2, r5
	mov	r6, r0
	bl	_p_96_plt_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry__ctor_object_intptr_llvm
	ldr	r0, [r10, #404]
	mov	r8, r0
	mov	r0, r6
	bl	_p_97_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_llvm
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end135:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateBindingBuilder
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateBindingBuilder:
Leh_func_begin136:
	push	{r4, r7, lr}
Ltmp278:
	add	r7, sp, #4
Ltmp279:
Ltmp280:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC136_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC136_0+8))
LPC136_0:
	add	r0, pc, r0
	ldr	r0, [r0, #408]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, #0
	mov	r2, #0
	mov	r3, #0
	mov	r4, r0
	bl	_p_98_plt_Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_llvm
	mov	r0, r4
	pop	{r4, r7, pc}
Leh_func_end136:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin137:
	bx	lr
Leh_func_end137:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
Leh_func_begin138:
	push	{r4, r5, r6, r7, lr}
Ltmp281:
	add	r7, sp, #12
Ltmp282:
	push	{r8}
Ltmp283:
	mov	r5, r0
	mov	r4, r1
	ldr	r0, [r5]
	ldr	r1, [r0, #212]
	mov	r0, r5
	blx	r1
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC138_0+8))
	mov	r1, r0
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC138_0+8))
LPC138_0:
	add	r6, pc, r6
	ldr	r0, [r6, #412]
	mov	r8, r0
	mov	r0, r4
	bl	_p_99_plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly_llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #216]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	ldr	r0, [r6, #416]
	mov	r8, r0
	mov	r0, r4
	bl	_p_100_plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Type_llvm
	pop	{r8}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end138:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterHolders
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterHolders:
Leh_func_begin139:
	push	{r4, r7, lr}
Ltmp284:
	add	r7, sp, #4
Ltmp285:
Ltmp286:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC139_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC139_0+8))
LPC139_0:
	add	r4, pc, r4
	ldr	r0, [r4, #420]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [r4, #424]
	ldr	r1, [r1]
	str	r1, [r0, #8]
	pop	{r4, r7, pc}
Leh_func_end139:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterAssemblies
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterAssemblies:
Leh_func_begin140:
	push	{r4, r5, r6, r7, lr}
Ltmp287:
	add	r7, sp, #12
Ltmp288:
Ltmp289:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC140_0+8))
	mov	r5, r0
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC140_0+8))
LPC140_0:
	add	r6, pc, r6
	ldr	r0, [r6, #428]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r4, r0
	ldr	r0, [r6, #432]
	ldr	r0, [r0]
	str	r0, [r4, #8]
	ldr	r0, [r5]
	ldr	r1, [r0, #72]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	mov	r0, r4
	bl	_p_101_plt_System_Collections_Generic_List_1_System_Reflection_Assembly_AddRange_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly_llvm
	ldr	r0, [r5]
	ldr	r1, [r0, #76]
	mov	r0, r5
	blx	r1
	mov	r1, r0
	mov	r0, r4
	bl	_p_101_plt_System_Collections_Generic_List_1_System_Reflection_Assembly_AddRange_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly_llvm
	mov	r0, r4
	pop	{r4, r5, r6, r7, pc}
Leh_func_end140:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
Leh_func_begin141:
	bx	lr
Leh_func_end141:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewToViewModelNaming
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewToViewModelNaming:
Leh_func_begin142:
	push	{r4, r5, r7, lr}
Ltmp290:
	add	r7, sp, #8
Ltmp291:
Ltmp292:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC142_0+8))
	mov	r1, #2
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC142_0+8))
LPC142_0:
	add	r4, pc, r4
	ldr	r0, [r4, #436]
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r5, r0
	ldr	r2, [r4, #440]
	mov	r1, #0
	ldr	r0, [r5]
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r5]
	ldr	r2, [r4, #444]
	mov	r1, #1
	ldr	r3, [r0, #128]
	mov	r0, r5
	blx	r3
	ldr	r0, [r4, #448]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r4, [r4, #452]
	strd	r4, r5, [r0, #8]
	pop	{r4, r5, r7, pc}
Leh_func_end142:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout:
Leh_func_begin143:
	push	{r4, r7, lr}
Ltmp293:
	add	r7, sp, #4
Ltmp294:
Ltmp295:
	mov	r4, r0
	bl	_p_102_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end143:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_intptr
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_intptr:
Leh_func_begin144:
	push	{r4, r7, lr}
Ltmp296:
	add	r7, sp, #4
Ltmp297:
Ltmp298:
	mov	r4, r0
	bl	_p_103_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end144:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle:
Leh_func_begin145:
	push	{r4, r7, lr}
Ltmp299:
	add	r7, sp, #4
Ltmp300:
Ltmp301:
	mov	r4, r0
	bl	_p_104_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm
	mov	r0, r4
	bl	_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm
	pop	{r4, r7, pc}
Leh_func_end145:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext:
Leh_func_begin146:
	push	{r7, lr}
Ltmp302:
	mov	r7, sp
Ltmp303:
	push	{r8}
Ltmp304:
	ldr	r0, [r0, #80]
	ldr	r2, [r0]
	movw	r1, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC146_0+8))
	movt	r1, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC146_0+8))
LPC146_0:
	add	r1, pc, r1
	ldr	r1, [r1, #96]
	sub	r2, r2, #32
	ldr	r2, [r2]
	mov	r8, r1
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end146:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object:
Leh_func_begin147:
	push	{r7, lr}
Ltmp305:
	mov	r7, sp
Ltmp306:
	push	{r8}
Ltmp307:
	ldr	r0, [r0, #80]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC147_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC147_0+8))
LPC147_0:
	add	r3, pc, r3
	ldr	r3, [r3, #100]
	sub	r2, r2, #76
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end147:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_ViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_ViewModel:
Leh_func_begin148:
	push	{r7, lr}
Ltmp308:
	mov	r7, sp
Ltmp309:
Ltmp310:
	bl	_p_105_plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext_llvm
	mov	r9, #0
	cmp	r0, #0
	beq	LBB148_3
	movw	r2, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC148_0+8))
	movt	r2, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC148_0+8))
	ldr	r3, [r0]
LPC148_0:
	add	r2, pc, r2
	ldr	r2, [r2, #104]
	ldrh	r1, [r3, #20]
	cmp	r1, r2
	blo	LBB148_3
	ldr	r1, [r3, #16]
	mov	r3, #1
	ldrb	r1, [r1, r2, asr #3]
	and	r2, r2, #7
	and	r1, r1, r3, lsl r2
	cmp	r1, #0
	moveq	r0, r1
	mov	r9, r0
LBB148_3:
	mov	r0, r9
	pop	{r7, pc}
Leh_func_end148:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
Leh_func_begin149:
	push	{r7, lr}
Ltmp311:
	mov	r7, sp
Ltmp312:
Ltmp313:
	bl	_p_106_plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object_llvm
	pop	{r7, pc}
Leh_func_end149:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_Request
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_Request:
Leh_func_begin150:
	ldr	r0, [r0, #76]
	bx	lr
Leh_func_end150:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
Leh_func_begin151:
	str	r1, [r0, #76]
	bx	lr
Leh_func_end151:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_BindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_BindingContext:
Leh_func_begin152:
	ldr	r0, [r0, #80]
	bx	lr
Leh_func_end152:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext:
Leh_func_begin153:
	str	r1, [r0, #80]
	bx	lr
Leh_func_end153:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin154:
	push	{r4, r5, r6, r7, lr}
Ltmp314:
	add	r7, sp, #12
Ltmp315:
Ltmp316:
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC154_1+8))
	mov	r4, r0
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC154_1+8))
LPC154_1:
	add	r6, pc, r6
	ldr	r0, [r6, #456]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r5, r0
	str	r4, [r5, #8]
	cmp	r5, #0
	ldr	r4, [r5, #8]
	beq	LBB154_2
	ldr	r0, [r6, #460]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r0
	str	r5, [r1, #16]
	ldr	r0, [r6, #464]
	str	r0, [r1, #20]
	ldr	r0, [r6, #468]
	str	r0, [r1, #28]
	ldr	r0, [r6, #472]
	str	r0, [r1, #12]
	mov	r0, r4
	bl	_p_107_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Views_IMvxView_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm
	pop	{r4, r5, r6, r7, pc}
Ltmp317:
LBB154_2:
	ldr	r0, LCPI154_0
LPC154_0:
	add	r1, pc, r0
	movw	r0, #518
	movt	r0, #512
	bl	_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm
	.align	2
	.data_region
LCPI154_0:
	.long	Ltmp317-(LPC154_0+8)
	.end_data_region
Leh_func_end154:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
Leh_func_begin155:
	push	{r4, r5, r6, r7, lr}
Ltmp318:
	add	r7, sp, #12
Ltmp319:
	push	{r8, r10}
Ltmp320:
	mov	r4, r0
	ldr	r0, [r4]
	movw	r6, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC155_0+8))
	movt	r6, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC155_0+8))
LPC155_0:
	add	r6, pc, r6
	ldr	r10, [r6, #476]
	sub	r0, r0, #76
	ldr	r1, [r0]
	mov	r0, r4
	mov	r8, r10
	blx	r1
	cmp	r0, #0
	bne	LBB155_2
	ldr	r0, [r6, #92]
	ldr	r5, [r6, #492]
	mov	r1, #0
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r1, r0
	mov	r0, r5
	bl	_p_109_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm
	ldr	r0, [r6, #496]
	mov	r8, r0
	bl	_p_110_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_llvm
	ldr	r2, [r0]
	ldr	r1, [r6, #500]
	sub	r2, r2, #64
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	ldr	r2, [r4]
	mov	r1, r0
	ldr	r0, [r6, #108]
	sub	r2, r2, #72
	mov	r8, r0
	mov	r0, r4
	ldr	r2, [r2]
	blx	r2
LBB155_2:
	ldr	r0, [r4]
	mov	r8, r10
	sub	r0, r0, #76
	ldr	r1, [r0]
	mov	r0, r4
	blx	r1
	cmp	r0, #0
	beq	LBB155_4
	ldr	r2, [r0]
	ldr	r1, [r6, #488]
	ldr	r2, [r2]
	ldr	r2, [r2, #8]
	ldr	r2, [r2, #8]
	cmp	r2, r1
	movne	r0, #0
LBB155_4:
	cmp	r0, #0
	beq	LBB155_6
	ldr	r1, [r0]
	ldr	r0, [r0, #24]
	b	LBB155_7
LBB155_6:
	ldr	r0, [r6, #480]
	mov	r8, r0
	bl	_p_108_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxViewModelLoader_llvm
	mov	r5, r0
	ldr	r0, [r4]
	mov	r8, r10
	sub	r0, r0, #76
	ldr	r1, [r0]
	mov	r0, r4
	blx	r1
	ldr	r2, [r5]
	mov	r1, r0
	ldr	r0, [r6, #484]
	sub	r2, r2, #24
	mov	r8, r0
	mov	r0, r5
	ldr	r3, [r2]
	mov	r2, #0
	blx	r3
	cmp	r0, #0
	beq	LBB155_8
LBB155_7:
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
LBB155_8:
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC155_1+8))
	mov	r1, #1904
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC155_1+8))
LPC155_1:
	ldr	r0, [pc, r0]
	bl	_p_30_plt__jit_icall_mono_helper_ldstr_llvm
	mov	r5, r0
	ldr	r0, [r4]
	mov	r8, r10
	sub	r0, r0, #76
	ldr	r1, [r0]
	mov	r0, r4
	blx	r1
	ldr	r1, [r0]
	ldr	r1, [r0, #8]
	mov	r0, r5
	bl	_p_48_plt_string_Concat_object_object_llvm
	mov	r4, r0
	ldr	r0, [r6, #112]
	bl	_p_13_plt__jit_icall_mono_object_new_fast_llvm
	mov	r1, r4
	mov	r5, r0
	bl	_p_49_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm
	mov	r0, r5
	bl	_p_11_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end155:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__ctor:
Leh_func_begin156:
	bx	lr
Leh_func_end156:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__RequestMainThreadActionb__0
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__RequestMainThreadActionb__0:
Leh_func_begin157:
	push	{r7, lr}
Ltmp321:
	mov	r7, sp
Ltmp322:
Ltmp323:
	ldr	r0, [r0, #8]
	bl	_p_111_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher_ExceptionMaskedAction_System_Action_llvm
	pop	{r7, pc}
Leh_func_end157:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ctor:
Leh_func_begin158:
	bx	lr
Leh_func_end158:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ShowViewModelb__3
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ShowViewModelb__3:
Leh_func_begin159:
	push	{r4, r5, r6, r7, lr}
Ltmp324:
	add	r7, sp, #12
Ltmp325:
	push	{r8, r10}
Ltmp326:
	movw	r10, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC159_0+8))
	mov	r4, r0
	mov	r1, #0
	movt	r10, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC159_0+8))
LPC159_0:
	add	r10, pc, r10
	ldr	r0, [r10, #92]
	ldr	r5, [r10, #504]
	ldr	r6, [r10, #508]
	bl	_p_33_plt__jit_icall_mono_array_new_specific_llvm
	mov	r2, r0
	mov	r0, r5
	mov	r1, r6
	bl	_p_112_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_string_string_object___llvm
	ldr	r0, [r4, #8]
	ldr	r0, [r0, #12]
	ldr	r1, [r4, #12]
	ldr	r2, [r0]
	ldr	r3, [r10, #512]
	sub	r2, r2, #12
	mov	r8, r3
	ldr	r2, [r2]
	blx	r2
	pop	{r8, r10}
	pop	{r4, r5, r6, r7, pc}
Leh_func_end159:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ctor:
Leh_func_begin160:
	bx	lr
Leh_func_end160:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ChangePresentationb__6
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ChangePresentationb__6:
Leh_func_begin161:
	push	{r7, lr}
Ltmp327:
	mov	r7, sp
Ltmp328:
	push	{r8}
Ltmp329:
	mov	r1, r0
	ldr	r0, [r1, #8]
	ldr	r0, [r0, #12]
	ldr	r1, [r1, #12]
	ldr	r2, [r0]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC161_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC161_0+8))
LPC161_0:
	add	r3, pc, r3
	ldr	r3, [r3, #516]
	sub	r2, r2, #16
	ldr	r2, [r2]
	mov	r8, r3
	blx	r2
	pop	{r8}
	pop	{r7, pc}
Leh_func_end161:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__ctor
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__ctor:
Leh_func_begin162:
	bx	lr
Leh_func_end162:

	.private_extern	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__OnViewCreateb__0
	.align	2
_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__OnViewCreateb__0:
Leh_func_begin163:
	push	{r7, lr}
Ltmp330:
	mov	r7, sp
Ltmp331:
Ltmp332:
	ldr	r0, [r0, #8]
	bl	_p_113_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm
	pop	{r7, pc}
Leh_func_end163:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_invoke_void__this___object_TEventArgs_object_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_invoke_void__this___object_TEventArgs_object_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs:
Leh_func_begin164:
	push	{r4, r5, r6, r7, lr}
Ltmp333:
	add	r7, sp, #12
Ltmp334:
Ltmp335:
	mov	r6, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC164_0+8))
	mov	r4, r2
	mov	r5, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC164_0+8))
LPC164_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB164_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB164_2:
	ldr	r0, [r6, #44]
	cmp	r0, #0
	beq	LBB164_4
	ldr	r3, [r0, #12]
	mov	r1, r5
	mov	r2, r4
	blx	r3
LBB164_4:
	ldr	r0, [r6, #16]
	ldr	r3, [r6, #8]
	cmp	r0, #0
	beq	LBB164_6
	mov	r1, r5
	mov	r2, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
LBB164_6:
	mov	r0, r5
	mov	r1, r4
	blx	r3
	pop	{r4, r5, r6, r7, pc}
Leh_func_end164:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__:
Leh_func_begin165:
	push	{r4, r7, lr}
Ltmp336:
	add	r7, sp, #4
Ltmp337:
Ltmp338:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC165_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC165_0+8))
LPC165_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB165_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB165_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB165_4
	ldr	r1, [r0, #12]
	blx	r1
LBB165_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB165_6
	blx	r1
	pop	{r4, r7, pc}
LBB165_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end165:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
Leh_func_begin166:
	push	{r4, r5, r7, lr}
Ltmp339:
	add	r7, sp, #8
Ltmp340:
Ltmp341:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC166_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC166_0+8))
LPC166_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB166_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB166_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB166_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB166_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB166_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB166_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end166:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
Leh_func_begin167:
	push	{r4, r5, r7, lr}
Ltmp342:
	add	r7, sp, #8
Ltmp343:
Ltmp344:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC167_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC167_0+8))
LPC167_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB167_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB167_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB167_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB167_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB167_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB167_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end167:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
Leh_func_begin168:
	push	{r4, r5, r7, lr}
Ltmp345:
	add	r7, sp, #8
Ltmp346:
Ltmp347:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC168_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC168_0+8))
LPC168_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB168_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB168_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB168_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB168_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB168_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB168_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end168:

	.private_extern	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_invoke_TResult__this__
	.align	2
_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_invoke_TResult__this__:
Leh_func_begin169:
	push	{r4, r7, lr}
Ltmp348:
	add	r7, sp, #4
Ltmp349:
Ltmp350:
	mov	r4, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC169_0+8))
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Touch_got-(LPC169_0+8))
LPC169_0:
	add	r0, pc, r0
	ldr	r0, [r0, #520]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB169_2
	bl	_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB169_2:
	ldr	r0, [r4, #44]
	cmp	r0, #0
	beq	LBB169_4
	ldr	r1, [r0, #12]
	blx	r1
LBB169_4:
	ldr	r0, [r4, #16]
	ldr	r1, [r4, #8]
	cmp	r0, #0
	beq	LBB169_6
	blx	r1
	pop	{r4, r7, pc}
LBB169_6:
	blx	r1
	pop	{r4, r7, pc}
Leh_func_end169:

.zerofill __DATA,__bss,_type_info_0,4,2
.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Touch_got,1012,4
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillTerminate_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_add_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_remove_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_FormFactor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_DisplayDensity
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute__ctor_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_Target
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_set_Target_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_IsConditionSatisfied
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_get_TouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_ViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_Request
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_get_TouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_ViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_Request
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_get_CurrentRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_set_CurrentRequest_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ShowViewModel_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_Request
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest_get_ViewModelInstance
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_MasterNavigationController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_set_MasterNavigationController_MonoTouch_UIKit_UINavigationController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_ApplicationDelegate
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_Window
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ShowFirstView_MonoTouch_UIKit_UIViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_SetWindowRootViewController_MonoTouch_UIKit_UIViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_OnMasterNavigationControllerCreated
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CreateNavigationController_MonoTouch_UIKit_UIViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_CurrentTopViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__PresentModalViewControllerb__0
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CreateModalNavigationController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CloseModalViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__2
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__3
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__6
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__7
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_CloseModalViewController
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__Closeb__0
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Window
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ApplicationDelegate
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateDebugTrace
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePluginManager
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewsContainer
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterTouchViewCreator_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewDispatcher
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializePlatformServices
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPlatformProperties
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateTouchSystemProperties
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterOldStylePlatformProperties
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterLifetime
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePresenter
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPresenter
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeBindingBuilder
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterBindingBuilderCallbacks
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateBindingBuilder
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterHolders
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterAssemblies
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewToViewModelNaming
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_intptr
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_ViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_Request
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_BindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__RequestMainThreadActionb__0
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ShowViewModelb__3
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ChangePresentationb__6
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__OnViewCreateb__0
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_invoke_void__this___object_TEventArgs_object_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	.no_dead_strip	_Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_invoke_TResult__this__
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Touch_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	170
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	3
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	4
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	5
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	6
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	7
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	8
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	9
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	10
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	11
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	12
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	13
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	16
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	17
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
	.long	18
Lset14 = Lmono_eh_func_begin14-mono_eh_frame
	.long	Lset14
	.long	19
Lset15 = Lmono_eh_func_begin15-mono_eh_frame
	.long	Lset15
	.long	20
Lset16 = Lmono_eh_func_begin16-mono_eh_frame
	.long	Lset16
	.long	21
Lset17 = Lmono_eh_func_begin17-mono_eh_frame
	.long	Lset17
	.long	22
Lset18 = Lmono_eh_func_begin18-mono_eh_frame
	.long	Lset18
	.long	23
Lset19 = Lmono_eh_func_begin19-mono_eh_frame
	.long	Lset19
	.long	24
Lset20 = Lmono_eh_func_begin20-mono_eh_frame
	.long	Lset20
	.long	25
Lset21 = Lmono_eh_func_begin21-mono_eh_frame
	.long	Lset21
	.long	26
Lset22 = Lmono_eh_func_begin22-mono_eh_frame
	.long	Lset22
	.long	27
Lset23 = Lmono_eh_func_begin23-mono_eh_frame
	.long	Lset23
	.long	28
Lset24 = Lmono_eh_func_begin24-mono_eh_frame
	.long	Lset24
	.long	31
Lset25 = Lmono_eh_func_begin25-mono_eh_frame
	.long	Lset25
	.long	32
Lset26 = Lmono_eh_func_begin26-mono_eh_frame
	.long	Lset26
	.long	33
Lset27 = Lmono_eh_func_begin27-mono_eh_frame
	.long	Lset27
	.long	34
Lset28 = Lmono_eh_func_begin28-mono_eh_frame
	.long	Lset28
	.long	35
Lset29 = Lmono_eh_func_begin29-mono_eh_frame
	.long	Lset29
	.long	36
Lset30 = Lmono_eh_func_begin30-mono_eh_frame
	.long	Lset30
	.long	37
Lset31 = Lmono_eh_func_begin31-mono_eh_frame
	.long	Lset31
	.long	38
Lset32 = Lmono_eh_func_begin32-mono_eh_frame
	.long	Lset32
	.long	39
Lset33 = Lmono_eh_func_begin33-mono_eh_frame
	.long	Lset33
	.long	40
Lset34 = Lmono_eh_func_begin34-mono_eh_frame
	.long	Lset34
	.long	41
Lset35 = Lmono_eh_func_begin35-mono_eh_frame
	.long	Lset35
	.long	42
Lset36 = Lmono_eh_func_begin36-mono_eh_frame
	.long	Lset36
	.long	43
Lset37 = Lmono_eh_func_begin37-mono_eh_frame
	.long	Lset37
	.long	44
Lset38 = Lmono_eh_func_begin38-mono_eh_frame
	.long	Lset38
	.long	45
Lset39 = Lmono_eh_func_begin39-mono_eh_frame
	.long	Lset39
	.long	46
Lset40 = Lmono_eh_func_begin40-mono_eh_frame
	.long	Lset40
	.long	47
Lset41 = Lmono_eh_func_begin41-mono_eh_frame
	.long	Lset41
	.long	48
Lset42 = Lmono_eh_func_begin42-mono_eh_frame
	.long	Lset42
	.long	49
Lset43 = Lmono_eh_func_begin43-mono_eh_frame
	.long	Lset43
	.long	50
Lset44 = Lmono_eh_func_begin44-mono_eh_frame
	.long	Lset44
	.long	51
Lset45 = Lmono_eh_func_begin45-mono_eh_frame
	.long	Lset45
	.long	52
Lset46 = Lmono_eh_func_begin46-mono_eh_frame
	.long	Lset46
	.long	53
Lset47 = Lmono_eh_func_begin47-mono_eh_frame
	.long	Lset47
	.long	54
Lset48 = Lmono_eh_func_begin48-mono_eh_frame
	.long	Lset48
	.long	55
Lset49 = Lmono_eh_func_begin49-mono_eh_frame
	.long	Lset49
	.long	56
Lset50 = Lmono_eh_func_begin50-mono_eh_frame
	.long	Lset50
	.long	57
Lset51 = Lmono_eh_func_begin51-mono_eh_frame
	.long	Lset51
	.long	59
Lset52 = Lmono_eh_func_begin52-mono_eh_frame
	.long	Lset52
	.long	60
Lset53 = Lmono_eh_func_begin53-mono_eh_frame
	.long	Lset53
	.long	63
Lset54 = Lmono_eh_func_begin54-mono_eh_frame
	.long	Lset54
	.long	64
Lset55 = Lmono_eh_func_begin55-mono_eh_frame
	.long	Lset55
	.long	65
Lset56 = Lmono_eh_func_begin56-mono_eh_frame
	.long	Lset56
	.long	66
Lset57 = Lmono_eh_func_begin57-mono_eh_frame
	.long	Lset57
	.long	67
Lset58 = Lmono_eh_func_begin58-mono_eh_frame
	.long	Lset58
	.long	68
Lset59 = Lmono_eh_func_begin59-mono_eh_frame
	.long	Lset59
	.long	69
Lset60 = Lmono_eh_func_begin60-mono_eh_frame
	.long	Lset60
	.long	70
Lset61 = Lmono_eh_func_begin61-mono_eh_frame
	.long	Lset61
	.long	71
Lset62 = Lmono_eh_func_begin62-mono_eh_frame
	.long	Lset62
	.long	72
Lset63 = Lmono_eh_func_begin63-mono_eh_frame
	.long	Lset63
	.long	73
Lset64 = Lmono_eh_func_begin64-mono_eh_frame
	.long	Lset64
	.long	74
Lset65 = Lmono_eh_func_begin65-mono_eh_frame
	.long	Lset65
	.long	75
Lset66 = Lmono_eh_func_begin66-mono_eh_frame
	.long	Lset66
	.long	76
Lset67 = Lmono_eh_func_begin67-mono_eh_frame
	.long	Lset67
	.long	77
Lset68 = Lmono_eh_func_begin68-mono_eh_frame
	.long	Lset68
	.long	78
Lset69 = Lmono_eh_func_begin69-mono_eh_frame
	.long	Lset69
	.long	79
Lset70 = Lmono_eh_func_begin70-mono_eh_frame
	.long	Lset70
	.long	80
Lset71 = Lmono_eh_func_begin71-mono_eh_frame
	.long	Lset71
	.long	81
Lset72 = Lmono_eh_func_begin72-mono_eh_frame
	.long	Lset72
	.long	82
Lset73 = Lmono_eh_func_begin73-mono_eh_frame
	.long	Lset73
	.long	83
Lset74 = Lmono_eh_func_begin74-mono_eh_frame
	.long	Lset74
	.long	84
Lset75 = Lmono_eh_func_begin75-mono_eh_frame
	.long	Lset75
	.long	85
Lset76 = Lmono_eh_func_begin76-mono_eh_frame
	.long	Lset76
	.long	86
Lset77 = Lmono_eh_func_begin77-mono_eh_frame
	.long	Lset77
	.long	87
Lset78 = Lmono_eh_func_begin78-mono_eh_frame
	.long	Lset78
	.long	88
Lset79 = Lmono_eh_func_begin79-mono_eh_frame
	.long	Lset79
	.long	89
Lset80 = Lmono_eh_func_begin80-mono_eh_frame
	.long	Lset80
	.long	90
Lset81 = Lmono_eh_func_begin81-mono_eh_frame
	.long	Lset81
	.long	91
Lset82 = Lmono_eh_func_begin82-mono_eh_frame
	.long	Lset82
	.long	92
Lset83 = Lmono_eh_func_begin83-mono_eh_frame
	.long	Lset83
	.long	93
Lset84 = Lmono_eh_func_begin84-mono_eh_frame
	.long	Lset84
	.long	94
Lset85 = Lmono_eh_func_begin85-mono_eh_frame
	.long	Lset85
	.long	95
Lset86 = Lmono_eh_func_begin86-mono_eh_frame
	.long	Lset86
	.long	96
Lset87 = Lmono_eh_func_begin87-mono_eh_frame
	.long	Lset87
	.long	97
Lset88 = Lmono_eh_func_begin88-mono_eh_frame
	.long	Lset88
	.long	98
Lset89 = Lmono_eh_func_begin89-mono_eh_frame
	.long	Lset89
	.long	99
Lset90 = Lmono_eh_func_begin90-mono_eh_frame
	.long	Lset90
	.long	100
Lset91 = Lmono_eh_func_begin91-mono_eh_frame
	.long	Lset91
	.long	101
Lset92 = Lmono_eh_func_begin92-mono_eh_frame
	.long	Lset92
	.long	102
Lset93 = Lmono_eh_func_begin93-mono_eh_frame
	.long	Lset93
	.long	103
Lset94 = Lmono_eh_func_begin94-mono_eh_frame
	.long	Lset94
	.long	104
Lset95 = Lmono_eh_func_begin95-mono_eh_frame
	.long	Lset95
	.long	105
Lset96 = Lmono_eh_func_begin96-mono_eh_frame
	.long	Lset96
	.long	106
Lset97 = Lmono_eh_func_begin97-mono_eh_frame
	.long	Lset97
	.long	107
Lset98 = Lmono_eh_func_begin98-mono_eh_frame
	.long	Lset98
	.long	108
Lset99 = Lmono_eh_func_begin99-mono_eh_frame
	.long	Lset99
	.long	109
Lset100 = Lmono_eh_func_begin100-mono_eh_frame
	.long	Lset100
	.long	110
Lset101 = Lmono_eh_func_begin101-mono_eh_frame
	.long	Lset101
	.long	111
Lset102 = Lmono_eh_func_begin102-mono_eh_frame
	.long	Lset102
	.long	112
Lset103 = Lmono_eh_func_begin103-mono_eh_frame
	.long	Lset103
	.long	113
Lset104 = Lmono_eh_func_begin104-mono_eh_frame
	.long	Lset104
	.long	114
Lset105 = Lmono_eh_func_begin105-mono_eh_frame
	.long	Lset105
	.long	115
Lset106 = Lmono_eh_func_begin106-mono_eh_frame
	.long	Lset106
	.long	116
Lset107 = Lmono_eh_func_begin107-mono_eh_frame
	.long	Lset107
	.long	117
Lset108 = Lmono_eh_func_begin108-mono_eh_frame
	.long	Lset108
	.long	118
Lset109 = Lmono_eh_func_begin109-mono_eh_frame
	.long	Lset109
	.long	119
Lset110 = Lmono_eh_func_begin110-mono_eh_frame
	.long	Lset110
	.long	120
Lset111 = Lmono_eh_func_begin111-mono_eh_frame
	.long	Lset111
	.long	121
Lset112 = Lmono_eh_func_begin112-mono_eh_frame
	.long	Lset112
	.long	122
Lset113 = Lmono_eh_func_begin113-mono_eh_frame
	.long	Lset113
	.long	123
Lset114 = Lmono_eh_func_begin114-mono_eh_frame
	.long	Lset114
	.long	124
Lset115 = Lmono_eh_func_begin115-mono_eh_frame
	.long	Lset115
	.long	125
Lset116 = Lmono_eh_func_begin116-mono_eh_frame
	.long	Lset116
	.long	126
Lset117 = Lmono_eh_func_begin117-mono_eh_frame
	.long	Lset117
	.long	127
Lset118 = Lmono_eh_func_begin118-mono_eh_frame
	.long	Lset118
	.long	128
Lset119 = Lmono_eh_func_begin119-mono_eh_frame
	.long	Lset119
	.long	129
Lset120 = Lmono_eh_func_begin120-mono_eh_frame
	.long	Lset120
	.long	130
Lset121 = Lmono_eh_func_begin121-mono_eh_frame
	.long	Lset121
	.long	131
Lset122 = Lmono_eh_func_begin122-mono_eh_frame
	.long	Lset122
	.long	132
Lset123 = Lmono_eh_func_begin123-mono_eh_frame
	.long	Lset123
	.long	133
Lset124 = Lmono_eh_func_begin124-mono_eh_frame
	.long	Lset124
	.long	134
Lset125 = Lmono_eh_func_begin125-mono_eh_frame
	.long	Lset125
	.long	135
Lset126 = Lmono_eh_func_begin126-mono_eh_frame
	.long	Lset126
	.long	136
Lset127 = Lmono_eh_func_begin127-mono_eh_frame
	.long	Lset127
	.long	137
Lset128 = Lmono_eh_func_begin128-mono_eh_frame
	.long	Lset128
	.long	138
Lset129 = Lmono_eh_func_begin129-mono_eh_frame
	.long	Lset129
	.long	139
Lset130 = Lmono_eh_func_begin130-mono_eh_frame
	.long	Lset130
	.long	140
Lset131 = Lmono_eh_func_begin131-mono_eh_frame
	.long	Lset131
	.long	141
Lset132 = Lmono_eh_func_begin132-mono_eh_frame
	.long	Lset132
	.long	142
Lset133 = Lmono_eh_func_begin133-mono_eh_frame
	.long	Lset133
	.long	143
Lset134 = Lmono_eh_func_begin134-mono_eh_frame
	.long	Lset134
	.long	144
Lset135 = Lmono_eh_func_begin135-mono_eh_frame
	.long	Lset135
	.long	145
Lset136 = Lmono_eh_func_begin136-mono_eh_frame
	.long	Lset136
	.long	146
Lset137 = Lmono_eh_func_begin137-mono_eh_frame
	.long	Lset137
	.long	147
Lset138 = Lmono_eh_func_begin138-mono_eh_frame
	.long	Lset138
	.long	148
Lset139 = Lmono_eh_func_begin139-mono_eh_frame
	.long	Lset139
	.long	149
Lset140 = Lmono_eh_func_begin140-mono_eh_frame
	.long	Lset140
	.long	150
Lset141 = Lmono_eh_func_begin141-mono_eh_frame
	.long	Lset141
	.long	151
Lset142 = Lmono_eh_func_begin142-mono_eh_frame
	.long	Lset142
	.long	152
Lset143 = Lmono_eh_func_begin143-mono_eh_frame
	.long	Lset143
	.long	153
Lset144 = Lmono_eh_func_begin144-mono_eh_frame
	.long	Lset144
	.long	154
Lset145 = Lmono_eh_func_begin145-mono_eh_frame
	.long	Lset145
	.long	155
Lset146 = Lmono_eh_func_begin146-mono_eh_frame
	.long	Lset146
	.long	156
Lset147 = Lmono_eh_func_begin147-mono_eh_frame
	.long	Lset147
	.long	157
Lset148 = Lmono_eh_func_begin148-mono_eh_frame
	.long	Lset148
	.long	158
Lset149 = Lmono_eh_func_begin149-mono_eh_frame
	.long	Lset149
	.long	159
Lset150 = Lmono_eh_func_begin150-mono_eh_frame
	.long	Lset150
	.long	160
Lset151 = Lmono_eh_func_begin151-mono_eh_frame
	.long	Lset151
	.long	161
Lset152 = Lmono_eh_func_begin152-mono_eh_frame
	.long	Lset152
	.long	162
Lset153 = Lmono_eh_func_begin153-mono_eh_frame
	.long	Lset153
	.long	163
Lset154 = Lmono_eh_func_begin154-mono_eh_frame
	.long	Lset154
	.long	164
Lset155 = Lmono_eh_func_begin155-mono_eh_frame
	.long	Lset155
	.long	165
Lset156 = Lmono_eh_func_begin156-mono_eh_frame
	.long	Lset156
	.long	166
Lset157 = Lmono_eh_func_begin157-mono_eh_frame
	.long	Lset157
	.long	167
Lset158 = Lmono_eh_func_begin158-mono_eh_frame
	.long	Lset158
	.long	168
Lset159 = Lmono_eh_func_begin159-mono_eh_frame
	.long	Lset159
	.long	169
Lset160 = Lmono_eh_func_begin160-mono_eh_frame
	.long	Lset160
	.long	170
Lset161 = Lmono_eh_func_begin161-mono_eh_frame
	.long	Lset161
	.long	171
Lset162 = Lmono_eh_func_begin162-mono_eh_frame
	.long	Lset162
	.long	172
Lset163 = Lmono_eh_func_begin163-mono_eh_frame
	.long	Lset163
	.long	177
Lset164 = Lmono_eh_func_begin164-mono_eh_frame
	.long	Lset164
	.long	178
Lset165 = Lmono_eh_func_begin165-mono_eh_frame
	.long	Lset165
	.long	179
Lset166 = Lmono_eh_func_begin166-mono_eh_frame
	.long	Lset166
	.long	180
Lset167 = Lmono_eh_func_begin167-mono_eh_frame
	.long	Lset167
	.long	181
Lset168 = Lmono_eh_func_begin168-mono_eh_frame
	.long	Lset168
	.long	182
Lset169 = Lmono_eh_func_begin169-mono_eh_frame
	.long	Lset169
Lset170 = Leh_func_end169-Leh_func_begin169
	.long	Lset170
Lset171 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset171
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin2:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin6:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7
	.byte	136
	.byte	8

Lmono_eh_func_begin8:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin9:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin10:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	5
	.byte	138
	.byte	6

Lmono_eh_func_begin11:
	.byte	0

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin14:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin15:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin16:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin17:
	.byte	0

Lmono_eh_func_begin18:
	.byte	0

Lmono_eh_func_begin19:
	.byte	0

Lmono_eh_func_begin20:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin21:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin22:
	.byte	0

Lmono_eh_func_begin23:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin24:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin25:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin26:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin27:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin28:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin29:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin30:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin31:
	.byte	0

Lmono_eh_func_begin32:
	.byte	0

Lmono_eh_func_begin33:
	.byte	0

Lmono_eh_func_begin34:
	.byte	0

Lmono_eh_func_begin35:
	.byte	0

Lmono_eh_func_begin36:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin37:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin38:
	.byte	0

Lmono_eh_func_begin39:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin40:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin41:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin42:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin43:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin44:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin45:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin46:
	.byte	0

Lmono_eh_func_begin47:
	.byte	0

Lmono_eh_func_begin48:
	.byte	0

Lmono_eh_func_begin49:
	.byte	0

Lmono_eh_func_begin50:
	.byte	0

Lmono_eh_func_begin51:
	.byte	0

Lmono_eh_func_begin52:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin53:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin54:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin55:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin56:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin57:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	5
	.byte	138
	.byte	6

Lmono_eh_func_begin58:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	5
	.byte	138
	.byte	6

Lmono_eh_func_begin59:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin60:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin61:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin62:
	.byte	0

Lmono_eh_func_begin63:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin64:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin65:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin66:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin67:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin68:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin69:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin70:
	.byte	0

Lmono_eh_func_begin71:
	.byte	0

Lmono_eh_func_begin72:
	.byte	0

Lmono_eh_func_begin73:
	.byte	0

Lmono_eh_func_begin74:
	.byte	0

Lmono_eh_func_begin75:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin76:
	.byte	0

Lmono_eh_func_begin77:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin78:
	.byte	0

Lmono_eh_func_begin79:
	.byte	0

Lmono_eh_func_begin80:
	.byte	0

Lmono_eh_func_begin81:
	.byte	0

Lmono_eh_func_begin82:
	.byte	0

Lmono_eh_func_begin83:
	.byte	0

Lmono_eh_func_begin84:
	.byte	0

Lmono_eh_func_begin85:
	.byte	0

Lmono_eh_func_begin86:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin87:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin88:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin89:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin90:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin91:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin92:
	.byte	0

Lmono_eh_func_begin93:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin94:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin95:
	.byte	0

Lmono_eh_func_begin96:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin97:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin98:
	.byte	0

Lmono_eh_func_begin99:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin100:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin101:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin102:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin103:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin104:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6

Lmono_eh_func_begin105:
	.byte	0

Lmono_eh_func_begin106:
	.byte	0

Lmono_eh_func_begin107:
	.byte	0

Lmono_eh_func_begin108:
	.byte	0

Lmono_eh_func_begin109:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin110:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin111:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin112:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin113:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin114:
	.byte	0

Lmono_eh_func_begin115:
	.byte	0

Lmono_eh_func_begin116:
	.byte	0

Lmono_eh_func_begin117:
	.byte	0

Lmono_eh_func_begin118:
	.byte	0

Lmono_eh_func_begin119:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin120:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	139
	.byte	6
	.byte	138
	.byte	7

Lmono_eh_func_begin121:
	.byte	0

Lmono_eh_func_begin122:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin123:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin124:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin125:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin126:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin127:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin128:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin129:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin130:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin131:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin132:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin133:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin134:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin135:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin136:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin137:
	.byte	0

Lmono_eh_func_begin138:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	6

Lmono_eh_func_begin139:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin140:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin141:
	.byte	0

Lmono_eh_func_begin142:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin143:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin144:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin145:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin146:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin147:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin148:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin149:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin150:
	.byte	0

Lmono_eh_func_begin151:
	.byte	0

Lmono_eh_func_begin152:
	.byte	0

Lmono_eh_func_begin153:
	.byte	0

Lmono_eh_func_begin154:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin155:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin156:
	.byte	0

Lmono_eh_func_begin157:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin158:
	.byte	0

Lmono_eh_func_begin159:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8
	.byte	138
	.byte	6
	.byte	136
	.byte	7

Lmono_eh_func_begin160:
	.byte	0

Lmono_eh_func_begin161:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin162:
	.byte	0

Lmono_eh_func_begin163:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin164:
	.byte	0
	.byte	14
	.byte	20
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	134
	.byte	3
	.byte	133
	.byte	4
	.byte	132
	.byte	5
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin165:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin166:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin167:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin168:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin169:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Touch.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0
_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0:

	.byte 128,64,45,233,13,112,160,225,112,13,45,233,56,208,77,226,13,176,160,225,40,0,139,229,1,160,160,225,40,0,155,229
	.byte 20,160,128,229,40,0,155,229,0,224,218,229,8,16,154,229
bl _p_46

	.byte 0,96,160,225,0,0,80,227,0,0,160,19,1,0,160,3,0,0,80,227,0,0,160,19,1,0,160,3,0,0,203,229
	.byte 0,0,80,227,19,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . -12
	.byte 0,0,159,231,221,16,160,227
bl _p_30

	.byte 0,224,218,229,8,16,154,229
bl _p_48

	.byte 52,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 100
	.byte 0,0,159,231
bl _p_13

	.byte 52,16,155,229,48,0,139,229
bl _p_49

	.byte 48,0,155,229
bl _p_11

	.byte 6,0,160,225
bl _p_47

	.byte 4,0,139,229,4,0,155,229,32,0,139,229,4,0,155,229,0,0,80,227,22,0,0,10,32,0,155,229,0,64,144,229
	.byte 180,1,212,225,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 60
	.byte 1,16,159,231,1,0,80,225,13,0,0,58,16,0,148,229,0,16,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 60
	.byte 1,16,159,231,193,33,160,225,2,0,128,224,0,0,208,229,7,32,1,226,1,16,160,227,17,18,160,225,1,0,0,224
	.byte 0,0,80,227,2,0,0,26,1,0,160,227,36,0,139,229,1,0,0,234,0,0,160,227,36,0,139,229,36,0,155,229
	.byte 0,0,80,227,2,0,0,10,0,0,160,227,8,0,139,229,1,0,0,234,4,0,155,229,8,0,139,229,8,80,155,229
	.byte 5,0,160,225,0,0,80,227,0,0,160,19,1,0,160,3,0,0,80,227,0,0,160,19,1,0,160,3,0,0,203,229
	.byte 0,0,80,227,18,0,0,26,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . -12
	.byte 0,0,159,231,15,17,0,227
bl _p_30

	.byte 6,16,160,225
bl _p_48

	.byte 52,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 100
	.byte 0,0,159,231
bl _p_13

	.byte 52,16,155,229,48,0,139,229
bl _p_49

	.byte 48,0,155,229
bl _p_11

	.byte 5,0,160,225,10,16,160,225,0,32,149,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 96
	.byte 8,128,159,231,4,224,143,226,72,240,18,229,0,0,0,0,5,64,160,225,0,0,0,235,9,0,0,234,28,224,139,229
	.byte 40,0,155,229,12,0,139,229,0,0,160,227,16,0,139,229,12,0,155,229,16,16,155,229,20,16,128,229,28,192,155,229
	.byte 12,240,160,225,4,0,160,225,56,208,139,226,112,13,189,232,128,128,189,232

Lme_3a:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object:

	.byte 128,64,45,233,13,112,160,225,112,9,45,233,20,208,77,226,13,176,160,225,0,128,139,229,0,96,160,225,8,16,139,229
	.byte 0,0,155,229
bl _p_117

	.byte 0,80,160,225,0,0,149,229,0,0,160,227,4,0,139,229,8,0,155,229,0,0,80,227,3,0,0,10,8,0,155,229
bl _p_18

	.byte 0,64,160,225,0,0,0,234,0,64,160,227,0,0,155,229
bl _p_116

	.byte 0,128,160,225,6,0,160,225,4,16,160,225
bl _p_115

	.byte 0,80,160,225,20,208,139,226,112,9,189,232,128,128,189,232

Lme_ae:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,40,208,77,226,13,176,160,225,4,128,139,229,12,0,139,229,16,16,139,229
	.byte 4,0,155,229
bl _p_120

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 24
	.byte 0,0,159,231
bl _p_13

	.byte 36,0,139,229,16,16,155,229
bl _p_19

	.byte 36,0,155,229,28,0,139,229,0,0,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 28
	.byte 0,0,159,231,0,0,144,229,32,0,139,229,4,0,155,229
bl _p_119
bl _p_21

	.byte 28,16,155,229,32,48,155,229,24,0,139,229,0,32,160,227
bl _p_118

	.byte 24,16,155,229,12,0,155,229
bl _p_23

	.byte 40,208,139,226,0,9,189,232,128,128,189,232

Lme_af:
.text
	.align 2
	.no_dead_strip _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:

	.byte 128,64,45,233,13,112,160,225,0,9,45,233,24,208,77,226,13,176,160,225,4,128,139,229,12,0,139,229,16,16,139,229
	.byte 4,0,155,229
bl _p_121

	.byte 0,0,139,229,0,0,144,229,0,0,160,227,8,0,139,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 32
	.byte 8,128,159,231
bl _p_24

	.byte 0,32,160,225,16,16,155,229,0,32,146,229,0,128,159,229,0,0,0,234
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 36
	.byte 8,128,159,231,4,224,143,226,12,240,18,229,0,0,0,0,24,208,139,226,0,9,189,232,128,128,189,232

Lme_b0:
.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillTerminate_MonoTouch_UIKit_UIApplication
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_add_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_remove_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_FormFactor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_DisplayDensity
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute__ctor_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_Target
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_set_Target_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_IsConditionSatisfied
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_get_TouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_ViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_Request
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_get_TouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_ViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_Request
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_get_CurrentRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_set_CurrentRequest_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ShowViewModel_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_Request
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest_get_ViewModelInstance
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_MasterNavigationController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_set_MasterNavigationController_MonoTouch_UIKit_UINavigationController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_ApplicationDelegate
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_Window
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ShowFirstView_MonoTouch_UIKit_UIViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_SetWindowRootViewController_MonoTouch_UIKit_UIViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_OnMasterNavigationControllerCreated
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CreateNavigationController_MonoTouch_UIKit_UIViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_CurrentTopViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__PresentModalViewControllerb__0
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CreateModalNavigationController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CloseModalViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__2
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__3
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__6
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__7
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_CloseModalViewController
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__Closeb__0
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Window
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ApplicationDelegate
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateDebugTrace
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePluginManager
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewsContainer
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterTouchViewCreator_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewDispatcher
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializePlatformServices
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPlatformProperties
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateTouchSystemProperties
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterOldStylePlatformProperties
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterLifetime
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePresenter
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPresenter
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeBindingBuilder
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterBindingBuilderCallbacks
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateBindingBuilder
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterHolders
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterAssemblies
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewToViewModelNaming
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_intptr
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_ViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_Request
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_BindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__RequestMainThreadActionb__0
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ShowViewModelb__3
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ChangePresentationb__6
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__ctor
.no_dead_strip _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__OnViewCreateb__0
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_invoke_void__this___object_TEventArgs_object_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
.no_dead_strip _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_invoke_TResult__this__

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_DidEnterBackground_MonoTouch_UIKit_UIApplication
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillTerminate_MonoTouch_UIKit_UIApplication
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FinishedLaunching_MonoTouch_UIKit_UIApplication
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_add_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_remove_LifetimeChanged_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_FormFactor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties_get_DisplayDensity
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchPlatformProperties__ctor
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute__ctor_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_Target
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_set_Target_Cirrious_MvvmCross_Touch_Platform_MvxTouchFormFactor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxFormFactorSpecificAttribute_get_IsConditionSatisfied
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_get_TouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_ViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_Request
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_BindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_get_TouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleViewDidLoadCalled_object_System_EventArgs
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter_HandleDisposeCalled_object_System_EventArgs
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_ViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_Request
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_BindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_get_CurrentRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_set_CurrentRequest_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ShowViewModel_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_System_Func_1_string
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace_Trace_Cirrious_CrossCore_Platform_MvxTraceLevel_string_string_object__
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxDebugTrace__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_ViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_Request
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_get_BindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest_get_ViewModelInstance
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_MasterNavigationController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_set_MasterNavigationController_MonoTouch_UIKit_UINavigationController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_ApplicationDelegate
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_Window
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_PresentModalViewController_MonoTouch_UIKit_UIViewController_bool
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_ShowFirstView_MonoTouch_UIKit_UIViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_SetWindowRootViewController_MonoTouch_UIKit_UIViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_OnMasterNavigationControllerCreated
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CreateNavigationController_MonoTouch_UIKit_UIViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_get_CurrentTopViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__PresentModalViewControllerb__0
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CreateModalNavigationController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_CloseModalViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__2
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__CloseModalViewControllerb__3
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__6
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalNavSupportTouchViewPresenter__Closeb__7
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_NativeModalViewControllerDisappearedOnItsOwn
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_CloseModalViewController
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxModalSupportTouchViewPresenter__Closeb__0
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_MonoTouch_UIKit_UIWindow
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup__ctor_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Window
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ApplicationDelegate
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateDebugTrace
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePluginManager
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_AddPluginsLoaders_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewsContainer
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterTouchViewCreator_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewDispatcher
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializePlatformServices
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPlatformProperties
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateTouchSystemProperties
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterOldStylePlatformProperties
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterLifetime
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreatePresenter
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterPresenter
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeLastChance
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_InitializeBindingBuilder
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_RegisterBindingBuilderCallbacks
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateBindingBuilder
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillBindingNames_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillValueConverters_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterHolders
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_ValueConverterAssemblies
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_FillTargetFactories_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_CreateViewToViewModelNaming
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_intptr
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_ViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_ViewModel_Cirrious_MvvmCross_ViewModels_IMvxViewModel
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_Request
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_Request_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_BindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_BindingContext_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContext
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__c__DisplayClass1__RequestMainThreadActionb__0
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass4__ShowViewModelb__3
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__c__DisplayClass7__ChangePresentationb__6
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__ctor
	bl _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods__c__DisplayClass1__OnViewCreateb__0
	bl method_addresses
	bl _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	bl _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	bl _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_invoke_void__this___object_TEventArgs_object_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_string_invoke_TResult__this__
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_invoke_void__this___T_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_invoke_void__this___T_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
	bl _Cirrious_MvvmCross_Touch__wrapper_delegate_invoke_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_invoke_TResult__this__
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 183,10,19,2
	.short 0, 10, 24, 38, 48, 58, 69, 85
	.short 96, 107, 118, 129, 140, 151, 162, 173
	.short 184, 195, 211
	.byte 0,0,0,1,2,2,2,2,3,4,20,2,2,4,255,255,255,255,228,0,30,2,4,4,44,4,2,2,2,4,4,5
	.byte 9,255,255,255,255,180,0,89,2,2,3,3,4,2,2,2,111,2,5,4,5,5,2,2,2,3,128,144,4,2,2,2
	.byte 2,2,2,2,7,128,172,255,255,255,255,84,0,128,174,3,8,2,7,7,6,128,213,6,2,2,2,2,3,3,4,2
	.byte 128,241,2,2,2,2,3,2,4,2,2,129,8,2,2,2,2,2,2,4,4,2,129,40,9,2,2,2,2,3,2,2
	.byte 2,129,73,3,4,17,23,2,2,2,2,2,129,137,4,2,15,2,2,2,2,2,3,129,176,2,3,4,3,2,3,3
	.byte 4,3,129,205,3,4,2,2,11,3,2,4,4,129,244,2,7,2,2,2,3,3,4,2,130,17,2,2,2,7,15,2
	.byte 2,2,7,130,60,4,2,255,255,255,253,190,130,68,2,4,4,4,4,130,90,4,4
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,0,0,0,0,0,0,0
	.long 0,0,636,175,0,0,0,0
	.long 0,0,0,730,182,0,706,180
	.long 0,0,0,0,0,0,0,0
	.long 0,0,670,177,0,682,178,0
	.long 653,176,0,0,0,0,0,0
	.long 0,718,181,0,0,0,0,619
	.long 174,19,694,179,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 9,174,619,175,636,176,653,177
	.long 670,178,682,179,694,180,706,181
	.long 718,182,730
.section __TEXT, __const
	.align 3
class_name_table:

	.short 73, 13, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 23, 0, 25, 0, 1
	.short 0, 29, 0, 0, 0, 0, 0, 10
	.short 0, 24, 0, 12, 0, 0, 0, 0
	.short 0, 35, 0, 0, 0, 0, 0, 36
	.short 0, 27, 0, 11, 77, 0, 0, 0
	.short 0, 0, 0, 3, 0, 0, 0, 0
	.short 0, 15, 0, 0, 0, 19, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 2
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 7, 74, 8
	.short 75, 0, 0, 0, 0, 0, 0, 22
	.short 0, 16, 76, 0, 0, 37, 0, 0
	.short 0, 0, 0, 31, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 4, 73, 0, 0, 0, 0, 5
	.short 0, 6, 0, 34, 0, 0, 0, 0
	.short 0, 0, 0, 26, 0, 17, 78, 33
	.short 0, 28, 0, 9, 0, 14, 0, 18
	.short 79, 20, 0, 21, 0, 30, 0, 32
	.short 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 131,10,14,2
	.short 0, 11, 22, 33, 44, 55, 66, 77
	.short 88, 99, 110, 121, 132, 143
	.byte 130,230,2,1,1,1,4,12,6,3,4,131,12,5,12,2,2,12,2,3,3,3,131,61,4,5,3,7,5,5,4,2
	.byte 4,131,103,7,3,4,3,3,4,3,5,3,131,141,5,3,3,3,4,4,4,5,4,131,180,4,4,4,4,2,2,5
	.byte 3,4,131,216,2,2,4,2,2,5,4,2,2,131,245,2,2,4,4,4,2,2,3,4,132,20,4,3,12,12,3,12
	.byte 4,3,12,132,97,3,12,12,3,6,12,3,6,12,132,169,6,12,4,12,12,16,6,16,6,133,10,4,4,4,4,3
	.byte 6,3,3,6,133,49,12,5,3,4,12,2,4,4,5,133,105
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 183,10,19,2
	.short 0, 11, 27, 42, 53, 64, 75, 91
	.short 102, 113, 124, 135, 146, 157, 168, 179
	.short 190, 201, 217
	.byte 0,0,0,138,103,3,3,3,3,3,3,138,124,3,3,3,255,255,255,245,123,0,138,136,23,33,23,138,218,3,3,3
	.byte 3,3,3,3,3,255,255,255,245,14,0,138,245,3,3,3,3,3,3,3,3,139,16,3,3,3,3,3,3,3,3,3
	.byte 139,46,3,3,3,3,3,3,3,3,13,139,86,255,255,255,244,170,0,139,89,3,3,3,3,3,3,139,110,3,3,3
	.byte 3,3,3,3,3,3,139,140,3,3,3,3,3,3,3,3,3,139,170,3,3,3,3,3,3,3,3,3,139,200,3,3
	.byte 3,3,3,3,3,3,3,139,230,3,3,3,3,3,3,3,3,3,140,4,3,3,3,3,3,3,3,3,3,140,34,3
	.byte 3,3,3,3,3,3,3,3,140,64,3,3,3,3,3,3,3,3,3,140,94,3,3,3,3,3,3,3,3,3,140,124
	.byte 3,3,3,3,3,3,3,3,3,140,154,3,3,255,255,255,243,96,140,163,29,29,29,3,3,141,3,3,3
.section __TEXT, __const
	.align 3
unwind_info:

	.byte 31,12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.byte 29,12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,48,68,13,11,23,12
	.byte 13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11,23,12,13,0,72,14,8,135,2,68
	.byte 14,16,136,4,139,3,142,1,68,14,40,68,13,11
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 37,10,4,2
	.short 0, 11, 24, 36
	.byte 141,12,7,7,7,7,99,99,7,69,25,142,90,23,24,23,44,7,128,219,41,128,223,57,144,246,5,5,26,7,128,219
	.byte 23,31,43,44,146,180,128,198,128,243,23,23,23,23

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Touch_plt:
_p_1_plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent
plt_Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_FireLifetimeChanged_Cirrious_MvvmCross_Platform_MvxLifetimeEvent:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 524,1386
_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 528,1388
_p_3_plt_System_Delegate_Combine_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Combine_System_Delegate_System_Delegate
plt_System_Delegate_Combine_System_Delegate_System_Delegate:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 532,1414
_p_4_plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline_llvm:
	.no_dead_strip plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline
plt__jit_icall_llvm_throw_corlib_exception_abs_trampoline:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 536,1419
_p_5_plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs__System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_llvm:
	.no_dead_strip plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs__System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs
plt_System_Threading_Interlocked_CompareExchange_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs__System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs_System_EventHandler_1_Cirrious_MvvmCross_Platform_MvxLifetimeEventArgs:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 540,1464
_p_6_plt_System_Delegate_Remove_System_Delegate_System_Delegate_llvm:
	.no_dead_strip plt_System_Delegate_Remove_System_Delegate_System_Delegate
plt_System_Delegate_Remove_System_Delegate_System_Delegate:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 544,1476
_p_7_plt_MonoTouch_UIKit_UIApplicationDelegate__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplicationDelegate__ctor
plt_MonoTouch_UIKit_UIApplicationDelegate__ctor:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 548,1481
_p_8_plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice
plt_MonoTouch_UIKit_UIDevice_get_CurrentDevice:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 552,1486
_p_9_plt_MonoTouch_UIKit_UIDevice_get_UserInterfaceIdiom_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIDevice_get_UserInterfaceIdiom
plt_MonoTouch_UIKit_UIDevice_get_UserInterfaceIdiom:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 556,1491
_p_10_plt__jit_icall_mono_create_corlib_exception_0_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_0
plt__jit_icall_mono_create_corlib_exception_0:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 560,1496
_p_11_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 564,1529
_p_12_plt_MonoTouch_UIKit_UIScreen_get_MainScreen_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIScreen_get_MainScreen
plt_MonoTouch_UIKit_UIScreen_get_MainScreen:
_p_12:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 568,1557
_p_13_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_13:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 572,1562
_p_14_plt_MonoTouch_ObjCRuntime_Selector__ctor_string_llvm:
	.no_dead_strip plt_MonoTouch_ObjCRuntime_Selector__ctor_string
plt_MonoTouch_ObjCRuntime_Selector__ctor_string:
_p_14:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 576,1585
_p_15_plt_System_Math_Round_double_llvm:
	.no_dead_strip plt_System_Math_Round_double
plt_System_Math_Round_double:
_p_15:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 580,1590
_p_16_plt__rgctx_fetch_0_llvm:
	.no_dead_strip plt__rgctx_fetch_0
plt__rgctx_fetch_0:
_p_16:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 584,1622
_p_17_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_TTargetViewModel_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string:
_p_17:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 588,1644
_p_18_plt_Cirrious_MvvmCross_Platform_MvxSimplePropertyDictionaryExtensionMethods_ToSimplePropertyDictionary_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Platform_MvxSimplePropertyDictionaryExtensionMethods_ToSimplePropertyDictionary_object
plt_Cirrious_MvvmCross_Platform_MvxSimplePropertyDictionaryExtensionMethods_ToSimplePropertyDictionary_object:
_p_18:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 592,1662
_p_19_plt_Cirrious_MvvmCross_ViewModels_MvxBundle__ctor_System_Collections_Generic_IDictionary_2_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxBundle__ctor_System_Collections_Generic_IDictionary_2_string_string
plt_Cirrious_MvvmCross_ViewModels_MvxBundle__ctor_System_Collections_Generic_IDictionary_2_string_string:
_p_19:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 596,1667
_p_20_plt__rgctx_fetch_1_llvm:
	.no_dead_strip plt__rgctx_fetch_1
plt__rgctx_fetch_1:
_p_20:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 600,1707
_p_21_plt__jit_icall_mono_object_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_specific
plt__jit_icall_mono_object_new_specific:
_p_21:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 604,1715
_p_22_plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1_TTargetViewModel__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1_TTargetViewModel__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1_TTargetViewModel__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_22:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 608,1742
_p_23_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest:
_p_23:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 612,1761
_p_24_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator:
_p_24:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 616,1763
_p_25_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties:
_p_25:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 620,1775
_p_26_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
_p_26:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 624,1787
_p_27_plt_Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
plt_Cirrious_MvvmCross_Touch_Views_MvxBindingViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
_p_27:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 628,1789
_p_28_plt_Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
plt_Cirrious_CrossCore_Touch_Views_MvxBaseViewControllerAdapter__ctor_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
_p_28:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 632,1791
_p_29_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContext__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContext__ctor
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContext__ctor:
_p_29:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 636,1796
_p_30_plt__jit_icall_mono_helper_ldstr_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr
plt__jit_icall_mono_helper_ldstr:
_p_30:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 640,1801
_p_31_plt__jit_icall_mono_create_corlib_exception_2_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_2
plt__jit_icall_mono_create_corlib_exception_2:
_p_31:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 644,1821
_p_32_plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_ClearAllBindings_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_ClearAllBindings_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner
plt_Cirrious_MvvmCross_Binding_BindingContext_MvxBindingContextOwnerExtensions_ClearAllBindings_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingContextOwner:
_p_32:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 648,1854
_p_33_plt__jit_icall_mono_array_new_specific_llvm:
	.no_dead_strip plt__jit_icall_mono_array_new_specific
plt__jit_icall_mono_array_new_specific:
_p_33:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 652,1859
_p_34_plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Warning_string_object__:
_p_34:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 656,1885
_p_35_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor:
_p_35:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 660,1890
_p_36_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController
plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerAdaptingExtensions_AdaptForBinding_Cirrious_CrossCore_Touch_Views_IMvxEventSourceViewController:
_p_36:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 664,1895
_p_37_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTabBarController__ctor_intptr:
_p_37:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 668,1897
_p_38_plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext
plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_get_DataContext:
_p_38:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 672,1902
_p_39_plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object
plt_Cirrious_MvvmCross_Touch_Views_MvxTabBarViewController_set_DataContext_object:
_p_39:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 676,1904
_p_40_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
_p_40:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 680,1906
_p_41_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_MonoTouch_UIKit_UITableViewStyle:
_p_41:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 684,1909
_p_42_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_intptr:
_p_42:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 688,1914
_p_43_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceTableViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_43:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 692,1919
_p_44_plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext
plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_get_DataContext:
_p_44:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 696,1924
_p_45_plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object
plt_Cirrious_MvvmCross_Touch_Views_MvxTableViewController_set_DataContext_object:
_p_45:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 700,1926
_p_46_plt_Cirrious_MvvmCross_Views_MvxViewsContainer_GetViewType_System_Type_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Views_MvxViewsContainer_GetViewType_System_Type
plt_Cirrious_MvvmCross_Views_MvxViewsContainer_GetViewType_System_Type:
_p_46:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 704,1928
_p_47_plt_System_Activator_CreateInstance_System_Type_llvm:
	.no_dead_strip plt_System_Activator_CreateInstance_System_Type
plt_System_Activator_CreateInstance_System_Type:
_p_47:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 708,1933
_p_48_plt_string_Concat_object_object_llvm:
	.no_dead_strip plt_string_Concat_object_object
plt_string_Concat_object_object:
_p_48:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 712,1938
_p_49_plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string
plt_Cirrious_CrossCore_Exceptions_MvxException__ctor_string:
_p_49:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 716,1943
_p_50_plt_Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel
plt_Cirrious_MvvmCross_Touch_Views_MvxViewModelInstanceRequest__ctor_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
_p_50:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 720,1948
_p_51_plt_Cirrious_MvvmCross_Views_MvxViewsContainer__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Views_MvxViewsContainer__ctor
plt_Cirrious_MvvmCross_Views_MvxViewsContainer__ctor:
_p_51:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 724,1950
_p_52_plt__jit_icall_mono_domain_get_llvm:
	.no_dead_strip plt__jit_icall_mono_domain_get
plt__jit_icall_mono_domain_get:
_p_52:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 728,1955
_p_53_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher__ctor_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher__ctor
plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher__ctor:
_p_53:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 732,1973
_p_54_plt__jit_icall_mono_class_static_field_address_llvm:
	.no_dead_strip plt__jit_icall_mono_class_static_field_address
plt__jit_icall_mono_class_static_field_address:
_p_54:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 736,1978
_p_55_plt_MonoTouch_UIKit_UIApplication_get_SharedApplication_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UIApplication_get_SharedApplication
plt_MonoTouch_UIKit_UIApplication_get_SharedApplication:
_p_55:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 740,2012
_p_56_plt_MonoTouch_Foundation_NSObject_BeginInvokeOnMainThread_MonoTouch_Foundation_NSAction_llvm:
	.no_dead_strip plt_MonoTouch_Foundation_NSObject_BeginInvokeOnMainThread_MonoTouch_Foundation_NSAction
plt_MonoTouch_Foundation_NSObject_BeginInvokeOnMainThread_MonoTouch_Foundation_NSAction:
_p_56:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 744,2017
_p_57_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor
plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher__ctor:
_p_57:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 748,2022
_p_58_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action
plt_Cirrious_MvvmCross_Touch_Views_MvxTouchUIThreadDispatcher_RequestMainThreadAction_System_Action:
_p_58:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 752,2024
_p_59_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_59:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 756,2026
_p_60_plt_string_Concat_object___llvm:
	.no_dead_strip plt_string_Concat_object__
plt_string_Concat_object__:
_p_60:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 760,2056
_p_61_plt_System_Console_WriteLine_string_llvm:
	.no_dead_strip plt_System_Console_WriteLine_string
plt_System_Console_WriteLine_string:
_p_61:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 764,2061
_p_62_plt_System_Console_WriteLine_string_object___llvm:
	.no_dead_strip plt_System_Console_WriteLine_string_object__
plt_System_Console_WriteLine_string_object__:
_p_62:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 768,2066
_p_63_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor:
_p_63:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 772,2071
_p_64_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_intptr:
_p_64:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 776,2076
_p_65_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_65:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 780,2081
_p_66_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_get_DataContext:
_p_66:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 784,2086
_p_67_plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object
plt_Cirrious_MvvmCross_Touch_Views_MvxViewController_set_DataContext_object:
_p_67:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 788,2088
_p_68_plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest__ctor_System_Type_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest__ctor_System_Type_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest__ctor_System_Type_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_68:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 792,2090
_p_69_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint
plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxBaseTouchViewPresenter_ChangePresentation_Cirrious_MvvmCross_ViewModels_MvxPresentationHint:
_p_69:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 796,2095
_p_70_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView
plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_ReflectionGetViewModel_Cirrious_MvvmCross_Views_IMvxView:
_p_70:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 800,2097
_p_71_plt_MonoTouch_UIKit_UINavigationController__ctor_MonoTouch_UIKit_UIViewController_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINavigationController__ctor_MonoTouch_UIKit_UIViewController
plt_MonoTouch_UIKit_UINavigationController__ctor_MonoTouch_UIKit_UIViewController:
_p_71:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 804,2102

.set _p_72_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow_llvm, _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter__ctor_MonoTouch_UIKit_UIApplicationDelegate_MonoTouch_UIKit_UIWindow
_p_73_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Show_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
_p_73:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 812,2109
_p_74_plt_MonoTouch_UIKit_UINavigationController__ctor_llvm:
	.no_dead_strip plt_MonoTouch_UIKit_UINavigationController__ctor
plt_MonoTouch_UIKit_UINavigationController__ctor:
_p_74:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 816,2111
_p_75_plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Error_string_object__:
_p_75:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 820,2116

.set _p_76_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController_llvm, _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_CloseModalViewController
_p_77_plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel
plt_Cirrious_MvvmCross_Touch_Views_Presenters_MvxTouchViewPresenter_Close_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
_p_77:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 828,2123
_p_78_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginManager__ctor_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginManager__ctor
plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginManager__ctor:
_p_78:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 832,2125
_p_79_plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry__ctor_string_System_Collections_Generic_IDictionary_2_string_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry__ctor_string_System_Collections_Generic_IDictionary_2_string_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin
plt_Cirrious_CrossCore_Plugins_MvxLoaderPluginRegistry__ctor_string_System_Collections_Generic_IDictionary_2_string_System_Func_1_Cirrious_CrossCore_Plugins_IMvxPlugin:
_p_79:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 836,2130

.set _p_80_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor_llvm, _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer__ctor
_p_81_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator_Cirrious_MvvmCross_Touch_Views_IMvxTouchViewCreator:
_p_81:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 844,2137
_p_82_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest:
_p_82:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 848,2149

.set _p_83_plt_Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter_llvm, _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxTouchSetup_get_Presenter

.set _p_84_plt_Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm, _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Views_MvxTouchViewDispatcher__ctor_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
_p_85_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem_Cirrious_CrossCore_Touch_Platform_IMvxTouchSystem:
_p_85:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 860,2166
_p_86_plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor
plt_Cirrious_CrossCore_Touch_Platform_MvxTouchSystem__ctor:
_p_86:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 864,2178
_p_87_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties_Cirrious_MvvmCross_Touch_Platform_IMvxTouchPlatformProperties:
_p_87:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 868,2183
_p_88_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Platform_IMvxLifetime_Cirrious_MvvmCross_Platform_IMvxLifetime_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Platform_IMvxLifetime_Cirrious_MvvmCross_Platform_IMvxLifetime
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Platform_IMvxLifetime_Cirrious_MvvmCross_Platform_IMvxLifetime:
_p_88:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 872,2195
_p_89_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter_Cirrious_MvvmCross_Touch_Views_Presenters_IMvxTouchViewPresenter:
_p_89:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 876,2207
_p_90_plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost
plt_Cirrious_CrossCore_Mvx_RegisterSingleton_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost_Cirrious_CrossCore_Touch_Views_IMvxTouchModalHost:
_p_90:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 880,2219
_p_91_plt__jit_icall_mono_ldvirtfn_llvm:
	.no_dead_strip plt__jit_icall_mono_ldvirtfn
plt__jit_icall_mono_ldvirtfn:
_p_91:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 884,2231
_p_92_plt_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry__ctor_object_intptr
plt_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry__ctor_object_intptr:
_p_92:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 888,2247
_p_93_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry
plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry:
_p_93:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 892,2258
_p_94_plt_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry__ctor_object_intptr
plt_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry__ctor_object_intptr:
_p_94:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 896,2270
_p_95_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry
plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry:
_p_95:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 900,2281
_p_96_plt_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry__ctor_object_intptr_llvm:
	.no_dead_strip plt_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry__ctor_object_intptr
plt_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry__ctor_object_intptr:
_p_96:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 904,2293
_p_97_plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
plt_Cirrious_CrossCore_Mvx_CallbackWhenRegistered_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
_p_97:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 908,2304
_p_98_plt_Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry
plt_Cirrious_MvvmCross_Binding_Touch_MvxTouchBindingBuilder__ctor_System_Action_1_Cirrious_MvvmCross_Binding_Bindings_Target_Construction_IMvxTargetBindingFactoryRegistry_System_Action_1_Cirrious_CrossCore_Converters_IMvxValueConverterRegistry_System_Action_1_Cirrious_MvvmCross_Binding_BindingContext_IMvxBindingNameRegistry:
_p_98:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 912,2316
_p_99_plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly
plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly:
_p_99:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 916,2321
_p_100_plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Type_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Type
plt_Cirrious_MvvmCross_Binding_Binders_MvxRegistryFillerExtensions_Fill_Cirrious_CrossCore_Converters_IMvxValueConverter_Cirrious_CrossCore_Platform_IMvxNamedInstanceRegistry_1_Cirrious_CrossCore_Converters_IMvxValueConverter_System_Collections_Generic_IEnumerable_1_System_Type:
_p_100:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 920,2333
_p_101_plt_System_Collections_Generic_List_1_System_Reflection_Assembly_AddRange_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly_llvm:
	.no_dead_strip plt_System_Collections_Generic_List_1_System_Reflection_Assembly_AddRange_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly
plt_System_Collections_Generic_List_1_System_Reflection_Assembly_AddRange_System_Collections_Generic_IEnumerable_1_System_Reflection_Assembly:
_p_101:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 924,2345
_p_102_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_MonoTouch_UIKit_UICollectionViewLayout:
_p_102:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 928,2356
_p_103_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_intptr:
_p_103:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 932,2361
_p_104_plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle
plt_Cirrious_CrossCore_Touch_Views_MvxEventSourceCollectionViewController__ctor_string_MonoTouch_Foundation_NSBundle:
_p_104:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 936,2366
_p_105_plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext
plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_get_DataContext:
_p_105:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 940,2371
_p_106_plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object
plt_Cirrious_MvvmCross_Touch_Views_MvxCollectionViewController_set_DataContext_object:
_p_106:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 944,2374
_p_107_plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Views_IMvxView_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Views_IMvxView_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel
plt_Cirrious_MvvmCross_Views_MvxViewExtensionMethods_OnViewCreate_Cirrious_MvvmCross_Views_IMvxView_System_Func_1_Cirrious_MvvmCross_ViewModels_IMvxViewModel:
_p_107:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 948,2377
_p_108_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxViewModelLoader_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxViewModelLoader
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_ViewModels_IMvxViewModelLoader:
_p_108:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 952,2382
_p_109_plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_Trace_string_object__:
_p_109:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 956,2394
_p_110_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_MvvmCross_Touch_Views_IMvxCurrentRequest:
_p_110:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 960,2399
_p_111_plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher_ExceptionMaskedAction_System_Action_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher_ExceptionMaskedAction_System_Action
plt_Cirrious_CrossCore_Core_MvxMainThreadDispatcher_ExceptionMaskedAction_System_Action:
_p_111:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 964,2411
_p_112_plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_string_string_object___llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_string_string_object__
plt_Cirrious_CrossCore_Platform_MvxTrace_TaggedTrace_string_string_object__:
_p_112:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 968,2416
_p_113_plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView
plt_Cirrious_MvvmCross_Touch_Views_MvxViewControllerExtensionMethods_LoadViewModel_Cirrious_MvvmCross_Touch_Views_IMvxTouchView:
_p_113:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 972,2421
_p_114_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_114:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 976,2424
_p_115_plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
plt_Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string:
_p_115:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 980,2462
_p_116_plt__rgctx_fetch_2_llvm:
	.no_dead_strip plt__rgctx_fetch_2
plt__rgctx_fetch_2:
_p_116:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 984,2497
_p_117_plt__rgctx_fetch_3_llvm:
	.no_dead_strip plt__rgctx_fetch_3
plt__rgctx_fetch_3:
_p_117:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 988,2519
_p_118_plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1___0__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy_llvm:
	.no_dead_strip plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1___0__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy
plt_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_1___0__ctor_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_IMvxBundle_Cirrious_MvvmCross_ViewModels_MvxRequestedBy:
_p_118:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 992,2552
_p_119_plt__rgctx_fetch_4_llvm:
	.no_dead_strip plt__rgctx_fetch_4
plt__rgctx_fetch_4:
_p_119:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 996,2588
_p_120_plt__rgctx_fetch_5_llvm:
	.no_dead_strip plt__rgctx_fetch_5
plt__rgctx_fetch_5:
_p_120:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 1000,2596
_p_121_plt__rgctx_fetch_6_llvm:
	.no_dead_strip plt__rgctx_fetch_6
plt__rgctx_fetch_6:
_p_121:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Touch_got - . + 1004,2638
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 8
	.asciz "Cirrious.MvvmCross.Touch"
	.asciz "1E871AF4-634E-4A45-AB5B-DCAE1A4CA1E3"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.MvvmCross"
	.asciz "066A9949-60EF-4499-87FB-90DB7F8EAEA9"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "monotouch"
	.asciz "3672C737-E295-496E-B5B5-0E73D121DCCB"
	.asciz ""
	.asciz "84e04ff9cfb79065"
	.align 3

	.long 1,0,0,0,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
	.asciz "Cirrious.MvvmCross.Binding"
	.asciz "0CA1A6F2-0F7E-44D7-B6B8-AE8E0BEDDDE8"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "Cirrious.CrossCore.Touch"
	.asciz "8754F76F-9641-4818-B901-A9D67CFE8550"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,0,0,0,0
	.asciz "Cirrious.MvvmCross.Binding.Touch"
	.asciz "AF50D302-0E63-49BF-AF60-DEF8AC108924"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "1E871AF4-634E-4A45-AB5B-DCAE1A4CA1E3"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Touch"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Touch_got
	.align 2
	.long _Cirrious_MvvmCross_Touch__Cirrious_MvvmCross_Touch_Platform_MvxApplicationDelegate_WillEnterForeground_MonoTouch_UIKit_UIApplication
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 131,1012,122,183,11,387000831,0,5344
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Touch_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Touch_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,0,0,0,0,0,0,0,0,0,1,4,0,2,6,5,0,2,6,5,0,0,0,0,0,2,8,7,0,0,0,0
	.byte 0,2,9,10,0,2,12,11,0,2,12,11,0,2,13,11,0,0,0,0,0,0,0,2,15,14,0,2,17,16,0,3
	.byte 19,18,18,0,7,18,19,18,18,21,20,18,0,11,18,18,19,18,18,23,22,19,18,18,19,0,0,0,0,0,1,24
	.byte 0,1,25,0,2,26,26,0,0,0,0,0,0,0,0,0,0,0,3,19,18,18,0,2,18,18,0,3,19,18,18,0
	.byte 3,19,18,18,0,0,0,0,0,0,0,1,24,0,1,25,0,2,26,26,0,0,0,0,0,0,0,0,0,0,0,0
	.byte 0,0,0,5,28,18,18,28,27,0,1,29,0,0,0,1,30,0,6,35,34,33,32,30,31,0,0,0,5,36,40,39
	.byte 38,37,0,5,41,40,43,42,37,0,4,23,44,45,44,0,4,23,46,45,46,0,4,23,46,45,46,0,0,0,0,0
	.byte 0,0,0,0,1,24,0,1,25,0,2,26,26,0,0,0,0,0,0,0,0,0,0,0,0,0,1,47,0,0,0,2
	.byte 23,48,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,49,49,0,2,19,28,0,0,0,8
	.byte 23,52,18,18,23,51,23,50,0,7,53,35,55,54,32,53,53,0,0,0,0,0,0,0,0,0,1,56,0,0,0,0
	.byte 0,0,0,5,57,28,19,19,57,0,1,56,0,2,23,58,0,15,62,35,64,63,32,62,62,59,35,61,60,32,59,59
	.byte 65,0,21,18,23,73,23,72,65,69,35,71,70,32,69,69,66,35,68,67,32,66,66,18,0,0,0,0,0,0,0,0
	.byte 0,0,0,5,57,28,19,19,57,0,2,23,58,0,0,0,13,18,23,73,23,72,74,35,76,75,32,74,74,18,0,0
	.byte 0,0,0,0,0,0,0,0,0,1,77,0,3,79,80,78,0,0,0,1,81,0,2,83,82,0,1,84,0,0,0,1
	.byte 85,0,1,86,0,2,88,87,0,1,89,0,0,0,1,90,0,2,92,91,0,0,0,0,0,9,93,101,100,99,98,97
	.byte 96,95,94,0,1,102,0,0,0,2,104,103,0,2,106,105,0,2,108,107,0,0,0,5,111,112,113,110,109,0,0,0
	.byte 0,0,0,0,1,24,0,1,25,0,2,26,26,0,0,0,0,0,0,0,0,0,0,0,5,114,118,117,116,115,0,13
	.byte 119,125,124,23,123,122,28,119,121,119,120,119,27,0,0,0,0,0,0,0,4,128,128,23,127,126,0,0,0,1,128,129
	.byte 0,0,0,0,0,0,0,2,9,10,0,2,11,12,0,1,128,130,0,1,128,130,0,1,128,130,0,1,128,130,0,1
	.byte 128,130,0,1,128,130,5,30,0,0,1,255,253,0,0,0,1,11,0,198,0,0,17,0,1,7,130,102,255,253,0,0
	.byte 0,1,11,0,198,0,0,18,0,1,7,130,102,255,253,0,0,0,1,11,0,198,0,0,19,0,1,7,130,102,255,252
	.byte 0,0,0,1,1,3,219,0,0,1,255,252,0,0,0,1,1,3,219,0,0,3,255,252,0,0,0,1,1,3,219,0
	.byte 0,4,255,252,0,0,0,1,1,3,219,0,0,5,255,252,0,0,0,1,1,3,219,0,0,6,255,252,0,0,0,1
	.byte 1,3,219,0,0,9,12,0,39,42,47,14,2,91,2,34,255,254,0,0,0,0,255,43,0,0,1,11,3,219,0,0
	.byte 1,17,0,1,14,2,123,3,14,2,84,2,16,2,94,2,92,34,255,254,0,0,0,0,255,43,0,0,3,6,15,6
	.byte 16,34,255,254,0,0,0,0,255,43,0,0,4,6,2,14,1,17,14,1,14,23,1,15,11,2,128,220,3,14,2,107
	.byte 5,6,197,0,2,212,17,0,113,14,6,1,2,130,123,1,6,196,0,1,138,6,196,0,1,139,23,2,93,2,6,31
	.byte 14,2,30,4,14,1,26,13,2,129,235,1,134,197,14,1,34,14,2,59,3,6,128,167,50,128,167,30,2,59,3,14
	.byte 1,35,14,2,129,251,1,6,128,169,50,128,169,30,2,129,251,1,14,1,36,6,128,171,50,128,171,17,0,129,127,14
	.byte 2,84,4,17,0,129,131,16,2,94,2,90,17,0,129,137,11,2,36,2,17,0,131,0,17,0,130,119,17,0,130,15
	.byte 16,1,28,23,6,108,50,108,14,2,128,197,3,23,1,7,17,0,132,19,16,1,29,26,6,116,50,116,16,1,29,25
	.byte 6,115,50,115,11,2,128,197,3,16,1,29,28,6,118,50,118,16,1,29,27,6,117,50,117,17,0,133,12,17,0,132
	.byte 137,16,1,30,30,6,124,50,124,14,1,23,14,2,99,4,17,0,133,163,14,2,100,4,14,1,19,34,255,254,0,0
	.byte 0,0,255,43,0,0,5,34,255,254,0,0,0,0,255,43,0,0,6,14,1,22,34,255,254,0,0,0,0,255,43,0
	.byte 0,7,14,2,4,6,14,1,9,34,255,254,0,0,0,0,255,43,0,0,8,34,255,254,0,0,0,0,255,43,0,0
	.byte 9,14,1,28,34,255,254,0,0,0,0,255,43,0,0,10,34,255,254,0,0,0,0,255,43,0,0,11,6,128,148,14
	.byte 3,219,0,0,4,34,255,254,0,0,0,0,255,43,0,0,12,6,128,151,14,3,219,0,0,5,34,255,254,0,0,0
	.byte 0,255,43,0,0,13,6,128,147,14,3,219,0,0,6,34,255,254,0,0,0,0,255,43,0,0,14,14,2,2,7,34
	.byte 255,254,0,0,0,0,255,43,0,0,15,34,255,254,0,0,0,0,255,43,0,0,16,14,3,219,0,0,7,4,2,130
	.byte 50,1,1,2,130,165,1,16,7,132,221,135,229,14,3,219,0,0,8,4,2,130,50,1,1,2,128,242,1,16,7,132
	.byte 243,135,229,14,6,1,2,130,146,1,17,0,133,177,17,0,133,187,14,2,27,2,17,2,132,74,14,1,37,14,3,219
	.byte 0,0,9,6,128,173,50,128,173,30,3,219,0,0,9,6,30,34,255,254,0,0,0,0,255,43,0,0,18,6,194,0
	.byte 1,1,11,1,26,17,0,133,217,34,255,254,0,0,0,0,255,43,0,0,17,6,1,17,0,129,57,17,0,129,89,6
	.byte 194,0,0,92,6,194,0,0,93,33,3,8,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116
	.byte 114,102,114,101,101,0,3,193,0,16,63,7,42,108,108,118,109,95,116,104,114,111,119,95,99,111,114,108,105,98,95,101
	.byte 120,99,101,112,116,105,111,110,95,97,98,115,95,116,114,97,109,112,111,108,105,110,101,0,3,255,254,0,0,0,0,255
	.byte 43,0,0,1,3,193,0,16,65,3,195,0,6,164,3,195,0,4,148,3,195,0,4,145,7,30,109,111,110,111,95,99
	.byte 114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,48,0,7,25,109,111,110,111,95
	.byte 97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105,111,110,0,3,195,0,5,35,7,20,109,111,110,111
	.byte 95,111,98,106,101,99,116,95,110,101,119,95,102,97,115,116,0,3,195,0,3,169,3,193,0,17,207,5,30,0,1,255
	.byte 255,255,255,255,17,255,253,0,0,0,1,11,0,198,0,0,17,0,1,7,134,59,35,134,69,140,17,255,253,0,0,0
	.byte 1,11,0,198,0,0,18,0,1,7,134,59,3,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,134,59,3,194
	.byte 0,1,27,3,194,0,1,13,5,30,0,1,255,255,255,255,255,18,255,253,0,0,0,1,11,0,198,0,0,18,0,1
	.byte 7,134,136,4,2,113,2,1,7,134,136,35,134,146,150,5,7,134,163,7,24,109,111,110,111,95,111,98,106,101,99,116
	.byte 95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,255,253,0,0,0,7,134,163,2,198,0,1,200,1,7,134,136
	.byte 0,3,20,3,255,254,0,0,0,0,255,43,0,0,3,3,255,254,0,0,0,0,255,43,0,0,4,3,43,3,28,3
	.byte 198,0,0,30,3,197,0,2,139,7,17,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,0,7,30,109
	.byte 111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112,116,105,111,110,95,50,0,3,197
	.byte 0,4,41,7,23,109,111,110,111,95,97,114,114,97,121,95,110,101,119,95,115,112,101,99,105,102,105,99,0,3,196,0
	.byte 1,124,3,198,0,0,68,3,26,3,198,0,0,69,3,34,3,35,3,128,164,3,198,0,0,88,3,198,0,0,89,3
	.byte 198,0,0,90,3,49,3,50,3,194,0,1,206,3,193,0,12,232,3,193,0,19,131,3,196,0,0,77,3,85,3,194
	.byte 0,1,202,7,15,109,111,110,111,95,100,111,109,97,105,110,95,103,101,116,0,3,196,0,0,64,7,31,109,111,110,111
	.byte 95,99,108,97,115,115,95,115,116,97,116,105,99,95,102,105,101,108,100,95,97,100,100,114,101,115,115,0,3,195,0,4
	.byte 14,3,195,0,2,16,3,64,3,65,7,27,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102
	.byte 114,101,101,95,98,111,120,0,3,193,0,19,136,3,193,0,14,68,3,193,0,14,69,3,198,0,0,109,3,198,0,0
	.byte 110,3,198,0,0,111,3,76,3,77,3,194,0,1,190,3,87,3,194,0,1,47,3,195,0,4,229,3,95,3,98,3
	.byte 195,0,4,226,3,196,0,1,125,3,99,3,100,3,196,0,1,159,3,196,0,1,160,3,61,3,255,254,0,0,0,0
	.byte 255,43,0,0,5,3,255,254,0,0,0,0,255,43,0,0,6,3,128,140,3,66,3,255,254,0,0,0,0,255,43,0
	.byte 0,7,3,198,0,0,8,3,255,254,0,0,0,0,255,43,0,0,8,3,255,254,0,0,0,0,255,43,0,0,9,3
	.byte 255,254,0,0,0,0,255,43,0,0,10,3,255,254,0,0,0,0,255,43,0,0,11,7,13,109,111,110,111,95,108,100
	.byte 118,105,114,116,102,110,0,3,255,254,0,0,0,0,202,0,0,103,3,255,254,0,0,0,0,255,43,0,0,12,3,255
	.byte 254,0,0,0,0,202,0,0,105,3,255,254,0,0,0,0,255,43,0,0,13,3,255,254,0,0,0,0,202,0,0,106
	.byte 3,255,254,0,0,0,0,255,43,0,0,14,3,199,0,0,1,3,255,254,0,0,0,0,255,43,0,0,15,3,255,254
	.byte 0,0,0,0,255,43,0,0,16,3,255,254,0,0,0,0,202,0,0,113,3,198,0,0,47,3,198,0,0,48,3,198
	.byte 0,0,49,3,128,156,3,128,157,3,194,0,1,43,3,255,254,0,0,0,0,255,43,0,0,18,3,196,0,1,123,3
	.byte 255,254,0,0,0,0,255,43,0,0,17,3,196,0,0,63,3,196,0,1,117,3,128,165,7,35,109,111,110,111,95,116
	.byte 104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105,110,116,0,3,255
	.byte 253,0,0,0,1,11,0,198,0,0,18,0,1,7,130,102,255,253,0,0,0,1,11,0,198,0,0,17,0,1,7,130
	.byte 102,35,137,176,140,17,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,130,102,35,137,176,192,0,92,41,255,253
	.byte 0,0,0,1,11,0,198,0,0,17,0,1,7,130,102,0,4,2,113,2,1,7,130,102,3,255,253,0,0,0,7,137
	.byte 240,2,198,0,1,200,1,7,130,102,0,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,130,102,35,138,11,150
	.byte 5,7,137,240,35,138,11,192,0,92,41,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,130,102,0,255,253,0
	.byte 0,0,1,11,0,198,0,0,19,0,1,7,130,102,35,138,61,192,0,92,41,255,253,0,0,0,1,11,0,198,0,0
	.byte 19,0,1,7,130,102,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,17,0,0,17,255,253,0,0,0,1,11,0,198,0,0,17,0,1,7,134,59,0,0,17
	.byte 0,0,17,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,134,136,0,0,5,30,0,1,255,255,255,255,255,19
	.byte 17,0,0,17,255,253,0,0,0,1,11,0,198,0,0,19,0,1,7,138,182,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,6,0,1,2,0,130,48,28,130,4,130,8,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0
	.byte 0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0
	.byte 16,0,0,3,32,0,1,11,0,17,255,253,0,0,0,1,11,0,198,0,0,17,0,1,7,130,102,1,0,1,1,0
	.byte 3,62,0,1,11,4,17,255,253,0,0,0,1,11,0,198,0,0,18,0,1,7,130,102,1,0,1,1,0,3,86,0
	.byte 1,11,4,17,255,253,0,0,0,1,11,0,198,0,0,19,0,1,7,130,102,1,0,1,1,0,16,0,0,16,0,0
	.byte 16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,0,128,144,8,0,0,1,0,128,144,8,0,0
	.byte 1,0,128,144,8,0,0,1,23,128,144,12,0,0,4,193,0,16,157,193,0,16,172,193,0,18,174,193,0,16,170,193
	.byte 0,16,156,193,0,16,128,193,0,16,129,193,0,16,130,193,0,16,131,193,0,16,132,193,0,16,133,193,0,16,134,193
	.byte 0,16,135,193,0,16,136,193,0,16,137,193,0,16,138,193,0,16,139,193,0,16,158,193,0,16,140,193,0,16,141,193
	.byte 0,16,142,193,0,16,143,193,0,16,160,23,128,144,12,0,0,4,193,0,16,157,193,0,16,172,193,0,18,174,193,0
	.byte 16,170,193,0,16,156,193,0,16,128,193,0,16,129,193,0,16,130,193,0,16,131,193,0,16,132,193,0,16,133,193,0
	.byte 16,134,193,0,16,135,193,0,16,136,193,0,16,137,193,0,16,138,193,0,16,139,193,0,16,158,193,0,16,140,193,0
	.byte 16,141,193,0,16,142,193,0,16,143,193,0,16,160,0,128,144,8,0,0,1,19,128,162,195,0,2,8,24,0,0,4
	.byte 195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,195,0,2,20,195,0,2,13
	.byte 195,0,2,12,195,0,2,7,195,0,2,6,4,5,6,195,0,6,168,195,0,6,167,7,9,10,6,128,144,8,0,0
	.byte 1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,12,13,0,128,144,8,0,0,1,4,128,144,8,0,0
	.byte 1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,5,128,128,12,0,0,4,193,0,18,178,193,0,13,174
	.byte 193,0,18,174,193,0,13,181,25,4,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172
	.byte 10,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,198,0,0,36,198,0,0,35,198
	.byte 0,0,34,198,0,0,33,29,198,0,0,31,0,128,144,8,0,0,1,58,128,162,195,0,2,8,80,0,0,4,195,0
	.byte 2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,198,0,0,75,195,0,2,13,195,0
	.byte 2,12,195,0,2,7,195,0,7,155,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6,26,195,0,6,40,195,0
	.byte 6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,198,0,0,73,198,0
	.byte 0,70,198,0,0,71,198,0,0,72,198,0,0,74,195,0,6,27,195,0,6,23,195,0,6,22,195,0,6,21,195,0
	.byte 6,20,195,0,6,19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,198,0,0,76,198,0,0,77,198,0
	.byte 0,78,198,0,0,79,198,0,0,80,198,0,0,81,198,0,0,82,198,0,0,83,198,0,0,84,198,0,0,85,198,0
	.byte 0,86,198,0,0,87,38,39,36,37,34,35,40,41,10,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18
	.byte 174,193,0,18,172,198,0,0,36,198,0,0,35,198,0,0,34,198,0,0,33,45,44,59,128,162,195,0,2,8,84,0
	.byte 0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,198,0,0,96,195,0
	.byte 2,13,195,0,2,12,195,0,2,7,195,0,7,190,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6,26,195,0
	.byte 6,40,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6,33,198,0
	.byte 0,94,198,0,0,91,198,0,0,92,198,0,0,93,198,0,0,95,195,0,6,27,195,0,6,23,195,0,6,22,195,0
	.byte 6,21,195,0,6,20,195,0,6,19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,195,0,7,194,198,0
	.byte 0,97,198,0,0,98,198,0,0,99,198,0,0,100,198,0,0,101,198,0,0,102,198,0,0,103,198,0,0,104,198,0
	.byte 0,105,198,0,0,106,198,0,0,107,198,0,0,108,53,54,51,52,49,50,55,56,15,128,160,24,0,0,4,193,0,18
	.byte 178,193,0,18,175,193,0,18,174,193,0,18,172,194,0,1,203,194,0,1,204,255,251,0,0,0,194,0,1,205,194,0
	.byte 1,207,194,0,1,208,194,0,1,206,59,60,57,60,59,0,128,144,8,0,0,1,255,255,255,255,255,255,255,255,255,255
	.byte 7,128,144,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,69,70,71,0,128,144,8,0,0
	.byte 1,58,128,162,195,0,2,8,80,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4
	.byte 195,0,2,9,198,0,0,117,195,0,2,13,195,0,2,12,195,0,2,7,195,0,6,10,195,0,5,28,195,0,5,27
	.byte 195,0,5,26,195,0,6,26,195,0,6,40,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35
	.byte 195,0,6,34,195,0,6,33,198,0,0,115,198,0,0,112,198,0,0,113,198,0,0,114,198,0,0,116,195,0,6,27
	.byte 195,0,6,23,195,0,6,22,195,0,6,21,195,0,6,20,195,0,6,19,195,0,6,18,195,0,6,17,195,0,6,16
	.byte 195,0,6,15,198,0,0,118,198,0,0,119,198,0,0,120,198,0,0,121,198,0,0,122,198,0,0,123,198,0,0,124
	.byte 198,0,0,125,198,0,0,126,198,0,0,127,198,0,0,128,198,0,0,129,80,81,78,79,76,77,82,83,4,128,160,28
	.byte 0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,12,128,144,8,0,0,1,193,0,18,178,193,0
	.byte 18,175,193,0,18,174,193,0,18,172,86,87,88,89,89,88,87,86,24,128,224,20,4,0,4,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,96,97,101,102,102,101,97,96,107,106,105,104,103,100,99,98,94,93,92,91,25,128,224
	.byte 24,16,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,96,97,101,112,112,101,97,96,107,106,105,104
	.byte 103,114,113,110,94,93,92,91,111,24,128,224,24,4,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172
	.byte 96,97,101,121,121,101,97,96,107,106,105,104,103,123,122,120,94,93,92,91,60,128,160,28,0,0,4,193,0,18,178,193
	.byte 0,18,175,193,0,18,174,193,0,18,172,194,0,1,129,128,143,194,0,1,120,194,0,1,119,128,152,194,0,1,117,194
	.byte 0,1,116,194,0,1,115,194,0,1,114,194,0,1,113,194,0,1,112,194,0,1,111,194,0,1,110,194,0,1,109,128
	.byte 130,194,0,1,107,194,0,1,106,194,0,1,105,128,135,194,0,1,103,194,0,1,102,194,0,1,101,194,0,1,100,194
	.byte 0,1,99,194,0,1,98,194,0,1,97,194,0,1,96,194,0,1,95,194,0,1,94,194,0,1,93,194,0,1,92,194
	.byte 0,1,91,194,0,1,90,194,0,1,89,194,0,1,88,194,0,1,87,128,134,128,132,0,128,129,128,151,128,150,128,149
	.byte 128,148,128,147,128,146,128,145,128,144,128,142,128,141,128,139,128,138,128,137,128,136,128,133,128,131,62,128,162,195,0,2
	.byte 8,84,0,0,4,195,0,2,18,193,0,18,175,195,0,2,8,193,0,18,172,195,0,2,4,195,0,2,9,198,0,0
	.byte 55,195,0,2,13,195,0,2,12,195,0,2,7,195,0,4,85,195,0,5,28,195,0,5,27,195,0,5,26,195,0,6
	.byte 26,195,0,6,40,195,0,6,39,195,0,6,38,195,0,6,37,195,0,6,36,195,0,6,35,195,0,6,34,195,0,6
	.byte 33,198,0,0,53,198,0,0,50,198,0,0,51,198,0,0,52,198,0,0,54,195,0,6,27,195,0,6,23,195,0,6
	.byte 22,195,0,6,21,195,0,6,20,195,0,6,19,195,0,6,18,195,0,6,17,195,0,6,16,195,0,6,15,195,0,4
	.byte 89,195,0,4,90,195,0,4,90,195,0,4,89,198,0,0,56,198,0,0,57,198,0,0,58,198,0,0,59,198,0,0
	.byte 60,198,0,0,61,198,0,0,62,198,0,0,63,198,0,0,64,198,0,0,65,198,0,0,66,198,0,0,67,128,160,128
	.byte 161,128,158,128,159,128,156,128,157,128,162,128,163,4,128,152,8,0,0,1,193,0,18,178,193,0,18,175,193,0,18,174
	.byte 193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,16,0
	.byte 0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,4,128,160,16,0,0,4,193,0,18,178,193,0,18
	.byte 175,193,0,18,174,193,0,18,172,4,128,160,12,0,0,4,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172
	.byte 98,111,101,104,109,0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug
LTDIE_2:

	.byte 17
	.asciz "System_Object"

	.byte 8,7
	.asciz "System_Object"

LDIFF_SYM3=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM3
LTDIE_2_POINTER:

	.byte 13
LDIFF_SYM4=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM4
LTDIE_2_REFERENCE:

	.byte 14
LDIFF_SYM5=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM5
LTDIE_4:

	.byte 17
	.asciz "System_Collections_Generic_IEqualityComparer`1"

	.byte 8,7
	.asciz "System_Collections_Generic_IEqualityComparer`1"

LDIFF_SYM6=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM6
LTDIE_4_POINTER:

	.byte 13
LDIFF_SYM7=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM7
LTDIE_4_REFERENCE:

	.byte 14
LDIFF_SYM8=LTDIE_4 - Ldebug_info_start
	.long LDIFF_SYM8
LTDIE_6:

	.byte 5
	.asciz "System_ValueType"

	.byte 8,16
LDIFF_SYM9=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM9
	.byte 2,35,0,0,7
	.asciz "System_ValueType"

LDIFF_SYM10=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM10
LTDIE_6_POINTER:

	.byte 13
LDIFF_SYM11=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM11
LTDIE_6_REFERENCE:

	.byte 14
LDIFF_SYM12=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM12
LTDIE_5:

	.byte 5
	.asciz "System_Int32"

	.byte 12,16
LDIFF_SYM13=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM13
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM14=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM14
	.byte 2,35,8,0,7
	.asciz "System_Int32"

LDIFF_SYM15=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM15
LTDIE_5_POINTER:

	.byte 13
LDIFF_SYM16=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM16
LTDIE_5_REFERENCE:

	.byte 14
LDIFF_SYM17=LTDIE_5 - Ldebug_info_start
	.long LDIFF_SYM17
LTDIE_3:

	.byte 5
	.asciz "System_Collections_Generic_Dictionary`2"

	.byte 48,16
LDIFF_SYM18=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM18
	.byte 2,35,0,6
	.asciz "table"

LDIFF_SYM19=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM19
	.byte 2,35,8,6
	.asciz "linkSlots"

LDIFF_SYM20=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM20
	.byte 2,35,12,6
	.asciz "keySlots"

LDIFF_SYM21=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM21
	.byte 2,35,16,6
	.asciz "valueSlots"

LDIFF_SYM22=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM22
	.byte 2,35,20,6
	.asciz "hcp"

LDIFF_SYM23=LTDIE_4_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM23
	.byte 2,35,24,6
	.asciz "touchedSlots"

LDIFF_SYM24=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM24
	.byte 2,35,28,6
	.asciz "emptySlot"

LDIFF_SYM25=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM25
	.byte 2,35,32,6
	.asciz "count"

LDIFF_SYM26=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM26
	.byte 2,35,36,6
	.asciz "threshold"

LDIFF_SYM27=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM27
	.byte 2,35,40,6
	.asciz "generation"

LDIFF_SYM28=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM28
	.byte 2,35,44,0,7
	.asciz "System_Collections_Generic_Dictionary`2"

LDIFF_SYM29=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM29
LTDIE_3_POINTER:

	.byte 13
LDIFF_SYM30=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM30
LTDIE_3_REFERENCE:

	.byte 14
LDIFF_SYM31=LTDIE_3 - Ldebug_info_start
	.long LDIFF_SYM31
LTDIE_7:

	.byte 5
	.asciz "System_Collections_Generic_List`1"

	.byte 20,16
LDIFF_SYM32=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM32
	.byte 2,35,0,6
	.asciz "_items"

LDIFF_SYM33=LDIE_SZARRAY - Ldebug_info_start
	.long LDIFF_SYM33
	.byte 2,35,8,6
	.asciz "_size"

LDIFF_SYM34=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM34
	.byte 2,35,12,6
	.asciz "_version"

LDIFF_SYM35=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM35
	.byte 2,35,16,0,7
	.asciz "System_Collections_Generic_List`1"

LDIFF_SYM36=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM36
LTDIE_7_POINTER:

	.byte 13
LDIFF_SYM37=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM37
LTDIE_7_REFERENCE:

	.byte 14
LDIFF_SYM38=LTDIE_7 - Ldebug_info_start
	.long LDIFF_SYM38
LTDIE_8:

	.byte 17
	.asciz "Cirrious_MvvmCross_Views_IMvxViewFinder"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Views_IMvxViewFinder"

LDIFF_SYM39=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM39
LTDIE_8_POINTER:

	.byte 13
LDIFF_SYM40=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM40
LTDIE_8_REFERENCE:

	.byte 14
LDIFF_SYM41=LTDIE_8 - Ldebug_info_start
	.long LDIFF_SYM41
LTDIE_1:

	.byte 5
	.asciz "Cirrious_MvvmCross_Views_MvxViewsContainer"

	.byte 20,16
LDIFF_SYM42=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM42
	.byte 2,35,0,6
	.asciz "_bindingMap"

LDIFF_SYM43=LTDIE_3_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM43
	.byte 2,35,8,6
	.asciz "_secondaryViewFinders"

LDIFF_SYM44=LTDIE_7_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM44
	.byte 2,35,12,6
	.asciz "_lastResortViewFinder"

LDIFF_SYM45=LTDIE_8_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM45
	.byte 2,35,16,0,7
	.asciz "Cirrious_MvvmCross_Views_MvxViewsContainer"

LDIFF_SYM46=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM46
LTDIE_1_POINTER:

	.byte 13
LDIFF_SYM47=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM47
LTDIE_1_REFERENCE:

	.byte 14
LDIFF_SYM48=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM48
LTDIE_11:

	.byte 5
	.asciz "System_Reflection_MemberInfo"

	.byte 8,16
LDIFF_SYM49=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM49
	.byte 2,35,0,0,7
	.asciz "System_Reflection_MemberInfo"

LDIFF_SYM50=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM50
LTDIE_11_POINTER:

	.byte 13
LDIFF_SYM51=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM51
LTDIE_11_REFERENCE:

	.byte 14
LDIFF_SYM52=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM52
LTDIE_10:

	.byte 5
	.asciz "System_Type"

	.byte 12,16
LDIFF_SYM53=LTDIE_11 - Ldebug_info_start
	.long LDIFF_SYM53
	.byte 2,35,0,6
	.asciz "_impl"

LDIFF_SYM54=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM54
	.byte 2,35,8,0,7
	.asciz "System_Type"

LDIFF_SYM55=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM55
LTDIE_10_POINTER:

	.byte 13
LDIFF_SYM56=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM56
LTDIE_10_REFERENCE:

	.byte 14
LDIFF_SYM57=LTDIE_10 - Ldebug_info_start
	.long LDIFF_SYM57
LTDIE_12:

	.byte 17
	.asciz "System_Collections_Generic_IDictionary`2"

	.byte 8,7
	.asciz "System_Collections_Generic_IDictionary`2"

LDIFF_SYM58=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM58
LTDIE_12_POINTER:

	.byte 13
LDIFF_SYM59=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM59
LTDIE_12_REFERENCE:

	.byte 14
LDIFF_SYM60=LTDIE_12 - Ldebug_info_start
	.long LDIFF_SYM60
LTDIE_14:

	.byte 8
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

	.byte 4
LDIFF_SYM61=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM61
	.byte 9
	.asciz "Unknown"

	.byte 0,9
	.asciz "UserAction"

	.byte 1,9
	.asciz "Bookmark"

	.byte 2,9
	.asciz "AutomatedService"

	.byte 3,9
	.asciz "Other"

	.byte 4,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedByType"

LDIFF_SYM62=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM62
LTDIE_14_POINTER:

	.byte 13
LDIFF_SYM63=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM63
LTDIE_14_REFERENCE:

	.byte 14
LDIFF_SYM64=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM64
LTDIE_13:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

	.byte 16,16
LDIFF_SYM65=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM65
	.byte 2,35,0,6
	.asciz "<Type>k__BackingField"

LDIFF_SYM66=LTDIE_14 - Ldebug_info_start
	.long LDIFF_SYM66
	.byte 2,35,12,6
	.asciz "<AdditionalInfo>k__BackingField"

LDIFF_SYM67=LDIE_STRING - Ldebug_info_start
	.long LDIFF_SYM67
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxRequestedBy"

LDIFF_SYM68=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM68
LTDIE_13_POINTER:

	.byte 13
LDIFF_SYM69=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM69
LTDIE_13_REFERENCE:

	.byte 14
LDIFF_SYM70=LTDIE_13 - Ldebug_info_start
	.long LDIFF_SYM70
LTDIE_9:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

	.byte 24,16
LDIFF_SYM71=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM71
	.byte 2,35,0,6
	.asciz "<ViewModelType>k__BackingField"

LDIFF_SYM72=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM72
	.byte 2,35,8,6
	.asciz "<ParameterValues>k__BackingField"

LDIFF_SYM73=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM73
	.byte 2,35,12,6
	.asciz "<PresentationValues>k__BackingField"

LDIFF_SYM74=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM74
	.byte 2,35,16,6
	.asciz "<RequestedBy>k__BackingField"

LDIFF_SYM75=LTDIE_13_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM75
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest"

LDIFF_SYM76=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM76
LTDIE_9_POINTER:

	.byte 13
LDIFF_SYM77=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM77
LTDIE_9_REFERENCE:

	.byte 14
LDIFF_SYM78=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM78
LTDIE_0:

	.byte 5
	.asciz "Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer"

	.byte 24,16
LDIFF_SYM79=LTDIE_1 - Ldebug_info_start
	.long LDIFF_SYM79
	.byte 2,35,0,6
	.asciz "<CurrentRequest>k__BackingField"

LDIFF_SYM80=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM80
	.byte 2,35,20,0,7
	.asciz "Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer"

LDIFF_SYM81=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM81
LTDIE_0_POINTER:

	.byte 13
LDIFF_SYM82=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM82
LTDIE_0_REFERENCE:

	.byte 14
LDIFF_SYM83=LTDIE_0 - Ldebug_info_start
	.long LDIFF_SYM83
LTDIE_15:

	.byte 17
	.asciz "Cirrious_MvvmCross_Touch_Views_IMvxTouchView"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Touch_Views_IMvxTouchView"

LDIFF_SYM84=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM84
LTDIE_15_POINTER:

	.byte 13
LDIFF_SYM85=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM85
LTDIE_15_REFERENCE:

	.byte 14
LDIFF_SYM86=LTDIE_15 - Ldebug_info_start
	.long LDIFF_SYM86
LTDIE_16:

	.byte 5
	.asciz "System_Boolean"

	.byte 9,16
LDIFF_SYM87=LTDIE_6 - Ldebug_info_start
	.long LDIFF_SYM87
	.byte 2,35,0,6
	.asciz "m_value"

LDIFF_SYM88=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM88
	.byte 2,35,8,0,7
	.asciz "System_Boolean"

LDIFF_SYM89=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM89
LTDIE_16_POINTER:

	.byte 13
LDIFF_SYM90=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM90
LTDIE_16_REFERENCE:

	.byte 14
LDIFF_SYM91=LTDIE_16 - Ldebug_info_start
	.long LDIFF_SYM91
	.byte 2
	.asciz "Cirrious.MvvmCross.Touch.Views.MvxTouchViewsContainer:CreateView"
	.long _Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0
	.long Lme_3a

	.byte 2,118,16,3
	.asciz "this"

LDIFF_SYM92=LTDIE_0_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM92
	.byte 2,123,40,3
	.asciz "request"

LDIFF_SYM93=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM93
	.byte 1,90,11
	.asciz "V_0"

LDIFF_SYM94=LTDIE_10_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM94
	.byte 1,86,11
	.asciz "V_1"

LDIFF_SYM95=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM95
	.byte 1,85,11
	.asciz "V_2"

LDIFF_SYM96=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM96
	.byte 1,84,11
	.asciz "V_3"

LDIFF_SYM97=LDIE_BOOLEAN - Ldebug_info_start
	.long LDIFF_SYM97
	.byte 2,123,0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM98=Lfde0_end - Lfde0_start
	.long LDIFF_SYM98
Lfde0_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0

LDIFF_SYM99=Lme_3a - _Cirrious_MvvmCross_Touch_Views_MvxTouchViewsContainer_CreateView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest_0
	.long LDIFF_SYM99
	.byte 12,13,0,72,14,8,135,2,68,14,32,132,8,133,7,134,6,136,5,138,4,139,3,142,1,68,14,88,68,13,11
	.align 2
Lfde0_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_17:

	.byte 17
	.asciz "Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView"

	.byte 8,7
	.asciz "Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView"

LDIFF_SYM100=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM100
LTDIE_17_POINTER:

	.byte 13
LDIFF_SYM101=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM101
LTDIE_17_REFERENCE:

	.byte 14
LDIFF_SYM102=LTDIE_17 - Ldebug_info_start
	.long LDIFF_SYM102
	.byte 2
	.asciz "Cirrious.MvvmCross.Touch.Views.MvxCanCreateTouchViewExtensionMethods:CreateViewControllerFor<!!0>"
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	.long Lme_ae

	.byte 2,118,16,3
	.asciz "view"

LDIFF_SYM103=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM103
	.byte 1,86,3
	.asciz "parameterObject"

LDIFF_SYM104=LDIE_OBJECT - Ldebug_info_start
	.long LDIFF_SYM104
	.byte 2,123,8,11
	.asciz "V_0"

LDIFF_SYM105=LTDIE_15_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM105
	.byte 1,85,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM106=Lfde1_end - Lfde1_start
	.long LDIFF_SYM106
Lfde1_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object

LDIFF_SYM107=Lme_ae - _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_object
	.long LDIFF_SYM107
	.byte 12,13,0,72,14,8,135,2,68,14,28,132,7,133,6,134,5,136,4,139,3,142,1,68,14,48,68,13,11
	.align 2
Lfde1_end:

.section __DWARF, __debug_info,regular,debug
LTDIE_18:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxBundle"

	.byte 12,16
LDIFF_SYM108=LTDIE_2 - Ldebug_info_start
	.long LDIFF_SYM108
	.byte 2,35,0,6
	.asciz "<Data>k__BackingField"

LDIFF_SYM109=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM109
	.byte 2,35,8,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxBundle"

LDIFF_SYM110=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM110
LTDIE_18_POINTER:

	.byte 13
LDIFF_SYM111=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM111
LTDIE_18_REFERENCE:

	.byte 14
LDIFF_SYM112=LTDIE_18 - Ldebug_info_start
	.long LDIFF_SYM112
LTDIE_19:

	.byte 5
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest`1"

	.byte 24,16
LDIFF_SYM113=LTDIE_9 - Ldebug_info_start
	.long LDIFF_SYM113
	.byte 2,35,0,0,7
	.asciz "Cirrious_MvvmCross_ViewModels_MvxViewModelRequest`1"

LDIFF_SYM114=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM114
LTDIE_19_POINTER:

	.byte 13
LDIFF_SYM115=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM115
LTDIE_19_REFERENCE:

	.byte 14
LDIFF_SYM116=LTDIE_19 - Ldebug_info_start
	.long LDIFF_SYM116
	.byte 2
	.asciz "Cirrious.MvvmCross.Touch.Views.MvxCanCreateTouchViewExtensionMethods:CreateViewControllerFor<!!0>"
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	.long Lme_af

	.byte 2,118,16,3
	.asciz "view"

LDIFF_SYM117=LTDIE_17_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM117
	.byte 2,123,12,3
	.asciz "parameterValues"

LDIFF_SYM118=LTDIE_12_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM118
	.byte 2,123,16,11
	.asciz "V_0"

LDIFF_SYM119=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM119
	.byte 0,11
	.asciz "V_1"

LDIFF_SYM120=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM120
	.byte 0,11
	.asciz "V_2"

LDIFF_SYM121=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM121
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM122=Lfde2_end - Lfde2_start
	.long LDIFF_SYM122
Lfde2_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string

LDIFF_SYM123=Lme_af - _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_System_Collections_Generic_IDictionary_2_string_string
	.long LDIFF_SYM123
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,56,68,13,11
	.align 2
Lfde2_end:

.section __DWARF, __debug_info,regular,debug

	.byte 2
	.asciz "Cirrious.MvvmCross.Touch.Views.MvxCanCreateTouchViewExtensionMethods:CreateViewControllerFor<!!0>"
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.long Lme_b0

	.byte 2,118,16,3
	.asciz "view"

LDIFF_SYM124=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM124
	.byte 0,3
	.asciz "request"

LDIFF_SYM125=LTDIE_9_REFERENCE - Ldebug_info_start
	.long LDIFF_SYM125
	.byte 2,123,16,11
	.asciz "V_0"

LDIFF_SYM126=LDIE_I4 - Ldebug_info_start
	.long LDIFF_SYM126
	.byte 0,0

.section __DWARF, __debug_frame,regular,debug

LDIFF_SYM127=Lfde3_end - Lfde3_start
	.long LDIFF_SYM127
Lfde3_start:

	.long 0
	.align 2
	.long _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest

LDIFF_SYM128=Lme_b0 - _Cirrious_MvvmCross_Touch_Views_MvxCanCreateTouchViewExtensionMethods_CreateViewControllerFor___0_Cirrious_MvvmCross_Touch_Views_IMvxCanCreateTouchView_Cirrious_MvvmCross_ViewModels_MvxViewModelRequest
	.long LDIFF_SYM128
	.byte 12,13,0,72,14,8,135,2,68,14,16,136,4,139,3,142,1,68,14,40,68,13,11
	.align 2
Lfde3_end:

.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
