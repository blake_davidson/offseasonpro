	.section	__TEXT,__text,regular,pure_instructions
	.section	__TEXT,__textcoal_nt,coalesced,pure_instructions
	.section	__TEXT,__const_coal,coalesced
	.section	__TEXT,__picsymbolstub4,symbol_stubs,none,16
	.section	__TEXT,__StaticInit,regular,pure_instructions
	.syntax unified
	.section	__TEXT,__text,regular,pure_instructions
	.align	2
_mono_aot_personality:
Leh_func_begin0:
	bx	lr
Leh_func_end0:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
	.align	2
_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded:
Leh_func_begin1:
	push	{r4, r7, lr}
Ltmp0:
	add	r7, sp, #4
Ltmp1:
	push	{r8}
Ltmp2:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC1_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC1_0+8))
LPC1_0:
	add	r4, pc, r4
	ldr	r0, [r4, #16]
	mov	r8, r0
	bl	_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm
	ldr	r1, [r4, #24]
	ldr	r2, [r0]
	sub	r2, r2, #40
	mov	r8, r1
	ldr	r2, [r2]
	blx	r2
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end1:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__ctor
	.align	2
_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__ctor:
Leh_func_begin2:
	bx	lr
Leh_func_end2:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__cctor
	.align	2
_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__cctor:
Leh_func_begin3:
	push	{r4, r7, lr}
Ltmp3:
	add	r7, sp, #4
Ltmp4:
Ltmp5:
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC3_0+8))
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC3_0+8))
LPC3_0:
	add	r4, pc, r4
	ldr	r0, [r4, #28]
	bl	_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm
	ldr	r1, [r4, #32]
	str	r0, [r1]
	pop	{r4, r7, pc}
Leh_func_end3:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream:
Leh_func_begin4:
	push	{r4, r5, r7, lr}
Ltmp6:
	add	r7, sp, #8
Ltmp7:
Ltmp8:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC4_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC4_0+8))
LPC4_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB4_2
	bl	_p_3_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB4_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB4_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB4_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB4_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB4_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end4:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current:
Leh_func_begin5:
	push	{r4, r5, r7, lr}
Ltmp9:
	add	r7, sp, #8
Ltmp10:
	push	{r8}
Ltmp11:
	movw	r5, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC5_0+8))
	movt	r5, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC5_0+8))
LPC5_0:
	add	r5, pc, r5
	ldr	r1, [r5, #40]
	mov	r8, r1
	bl	_p_4_plt_System_Array_InternalEnumerator_1_byte_get_Current_llvm
	mov	r4, r0
	ldr	r0, [r5, #44]
	bl	_p_5_plt__jit_icall_mono_object_new_ptrfree_box_llvm
	strb	r4, [r0, #8]
	pop	{r8}
	pop	{r4, r5, r7, pc}
Leh_func_end5:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array:
Leh_func_begin6:
	mov	r2, r1
	mvn	r3, #1
	strd	r2, r3, [r0]
	bx	lr
Leh_func_end6:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current:
Leh_func_begin7:
	push	{r7, lr}
Ltmp12:
	mov	r7, sp
Ltmp13:
	push	{r8}
Ltmp14:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	beq	LBB7_3
	ldr	r1, [r0, #4]
	cmn	r1, #1
	beq	LBB7_4
	ldr	r2, [r0]
	ldr	r1, [r0]
	ldr	r1, [r1, #12]
	ldr	r9, [r0, #4]
	movw	r3, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC7_0+8))
	movt	r3, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC7_0+8))
LPC7_0:
	add	r3, pc, r3
	ldr	r3, [r3, #48]
	ldr	r0, [r2]
	sub	r1, r1, #1
	mov	r0, r2
	sub	r1, r1, r9
	mov	r8, r3
	bl	_p_6_plt_System_Array_InternalArray__get_Item_byte_int_llvm
	pop	{r8}
	pop	{r7, pc}
LBB7_3:
	movw	r0, #47632
	b	LBB7_5
LBB7_4:
	movw	r0, #47718
LBB7_5:
	bl	_p_7_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #606
	movt	r0, #512
	bl	_p_8_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_9_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end7:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose:
Leh_func_begin8:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end8:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext:
Leh_func_begin9:
	ldr	r1, [r0, #4]
	cmn	r1, #2
	ldreq	r1, [r0]
	ldreq	r1, [r1, #12]
	streq	r1, [r0, #4]
	mov	r1, #0
	ldr	r2, [r0, #4]
	cmn	r2, #1
	beq	LBB9_2
	ldr	r1, [r0, #4]
	cmp	r1, #0
	sub	r2, r1, #1
	movne	r1, #1
	str	r2, [r0, #4]
LBB9_2:
	mov	r0, r1
	bx	lr
Leh_func_end9:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset:
Leh_func_begin10:
	mvn	r1, #1
	str	r1, [r0, #4]
	bx	lr
Leh_func_end10:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__IEnumerable_GetEnumerator_byte:
Leh_func_begin11:
	push	{r4, r7, lr}
Ltmp15:
	add	r7, sp, #4
Ltmp16:
	push	{r8}
Ltmp17:
	sub	sp, sp, #16
	bic	sp, sp, #7
	movw	r4, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC11_0+8))
	mov	r1, r0
	mov	r0, #0
	movt	r4, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC11_0+8))
	str	r0, [sp, #4]
	str	r0, [sp]
	mov	r0, sp
LPC11_0:
	add	r4, pc, r4
	ldr	r2, [r4, #40]
	mov	r8, r2
	bl	_p_10_plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array_llvm
	ldm	sp, {r0, r1}
	str	r1, [sp, #12]
	str	r0, [sp, #8]
	ldr	r0, [r4, #40]
	bl	_p_11_plt__jit_icall_mono_object_new_fast_llvm
	ldr	r1, [sp, #8]
	str	r1, [r0, #8]
	ldr	r1, [sp, #12]
	str	r1, [r0, #12]
	sub	sp, r7, #8
	pop	{r8}
	pop	{r4, r7, pc}
Leh_func_end11:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.align	2
_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream:
Leh_func_begin12:
	push	{r4, r5, r7, lr}
Ltmp18:
	add	r7, sp, #8
Ltmp19:
Ltmp20:
	mov	r5, r0
	movw	r0, :lower16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC12_0+8))
	mov	r4, r1
	movt	r0, :upper16:(_mono_aot_Cirrious_MvvmCross_Plugins_File_got-(LPC12_0+8))
LPC12_0:
	add	r0, pc, r0
	ldr	r0, [r0, #36]
	ldr	r0, [r0]
	cmp	r0, #0
	beq	LBB12_2
	bl	_p_3_plt__jit_icall_mono_thread_interruption_checkpoint_llvm
LBB12_2:
	ldr	r0, [r5, #44]
	cmp	r0, #0
	beq	LBB12_4
	ldr	r2, [r0, #12]
	mov	r1, r4
	blx	r2
LBB12_4:
	ldr	r0, [r5, #16]
	ldr	r2, [r5, #8]
	cmp	r0, #0
	beq	LBB12_6
	mov	r1, r4
	blx	r2
	pop	{r4, r5, r7, pc}
LBB12_6:
	mov	r0, r4
	blx	r2
	pop	{r4, r5, r7, pc}
Leh_func_end12:

	.private_extern	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__get_Item_byte_int
	.align	2
_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__get_Item_byte_int:
Leh_func_begin13:
	push	{r7, lr}
Ltmp21:
	mov	r7, sp
Ltmp22:
Ltmp23:
	ldr	r2, [r0, #12]
	cmp	r2, r1
	addhi	r0, r1, r0
	ldrbhi	r0, [r0, #16]
	pophi	{r7, pc}
	movw	r0, #11703
	bl	_p_7_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm
	mov	r1, r0
	movw	r0, #520
	movt	r0, #512
	bl	_p_8_plt__jit_icall_mono_create_corlib_exception_1_llvm
	bl	_p_9_plt__jit_icall_mono_arch_throw_exception_llvm
Leh_func_end13:

.zerofill __DATA,__bss,_mono_aot_Cirrious_MvvmCross_Plugins_File_got,104,4
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__ctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__cctor
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	.no_dead_strip	_Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__get_Item_byte_int
	.no_dead_strip	_mono_aot_Cirrious_MvvmCross_Plugins_File_got
	.align	4
mono_eh_frame:
	.byte	3
	.byte	0
	.align	2
	.long	14
	.long	-1
Lset0 = Lmono_eh_func_begin0-mono_eh_frame
	.long	Lset0
	.long	15
Lset1 = Lmono_eh_func_begin1-mono_eh_frame
	.long	Lset1
	.long	16
Lset2 = Lmono_eh_func_begin2-mono_eh_frame
	.long	Lset2
	.long	17
Lset3 = Lmono_eh_func_begin3-mono_eh_frame
	.long	Lset3
	.long	23
Lset4 = Lmono_eh_func_begin4-mono_eh_frame
	.long	Lset4
	.long	25
Lset5 = Lmono_eh_func_begin5-mono_eh_frame
	.long	Lset5
	.long	26
Lset6 = Lmono_eh_func_begin6-mono_eh_frame
	.long	Lset6
	.long	27
Lset7 = Lmono_eh_func_begin7-mono_eh_frame
	.long	Lset7
	.long	28
Lset8 = Lmono_eh_func_begin8-mono_eh_frame
	.long	Lset8
	.long	29
Lset9 = Lmono_eh_func_begin9-mono_eh_frame
	.long	Lset9
	.long	30
Lset10 = Lmono_eh_func_begin10-mono_eh_frame
	.long	Lset10
	.long	31
Lset11 = Lmono_eh_func_begin11-mono_eh_frame
	.long	Lset11
	.long	32
Lset12 = Lmono_eh_func_begin12-mono_eh_frame
	.long	Lset12
	.long	33
Lset13 = Lmono_eh_func_begin13-mono_eh_frame
	.long	Lset13
Lset14 = Leh_func_end13-Leh_func_begin13
	.long	Lset14
Lset15 = Lmono_eh_frame_end-mono_eh_frame
	.long	Lset15
	.byte	1
	.byte	124
	.byte	14
	.byte	255
	.byte	12
	.byte	13
	.byte	0

Lmono_eh_func_begin0:
	.byte	0

Lmono_eh_func_begin1:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin2:
	.byte	0

Lmono_eh_func_begin3:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin4:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin5:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	5

Lmono_eh_func_begin6:
	.byte	0

Lmono_eh_func_begin7:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	3

Lmono_eh_func_begin8:
	.byte	0

Lmono_eh_func_begin9:
	.byte	0

Lmono_eh_func_begin10:
	.byte	0

Lmono_eh_func_begin11:
	.byte	0
	.byte	14
	.byte	12
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	132
	.byte	3
	.byte	12
	.byte	7
	.byte	8
	.byte	136
	.byte	4

Lmono_eh_func_begin12:
	.byte	0
	.byte	14
	.byte	16
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	133
	.byte	3
	.byte	132
	.byte	4
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_func_begin13:
	.byte	0
	.byte	14
	.byte	8
	.byte	142
	.byte	1
	.byte	135
	.byte	2
	.byte	12
	.byte	7
	.byte	8

Lmono_eh_frame_end:

.subsections_via_symbols
.subsections_via_symbols
.section __DWARF, __debug_abbrev,regular,debug

	.byte 1,17,1,37,8,3,8,27,8,19,11,17,1,18,1,16,6,0,0,2,46,1,3,8,17,1,18,1,64,10,0,0
	.byte 3,5,0,3,8,73,19,2,10,0,0,15,5,0,3,8,73,19,2,6,0,0,4,36,0,11,11,62,11,3,8,0
	.byte 0,5,2,1,3,8,11,15,0,0,17,2,0,3,8,11,15,0,0,6,13,0,3,8,73,19,56,10,0,0,7,22
	.byte 0,3,8,73,19,0,0,8,4,1,3,8,11,15,73,19,0,0,9,40,0,3,8,28,13,0,0,10,57,1,3,8
	.byte 0,0,11,52,0,3,8,73,19,2,10,0,0,12,52,0,3,8,73,19,2,6,0,0,13,15,0,73,19,0,0,14
	.byte 16,0,73,19,0,0,16,28,0,73,19,56,10,0,0,18,46,0,3,8,17,1,18,1,0,0,0
.section __DWARF, __debug_info,regular,debug
Ldebug_info_start:

LDIFF_SYM0=Ldebug_info_end - Ldebug_info_begin
	.long LDIFF_SYM0
Ldebug_info_begin:

	.short 2
	.long 0
	.byte 4,1
	.asciz "Mono AOT Compiler 3.2.7 (monotouch-7.0.6-branch/04f55b8 Tue Jan 21 06:20:46 EST 2014)"
	.asciz "Cirrious.MvvmCross.Plugins.File.dll"
	.asciz ""

	.byte 2,0,0,0,0,0,0,0,0
LDIFF_SYM1=Ldebug_line_start - Ldebug_line_section_start
	.long LDIFF_SYM1
LDIE_I1:

	.byte 4,1,5
	.asciz "sbyte"
LDIE_U1:

	.byte 4,1,7
	.asciz "byte"
LDIE_I2:

	.byte 4,2,5
	.asciz "short"
LDIE_U2:

	.byte 4,2,7
	.asciz "ushort"
LDIE_I4:

	.byte 4,4,5
	.asciz "int"
LDIE_U4:

	.byte 4,4,7
	.asciz "uint"
LDIE_I8:

	.byte 4,8,5
	.asciz "long"
LDIE_U8:

	.byte 4,8,7
	.asciz "ulong"
LDIE_I:

	.byte 4,4,5
	.asciz "intptr"
LDIE_U:

	.byte 4,4,7
	.asciz "uintptr"
LDIE_R4:

	.byte 4,4,4
	.asciz "float"
LDIE_R8:

	.byte 4,8,4
	.asciz "double"
LDIE_BOOLEAN:

	.byte 4,1,2
	.asciz "boolean"
LDIE_CHAR:

	.byte 4,2,8
	.asciz "char"
LDIE_STRING:

	.byte 4,4,1
	.asciz "string"
LDIE_OBJECT:

	.byte 4,4,1
	.asciz "object"
LDIE_SZARRAY:

	.byte 4,4,1
	.asciz "object"
.section __DWARF, __debug_loc,regular,debug
Ldebug_loc_start:
.section __DWARF, __debug_frame,regular,debug
	.align 3

LDIFF_SYM2=Lcie0_end - Lcie0_start
	.long LDIFF_SYM2
Lcie0_start:

	.long -1
	.byte 3
	.asciz ""

	.byte 1,124,14
	.align 2
Lcie0_end:
.text
	.align 3
	.space 16
.text
ut_25:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current

.text
ut_26:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array

.text
ut_27:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current

.text
ut_28:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose

.text
ut_29:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext

.text
ut_30:

	.byte 8,0,128,226
	b _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset

.text
	.align 3
methods_end:

	.long 0
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__ctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__cctor
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
.no_dead_strip _Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__get_Item_byte_int

.text
	.align 3
method_addresses:
	.no_dead_strip method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
	bl _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__ctor
	bl _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader__cctor
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Func_2_System_IO_Stream_bool_invoke_TResult__this___T_System_IO_Stream
	bl method_addresses
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_get_Current
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte__ctor_System_Array
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_get_Current
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_Dispose
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_MoveNext
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalEnumerator_1_byte_System_Collections_IEnumerator_Reset
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__IEnumerable_GetEnumerator_byte
	bl _Cirrious_MvvmCross_Plugins_File__wrapper_delegate_invoke_System_Action_1_System_IO_Stream_invoke_void__this___T_System_IO_Stream
	bl _Cirrious_MvvmCross_Plugins_File__System_Array_InternalArray__get_Item_byte_int
method_addresses_end:
.section __TEXT, __const
	.align 3
code_offsets:

	.long 0

.text
	.align 3
unbox_trampolines:

	.long 25

	bl ut_25

	.long 26

	bl ut_26

	.long 27

	bl ut_27

	.long 28

	bl ut_28

	.long 29

	bl ut_29

	.long 30

	bl ut_30
unbox_trampolines_end:

	.long 0
.section __TEXT, __const
	.align 3
method_info_offsets:

	.long 34,10,4,2
	.short 0, 10, 24, 38
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,6,3,255,255,255,255,246,0,0,0,0,15,255,255,255,255
	.byte 241,18,4,2,3,2,31,2,4,3
.section __TEXT, __const
	.align 3
extra_method_table:

	.long 19,0,0,0,166,30,0,0
	.long 0,0,76,25,19,94,26,20
	.long 0,0,0,130,28,0,0,0
	.long 0,184,31,0,0,0,0,0
	.long 0,0,57,23,0,224,33,0
	.long 0,0,0,214,32,0,0,0
	.long 0,0,0,0,0,0,0,0
	.long 0,0,112,27,0,148,29,0
.section __TEXT, __const
	.align 3
extra_method_info_offsets:

	.long 15,19,0,20,0,21,0,22
	.long 0,23,57,24,0,25,76,26
	.long 94,27,112,28,130,29,148,30
	.long 166,31,184,32,214,33,224
.section __TEXT, __const
	.align 3
class_name_table:

	.short 11, 1, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 2, 0, 3, 0
.section __TEXT, __const
	.align 3
got_info_offsets:

	.long 14,10,2,2
	.short 0, 11
	.byte 128,244,2,1,1,1,12,12,12,3,4,129,37,3,5,21
.section __TEXT, __const
	.align 3
ex_info_offsets:

	.long 34,10,4,2
	.short 0, 10, 25, 41
	.byte 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,130,109,3,3,255,255,255,253,141,0,0,0,0,130,118,255,255
	.byte 255,253,138,130,121,3,3,3,3,130,136,3,3,3
.section __TEXT, __const
	.align 3
unwind_info:
.section __TEXT, __const
	.align 3
class_info_offsets:

	.long 3,10,1,2
	.short 0
	.byte 130,148,7,7

.text
	.align 4
plt:
_mono_aot_Cirrious_MvvmCross_Plugins_File_plt:
_p_1_plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager_llvm:
	.no_dead_strip plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager
plt_Cirrious_CrossCore_Mvx_Resolve_Cirrious_CrossCore_Plugins_IMvxPluginManager:
_p_1:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 56,343
_p_2_plt__jit_icall_mono_object_new_ptrfree_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree
plt__jit_icall_mono_object_new_ptrfree:
_p_2:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 60,355
_p_3_plt__jit_icall_mono_thread_interruption_checkpoint_llvm:
	.no_dead_strip plt__jit_icall_mono_thread_interruption_checkpoint
plt__jit_icall_mono_thread_interruption_checkpoint:
_p_3:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 64,381
_p_4_plt_System_Array_InternalEnumerator_1_byte_get_Current_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_byte_get_Current
plt_System_Array_InternalEnumerator_1_byte_get_Current:
_p_4:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 68,419
_p_5_plt__jit_icall_mono_object_new_ptrfree_box_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_ptrfree_box
plt__jit_icall_mono_object_new_ptrfree_box:
_p_5:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 72,438
_p_6_plt_System_Array_InternalArray__get_Item_byte_int_llvm:
	.no_dead_strip plt_System_Array_InternalArray__get_Item_byte_int
plt_System_Array_InternalArray__get_Item_byte_int:
_p_6:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 76,468
_p_7_plt__jit_icall_mono_helper_ldstr_mscorlib_llvm:
	.no_dead_strip plt__jit_icall_mono_helper_ldstr_mscorlib
plt__jit_icall_mono_helper_ldstr_mscorlib:
_p_7:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 80,489
_p_8_plt__jit_icall_mono_create_corlib_exception_1_llvm:
	.no_dead_strip plt__jit_icall_mono_create_corlib_exception_1
plt__jit_icall_mono_create_corlib_exception_1:
_p_8:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 84,518
_p_9_plt__jit_icall_mono_arch_throw_exception_llvm:
	.no_dead_strip plt__jit_icall_mono_arch_throw_exception
plt__jit_icall_mono_arch_throw_exception:
_p_9:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 88,551
_p_10_plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array_llvm:
	.no_dead_strip plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array
plt_System_Array_InternalEnumerator_1_byte__ctor_System_Array:
_p_10:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 92,579
_p_11_plt__jit_icall_mono_object_new_fast_llvm:
	.no_dead_strip plt__jit_icall_mono_object_new_fast
plt__jit_icall_mono_object_new_fast:
_p_11:

	.byte 0,192,159,229,12,240,159,231
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got - . + 96,598
plt_end:
.section __TEXT, __const
	.align 3
image_table:

	.long 3
	.asciz "Cirrious.MvvmCross.Plugins.File"
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
	.asciz ""
	.asciz ""
	.align 3

	.long 0,1,0,0,0
	.asciz "mscorlib"
	.asciz "BF8E86E6-90A5-4AEA-B5B9-B1409C7B789D"
	.asciz ""
	.asciz "7cec85d7bea7798e"
	.align 3

	.long 1,2,0,5,0
	.asciz "Cirrious.CrossCore"
	.asciz "B7E40759-51D0-4888-A382-A768E0AEE4E8"
	.asciz ""
	.asciz "e16445fd9b451819"
	.align 3

	.long 1,1,0,0,0
.section __TEXT, __const
	.align 2
assembly_guid:
	.asciz "19A70509-4816-4223-A01F-3E74A82740CD"
.section __TEXT, __const
	.align 2
runtime_version:
	.asciz ""
.section __TEXT, __const
	.align 2
assembly_name:
	.asciz "Cirrious.MvvmCross.Plugins.File"
.data
	.align 3
_mono_aot_file_info:

	.long 97,0
	.align 2
	.long _mono_aot_Cirrious_MvvmCross_Plugins_File_got
	.align 2
	.long _Cirrious_MvvmCross_Plugins_File__Cirrious_MvvmCross_Plugins_File_PluginLoader_EnsureLoaded
	.align 2
	.long mono_eh_frame
	.align 2
	.long blob
	.align 2
	.long class_name_table
	.align 2
	.long class_info_offsets
	.align 2
	.long method_info_offsets
	.align 2
	.long ex_info_offsets
	.align 2
	.long code_offsets
	.align 2
	.long method_addresses
	.align 2
	.long extra_method_info_offsets
	.align 2
	.long extra_method_table
	.align 2
	.long got_info_offsets
	.align 2
	.long methods_end
	.align 2
	.long unwind_info
	.align 2
	.long mem_end
	.align 2
	.long image_table
	.align 2
	.long plt
	.align 2
	.long plt_end
	.align 2
	.long assembly_guid
	.align 2
	.long runtime_version
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long 0
	.align 2
	.long globals
	.align 2
	.long assembly_name
	.align 2
	.long unbox_trampolines
	.align 2
	.long unbox_trampolines_end

	.long 14,104,12,34,11,387000831,0,699
	.long 0,0,0,0,0,0,0,0
	.long 0,0,0,0,128,4,4,14
	.long 0,0,0,0,0
	.globl _mono_aot_module_Cirrious_MvvmCross_Plugins_File_info
	.align 2
_mono_aot_module_Cirrious_MvvmCross_Plugins_File_info:
	.align 2
	.long _mono_aot_file_info
.section __TEXT, __const
	.align 3
blob:

	.byte 0,1,3,3,6,5,4,1,3,0,1,3,2,8,7,0,1,9,0,2,11,10,0,0,0,1,12,0,0,0,0,0
	.byte 0,0,2,10,10,0,1,9,0,1,13,4,2,130,70,1,2,2,128,226,1,2,130,25,1,255,252,0,0,0,1,1
	.byte 7,43,4,2,130,11,1,1,2,130,27,1,255,253,0,0,0,7,66,1,198,0,13,120,1,2,130,27,1,0,255,253
	.byte 0,0,0,7,66,1,198,0,13,121,1,2,130,27,1,0,255,253,0,0,0,7,66,1,198,0,13,122,1,2,130,27
	.byte 1,0,255,253,0,0,0,7,66,1,198,0,13,123,1,2,130,27,1,0,255,253,0,0,0,7,66,1,198,0,13,124
	.byte 1,2,130,27,1,0,255,253,0,0,0,7,66,1,198,0,13,125,1,2,130,27,1,0,255,253,0,0,0,2,130,10
	.byte 1,1,198,0,13,43,0,1,2,130,27,1,4,2,129,252,1,1,2,128,226,1,255,252,0,0,0,1,1,7,128,204
	.byte 255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130,27,1,12,0,39,42,47,34,255,254,0,0,0,0
	.byte 255,43,0,0,1,34,255,254,0,0,0,0,255,43,0,0,2,6,255,254,0,0,0,0,255,43,0,0,2,14,1,3
	.byte 16,1,3,1,33,14,7,66,14,2,130,27,1,34,255,253,0,0,0,2,130,10,1,1,198,0,13,54,0,1,2,130
	.byte 27,1,34,255,253,0,0,0,2,130,10,1,1,198,0,13,56,0,1,2,130,27,1,3,255,254,0,0,0,0,255,43
	.byte 0,0,1,7,23,109,111,110,111,95,111,98,106,101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,0,7,35,109
	.byte 111,110,111,95,116,104,114,101,97,100,95,105,110,116,101,114,114,117,112,116,105,111,110,95,99,104,101,99,107,112,111,105
	.byte 110,116,0,3,255,253,0,0,0,7,66,1,198,0,13,122,1,2,130,27,1,0,7,27,109,111,110,111,95,111,98,106
	.byte 101,99,116,95,110,101,119,95,112,116,114,102,114,101,101,95,98,111,120,0,3,255,253,0,0,0,2,130,10,1,1,198
	.byte 0,13,54,0,1,2,130,27,1,7,26,109,111,110,111,95,104,101,108,112,101,114,95,108,100,115,116,114,95,109,115,99
	.byte 111,114,108,105,98,0,7,30,109,111,110,111,95,99,114,101,97,116,101,95,99,111,114,108,105,98,95,101,120,99,101,112
	.byte 116,105,111,110,95,49,0,7,25,109,111,110,111,95,97,114,99,104,95,116,104,114,111,119,95,101,120,99,101,112,116,105
	.byte 111,110,0,3,255,253,0,0,0,7,66,1,198,0,13,121,1,2,130,27,1,0,7,20,109,111,110,111,95,111,98,106
	.byte 101,99,116,95,110,101,119,95,102,97,115,116,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16
	.byte 0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,16,0,0,0,128,144,8,0,0,1,0,128,144,8,0
	.byte 0,1,5,128,196,18,8,4,0,1,193,0,18,178,193,0,18,175,193,0,18,174,193,0,18,172,16,98,111,101,104,109
	.byte 0
.section __TEXT, __const
	.align 3
Lglobals_hash:

	.short 11, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0, 0
	.short 0, 0, 0, 0, 0, 0, 0
.data
	.align 3
globals:
	.align 2
	.long Lglobals_hash

	.long 0,0
.section __DWARF, __debug_info,regular,debug

	.byte 0
Ldebug_info_end:
.section __DWARF, __debug_line,regular,debug
Ldebug_line_section_start:
Ldebug_line_start:

	.long Ldebug_line_end - . -4
	.short 2
	.long Ldebug_line_header_end - . -4
	.byte 1,1,251,14,13,0,1,1,1,1,0,0,0,1,0,0,1
.section __DWARF, __debug_line,regular,debug

	.byte 0
	.asciz "<unknown>"

	.byte 0,0,0,0
Ldebug_line_header_end:

	.byte 0,1,1
Ldebug_line_end:
.text
	.align 3
mem_end:
