// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import "WorkoutView.h"

@implementation WorkoutView

@synthesize lblCurrentPosition = _lblCurrentPosition;
@synthesize btnPauseResume = _btnPauseResume;
@synthesize btnReset = _btnReset;
@synthesize btnBack = _btnBack;

@end
