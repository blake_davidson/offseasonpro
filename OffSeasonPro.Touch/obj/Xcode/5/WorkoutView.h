// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>


@interface WorkoutView : UIViewController {
	UILabel *_lblCurrentPosition;
	UIButton *_btnPauseResume;
	UIButton *_btnReset;
	UIButton *_btnBack;
}

@property (nonatomic, retain) IBOutlet UILabel *lblCurrentPosition;

@property (nonatomic, retain) IBOutlet UIButton *btnPauseResume;

@property (nonatomic, retain) IBOutlet UIButton *btnReset;

@property (nonatomic, retain) IBOutlet UIButton *btnBack;

@end
