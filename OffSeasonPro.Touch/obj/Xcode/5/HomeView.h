// WARNING
// This file has been generated automatically by Xamarin Studio to
// mirror C# types. Changes in this file made by drag-connecting
// from the UI designer will be synchronized back to C#, but
// more complex manual changes may not transfer correctly.


#import <Foundation/Foundation.h>


@interface HomeView : UIViewController {
	UIButton *_btnAction;
	UIButton *_btnPlaylists;
	UIButton *_btnSettings;
}

@property (nonatomic, retain) IBOutlet UIButton *btnAction;

@property (nonatomic, retain) IBOutlet UIButton *btnPlaylists;

@property (nonatomic, retain) IBOutlet UIButton *btnSettings;

@end
