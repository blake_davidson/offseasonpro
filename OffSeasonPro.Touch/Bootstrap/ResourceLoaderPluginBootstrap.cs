using Cirrious.CrossCore.Plugins;

namespace OffSeasonPro.Touch.Bootstrap
{
    public class ResourceLoaderPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cirrious.MvvmCross.Plugins.ResourceLoader.PluginLoader, Cirrious.MvvmCross.Plugins.ResourceLoader.Touch.Plugin>
    {
    }
}