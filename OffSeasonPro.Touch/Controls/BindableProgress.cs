using System;
using MBProgressHUD;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Views;
using OffSeasonPro.Touch.Views;
using OffSeasonPro.Core.ViewModels;
<<<<<<< HEAD
=======
using System.Windows.Input;
>>>>>>> Bug Fixes

namespace OffSeasonPro.Touch
{
	public class BindableProgress
	{
		private MTMBProgressHUD _progress;
<<<<<<< HEAD
		private UIView _parent;
		private UIViewController _parentVC;
		private string _labelText;
		private bool _closeViewWhenFinished;

		public BindableProgress(UIViewController parent, string labelText, bool closeViewWhenFinished)
		{
			_parent = parent.View;
			_labelText = labelText;
			_parentVC = parent;
			_closeViewWhenFinished = closeViewWhenFinished;
=======
		private readonly UIView _parent;
		private readonly string _labelText;
		private readonly bool _closeViewWhenFinished;
		private readonly ICommand _closeCommand;

		public BindableProgress(UIViewController parent, string labelText, bool closeViewWhenFinished, ICommand closeCommand)
		{
			_parent = parent.View;
			_labelText = labelText;
			_closeViewWhenFinished = closeViewWhenFinished;
			_closeCommand = closeCommand;
>>>>>>> Bug Fixes
		}

		public bool Visible
		{
			get { return _progress != null; }
			set
			{
				if (Visible == value)
					return;

				if (value)
				{
					_progress = new MTMBProgressHUD(_parent)
					{
						LabelText = _labelText,
						RemoveFromSuperViewOnHide = true
					};
					_parent.AddSubview(_progress);
					_progress.Show(true);
				}
				else
				{
					_progress.Hide(true);
					_progress = null;

<<<<<<< HEAD
					if (_closeViewWhenFinished) {
						var vc = ((MvxViewController)_parentVC);
						var vm = (BaseViewModel)vc.ViewModel;
						vm.CloseCommand.Execute (null);
						
					}
						
=======
					if (_closeViewWhenFinished && _closeCommand != null) {
						_closeCommand.Execute (null);
					}						
>>>>>>> Bug Fixes
				}
			}
		}

	}
}

