using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using CrossUI.Touch.Dialog.Elements;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch.Controls
{
    class MyEntryElement : EntryElement
    {

        public MyEntryElement(string c, string p, string v)
            : base(c, p, v)
        {
            MaxLength = -1;
            Alignment = base.Alignment;
        }

        public MyEntryElement(string c)
            : base(c)
        {
            MaxLength = -1;
            Alignment = base.Alignment;
        }

        public MyEntryElement(string c, string p)
            : base(c, p)
        {
            MaxLength = -1;
            Alignment = base.Alignment;
        }
        public new UITextAlignment Alignment { get; set; }
        public int MaxLength { get; set; }

        static readonly NSString _cellKey = new NSString("MyEntryElement");
        protected override NSString CellKey
        {
            get { return _cellKey; }
        }

        protected override UITextField CreateTextField(RectangleF frame)
        {
            if (Alignment == UITextAlignment.Right)
                frame.Width -= 10;

            UITextField tf = base.CreateTextField(frame);

            tf.TextAlignment = Alignment;
			tf.Font = UIFont.FromName(Constants.BebasNeueBold, 26f);
            tf.Layer.ShadowOpacity = 1.0f;
            tf.Layer.ShadowRadius = 0f;
            tf.Layer.ShadowColor = UIColor.Black.CGColor;
			//tf.TextColor = UIColor.FromRGB(66,66,66);
			tf.TextColor = UIColor.LightGray;
			tf.Layer.ShadowOffset = new SizeF(0.80f, 0.80f);

            tf.ShouldChangeCharacters += delegate(UITextField textField, NSRange range, string replacementString)
            {
                if (MaxLength == -1)
                    return true;

				bool result = true;
				string filter="0123456789";

				result = (filter.Contains(replacementString) || replacementString.Equals(string.Empty));

				if ((result) && (MaxLength > 0))
				{
					result = textField.Text.Length + replacementString.Length - range.Length <= MaxLength;
				}

				return result;
            };
            return tf;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv)
        {
            var cell = base.GetCellImpl(tv);
            var color = new UIColor(255, 255, 255, 0.1f);
            cell.BackgroundColor = color;

            if (cell.TextLabel != null)
            {
				cell.TextLabel.Font = UIFont.FromName(Constants.BebasNeueBold, 32f);
				cell.TextLabel.Layer.ShadowColor = UIColor.Black.CGColor;
				cell.TextLabel.Layer.ShadowOffset = new SizeF (0.80f, 0.80f);
				cell.TextLabel.Layer.MasksToBounds = false;
                cell.TextLabel.TextColor = UIColor.FromRGB(202,202,202);
				cell.TextLabel.Layer.ShadowOpacity = .8f;
            }


            return cell;
        }
    }
}
