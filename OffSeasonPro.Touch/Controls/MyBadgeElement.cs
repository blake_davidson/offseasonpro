﻿using System.Drawing;
using CrossUI.Touch.Dialog.Elements;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch.Controls
{
    public class MyBadgeElement : BadgeElement 
    {
        public MyBadgeElement(UIImage badgeImage, string cellText) : base(badgeImage, cellText)
        {
        }

        public MyBadgeElement(UIImage badgeImage, string cellText, NSAction tapped) : base(badgeImage, cellText, tapped)
        {
        }

        protected override UITableViewCell GetCellImpl(UITableView tv)
        {
            var cell = base.GetCellImpl(tv);
            var color = new UIColor(255, 255, 255, 0.1f);
            cell.BackgroundColor = color;

            if (cell.TextLabel != null)
            {
				cell.TextLabel.Font = UIFont.FromName(Constants.BebasNeueBold, 30f);
                cell.TextLabel.ShadowColor = UIColor.Black;
                cell.TextLabel.TextColor = UIColor.FromRGB(202, 202, 202);
                cell.TextLabel.ShadowOffset = new SizeF(.80f, .80f);
            }
                

            return cell;
        }
    }
}
