using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using CrossUI.Touch.Dialog.Elements;
using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch.Controls
{
    public class MyRadioElement : RadioElement
    {
        private UIImage _image;

        public MyRadioElement(string caption, string group, UIImage image)
            : base(caption, group)
        {
            _image = image;
        }

        protected override UITableViewCell GetCellImpl(UITableView tv)
        {
            var baseCell = base.GetCell(tv);
            var cell = new UITableViewCell(UITableViewCellStyle.Subtitle, "cellId");
            cell.TextLabel.Text = Caption;
            var color = new UIColor(255, 255, 255, 0.1f);
            cell.BackgroundColor = color;

            if (cell.TextLabel != null)
            {
				cell.TextLabel.Font = UIFont.FromName(Constants.BebasNeueRegular, 20f);
                cell.TextLabel.ShadowColor = UIColor.Black;
                cell.TextLabel.TextColor = UIColor.FromRGB(202, 202, 202);
                cell.TextLabel.ShadowOffset = new SizeF(.80f, .80f);
            }


            cell.Accessory = baseCell.Accessory;
            cell.ImageView.Image = _image;
            return cell;
        }
    }
}