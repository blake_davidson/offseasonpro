// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//

using MonoTouch.Foundation;

namespace OffSeasonPro.Touch.Views
{
	[Register ("HomeView")]
	partial class HomeView
	{
		[Outlet]
		MonoTouch.UIKit.UIButton btnAction { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnPlaylists { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton btnSettings { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (btnAction != null) {
				btnAction.Dispose ();
				btnAction = null;
			}

			if (btnPlaylists != null) {
				btnPlaylists.Dispose ();
				btnPlaylists = null;
			}

			if (btnSettings != null) {
				btnSettings.Dispose ();
				btnSettings = null;
			}
		}
	}
}
