using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using MonoTouch.Foundation;
using MonoTouch.MediaPlayer;
using MonoTouch.UIKit;

namespace OffSeasonPro.Touch.Views
{
    public class MediaPickerView
        : MPMediaPickerController
    {
        public MediaPickerView()
            :base(MPMediaType.Music)
        {
			this.Title = "";
        }

        public override bool PrefersStatusBarHidden()
        {
            return true;
        }

        public override UIStatusBarStyle PreferredStatusBarStyle()
        {
            return UIStatusBarStyle.Default;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
        }
    }
}