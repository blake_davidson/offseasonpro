using System;
using System.Drawing;
using System.Linq;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.Touch.Views;
using FlipNumbers;
using GoogleAdMobAds;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.iAd;
using OffSeasonPro.Core.Messages;
using OffSeasonPro.Core.ViewModels;

namespace OffSeasonPro.Touch.Views
{
    public partial class WorkoutView
        : MvxViewController
    {
        private FlipNumbersView _flipper;

        private MvxSubscriptionToken _token;
        const string AdmobId = "ca-app-pub-5885715424694346/5363213113";

        GADInterstitial _adMobView;
        private ADBannerView _iAdBannerView;

        private bool _didInterstitialLoad = false;
        private bool _didLoadInterFailed;
        private bool _viewingAd = false;

        public WorkoutView()
            : base("WorkoutView", null)
        {
        }

        public new WorkoutViewModel ViewModel
        {
            get { return (WorkoutViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        private int _val;

        public int Val
        {
            get { return _val; }
            set { _val = value; _flipper.SetValue(_val, true); }
        }

		private bool _resizeLabel;
		public bool ResizeLabel
		{
			get { return _resizeLabel; }
			set {
				_resizeLabel = value;
				if (lblCurrentPosition.Text == "Workout Complete") {
					lblCurrentPosition.Lines = 2;
				} else {
					lblCurrentPosition.Lines = 1;
				}

				lblCurrentPosition.AdjustsFontSizeToFitWidth = true;
				lblCurrentPosition.SizeToFit ();
			}
		}
	

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            ViewModel.StopAndSave.Execute(null);

            //Allow screen to lock
            UIApplication.SharedApplication.IdleTimerDisabled = false;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            //            if (!ViewModel.Settings.IsPaid)
            //                LoadIAd();

            LoadAdMod();

            var messenger = Mvx.Resolve<IMvxMessenger>();
            _token = messenger.SubscribeOnMainThread<DisplayMessage>(OnMessageReceived);

            // ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

			_flipper = new FlipNumbersView (ViewModel.DigitCount);
			var center = View.Center;
			center.Y -= 40;
			_flipper.Center = center;

            View.AddSubview(_flipper);
            var set = this.CreateBindingSet<WorkoutView, WorkoutViewModel>();
            set.Bind(this).For(fv => fv.Val).To(vm => vm.ClockValue);
            set.Bind(lblCurrentPosition).To(vm => vm.CurrentPosition);

			set.Bind(btnPauseResume).To(vm => vm.PauseResumeWorkout);
            set.Bind(btnPauseResume).For("Title").To(vm => vm.BtnPausePlayText);
            set.Bind(btnReset).To(vm => vm.Reset);
            set.Bind(btnBack).To(vm => vm.GoHome);

			set.Bind (btnBack).For (button => button.Enabled).To (vm => vm.ButtonsEnabled);
			set.Bind (btnReset).For (button => button.Enabled).To (vm => vm.ButtonsEnabled);
			set.Bind (btnPauseResume).For (button => button.Enabled).To (vm => vm.ButtonsEnabled);
			set.Bind (this).For (fv => fv.ResizeLabel).To (vm => vm.ResizeLabel);
            set.Apply();

            if (ViewModel.Settings.UsePhotoAlbumForBgImage)
            {
                View.BackgroundColor =
                    UIColor.FromPatternImage(UIImage.FromFile(ViewModel.Settings.WorkoutBackgroundImage));
            }
            else
            {
                //Add background
                View.BackgroundColor = string.IsNullOrEmpty(ViewModel.Settings.WorkoutBackgroundImage)
                                           ? UIColor.FromPatternImage(UIImage.FromBundle(Constants.ConcreteBackground))
                                           : UIColor.FromPatternImage(UIImage.FromBundle(ViewModel.Settings.WorkoutBackgroundImage));
            }


            //Add Custom font
            lblCurrentPosition.Font = UIFont.FromName(Constants.ChalkFont, 45f);
			lblCurrentPosition.LineBreakMode = UILineBreakMode.WordWrap;
			lblCurrentPosition.Lines = 1;
			lblCurrentPosition.ShadowColor = UIColor.Black;
			lblCurrentPosition.ShadowOffset = new SizeF (.80f, .80f);

			btnBack.Font = UIFont.FromName(Constants.ChalkFont, 32f);
			btnBack.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnBack.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnBack.TitleShadowOffset = new SizeF (.80f, .80f);
			btnBack.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);

			btnPauseResume.Font = UIFont.FromName(Constants.ChalkFont, 32f);
			btnPauseResume.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);
			btnPauseResume.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnPauseResume.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnPauseResume.TitleShadowOffset = new SizeF (.80f, .80f);

			btnReset.Font = UIFont.FromName(Constants.ChalkFont, 32f);
			btnReset.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);
			btnReset.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnReset.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnReset.TitleShadowOffset = new SizeF (.80f, .80f);

        }

        public override bool PrefersStatusBarHidden()
        {
            return true;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            if (!_viewingAd)
            {
                ViewModel.BeginWorkoutCommand.Execute(null);
            }
            else
            {
                _viewingAd = false;
                LoadAdMod();
            }

            //keep screen from locking
            UIApplication.SharedApplication.IdleTimerDisabled = true;
        }

        private void OnMessageReceived(DisplayMessage message)
        {
            if (message.Message == "ShowAd")
            {
                if (_didInterstitialLoad)
                {
                    _adMobView.PresentFromRootViewController(this);
                    _viewingAd = true;
                }

            }
        }

        private void LoadIAd()
        {
            _iAdBannerView = new ADBannerView();
            ADBannerView adBannerView = _iAdBannerView as ADBannerView;

            adBannerView.AdLoaded += (sender, args) =>
                {
                    _iAdBannerView.Hidden = false;
                };
            adBannerView.FailedToReceiveAd += (sender, args) => { _iAdBannerView.Hidden = true; };

            adBannerView.Frame = new RectangleF(0, Constants.IsTallScreen ? 518 : 430, View.Bounds.Width, 50);
            View.AddSubview(adBannerView);
            adBannerView.Hidden = true;
        }

        private void LoadAdMod()
        {
            _adMobView = new GADInterstitial { AdUnitID = AdmobId };

            _adMobView.LoadRequest(GADRequest.Request);

            _adMobView.DidReceiveAd += (sender, args) =>
            {
                _didInterstitialLoad = true;
            };

            _adMobView.DidFailToReceiveAd += (sender, args) =>
                {
                    _didLoadInterFailed = true;
                    //Show Paid Ap Ad Here
                };

            _adMobView.DidDismissScreen += (sender, e) =>
            {

            };
        }



    }

}