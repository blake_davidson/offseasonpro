using Cirrious.CrossCore;
using Cirrious.MvvmCross.Binding.BindingContext;
using Cirrious.MvvmCross.Plugins.Messenger;
using Cirrious.MvvmCross.Touch.Views;
using GoogleAdMobAds;
using MonoTouch.MediaPlayer;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using OffSeasonPro.Core;
using OffSeasonPro.Core.Messages;
using OffSeasonPro.Core.ViewModels;
using OffSeasonPro.Plugin.MusicPlayerPlugin.Touch;
using System.Drawing;

namespace OffSeasonPro.Touch.Views
{
    public partial class HomeView
        : MvxViewController
    {
        private readonly MyMusicPlayer _player = MyMusicPlayer.Instance;

        public HomeView()
            : base("HomeView", null)
        {
        }

        public new HomeViewModel ViewModel
        {
            get { return (HomeViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            //'Hide' Navigation bar
            NavigationController.SetNavigationBarHidden(true, false);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();



            // ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            // Perform any additional setup after loading the view, typically from a nib.
            MvxFluentBindingDescriptionSet<HomeView, HomeViewModel> set =
                this.CreateBindingSet<HomeView, HomeViewModel>();
            set.Bind(btnSettings).To(vm => vm.ShowSettingsCommand);
            set.Apply();

            btnAction.TouchUpInside += delegate
                {
 
                    if (ViewModel.Settings.CurrentWorkout.InWorkout())
                    {
                        new UIAlertView("Resume Workout", "Would you like to continue the previous workout?",
                                        new ContinueWorkoutDelegate(ViewModel), "Yes", "No").Show();
                    }
                    else
                    {
                        ViewModel.ToggleActionCommand.Execute(null);
                    }
                };

            btnPlaylists.TouchUpInside += delegate { ShowMediaPicker(); };

            //Set Background
            View.BackgroundColor = UIColor.FromPatternImage(UIImage.FromBundle(Constants.Background));

            //Add Custom ChalkFont
            btnAction.Font = UIFont.FromName(Constants.ChalkFont, 75f);
			btnPlaylists.Font = UIFont.FromName(Constants.ChalkFont, 32f);
			btnSettings.Font = UIFont.FromName(Constants.ChalkFont, 32f);

			btnAction.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);
			btnAction.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnAction.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnAction.TitleShadowOffset = new SizeF (.80f, .80f);

			btnPlaylists.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);
			btnPlaylists.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnPlaylists.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnPlaylists.TitleShadowOffset = new SizeF (.80f, .80f);

			btnSettings.SetTitleColor (UIColor.DarkGray, UIControlState.Highlighted);
			btnSettings.SetTitleShadowColor (UIColor.Black, UIControlState.Normal);
			btnSettings.SetTitleShadowColor (UIColor.Black, UIControlState.Highlighted);
			btnSettings.TitleShadowOffset = new SizeF (.80f, .80f);
        }

        public void ShowMediaPicker()
        {
            var picker = new MediaPickerView();
            picker.Delegate = new MyMusicPlayer.MediaPickerDelegate(_player, picker);
            picker.AllowsPickingMultipleItems = true;
            picker.Prompt = @"Select Songs to Play";
            PresentViewController(picker, true, null);
        }

        public override bool PrefersStatusBarHidden()
        {
            return true;
        }

        private class ContinueWorkoutDelegate : UIAlertViewDelegate
        {
            private readonly HomeViewModel _vm;

            public ContinueWorkoutDelegate(HomeViewModel vm)
            {
                _vm = vm;
            }

            public override void Clicked(UIAlertView alertview, int buttonIndex)
            {
                if (buttonIndex == 0)
                {
                    _vm.ToggleActionCommand.Execute(null);
                }
                else
                {
                    _vm.Settings.ClearWorkout();
                    _vm.ToggleActionCommand.Execute(null);
                }
            }
        }

    }
}