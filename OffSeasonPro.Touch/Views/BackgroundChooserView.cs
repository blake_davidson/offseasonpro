using System;
using System.Collections.Generic;
using System.Drawing;
using Cirrious.MvvmCross.Touch.Views;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using OffSeasonPro.Core.ViewModels;

namespace OffSeasonPro.Touch.Views
{
    public class BackgroundSource : UICollectionViewSource
    {
        public BackgroundSource()
        {
            Rows = new List<BackgroundImageElement>();
        }

        public List<BackgroundImageElement> Rows { get; private set; }

        public Single FontSize { get; set; }

        public SizeF ImageViewSize { get; set; }

        public override Int32 NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override Int32 GetItemsCount(UICollectionView collectionView, Int32 section)
        {
            return Rows.Count;
        }

        public override Boolean ShouldHighlightItem(UICollectionView collectionView, NSIndexPath indexPath)
        {
            return true;
        }

        public override void ItemHighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (BackgroundImageCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 0.5f;
        }

        public override void ItemUnhighlighted(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (BackgroundImageCell)collectionView.CellForItem(indexPath);
            cell.ImageView.Alpha = 1;

            BackgroundImageElement row = Rows[indexPath.Row];
            row.Tapped.Invoke();
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (BackgroundImageCell)collectionView.DequeueReusableCell(BackgroundImageCell.CellId, indexPath);

            BackgroundImageElement row = Rows[indexPath.Row];

            cell.UpdateRow(row, FontSize, ImageViewSize);

            return cell;
        }
    }

    public class BackgroundImageElement
    {
        public BackgroundImageElement(UIImage image, NSAction tapped)
        {
            Image = image;
            Tapped = tapped;
        }

        public UIImage Image { get; set; }

        public NSAction Tapped { get; set; }
    }

    public sealed class BackgroundImageCell : UICollectionViewCell
    {
        public static NSString CellId = new NSString("BackgroundSource");

        [Export("initWithFrame:")]
        public BackgroundImageCell(RectangleF frame)
            : base(frame)
        {
            ImageView = new UIImageView();
            ImageView.Layer.BorderColor = UIColor.DarkGray.CGColor;
            ImageView.Layer.BorderWidth = 1f;
            ImageView.Layer.CornerRadius = 3f;
            ImageView.Layer.MasksToBounds = true;
            ImageView.ContentMode = UIViewContentMode.ScaleToFill;

            ContentView.AddSubview(ImageView);
        }

        public UIImageView ImageView { get; private set; }

        public void UpdateRow(BackgroundImageElement element, Single fontSize, SizeF imageViewSize)
        {
            ImageView.Image = element.Image;

            ImageView.Frame = new RectangleF(0, 0, imageViewSize.Width, imageViewSize.Height);
        }
    }

    public partial class BackgroundChooserView : MvxViewController
    {
        private BackgroundSource _backgroundSource;
        private UICollectionView _collectionViewUser;

        public BackgroundChooserView()
        {
        }

        public new BackgroundChooserViewModel ViewModel
        {
            get { return (BackgroundChooserViewModel)base.ViewModel; }
            set { base.ViewModel = value; }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            // ios7 layout
            if (RespondsToSelector(new Selector("edgesForExtendedLayout")))
                EdgesForExtendedLayout = UIRectEdge.None;

            _backgroundSource = new BackgroundSource { FontSize = 11f, ImageViewSize = new SizeF(170, 200) };

            _collectionViewUser = new UICollectionView(new RectangleF(0, 0, View.Bounds.Width, View.Bounds.Height), new UICollectionViewFlowLayout { ItemSize = new SizeF(170, 200), ScrollDirection = UICollectionViewScrollDirection.Vertical });

            _collectionViewUser.RegisterClassForCell(typeof(BackgroundImageCell), BackgroundImageCell.CellId);
            _collectionViewUser.ShowsHorizontalScrollIndicator = false;
            _collectionViewUser.Source = _backgroundSource;

            var bgs = new List<string>
                {
                    "Backgrounds/OffSeason_Pro_ChalkBoard",
                    "Backgrounds/OffSeason_Pro_Asphalt",
                    "Backgrounds/OffSeason_Pro_Background",
                    "Backgrounds/OffSeason_Pro_Concrete",
                    "Backgrounds/OffSeason_Pro_Court"
                };

            foreach (var bg in bgs)
            {
                _backgroundSource.Rows.Add(Constants.IsTallScreen
                                               ? new BackgroundImageElement(UIImage.FromBundle(bg + "-5"),
                                                                            () => ElementTapped(bg + "-5"))
                                               : new BackgroundImageElement(UIImage.FromBundle(bg),
                                                                            () => ElementTapped(bg)));
            }
     
            View.Add(_collectionViewUser);

			var btnInfo = new UIButton (UIButtonType.InfoLight);

			btnInfo.Frame = new RectangleF (View.Bounds.Width - btnInfo.Bounds.Width - 5, 5, btnInfo.Bounds.Width, btnInfo.Bounds.Height);
            btnInfo.TintColor = UIColor.LightGray;

            btnInfo.TouchUpInside += (sender, e) => new UIAlertView("Custom Background",
                "Interested in us creating a custom background? Press OK to send us an e-mail, and we'll be in touch!"
                , new CustomBgDelegate(ViewModel)  , "Cancel", new[] {"OK"}).Show();
			View.Add (btnInfo);

			var attr = new UITextAttributes ();
			attr.TextColor = UIColor.LightGray;
			attr.TextShadowColor = UIColor.Black;
			attr.TextShadowOffset = new UIOffset (.80f, .80f);

			var btnCancel = new UIBarButtonItem ("Cancel", UIBarButtonItemStyle.Plain,(s, e) => ViewModel.CloseCommand.Execute (null));
			btnCancel.SetTitleTextAttributes (attr, UIControlState.Normal);

			var btnPhoto = new UIBarButtonItem(UIBarButtonSystemItem.Camera, (s,e) => ShowPhotoActionSheet());
			btnPhoto.TintColor = UIColor.LightGray;

            this.SetToolbarItems(new UIBarButtonItem[] {
				  btnPhoto
                , new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace) { Width = 50 }
				, btnCancel  
            }, false);

            //Add background
            _collectionViewUser.BackgroundView = null;
            _collectionViewUser.BackgroundColor = UIColor.Clear;

            _collectionViewUser.ReloadData();
        }


        private void ShowPhotoActionSheet()
        {
            var mediaPicker = new UIImagePickerController();
            var del = new PictureChosenDelegate(ViewModel);
            mediaPicker.Delegate = del;
            mediaPicker.AllowsEditing = true;
            if (UIImagePickerController.IsSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
            {
				var actionsheet = new UIActionSheet ("");

				actionsheet.AddButton("Take Photo");
				actionsheet.AddButton("Choose Existing");
				actionsheet.AddButton("Cancel");
				actionsheet.AddButton("");

				actionsheet.CancelButtonIndex = 2;

				actionsheet.Style = UIActionSheetStyle.Automatic;
                actionsheet.Clicked += delegate(object sender, UIButtonEventArgs args)
                    {
                        switch (args.ButtonIndex)
                        {
                            case 0:
                                mediaPicker.SourceType = UIImagePickerControllerSourceType.Camera;
                                PresentViewController(mediaPicker, true, null);
                                break;
							case 1:
                                mediaPicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                                PresentViewController(mediaPicker, true, null);
                                break;
							case 2:
						        actionsheet.DismissWithClickedButtonIndex(2, true);
                                break;
							default:
								actionsheet.DismissWithClickedButtonIndex(2, true);
								break;
				        }
                    };
                actionsheet.ShowInView(View);
            }
            else
            {
                mediaPicker.SourceType = UIImagePickerControllerSourceType.PhotoLibrary;
                this.PresentViewController(mediaPicker, true, null);
            }

        }

        public override bool PrefersStatusBarHidden()
        {
            return true;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            if(NavigationController != null)
                NavigationController.ToolbarHidden = false;
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
            this.NavigationController.ToolbarHidden = true;
        }

        private void ElementTapped(String background)
        {
            ViewModel.Settings.WorkoutBackgroundImage = background;
            ViewModel.Settings.UsePhotoAlbumForBgImage = false;
            ViewModel.CloseCommand.Execute(null);
        }

        public class CustomBgDelegate : UIAlertViewDelegate
        {
            private readonly BackgroundChooserViewModel _viewModel;

            public CustomBgDelegate(BackgroundChooserViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            public override void Clicked(UIAlertView alertview, int buttonIndex)
            {
                if(buttonIndex == 1)
                    _viewModel.SendBackgroundRequestEmail.Execute(null);
            }

        }

        public class PictureChosenDelegate : UIImagePickerControllerDelegate
        {
            public PictureChosenDelegate(BackgroundChooserViewModel viewModel)
            {
                _viewModel = viewModel;
            }

            private BackgroundChooserViewModel _viewModel;

            public override void FinishedPickingMedia(UIImagePickerController picker, NSDictionary info)
            {
                var cameraImage = info[UIImagePickerController.OriginalImage] as UIImage;
                if (cameraImage != null)
                {
                    var documentsDirectory = Environment.GetFolderPath
                         (Environment.SpecialFolder.Personal);
                    string jpgFilename = System.IO.Path.Combine(documentsDirectory, "user_background.jpg"); // hardcoded filename, overwritten each time
                    NSData imgData = cameraImage.AsJPEG();
                    NSError err = null;
                    if (imgData.Save(jpgFilename, false, out err))
                    {
                        _viewModel.Settings.WorkoutBackgroundImage = jpgFilename;
                        _viewModel.Settings.UsePhotoAlbumForBgImage = true;
                        _viewModel.CloseCommand.Execute(null);
                    }
                    picker.DismissViewController(true, null);
                }
            }

            public override void Canceled(UIImagePickerController picker)
            {
                picker.DismissViewController(true, null);
            }
        }
    }
}