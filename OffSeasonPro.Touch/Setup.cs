using System;
using System.Reflection;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using Cirrious.MvvmCross.Touch.Platform;
using Cirrious.MvvmCross.Touch.Views.Presenters;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core;
using Cirrious.MvvmCross.Localization;
using Cirrious.CrossCore.Plugins;
using Cirrious.CrossCore;
using OffSeasonPro.Core.Interfaces;
using System.Collections.Generic;

namespace OffSeasonPro.Touch
{
    public class Setup 
        : MvxTouchSetup
    {
        private OffSeasonProPresenter _presenter;

        public Setup(MvxApplicationDelegate applicationDelegate, OffSeasonProPresenter presenter)
            : base(applicationDelegate, presenter)
        {
            _presenter = presenter;
        }

        protected override IMvxApplication CreateApp()
        {

            String fonts = "--------------------------------------\n";
            List<String> fontFamilies = new List<String>(UIFont.FamilyNames);
            fontFamilies.Sort();
            foreach (String familyName in fontFamilies)
            {
                foreach (String fontName in UIFont.FontNamesForFamilyName(familyName))
                {
                    fonts += familyName;
                    fonts += "\t";
                    fonts += fontName;
                    fonts += "\n";
                }
                fonts += "--------------------------------------\n";
            }
            Console.WriteLine(fonts);

            return new App();
        }

        protected override void AddPluginsLoaders(MvxLoaderPluginRegistry registry)
        {
            registry.AddConventionalPlugin<Cirrious.MvvmCross.Plugins.File.Touch.Plugin>();
            registry.AddConventionalPlugin<Cirrious.MvvmCross.Plugins.ResourceLoader.Touch.Plugin>();
          //  registry.AddConventionalPlugin<Cirrious.MvvmCross.Plugins.DownloadCache.Touch.Plugin>();
            base.AddPluginsLoaders(registry);
        }

        protected override void InitializeLastChance()
        {
            var errorDisplayer = new ErrorDisplayer();
            
            base.InitializeLastChance();

            Mvx.RegisterSingleton<IViewModelCloser>(_presenter);
            Cirrious.MvvmCross.Plugins.File.PluginLoader.Instance.EnsureLoaded();
            
        }
    }
}