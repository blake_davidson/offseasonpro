﻿using System;
using System.Collections.Generic;

namespace OffSeasonPro.Plugin.TextToSpeech
{
    public interface ITextToSpeech
    {
        void Play(string text, Action finished);
		void TextToSpeech(Dictionary<string,string> textsAndFiles, Action Finished, Action ErrorOccured);
        Dictionary<string, string> Texts { get; set; }
    }
}
