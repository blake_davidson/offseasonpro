﻿using System;
using Cirrious.CrossCore.Core;
using OffSeasonPro.Core.Interfaces;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core.ApplicationObjects
{
    public class ErrorApplicationObject
        : MvxMainThreadDispatchingObject
          , IErrorReporter
          , IErrorSource
    {
        public void ReportError(string error)
        {
            if (ErrorReported == null)
                return;

            InvokeOnMainThread(() =>
                {
                    EventHandler<ErrorEventArgs> handler = ErrorReported;
                    if (handler != null)
                        handler(this, new ErrorEventArgs(error));
                });
        }

        public event EventHandler<ErrorEventArgs> ErrorReported;
    }
}