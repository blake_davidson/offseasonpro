﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Cirrious.MvvmCross.Plugins.Messenger;

namespace OffSeasonPro.Core.Messages
{
    public class DisplayMessage
        :MvxMessage
    {
        public DisplayMessage(object sender, string message)
            :base(sender)
        {
            Message = message;
        }

        public string Message { get; private set; }
    }
}
