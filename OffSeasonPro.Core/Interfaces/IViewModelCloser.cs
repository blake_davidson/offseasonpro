﻿using Cirrious.MvvmCross.ViewModels;

namespace OffSeasonPro.Core.Interfaces
{
    public interface IViewModelCloser
    {
        void RequestClose(IMvxViewModel viewModel);
    }
}