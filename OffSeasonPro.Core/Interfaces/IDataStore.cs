﻿using OffSeasonPro.Core.Models;
using System;

namespace OffSeasonPro.Core.Interfaces
{
    public interface IDataStore
    {
        Settings Settings { get; }
        void SaveSettings();
        void ReloadSettings();
		void CreateTts(Action Finished, Action ErrorOccured);
    }
}