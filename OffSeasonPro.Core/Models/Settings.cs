﻿namespace OffSeasonPro.Core.Models
{
    public class Settings
    {
        public string StationCount { get; set; }
        public string PartnerCount { get; set; }
        public string SetCount { get; set; }
        public string SetLengthSeconds { get; set; }
        public string TransitionLengthSeconds { get; set; }
        public bool SkipAfterLift { get; set; }
        public string StartupLength { get; set; }
        public string WorkoutBackgroundImage { get; set; }
        public bool UsePhotoAlbumForBgImage { get; set; }
        public bool IsPaid { get; private set; }

        public Workout CurrentWorkout { get; set; }
    }
}