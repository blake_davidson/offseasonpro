﻿namespace OffSeasonPro.Core.Models
{
    public class Workout
    {
        public int CurrentPartner;
        public int CurrentSet;
        public int CurrentStation;
        public int SecondsElapsed;
        public bool Transitioning;
        public bool WarmingUp;
    }
}