﻿using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core.ViewModels
{
    public class HomeViewModel
        : BaseViewModel
    {
        public HomeViewModel()
        {
          //  DataStore.CreateTts();
        }

        public Settings Settings
        {
            get { return DataStore.Settings; }
        }

        public ICommand ShowSettingsCommand
        {
            get { return new MvxCommand(() => ShowViewModel<SettingsViewModel>()); }
        }

        public ICommand ToggleActionCommand
        {
            get { return new MvxCommand(ToggleAction); }
        }

        private void ToggleAction()
        {
            if (!Settings.SettingsValid())
                ReportError("Invalid Settings. Values must be greater than 0");
            else
            {
                ShowViewModel<WorkoutViewModel>();
            }
        }
    }
}