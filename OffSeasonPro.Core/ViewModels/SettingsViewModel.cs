using System.Windows.Input;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Models;

namespace OffSeasonPro.Core.ViewModels
{
    public class SettingsViewModel
        : BaseViewModel
    {
        private Settings _settings;

        public Settings Settings
        {
            get { return DataStore.Settings; }
            set
            {
                _settings = value;
                RaisePropertyChanged(() => Settings);
            }
        }

        public ICommand CancelSave
        {
            get { return new MvxCommand(Cancel); }
        }

        public ICommand SaveSettings
        {
            get { return new MvxCommand(Save); }
        }

		private bool _isBusy;
		public bool IsBusy {
			get { return _isBusy; }
			set {_isBusy = value; RaisePropertyChanged (() => IsBusy); }
		}

        private void Cancel()
        {
            DataStore.ReloadSettings();
            CloseCommand.Execute(null);
        }

        private void Save()
        {
            if (!Settings.SettingsValid())
                ReportError("Invalid Settings. Values must be greater than 0");
            else
            {
				IsBusy = true;
				Settings.ClearWorkout ();
                DataStore.SaveSettings();               
				DataStore.CreateTts (Finished, ErrorOccured);
            }
        }

		void Finished ()
		{
			IsBusy = false;
			//CloseCommand.Execute(null);
		}

		void ErrorOccured ()
		{
			ReportError ("Error Saving Settings. Please try again");
		}

        public ICommand ShowBackgroundChooser
        {
            get { return new MvxCommand(ShouldShowBackgroundChooser) ; }
        }

        private void ShouldShowBackgroundChooser()
        {
            if (Settings.IsPaid)
            {
                ShowViewModel<BackgroundChooserViewModel>();
            }
            else
            {
                ReportError("Please download the full app to choose a custom background.");
            }
        }
    }
}