using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Cirrious.CrossCore;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Plugins.File;
using Cirrious.MvvmCross.ViewModels;
using OffSeasonPro.Core.Models;
using Cirrious.MvvmCross.Plugins.DownloadCache;

namespace OffSeasonPro.Core.ViewModels
{
    public class BackgroundChooserViewModel
        : BaseViewModel
    {

        public BackgroundChooserViewModel()
        {
        }

        private Settings _settings;
        public Settings Settings
        {
            get { return DataStore.Settings; }
            set
            {
                _settings = value;
                RaisePropertyChanged(() => Settings);
            }
        }

        public ICommand SendBackgroundRequestEmail
        {
            get {return new MvxCommand(() => ComposeEmail("offseasonpro@gmail.com", "Custom Background", GetEmailBody()));}
        }

        public ICommand SaveSettings
        {
            get { return new MvxCommand(Save); }
        }

        private void Save()
        {
            DataStore.SaveSettings();
            CloseCommand.Execute(null);
        }

        private string GetEmailBody()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Team OSP - ");
            sb.AppendLine(" ");
            sb.AppendLine(
                "I am interested in a custom background for the offseason pro app. Please contact me ASAP for more details.");

            return sb.ToString();
        }
    }
}
