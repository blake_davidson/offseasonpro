﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OffSeasonPro.Plugin.MusicPlayerPlugin
{
    public interface IMusicPlayer
    {
        void Start();
        void Stop();
        void NextTrack();
    }
}
